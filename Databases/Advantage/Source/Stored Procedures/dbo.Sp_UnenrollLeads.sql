SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[Sp_UnenrollLeads]
    @leadid UNIQUEIDENTIFIER
   ,@leadstatus UNIQUEIDENTIFIER
AS
    BEGIN
        DECLARE @stuenrollid UNIQUEIDENTIFIER
           ,@studentid UNIQUEIDENTIFIER
           ,@studentawardid UNIQUEIDENTIFIER;
        DECLARE @paymentplanid UNIQUEIDENTIFIER
           ,@oldtransactionid UNIQUEIDENTIFIER
           ,@oldstudentawardid UNIQUEIDENTIFIER;
        DECLARE @counter INT
           ,@timer INT
           ,@@transactionid VARCHAR(8000)
           ,@@studentawardid VARCHAR(8000);
        DECLARE @@paymentawardid VARCHAR(8000)
           ,@oldpaymentplanid VARCHAR(8000)
           ,@@paymentplanid VARCHAR(8000);
        DECLARE @errornumber INT
           ,@TransCount INT
           ,@StudentAwardCount INT
           ,@PaymentPlanCount INT;

        
        SET @stuenrollid = (
                             SELECT DISTINCT
                                    StuEnrollId
                             FROM   arStuEnrollments
                             WHERE  LeadId = @leadid
                           );
        SET @studentid = (
                           SELECT DISTINCT
                                    StudentId
                           FROM     arStuEnrollments
                           WHERE    StuEnrollId = @stuenrollid
                         );

-- Begin Section 1 
--        delete from adEntrTestOverRide where LeadId=@leadid
--        delete from adLeadByLeadGroups where LeadId=@leadid
--        delete from adLeadDocsReceived where LeadId=@leadid
--        delete from adLeadEducation where LeadId=@leadid
--        delete from adLeadEmployment where LeadId=@leadid
--        delete from adLeadEntranceTest where LeadId=@leadid
--        delete from adLeadExtraCurriculars where LeadID=@leadid
--        delete from adLeadNotes where LeadId=@leadid
--        delete from adLeadPhone where LeadId=@leadid
--        delete from adLeadSkills where LeadId=@leadid
--        delete from cmActivityAssignment where LeadId=@leadid
--        delete from syDocumentHistory where LeadId=@leadid
--        delete from syLeadStatusesChanges where LeadId=@leadid
-- End Section 1 
        

-- Begin Section 3
        --delete from adLeadByLeadGroups where StuEnrollId=@stuenrollid
        UPDATE  adLeadByLeadGroups
        SET     StuEnrollId = NULL
        WHERE   StuEnrollId = @stuenrollid;          

        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arExternshipAttendance' )
            BEGIN
                DELETE  FROM arExternshipAttendance
                WHERE   StuEnrollId = @stuenrollid;
            END;
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arGrdBkConversionResults' )
            BEGIN
                DELETE  FROM arGrdBkConversionResults
                WHERE   StuEnrollId = @stuenrollid;
            END;
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arGrdBkResults' )
            BEGIN
                DELETE  FROM arGrdBkResults
                WHERE   StuEnrollId = @stuenrollid;
            END;
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arOverridenPreReqs' )
            BEGIN
                DELETE  FROM arOverridenPreReqs
                WHERE   StuEnrollId = @stuenrollid;
            END;
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arResults' )
            BEGIN
                DELETE  FROM arResults
                WHERE   StuEnrollId = @stuenrollid;
            END;
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arSAPChkResults' )
            BEGIN
                DELETE  FROM arSAPChkResults
                WHERE   StuEnrollId = @stuenrollid;
            END;
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arStdSuspensions' )
            BEGIN
                DELETE  FROM arStdSuspensions
                WHERE   StuEnrollId = @stuenrollid;
            END;
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arStudentClockAttendance' )
            BEGIN
                DELETE  FROM arStudentClockAttendance
                WHERE   StuEnrollId = @stuenrollid;
            END;
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arStudentLOAs' )
            BEGIN
                DELETE  FROM arStudentLOAs
                WHERE   StuEnrollId = @stuenrollid;
            END;
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arStudentSchedules' )
            BEGIN
                DELETE  FROM arStudentSchedules
                WHERE   StuEnrollId = @stuenrollid;
            END;
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arStudentTimeClockPunches' )
            BEGIN
                DELETE  FROM arStudentTimeClockPunches
                WHERE   StuEnrollId = @stuenrollid;
            END;
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arStuProbWarnings' )
            BEGIN
                DELETE  FROM arStuProbWarnings
                WHERE   StuEnrollId = @stuenrollid;
            END;
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arTrackTransfer' )
            BEGIN
                DELETE  FROM arTrackTransfer
                WHERE   StuEnrollId = @stuenrollid;
            END;
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arTransferGrades' )
            BEGIN
                DELETE  FROM arTransferGrades
                WHERE   StuEnrollId = @stuenrollid;
            END;
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'atClsSectAttendance' )
            BEGIN
                DELETE  FROM atClsSectAttendance
                WHERE   StuEnrollId = @stuenrollid;
            END;
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arStudentClockAttendance' )
            BEGIN
                DELETE  FROM arStudentClockAttendance
                WHERE   StuEnrollId = @stuenrollid;
            END;
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'atConversionAttendance' )
            BEGIN
                DELETE  FROM atConversionAttendance
                WHERE   StuEnrollId = @stuenrollid;
            END;
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'atOnLineStudents' )
            BEGIN
                DELETE  FROM atOnLineStudents
                WHERE   StuEnrollId = @stuenrollid;
            END;
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'ComCourseStudents' )
            BEGIN
                DELETE  FROM ComCourseStudents
                WHERE   StuEnrollId = @stuenrollid;
            END;


        SET @StudentAwardCount = (
                                   SELECT   COUNT(*)
                                   FROM     faStudentAwards
                                   WHERE    StuEnrollId = @stuenrollid
                                 );
        IF EXISTS ( SELECT DISTINCT
                            StudentAwardId
                    FROM    faStudentAwards
                    WHERE   StuEnrollId = @stuenrollid )
            BEGIN
                IF EXISTS ( SELECT  1
                            FROM    sysobjects
                            WHERE   type = 'u'
                                    AND name = 'saPmtDisbRel' )
                    BEGIN
                        DELETE  FROM saPmtDisbRel
                        WHERE   AwardScheduleId IN ( SELECT DISTINCT
                                                            AwardScheduleId
                                                     FROM   faStudentAwardSchedule
                                                     WHERE  StudentAwardId IN ( SELECT DISTINCT
                                                                                        StudentAwardId
                                                                                FROM    faStudentAwards
                                                                                WHERE   StuEnrollId = @stuenrollid ) );
                    END;
                IF EXISTS ( SELECT  1
                            FROM    sysobjects
                            WHERE   type = 'u'
                                    AND name = 'faStudentAwardSchedule' )
                    BEGIN
                        DELETE  FROM faStudentAwardSchedule
                        WHERE   StudentAwardId IN ( SELECT DISTINCT
                                                            StudentAwardId
                                                    FROM    faStudentAwards
                                                    WHERE   StuEnrollId = @stuenrollid );
                    END;
            END;
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'faStudentAwards' )
            BEGIN
                DELETE  FROM faStudentAwards
                WHERE   StuEnrollId = @stuenrollid;
            END;        
       

        SET @PaymentPlanCount = (
                                  SELECT    COUNT(*)
                                  FROM      faStudentPaymentPlans
                                  WHERE     StuEnrollId = @stuenrollid
                                );
        IF ( @PaymentPlanCount >= 1 )
            BEGIN
                IF EXISTS ( SELECT  1
                            FROM    sysobjects
                            WHERE   type = 'u'
                                    AND name = 'saPmtDisbRel' )
                    BEGIN
                        DELETE  FROM saPmtDisbRel
                        WHERE   PayPlanScheduleId IN ( SELECT DISTINCT
                                                                PayPlanScheduleId
                                                       FROM     faStuPaymentPlanSchedule
                                                       WHERE    PaymentPlanId IN ( SELECT DISTINCT
                                                                                            PaymentPlanId
                                                                                   FROM     faStudentPaymentPlans
                                                                                   WHERE    StuEnrollId = @stuenrollid ) );
                    END;
                IF EXISTS ( SELECT  1
                            FROM    sysobjects
                            WHERE   type = 'u'
                                    AND name = 'faStuPaymentPlanSchedule' )
                    BEGIN
                        DELETE  FROM faStuPaymentPlanSchedule
                        WHERE   PaymentPlanId IN ( SELECT DISTINCT
                                                            PaymentPlanId
                                                   FROM     faStudentPaymentPlans
                                                   WHERE    StuEnrollId = @stuenrollid );
                    END;
                IF EXISTS ( SELECT  1
                            FROM    sysobjects
                            WHERE   type = 'u'
                                    AND name = 'faStudentPaymentPlans' )
                    BEGIN
                        DELETE  FROM faStudentPaymentPlans
                        WHERE   StuEnrollId = @stuenrollid;
                    END;
            END;
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'PlStudentsPlaced' )
            BEGIN
                DELETE  FROM PlStudentsPlaced
                WHERE   StuEnrollId = @stuenrollid;
            END;
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'saAdmissionDeposits' )
            BEGIN
                DELETE  FROM saAdmissionDeposits
                WHERE   StuEnrollId = @stuenrollid;
            END;
        SET @TransCount = (
                            SELECT  COUNT(*)
                            FROM    saTransactions
                            WHERE   StuEnrollId = @stuenrollid
                          );
        IF ( @TransCount >= 1 )
            BEGIN
                IF EXISTS ( SELECT  1
                            FROM    sysobjects
                            WHERE   type = 'u'
                                    AND name = 'saDeferredRevenues' )
                    BEGIN
                        DELETE  FROM saDeferredRevenues
                        WHERE   TransactionId IN ( SELECT   TransactionId
                                                   FROM     saTransactions
                                                   WHERE    StuEnrollId = @stuenrollid );      
                    END;
                IF EXISTS ( SELECT  1
                            FROM    sysobjects
                            WHERE   type = 'u'
                                    AND name = 'saGLDistributions' )
                    BEGIN
                        DELETE  FROM saGLDistributions
                        WHERE   TransactionId IN ( SELECT   TransactionId
                                                   FROM     saTransactions
                                                   WHERE    StuEnrollId = @stuenrollid );   
                    END;
                IF EXISTS ( SELECT  1
                            FROM    sysobjects
                            WHERE   type = 'u'
                                    AND name = 'saPayments' )
                    BEGIN
                        DELETE  FROM saPayments
                        WHERE   TransactionId IN ( SELECT   TransactionId
                                                   FROM     saTransactions
                                                   WHERE    StuEnrollId = @stuenrollid );    
                    END;
                IF EXISTS ( SELECT  1
                            FROM    sysobjects
                            WHERE   type = 'u'
                                    AND name = 'saReversedTransactions' )
                    BEGIN
                        DELETE  FROM saReversedTransactions
                        WHERE   TransactionId IN ( SELECT   TransactionId
                                                   FROM     saTransactions
                                                   WHERE    StuEnrollId = @stuenrollid );      
                    END;
                IF EXISTS ( SELECT  1
                            FROM    sysobjects
                            WHERE   type = 'u'
                                    AND name = 'saReversedTransactions' )
                    BEGIN
                        DELETE  FROM saReversedTransactions
                        WHERE   TransactionId IN ( SELECT   TransactionId
                                                   FROM     saTransactions
                                                   WHERE    StuEnrollId = @stuenrollid );  
                    END;
                IF EXISTS ( SELECT  1
                            FROM    sysobjects
                            WHERE   type = 'u'
                                    AND name = 'saTransactions' )
                    BEGIN
                        DELETE  FROM saTransactions
                        WHERE   StuEnrollId = @stuenrollid;      
                    END;
            END;
        
       
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'syStudentStatusChanges' )
            BEGIN
                DELETE  FROM syStudentStatusChanges
                WHERE   StuEnrollId = @stuenrollid;
            END;
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'TranscriptWithIDS2' )
            BEGIN
                DELETE  FROM TranscriptWithIDS2
                WHERE   StuEnrollId = @stuenrollid;
            END;
-- End Section 3

        
-- Begin Section 4
--        delete from adEntrTestOverRide where StudentId=@studentid
--        delete from adLeadEntranceTest where StudentId=@studentid
--        delete from adQuickLead where StudentID=@studentid

        UPDATE  adEntrTestOverRide
        SET     StudentId = NULL
        WHERE   StudentId = @studentid;
        UPDATE  adLeadEntranceTest
        SET     StudentId = NULL
        WHERE   StudentId = @studentid;
        UPDATE  adQuickLead
        SET     StudentID = NULL
        WHERE   StudentID = @studentid;
		
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'adResumeObjective' )
            BEGIN
                DELETE  FROM adResumeObjective
                WHERE   StudentId = @studentid;
            END;
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arClsSectStudents' )
            BEGIN
                DELETE  FROM arClsSectStudents
                WHERE   StudentId = @studentid;
            END;
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'atClsSectAttendance' )
            BEGIN
                DELETE  FROM atClsSectAttendance
                WHERE   StudentId = @studentid;
            END;        
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'cmActivityAssignment' )
            BEGIN
                DELETE  FROM cmActivityAssignment
                WHERE   StudentId = @studentid;
            END;
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'cmRoleEmpStudent' )
            BEGIN
                DELETE  FROM cmRoleEmpStudent
                WHERE   StudentId = @studentid;
            END;
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'cmStudentPhoneType' )
            BEGIN
                DELETE  FROM cmStudentPhoneType
                WHERE   StudentId = @studentid;
            END;
       
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'plDocsByStudent' )
            BEGIN
                DELETE  FROM plDocsByStudent
                WHERE   StudentId = @studentid;
            END;
       
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'plStudentDocs' )
            BEGIN
                DELETE  FROM plStudentDocs
                WHERE   StudentId = @studentid;
            END;
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'References' )
            BEGIN
                DELETE  FROM [References]
                WHERE   StudentId = @studentid;
            END;
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'syDocumentHistory' )
            BEGIN
                DELETE  FROM syDocumentHistory
                WHERE   StudentId = @studentid;
            END;
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'syMessages' )
            BEGIN
                DELETE  FROM syMessages
                WHERE   StudentId = @studentid;
            END;
      
-- End Section 4

-- Begin Section 5
        IF EXISTS ( SELECT  1
                    FROM    sysobjects
                    WHERE   type = 'u'
                            AND name = 'arStuEnrollments' )
            BEGIN
                DELETE  FROM arStuEnrollments
                WHERE   StudentId = @studentid;
            END;
        UPDATE  adLeads
        SET     LeadStatus = @leadstatus
               ,StudentId = '00000000-0000-0000-0000-000000000000'
        WHERE   LeadId = @leadid;
        
-- End Section 5
    END;
GO
