SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_STUDENTGPA_GETDETAILS_BYTERM]
    @StuEnrollId UNIQUEIDENTIFIER
   ,@pTermId UNIQUEIDENTIFIER = NULL
   ,@termGPAGT DECIMAL(18,2) = NULL
   ,@termGPALT DECIMAL(18,2) = NULL
   ,@termCreditsRangeGT DECIMAL(18,2)
   ,@termCreditsRangeLT DECIMAL(18,2)
AS
    BEGIN
        DECLARE @CumulativeGPA_Range DECIMAL(18,2);
        DECLARE @cumSimpleCourseCredits DECIMAL(18,2)
           ,@cumSimple_GPA_Credits DECIMAL(18,2)
           ,@cumSimpleGPA DECIMAL(18,2)
           ,@cumSimpleCourseCredits_OverAll DECIMAL(18,2)
           ,@cumSimple_GPA_Credits_OverAll DECIMAL(18,2)
           ,@cumSimpleGPA_OverAll DECIMAL(18,2);
        DECLARE @GradeCourseRepetitionsMethod VARCHAR(50);
        DECLARE @times INT
           ,@counter INT
           ,@StudentId VARCHAR(50);
		  
        DECLARE @cumCourseCredits DECIMAL(18,2)
           ,@cumWeighted_GPA_Credits DECIMAL(18,2)
           ,@cumWeightedGPA DECIMAL(18,2);
        DECLARE @EquivCourse_SA_CC INT
           ,@EquivCourse_SA_GPA DECIMAL(18,2)
           ,@EquivCourse_SA_CC_OverAll INT
           ,@EquivCourse_SA_GPA_OverAll DECIMAL(18,2);

        DECLARE @CoursesNotRepeated TABLE
            (
             StuEnrollId UNIQUEIDENTIFIER
            ,TermId UNIQUEIDENTIFIER
            ,TermDescrip VARCHAR(100)
            ,ReqId UNIQUEIDENTIFIER
            ,ReqDescrip VARCHAR(100)
            ,ClsSectionId VARCHAR(50)
            ,CreditsEarned DECIMAL(18,2)
            ,CreditsAttempted DECIMAL(18,2)
            ,CurrentScore DECIMAL(18,2)
            ,CurrentGrade VARCHAR(10)
            ,FinalScore DECIMAL(18,2)
            ,FinalGrade VARCHAR(10)
            ,Completed BIT
            ,FinalGPA DECIMAL(18,2)
            ,Product_WeightedAverage_Credits_GPA DECIMAL(18,2)
            ,Count_WeightedAverage_Credits DECIMAL(18,2)
            ,Product_SimpleAverage_Credits_GPA DECIMAL(18,2)
            ,Count_SimpleAverage_Credits DECIMAL(18,2)
            ,ModUser VARCHAR(50)
            ,ModDate DATETIME
            ,TermGPA_Simple DECIMAL(18,2)
            ,TermGPA_Weighted DECIMAL(18,2)
            ,coursecredits DECIMAL(18,2)
            ,CumulativeGPA DECIMAL(18,2)
            ,CumulativeGPA_Simple DECIMAL(18,2)
            ,FACreditsEarned DECIMAL(18,2)
            ,Average DECIMAL(18,2)
            ,CumAverage DECIMAL(18,2)
            ,TermStartDate DATETIME
            ,rownumber INT
            ,StudentId UNIQUEIDENTIFIER
            );

--Set Values
        SET @GradeCourseRepetitionsMethod = (
                                              SELECT    Value
                                              FROM      dbo.syConfigAppSetValues t1
                                              INNER JOIN dbo.syConfigAppSettings t2 ON t1.SettingId = t2.SettingId
                                                                                       AND t2.KeyName = 'GradeCourseRepetitionsMethod'
                                            );

        SET @cumSimpleGPA = 0;

        CREATE TABLE #getGPAByStudentEnrollmentsAndTerm
            (
             StuEnrollId UNIQUEIDENTIFIER
            ,TermId UNIQUEIDENTIFIER
            ,TermCode VARCHAR(50)
            ,TermDescription VARCHAR(50)
            ,TermStartDate DATETIME
            ,TermCredits DECIMAL(18,2)
            ,CumulativeSimpleGPA DECIMAL(18,2)
            ,CumulativeWeightedGPA DECIMAL(18,2)
            );



        INSERT  INTO @CoursesNotRepeated
                SELECT  *
                       ,NULL AS rownumber
                       ,(
                          SELECT TOP 1
                                    StudentId
                          FROM      arStuEnrollments
                          WHERE     StuEnrollId = dbo.syCreditSummary.StuEnrollId
                        ) AS StudentId
                FROM    syCreditSummary
                WHERE   StuEnrollId IN ( @StuEnrollId )
                        AND ReqId IN ( SELECT   ReqId
                                       FROM     (
                                                  SELECT    ReqId
                                                           ,ReqDescrip
                                                           ,COUNT(*) AS counter
                                                  FROM      syCreditSummary
                                                  WHERE     StuEnrollId IN ( @StuEnrollId )
                                                  GROUP BY  ReqId
                                                           ,ReqDescrip
                                                  HAVING    COUNT(*) = 1
                                                ) dt );

/************* Get Course Repetition Methods and Build the temp table Starts Here *******************/


        IF LOWER(@GradeCourseRepetitionsMethod) = 'latest'
            BEGIN
                INSERT  INTO @CoursesNotRepeated
                        SELECT  *
                        FROM    (
                                  SELECT    *
                                           ,ROW_NUMBER() OVER ( PARTITION BY ReqId ORDER BY TermStartDate DESC ) AS RowNumber
                                           ,(
                                              SELECT TOP 1
                                                        StudentId
                                              FROM      arStuEnrollments
                                              WHERE     StuEnrollId = dbo.syCreditSummary.StuEnrollId
                                            ) AS StudentId
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId IN ( @StuEnrollId )
                                            AND (
                                                  FinalScore IS NOT NULL
                                                  OR FinalGrade IS NOT NULL
                                                )
                                            AND ReqId IN ( SELECT   ReqId
                                                           FROM     (
                                                                      SELECT    ReqId
                                                                               ,ReqDescrip
                                                                               ,COUNT(*) AS Counter
                                                                      FROM      syCreditSummary
                                                                      WHERE     StuEnrollId IN ( @StuEnrollId )
                                                                      GROUP BY  ReqId
                                                                               ,ReqDescrip
                                                                      HAVING    COUNT(*) > 1
                                                                    ) dt )
                                ) dt2
                        WHERE   RowNumber = 1
                        ORDER BY TermStartDate DESC;
            END;

        IF LOWER(@GradeCourseRepetitionsMethod) = 'best'
            BEGIN
                INSERT  INTO @CoursesNotRepeated
                        SELECT  *
                        FROM    (
                                  SELECT    *
                                           --,ROW_NUMBER() OVER ( PARTITION BY ReqId ORDER BY FinalGPA DESC ) AS RowNumber
                                           ,ROW_NUMBER() OVER ( PARTITION BY ReqId ORDER BY Completed DESC, CreditsEarned DESC, FinalGPA DESC ) AS RowNumber
                                           ,(
                                              SELECT TOP 1
                                                        StudentId
                                              FROM      arStuEnrollments
                                              WHERE     StuEnrollId = dbo.syCreditSummary.StuEnrollId
                                            ) AS StudentId
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId IN ( @StuEnrollId )
                                            AND (
                                                  FinalScore IS NOT NULL
                                                  OR FinalGrade IS NOT NULL
                                                )
                                            AND ReqId IN ( SELECT   ReqId
                                                           FROM     (
                                                                      SELECT    ReqId
                                                                               ,ReqDescrip
                                                                               ,COUNT(*) AS Counter
                                                                      FROM      syCreditSummary
                                                                      WHERE     StuEnrollId IN ( @StuEnrollId )
                                                                      GROUP BY  ReqId
                                                                               ,ReqDescrip
                                                                      HAVING    COUNT(*) > 1
                                                                    ) dt )
                                ) dt2
                        WHERE   RowNumber = 1; 
            END;

        IF LOWER(@GradeCourseRepetitionsMethod) = 'average'
            BEGIN
                INSERT  INTO @CoursesNotRepeated
                        SELECT  *
                        FROM    (
                                  SELECT    *
                                           ,ROW_NUMBER() OVER ( PARTITION BY ReqId ORDER BY FinalGPA DESC ) AS RowNumber
                                           ,(
                                              SELECT TOP 1
                                                        StudentId
                                              FROM      arStuEnrollments
                                              WHERE     StuEnrollId = dbo.syCreditSummary.StuEnrollId
                                            ) AS StudentId
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId IN ( @StuEnrollId )
                                            AND (
                                                  FinalScore IS NOT NULL
                                                  OR FinalGrade IS NOT NULL
                                                )
                                            AND ReqId IN ( SELECT   ReqId
                                                           FROM     (
                                                                      SELECT    ReqId
                                                                               ,ReqDescrip
                                                                               ,COUNT(*) AS Counter
                                                                      FROM      syCreditSummary
                                                                      WHERE     StuEnrollId IN ( @StuEnrollId )
                                                                      GROUP BY  ReqId
                                                                               ,ReqDescrip
                                                                      HAVING    COUNT(*) > 1
                                                                    ) dt )
                                ) dt2; 
            END;
	
/************* Get Course Repetition Methods and Build the temp table Ends Here *******************/

/**SELECT * FROM @CoursesNotRepeated WHERE StuEnrollId=@StuEnrollId AND TermId='EB6484F3-7FE2-4530-B557-945F2CB2ABE7' ORDER BY StuEnrollId,TermId

SELECT  SUM(CreditsEarned)
										  FROM      @CoursesNotRepeated
										  WHERE     StuEnrollId IN (@StuEnrollId)
													AND TermId IN ('EB6484F3-7FE2-4530-B557-945F2CB2ABE7')
													AND FinalGPA IS NOT NULL

--SELECT DISTINCT StuEnrollID,TermId FROM @CoursesNotRepeated;
**/
        DECLARE @TermId UNIQUEIDENTIFIER;
        DECLARE getNodes_Cursor CURSOR
        FOR
            SELECT  DISTINCT
                    StuEnrollId
                   ,TermId
            FROM    @CoursesNotRepeated;
	  

        OPEN getNodes_Cursor;
        FETCH NEXT FROM getNodes_Cursor
				INTO @StuEnrollId,@TermId;
        WHILE @@FETCH_STATUS = 0
            BEGIN
				----- Program Version GPA Starts Here-----------------------------------------------------------------------
                SET @cumSimpleCourseCredits = (
                                                SELECT  COUNT(coursecredits)
                                                FROM    @CoursesNotRepeated
                                                WHERE   StuEnrollId IN ( @StuEnrollId )
                                                        AND TermId IN ( @TermId )
                                                        AND FinalGPA IS NOT NULL
                                              );


                SET @cumSimple_GPA_Credits = (
                                               SELECT   SUM(FinalGPA)
                                               FROM     @CoursesNotRepeated
                                               WHERE    StuEnrollId IN ( @StuEnrollId )
                                                        AND TermId IN ( @TermId )
                                                        AND FinalGPA IS NOT NULL
                                             );

                SET @EquivCourse_SA_CC = (
                                           SELECT   ISNULL(COUNT(Credits),0) AS Credits
                                           FROM     arReqs
                                           WHERE    ReqId IN ( SELECT   CE.EquivReqId
                                                               FROM     arCourseEquivalent CE
                                                               INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                                               INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                                               WHERE    StuEnrollId IN ( @StuEnrollId )
                                                                        AND CS.TermId IN ( @TermId )
                                                                        AND R.GrdSysDetailId IS NOT NULL )
                                         );

                SET @EquivCourse_SA_GPA = (
                                            SELECT  SUM(ISNULL(GSD.GPA,0.00))
                                            FROM    arCourseEquivalent CE
                                            INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                            INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                            INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                            WHERE   StuEnrollId IN ( @StuEnrollId )
                                                    AND CS.TermId IN ( @TermId )
                                                    AND R.GrdSysDetailId IS NOT NULL
                                          );


                SET @cumSimpleCourseCredits = @cumSimpleCourseCredits + ISNULL(@EquivCourse_SA_CC,0.00);
                SET @cumSimple_GPA_Credits = @cumSimple_GPA_Credits + ISNULL(@EquivCourse_SA_GPA,0.00);

	
                IF @cumSimpleCourseCredits >= 1
                    BEGIN
                        SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits;
                    END; 

                SET @cumWeightedGPA = 0;
                SET @cumCourseCredits = (
                                          SELECT    SUM(coursecredits)
                                          FROM      @CoursesNotRepeated
                                          WHERE     StuEnrollId IN ( @StuEnrollId )
                                                    AND TermId IN ( @TermId )
                                                    AND FinalGPA IS NOT NULL
                                        );
                SET @cumWeighted_GPA_Credits = (
                                                 SELECT SUM(coursecredits * FinalGPA)
                                                 FROM   @CoursesNotRepeated
                                                 WHERE  StuEnrollId IN ( @StuEnrollId )
                                                        AND TermId IN ( @TermId )
                                                        AND FinalGPA IS NOT NULL
                                               );

                DECLARE @doesThisCourseHaveAEquivalentCourse INT
                   ,@EquivCourse_WGPA_CC1 INT
                   ,@EquivCourse_WGPA_GPA1 DECIMAL(18,2);
	
                SET @EquivCourse_WGPA_CC1 = (
                                              SELECT    SUM(ISNULL(Credits,0)) AS Credits
                                              FROM      arReqs
                                              WHERE     ReqId IN ( SELECT   CE.EquivReqId
                                                                   FROM     arCourseEquivalent CE
                                                                   INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                                                   INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                                                   WHERE    StuEnrollId IN ( @StuEnrollId )
                                                                            AND R.GrdSysDetailId IS NOT NULL
                                                                            AND CS.TermId IN ( @TermId ) )
                                            );
		
                SET @EquivCourse_WGPA_GPA1 = (
                                               SELECT   SUM(GSD.GPA * R1.Credits)
                                               FROM     arCourseEquivalent CE
                                               INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                               INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                               INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                               INNER JOIN arReqs R1 ON R1.ReqId = CE.ReqId
                                               WHERE    StuEnrollId IN ( @StuEnrollId )
                                                        AND R.GrdSysDetailId IS NOT NULL
                                                        AND CS.TermId IN ( @TermId )
                                             );

		
                SET @cumCourseCredits = @cumCourseCredits + ISNULL(@EquivCourse_WGPA_CC1,0.00);
                SET @cumWeighted_GPA_Credits = @cumWeighted_GPA_Credits + ISNULL(@EquivCourse_WGPA_GPA1,0.00);

                IF @cumCourseCredits >= 1
                    BEGIN
                        SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
                    END; 	

                DECLARE @TermCredits DECIMAL(18,2);
                SET @TermCredits = (
                                     SELECT SUM(CreditsEarned)
                                     FROM   @CoursesNotRepeated
                                     WHERE  StuEnrollId IN ( @StuEnrollId )
                                            AND TermId IN ( @TermId )
                                            AND FinalGPA IS NOT NULL
                                   );

                INSERT  INTO #getGPAByStudentEnrollmentsAndTerm
                        (
                         StuEnrollId
                        ,TermId
                        ,TermCode
                        ,TermDescription
                        ,TermStartDate
                        ,TermCredits
                        ,CumulativeSimpleGPA
                        ,CumulativeWeightedGPA
						)
                VALUES  (
                         @StuEnrollId
                        ,@TermId  -- TermId - uniqueidentifier
                        ,(
                           SELECT TOP 1
                                    TermCode
                           FROM     arTerm
                           WHERE    TermId = @TermId
                         )
                        ,(
                           SELECT TOP 1
                                    TermDescrip
                           FROM     arTerm
                           WHERE    TermId = @TermId
                         )
                        ,(
                           SELECT TOP 1
                                    StartDate
                           FROM     arTerm
                           WHERE    TermId = @TermId
                         )
                        ,@TermCredits
                        ,@cumSimpleGPA  -- CumulativeSimpleGPA - decimal
                        ,@cumWeightedGPA  -- CumulativeWeightedGPA - decimal
						);
		  
                FETCH NEXT FROM getNodes_Cursor
				INTO @StuEnrollId,@TermId;
            END;

        SELECT  *
        FROM    #getGPAByStudentEnrollmentsAndTerm
        WHERE   (
                  @pTermId IS NULL
                  OR TermId IN ( SELECT Val
                                 FROM   MultipleValuesForReportParameters(@pTermId,',',1) )
                )
                AND (
                      @termGPAGT IS NULL
                      OR CumulativeSimpleGPA >= @termGPAGT
                    )
                AND (
                      @termGPALT IS NULL
                      OR CumulativeSimpleGPA <= @termGPALT
                    )
                AND (
                      @termCreditsRangeGT IS NULL
                      OR TermCredits >= @termCreditsRangeGT
                    )
                AND (
                      @termCreditsRangeLT IS NULL
                      OR TermCredits <= @termCreditsRangeLT
                    )
        ORDER BY TermStartDate;
        DROP TABLE #getGPAByStudentEnrollmentsAndTerm;
        CLOSE getNodes_Cursor;
        DEALLOCATE getNodes_Cursor;
    END;

GO
