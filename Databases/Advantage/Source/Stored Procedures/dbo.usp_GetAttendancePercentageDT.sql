SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetAttendancePercentageDT]
    (
     @stuEnrollId UNIQUEIDENTIFIER
    ,@cutOffDate DATETIME
    ,@clsSectionId UNIQUEIDENTIFIER = NULL  
    )
AS
    SET NOCOUNT ON;  
  
  
    SET DATEFIRST 7;   
    SELECT DISTINCT
            CSA.ClsSectionId
           ,TI.TimeIntervalDescrip AS StartTime
           ,(
              SELECT    TimeIntervalDescrip
              FROM      cmTimeInterval
              WHERE     TimeIntervalId = CSM.EndIntervalId
            ) AS EndTime
           ,CSA.MeetDate
           ,CSA.Actual
           ,CSA.Tardy
           ,CSA.Excused
           ,(
              SELECT    t3.UnitTypeDescrip
              FROM      arClassSections t1
                       ,arReqs t2
                       ,arAttUnitType t3
              WHERE     t1.ClsSectionId = CSA.ClsSectionId
                        AND t1.ReqId = t2.ReqId
                        AND t2.UnitTypeId = t3.UnitTypeId
            ) AS AttendanceType
           ,DATEDIFF(MI,TI.TimeIntervalDescrip,(
                                                 SELECT TimeIntervalDescrip
                                                 FROM   cmTimeInterval
                                                 WHERE  TimeIntervalId = CSM.EndIntervalId
                                               )) AS duration
           ,CSA.ClsSectMeetingId
           ,CSA.Scheduled
           ,CSM.InstructionTypeID
    FROM    atClsSectAttendance CSA
           ,plWorkDays WD
           ,arClsSectMeetings CSM
           ,cmTimeInterval TI
    WHERE   CSA.ClsSectionId = CSM.ClsSectionId
            AND WD.ViewOrder = DATEPART(dw,MeetDate) - 1
            AND CSM.TimeIntervalId = TI.timeIntervalId
            AND CSA.Actual <> 999
            AND StuEnrollId = @stuEnrollId
            AND CSA.MeetDate <= @cutOffDate
            AND (
                  @clsSectionId IS NULL
                  OR CSA.ClsSectionId = @clsSectionId
                )
    ORDER BY CSA.ClsSectionId
           ,CSA.MeetDate;   



GO
