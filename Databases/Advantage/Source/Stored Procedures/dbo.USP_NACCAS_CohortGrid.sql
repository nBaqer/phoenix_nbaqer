SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_NACCAS_CohortGrid]
    @PrgIdList VARCHAR(MAX) = NULL
   ,@CampusId VARCHAR(50)
   ,@ReportYear INT
   ,@ReportType VARCHAR(50)
AS
    DECLARE @StartMonth INT = 1
           ,@StartDay INT = 1
           ,@EndMonth INT = 12
           ,@EndDay INT = 31
           ,@FutureStart INT = 7
           ,@CurrentlyAttending INT = 9
           ,@LOA INT = 10
           ,@Suspension INT = 11
           ,@Dropped INT = 12
           ,@Graduated INT = 14
           ,@Transfer INT = 19
           ,@Preliminary VARCHAR(50) = 'Preliminary'
           ,@FallAnnual VARCHAR(50) = 'FallAnnual'
           ,@MilitaryTransfer UNIQUEIDENTIFIER
           ,@CallToActiveDuty UNIQUEIDENTIFIER
           ,@Deceased UNIQUEIDENTIFIER
           ,@TemporarilyDisabled UNIQUEIDENTIFIER
           ,@PermanentlyDisabled UNIQUEIDENTIFIER
           ,@TransferredToEquivalent UNIQUEIDENTIFIER;

    BEGIN

        SET @MilitaryTransfer = (
                                SELECT TOP 1 NACCASDropReasonId
                                FROM   dbo.syNACCASDropReasons
                                WHERE  Name = 'Military Transfer'
                                );
        SET @CallToActiveDuty = (
                                SELECT TOP 1 NACCASDropReasonId
                                FROM   dbo.syNACCASDropReasons
                                WHERE  Name = 'Call to Active Duty'
                                );
        SET @Deceased = (
                        SELECT TOP 1 NACCASDropReasonId
                        FROM   dbo.syNACCASDropReasons
                        WHERE  Name = 'Deceased'
                        );
        SET @TemporarilyDisabled = (
                                   SELECT TOP 1 NACCASDropReasonId
                                   FROM   dbo.syNACCASDropReasons
                                   WHERE  Name = 'Temporarily disabled'
                                   );
        SET @PermanentlyDisabled = (
                                   SELECT TOP 1 NACCASDropReasonId
                                   FROM   dbo.syNACCASDropReasons
                                   WHERE  Name = 'Permanently disabled'
                                   );
        SET @TransferredToEquivalent = (
                                       SELECT TOP 1 NACCASDropReasonId
                                       FROM   dbo.syNACCASDropReasons
                                       WHERE  Name = 'Transferred to an equivalent program at another school with same accreditation'
                                       );

        DECLARE @Enrollments TABLE
            (
                PrgVerID UNIQUEIDENTIFIER
               ,PrgVerDescrip VARCHAR(50)
               ,ProgramHours DECIMAL(9, 2)
               ,ProgramCredits DECIMAL(18, 2)
               ,TotalTransferHours DECIMAL(18, 2)
               ,TransferredProgram UNIQUEIDENTIFIER
               ,TransferHoursFromProgram DECIMAL(18, 2)
               ,SSN VARCHAR(50)
               ,PhoneNumber VARCHAR(50)
               ,Email VARCHAR(100)
               ,Student VARCHAR(50)
               ,LeadId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,EnrollmentID NVARCHAR(50)
               ,Status VARCHAR(80)
               ,SysStatusId INT
               ,DropReasonId UNIQUEIDENTIFIER
               ,StartDate DATETIME
               ,ContractedGradDate DATETIME
               ,ExpectedGradDate DATETIME
               ,LDA DATETIME
               ,LicensureWrittenAllParts BIT
               ,LicensureLastPartWrittenOn DATETIME
               ,LicensurePassedAllParts BIT
               ,AllowsMoneyOwed BIT
               ,AllowsIncompleteReq BIT
            );

        DECLARE @EnrollmentsWithLastStatus TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,SysStatusId INT
               ,DropReasonId UNIQUEIDENTIFIER
               ,StatusCodeDescription VARCHAR(80)
            );
        INSERT INTO @EnrollmentsWithLastStatus
                    SELECT lastStatusOfYear.StuEnrollId
                          ,lastStatusOfYear.SysStatusId
                          ,lastStatusOfYear.DropReasonId
                          ,lastStatusOfYear.StatusCodeDescrip
                    FROM   (
                           SELECT     StuEnrollId
                                     ,syStatusCodes.SysStatusId
                                     ,StatusCodeDescrip
                                     ,DateOfChange
                                     ,DropReasonId
                                     ,ROW_NUMBER() OVER ( PARTITION BY StuEnrollId
                                                          ORDER BY DateOfChange DESC
                                                        ) rn
                           FROM       dbo.syStudentStatusChanges
                           INNER JOIN dbo.syStatusCodes ON syStatusCodes.StatusCodeId = syStudentStatusChanges.NewStatusId
                           WHERE      DATEPART(YEAR, DateOfChange) <= ( @ReportYear - 1 )
                           ) lastStatusOfYear
                    WHERE  rn = 1;





        INSERT INTO @Enrollments
                    SELECT     Prog.ProgId AS PrgVerID
                              ,Prog.ProgDescrip AS PrgVerDescrip
                              ,PV.Hours AS ProgramHours
                              ,PV.Credits AS ProgramCredits
                              ,SE.TransferHours
                              ,SE.TransferHoursFromThisSchoolEnrollmentId
                              ,SE.TotalTransferHoursFromThisSchool
                              ,LD.SSN
                              ,ISNULL((
                                      SELECT   TOP 1 Phone
                                      FROM     dbo.adLeadPhone
                                      WHERE    LeadId = LD.LeadId
                                               AND (
                                                   IsBest = 1
                                                   OR IsShowOnLeadPage = 1
                                                   )
                                      ORDER BY Position
                                      )
                                     ,''
                                     ) AS PhoneNumber
                              ,ISNULL((
                                      SELECT   TOP 1 EMail
                                      FROM     dbo.AdLeadEmail
                                      WHERE    LeadId = LD.LeadId
                                               AND (
                                                   IsPreferred = 1
                                                   OR IsShowOnLeadPage = 1
                                                   )
                                      ORDER BY IsPreferred DESC
                                              ,IsShowOnLeadPage DESC
                                      )
                                     ,''
                                     ) AS Email
                              ,LD.LastName + ', ' + LD.FirstName + ' ' + ISNULL(LD.MiddleName, '') AS Student
                              ,LD.LeadId
                              ,SE.StuEnrollId
                              ,LD.StudentNumber AS EnrollmentID
                              ,[@EnrollmentsWithLastStatus].StatusCodeDescription AS Status
                              ,[@EnrollmentsWithLastStatus].SysStatusId AS SysStatusId
                              ,[@EnrollmentsWithLastStatus].DropReasonId AS DropReasonId
                              ,SE.StartDate AS StartDate
                              ,SE.ContractedGradDate AS ContractedGradDate
                              ,dbo.GetGraduatedDate(SE.StuEnrollId) AS ExpectedGradDate
                              ,dbo.GetLDA(SE.StuEnrollId) AS LDA
                              ,SE.LicensureWrittenAllParts AS LicensureWrittenAllParts
                              ,SE.LicensureLastPartWrittenOn AS LicensureLastPartWrittenOn
                              ,SE.LicensurePassedAllParts AS LicensurePassedAllParts
                              ,camp.AllowGraduateAndStillOweMoney AS AllowsMoneyOwed
                              ,camp.AllowGraduateWithoutFullCompletion AS AllowsIncompleteReq
                    FROM       dbo.arStuEnrollments AS SE
                    INNER JOIN dbo.adLeads AS LD ON LD.LeadId = SE.LeadId
                    INNER JOIN dbo.arPrgVersions AS PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN dbo.arPrograms AS Prog ON Prog.ProgId = PV.ProgId
                    INNER JOIN dbo.syApprovedNACCASProgramVersion NaccasApproved ON NaccasApproved.ProgramVersionId = SE.PrgVerId
                    INNER JOIN dbo.syNaccasSettings NaccasSettings ON NaccasSettings.NaccasSettingId = NaccasApproved.NaccasSettingId
                    INNER JOIN dbo.syCampuses AS camp ON camp.CampusId = SE.CampusId
                    INNER JOIN @EnrollmentsWithLastStatus ON [@EnrollmentsWithLastStatus].StuEnrollId = SE.StuEnrollId
                    WHERE      SE.CampusId = @CampusId
                               AND NaccasSettings.CampusId = @CampusId
                               --that have a contracted graduation date between January 1 of reporting year minus 1 year and December 31st of reporting year minus 1 year
                               -- Not a third party contract
                               AND NOT SE.ThirdPartyContract = 1
                               AND NaccasApproved.IsApproved = 1
                               AND SE.ContractedGradDate
                               BETWEEN CONVERT(DATETIME, CONVERT(VARCHAR(50), (( @ReportYear - 1 ) * 10000 + @StartMonth * 100 + @StartDay )), 112) AND CONVERT(
                                                                                                                                                                   DATETIME
                                                                                                                                                                  ,CONVERT(
                                                                                                                                                                              VARCHAR(50)
                                                                                                                                                                             ,(( @ReportYear
                                                                                                                                                                                 - 1
                                                                                                                                                                               )
                                                                                                                                                                               * 10000
                                                                                                                                                                               + @EndMonth
                                                                                                                                                                               * 100
                                                                                                                                                                               + @EndDay
                                                                                                                                                                              )
                                                                                                                                                                          )
                                                                                                                                                                  ,112
                                                                                                                                                               );
        --Base Case 
        WITH A
        AS ( SELECT se.PrgVerID
                   ,se.PrgVerDescrip
                   ,se.ProgramHours
                   ,se.ProgramCredits
                   ,se.SSN
                   ,se.PhoneNumber
                   ,se.Email
                   ,se.Student
                   ,se.LeadId
                   ,se.StuEnrollId
                   ,se.EnrollmentID
                   ,se.Status
                   ,se.SysStatusId
                   ,se.DropReasonId
                   ,se.StartDate
                   ,se.ContractedGradDate
                   ,se.ExpectedGradDate
                   ,se.LDA
                   ,se.LicensureWrittenAllParts
                   ,se.LicensureLastPartWrittenOn
                   ,se.LicensurePassedAllParts
                   ,se.AllowsMoneyOwed
                   ,se.AllowsIncompleteReq
             FROM   (
                    SELECT *
                          ,ROW_NUMBER() OVER ( PARTITION BY LeadId
                                                           ,PrgVerID
                                                           ,SysStatusId
                                               ORDER BY StartDate DESC
                                             ) RN
                    FROM   @Enrollments
                    ) se
             --Is currently attending the school or on LOA or Suspended or is enrolled but have not yet started
             WHERE  se.SysStatusId IN ( @FutureStart, @CurrentlyAttending, @Suspension, @LOA )
                    AND se.RN = 1
                    AND se.StuEnrollId NOT IN ((
                                               SELECT     se.TransferredProgram
                                               FROM       @Enrollments se
                                               INNER JOIN @Enrollments pe ON pe.StuEnrollId = se.TransferredProgram
                                               WHERE      (
                                                          (
                                                          se.TotalTransferHours IS NOT NULL
                                                          AND se.TotalTransferHours <> 0
                                                          )
                                                          AND (( se.TransferHoursFromProgram / pe.ProgramHours ) = 1 )
                                                          )
                                               UNION
                                               SELECT     pe.TransferredProgram
                                               FROM       @Enrollments se
                                               INNER JOIN @Enrollments pe ON pe.StuEnrollId = se.TransferredProgram
                                               WHERE      (
                                                          (
                                                          se.TotalTransferHours IS NOT NULL
                                                          AND se.TotalTransferHours <> 0
                                                          )
                                                          AND (( se.TransferHoursFromProgram / pe.ProgramHours ) <> 1 )
                                                          )
                                               )
                                              ))

            --Was enrolled in two different NACCAS approved programs in the same school and graduated/licensed/placed from both the programs in the same report period
            ,B
        AS ( SELECT se.PrgVerID
                   ,se.PrgVerDescrip
                   ,se.ProgramHours
                   ,se.ProgramCredits
                   ,se.SSN
                   ,se.PhoneNumber
                   ,se.Email
                   ,se.Student
                   ,se.LeadId
                   ,se.StuEnrollId
                   ,se.EnrollmentID
                   ,se.Status
                   ,se.SysStatusId
                   ,se.DropReasonId
                   ,se.StartDate
                   ,se.ContractedGradDate
                   ,se.ExpectedGradDate
                   ,se.LDA
                   ,se.LicensureWrittenAllParts
                   ,se.LicensureLastPartWrittenOn
                   ,se.LicensurePassedAllParts
                   ,se.AllowsMoneyOwed
                   ,se.AllowsIncompleteReq
             FROM   (
                    SELECT *
                          ,COUNT(1) OVER ( PARTITION BY LeadId
                                                       ,SysStatusId
                                         ) AS cnt
                    FROM   @Enrollments
                    ) se
             WHERE  se.SysStatusId IN ( @Graduated ))
            ,C
        AS (
           --Has dropped from a program whose length is less than one academic year of 900 hours after being enrolled for more than 15 calendar days
           --Has dropped from a program whose length is equal to or more than one academic year of 900 hours after being enrolled for more than 30 calendar DAYS
           SELECT se.PrgVerID
                 ,se.PrgVerDescrip
                 ,se.ProgramHours
                 ,se.ProgramCredits
                 ,se.SSN
                 ,se.PhoneNumber
                 ,se.Email
                 ,se.Student
                 ,se.LeadId
                 ,se.StuEnrollId
                 ,se.EnrollmentID
                 ,se.Status
                 ,se.SysStatusId
                 ,se.DropReasonId
                 ,se.StartDate
                 ,se.ContractedGradDate
                 ,se.ExpectedGradDate
                 ,se.LDA
                 ,se.LicensureWrittenAllParts
                 ,se.LicensureLastPartWrittenOn
                 ,se.LicensurePassedAllParts
                 ,se.AllowsMoneyOwed
                 ,se.AllowsIncompleteReq
           FROM   @Enrollments se
           WHERE  se.SysStatusId IN ( @Dropped )
                  AND (
                      (
                      se.ProgramHours < 900
                      AND DATEDIFF(DAY, se.StartDate, se.LDA) > 15
                      AND NOT EXISTS (
                                     SELECT     *
                                     FROM       dbo.syNACCASDropReasonsMapping nm
                                     INNER JOIN dbo.syNaccasSettings ns ON ns.NaccasSettingId = nm.NaccasSettingId
                                     WHERE      ns.CampusId = @CampusId
                                                AND se.DropReasonId = nm.ADVDropReasonId
                                                AND nm.NACCASDropReasonId IN ( @CallToActiveDuty, @MilitaryTransfer, @Deceased, @TemporarilyDisabled
                                                                              ,@PermanentlyDisabled, @TransferredToEquivalent
                                                                             )
                                     )
                      )
                      OR (
                         se.ProgramHours >= 900
                         AND DATEDIFF(DAY, se.StartDate, se.LDA) > 30
                         AND NOT EXISTS (
                                        SELECT     *
                                        FROM       dbo.syNACCASDropReasonsMapping nm
                                        INNER JOIN dbo.syNaccasSettings ns ON ns.NaccasSettingId = nm.NaccasSettingId
                                        WHERE      ns.CampusId = @CampusId
                                                   AND se.DropReasonId = nm.ADVDropReasonId
                                                   AND nm.NACCASDropReasonId IN ( @CallToActiveDuty, @MilitaryTransfer, @Deceased, @TemporarilyDisabled
                                                                                 ,@PermanentlyDisabled, @TransferredToEquivalent
                                                                                )
                                        )
                         )
                      )
                  AND se.StuEnrollId NOT IN ((
                                             SELECT     se.TransferredProgram
                                             FROM       @Enrollments se
                                             INNER JOIN @Enrollments pe ON pe.StuEnrollId = se.TransferredProgram
                                             WHERE      (
                                                        (
                                                        se.TotalTransferHours IS NOT NULL
                                                        AND se.TotalTransferHours <> 0
                                                        )
                                                        AND (( se.TransferHoursFromProgram / pe.ProgramHours ) = 1 )
                                                        )
                                             )
                                            ))
            --Was transferred from the school for which the report is generated to another school and both the schools are a branch of the same parent.
            -- Such a student would be included in the report generated for the original school as did not complete
            ,D
        AS ( SELECT     se.PrgVerID
                       ,se.PrgVerDescrip
                       ,se.ProgramHours
                       ,se.ProgramCredits
                       ,se.SSN
                       ,se.PhoneNumber
                       ,se.Email
                       ,se.Student
                       ,se.LeadId
                       ,se.StuEnrollId
                       ,se.EnrollmentID
                       ,se.Status
                       ,se.SysStatusId
                       ,se.DropReasonId
                       ,se.StartDate
                       ,se.ContractedGradDate
                       ,se.ExpectedGradDate
                       ,se.LDA
                       ,se.LicensureWrittenAllParts
                       ,se.LicensureLastPartWrittenOn
                       ,se.LicensurePassedAllParts
                       ,se.AllowsMoneyOwed
                       ,se.AllowsIncompleteReq
             FROM       @Enrollments se
             INNER JOIN dbo.arTrackTransfer tt ON se.StuEnrollId = tt.StuEnrollId
             INNER JOIN dbo.syCampuses srcCmp ON srcCmp.CampusId = tt.SourceCampusId
             INNER JOIN dbo.syCampuses targetCmp ON targetCmp.CampusId = tt.TargetCampusId
             INNER JOIN dbo.arPrgVersions targetPV ON targetPV.PrgVerId = tt.TargetPrgVerId
             WHERE      se.SysStatusId IN ( @Transfer )
                        AND ((
                             srcCmp.IsBranch = 1
                             AND targetCmp.IsBranch = 1
                             AND srcCmp.ParentCampusId <> targetCmp.ParentCampusId
                             )
                            ))
            ,E
        AS (
           -- select from PE id not all hours are transffered else select from SE
           SELECT     se.PrgVerID
                     ,se.PrgVerDescrip
                     ,se.ProgramHours
                     ,se.ProgramCredits
                     ,se.SSN
                     ,se.PhoneNumber
                     ,se.Email
                     ,se.Student
                     ,se.LeadId
                     ,se.StuEnrollId
                     ,se.EnrollmentID
                     ,se.Status
                     ,se.SysStatusId
                     ,se.DropReasonId
                     ,se.StartDate
                     ,se.ContractedGradDate
                     ,se.ExpectedGradDate
                     ,se.LDA
                     ,se.LicensureWrittenAllParts
                     ,se.LicensureLastPartWrittenOn
                     ,se.LicensurePassedAllParts
                     ,se.AllowsMoneyOwed
                     ,se.AllowsIncompleteReq
           FROM       @Enrollments se
           INNER JOIN @Enrollments pe ON pe.StuEnrollId = se.TransferredProgram
           WHERE      (
                      (
                      se.TotalTransferHours IS NOT NULL
                      AND se.TotalTransferHours <> 0
                      )
                      AND (( se.TransferHoursFromProgram / pe.ProgramHours ) = 1 )
                      )
           UNION
           SELECT     pe.PrgVerID
                     ,pe.PrgVerDescrip
                     ,pe.ProgramHours
                     ,pe.ProgramCredits
                     ,pe.SSN
                     ,pe.PhoneNumber
                     ,pe.Email
                     ,pe.Student
                     ,pe.LeadId
                     ,pe.StuEnrollId
                     ,pe.EnrollmentID
                     ,pe.Status
                     ,pe.SysStatusId
                     ,pe.DropReasonId
                     ,pe.StartDate
                     ,pe.ContractedGradDate
                     ,pe.ExpectedGradDate
                     ,pe.LDA
                     ,pe.LicensureWrittenAllParts
                     ,pe.LicensureLastPartWrittenOn
                     ,pe.LicensurePassedAllParts
                     ,pe.AllowsMoneyOwed
                     ,pe.AllowsIncompleteReq
           FROM       @Enrollments se
           INNER JOIN @Enrollments pe ON pe.StuEnrollId = se.TransferredProgram
           WHERE      (
                      (
                      se.TotalTransferHours IS NOT NULL
                      AND se.TotalTransferHours <> 0
                      )
                      AND (( se.TransferHoursFromProgram / pe.ProgramHours ) <> 1 )
                      ))
        SELECT dataWithNumOfJobs.PrgVerID,
              dataWithNumOfJobs.PrgVerDescrip,
              dataWithNumOfJobs.ProgramHours,
              dataWithNumOfJobs.ProgramCredits,
              dataWithNumOfJobs.SSN,
              dataWithNumOfJobs.PhoneNumber,
              dataWithNumOfJobs.Email,
              dataWithNumOfJobs.Student,
              dataWithNumOfJobs.LeadId,
              dataWithNumOfJobs.StuEnrollId,
              dataWithNumOfJobs.EnrollmentID,
              dataWithNumOfJobs.Status,
              dataWithNumOfJobs.SysStatusId,
              dataWithNumOfJobs.DropReasonId,
              dataWithNumOfJobs.StartDate,
              dataWithNumOfJobs.ContractedGradDate,
              dataWithNumOfJobs.ExpectedGradDate,
              dataWithNumOfJobs.LDA,
              dataWithNumOfJobs.LicensureWrittenAllParts,
              dataWithNumOfJobs.LicensureLastPartWrittenOn,
              dataWithNumOfJobs.LicensurePassedAllParts,
              dataWithNumOfJobs.AllowsMoneyOwed,
              dataWithNumOfJobs.AllowsIncompleteReq,
              dataWithNumOfJobs.Graduated,
              dataWithNumOfJobs.OwesMoney,
              dataWithNumOfJobs.MissingRequired,
              dataWithNumOfJobs.GraduatedProgram,
              dataWithNumOfJobs.DateGraduated,
              dataWithNumOfJobs.PlacementStatus,
              dataWithNumOfJobs.IneligibilityReason,
              dataWithNumOfJobs.Placed,
              dataWithNumOfJobs.SatForAllExamParts,
              dataWithNumOfJobs.PassedAllParts,
              dataWithNumOfJobs.EmployerName,
              dataWithNumOfJobs.EmployerAddress,
              dataWithNumOfJobs.EmployerCity,
              dataWithNumOfJobs.EmployerState,
              dataWithNumOfJobs.EmployerZip,
              dataWithNumOfJobs.EmployerPhone
              
        FROM     (
                 SELECT    allData.PrgVerID
                          ,allData.PrgVerDescrip
                          ,allData.ProgramHours
                          ,allData.ProgramCredits
                          ,allData.SSN
                          ,(
                           SELECT STUFF(STUFF(STUFF(allData.PhoneNumber, 1, 0, '('), 5, 0, ')'), 9, 0, '-')
                           ) AS PhoneNumber
                          ,allData.Email
                          ,allData.Student
                          ,allData.LeadId
                          ,allData.StuEnrollId
                          ,allData.EnrollmentID
                          ,allData.Status
                          ,allData.SysStatusId
                          ,allData.DropReasonId
                          ,allData.StartDate
                          ,allData.ContractedGradDate
                          ,allData.ExpectedGradDate
                          ,allData.LDA
                          ,allData.LicensureWrittenAllParts
                          ,allData.LicensureLastPartWrittenOn
                          ,allData.LicensurePassedAllParts
                          ,allData.AllowsMoneyOwed
                          ,allData.AllowsIncompleteReq
                          ,allData.Graduated
                          ,allData.OwesMoney
                          ,allData.MissingRequired
                          ,allData.GraduatedProgram
                          ,allData.DateGraduated
                          ,allData.PlacementStatus
                          ,allData.IneligibilityReason
                          ,allData.Placed
                          ,allData.SatForAllExamParts
                          ,allData.PassedAllParts
                          ,ISNULL(pemp.EmployerDescrip, '') AS EmployerName
                          ,ISNULL(pemp.Address1 + ' ' + pemp.Address2, '') AS EmployerAddress
                          ,ISNULL(pemp.City, '') AS EmployerCity
                          ,ISNULL(states.StateDescrip, '') AS EmployerState
                          ,ISNULL(pemp.Zip, '') AS EmployerZip
                          ,ISNULL(pemp.Phone, '') AS EmployerPhone
                          ,ROW_NUMBER() OVER ( PARTITION BY allData.StuEnrollId
                                               ORDER BY psp.PlacedDate DESC
                                             ) AS numOfJobs
                 FROM      (
                           SELECT *
                                 ,(
                                  SELECT CASE WHEN satForExam.SatForAllExamParts = 'Y' THEN CASE WHEN satForExam.LicensurePassedAllParts = 1 THEN 'Y'
                                                                                                 ELSE 'N'
                                                                                            END
                                              ELSE 'N/A'
                                         END
                                  ) PassedAllParts
                           FROM   (
                                  SELECT *
                                        ,(
                                         SELECT CASE WHEN placed.GraduatedProgram = 'Y' THEN
                                                         CASE WHEN placed.LicensureWrittenAllParts = 0 THEN 'N'
                                                              ELSE
                                                                  CASE WHEN @ReportType = @Preliminary THEN
                                                                           CASE WHEN placed.LicensureLastPartWrittenOn < CONVERT(
                                                                                                                                    DATETIME
                                                                                                                                   ,CONVERT(
                                                                                                                                               VARCHAR(50)
                                                                                                                                              ,(( @ReportYear ) * 10000
                                                                                                                                                + 3 * 100 + 30
                                                                                                                                               )
                                                                                                                                           )
                                                                                                                                   ,112
                                                                                                                                ) THEN 'Y'
                                                                                ELSE 'N'
                                                                           END
                                                                       ELSE
                                                                           CASE WHEN placed.LicensureLastPartWrittenOn < CONVERT(
                                                                                                                                    DATETIME
                                                                                                                                   ,CONVERT(
                                                                                                                                               VARCHAR(50)
                                                                                                                                              ,(( @ReportYear ) * 10000
                                                                                                                                                + 11 * 100 + 30
                                                                                                                                               )
                                                                                                                                           )
                                                                                                                                   ,112
                                                                                                                                ) THEN 'Y'
                                                                                ELSE 'N'
                                                                           END
                                                                  END
                                                         END
                                                     ELSE 'N/A'
                                                END
                                         ) AS SatForAllExamParts
                                  FROM   (
                                         SELECT *
                                               ,(
                                                SELECT CASE WHEN placementStat.PlacementStatus = 'E' THEN
                                                                CASE WHEN EXISTS (
                                                                                 SELECT     1
                                                                                 FROM       dbo.PlStudentsPlaced
                                                                                 INNER JOIN dbo.plFldStudy ON plFldStudy.FldStudyId = PlStudentsPlaced.FldStudyId
                                                                                 WHERE      dbo.PlStudentsPlaced.StuEnrollId = placementStat.StuEnrollId
                                                                                            AND (
                                                                                                dbo.plFldStudy.FldStudyCode = 'Yes'
                                                                                                OR dbo.plFldStudy.FldStudyCode = 'Related'
                                                                                                )
                                                                                 ) THEN 'Y'
                                                                     ELSE 'N'
                                                                END
                                                            ELSE 'N/A'
                                                       END
                                                ) AS Placed
                                         FROM   (
                                                SELECT *
                                                      ,(
                                                       SELECT CASE WHEN graduated.GraduatedProgram = 'Y' THEN CONVERT(VARCHAR, graduated.ExpectedGradDate, 101)
                                                                   ELSE 'N/A'
                                                              END
                                                       ) AS DateGraduated
                                                      ,(
                                                       SELECT CASE WHEN graduated.GraduatedProgram = 'Y' THEN
                                                                       CASE WHEN (
                                                                                 SELECT   TOP 1 Eligible
                                                                                 FROM     dbo.plExitInterview
                                                                                 WHERE    EnrollmentId = graduated.StuEnrollId
                                                                                 ORDER BY COALESCE(ExitInterviewDate, graduated.ExpectedGradDate) DESC
                                                                                 ) = 'Yes' THEN 'E'
                                                                            ELSE 'I'
                                                                       END
                                                                   ELSE 'N/A'
                                                              END
                                                       ) AS PlacementStatus
                                                      ,(
                                                       SELECT   TOP 1 COALESCE(Reason, '')
                                                       FROM     dbo.plExitInterview
                                                       WHERE    EnrollmentId = graduated.StuEnrollId
                                                       ORDER BY COALESCE(ExitInterviewDate, graduated.ExpectedGradDate) DESC
                                                       ) AS IneligibilityReason
                                                FROM   (
                                                       SELECT *
                                                             ,(
                                                              SELECT CASE WHEN didGraduate.Graduated = 0 THEN 'N'
                                                                          ELSE CASE WHEN didGraduate.OwesMoney = 1
                                                                                         AND didGraduate.AllowsMoneyOwed = 0 THEN 'N'
                                                                                    WHEN didGraduate.MissingRequired = 1
                                                                                         AND didGraduate.AllowsIncompleteReq = 0 THEN 'N'
                                                                                    ELSE 'Y'
                                                                               END
                                                                     END
                                                              ) AS GraduatedProgram
                                                       FROM   (
                                                              SELECT *
                                                                    ,(
                                                                     SELECT CASE WHEN @ReportType = @Preliminary THEN
                                                                                     CASE WHEN result.ExpectedGradDate < CONVERT(
                                                                                                                                    DATETIME
                                                                                                                                   ,CONVERT(
                                                                                                                                               VARCHAR(50)
                                                                                                                                              ,(( @ReportYear ) * 10000 + 3 * 100
                                                                                                                                                + 30
                                                                                                                                               )
                                                                                                                                           )
                                                                                                                                   ,112
                                                                                                                                ) THEN 1
                                                                                          ELSE 0
                                                                                     END
                                                                                 ELSE
                                                                                     CASE WHEN result.ExpectedGradDate < CONVERT(
                                                                                                                                    DATETIME
                                                                                                                                   ,CONVERT(
                                                                                                                                               VARCHAR(50)
                                                                                                                                              ,(( @ReportYear ) * 10000 + 11 * 100
                                                                                                                                                + 30
                                                                                                                                               )
                                                                                                                                           )
                                                                                                                                   ,112
                                                                                                                                ) THEN 1
                                                                                          ELSE 0
                                                                                     END
                                                                            END
                                                                     ) AS Graduated
                                                                    ,dbo.DoesEnrollmentHavePendingBalance(result.StuEnrollId) AS OwesMoney
                                                                    ,~ ( dbo.CompletedRequiredCourses(result.StuEnrollId)) AS MissingRequired
                                                              FROM   (
                                                                     SELECT *
                                                                     FROM   A
                                                                     UNION
                                                                     SELECT *
                                                                     FROM   B
                                                                     UNION
                                                                     SELECT *
                                                                     FROM   C
                                                                     UNION
                                                                     SELECT *
                                                                     FROM   D
                                                                     UNION
                                                                     SELECT *
                                                                     FROM   E
                                                                     ) AS result
                                                              WHERE  (
                                                                     @PrgIdList IS NULL
                                                                     OR result.PrgVerID IN (
                                                                                           SELECT Val
                                                                                           FROM   MultipleValuesForReportParameters(@PrgIdList, ',', 1)
                                                                                           )
                                                                     )
                                                              ) didGraduate
                                                       ) graduated
                                                ) placementStat
                                         ) placed
                                  ) satForExam
                           ) allData
                 LEFT JOIN dbo.PlStudentsPlaced psp ON psp.StuEnrollId = allData.StuEnrollId
                 LEFT JOIN dbo.plEmployerJobs pej ON pej.EmployerJobId = psp.EmployerJobId
                 LEFT JOIN dbo.plEmployers pemp ON pemp.EmployerId = pej.EmployerId
                 LEFT JOIN dbo.syStates states ON states.StateId = pemp.StateId
                 ) dataWithNumOfJobs
        WHERE    dataWithNumOfJobs.numOfJobs = 1
        ORDER BY dataWithNumOfJobs.PrgVerDescrip
                ,dataWithNumOfJobs.Student;



    END;


GO
