SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_CopyClasses_Insert] @RuleValues NTEXT
AS
    DECLARE @hDoc INT;    
        --Prepare input values as an XML document    
    EXEC sp_xml_preparedocument @hDoc OUTPUT,@RuleValues;    
    
        -- Insert records into Bridge Table     
       --SELECT ClsSectMeetingId FROM dbo.arClsSectMeetings
       
    INSERT  INTO arClsSectMeetings
            (
             ClsSectMeetingId
            ,WorkDaysId
            ,RoomId
            ,TimeIntervalId
            ,ClsSectionId
            ,EndIntervalId
            ,ModUser
            ,ModDate
            ,PeriodId
            ,AltPeriodId
            ,StartDate
            ,EndDate
            ,InstructionTypeID
            ,IsMeetingRescheduled
            )
            SELECT DISTINCT
                    NEWID()
                   ,NULL AS WorkDaysId
                   ,RoomId
                   ,NULL AS TimeIntervalId
                   ,ClsSectionId
                   ,NULL AS EndIntervalId
                   ,ModUser
                   ,GETDATE()
                   ,PeriodId
                   ,NULL AS AltPeriodId
                   ,StartDate
                   ,EndDate
                   ,InstructionTypeID
                   ,0
            FROM    OPENXML(@hDoc,'/NewDataSet/SetupMeetings',1)     
                    WITH (ClSectMeetingId UNIQUEIDENTIFIER,WorkDaysId UNIQUEIDENTIFIER,RoomId UNIQUEIDENTIFIER,    
                    TimeIntervalId UNIQUEIDENTIFIER,ClsSectionId UNIQUEIDENTIFIER,EndIntervalId UNIQUEIDENTIFIER,    
                    ModUser VARCHAR(50),ModDate DATETIME,    
                    PeriodId UNIQUEIDENTIFIER,AltPeriodId UNIQUEIDENTIFIER,    
                    StartDate DATETIME,EndDate DATETIME,InstructionTypeID UNIQUEIDENTIFIER, IsMeetingRescheduled BIT);   
    EXEC sp_xml_removedocument @hDoc;    



GO
