SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jose Torres
-- Create date: 11/20/2014
-- Description: Check If a Billing Method has already Paymen Period as IsCharged =1 True
--              if it is true the Billing Method should not deatach from the Program Version
--                            the Payment Period could not be update or deleted
-- EXECUTE USP_BillingMethodCheckForChargedPaymentPeriods
--     @BillingMethodId = N'F335D044-F05B-4F4F-8590-39552C6FDC3F'              
--                 
-- =============================================

CREATE PROCEDURE [dbo].[USP_BillingMethodCheckForChargedPaymentPeriods]
    @BillingMethodId NVARCHAR(50)
AS
    BEGIN
        DECLARE @IsBeingUsed AS BIT;
        SET @IsBeingUsed = 0;  -- FALSE 
        IF EXISTS ( SELECT  SPP.IsCharged
                    FROM    saPmtPeriods AS SPP
                    INNER JOIN dbo.saIncrements AS SI ON SPP.IncrementId = SI.IncrementId
                    INNER JOIN dbo.saBillingMethods AS SBM ON SI.BillingMethodId = SBM.BillingMethodId
                    WHERE   SBM.BillingMethodId = @BillingMethodId
                            AND SPP.IsCharged = 1 )
            BEGIN
                SET @IsBeingUsed = 1;
            END;
        ELSE
            BEGIN
                SET @IsBeingUsed = 0; 
            END;
        SELECT  @IsBeingUsed;
    END;




GO
