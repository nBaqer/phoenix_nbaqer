SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetStudentSummaryDetails]
@StudentEnrollmentId UNIQUEIDENTIFIER
as
SELECT   CASE WHEN dbo.GetAppSettingValueByKeyName('studentIdentifier', null) = 'StudentId' THEN '#' + L.StudentNumber  ELSE 'xxx-xx-' + RIGHT(l.SSN,4) END AS StudentIdentifier
         ,L.LastName + ', ' + L.FirstName  + (CASE WHEN l.MiddleName IS NULL THEN '' ELSE ' ' + l.MiddleName END )AS FullName
         ,LA.Address1
         ,LA.City
         ,LA.State
         ,LA.ZipCode
		 ,CASE WHEN sc.SysStatusId = 12 THEN sc.StatusCodeDescrip +  ' on ' + CONVERT(VARCHAR,e.DateDetermined,101)
		 ELSE sc.StatusCodeDescrip END AS StudentStatus
FROM      dbo.arStuEnrollments E
LEFT JOIN adLeads L ON L.LeadId = E.LeadId
LEFT JOIN dbo.adLeadAddresses LA ON LA.LeadId = L.LeadId
                                    AND LA.IsShowOnLeadPage = 1
									LEFT JOIN dbo.syStatusCodes SC ON SC.StatusCodeId = E.StatusCodeId
WHERE     E.StuEnrollId = @StudentEnrollmentId;








GO
