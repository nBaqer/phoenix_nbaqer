SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[usp_EdExpress_GetPendingPayments]
    @FileName VARCHAR(250)
   ,@CampusIds VARCHAR(1000)
   ,@EnrollmentStatustIds VARCHAR(1000)
   ,@FundSourceIds VARCHAR(1000)
   ,@LenderIds VARCHAR(1000)
   ,@ExpDateFrom DATETIME
   ,@ExpDateTo DATETIME
AS
    BEGIN

        DECLARE @SQL VARCHAR(MAX);

        SET @SQL = ' Select ST.ParentId, DT.DetailId,ST.StudentAwardID, DT.AwardScheduleId, ST.FirstName, ST.LastName,ST.SSN,ST.OriginalSSN, ST.CampusName,ST.StuEnrollmentId, ST.CampusId,';
        SET @SQL = @SQL
            + ' (select Max(LDA) from ( select max(AttendedDate)as LDA from arExternshipAttendance where StuEnrollId=ST.StuEnrollmentId union all select max(MeetDate) as LDA from atClsSectAttendance where StuEnrollId=ST.StuEnrollmentId and Actual >= 1 uni
on all select max(AttendanceDate) as LDA from atAttendance where EnrollId=ST.StuEnrollmentId and Actual >=1 union all select max(RecordDate) as LDA from arStudentClockAttendance where StuEnrollId=ST.StuEnrollmentId and (ActualHours >=1.00 and ActualHours 
<> 99.00 and ActualHours <> 999.00 and ActualHours <> 9999.00) union all select max(MeetDate) as LDA from atConversionAttendance where StuEnrollId=ST.StuEnrollmentId and (Actual >=1.00 and Actual <> 99.00 and Actual <> 999.00 and Actual <> 9999.00) )TR) a
s LDA, ';
        SET @SQL = @SQL
            + ' ST.AwardId, CASE(ST.DbIn) When ''T'' Then Case (ST.GrantType )when '''' then ''PELL'' When ''P'' then ''PELL'' When ''A'' then ''ACG'' when ''S'' then ''SMART'' when ''T'' then ''SMART'' else '''' End Else ''TEACH'' End AS GrantType, DT.Di
sbDate as ExpDisbDate ,DT.DisbNum,DT.DusbSeqNum,DT.SimittedDisbAmount,DT.AccDisbAmount,DT.AccDisbAmount as PaymentAmount, DT.DisbDate, ST.IsInSchool AS Pay, ST.IsInSchool, ST.AcademicYearId '; 
        SET @SQL = @SQL
            + ' From syEDExpNotPostPell_ACG_SMART_Teach_StudentTable ST, syEDExpNotPostPell_ACG_SMART_Teach_DisbursementTable DT, arStuEnrollments SE , faStudentAwards SA '; 
        SET @SQL = @SQL + ' Where ST.ParentId=DT.ParentId and SE.StuEnrollId=ST.StuEnrollmentId and SA.StudentAwardId=ST.StudentAwardId And ST.FileName='''
            + @FileName + '''';
        IF @CampusIds <> ''
            BEGIN
                SET @SQL = @SQL + @CampusIds;
            END;
        IF @EnrollmentStatustIds <> ''
            BEGIN
                SET @SQL = @SQL + @EnrollmentStatustIds;
            END;
        IF @FundSourceIds <> ''
            BEGIN
                SET @SQL = @SQL + @FundSourceIds;
            END;
        IF @LenderIds <> ''
            BEGIN
                SET @SQL = @SQL + @LenderIds;
            END;
        IF @ExpDateFrom IS NOT NULL
            AND @ExpDateTo IS NOT NULL
            BEGIN
                SET @SQL = @SQL + ' And DT.DisbDate>=''' + CONVERT(VARCHAR(10),@ExpDateFrom,101) + ''' and DT.DisbDate<=''' + CONVERT(VARCHAR(10),@ExpDateTo,101)
                    + '''';
            END;
        IF @ExpDateFrom IS NOT NULL
            AND @ExpDateTo IS NULL
            BEGIN
                SET @SQL = @SQL + ' And DT.DisbDate>=''' + CONVERT(VARCHAR(10),@ExpDateFrom,101) + '''';
            END;
        IF @ExpDateFrom IS NULL
            AND @ExpDateTo IS NOT NULL
            BEGIN
                SET @SQL = @SQL + ' And DT.DisbDate<=''' + CONVERT(VARCHAR(10),@ExpDateTo,101) + '''';
            END;
        SET @SQL = @SQL + ' Order by DisbNum';
        PRINT @SQL;
        EXEC (@SQL); 
    END;




GO
