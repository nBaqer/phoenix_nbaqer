SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_ConsecutiveDaysAbsentByClass_MainReport]
    @TermId VARCHAR(MAX) = NULL
   ,@ReqId VARCHAR(MAX) = NULL
   ,@ConsDays INT = 1
   ,@CutoffDate DATETIME
   ,@CampusId VARCHAR(MAX)
   ,@ShowCalendarDays BIT = 0
AS
    BEGIN
        SELECT   TermId
                ,TermDescrip
                ,ReqId
                ,Course
                ,ClsSectionId
                ,ClsSection
                ,[Term Start Date]
                ,COUNT(*) AS ID
        FROM     (
                 SELECT     st.StudentNumber
                           ,st.FirstName + ' ' + ISNULL(st.MiddleName, '') + ' ' + st.LastName AS Student
                           ,se.StartDate
                           ,se.LDA
                           ,SC.StatusCodeDescrip AS status
                           ,tm.TermId
                           ,tm.TermDescrip
                           ,tm.StartDate AS [Term Start Date]
                           ,rq.ReqId
                           ,rq.Descrip AS Course
                           ,cs.ClsSectionId
                           ,cs.ClsSection
                           ,(
                            SELECT dbo.MaxConsecutiveDaysAbsentForProgram(se.StuEnrollId, @CutoffDate, @ShowCalendarDays, @ConsDays, cs.ClsSectionId)
                            ) AS 'Days Missed'
                 FROM       adLeads AS st
                 INNER JOIN dbo.arStuEnrollments AS se ON st.StudentId = se.StudentId
                 INNER JOIN arPrgVersions AS pv ON se.PrgVerId = pv.PrgVerId
                 INNER JOIN dbo.arResults AS rs ON se.StuEnrollId = rs.StuEnrollId
                 INNER JOIN dbo.arClassSections AS cs ON rs.TestId = cs.ClsSectionId
                 INNER JOIN dbo.arTerm AS tm ON cs.TermId = tm.TermId
                 INNER JOIN arReqs AS rq ON cs.ReqId = rq.ReqId
                 INNER JOIN dbo.syStatusCodes AS SC ON se.StatusCodeId = SC.StatusCodeId
                 INNER JOIN dbo.sySysStatus AS SS ON SC.SysStatusId = SS.SysStatusId
                 WHERE      (
                            @TermId IS NULL
                            OR tm.TermId IN (
                                            SELECT Val
                                            FROM   MultipleValuesForReportParameters(@TermId, ',', 1)
                                            )
                            )
                            AND (
                                @ReqId IS NULL
                                OR rq.ReqId IN (
                                               SELECT Val
                                               FROM   MultipleValuesForReportParameters(@ReqId, ',', 1)
                                               )
                                )
                            AND SS.InSchool = 1
                            AND (
                                @CampusId IS NULL
                                OR ( se.CampusId IN (
                                                    SELECT Val
                                                    FROM   MultipleValuesForReportParameters(@CampusId, ',', 1)
                                                    )
                                   )
                                )
                 ) AS R
        WHERE    ( R.[Days Missed] >= @ConsDays )
        GROUP BY TermId
                ,TermDescrip
                ,ReqId
                ,Course
                ,ClsSectionId
                ,ClsSection
                ,[Term Start Date]
        ORDER BY [Term Start Date]
                ,R.Course;

    END;



GO
