SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_ApplicantLedger_ByLead_Void_GetList]
    @CampusId UNIQUEIDENTIFIER
   ,@TransStartDate DATE
   ,@TransEndDate DATE
   ,@LeadId UNIQUEIDENTIFIER
AS
    BEGIN
        SELECT  LT.TransactionId
               ,LT.LeadId
               ,(
                  SELECT    L.Firstname + ' ' + L.LastName
                ) AS LeadName
               ,LT.TransCodeId
               ,LT.IsEnrolled
               ,(
                  SELECT DISTINCT
                            TransCodeDescrip
                  FROM      saTransCodes
                  WHERE     TransCodeId = LT.TransCodeId
                ) AS TransactionCode
               ,LT.TransDescrip AS TransDescription
               ,CASE WHEN Voided = 1
                          AND TransTypeId = 0 THEN 'Charge'
                     ELSE 'Payment'
                END AS TransactionType
               ,LT.ModUser AS Postedby
               ,LT.TransAmount AS TransactionAmount
               ,CONVERT(CHAR(10),LT.CreatedDate,101) AS TransDate
               ,TransReference
               ,(
                  SELECT TOP 1
                            CheckNumber
                  FROM      adLeadPayments
                  WHERE     TransactionId = LT.TransactionId
                ) AS DocumentId
               ,LT.Voided AS Voided
               ,LT.ReversalReason
        FROM    adLeadTransactions LT
        INNER JOIN adLeads L ON LT.LeadId = L.LeadId
        WHERE   L.CampusId = @CampusId
                AND L.LeadId = @LeadId
                AND (
                      CONVERT(CHAR(10),LT.CreatedDate,101) >= @TransStartDate
                      AND CONVERT(CHAR(10),LT.CreatedDate,101) <= @TransEndDate
                    )
                AND LT.TransTypeId = 2
                AND LT.Voided = 1
        ORDER BY LT.DisplaySequence
               ,LT.SecondDisplaySequence
               ,TransTypeId;
		
    END;



GO
