SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetStudentTutionEarningMethod]
    (
     @stuEnrollId UNIQUEIDENTIFIER
    )
AS
    SET NOCOUNT ON;
    SELECT  COUNT(*) AS Cnt
    FROM    arStuEnrollments a
           ,arPrgVersions b
           ,saTuitionEarnings c
    WHERE   a.PrgVerId = b.PrgVerId
            AND b.TuitionEarningId = c.TuitionEarningId
            AND c.PercentToEarnIdx = 1
            AND a.StuEnrollid = @stuEnrollId;



GO
