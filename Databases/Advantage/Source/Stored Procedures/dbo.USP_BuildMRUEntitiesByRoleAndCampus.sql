SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-----------------------------------------------------------------------------------------------------------------------
--DE8086 QA: Selecting an employee from search page gives an error page and from MRU gives a java script error
--Balaji Date 7/27/2012  End
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
--DE8134 QA: Issue found within Manage Security (Student Accounts Module > Student Pages)
--Balaji Date 7/30/2012  Start
-----------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[USP_BuildMRUEntitiesByRoleAndCampus] @UserId VARCHAR(50)
AS
    DECLARE @RolesVar TABLE ( RoleId VARCHAR(8000) );
    INSERT  INTO @RolesVar
            (
             RoleId
            )
            SELECT DISTINCT
                    RoleId
            FROM    syUsersRolesCampGrps
            WHERE   UserId = @UserId;

    SELECT  ResourceId
           ,RESOURCE
           ,Entity
           ,MRUType
    FROM    (
              SELECT DISTINCT
                        ResourceID
                       ,Resource
                       ,CASE WHEN ModuleId = 189 THEN 394  -- AD:Lead Entity
                             ELSE CASE WHEN ModuleId IN ( 26,191,193,194,300 ) THEN 395 -- AR,PL,FinAid,Faculty, StudentAccounts Module:Student Entity
                                       ELSE CASE WHEN ModuleId IN ( 192 ) THEN 396 -- HR:Employees
                                                 ELSE NULL
                                            END
                                  END
                        END AS Entity
                       ,CASE WHEN ModuleId = 189 THEN 4  -- AD:Lead Entity
                             ELSE CASE WHEN ModuleId IN ( 26,191,193,194,300 ) THEN 1 -- AR,PL,FinAid,Faculty, StudentAccounts Module:Student Entity
                                       ELSE CASE WHEN ModuleId IN ( 192 ) THEN 3 -- HR:Employees
                                                 ELSE NULL
                                            END
                                  END
                        END AS MRUType
              FROM      syRolesModules RM
                       ,syResources R
              WHERE     RM.ModuleId = R.ResourceId
                        AND RM.RoleId IN ( SELECT DISTINCT
                                                    RoleId
                                           FROM     @RolesVar )
              UNION
 --Placement Module has two entities - Student and Employer
              SELECT DISTINCT
                        ResourceID
                       ,Resource
                       ,CASE WHEN ModuleId = 193 THEN 397 --PL:Employers
                             ELSE CASE WHEN ModuleId = 191 THEN 434 -- FinAid:Lenders
                                       ELSE NULL
                                  END
                        END AS Entity
                       ,CASE WHEN ModuleId = 193 THEN 2 --PL:Employers
                             ELSE CASE WHEN ModuleId = 191 THEN 5 -- FinAid:Lenders
                                       ELSE NULL
                                  END
                        END AS MRUType
              FROM      syRolesModules RM
                       ,syResources R
              WHERE     RM.ModuleId = R.ResourceId 
		   --AND    RM.RoleId=@RoleId --'436FFB3E-A55F-49BB-908D-70B104BC952D' 
                        AND RM.RoleId IN ( SELECT DISTINCT
                                                    RoleId
                                           FROM     @RolesVar )
            ) t1
    WHERE   Entity IS NOT NULL
            AND MRUType NOT IN ( 5 )
    ORDER BY MRUType;  



GO
