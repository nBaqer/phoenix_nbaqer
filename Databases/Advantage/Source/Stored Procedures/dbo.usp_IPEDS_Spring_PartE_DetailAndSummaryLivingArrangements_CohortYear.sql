SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[usp_IPEDS_Spring_PartE_DetailAndSummaryLivingArrangements_CohortYear]
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@CohortYear VARCHAR(10) = NULL
   ,@OrderBy VARCHAR(100)
   ,@StartDate DATETIME
   ,@EndDate DATETIME
   ,@SchoolType VARCHAR(50)
   ,@InstitutionType VARCHAR(50)
   ,@EndDate_Academic DATETIME = NULL
   ,@LargestProgramID VARCHAR(50) = NULL
AS
    DECLARE @ReturnValue VARCHAR(100)
       ,@Value VARCHAR(100);
    DECLARE @SSN VARCHAR(10)
       ,@FirstName VARCHAR(100)
       ,@LastName VARCHAR(100)
       ,@StudentNumber VARCHAR(50)
       ,@TransferredOut INT
       ,@Exclusions INT;
    DECLARE @CitizenShip_IPEDSValue INT
       ,@ProgramType_IPEDSValue INT
       ,@Gender_IPEDSValue INT
       ,@FullTimePartTime_IPEDSValue INT
       ,@StudentId UNIQUEIDENTIFIER; 
    DECLARE @AcademicEndDate DATETIME;
    --12/05/2012 DE8824 - Academic year options should have a cutoff start date
    DECLARE @AcademicStartDate DATETIME; 
	
-- Check if School tracks grades by letter or numeric 
    SET @Value = (
                   SELECT TOP 1
                            Value
                   FROM     dbo.syConfigAppSetValues
                   WHERE    SettingId = 47
                 );


--Here, we're determining if the school type is academic. If so, we will change the new var
--@AcademicEndDate to the @EndDate_Academic, if not, than use @EndDate. We will use the new
--@AcademicEndDate variable to help select the student list.  DD Rev 01/10/12
    IF LOWER(@SchoolType) = 'academic'
        BEGIN
            SET @AcademicEndDate = @EndDate_Academic;
            --12/05/2012 DE8824 - Academic year options should have a cutoff start date
            SET @AcademicStartDate = DATEADD(YEAR,-1,@EndDate_Academic); 
        END;
    ELSE
        BEGIN
            SET @AcademicEndDate = @EndDate;
        END;


    IF @ProgId IS NOT NULL
        BEGIN
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.ProgId IN ( SELECT   Val
                                   FROM     MultipleValuesForReportParameters(@ProgId,',',1) );
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);
        END;
    ELSE
        BEGIN
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND t1.CampGrpId IN ( SELECT    CampGrpId
                                          FROM      syCmpGrpCmps
                                          WHERE     CampusId = @CampusId );
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);
        END;

/*********Get the list of students that will be shown in the report - Starts Here ******************/			
    CREATE TABLE #StudentsList
        (
         StudentId UNIQUEIDENTIFIER
        ,SSN VARCHAR(10)
        ,FirstName VARCHAR(100)
        ,LastName VARCHAR(100)
        ,MiddleName VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,TransferredOut INT
        ,Exclusions INT
        ,CitizenShip_IPEDSValue INT
        ,ProgramType_IPEDSValue INT
        ,Gender_IPEDSValue INT
        ,FullTimePartTime_IPEDSValue INT
        ,GenderId UNIQUEIDENTIFIER
        ,RaceId UNIQUEIDENTIFIER
        ,CitizenId UNIQUEIDENTIFIER
        ,GenderDescription VARCHAR(50)
        ,RaceDescription VARCHAR(50)
        ,StudentGraduatedStatusCount INT
        ,ClockHourProgramCount INT
        ,StartDate DATETIME
        ,HoustingType UNIQUEIDENTIFIER
        );
		
-- The following table will store only Full Time First Time UnderGraduate Students
    CREATE TABLE #UnderGraduateStudentsWhoReceivedFinancialAid
        (
         StudentId UNIQUEIDENTIFIER
        ,FinancialAidType INT
        ,TitleIV BIT
        );


-- Get the list of UnderGraduate Students
    IF @SchoolType <> 'program'
        AND @InstitutionType = '1'
        BEGIN
            INSERT  INTO #StudentsList
                    SELECT DISTINCT
                            t1.StudentId
                           ,t1.SSN
                           ,t1.FirstName
                           ,t1.LastName
                           ,t1.MiddleName
                           ,t1.StudentNumber
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SQ1
                                       ,syStatusCodes SQ2
                                       ,dbo.sySysStatus SQ3
                              WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                        AND SQ2.SysStatusId = SQ3.SysStatusId
                                        AND SQ1.StudentId = t1.StudentId
                                        AND SQ3.SysStatusId = 19
                                        AND SQ1.TransferDate <= @AcademicEndDate
                            ) AS TransferredOut
                           ,(
					-- Check if the student was either dropped and if the drop reason is either
					-- deceased, active duty, foreign aid service, church mission
                              CASE WHEN (
                                          SELECT    COUNT(*)
                                          FROM      arStuEnrollments SQ1
                                                   ,syStatusCodes SQ2
                                                   ,dbo.sySysStatus SQ3
                                                   ,arDropReasons SQ44
                                          WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                                    AND SQ2.SysStatusId = SQ3.SysStatusId
                                                    AND SQ1.DropReasonId = SQ44.DropReasonId
                                                    AND SQ1.StudentId = t1.StudentId
                                                    AND SQ3.SysStatusId IN ( 12 ) -- Dropped
                                                    AND SQ1.DateDetermined <= @AcademicEndDate
                                                    AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                        ) >= 1 THEN 1
                                   ELSE 0
                              END ) AS Exclusions
                           ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                           ,t9.IPEDSValue AS ProgramType_IPEDSValue
                           ,t3.IPEDSValue AS Gender_IPEDSValue
                           ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                           ,t1.Gender
                           ,t1.Race
                           ,t1.Citizen
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t3.IPEDSValue
                            ) AS GenderDescription
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t4.IPEDSValue
                            ) AS RaceDescription
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,syStatusCodes SC
                              WHERE     SEC.StudentId = t1.StudentId
                                        AND SEC.StatusCodeId = SC.StatusCodeId
                                        AND SC.SysStatusId = 14
                            ) AS StudentGraduatedStatusCount
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,arPrograms P
                                       ,arPrgVersions PV
                              WHERE     SEC.PrgVerId = PV.PrgVerId
                                        AND P.ProgId = PV.ProgId
                                        AND SEC.StudentId = t1.StudentId
                                        AND P.ACId = 5
                            ) AS ClockHourProgramCount
                           ,t2.StartDate
                           ,t1.HousingId
                    FROM    adGenders t3
                    LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                    LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                    INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                    INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                 AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students
                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                    LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.DegCertSeekingId = t12.DegCertSeekingId
                    INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                    INNER JOIN dbo.saTransactions SA ON SA.StuEnrollId = t2.StuEnrollId
                    INNER JOIN dbo.saFundSources FS ON SA.FundSourceId = FS.FundSourceId
                            --DE8813 - Item 2
                            --INNER JOIN dbo.saTuitionCategories TC ON t2.TuitionCategoryId = TC.TuitionCategoryId
                    WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t10.IPEDSValue = 61
                            AND -- Full Time
                            t12.IPEDSValue = 11
                            AND -- First Time
                            t9.IPEDSValue = 58
                            AND SA.Voided = 0
                            AND FS.AdvFundSourceId NOT IN ( 1,19,23 )
                            AND -- Under Graduate
                       -- TC.IPEDSValue IN (145,146) AND -- Who paid In-State or In-District Tuition
							--12/05/2012 DE8824 - Academic year options should have a cutoff start date
                            --( t2.StartDate <= @AcademicEndDate )
                            (
                              t2.StartDate > @AcademicStartDate
                              AND t2.StartDate <= @AcademicEndDate
                            )
                            AND t2.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicEndDate
                                          OR ExpGradDate < @AcademicEndDate
                                          OR LDA < @AcademicEndDate
                                        ) ) 
                                                                      
                       -- If Student is enrolled in only one program version and if that program version 
                       -- happens to be a continuing ed program exclude the student
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd )
       --                 AND 
							--(
							--	(SA.AwardStartDate>=@StartDate AND SA.AwardstartDate<=@EndDate) 
							--	OR 
							--	(SA.AwardEndDate>=@StartDate AND SA.AwardEndDate<=@EndDate)
							--)
							-- FFEL PLUS (9)(added for DE8816)
                            AND ISNULL(FS.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- Exclude FWS and Plus Loan
                            AND FS.TitleIV = 1;
        END;
    IF @SchoolType = 'program'
        BEGIN
            INSERT  INTO #StudentsList
                    SELECT DISTINCT
                            t1.StudentId
                           ,t1.SSN
                           ,t1.FirstName
                           ,t1.LastName
                           ,t1.MiddleName
                           ,t1.StudentNumber
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SQ1
                                       ,syStatusCodes SQ2
                                       ,dbo.sySysStatus SQ3
                              WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                        AND SQ2.SysStatusId = SQ3.SysStatusId
                                        AND SQ1.StudentId = t1.StudentId
                                        AND SQ3.SysStatusId = 19
                                        AND SQ1.TransferDate <= @EndDate
                            ) AS TransferredOut
                           ,(
					-- Check if the student was either dropped and if the drop reason is either
					-- deceased, active duty, foreign aid service, church mission
                              CASE WHEN (
                                          SELECT    COUNT(*)
                                          FROM      arStuEnrollments SQ1
                                                   ,syStatusCodes SQ2
                                                   ,dbo.sySysStatus SQ3
                                                   ,arDropReasons SQ44
                                          WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                                    AND SQ2.SysStatusId = SQ3.SysStatusId
                                                    AND SQ1.DropReasonId = SQ44.DropReasonId
                                                    AND SQ1.StudentId = t1.StudentId
                                                    AND SQ3.SysStatusId IN ( 12 ) -- Dropped
                                                    AND SQ1.DateDetermined <= @EndDate
                                                    AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                        ) >= 1 THEN 1
                                   ELSE 0
                              END ) AS Exclusions
                           ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                           ,t9.IPEDSValue AS ProgramType_IPEDSValue
                           ,t3.IPEDSValue AS Gender_IPEDSValue
                           ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                           ,t1.Gender
                           ,t1.Race
                           ,t1.Citizen
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t3.IPEDSValue
                            ) AS GenderDescription
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t4.IPEDSValue
                            ) AS RaceDescription
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,syStatusCodes SC
                              WHERE     SEC.StudentId = t1.StudentId
                                        AND SEC.StatusCodeId = SC.StatusCodeId
                                        AND SC.SysStatusId = 14
                            ) AS StudentGraduatedStatusCount
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,arPrograms P
                                       ,arPrgVersions PV
                              WHERE     SEC.PrgVerId = PV.PrgVerId
                                        AND P.ProgId = PV.ProgId
                                        AND SEC.StudentId = t1.StudentId
                                        AND P.ACId = 5
                            ) AS ClockHourProgramCount
                           ,t2.StartDate
                           ,t1.HousingId
                    FROM    adGenders t3
                    LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                    LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                    INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                    INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                 AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students
                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                    LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.DegCertSeekingId = t12.DegCertSeekingId
                    INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                    INNER JOIN dbo.saTransactions SA ON SA.StuEnrollId = t2.StuEnrollId
                    INNER JOIN dbo.saFundSources FS ON SA.FundSourceId = FS.FundSourceId
                    WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND
                        --(@ProgId is null or t8.ProgId in (Select Val from [MultipleValuesForReportParameters](@ProgId,',',1))) AND
                            t8.ProgId = @LargestProgramID
                            AND  --DD 1/21/12
                            t10.IPEDSValue = 61
                            AND -- Full Time
                            t12.IPEDSValue = 11
                            AND -- First Time
                            t9.IPEDSValue = 58
                            AND SA.Voided = 0
                            AND FS.AdvFundSourceId NOT IN ( 1,19,23 )
                            AND -- Under Graduate
                            (
                              t2.StartDate >= @StartDate
                              AND t2.StartDate <= @EndDate
                            )
                       -- (t2.StartDate<=@EndDate )
                            AND t2.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDate
                                          OR ExpGradDate < @StartDate
                                          OR LDA < @StartDate
                                        ) ) 
                       -- If Student is enrolled in only one program version and if that program version 
                       -- happens to be a continuing ed program exclude the student
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd )	
       --                    AND 
							--(
							--	(SA.AwardStartDate>=@StartDate AND SA.AwardstartDate<=@EndDate) 
							--	OR 
							--	(SA.AwardEndDate>=@StartDate AND SA.AwardEndDate<=@EndDate)
							--)
							-- FFEL PLUS (9)(added for DE8816)
                            AND ISNULL(FS.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- Exclude FWS and Plus Loan
                            AND FS.TitleIV = 1;
        END;


/*********Get the list of students that will be shown in the report - Ends Here *****************8*/

-- Get all Full Time, First Time UnderGraduate Students
-- Use #StudentsList as it pulls UnderGraduate Students
    IF @SchoolType <> 'program'
        AND @InstitutionType = '1'
        BEGIN
            INSERT  INTO #UnderGraduateStudentsWhoReceivedFinancialAid
                    SELECT  t1.StudentId
                           ,FinancialAidType
                           ,TitleIV
                    FROM    #StudentsList t1
                    INNER JOIN (
                                 SELECT SE.StudentId
                                       ,FS.IPEDSValue AS FinancialAidType
                                       ,FS.TitleIV AS TitleIV
                                 FROM   dbo.saTransactions SA
                                 INNER JOIN dbo.arStuEnrollments SE ON SA.StuEnrollId = SE.StuEnrollId
                                 INNER JOIN saFundSources FS ON SA.FundSourceId = FS.FundSourceId
                                                 --DE8813 - Item 2
                                                 --INNER JOIN dbo.saTuitionCategories TC ON SE.TuitionCategoryId = TC.TuitionCategoryId
                                 WHERE  SE.StudentId IN ( SELECT DISTINCT
                                                                    StudentId
                                                          FROM      #StudentsList )  -- UnderGraduates
                                        AND TitleIV = 1
                                        AND SA.Voided = 0
                                        AND FS.AdvFundSourceId NOT IN ( 1,19,23 )
					--AND TC.IPEDSValue IN (145,146) --Instate or In-district tuition
                                        AND ISNULL(FS.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- FFEL PLUS (9)(added for DE8816)
                                        AND SE.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicEndDate
                                          OR ExpGradDate < @AcademicEndDate
                                          OR LDA < @AcademicEndDate
                                        ) )
                                        AND ( SA.TransDate BETWEEN @StartDate AND @EndDate )
                               ) getStudentsThatReceivedFinancialAid ON t1.StudentId = getStudentsThatReceivedFinancialAid.StudentId; 

        END;
    IF @SchoolType = 'program' --and @InstitutionType='1'
        BEGIN
            INSERT  INTO #UnderGraduateStudentsWhoReceivedFinancialAid
                    SELECT  t1.StudentId
                           ,FinancialAidType
                           ,TitleIV
                    FROM    #StudentsList t1
                    INNER JOIN (
                                 SELECT SE.StudentId
                                       ,FS.IPEDSValue AS FinancialAidType
                                       ,FS.TitleIV AS TitleIV
                                 FROM   dbo.saTransactions SA
                                 INNER JOIN dbo.arStuEnrollments SE ON SA.StuEnrollId = SE.StuEnrollId
                                 INNER JOIN saFundSources FS ON SA.FundSourceId = FS.FundSourceId
                                 WHERE  SE.StudentId IN ( SELECT DISTINCT
                                                                    StudentId
                                                          FROM      #StudentsList )  -- UnderGraduates
                                        AND TitleIV = 1
                                        AND SA.Voided = 0
                                        AND FS.AdvFundSourceId NOT IN ( 1,19,23 )
                                        AND ISNULL(FS.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- FFEL PLUS (9)(added for DE8816)
                                        AND SE.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDate
                                          OR ExpGradDate < @StartDate
                                          OR LDA < @StartDate
                                        ) )
                                        AND ( SA.TransDate BETWEEN @StartDate AND @EndDate )
                               ) getStudentsThatReceivedFinancialAid ON t1.StudentId = getStudentsThatReceivedFinancialAid.StudentId; 

        END;

    CREATE TABLE #CohortYear
        (
         RowNumber_CohortYr UNIQUEIDENTIFIER
        ,SSN_CohortYr VARCHAR(11)
        ,StudentNumber_CohortYr VARCHAR(50)
        ,StudentName_CohortYr VARCHAR(200)
        ,OnCampusCountX_CohortYr VARCHAR(1)
        ,OnCampusCount_CohortYr INT
        ,OffCampusCountX_CohortYr VARCHAR(1)
        ,OffCampusCount_CohortYr INT
        ,WithParentCountX_CohortYr VARCHAR(1)
        ,WithParentCount_CohortYr INT
        );

    INSERT  INTO #CohortYear
            SELECT  RowNumber AS RowNumber_CohortYr
                   ,SSN AS SSN_CohortYr
                   ,StudentNumber AS StudentNumber_CohortYr
                   ,StudentName AS StudentName_CohortYr
                   ,CASE WHEN OnCampusCount >= 1 THEN 'X'
                         ELSE ''
                    END AS OnCampusCountX_CohortYr
                   ,OnCampusCount AS OnCampusCount_CohortYr
                   ,CASE WHEN OffCampusCount >= 1 THEN 'X'
                         ELSE ''
                    END AS OffCampusCountX_CohortYr
                   ,OffCampusCount AS OffCampusCount_CohortYr
                   ,CASE WHEN WithParentCount >= 1 THEN 'X'
                         ELSE ''
                    END AS WithParentCountX_CohortYr
                   ,WithParentCount AS WithParentCount_CohortYr
            FROM    (
                      SELECT    NEWID() AS RowNumber
                               ,dbo.UDF_FormatSSN(SSN) AS SSN
                               ,StudentNumber
                               ,LastName + ', ' + FirstName + ' ' + ISNULL(MiddleName,'') AS StudentName
                               ,
		
		
		-- From FULL TIME First Time UnderGraduates Student List, Select Students who received those who received any Federal Work Study, loans to students, 
		-- or grant or scholarship aid from the federal government, state/local government, the institution, or other sources known to the institution. 
		--FinancialAidType (66) - Federal grants(grants/educational assistance funds)
		--FinancialAidType (67) - State/local government grants(grants/scholarships/waivers)
		--FinancialAidType (68) - Institutional grants
		--FinancialAidType (69) - Private grants or scholarships
		--FinancialAidType (70) - Loans to students
		--FinancialAidType (71) - Other sources
		-- Values are coming from saFundSources - IPEDSValue Field
                                (
                                  SELECT    COUNT(DISTINCT t2.StudentId)
                                  FROM      arStuEnrollments t2
                                  INNER JOIN #UnderGraduateStudentsWhoReceivedFinancialAid t3 ON t2.StudentId = t3.StudentId
                                                                                                 AND t2.StudentId = SL.StudentId
                                  INNER JOIN arHousing H ON SL.HoustingType = H.HousingId
                                                            AND H.IPEDSValue = 35
                                  WHERE     TitleIV = 1
                                ) AS OnCampusCount
                               ,(
                                  SELECT    COUNT(DISTINCT t2.StudentId)
                                  FROM      arStuEnrollments t2
                                  INNER JOIN #UnderGraduateStudentsWhoReceivedFinancialAid t3 ON t2.StudentId = t3.StudentId
                                                                                                 AND t2.StudentId = SL.StudentId
                                  INNER JOIN arHousing H ON SL.HoustingType = H.HousingId
                                                            AND H.IPEDSValue = 36
                                  WHERE     TitleIV = 1
                                ) AS OffCampusCount
                               ,(
                                  SELECT    COUNT(DISTINCT t2.StudentId)
                                  FROM      arStuEnrollments t2
                                  INNER JOIN #UnderGraduateStudentsWhoReceivedFinancialAid t3 ON t2.StudentId = t3.StudentId
                                                                                                 AND t2.StudentId = SL.StudentId
                                  INNER JOIN arHousing H ON SL.HoustingType = H.HousingId
                                                            AND H.IPEDSValue = 37
                                  WHERE     TitleIV = 1
                                ) AS WithParentCount
                      FROM      #StudentsList SL
                    ) dt
            ORDER BY CASE WHEN @OrderBy = 'SSN' THEN dt.SSN
                     END
                   ,CASE WHEN @OrderBy = 'LastName' THEN dt.StudentName
                    END
                   ,
		-- Updated 11/27/2012 - sort by student number not working
		--Case When @OrderBy='StudentNumber' Then Convert(int,dt.StudentNumber)
                    CASE WHEN @OrderBy = 'Student Number' THEN dt.StudentNumber
                    END;
    DROP TABLE #UnderGraduateStudentsWhoReceivedFinancialAid;
    DROP TABLE #StudentsList;



    SELECT  *
    FROM    #CohortYear;

--SELECT * FROM #CohortYear
--SELECT * FROM #CohortYearMinus1
--SELECT * FROM #CohortYearMinus2

    DROP TABLE #CohortYear;



GO
