SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-----------------------------------------------------------------------------------------------------------------------
--DE8090 QA: Trying to select the Enroll lead links is giving an error page against a certain school.
--Bruce S Date 7/27/2012  End
-----------------------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------------------
--DE8086 QA: Selecting an employee from search page gives an error page and from MRU gives a java script error
--Balaji Date 7/27/2012  Start
-----------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[USP_GetEmployeeDetailsForStatusBar]
    @EmployeeId UNIQUEIDENTIFIER
AS
    SELECT  S.FirstName + ' ' + ISNULL(S.MI,'') + ' ' + S.LastName AS LeadName
           ,S.EmpId AS EmpId
           ,(
              SELECT    C.CampDescrip
              FROM      dbo.syCampuses C
              WHERE     C.CampusId = S.CampusId
            ) AS CampusDescription
           ,SSN AS SSN
           ,Address1
           ,Address2
           ,City
           ,(
              SELECT    StateDescrip
              FROM      syStates
              WHERE     StateId = S.StateId
            ) AS StateDescrip
           ,S.Zip
           ,S.CampusId
    FROM    hrEmployees S
    WHERE   S.EmpId = @EmployeeId;



GO
