SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DEVELOPMENT_InsertUpdateFieldIn_syRptAdHocFields]
    @newTableName VARCHAR(50)
   ,@fldName VARCHAR(50)
   ,@oldTableName VARCHAR(50)
   --,@headerCaption VARCHAR(50)
   ,@newFldName VARCHAR(50)
AS
    BEGIN
        BEGIN TRAN;
        SET NOCOUNT ON;
        DECLARE @TblFldId INT
           ,@TblId INT
           ,@fldId INT
           ,@CategoryId INT
           ,@FKColDescrip VARCHAR(50)
           ,@oldTblFldId INT;
        SET @TblId = (
                       SELECT   TblId
                       FROM     syTables
                       WHERE    TblName = @newTableName
                     );
        SET @fldId = (
                       SELECT TOP 1
                                FldId
                       FROM     syFields
                       WHERE    FldName = @fldName
                     );
--get the categoryId for Adhoc reports(general section)
--SET @CategoryId = (SELECT CategoryId FROM syFldCategories WHERE Descrip = 'General Information' AND EntityId = 395)
        SELECT TOP 1
                @CategoryId = CategoryId
               ,@FKColDescrip = FKColDescrip
               ,@oldTblFldId = TblFldsId
        FROM    syTblFlds
        WHERE   FldId = @fldId
                AND TblId = (
                              SELECT    TblId
                              FROM      syTables
                              WHERE     TblName = @oldTableName
                            );
        UPDATE  syTblFlds
        SET     CategoryId = NULL
        WHERE   TblFldsId = (
                              SELECT TOP 1
                                        TblFldsId
                              FROM      syTblFlds
                              WHERE     FldId = @fldId
                                        AND TblId = (
                                                      SELECT    TblId
                                                      FROM      syTables
                                                      WHERE     TblName = @oldTableName
                                                    )
                            );
        --PRINT '@oldTblFldId';
        --PRINT @oldTblFldId;
---add the logic of the new field here
        --PRINT '@newFldName';
        --PRINT @newFldName;
        --PRINT '@CategoryId';
        --PRINT @CategoryId;
        IF @newFldName IS NOT NULL
            BEGIN
                IF @newFldName = 'State'
                    BEGIN
                        SET @fldId = (
                                       SELECT   FldId
                                       FROM     syFields
                                       WHERE    FldName = @newFldName
                                                AND FldTypeId = 200
                                     );
                    END;
                ELSE
                    BEGIN
                        SET @fldId = (
                                       SELECT   FldId
                                       FROM     syFields
                                       WHERE    FldName = @newFldName
                                     );
                    END;
                
            END;	
        SET @TblFldId = (
                          SELECT TOP 1
                                    TblFldsId
                          FROM      syTblFlds
                          WHERE     TblId = @TblId
                                    AND FldId = @fldId
                        );
        IF @TblFldId IS NULL
            BEGIN
                SET @TblFldId = (
                                  SELECT    MAX(TblFldsId) + 1
                                  FROM      syTblFlds
                                ); 
                INSERT  INTO dbo.syTblFlds
                        (
                         TblFldsId
                        ,TblId
                        ,FldId
                        ,CategoryId
                        ,FKColDescrip
				        )
                VALUES  (
                         @TblFldId  -- TblFldsId - int
                        ,@TblId  -- TblId - int
                        ,@fldId  -- FldId - int
                        ,@CategoryId  -- CategoryId - int
                        ,@FKColDescrip  -- FKColDescrip - varchar(50)
				        );	
            END;
        ELSE
            BEGIN
                IF ( @CategoryId IS NOT NULL )
                    BEGIN
                        UPDATE  dbo.syTblFlds
                        SET     TblId = @TblId
                               ,FldId = @fldId
                               ,CategoryId = @CategoryId
                               ,FKColDescrip = @FKColDescrip
                        WHERE   TblFldsId = @TblFldId;
                    END;                
            END;
--list of resourceid
        --PRINT '@TblFldId';
        --PRINT @TblFldId;
        --PRINT '@oldTblFldId';
        --PRINT @oldTblFldId;
       
        UPDATE  syRptAdHocFields
        SET     TblFldsId = @TblFldId
        WHERE   AdHocFieldId IN ( SELECT    AdHocFieldId
                                  FROM      syRptAdHocFields
                                  WHERE     TblFldsId = @oldTblFldId
                                            AND ResourceId IN ( SELECT DISTINCT
                                                                        UR.ResourceId
                                                                FROM    syUserResources UR
                                                                       ,syUserResPermissions UP
                                                                WHERE   UR.ResourceId = UP.UserResourceId
                                                                        AND UP.ResourceId = 189 -- for all leads
) );
       
        COMMIT;
    END;
GO
