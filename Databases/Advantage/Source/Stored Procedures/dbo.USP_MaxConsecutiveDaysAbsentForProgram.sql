SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- US6625
CREATE PROCEDURE [dbo].[USP_MaxConsecutiveDaysAbsentForProgram]
    @PrgVerIdList VARCHAR(MAX) = NULL
   ,@ClassMeetingDate DATETIME = GETDATE
   ,@ConsecutiveDays SMALLINT = 1
   ,@CampusId VARCHAR(MAX)
   ,@ShowCalendarDays BIT = 0
AS
    BEGIN
		
        SELECT  R.PrgVerID
               ,R.PrgVerDescrip
               ,R.Student
               ,R.[Student ID]
               ,R.status
               ,R.[Start Date]
               ,R.LDA
               ,R.[Days Missed]
        FROM    (
                  SELECT    PV.PrgVerId AS PrgVerID
                           ,PV.PrgVerDescrip AS PrgVerDescrip
                           ,ST.LastName + ', ' + ST.FirstName + ' ' + ISNULL(ST.MiddleName,'') AS Student
                           ,ST.StudentNumber AS [Student ID]
                           ,SC.StatusCodeDescrip AS status
                           ,SE.StartDate AS [Start Date]
	  --         , (CASE WHEN SE.LDA IS NULL 
	  --                     THEN CONVERT(DATE,  (SELECT TOP 1 SSAS.StudentAttendedDate
	  --                                          FROM dbo.syStudentAttendanceSummary   AS SSAS
	  --                                          WHERE SSAS.StuEnrollId = SE.StuEnrollId
	  --                                            AND SSAS.ActualDays <> 0
	  --                                            AND SSAS.ActualDays <> 9999
	  --                                            AND SSAS.ActualDays <> 99999
	  --                                          ORDER BY SSAS.StudentAttendedDate DESC
	  --                                          ), 101)
					 --      ELSE SE.LDA 
				  --END )                             AS LDA 
                           ,(
                              SELECT    MAX(LDA)
                              FROM      (
                                          SELECT    MAX(AttendedDate) AS LDA
                                          FROM      arExternshipAttendance
                                          WHERE     StuEnrollId = SE.StuEnrollId
                                          UNION ALL
                                          SELECT    MAX(MeetDate) AS LDA
                                          FROM      atClsSectAttendance
                                          WHERE     StuEnrollId = SE.StuEnrollId
                                                    AND Actual > 0
                                                    AND Actual <> 9999.00
                                          UNION ALL
                                          SELECT    MAX(AttendanceDate) AS LDA
                                          FROM      atAttendance
                                          WHERE     EnrollId = SE.StuEnrollId
                                                    AND Actual > 0
                                                    AND Actual <> 9999.00
                                          UNION ALL
                                          SELECT    MAX(RecordDate) AS LDA
                                          FROM      arStudentClockAttendance
                                          WHERE     StuEnrollId = SE.StuEnrollId
                                                    AND (
                                                          ActualHours > 0
                                                          AND ActualHours <> 99.00
                                                          AND ActualHours <> 999.00
                                                          AND ActualHours <> 9999.00
                                                        )
                                          UNION ALL
                                          SELECT    MAX(MeetDate) AS LDA
                                          FROM      atConversionAttendance
                                          WHERE     StuEnrollId = SE.StuEnrollId
                                                    AND (
                                                          Actual > 0
                                                          AND Actual <> 99.00
                                                          AND Actual <> 999.00
                                                          AND Actual <> 9999.00
                                                        )
                                          UNION ALL
                                          SELECT TOP 1
                                                    LDA
                                          FROM      arStuEnrollments
                                          WHERE     StuEnrollId = SE.StuEnrollId
                                        ) TR
                            ) AS LDA
                           ,(
                              SELECT    dbo.MaxConsecutiveDaysAbsentForProgram(SE.StuEnrollId,@ClassMeetingDate, @ShowCalendarDays, @ConsecutiveDays, '00000000-0000-0000-0000-000000000000')
                            ) AS [Days Missed]
                  FROM      arStudent AS ST
                  INNER JOIN dbo.arStuEnrollments AS SE ON ST.StudentId = SE.StudentId
                  INNER JOIN arPrgVersions AS PV ON SE.PrgVerId = PV.PrgVerId
                  INNER JOIN dbo.arPrograms AS PG ON PV.ProgId = PG.ProgId
                  INNER JOIN dbo.syStatusCodes AS SC ON SE.StatusCodeId = SC.StatusCodeId
                  INNER JOIN dbo.sySysStatus AS SS ON SC.SysStatusId = SS.SysStatusId
                  WHERE     (
                              @PrgVerIdList IS NULL
                              OR PV.PrgVerId IN ( SELECT    Val
                                                  FROM      MultipleValuesForReportParameters(@PrgVerIdList,',',1) )
                            )
                            AND SS.InSchool = 1
							AND (
                                @CampusId IS NULL
                                OR SE.CampusId IN (
                                                   SELECT Val
                                                   FROM   MultipleValuesForReportParameters(@CampusId, ',', 1)
                                                   )
                                )
                ) AS R
        WHERE   ( R.[Days Missed] >= @ConsecutiveDays )
        ORDER BY R.PrgVerDescrip
               ,R.Student;
    END;




GO
