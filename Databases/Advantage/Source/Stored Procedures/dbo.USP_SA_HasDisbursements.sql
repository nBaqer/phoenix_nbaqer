SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[USP_SA_HasDisbursements]
    (
     @StuEnrollId UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
    Author : Saraswathi Lakshmanan
    
    Create date : 04/12/2010
    
    Procedure Name : [USP_SA_HasDisbursements]

    Objective : find the disbursements
    
    Parameters : Name Type Data Type Required? 
                        ===== ==== ========= ========= 
                        @StuEnrollId In Uniqueidentifier Required
    
    Output : Returns the AwardAmount and the Payment Plan amount 
                        
*/-----------------------------------------------------------------------------------------------------

    BEGIN
--Has DisbUrsements
        SELECT  (
                  SELECT    COUNT(*)
                  FROM      (
                              SELECT    Amount - COALESCE((
                                                            SELECT  SUM(Amount)
                                                            FROM    saPmtDisbRel
                                                                   ,saTransactions
                                                            WHERE   AwardScheduleId = SAS.AwardScheduleId
                                                                    AND saPmtDisbRel.TransactionId = saTransactions.TransactionId
                                                                    AND Voided = 0
                                                          ),0) AS Balance
                              FROM      faStudentAwards SA
                                       ,faStudentAwardSchedule SAS
                              WHERE     SA.StudentAwardId = SAS.StudentAwardId
                                        AND SA.StuEnrollId = @StuEnrollId
                            ) T
                  WHERE     -- Code changed by kamalesh Ahuja to fix mantis issue id 19076
        --Balance <> 0.00 ) As AwardAmount, 
                            Balance > 0.00
                ) AS AwardAmount
               , 
        -------------------
                (
                  SELECT    COUNT(*)
                  FROM      (
                              SELECT    Amount - COALESCE((
                                                            SELECT  SUM(Amount)
                                                            FROM    saPmtDisbRel
                                                                   ,saTransactions
                                                            WHERE   PayPlanScheduleId = PPS.PayPlanScheduleId
                                                                    AND saPmtDisbRel.TransactionId = saTransactions.TransactionId
                                                                    AND Voided = 0
                                                          ),0) AS Balance
                              FROM      faStudentPaymentPlans SPP
                                       ,faStuPaymentPlanSchedule PPS
                              WHERE     SPP.PaymentPlanId = PPS.PaymentPlanId
                                        AND SPP.StuEnrollId = @StuEnrollId
                            ) T 
            
    -- Code changed by kamalesh Ahuja to fix mantis issue id 19076
    --where Balance <> 0.00 ) As PPAmount
                  WHERE     Balance > 0.00
                ) AS PPAmount;
    ---------------
    END;




GO
