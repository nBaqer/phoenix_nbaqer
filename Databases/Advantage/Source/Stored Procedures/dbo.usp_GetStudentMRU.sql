SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--EXEC usp_GetStudentMRU '0DAE10C4-FD3D-4F73-8F9D-554ADB6E46E4','C963E931-8529-4EAA-AFD3-B4E9B31F1546'
CREATE PROCEDURE [dbo].[usp_GetStudentMRU]
    @UserId UNIQUEIDENTIFIER
   ,@CampusId UNIQUEIDENTIFIER
AS
    SET NOCOUNT ON;
-- For new users pull 20 students from the user's default campus
-- For existing users pull 20 students from the syMRU table, students can be from multiple campuses
    DECLARE @CountMRU INT;
    DECLARE @StudentIdentifier VARCHAR(50);
    SET @StudentIdentifier = (
                               SELECT   Value
                               FROM     dbo.syConfigAppSetValues
                               WHERE    SettingId = 69
                                        AND CampusId IS NULL
                             );

    DECLARE @DefaultCampusId UNIQUEIDENTIFIER;
-- used to return MRU results
    DECLARE @MRUList TABLE
        (
         MRUId UNIQUEIDENTIFIER NOT NULL
        ,Counter INT NOT NULL
        ,ChildId UNIQUEIDENTIFIER NOT NULL
        ,MRUTypeId TINYINT NOT NULL
        ,UserId UNIQUEIDENTIFIER NOT NULL
        ,CampusId UNIQUEIDENTIFIER NULL
        ,SortOrder INT NULL
        ,IsSticky BIT NULL
        ,StudentId UNIQUEIDENTIFIER NOT NULL
        ,FullName VARCHAR(200) NOT NULL
        ,CampusDescrip VARCHAR(200) NOT NULL
        ,LeadId UNIQUEIDENTIFIER NULL
        ,ModDate DATETIME
        ,PrgVerDescrip VARCHAR(200)
        ,StatusCodeDescrip VARCHAR(200)
        );
-- used to create default MRU list for the user if one doesn't exist
    DECLARE @DefaultMRU TABLE
        (
         ChildId UNIQUEIDENTIFIER NOT NULL
        ,MRUTypeId TINYINT NOT NULL
        ,UserId UNIQUEIDENTIFIER NOT NULL
        ,CampusId UNIQUEIDENTIFIER NULL
        ,SortOrder INT NOT NULL
                       IDENTITY
        ,IsSticky BIT NULL
        ,ModDate DATETIME
        );
    SET @CountMRU = (
                      SELECT    COUNT(*)
                      FROM      syMRUS
                      WHERE     UserId = @UserId
                                AND MRUTypeId = 1
                    );

-- Update Default Campus
-- DE8070
    UPDATE  syUsers
    SET     CampusId = @CampusId
    WHERE   UserId = @UserId;
--PRINT 'CountMRU='
--PRINT @CountMRU
-- Print @CountMRU
    IF ( @CountMRU > 0 )
        BEGIN
            INSERT  INTO @MRUList
                    (
                     MRUId
                    ,Counter
                    ,ChildId
                    ,MRUTypeId
                    ,UserId
                    ,CampusId
                    ,SortOrder
                    ,IsSticky
                    ,StudentId
                    ,FullName
                    ,CampusDescrip
                    ,LeadId
                    ,ModDate
                    ,PrgVerDescrip
                    ,StatusCodeDescrip
					 )
                    SELECT DISTINCT TOP 20
                            M.MRUId
                           ,M.Counter
                           ,M.ChildId
                           ,M.MRUTypeId
                           ,M.UserId
                           ,M.CampusId
                           ,M.SortOrder
                           ,M.IsSticky
                           ,S.StudentId
                           ,CASE WHEN S.MiddleName IS NULL
                                      OR S.MiddleName = '' THEN S.FirstName + ' ' + S.LastName
                                 ELSE S.FirstName + ' ' + S.MiddleName + ' ' + S.LastName
                            END AS FullName
                           ,C.CampDescrip
                           ,(
                              SELECT TOP 1
                                        LeadId
                              FROM      arStuEnrollments SQ2
                              WHERE     SQ2.StudentId = S.StudentId
                                        AND LeadId IS NOT NULL
                            ) AS LeadId
                           ,M.ModDate
                           ,(
                              SELECT TOP 1
                                        t1.PrgVerDescrip
                              FROM      arPrgVersions t1
                              INNER JOIN arStuEnrollments t2 ON t1.PrgVerId = t2.PrgVerId
                              WHERE     t2.StudentId = S.StudentId
                                        AND t2.CampusId = M.CampusId
                            ) AS PrgVersionDesc
                           ,(
                              SELECT TOP 1
                                        t1.StatusCodeDescrip
                              FROM      syStatusCodes t1
                              INNER JOIN arStuEnrollments t2 ON t1.StatusCodeId = t2.StatusCodeId
                              WHERE     t2.StudentId = S.StudentId
                                        AND t2.CampusId = M.CampusId
                            ) AS StatusCodeDescrip
                    FROM    dbo.syMRUS AS M
                    INNER JOIN dbo.syMRUTypes AS T ON M.MRUTypeId = T.MRUTypeId
                    INNER JOIN dbo.arStudent AS S ON M.ChildId = S.StudentId
                    INNER  JOIN dbo.syCampuses AS C ON M.CampusId = C.CampusId
                    WHERE   M.MRUTypeId = 1
                            AND UserId = @UserId
                            AND
					-- MRU List will show leads from the campuses user has access to
                            C.CampusId IN ( SELECT DISTINCT
                                                    C.CampusId
                                            FROM    syUsers U
                                            INNER JOIN syUsersRolesCampGrps URC ON U.UserId = URC.UserId
                                            INNER JOIN syCmpGrpCmps CGC ON URC.CampGrpId = CGC.CampGrpId
                                            INNER JOIN syCampuses C ON C.CampusId = CGC.CampusId
                                            WHERE   U.UserId = @UserId
                                                    AND C.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' --Order by C.CampDescrip
					)
                    ORDER BY M.ModDate DESC;

            DECLARE @MruListCount INT; 
            SET @MruListCount = 0;
            SET @MruListCount = (
                                  SELECT    COUNT(*) AS MruCountForFirstTimeUser
                                  FROM      @MRUList
                                );
            SET @DefaultCampusId = (
                                     SELECT DISTINCT
                                            U.CampusId
                                     FROM   syUsers U
                                     WHERE  U.UserId = @UserId
                                   );
			
			--PRINT 'MRUListCount='
			--PRINT @MRUListCount 
			--PRINT '@DefaultCampusId='
			--PRINT @DefaultCampusId
			
			--Print 'E--'
			--Print @DefaultCampusId
			--Print @MruListCount
			
            IF @DefaultCampusId IS NULL
                BEGIN
                    SET @DefaultCampusId = (
                                             SELECT DISTINCT TOP 1
                                                    SC.CampusId
                                             FROM   dbo.syCmpGrpCmps SC
                                                   ,dbo.syUsersRolesCampGrps URC
                                                   ,dbo.syCampuses S
                                             WHERE  URC.UserId = @UserId
                                                    AND SC.CampGrpId = URC.CampGrpId
                                                    AND S.CampusId = SC.CampusId
                                                    AND S.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
									 --Order by S.CampDescrip
                                           );

                END;
            --Print 'G----'
            --Print @DefaultCampusId
            
            -- The MRU list is currently empty
            -- Get the most recently added 20 leads from default campus
            IF @MruListCount = 0
                BEGIN
				--Print 'B--'
				--Print @MruListCount
					-- Print 'A'
                    INSERT  INTO @MRUList
                            (
                             MRUId
                            ,Counter
                            ,ChildId
                            ,MRUTypeId
                            ,UserId
                            ,CampusId
                            ,SortOrder
                            ,IsSticky
                            ,StudentId
                            ,FullName
                            ,CampusDescrip
                            ,ModDate
                            ,PrgVerDescrip
                            ,StatusCodeDescrip
                            )
                            SELECT DISTINCT TOP 20
                                    NEWID() AS MRUId
                                   ,101 AS Counter
                                   ,  -- not being used so hardcoded a value
                                    S.StudentId AS ChildId
                                   ,1 AS MRUTypeId
                                   ,@UserId AS UserId
                                   ,C.CampusId
                                   ,0 AS SortOrder
                                   ,0 AS IsSticky
                                   ,L.LeadId AS LeadId
                                   ,CASE WHEN L.MiddleName IS NULL
                                              OR L.MiddleName = '' THEN L.FirstName + ' ' + L.LastName
                                         ELSE L.FirstName + ' ' + L.MiddleName + ' ' + L.LastName
                                    END AS FullName
                                   ,C.CampDescrip
                                   ,L.ModDate
                                   ,(
                                      SELECT TOP 1
                                                t1.PrgVerDescrip
                                      FROM      arPrgVersions t1
                                      WHERE     t1.PrgVerId = E.PrgVerId
                                    ) AS PrgVersionDesc
                                   ,(
                                      SELECT TOP 1
                                                t1.StatusCodeDescrip
                                      FROM      syStatusCodes t1
                                      WHERE     t1.StatusCodeId = E.StatusCodeId
                                    ) AS StatusCodeDescrip
                            FROM    dbo.arStudent AS S
                            INNER JOIN dbo.arStuEnrollments AS E ON S.StudentId = E.StudentId
                            INNER JOIN dbo.syCampuses AS C ON E.CampusId = C.CampusId
                            INNER JOIN dbo.adLeads AS L ON E.LeadId = L.LeadId
                            WHERE   -- MRU List will show leads from the campuses user has access to
                                    C.CampusId = @DefaultCampusId
                            ORDER BY L.ModDate DESC;
                END;
            DECLARE @intCount INT;
            SET @intCount = (
                              SELECT    COUNT(*)
                              FROM      @MRUList
                            );
				
				-- Print @intCount
				-- Print 'E'
				
            SET @MruListCount = (
                                  SELECT    COUNT(*) AS MruCountForFirstTimeUser
                                  FROM      @MRUList
                                );
				--Print '@MruListCount='
				--Print @MruListCount
				-- The MRU list from default campus is currently empty
				-- Generate the list again from other campus
            IF @MruListCount = 0
                BEGIN
					 --Print 'C'
					 --Print @MruListCount
                    INSERT  INTO @MRUList
                            (
                             MRUId
                            ,Counter
                            ,ChildId
                            ,MRUTypeId
                            ,UserId
                            ,CampusId
                            ,SortOrder
                            ,IsSticky
                            ,LeadId
                            ,FullName
                            ,CampusDescrip
                            ,ModDate
                            ,PrgVerDescrip
                            ,StatusCodeDescrip
                            )
                            SELECT DISTINCT TOP 20
                                    NEWID() AS MRUId
                                   ,101 AS Counter
                                   ,  -- not being used so hardcoded a value
                                    S.StudentId AS ChildId
                                   ,1 AS MRUTypeId
                                   ,@UserId AS UserId
                                   ,C.CampusId
                                   ,0 AS SortOrder
                                   ,0 AS IsSticky
                                   ,L.LeadId AS LeadId
                                   ,CASE WHEN L.MiddleName IS NULL
                                              OR L.MiddleName = '' THEN L.FirstName + ' ' + L.LastName
                                         ELSE L.FirstName + ' ' + L.MiddleName + ' ' + L.LastName
                                    END AS FullName
                                   ,C.CampDescrip
                                   ,L.ModDate
                                   ,(
                                      SELECT TOP 1
                                                t1.PrgVerDescrip
                                      FROM      arPrgVersions t1
                                      WHERE     t1.PrgVerId = E.PrgVerId
                                    ) AS PrgVersionDesc
                                   ,(
                                      SELECT TOP 1
                                                t1.StatusCodeDescrip
                                      FROM      syStatusCodes t1
                                      WHERE     t1.StatusCodeId = E.StatusCodeId
                                    ) AS StatusCodeDescrip
                            FROM    dbo.arStudent AS S
                            INNER JOIN dbo.arStuEnrollments AS E ON S.StudentId = E.StudentId
                            INNER JOIN dbo.syCampuses AS C ON E.CampusId = C.CampusId
                            INNER JOIN dbo.adLeads AS L ON E.LeadId = L.LeadId
                            WHERE   -- MRU List will show leads from the campuses user has access to
                                    C.CampusId IN ( SELECT DISTINCT
                                                            C.CampusId
                                                    FROM    syUsers U
                                                    INNER JOIN syUsersRolesCampGrps URC ON U.UserId = URC.UserId
                                                    INNER JOIN syCmpGrpCmps CGC ON URC.CampGrpId = CGC.CampGrpId
                                                    INNER JOIN syCampuses C ON C.CampusId = CGC.CampusId
                                                    WHERE   U.UserId = @UserId
                                                            AND C.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' --Order by C.CampDescrip
									)
                            ORDER BY L.ModDate DESC;
                END;
					
					--Select * from @MRUList
					-- Always keep the count to 20.
					-- If existing users had more than 20 records delete it
            DELETE  FROM syMRUS
            WHERE   UserId = @UserId
                    AND MRUTypeId = 1
                    AND MRUId NOT IN ( SELECT DISTINCT
                                                MRUId
                                       FROM     @MRUList );

        END;
    ELSE
        BEGIN
		
            SET @DefaultCampusId = (
                                     SELECT DISTINCT
                                            U.CampusId
                                     FROM   syUsers U
                                     WHERE  U.UserId = @UserId
                                   );
		--Print 'Default Campus'
		--Print @DefaultCampusId
            IF @DefaultCampusId IS NULL
                BEGIN
                    SET @DefaultCampusId = (
                                             SELECT DISTINCT TOP 1
                                                    SC.CampusId
                                             FROM   dbo.syCmpGrpCmps SC
                                                   ,dbo.syUsersRolesCampGrps URC
                                                   ,dbo.syCampuses S
                                             WHERE  URC.UserId = @UserId
                                                    AND SC.CampGrpId = URC.CampGrpId
                                                    AND S.CampusId = SC.CampusId
                                                    AND S.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
								--Order by S.CampDescrip
                                           );

                END;


            INSERT  INTO @DefaultMRU
                    (
                     ChildId
                    ,MRUTypeId
                    ,UserId
                    ,CampusId
                    ,IsSticky
                    ,ModDate 
					)
                    SELECT DISTINCT TOP 20
                            S.StudentId AS ChildId
                           ,1 AS MRUTypeId
                           ,@UserId AS UserId
                           ,C.CampusId
                           ,0 AS IsSticky
                           ,S.ModDate --From arStudent table
                    FROM    dbo.arStudent AS S
                    INNER JOIN dbo.arStuEnrollments AS E ON S.StudentId = E.StudentId
                    INNER JOIN dbo.syCampuses AS C ON E.CampusId = C.CampusId
                    INNER JOIN dbo.adLeads AS L ON E.LeadId = L.LeadId
                    WHERE   -- MRU List will show leads from the campuses user has access to
                            C.CampusId = @DefaultCampusId
                    ORDER BY S.ModDate DESC;

            DECLARE @MruCountForFirstTimeUser INT; 
            SET @MruCountForFirstTimeUser = 0;
            SET @MruCountForFirstTimeUser = (
                                              SELECT    COUNT(*) AS MruCountForFirstTimeUser
                                              FROM      @DefaultMRU
                                            );
            
            -- There are no leads from default campus, so get leads from other campus
            -- user has access to
            IF @MruCountForFirstTimeUser = 0
                BEGIN
					 --Print 'C'
                    INSERT  INTO @DefaultMRU
                            (
                             ChildId
                            ,MRUTypeId
                            ,UserId
                            ,CampusId
                            ,IsSticky
                            ,ModDate 
						    )
                            SELECT DISTINCT TOP 20
                                    S.StudentId AS ChildId
                                   ,1 AS MRUTypeId
                                   ,@UserId AS UserId
                                   ,E.CampusId
                                   ,0 AS IsSticky
                                   ,L.ModDate
                            FROM    dbo.arStudent AS S
                            INNER JOIN dbo.arStuEnrollments AS E ON S.StudentId = E.StudentId
                            INNER JOIN dbo.adLeads AS L ON E.LeadId = L.LeadId
                            WHERE   -- MRU List will show leads from the campuses user has access to
                                    E.CampusId IN ( SELECT DISTINCT
                                                            C.CampusId
                                                    FROM    syUsers U
                                                    INNER JOIN syUsersRolesCampGrps URC ON U.UserId = URC.UserId
                                                    INNER JOIN syCmpGrpCmps CGC ON URC.CampGrpId = CGC.CampGrpId
                                                    INNER JOIN syCampuses C ON C.CampusId = CGC.CampusId
                                                    WHERE   U.UserId = @UserId
                                                            AND C.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' --Order by C.CampDescrip
								 )
                            ORDER BY L.ModDate DESC;
                END;

				-- insert records into the MRU table
            INSERT  INTO dbo.syMRUS
                    (
                     ChildId
                    ,MRUTypeId
                    ,UserId
                    ,CampusId
                    ,SortOrder
                    ,IsSticky
                    ,ModDate 
						
                    )
                    SELECT  ChildId
                           ,MRUTypeId
                           ,UserId
                           ,CampusId
                           ,SortOrder
                           ,IsSticky
                           ,ModDate
                    FROM    @DefaultMRU;


				-- Fill the MRU results into the @MRUList table variable
            INSERT  INTO @MRUList
                    (
                     MRUId
                    ,Counter
                    ,ChildId
                    ,MRUTypeId
                    ,UserId
                    ,CampusId
                    ,SortOrder
                    ,IsSticky
                    ,StudentId
                    ,FullName
                    ,CampusDescrip
                    ,LeadId
                    ,ModDate
                    ,PrgVerDescrip
                    ,StatusCodeDescrip
						
                    )
                    SELECT DISTINCT TOP 20
                            NEWID() AS MRUId
                           ,101 AS Counter
                           ,ChildId
                           ,MRUTypeId
                           ,UserId
                           ,CampusId
                           ,SortOrder
                           ,IsSticky
                           ,DM.ChildId AS StudentId
                           ,(
                              SELECT DISTINCT
                                        FirstName + ' ' + ISNULL(MiddleName,'') + ' ' + LastName
                              FROM      arStudent L
                              WHERE     StudentId = DM.ChildId
                            ) AS FullName
                           ,(
                              SELECT DISTINCT
                                        CampDescrip
                              FROM      syCampuses
                              WHERE     CampusId = DM.CampusId
                            ) AS CampusDescrip
                           ,(
                              SELECT TOP 1
                                        LeadId
                              FROM      arStuEnrollments SQ2
                              WHERE     SQ2.StudentId = DM.ChildId
                                        AND LeadId IS NOT NULL
                            ) AS LeadId
                           ,DM.ModDate
                           ,(
                              SELECT TOP 1
                                        t1.PrgVerDescrip
                              FROM      arPrgVersions t1
                              INNER JOIN arStuEnrollments t2 ON t1.PrgVerId = t2.PrgVerId
                              WHERE     t2.StudentId = DM.ChildId
                                        AND t2.CampusId = DM.CampusId
                            ) AS ProgramVersionDesc
                           ,(
                              SELECT TOP 1
                                        t1.StatusCodeDescrip
                              FROM      syStatusCodes t1
                              INNER JOIN arStuEnrollments t2 ON t1.StatusCodeId = t2.StatusCodeId
                              WHERE     t2.StudentId = DM.ChildId
                                        AND t2.CampusId = DM.CampusId
                            ) AS StatusCodeDescrip
                    FROM    @DefaultMRU DM
                    ORDER BY ModDate DESC;
        END;



--Drop table MRUTable
    SELECT  MRUId
           ,Counter
           ,ChildId
           ,MRUTypeId
           ,UserId
           ,CampusId
           ,SortOrder
           ,IsSticky
           ,StudentId
           ,FullName
           ,CampusDescrip
           ,LeadId
           ,ModDate
           ,'Program: ' + PrgVerDescrip AS ProgramVersionDesc
           ,'Status: ' + StatusCodeDescrip AS StatusCodeDescrip
    FROM    @MRUList
    ORDER BY ModDate DESC;
GO
