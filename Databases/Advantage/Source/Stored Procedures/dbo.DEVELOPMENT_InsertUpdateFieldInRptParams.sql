SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Sweta Thakkar
-- Create date: 11/1/2016
-- Description:	Insert/Update New/Updated Column in syRptParams
--              The column is register in syFields, caption.
--				The fields is linked to a table and to 
--				specific resource for report. If the table does not exists be created
--				Be CAREFULLY if you enter a ntable name erroneous, the table
--              resource should be created
-- Conditions: 1 -Resource in syRptParams must exists
-- 
-- =============================================
CREATE PROCEDURE [dbo].[DEVELOPMENT_InsertUpdateFieldInRptParams]
    @ResourceId INT
   ,@RptCaption VARCHAR(50) 
   -- Fill with your fieldName (the column to be inserted in resources. This name must be unique
   ,@FieldName VARCHAR(50)
   ,@oldFldName VARCHAR(50)
 -- Enter the type of field (see table syFieldsType)
 -- FldTypeId	FldType
 -- 2	Smallint
 -- 3	Int
 -- 5	Float
 -- 6	Money
 -- 11	Bit
 -- 17	TinyInt
 -- 72	Uniqueidentifier
 -- 129	Char
 -- 131	Decimal
 -- 135	Datetime
 -- 200	Varchar
   ,@FldTypeId INT
   
   -- Field Length. this is valid for text
   ,@FldLen INT = 36
   
   -- DLL table ID to be connected in the case of combobox 
   -- form 3.8 it is only use in Ad Hoc report. Otherwise 0
   ,@DDLId INT = 0
   ,   
   -- If the field should be included in the log 
   -- 1 log change 0 no log.
    @LogChanges BIT = 0
   ,    
	-- Begin personalitation FOR syFldCaptions ------------------------------------------------------------
    -- Caption to be show if the column is displayed in Advantage
    @FieldCaption VARCHAR(50) = ''
	--Language ID to select a specific language in table. Use 1 USA in moment
	-- it is the only
   ,@LanguageId INT = 1
   ,
-- Personalitation Table syTable
    -- Name of the table where the resource is
    @TblName VARCHAR(50) = ''
-- Begin personalitation FOR SyTblFlds ------------------------------------------------------------
	-- CategoryId
	-- 
	-- CategoryId	EntityID	Description
	-- 13940		394			General Information
	-- 13941		394			Course
	-- 13942		394			Programs
	-- 13945		394			Enrollment
	-- 13944		394			SAP
	-- 23950		395			General Information
	-- 23951		395			Advertisement
	-- 23952		395			Admissions Rep
	-- 23953		395			Education
	-- 33960		396			General Information
	-- 43970		397			General Information
	-- 13949		394			Placement Information
	-- 54340		434			General Information
	-- 1			394			User Defined
	-- 2			395			User Defined
	-- 3			396			User Defined
	-- 4			397			User Defined
	-- 5			434			User Defined
	-- 13946		394			Ledger
	-- 13947		394			Awards
	-- 13948		394			Payments
	-- 13949		394			Requirements
	-- 13950		394			Education
	-- 13951		394			Term
	-- 13952		394			Class
	-- 13953		394			Exit Interview
	-- 13954		394			Placement History
	-- 54341		395			Lead Transactions 
   ,@CategoryId INT = NULL
   ,
 -- has only sense in Ad Hoc reports. 
    @FKColDescrip VARCHAR(100) = ''
-- Begin personalitation FOR syRptParams ------------------------------------------------------------
    
-- End personalitation FOR syRptParams ---
AS
    BEGIN
-- *****************************************************************************************************************
        BEGIN TRAN;
        SET NOCOUNT ON;
-- STEP 1 Determine if the table is registered, is not register it in syTables as first step
--		  If table exists the record is updated!!
	-- See If the table exists in syTables
        DECLARE @TableId INT = (
                                 SELECT TOP 1
                                        TblId
                                 FROM   syTables
                                 WHERE  TblName = @TblName
                               );
        IF @TableId IS NULL
            BEGIN -- insert table
                SET @TableId = (
                                 SELECT MAX(TblId) + 1
                                 FROM   syTables
                               );
                INSERT  INTO dbo.syTables
                        ( TblId,TblName,TblPK )
                VALUES  ( @TableId  -- TblId - int
                          ,@TblName  -- TblName - varchar(50)
                          ,0 -- TblPK- int (provisional later this change to the real value)
                          );
            END;
        
-- STEP II - syFields - Check if the field associated exists. should be only one.
    -- Insert statements for procedure here
	-- Check if exists the field
        DECLARE @FieldId INT = (
                                 SELECT TOP 1
                                        syFields.FldId
                                 FROM   syFields
                                 WHERE  FldName = @FieldName
                               );
        IF @FieldId IS NULL
            BEGIN -- INSERT 
                SET @FieldId = (
                                 SELECT MAX(FldId) + 1
                                 FROM   syFields
                               ); 
                INSERT  INTO syFields
                        (
                         FldId
                        ,FldName
                        ,FldTypeId
                        ,FldLen
                        ,DDLId
                        ,LogChanges
                        )
                VALUES  (
                         @FieldId
                        ,@FieldName
                        ,@FldTypeId
                        ,@FldLen
                        ,@DDLId
                        ,@LogChanges
                        );
            END;
        ELSE
            BEGIN
                UPDATE  syFields
                SET     FldName = @FieldName
                       ,FldTypeId = @FldTypeId
                       ,FldLen = @FldLen
                       ,DDLId = @DDLId
                       ,LogChanges = @LogChanges
                WHERE   ( FldId = @FieldId );
            END;
 
-- STEP III: Add in syTblFlds the relation between the new column and the table that contain it************
 
      
        DECLARE @TblId INT = (
                               SELECT TOP 1
                                        TblId
                               FROM     syTables
                               WHERE    TblName = @TblName
                             );
        DECLARE @TblFldId INT = (
                                  SELECT TOP 1
                                            TblFldsId
                                  FROM      dbo.syTblFlds
                                  WHERE     TblId = @TblId
                                            AND FldId = @FieldId
                                );
     
-- If the relation does not exists 
 
        IF @TblFldId IS NULL
            BEGIN
                SET @TblFldId = (
                                  SELECT    MAX(TblFldsId) + 1
                                  FROM      syTblFlds
                                ); 
                INSERT  INTO dbo.syTblFlds
                        (
                         TblFldsId
                        ,TblId
                        ,FldId
                        ,CategoryId
                        ,FKColDescrip
				        )
                VALUES  (
                         @TblFldId  -- TblFldsId - int
                        ,@TblId  -- TblId - int
                        ,@FieldId  -- FldId - int
                        ,@CategoryId  -- CategoryId - int
                        ,@FKColDescrip  -- FKColDescrip - varchar(50)
				        );	
            END;
        ELSE
            BEGIN
                
                UPDATE  dbo.syTblFlds
                SET     TblId = @TblId
                       ,FldId = @FieldId
                       ,CategoryId = @CategoryId
                       ,FKColDescrip = @FKColDescrip
                WHERE   TblFldsId = @TblFldId;
            END;
--STEP IV: Add in syFldCaptions the caption for the field ********************************************
 
        DECLARE @PlacementFldId INT = (
                                        SELECT TOP 1
                                                syFields.FldId
                                        FROM    syFields
                                        --JOIN    dbo.syTblFlds ON syTblFlds.FldId = syFields.FldId
                                        --JOIN    dbo.syTables ON syTables.TblId = syTblFlds.TblId
                                        WHERE   FldName = @FieldName
                                        --        AND TblName = @TblName
                                      );
        DECLARE @PlacementCaption INT = (
                                          SELECT TOP 1
                                                    FldCapId
                                          FROM      syFldCaptions
                                          WHERE     FldId = @PlacementFldId
                                        );
--insert if not exists
        IF @PlacementCaption IS NULL
            BEGIN
                INSERT  INTO syFldCaptions
                        (
                         FldCapId
                        ,FldId
                        ,LangId
                        ,Caption
                        ,FldDescrip
                        )
                VALUES  (
                         (
                           SELECT   MAX(FldCapId)
                           FROM     syFldCaptions
                         ) + 1
                        ,@PlacementFldId
                        ,1
                        ,@FieldCaption
                        ,NULL
                        );
            END;
        ELSE
            BEGIN
                UPDATE  syFldCaptions
                SET     FldId = @PlacementFldId
                       ,LangId = @LanguageId
                       ,Caption = @FieldCaption
                       ,FldDescrip = NULL
                WHERE   FldCapId = @PlacementCaption;
            END;   	
 
-- STEP V: Add in syRptParams the relation between the new column and the resource in syRptParams
        DECLARE @RptParamId INT;
        SET @RptParamId = (
                            SELECT  RptParamId
                            FROM    syRptParams t1
                            JOIN    syTblFlds t2 ON t2.TblFldsId = t1.TblFldsId
                            JOIN    syFields t5 ON t5.FldId = t2.FldId
                            WHERE   ResourceId = @ResourceId
                                    AND RptCaption = @RptCaption
                                    AND FldName = @oldFldName
                          );
        UPDATE  syRptParams
        SET     TblFldsId = @TblFldId
        WHERE   RptParamId = @RptParamId;
        COMMIT;
    END;
GO
