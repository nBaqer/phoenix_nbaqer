SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_RulesByEffectiveDate_GetList]
    @ReqId UNIQUEIDENTIFIER
AS
    SELECT DISTINCT
            EffectiveDate
    FROM    arRules_GradeComponentTypes_Courses t1
    INNER JOIN (
                 SELECT *
                 FROM   arBridge_GradeComponentTypes_Courses
                 WHERE  ReqId = @ReqId
               ) t2 ON t1.GrdComponentTypeId_ReqId = t2.GrdComponentTypeId_ReqId
    UNION
    SELECT DISTINCT
            EffectiveDate
    FROM    arPrerequisites_GradeComponentTypes_Courses t1
    INNER JOIN (
                 SELECT *
                 FROM   arBridge_GradeComponentTypes_Courses
                 WHERE  ReqId = @ReqId
               ) t2 ON t1.ReqId = t2.ReqId
    UNION
    SELECT DISTINCT
            EffectiveDate
    FROM    arMentor_GradeComponentTypes_Courses t1
    INNER JOIN (
                 SELECT *
                 FROM   arBridge_GradeComponentTypes_Courses
                 WHERE  ReqId = @ReqId
               ) t2 ON t1.ReqId = t2.ReqId
    ORDER BY EffectiveDate DESC;



GO
