SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetFERPAPolicy]
    (
     @FERPAEntityID UNIQUEIDENTIFIER
    ,@studentID UNIQUEIDENTIFIER
    )
AS
    SET NOCOUNT ON;
    SELECT  *
    FROM    arFerpaPolicy
    WHERE   FERPAEntityID = @FERPAEntityID
            AND studentid = @studentID;



GO
