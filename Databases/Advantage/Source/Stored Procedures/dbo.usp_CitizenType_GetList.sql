SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_CitizenType_GetList] @Descrip VARCHAR(50)
AS
    SELECT  CitizenShipId
    FROM    adCitizenships
    WHERE   CitizenshipDescrip LIKE @Descrip + '%';



GO
