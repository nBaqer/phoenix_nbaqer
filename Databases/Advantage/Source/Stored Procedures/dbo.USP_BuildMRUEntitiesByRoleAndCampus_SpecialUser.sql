SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
CREATE PROCEDURE [dbo].[USP_BuildMRUEntitiesByRoleAndCampus_SpecialUser]
AS
    SELECT DISTINCT
            ResourceId
           ,RESOURCE
           ,Entity
           ,MRUType
    FROM    (
              SELECT    ResourceID
                       ,Resource
                       ,CASE WHEN ModuleId = 189 THEN 394  -- AD:Lead Entity
                             ELSE CASE WHEN ModuleId IN ( 26,191,193,194,300 ) THEN 395 -- AR,PL,FinAid,Faculty, StudentAccounts Module:Student Entity
                                       ELSE CASE WHEN ModuleId IN ( 192 ) THEN 396 -- HR:Employees
                                                 ELSE NULL
                                            END
                                  END
                        END AS Entity
                       ,CASE WHEN ModuleId = 189 THEN 1  -- AD:Lead Entity
                             ELSE CASE WHEN ModuleId IN ( 26,191,193,194,300 ) THEN 2 -- AR,PL,FinAid,Faculty, StudentAccounts Module:Student Entity
                                       ELSE CASE WHEN ModuleId IN ( 192 ) THEN 4 -- HR:Employees
                                                 ELSE NULL
                                            END
                                  END
                        END AS MRUType
              FROM      syRolesModules RM
                       ,syResources R
              WHERE     RM.ModuleId = R.ResourceId
              UNION
 --Placement Module has two entities - Student and Employer
              SELECT DISTINCT
                        ResourceID
                       ,Resource
                       ,CASE WHEN ModuleId = 193 THEN 397 --PL:Employers
                             ELSE CASE WHEN ModuleId = 191 THEN 434 -- FinAid:Lenders
                                       ELSE NULL
                                  END
                        END AS Entity
                       ,CASE WHEN ModuleId = 193 THEN 3 --PL:Employers
                             ELSE CASE WHEN ModuleId = 191 THEN 5 -- FinAid:Lenders
                                       ELSE NULL
                                  END
                        END AS MRUType
              FROM      syRolesModules RM
                       ,syResources R
              WHERE     RM.ModuleId = R.ResourceId
            ) t1
    WHERE   Entity IS NOT NULL
    ORDER BY MRUType;  



GO
