SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jose Torres
-- Create date: 11/20/2014
-- Description: Get List of active Program Versions that are attached a Billing Method 
--              Also get if a Payment Period of the BillinMethod is al being used to charge students
--              if it is true the Billing Method should not deatach from the Program Version
--                            the Payment Period could not be update or deleted
-- EXECUTE  [USP_PrgVersionsGetListForABillingMethod    @BillingMethodId = N'F335D044-F05B-4F4F-8590-39552C6FDC3F'              
--                 
-- =============================================

/*
IncrementType
	<Description("Not Apply")>         NotApply         = -1
	<Description("Actual Hours")>      ActualHours      = 0
	<Description("Scheduled Hours")>   ScheduledHours   = 1
	<Description("Credits Attempted")> CreditsAttempted = 2
	<Description("Credits Earned")>    CreditsEarned    = 3
*/

CREATE PROCEDURE [dbo].[USP_PrgVersionsGetListForABillingMethod]
    @BillingMethodId NVARCHAR(50)
AS
    BEGIN
        DECLARE @temp AS TABLE
            (
             PrgVerId NVARCHAR(50) NOT NULL
            ,PrgVerDescrip NVARCHAR(50)
            ,IsBillingMethodCharged NVARCHAR(10)
            ,IsUsed NVARCHAR(10)
            );  
        DECLARE @IsUsed NVARCHAR(10);
	 
        INSERT  INTO @temp
                (
                 PrgVerId
                ,PrgVerDescrip
                ,IsBillingMethodCharged
                ,IsUsed
                )
                SELECT DISTINCT
                        APV.PrgVerId
                       ,APV.PrgVerDescrip AS PrgVerDescrip
                       ,CASE WHEN APV.IsBillingMethodCharged = 1 THEN 'True'
                             ELSE 'False'
                        END AS IsBillingMethodCharged
                       ,'False'
                FROM    arPrgVersions AS APV
                INNER JOIN syStatuses AS SS ON APV.StatusId = SS.StatusId
                WHERE   SS.StatusCode = 'A'
                        AND APV.BillingMethodId = @BillingMethodId
                ORDER BY APV.PrgVerDescrip; 


        SELECT TOP 1
                @IsUsed = 'True'
        FROM    saPmtPeriods AS SPP
        INNER JOIN dbo.saIncrements AS SI ON SPP.IncrementId = SI.IncrementId
        INNER JOIN dbo.saBillingMethods AS SBM ON SI.BillingMethodId = SBM.BillingMethodId
        WHERE   SBM.BillingMethodId = @BillingMethodId
                AND SPP.IsCharged = 1;


        UPDATE  T
        SET     T.IsUsed = @IsUsed
        FROM    @temp AS T;   
	   
        SELECT  T.PrgVerId
               ,T.PrgVerDescrip
               ,T.IsBillingMethodCharged
               ,T.IsUsed
        FROM    @temp AS T;

    END;




GO
