SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_pr_main_ssrs_subreport3_greaterthan2]
    @StuEnrollId VARCHAR(50)
   ,@TermId VARCHAR(50) = NULL
   ,@GradesFormat VARCHAR(50)
   ,@GPAMethod VARCHAR(50)
AS
    SELECT  *
    FROM    (
              SELECT    *
                       ,ROW_NUMBER() OVER ( PARTITION BY StuEnrollId,TermId ORDER BY TermStartDate, TermEndDate, TermDescription, CourseDescription ) AS rownumber2
              FROM      (
                          SELECT DISTINCT
                                    3 AS Tag
                                   ,2 AS Parent
                                   ,PV.PrgVerId
                                   ,PV.PrgVerDescrip
                                   ,NULL AS ProgramCredits
                                   ,T.TermId
                                   ,T.TermDescrip AS TermDescription
                                   ,T.StartDate AS TermStartDate
                                   ,T.EndDate AS TermEndDate
                                   ,CS.ReqId AS CourseId
                                   ,R.Code AS CourseCode
                                   ,R.Descrip AS CourseDescription
                                   ,'(' + R.Code + ')' + R.Descrip AS CourseCodeDescription
                                   ,R.Credits AS CourseCredits
                                   ,R.FinAidCredits AS CourseFinAidCredits
                                   ,(
                                      SELECT    MIN(MinVal)
                                      FROM      arGradeScaleDetails GCD
                                               ,arGradeSystemDetails GSD
                                      WHERE     GCD.GrdSysDetailId = GSD.GrdSysDetailId
                                                AND GSD.IsPass = 1
                                                AND GCD.GrdScaleId = CS.GrdScaleId
                                    ) AS MinVal
                                   ,RES.Score AS CourseScore
                                   ,NULL AS GradeBook_ResultId
                                   ,NULL AS GradeBookDescription
                                   ,NULL AS GradeBookScore
                                   ,NULL AS GradeBookPostDate
                                   ,NULL AS GradeBookPassingGrade
                                   ,NULL AS GradeBookWeight
                                   ,NULL AS GradeBookRequired
                                   ,NULL AS GradeBookMustPass
                                   ,NULL AS GradeBookSysComponentTypeId
                                   ,NULL AS GradeBookHoursRequired
                                   ,NULL AS GradeBookHoursCompleted
                                   ,SE.StuEnrollId
                                   ,NULL AS MinResult
                                   ,NULL AS GradeComponentDescription -- Student data   
                                   ,SCS.CreditsAttempted AS CreditsAttempted
                                   ,SCS.CreditsEarned AS CreditsEarned
                                   ,SCS.Completed AS Completed
                                   ,SCS.CurrentScore AS CurrentScore
                                   ,SCS.CurrentGrade AS CurrentGrade
                                   ,SCS.FinalScore AS FinalScore
                                   ,SCS.FinalGrade AS FinalGrade
                                   ,C.CampusId
                                   ,C.CampDescrip
                                   ,NULL AS rownumber
                                   ,S.FirstName AS FirstName
                                   ,S.LastName AS LastName
                                   ,S.MiddleName
                                   ,(
                                      SELECT    COUNT(*) AS GrdBkWgtDetailsCount
                                      FROM      arGrdBkResults
                                      WHERE     StuEnrollId = SE.StuEnrollId
                                                AND ClsSectionId = RES.TestId
                                    ) AS GrdBkWgtDetailsCount
                                   ,CASE WHEN P.ACId = 5 THEN 'True'
                                         ELSE 'False'
                                    END AS ClockHourProgram
                          FROM      arClassSections CS
                          INNER JOIN arResults GBR ON CS.ClsSectionId = GBR.TestId
                          INNER JOIN arStuEnrollments SE ON GBR.StuEnrollId = SE.StuEnrollId
                          INNER JOIN (
                                       SELECT   StudentId
                                               ,FirstName
                                               ,LastName
                                               ,MiddleName
                                       FROM     arStudent
                                     ) S ON S.StudentId = SE.StudentId
                          INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                          INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                          INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                          INNER JOIN arTerm T ON CS.TermId = T.TermId
                          INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                          INNER JOIN arResults RES ON RES.StuEnrollId = GBR.StuEnrollId
                                                      AND RES.TestId = CS.ClsSectionId
                          LEFT JOIN syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                                           AND T.TermId = SCS.TermId
                                                           AND R.ReqId = SCS.ReqId
                          WHERE     SE.StuEnrollId = @StuEnrollId
                                    AND (
                                          @TermId IS NULL
                                          OR T.TermId = @TermId
                                        )
                          UNION
                          SELECT DISTINCT
                                    3
                                   ,2
                                   ,PV.PrgVerId
                                   ,PV.PrgVerDescrip
                                   ,NULL
                                   ,T.TermId
                                   ,T.TermDescrip
                                   ,T.StartDate
                                   ,T.EndDate
                                   ,GBCR.ReqId
                                   ,R.Code AS CourseCode
                                   ,R.Descrip AS CourseDescrip
                                   ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                   ,R.Credits
                                   ,R.FinAidCredits
                                   ,(
                                      SELECT    MIN(MinVal)
                                      FROM      arGradeScaleDetails GCD
                                               ,arGradeSystemDetails GSD
                                      WHERE     GCD.GrdSysDetailId = GSD.GrdSysDetailId
                                                AND GSD.IsPass = 1
                                    )
                                   ,ISNULL(GBCR.Score,0)
                                   ,NULL
                                   ,NULL
                                   ,NULL
                                   ,NULL
                                   ,NULL
                                   ,NULL
                                   ,NULL
                                   ,NULL
                                   ,NULL
                                   ,NULL
                                   ,NULL
                                   ,SE.StuEnrollId
                                   ,NULL AS MinResult
                                   ,NULL AS GradeComponentDescription -- Student data    
                                   ,SCS.CreditsAttempted AS CreditsAttempted
                                   ,SCS.CreditsEarned AS CreditsEarned
                                   ,SCS.Completed AS Completed
                                   ,SCS.CurrentScore AS CurrentScore
                                   ,SCS.CurrentGrade AS CurrentGrade
                                   ,SCS.FinalScore AS FinalScore
                                   ,SCS.FinalGrade AS FinalGrade
                                   ,C.CampusId
                                   ,C.CampDescrip
                                   ,NULL AS rownumber
                                   ,S.FirstName AS FirstName
                                   ,S.LastName AS LastName
                                   ,S.MiddleName
                                   ,(
                                      SELECT    COUNT(*) AS GrdBkWgtDetailsCount
                                      FROM      arGrdBkConversionResults
                                      WHERE     StuEnrollId = SE.StuEnrollId
                                                AND TermId = GBCR.TermId
                                                AND ReqId = GBCR.ReqId
                                    ) AS GrdBkWgtDetailsCount
                                   ,CASE WHEN P.ACId = 5 THEN 'True'
                                         ELSE 'False'
                                    END AS ClockHourProgram
                          FROM      arTransferGrades GBCR
                          INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                          INNER JOIN (
                                       SELECT   StudentId
                                               ,FirstName
                                               ,LastName
                                               ,MiddleName
                                       FROM     arStudent
                                     ) S ON S.StudentId = SE.StudentId
                          INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                          INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                          INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                          INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                          INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
						--INNER JOIN arTransferGrades AR ON GBCR.StuEnrollId = AR.StuEnrollId
                          LEFT JOIN syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                                           AND T.TermId = SCS.TermId
                                                           AND R.ReqId = SCS.ReqId
                          WHERE     SE.StuEnrollId = @StuEnrollId
                                    AND (
                                          @TermId IS NULL
                                          OR T.TermId = @TermId
                                        )
                        ) dt
            ) dt1
    WHERE   rownumber2 > 2
    ORDER BY TermStartDate
           ,TermEndDate
           ,TermDescription
           ,FinalGrade DESC
           ,FinalScore DESC
           ,CourseCode
           ,rownumber2;




GO
