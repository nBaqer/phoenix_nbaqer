SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GetAttendanceSummaryReportData]
    @StartDate DATETIME2
   ,@EndDate DATETIME2
   ,@ProgramVersions VARCHAR(MAX) = NULL
   ,@StudentGroups VARCHAR(MAX) = NULL
   ,@Enrollments VARCHAR(MAX) = NULL
AS
    DECLARE @RangeResults TABLE
        (
            FirstName VARCHAR(50)
           ,LastName VARCHAR(50)
           ,MiddleName VARCHAR(50)
           ,BadgeNumber VARCHAR(50)
           ,StudentEnrollmentId UNIQUEIDENTIFIER
           ,Actual DECIMAL(18, 2)
           ,Absent DECIMAL(18, 2)
           ,Makeup DECIMAL(18, 2)
           ,Scheduled DECIMAL(18, 2)
           ,AttendancePercentage DECIMAL(18, 2)
        );

    DECLARE @TotalResults TABLE
        (
            StudentEnrollmentId UNIQUEIDENTIFIER
           ,Actual DECIMAL(18, 2)
           ,Absent DECIMAL(18, 2)
           ,Makeup DECIMAL(18, 2)
           ,Scheduled DECIMAL(18, 2)
           ,AttendancePercentage DECIMAL(18, 2)
        );

    --Used to calculate make up and absent days since they are totals at given day
    DECLARE @DayBeforeStartDate DATETIME2 = DATEADD(DAY, -1, @StartDate);

    DECLARE @EmptyGuid VARCHAR(MAX) = '00000000-0000-0000-0000-000000000000';


    IF @Enrollments <> @EmptyGuid
       AND @Enrollments IS NOT NULL
       AND @Enrollments <> ''
        BEGIN
            SET @ProgramVersions = NULL;
            SET @StudentGroups = NULL;
        END;
    ELSE
        BEGIN
            SET @Enrollments = NULL;

        END;


    INSERT INTO @RangeResults
                SELECT   NotDupes.FirstName
                        ,NotDupes.LastName
                        ,NotDupes.MiddleName
                        ,NotDupes.BadgeNumber
                        ,NotDupes.StuEnrollId
                        ,SUM(   CASE WHEN NotDupes.StudentAttendedDate >= @StartDate
                                          AND NotDupes.StudentAttendedDate <= @EndDate THEN NotDupes.ActualDays
                                     ELSE 0
                                END
                            ) AS Actual
                        ,SUM(   CASE WHEN NotDupes.StudentAttendedDate >= @StartDate
                                          AND NotDupes.StudentAttendedDate <= @EndDate
                                          AND ( NotDupes.ScheduledDays - NotDupes.ActualDays ) > 0 THEN ( NotDupes.ScheduledDays - NotDupes.ActualDays )
                                     ELSE 0
                                END
                            ) AS Absent
                        ,SUM(   CASE WHEN NotDupes.StudentAttendedDate >= @StartDate
                                          AND NotDupes.StudentAttendedDate <= @EndDate
                                          AND ( NotDupes.ActualDays - NotDupes.ScheduledDays ) > 0 THEN ( NotDupes.ActualDays - NotDupes.ScheduledDays )
                                     ELSE 0
                                END
                            ) AS Makeup
                        ,SUM(   CASE WHEN NotDupes.StudentAttendedDate >= @StartDate
                                          AND NotDupes.StudentAttendedDate <= @EndDate THEN NotDupes.ScheduledDays
                                     ELSE 0
                                END
                            ) AS Scheduled
                        ,CASE WHEN SUM(   CASE WHEN NotDupes.StudentAttendedDate >= @StartDate
                                                    AND NotDupes.StudentAttendedDate <= @EndDate THEN NotDupes.ScheduledDays
                                               ELSE 0
                                          END
                                      ) = 0 THEN 0
                              ELSE ( SUM(   CASE WHEN NotDupes.StudentAttendedDate >= @StartDate
                                                      AND NotDupes.StudentAttendedDate <= @EndDate THEN NotDupes.ActualDays
                                                 ELSE 0
                                            END
                                        ) / SUM(   CASE WHEN NotDupes.StudentAttendedDate >= @StartDate
                                                             AND NotDupes.StudentAttendedDate <= @EndDate THEN NotDupes.ScheduledDays
                                                        ELSE 0
                                                   END
                                               )
                                   ) * 100
                         END AS AttendancePercentage
                FROM     (
                         SELECT    DISTINCT L.FirstName
                                           ,L.LastName
                                           ,L.MiddleName
                                           ,E.BadgeNumber
                                           ,E.StuEnrollId
                                           ,A.ActualDays
                                           ,A.ScheduledDays
                                           ,A.StudentAttendedDate
                         FROM      dbo.syStudentAttendanceSummary A
                         LEFT JOIN dbo.arStuEnrollments E ON E.StuEnrollId = A.StuEnrollId
                         LEFT JOIN dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
                         LEFT JOIN dbo.adLeadByLeadGroups LG ON LG.LeadId = E.LeadId
                         LEFT JOIN dbo.adLeads L ON L.LeadId = E.LeadId
                         LEFT JOIN dbo.syStatusCodes SC ON SC.StatusCodeId = E.StatusCodeId
                         LEFT JOIN dbo.sySysStatus SS ON SS.SysStatusId = SC.SysStatusId
                         WHERE     (
                                   (
                                   StudentAttendedDate >= @StartDate
                                   AND StudentAttendedDate <= @EndDate
                                   )
                                   OR ( EXISTS (
                                               SELECT LOA.StudentLOAId
                                               FROM   dbo.arStudentLOAs LOA
                                               WHERE  LOA.StartDate <= @EndDate
                                                      AND LOA.EndDate >= @StartDate
                                                      AND LOA.StuEnrollId = E.StuEnrollId
                                               )
                                      )
                                   )
                                   AND (
                                       EXISTS (
                                              SELECT TOP 1 *
                                              FROM   (
                                                     SELECT lastStatusOfYear.StuEnrollId
                                                           ,lastStatusOfYear.SysStatusId
                                                           ,lastStatusOfYear.StatusCodeDescrip
                                                           ,lastStatusOfYear.InSchool
                                                     FROM   (
                                                            SELECT     StuEnrollId
                                                                      ,syStatusCodes.SysStatusId
                                                                      ,StatusCodeDescrip
                                                                      ,DateOfChange
                                                                      ,SS_SS.InSchool
                                                                      ,ROW_NUMBER() OVER ( PARTITION BY StuEnrollId
                                                                                           ORDER BY DateOfChange DESC
                                                                                         ) rn
                                                            FROM       dbo.syStudentStatusChanges
                                                            INNER JOIN dbo.syStatusCodes ON syStatusCodes.StatusCodeId = syStudentStatusChanges.NewStatusId
                                                            LEFT JOIN  dbo.sySysStatus SS_SS ON SS_SS.SysStatusId = syStatusCodes.SysStatusId
                                                            WHERE      DateOfChange <= @EndDate
                                                            ) lastStatusOfYear
                                                     WHERE  rn = 1
                                                     ) laststatusBefore
                                              WHERE  laststatusBefore.InSchool = 1
                                                     AND laststatusBefore.StuEnrollId = E.StuEnrollId
                                              )
                                       OR EXISTS (
                                                 SELECT    *
                                                 FROM      dbo.syStudentStatusChanges
                                                 LEFT JOIN dbo.syStatusCodes ON syStatusCodes.StatusCodeId = syStudentStatusChanges.NewStatusId
                                                 LEFT JOIN dbo.sySysStatus SS_SS ON SS_SS.SysStatusId = syStatusCodes.SysStatusId
                                                 LEFT JOIN dbo.syStatusCodes OSS_SC ON OSS_SC.StatusCodeId = syStudentStatusChanges.OrigStatusId
                                                 LEFT JOIN dbo.sySysStatus OSS_SS ON OSS_SS.SysStatusId = OSS_SC.SysStatusId
                                                 WHERE     StuEnrollId = E.StuEnrollId
                                                           AND (
                                                               DateOfChange <= @EndDate
                                                               AND DateOfChange >= @StartDate
                                                               AND (
                                                                   SS_SS.InSchool = 1
                                                                   OR OSS_SS.InSchool = 1
                                                                   )
                                                               )
                                                 )
                                       )
                                   AND (
                                       (
                                       @Enrollments IS NOT NULL
                                       AND E.StuEnrollId IN ((
                                                             SELECT Val
                                                             FROM   MultipleValuesForReportParameters(@Enrollments, ',', 1)
                                                             )
                                                            )
                                       )
                                       OR (
                                          @Enrollments IS NULL
                                          AND (
                                              (
                                              @ProgramVersions IS NOT NULL
                                              AND PV.PrgVerId IN (
                                                                 SELECT Val
                                                                 FROM   MultipleValuesForReportParameters(@ProgramVersions, ',', 1)
                                                                 )
                                              )
                                              OR (
                                                 @StudentGroups IS NOT NULL
                                                 AND LG.LeadGrpId IN (
                                                                     SELECT Val
                                                                     FROM   MultipleValuesForReportParameters(@StudentGroups, ',', 1)
                                                                     )
                                                 )
                                              )
                                          )
                                       )
                         ) NotDupes
                GROUP BY StuEnrollId
                        ,FirstName
                        ,LastName
                        ,MiddleName
                        ,BadgeNumber;





    INSERT INTO @TotalResults
                SELECT   NotDupes.StuEnrollId
                        ,SUM(NotDupes.ActualDays) AS Actual
                        ,MAX(NotDupes.ActualRunningAbsentDays) AS Absent
                        ,MAX(NotDupes.ActualRunningMakeupDays) AS Makeup
                        ,SUM(NotDupes.ScheduledDays) AS Scheduled
                        ,CASE WHEN SUM(NotDupes.ScheduledDays) = 0 THEN 0
                              ELSE ( SUM(NotDupes.ActualDays) / SUM(NotDupes.ScheduledDays)) * 100
                         END AS AttendancePercentage
                FROM     (
                         SELECT    DISTINCT L.FirstName
                                           ,L.LastName
                                           ,L.MiddleName
                                           ,E.BadgeNumber
                                           ,E.StuEnrollId
                                           ,A.ActualDays
                                           ,A.ScheduledDays
                                           ,A.StudentAttendedDate
                                           ,A.ActualRunningAbsentDays
                                           ,A.ActualRunningMakeupDays
                         FROM      dbo.syStudentAttendanceSummary A
                         LEFT JOIN dbo.arStuEnrollments E ON E.StuEnrollId = A.StuEnrollId
                         LEFT JOIN dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
                         LEFT JOIN dbo.adLeadByLeadGroups LG ON LG.LeadId = E.LeadId
                         LEFT JOIN dbo.adLeads L ON L.LeadId = E.LeadId
                         LEFT JOIN dbo.syStatusCodes SC ON SC.StatusCodeId = E.StatusCodeId
                         LEFT JOIN dbo.sySysStatus SS ON SS.SysStatusId = SC.SysStatusId
                         WHERE     StudentAttendedDate <= @EndDate
                                   AND (
                                       (
                                       @Enrollments IS NOT NULL
                                       AND E.StuEnrollId IN ((
                                                             SELECT Val
                                                             FROM   MultipleValuesForReportParameters(@Enrollments, ',', 1)
                                                             )
                                                            )
                                       )
                                       OR (
                                          @Enrollments IS NULL
                                          AND (
                                              (
                                              @ProgramVersions IS NOT NULL
                                              AND PV.PrgVerId IN (
                                                                 SELECT Val
                                                                 FROM   MultipleValuesForReportParameters(@ProgramVersions, ',', 1)
                                                                 )
                                              )
                                              OR (
                                                 @StudentGroups IS NOT NULL
                                                 AND LG.LeadGrpId IN (
                                                                     SELECT Val
                                                                     FROM   MultipleValuesForReportParameters(@StudentGroups, ',', 1)
                                                                     )
                                                 )
                                              )
                                          )
                                       )
                         ) NotDupes
                GROUP BY StuEnrollId;


    SELECT     R.BadgeNumber AS BadgeNumber
              ,( R.LastName + ', ' + R.FirstName + ' ' + ( CASE WHEN R.MiddleName IS NOT NULL THEN R.MiddleName
                                                                ELSE ''
                                                           END
                                                         )
               ) AS FullName
              ,R.Actual AS Actual
              ,R.Absent AS Absent
              ,R.Makeup AS Makeup
              ,R.Scheduled AS Schedule
              ,R.AttendancePercentage AS AttendancePercentage
              ,T.Actual AS TotalActualToDate
              ,T.Absent AS TotalAbsentToDate
              ,T.Makeup AS TotalMakeupToDate
              ,T.Scheduled AS TotalScheduleToDate
              ,T.AttendancePercentage AS TotalAttendancePercentageToDate
    FROM       @RangeResults R
    INNER JOIN @TotalResults T ON T.StudentEnrollmentId = R.StudentEnrollmentId
    ORDER BY   FullName;




GO
