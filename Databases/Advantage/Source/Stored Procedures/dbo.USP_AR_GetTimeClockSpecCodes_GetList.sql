SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[USP_AR_GetTimeClockSpecCodes_GetList] ( @Status VARCHAR(50) )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Janet Robinson
    
    Create date		:	08/09/2011
    
	Procedure Name	:	[USP_AR_GetTimeClockSpecCodes_GetList]

	Objective		:	Get the list of Time Clock Special Codes from the arTimeClockSpecialCode table
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@Status 		In		varchar			    Required
						
	
	Output			:	Returns the list of Time Clock Special Codes by Status 
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN

        DECLARE @ShowActiveValue NVARCHAR(20);
        IF LOWER(@Status) = 'true'
            BEGIN
                SET @ShowActiveValue = 'Active';
            END;
        ELSE
            BEGIN
                SET @ShowActiveValue = 'Inactive';
            END;

        SELECT  DISTINCT
                CT.TCSpecialCode
        FROM    arTimeClockSpecialCode CT
        INNER JOIN syStatuses ST ON ST.StatusId = CT.StatusId
        WHERE   ST.Status = @ShowActiveValue
        ORDER BY CT.TCSpecialCode;

    END;



GO
