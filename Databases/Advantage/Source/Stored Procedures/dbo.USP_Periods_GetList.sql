SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_Periods_GetList]
    @PrgVerId VARCHAR(8000)
AS
    BEGIN
        SELECT DISTINCT
                PP.PeriodNumber
               ,IncrementValue
               ,CumulativeValue
               ,ChargeAmount
               ,PV.PrgVerDescrip
        FROM    arPrgVersions PV
        INNER JOIN saBillingMethods BM ON PV.BillingMethodId = BM.BillingMethodId
        INNER JOIN saIncrements I ON I.BillingMethodId = BM.BillingMethodId
        INNER JOIN saPmtPeriods PP ON I.IncrementId = PP.IncrementId
        WHERE   PV.PrgVerId IN ( SELECT Val
                                 FROM   MultipleValuesForReportParameters(@PrgVerId,',',1) );
    END;
GO
