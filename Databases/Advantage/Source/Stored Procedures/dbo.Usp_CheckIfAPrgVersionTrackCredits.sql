SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[Usp_CheckIfAPrgVersionTrackCredits]
    @PrgVerId AS UNIQUEIDENTIFIER
AS -- Def Program Version Track Credits it  Credits is > 0.0
    DECLARE @IsIsPrgVersionTrackCredits BIT;
    SET @IsIsPrgVersionTrackCredits = 0;
    BEGIN
        IF EXISTS ( SELECT  1
                    FROM    arPrgVersions AS APV
                    WHERE   APV.PrgVerId = @PrgVerId
                            AND APV.Credits > 0.0 )
            BEGIN
                SET @IsIsPrgVersionTrackCredits = 1; 
            END;
        ELSE
            BEGIN
                SET @IsIsPrgVersionTrackCredits = 0;
            END;
        SELECT  @IsIsPrgVersionTrackCredits;
    END;


GO
