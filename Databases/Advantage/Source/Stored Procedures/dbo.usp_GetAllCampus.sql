SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetAllCampus]
    @showActiveOnly BIT
   ,@campusId VARCHAR(50)
AS
    SET NOCOUNT ON;
    IF @showActiveOnly = 1
        BEGIN
            EXEC usp_GetAllActiveCampus @campusId;
        END; 
    ELSE
        BEGIN
            EXEC usp_GetAllActiveAndInActiveCampus @campusId;
        END; 
               
        





GO
