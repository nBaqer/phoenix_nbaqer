SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


 
CREATE PROCEDURE [dbo].[USP_GetStudentsWithMissingItemsReport_Grad]
    (
     @CampusID UNIQUEIDENTIFIER
    ,@CampGrpID VARCHAR(8000)
    ,@ShiftID VARCHAR(8000) = NULL
    ,@StatusCodeID VARCHAR(8000) = NULL
    ,@LeadGrpID VARCHAR(8000) = NULL 
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	02/03/2011
    
	Procedure Name	:	[USP_GetStudentsWithMissingItemsReport_Grad]

	Objective		:	Get the list of Students who have overrides that are not approved
		
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
					
					
	
	Output			:	Returns the requested details	
						
*/-----------------------------------------------------------------------------------------------------
    BEGIN

      --  DECLARE @StatusID UNIQUEIDENTIFIER
        DECLARE @ActiveStatusID AS UNIQUEIDENTIFIER;
        SET @ActiveStatusID = (
                                SELECT  StatusID
                                FROM    syStatuses
                                WHERE   Status = 'Active'
                              ); 
  
        DECLARE @AllCampGrpId AS UNIQUEIDENTIFIER;
        SET @AllCampGrpId = (
                              SELECT    CAmpGrpID
                              FROM      dbo.syCampGrps
                              WHERE     IsAllCampusGrp = 1
                                        AND CampGrpCode = 'All'
                            );

        SELECT DISTINCT
                syCampGrps.CampGrpId
               ,syCampGrps.CampGrpDescrip
               ,arStuEnrollments.CampusId
               ,(
                  SELECT    CampDescrip
                  FROM      syCampuses
                  WHERE     syCampuses.CampusId = arStuEnrollments.CampusId
                ) AS CampusDescrip
               ,arStuEnrollments.StuEnrollId
               ,getStudents.StudentId
               ,S.LastName
               ,S.FirstName
               ,S.SSN
               ,arStuEnrollments.PrgVerId
               ,PV.PrgVerDescrip
               ,arStuEnrollments.StatusCodeId
               ,SC.StatusCodeDescrip
               ,arStuEnrollments.ShiftId
               ,(
                  SELECT TOP 1
                            ShiftDescrip
                  FROM      arShifts
                  WHERE     ShiftId = arStuEnrollments.ShiftId
                ) AS ShiftDescrip
               ,arStuEnrollments.EdLvlId
               ,(
                  SELECT TOP 1
                            EdLvlDescrip
                  FROM      adEdLvls
                  WHERE     EdLvlId = arStuEnrollments.EdLvlId
                ) AS EdLvlDescrip
               ,adReqId
               ,ReqDescrip
               ,ReqType
               ,NULL AS Required
               ,0 AS Fullfilled
        FROM    (
                  SELECT DISTINCT
                            StudentId
                           ,Descrip AS ReqDescrip
                           ,t1.adReqId
                           ,(
                              SELECT    Descrip
                              FROM      adReqTypes
                              WHERE     adReqTypeId = t1.adReqTypeId
                            ) AS ReqType
                  FROM      (
                              SELECT DISTINCT
                                        adReqId
                                       ,Descrip
                                       ,adReqTypeId
                                       ,CampGrpId
                              FROM      adReqs
                              WHERE     adReqTypeId IN ( 1,3 )
                                        AND ReqforGraduation = 1
                                        AND StatusId = @ActiveStatusID
                            ) t1
                           ,(
                              SELECT DISTINCT
                                        StartDate
                                       ,EndDate
                                       ,MinScore
                                       ,adReqId
                              FROM      adReqsEffectiveDates
                              WHERE     MandatoryRequirement = 1
                                --AND GETDATE() >= StartDate
                                --AND ( GETDATE() <= EndDate
                                --      OR EndDate IS NULL
                                --    )
                            ) t2
                           ,(
                              SELECT DISTINCT
                                        DocumentId
                                       ,DocStatusId
                                       ,StudentId
                                       ,(
                                          SELECT    MIN(StartDate)
                                          FROM      arStuEnrollments
                                          WHERE     StudentID = PL.StudentID
                                          GROUP BY  StudentID
                                        ) AS StudentStartDate
                              FROM      plStudentDocs PL
                              WHERE     DocStatusId IN ( SELECT DISTINCT
                                                                DocStatusId
                                                         FROM   syDocStatuses t2
                                                               ,sySysDocStatuses t3
                                                               ,syCmpGrpCmps t4
                                                         WHERE  t2.SysDocStatusId = t3.SysDocStatusId
                                                                AND t2.CampGrpId = t4.CampGrpId
                                                                AND t4.CampusId = @CampusID
                                                                AND t3.SysDocStatusId = 2
                                                                AND t2.StatusId = @ActiveStatusID )
                            ) t3
                  WHERE     t1.adReqId = t2.adReqId
                            AND t1.adReqId = t3.DocumentId
                            AND StudentStartDate >= StartDate
                            AND (
                                  StudentStartDate <= EndDate
                                  OR EndDate IS NULL
                                )
                            AND t1.CampGrpId IN ( SELECT DISTINCT
                                                            t1.CampGrpId
                                                  FROM      syCmpGrpCmps t1
                                                  WHERE     t1.CampGrpId IN ( (
                                                                                SELECT  strval
                                                                                FROM    dbo.SPLIT(@CampGrpID)
                                                                              )
                                                                              ,@AllCampGrpId
                                                                         ) )
                  UNION
                  SELECT DISTINCT
                            StudentId
                           ,Descrip AS ReqDescrip
                           ,t1.adReqId
                           ,(
                              SELECT    Descrip
                              FROM      adReqTypes
                              WHERE     adReqTypeId = t1.adReqTypeId
                            ) AS ReqType
                  FROM      (
                              SELECT DISTINCT
                                        adReqId
                                       ,Descrip
                                       ,adReqTypeId
                                       ,CampGrpId
                              FROM      adReqs
                              WHERE     adReqTypeId IN ( 1,3 )
                                        AND ReqforGraduation = 1
                                        AND StatusId = @ActiveStatusID
                            ) t1
                           ,(
                              SELECT DISTINCT
                                        StartDate
                                       ,EndDate
                                       ,MinScore
                                       ,adReqId
                              FROM      adReqsEffectiveDates
                              WHERE     MandatoryRequirement = 1
                                --AND GETDATE() >= StartDate
                                --AND ( GETDATE() <= EndDate
                                --      OR EndDate IS NULL
                                --    )
                            ) t2
                           ,(
                              SELECT    EntrTestId
                                       ,StudentId
                                       ,(
                                          SELECT    MIN(StartDate)
                                          FROM      arStuEnrollments
                                          WHERE     StudentID = ET.StudentID
                                          GROUP BY  StudentID
                                        ) AS StudentStartDate
                              FROM      adLeadEntranceTest ET
                              WHERE     Pass = 0
                                        AND StudentId IS NOT NULL
                            ) t4
                  WHERE     t1.adReqId = t2.adReqId
                            AND t1.adReqId = t4.EntrTestId
                            AND StudentStartDate >= StartDate
                            AND (
                                  StudentStartDate <= EndDate
                                  OR EndDate IS NULL
                                )
                            AND t1.CampGrpId IN ( SELECT DISTINCT
                                                            t1.CampGrpId
                                                  FROM      syCmpGrpCmps t1
                                                  WHERE     t1.CampGrpId IN ( (
                                                                                SELECT  strval
                                                                                FROM    dbo.SPLIT(@CampGrpID)
                                                                              )
                                                                              ,@AllCampGrpId
                                                                         ) )
                  UNION
                  SELECT DISTINCT
                            StudentId
                           ,Descrip AS ReqDescrip
                           ,t1.adReqId
                           ,(
                              SELECT    Descrip
                              FROM      adReqTypes
                              WHERE     adReqTypeId = t1.adReqTypeId
                            ) AS ReqType
                  FROM      (
                              SELECT DISTINCT
                                        adReqId
                                       ,Descrip
                                       ,adReqTypeId
                                       ,CampGrpId
                              FROM      adReqs
                              WHERE     adReqTypeId IN ( 1,3 )
                                        AND ReqforGraduation = 1
                                        AND StatusId = @ActiveStatusID
                            ) t1
                           ,(
                              SELECT DISTINCT
                                        StartDate
                                       ,EndDate
                                       ,MinScore
                                       ,adReqId
                              FROM      adReqsEffectiveDates
                              WHERE     MandatoryRequirement = 1
                                --AND GETDATE() >= StartDate
                                --AND ( GETDATE() <= EndDate
                                --      OR EndDate IS NULL
                                --    )
                            ) t2
                           ,(
                              SELECT DISTINCT
                                        EntrTestId
                                       ,StudentId
                                       ,(
                                          SELECT    MIN(StartDate)
                                          FROM      arStuEnrollments
                                          WHERE     StudentID = ETO.StudentID
                                          GROUP BY  StudentID
                                        ) AS StudentStartDate
                              FROM      adEntrTestOverride ETO
                              WHERE     Override = 1
                                        AND StudentId IS NOT NULL
                            ) t4
                  WHERE     t1.adReqId = t2.adReqId
                            AND t1.adReqId = t4.EntrTestId
                            AND StudentStartDate >= StartDate
                            AND (
                                  StudentStartDate <= EndDate
                                  OR EndDate IS NULL
                                )
                            AND t1.CampGrpId IN ( SELECT DISTINCT
                                                            t1.CampGrpId
                                                  FROM      syCmpGrpCmps t1
                                                  WHERE     t1.CampGrpId IN ( (
                                                                                SELECT  strval
                                                                                FROM    dbo.SPLIT(@CampGrpID)
                                                                              )
                                                                              ,@AllCampGrpId
                                                                         ) )
                  UNION
                  SELECT DISTINCT
                            StudentId
                           ,Descrip AS ReqDescrip
                           ,t1.adReqId
                           ,(
                              SELECT    Descrip
                              FROM      adReqTypes
                              WHERE     adReqTypeId = t1.adReqTypeId
                            ) AS ReqType
                  FROM      (
                              SELECT DISTINCT
                                        adReqId
                                       ,Descrip
                                       ,adReqTypeId
                                       ,CampGrpId
                              FROM      adReqs
                              WHERE     adReqTypeId IN ( 1,3 )
                                        AND ReqforGraduation = 1
                                        AND StatusId = @ActiveStatusID
                            ) t1
                           ,(
                              SELECT DISTINCT
                                        StartDate
                                       ,EndDate
                                       ,MinScore
                                       ,adReqId
                              FROM      adReqsEffectiveDates
                              WHERE     MandatoryRequirement <> 1
                                --AND GETDATE() >= StartDate
                                --AND ( GETDATE() <= EndDate
                                --      OR EndDate IS NULL
                                --    )
                            ) t2
                           ,(
                              SELECT DISTINCT
                                        adReqId
                              FROM      adPrgVerTestDetails t1
                                       ,arStuEnrollments t2
                                       ,arStudent t3
                              WHERE     t1.PrgVerId = t2.PrgVerId
                                        AND t2.StudentId = t3.StudentId
                                        AND ReqGrpId IS NULL
                                        AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                                        s1.adReqId
                                                                FROM    adReqGrpDef s1
                                                                       ,adPrgVerTestDetails s2
                                                                       ,arStuEnrollments s3
                                                                       ,arStudent s4
                                                                WHERE   s1.ReqGrpId = s2.ReqGrpId
                                                                        AND s2.PrgVerId = s3.PrgVerId
                                                                        AND s3.StudentId = s4.StudentId
                                                                        AND s2.adReqId IS NULL )
                            ) t5
                           ,(
                              SELECT DISTINCT
                                        DocumentId
                                       ,DocStatusId
                                       ,StudentId
                                       ,(
                                          SELECT    MIN(StartDate)
                                          FROM      arStuEnrollments
                                          WHERE     StudentID = PL.StudentID
                                          GROUP BY  StudentID
                                        ) AS StudentStartDate
                              FROM      plStudentDocs PL
                              WHERE     DocStatusId IN ( SELECT DISTINCT
                                                                DocStatusId
                                                         FROM   syDocStatuses t2
                                                               ,sySysDocStatuses t3
                                                               ,syCmpGrpCmps t4
                                                         WHERE  t2.SysDocStatusId = t3.SysDocStatusId
                                                                AND t2.CampGrpId = t4.CampGrpId
                                                                AND t4.CampusId = @CampusID
                                                                AND t3.SysDocStatusId = 2
                                                                AND t2.StatusId = @ActiveStatusID )
                            ) t6
                  WHERE     t1.adReqId = t2.adReqId
                            AND t1.adReqId = t5.adReqId
                            AND t1.adReqId = t6.DocumentId
                            AND StudentStartDate >= StartDate
                            AND (
                                  StudentStartDate <= EndDate
                                  OR EndDate IS NULL
                                )
                            AND t1.CampGrpId IN ( SELECT DISTINCT
                                                            t1.CampGrpId
                                                  FROM      syCmpGrpCmps t1
                                                  WHERE     t1.CampGrpId IN ( (
                                                                                SELECT  strval
                                                                                FROM    dbo.SPLIT(@CampGrpID)
                                                                              )
                                                                              ,@AllCampGrpId
                                                                         ) )
                  UNION
                  SELECT DISTINCT
                            StudentId
                           ,Descrip AS ReqDescrip
                           ,t1.adReqId
                           ,(
                              SELECT    Descrip
                              FROM      adReqTypes
                              WHERE     adReqTypeId = t1.adReqTypeId
                            ) AS ReqType
                  FROM      (
                              SELECT DISTINCT
                                        adReqId
                                       ,Descrip
                                       ,adReqTypeId
                                       ,CampGrpId
                              FROM      adReqs
                              WHERE     adReqTypeId IN ( 1,3 )
                                        AND ReqforGraduation = 1
                                        AND StatusId = @ActiveStatusID
                            ) t1
                           ,(
                              SELECT DISTINCT
                                        StartDate
                                       ,EndDate
                                       ,MinScore
                                       ,adReqId
                              FROM      adReqsEffectiveDates
                              WHERE     MandatoryRequirement <> 1
                                --AND GETDATE() >= StartDate
                                --AND ( GETDATE() <= EndDate
                                --      OR EndDate IS NULL
                                --    )
                            ) t2
                           ,(
                              SELECT DISTINCT
                                        adReqId
                              FROM      adPrgVerTestDetails t1
                                       ,arStuEnrollments t2
                                       ,arStudent t3
                              WHERE     t1.PrgVerId = t2.PrgVerId
                                        AND t2.StudentId = t3.StudentId
                                        AND ReqGrpId IS NULL
                                        AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                                        s1.adReqId
                                                                FROM    adReqGrpDef s1
                                                                       ,adPrgVerTestDetails s2
                                                                       ,arStuEnrollments s3
                                                                       ,arStudent s4
                                                                WHERE   s1.ReqGrpId = s2.ReqGrpId
                                                                        AND s2.PrgVerId = s3.PrgVerId
                                                                        AND s3.StudentId = s4.StudentId
                                                                        AND s2.adReqId IS NULL )
                            ) t5
                           ,(
                              SELECT DISTINCT
                                        EntrTestId
                                       ,StudentId
                                       ,(
                                          SELECT    MIN(StartDate)
                                          FROM      arStuEnrollments
                                          WHERE     StudentID = ET.StudentID
                                          GROUP BY  StudentID
                                        ) AS StudentStartDate
                              FROM      adLeadEntranceTest ET
                              WHERE     Pass = 0
                                        AND StudentId IS NOT NULL
                            ) t7
                  WHERE     t1.adReqId = t2.adReqId
                            AND t1.adReqId = t5.adReqId
                            AND t1.adReqId = t7.EntrTestId
                            AND StudentStartDate >= StartDate
                            AND (
                                  StudentStartDate <= EndDate
                                  OR EndDate IS NULL
                                )
                            AND t1.CampGrpId IN ( SELECT DISTINCT
                                                            t1.CampGrpId
                                                  FROM      syCmpGrpCmps t1
                                                  WHERE     t1.CampGrpId IN ( (
                                                                                SELECT  strval
                                                                                FROM    dbo.SPLIT(@CampGrpID)
                                                                              )
                                                                              ,@AllCampGrpId
                                                                         ) )
                  UNION
                  SELECT DISTINCT
                            StudentId
                           ,Descrip AS ReqDescrip
                           ,t1.adReqId
                           ,(
                              SELECT    Descrip
                              FROM      adReqTypes
                              WHERE     adReqTypeId = t1.adReqTypeId
                            ) AS ReqType
                  FROM      (
                              SELECT DISTINCT
                                        adReqId
                                       ,Descrip
                                       ,adReqTypeId
                                       ,CampGrpId
                              FROM      adReqs
                              WHERE     adReqTypeId IN ( 1,3 )
                                        AND ReqforGraduation = 1
                                        AND StatusId = @ActiveStatusID
                            ) t1
                           ,(
                              SELECT DISTINCT
                                        StartDate
                                       ,EndDate
                                       ,MinScore
                                       ,adReqId
                              FROM      adReqsEffectiveDates
                              WHERE     MandatoryRequirement <> 1
                                --AND GETDATE() >= StartDate
                                --AND ( GETDATE() <= EndDate
                                --      OR EndDate IS NULL
                                --    )
                            ) t2
                           ,(
                              SELECT DISTINCT
                                        adReqId
                              FROM      adPrgVerTestDetails t1
                                       ,arStuEnrollments t2
                                       ,arStudent t3
                              WHERE     t1.PrgVerId = t2.PrgVerId
                                        AND t2.StudentId = t3.StudentId
                                        AND ReqGrpId IS NULL
                                        AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                                        s1.adReqId
                                                                FROM    adReqGrpDef s1
                                                                       ,adPrgVerTestDetails s2
                                                                       ,arStuEnrollments s3
                                                                       ,arStudent s4
                                                                WHERE   s1.ReqGrpId = s2.ReqGrpId
                                                                        AND s2.PrgVerId = s3.PrgVerId
                                                                        AND s3.StudentId = s4.StudentId
                                                                        AND s2.adReqId IS NULL )
                            ) t5
                           ,(
                              SELECT DISTINCT
                                        EntrTestId
                                       ,StudentId
                                       ,(
                                          SELECT    MIN(StartDate)
                                          FROM      arStuEnrollments
                                          WHERE     StudentID = ETO.StudentID
                                          GROUP BY  StudentID
                                        ) AS StudentStartDate
                              FROM      adEntrTestOverride ETO
                              WHERE     Override = 1
                                        AND StudentId IS NOT NULL
                            ) t7
                  WHERE     t1.adReqId = t2.adReqId
                            AND t1.adReqId = t5.adReqId
                            AND t1.adReqId = t7.EntrTestId
                            AND StudentStartDate >= StartDate
                            AND (
                                  StudentStartDate <= EndDate
                                  OR EndDate IS NULL
                                )
                            AND t1.CampGrpId IN ( SELECT DISTINCT
                                                            t1.CampGrpId
                                                  FROM      syCmpGrpCmps t1
                                                  WHERE     t1.CampGrpId IN ( (
                                                                                SELECT  strval
                                                                                FROM    dbo.SPLIT(@CampGrpID)
                                                                              )
                                                                              ,@AllCampGrpId
                                                                         ) )
                  UNION
                  SELECT DISTINCT
                            StudentId
                           ,Descrip AS ReqDescrip
                           ,t1.adReqId
                           ,(
                              SELECT    Descrip
                              FROM      adReqTypes
                              WHERE     adReqTypeId = t1.adReqTypeId
                            ) AS ReqType
                  FROM      (
                              SELECT DISTINCT
                                        adReqId
                                       ,Descrip
                                       ,adReqTypeId
                                       ,CampGrpId
                              FROM      adReqs
                              WHERE     adReqTypeId IN ( 1,3 )
                                        AND ReqforGraduation = 1
                                        AND StatusId = @ActiveStatusID
                            ) t1
                           ,(
                              SELECT DISTINCT
                                        StartDate
                                       ,EndDate
                                       ,MinScore
                                       ,adReqId
                                       ,adReqEffectiveDateId
                              FROM      adReqsEffectiveDates
                              WHERE     MandatoryRequirement <> 1
                                --AND GETDATE() >= StartDate
                                --AND ( GETDATE() <= EndDate
                                --      OR EndDate IS NULL
                                --    )
                            ) t2
                           ,adReqLeadGroups t3
                           ,(
                              SELECT DISTINCT
                                        DocumentId
                                       ,DocStatusId
                                       ,StudentId
                                       ,(
                                          SELECT    MIN(StartDate)
                                          FROM      arStuEnrollments
                                          WHERE     StudentID = PL.StudentID
                                          GROUP BY  StudentID
                                        ) AS StudentStartDate
                              FROM      plStudentDocs PL
                              WHERE     DocStatusId IN ( SELECT DISTINCT
                                                                DocStatusId
                                                         FROM   syDocStatuses t2
                                                               ,sySysDocStatuses t3
                                                               ,syCmpGrpCmps t4
                                                         WHERE  t2.SysDocStatusId = t3.SysDocStatusId
                                                                AND t2.CampGrpId = t4.CampGrpId
                                                                AND t4.CampusId = @CampusID
                                                                AND t3.SysDocStatusId = 2
                                                                AND t2.StatusId = @ActiveStatusID )
                            ) t4
                  WHERE     t1.adReqId = t2.adReqId
                            AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                            AND StudentStartDate >= StartDate
                            AND (
                                  StudentStartDate <= EndDate
                                  OR EndDate IS NULL
                                )
                            AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                            s1.adReqId
                                                    FROM    adReqGrpDef s1
                                                           ,adPrgVerTestDetails s2
                                                           ,arStuEnrollments s3
                                                           ,arStudent s4
                                                    WHERE   s1.ReqGrpId = s2.ReqGrpId
                                                            AND s2.PrgVerId = s3.PrgVerId
                                                            AND s3.StudentId = s4.StudentId
                                                            AND s2.adReqId IS NULL )
                            AND t3.LeadGrpId IN ( SELECT DISTINCT
                                                            LeadGrpId
                                                  FROM      adLeadByLeadGroups t1
                                                           ,arStuEnrollments t2
                                                           ,arStudent t3
                                                  WHERE     t1.StuEnrollId = t2.StuEnrollId
                                                            AND t2.StudentId = t3.StudentId
                                                            AND (
                                                                  t1.leadgrpId IN ( SELECT  strval
                                                                                    FROM    dbo.SPLIT(@LeadGrpID) )
                                                                  OR @LeadGrpID IS NULL
                                                                ) )
                            AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                            adReqId
                                                    FROM    adPrgVerTestDetails t1
                                                           ,arStuEnrollments t2
                                                           ,arStudent t3
                                                    WHERE   t1.PrgVerId = t2.PrgVerId
                                                            AND t2.StudentId = t3.StudentId
                                                            AND ReqGrpId IS NULL )
                            AND t1.CampGrpId IN ( SELECT DISTINCT
                                                            t1.CampGrpId
                                                  FROM      syCmpGrpCmps t1
                                                  WHERE     t1.CampGrpId IN ( (
                                                                                SELECT  strval
                                                                                FROM    dbo.SPLIT(@CampGrpID)
                                                                              )
                                                                              ,@AllCampGrpId
                                                                         ) )
                            AND t3.IsRequired = 1
                            AND t1.adReqId = t4.DocumentId
                  UNION
                  SELECT DISTINCT
                            StudentId
                           ,Descrip AS ReqDescrip
                           ,t1.adReqId AS adReqId
                           ,(
                              SELECT    Descrip
                              FROM      adReqTypes
                              WHERE     adReqTypeId = t1.adReqTypeId
                            ) AS ReqType
                  FROM      (
                              SELECT DISTINCT
                                        adReqId
                                       ,Descrip
                                       ,adReqTypeId
                                       ,CampGrpId
                              FROM      adReqs
                              WHERE     adReqTypeId IN ( 1,3 )
                                        AND ReqforGraduation = 1
                                        AND StatusId = @ActiveStatusID
                            ) t1
                           ,(
                              SELECT DISTINCT
                                        StartDate
                                       ,EndDate
                                       ,MinScore
                                       ,adReqId
                                       ,adReqEffectiveDateId
                              FROM      adReqsEffectiveDates
                              WHERE     MandatoryRequirement <> 1
                                --AND GETDATE() >= StartDate
                                --AND ( GETDATE() <= EndDate
                                --      OR EndDate IS NULL
                                --    )
                            ) t2
                           ,adReqLeadGroups t3
                           ,(
                              SELECT    EntrTestId
                                       ,StudentId
                                       ,(
                                          SELECT    MIN(StartDate)
                                          FROM      arStuEnrollments
                                          WHERE     StudentID = ET.StudentID
                                          GROUP BY  StudentID
                                        ) AS StudentStartDate
                              FROM      adLeadEntranceTest ET
                              WHERE     Pass = 0
                                        AND StudentId IS NOT NULL
                            ) t5
                  WHERE     t1.adReqId = t2.adReqId
                            AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                            AND StudentStartDate >= StartDate
                            AND (
                                  StudentStartDate <= EndDate
                                  OR EndDate IS NULL
                                )
                            AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                            s1.adReqId
                                                    FROM    adReqGrpDef s1
                                                           ,adPrgVerTestDetails s2
                                                           ,arStuEnrollments s3
                                                           ,arStudent s4
                                                    WHERE   s1.ReqGrpId = s2.ReqGrpId
                                                            AND s2.PrgVerId = s3.PrgVerId
                                                            AND s3.StudentId = s4.StudentId
                                                            AND s2.adReqId IS NULL )
                            AND t3.LeadGrpId IN ( SELECT DISTINCT
                                                            LeadGrpId
                                                  FROM      adLeadByLeadGroups t1
                                                           ,arStuEnrollments t2
                                                           ,arStudent t3
                                                  WHERE     t1.StuEnrollId = t2.StuEnrollId
                                                            AND t2.StudentId = t3.StudentId
                                                            AND (
                                                                  t1.leadgrpId IN ( SELECT  strval
                                                                                    FROM    dbo.SPLIT(@LeadGrpID) )
                                                                  OR @LeadGrpID IS NULL
                                                                ) )
                            AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                            adReqId
                                                    FROM    adPrgVerTestDetails t1
                                                           ,arStuEnrollments t2
                                                           ,arStudent t3
                                                    WHERE   t1.PrgVerId = t2.PrgVerId
                                                            AND t2.StudentId = t3.StudentId
                                                            AND ReqGrpId IS NULL )
                            AND t1.CampGrpId IN ( SELECT DISTINCT
                                                            t1.CampGrpId
                                                  FROM      syCmpGrpCmps t1
                                                  WHERE     t1.CampGrpId IN ( (
                                                                                SELECT  strval
                                                                                FROM    dbo.SPLIT(@CampGrpID)
                                                                              )
                                                                              ,@AllCampGrpId
                                                                         ) )
                            AND t3.IsRequired = 1
                            AND t1.adReqId = t5.EntrTestId
                  UNION
                  SELECT DISTINCT
                            StudentId
                           ,Descrip AS ReqDescrip
                           ,t1.adReqId AS adReqId
                           ,(
                              SELECT    Descrip
                              FROM      adReqTypes
                              WHERE     adReqTypeId = t1.adReqTypeId
                            ) AS ReqType
                  FROM      (
                              SELECT DISTINCT
                                        adReqId
                                       ,Descrip
                                       ,adReqTypeId
                                       ,CampGrpId
                              FROM      adReqs
                              WHERE     adReqTypeId IN ( 1,3 )
                                        AND ReqforGraduation = 1
                                        AND StatusId = @ActiveStatusID
                            ) t1
                           ,(
                              SELECT DISTINCT
                                        StartDate
                                       ,EndDate
                                       ,MinScore
                                       ,adReqId
                                       ,adReqEffectiveDateId
                              FROM      adReqsEffectiveDates
                              WHERE     MandatoryRequirement <> 1
                                --AND GETDATE() >= StartDate
                                --AND ( GETDATE() <= EndDate
                                --      OR EndDate IS NULL
                                --    )
                            ) t2
                           ,adReqLeadGroups t3
                           ,(
                              SELECT DISTINCT
                                        EntrTestId
                                       ,StudentId
                                       ,(
                                          SELECT    MIN(StartDate)
                                          FROM      arStuEnrollments
                                          WHERE     StudentID = ETO.StudentID
                                          GROUP BY  StudentID
                                        ) AS StudentStartDate
                              FROM      adEntrTestOverride ETO
                              WHERE     Override = 1
                                        AND StudentId IS NOT NULL
                            ) t5
                  WHERE     t1.adReqId = t2.adReqId
                            AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                            AND StudentStartDate >= StartDate
                            AND (
                                  StudentStartDate <= EndDate
                                  OR EndDate IS NULL
                                )
                            AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                            s1.adReqId
                                                    FROM    adReqGrpDef s1
                                                           ,adPrgVerTestDetails s2
                                                           ,arStuEnrollments s3
                                                           ,arStudent s4
                                                    WHERE   s1.ReqGrpId = s2.ReqGrpId
                                                            AND s2.PrgVerId = s3.PrgVerId
                                                            AND s3.StudentId = s4.StudentId
                                                            AND s2.adReqId IS NULL )
                            AND t3.LeadGrpId IN ( SELECT DISTINCT
                                                            LeadGrpId
                                                  FROM      adLeadByLeadGroups t1
                                                           ,arStuEnrollments t2
                                                           ,arStudent t3
                                                  WHERE     t1.StuEnrollId = t2.StuEnrollId
                                                            AND t2.StudentId = t3.StudentId
                                                            AND (
                                                                  t1.leadgrpId IN ( SELECT  strval
                                                                                    FROM    dbo.SPLIT(@LeadGrpID) )
                                                                  OR @LeadGrpID IS NULL
                                                                ) )
                            AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                            adReqId
                                                    FROM    adPrgVerTestDetails t1
                                                           ,arStuEnrollments t2
                                                           ,arStudent t3
                                                    WHERE   t1.PrgVerId = t2.PrgVerId
                                                            AND t2.StudentId = t3.StudentId
                                                            AND ReqGrpId IS NULL )
                            AND t1.CampGrpId IN ( SELECT DISTINCT
                                                            t1.CampGrpId
                                                  FROM      syCmpGrpCmps t1
                                                  WHERE     t1.CampGrpId IN ( (
                                                                                SELECT  strval
                                                                                FROM    dbo.SPLIT(@CampGrpID)
                                                                              )
                                                                              ,@AllCampGrpId
                                                                         ) )
                            AND t3.IsRequired = 1
                            AND t1.adReqId = t5.EntrTestId
                  UNION
                  SELECT DISTINCT
                            StudentId
                           ,Descrip AS ReqDescrip
                           ,ReqGrpId AS adReqId
                           ,'Requirement Group' AS ReqType
                  FROM      (
                              SELECT    ReqGrpId
                                       ,R5.Descrip
                                       ,R5.NumReqs
                                       ,ISNULL(R5.TestPassed,0) + ISNULL(R5.DocsApproved,0) AS AttemptedReqs
                                       ,CASE WHEN LeadGrpId IS NULL THEN '00000000-0000-0000-0000-000000000000'
                                             ELSE LeadGrpId
                                        END AS LeadGrpId
                                       ,StudentId
                              FROM      (
                                          SELECT  DISTINCT
                                                    t1.ReqGrpId
                                                   ,t2.Descrip
                                                   ,t2.CampGrpId
                                                   ,t3.NumReqs AS Numreqs
                                                   ,t6.StudentId
                                                   ,(
                                                      SELECT    COUNT(*)
                                                      FROM      (
                                                                  SELECT DISTINCT
                                                                            A2.adReqId
                                                                           ,
                                                              --GETDATE() AS CurrentDate ,
                                                                            A3.StartDate
                                                                           ,A3.EndDate
                                                                           ,A4.LeadGrpId
                                                                  FROM      adReqGrpDef A1
                                                                           ,adReqs A2
                                                                           ,adReqsEffectiveDates A3
                                                                           ,adReqLeadGroups A4
                                                                  WHERE     A1.adReqId = A2.adReqId
                                                                            AND A2.adReqId = A3.adReqId
                                                                            AND A3.adReqEffectiveDateId = A4.adReqEffectiveDateId
                                                                            AND A4.LeadGrpId IN (
                                                                            SELECT DISTINCT
                                                                                    LeadGrpId
                                                                            FROM    adLeadByLeadGroups t1
                                                                                   ,arStuEnrollments t2
                                                                                   ,arStudent t3
                                                                            WHERE   t1.StuEnrollId = t2.StuEnrollId
                                                                                    AND t2.StudentId = t3.StudentId
                                                                                    AND (
                                                                                          t1.leadgrpId IN ( SELECT  strval
                                                                                                            FROM    dbo.SPLIT(@LeadGrpID) )
                                                                                          OR @LeadGrpID IS NULL
                                                                                        ) )
                                                                            AND ReqforGraduation = 1
                                                                            AND ReqGrpId = t1.ReqGrpId
                                                                            AND A2.adreqTypeId = 1
                                                                ) R1
                                                               ,adLeadEntranceTest R2
                                                               ,(
                                                                  SELECT    MIN(StartDate) AS StudentStartDAte
                                                                  FROM      arStuEnrollments
                                                                  WHERE     StudentId = t6.studentID
                                                                  GROUP BY  StudentID
                                                                ) AS r3
                                                      WHERE     R1.adReqId = R2.EntrTestId
                                                                AND R2.StudentId = t6.StudentId
                                                        --AND R1.CurrentDate >= R1.StartDate
                                                                AND r3.StudentStartDAte >= R1.StartDate
                                                                AND LEN(R2.TestTaken) >= 4
                                                                AND R2.Pass = 1
                                                                AND R2.EntrTestId NOT IN ( SELECT DISTINCT
                                                                                                    EntrTestId
                                                                                           FROM     adEntrTestOverRide t1
                                                                                           WHERE    t1.StudentId = t6.StudentId
                                                                                                    AND OverRide = 1 )
                                                        --AND ( R1.CurrentDate <= R1.EndDate
                                                        --      OR R1.EndDate IS NULL
                                                        --    )
                                                                AND (
                                                                      r3.StudentStartDAte <= R1.EndDate
                                                                      OR R1.EndDate IS NULL
                                                                    )
                                                    ) AS TestPassed
                                                   ,(
                                                      SELECT    COUNT(*)
                                                      FROM      (
                                                                  SELECT DISTINCT
                                                                            A2.adReqId
                                                                           ,
                                                             -- GETDATE() AS CurrentDate ,
                                                                            A3.StartDate
                                                                           ,A3.EndDate
                                                                           ,A4.LeadGrpId
                                                                  FROM      adReqGrpDef A1
                                                                           ,adReqs A2
                                                                           ,adReqsEffectiveDates A3
                                                                           ,adReqLeadGroups A4
                                                                  WHERE     A1.adReqId = A2.adReqId
                                                                            AND A2.adReqId = A3.adReqId
                                                                            AND A3.adReqEffectiveDateId = A4.adReqEffectiveDateId
                                                                            AND A4.LeadGrpId IN (
                                                                            SELECT DISTINCT
                                                                                    LeadGrpId
                                                                            FROM    adLeadByLeadGroups t1
                                                                                   ,arStuEnrollments t2
                                                                                   ,arStudent t3
                                                                            WHERE   t1.StuEnrollId = t2.StuEnrollId
                                                                                    AND t2.StudentId = t3.StudentId
                                                                                    AND (
                                                                                          t1.leadgrpId IN ( SELECT  strval
                                                                                                            FROM    dbo.SPLIT(@LeadGrpID) )
                                                                                          OR @LeadGrpID IS NULL
                                                                                        ) )
                                                                            AND ReqforGraduation = 1
                                                                            AND ReqGrpId = t1.ReqGrpId
                                                                            AND A2.adreqTypeId = 3
                                                                ) R1
                                                               ,plStudentDocs R2
                                                               ,syDocStatuses R3
                                                               ,(
                                                                  SELECT    MIN(StartDate) AS StudentStartDAte
                                                                  FROM      arStuEnrollments
                                                                  WHERE     StudentId = t6.studentID
                                                                  GROUP BY  StudentID
                                                                ) AS R4
                                                      WHERE     R1.adReqId = R2.DocumentId
                                                                AND R2.StudentId = t6.StudentId
                                                                AND R2.DocumentId NOT IN ( SELECT DISTINCT
                                                                                                    EntrTestId
                                                                                           FROM     adEntrTestOverRide t1
                                                                                           WHERE    t1.StudentId = t6.StudentId
                                                                                                    AND OverRide = 1 )
                                                                AND R2.DocStatusId = R3.DocStatusId
                                                                AND R3.SysDocStatusId = 1
                                                        --AND R1.CurrentDate >= R1.StartDate
                                                        --AND ( R1.CurrentDate <= R1.EndDate
                                                        --      OR R1.EndDate IS NULL
                                                        --    )
                                                                AND R4.StudentStartDAte >= R1.StartDate
                                                                AND (
                                                                      R4.StudentStartDAte <= R1.EndDate
                                                                      OR R1.EndDate IS NULL
                                                                    )
                                                    ) AS DocsApproved
                                                   ,t4.LeadGrpId
                                          FROM      adPrgVerTestDetails t1
                                                   ,adReqGroups t2
                                                   ,adLeadGrpReqGroups t3
                                                   ,adLeadGroups t4
                                                   ,arStuEnrollments t5
                                                   ,arStudent t6
                                          WHERE     t1.ReqGrpId = t2.ReqGrpId
                                                    AND t2.ReqGrpId = t3.ReqGrpId
                                                    AND t3.LeadGrpId = t4.LeadGrpId
                                                    AND t1.PrgVerId = t5.PrgVerId
                                                    AND t5.StudentId = t6.StudentId
                                                    AND t2.IsMandatoryReqGrp <> 1
                                                    AND t3.LeadGrpId IN ( SELECT DISTINCT
                                                                                    LeadGrpId
                                                                          FROM      adLeadByLeadGroups t1
                                                                                   ,arStuEnrollments t2
                                                                                   ,arStudent t3
                                                                          WHERE     t1.StuEnrollId = t2.StuEnrollId
                                                                                    AND t2.StudentId = t3.StudentId )
                                        ) R5
                              WHERE     R5.CampGrpId IN ( SELECT DISTINCT
                                                                    t1.CampGrpId
                                                          FROM      syCmpGrpCmps t1
                                                          WHERE     t1.CampGrpId IN ( (
                                                                                        SELECT  strval
                                                                                        FROM    dbo.SPLIT(@CampGrpID)
                                                                                      )
                                                                                      ,@AllCampGrpId
                                                                                 ) )
                            ) R8
                  WHERE     (
                              AttemptedReqs < NumReqs
                              OR (
                                   SELECT   COUNT(*)
                                   FROM     adReqGrpDef
                                   WHERE    ReqGrpId = R8.ReqGrpId
                                            AND LeadGrpId IN ( SELECT   LeadGrpId
                                                               FROM     adLeadByLeadGroups
                                                               WHERE    StuEnrollId IN ( SELECT DISTINCT
                                                                                                StuEnrollId
                                                                                         FROM   arStuEnrollments
                                                                                         WHERE  StudentId = R8.StudentId ) )
                                            AND IsRequired = 1
                                            AND (
                                                  adReqId IN ( SELECT DISTINCT
                                                                        EntrTestId
                                                               FROM     adLeadEntranceTest
                                                               WHERE    StudentId = R8.StudentId
                                                                        AND Pass = 0
                                                                        AND EntrTestId NOT IN ( SELECT  EntrTestId
                                                                                                FROM    adEntrTestOverride
                                                                                                WHERE   StudentId = R8.StudentId
                                                                                                        AND override = 1 ) )
                                                  OR adReqId IN ( SELECT DISTINCT
                                                                            DocumentId
                                                                  FROM      plStudentDocs t1
                                                                           ,syDocStatuses t2
                                                                  WHERE     t1.StudentId = R8.StudentId
                                                                            AND t1.DocStatusId = t2.DocStatusId
                                                                            AND t2.SysDocStatusId <> 1
                                                                            AND DocumentId NOT IN ( SELECT  EntrTestId
                                                                                                    FROM    adEntrTestOverride
                                                                                                    WHERE   StudentId = R8.StudentId
                                                                                                            AND override = 1 ) )
                                                  OR adReqId IN ( SELECT DISTINCT
                                                                            EntrTestId
                                                                  FROM      adEntrTestOverride
                                                                  WHERE     StudentId = R8.StudentId
                                                                            AND override = 1 )
                                                )
                                 ) >= 1
                            )
                ) getStudents
               ,arStudent S
               ,arStuEnrollments
               ,adLeadByLeadGroups LBLG
               ,syStatusCodes SC
               ,arPrgVersions PV
               ,arPrograms P
               ,syCampGrps
               ,adLeadGroups
        WHERE   getStudents.StudentId = S.StudentId
                AND S.StudentId = arStuEnrollments.StudentId
                AND arStuEnrollments.StatusCodeId = SC.StatusCodeId
                AND arStuEnrollments.StuEnrollId = LBLG.StuEnrollId
                AND LBLG.LeadGrpId = adLeadGroups.LeadGrpId
                AND P.ProgId = PV.ProgId
                AND PV.PrgVerId = arStuEnrollments.PrgVerId
                AND arStuEnrollments.CampusId IN ( SELECT DISTINCT
                                                            t1.CampusId
                                                   FROM     syCmpGrpCmps t1
                                                   WHERE    t1.CampGrpId IN ( (
                                                                                SELECT  strval
                                                                                FROM    dbo.SPLIT(@CampGrpID)
                                                                              )
                                                                         ) )
                AND (
                      adReqId IN ( SELECT DISTINCT
                                            t1.adReqId
                                   FROM     adReqs t1
                                           ,adReqsEffectiveDates t2
                                   WHERE    t1.adReqId = t2.adReqId
                                            AND MandatoryRequirement = 1
                                            AND ReqforGraduation = 1 )
                      OR adReqId IN ( SELECT DISTINCT
                                                adReqId
                                      FROM      adPrgVerTestDetails
                                      WHERE     PrgVerId = arStuEnrollments.PrgVerId
                                                AND adReqId IS NOT NULL )
                      OR adReqId IN ( SELECT DISTINCT
                                                PV.ReqGrpId
                                      FROM      adPrgVerTestDetails PV
                                               ,dbo.adReqGrpDef RG
                                               ,dbo.adReqs R
                                      WHERE     PV.ReqGrpId = RG.ReqGrpId
                                                AND RG.adreqID = R.adReqId
                                                AND PrgVerId = arStuEnrollments.PrgVerId
                                                AND R.ReqforGraduation = 1
                                                AND PV.ReqGrpId IS NOT NULL )
                      OR adReqId IN ( SELECT DISTINCT
                                                t1.adReqId
                                      FROM      adReqs t1
                                               ,adReqsEffectiveDates t2
                                               ,adReqLeadGroups t3
                                      WHERE     t1.adReqId = t2.adReqId
                                                AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                                                AND t3.IsRequired = 1
                                                AND t3.LeadgrpId IN ( SELECT DISTINCT
                                                                                LeadGrpId
                                                                      FROM      adLeadByLeadGroups
                                                                      WHERE     StuEnrollId = arStuEnrollments.StuEnrollId )
                                                AND ReqforGraduation = 1 )
                    )
                AND syCampGrps.CampGrpId IN ( SELECT DISTINCT
                                                        t1.CampGrpId
                                              FROM      syCmpGrpCmps t1
                                              WHERE     t1.CampGrpId IN ( SELECT    strval
                                                                          FROM      dbo.SPLIT(@CampGrpID) ) )
                AND (
                      adLeadGroups.LeadGrpId IN ( SELECT    strval
                                                  FROM      dbo.SPLIT(@LeadGrpID) )
                      OR @LeadGrpID IS NULL
                    )
                AND (
                      arStuEnrollments.statuscodeid IN ( SELECT strval
                                                         FROM   dbo.SPLIT(@StatusCodeID) )
                      OR @StatusCodeID IS NULL
                    )
                AND (
                      arStuEnrollments.shiftid IN ( SELECT  strval
                                                    FROM    dbo.SPLIT(@ShiftID) )
                      OR @ShiftID IS NULL
                    )
        ORDER BY syCampGrps.CampGrpId
               ,arStuEnrollments.CampusID
               ,LastName
               ,FirstName;
    END;






GO
