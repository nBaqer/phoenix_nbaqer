SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[USP_GetUserslist_ForwhomtheUserCanAssignTasksTo]
    (
     @UserID AS VARCHAR(50)
    ,@RoleID AS VARCHAR(8000) = NULL
    ,@CampusDesc AS VARCHAR(8000) = NULL
    ,@Fullname AS VARCHAR(100) = NULL
    )
AS
    SET NOCOUNT ON;
    BEGIN 

        SELECT DISTINCT
                UserName
               ,FullName
               ,S.CampusID
               ,CampDescrip
               ,S.UserID
        FROM    SyUsers S
               ,dbo.syCampuses SC
               ,dbo.syUsersRolesCampGrps SR
        WHERE   S.CAmpusID = SC.CampusId
                AND SR.UserId = S.USerID
                AND (
                      SR.Roleid IN ( SELECT strval
                                     FROM   dbo.SPLIT(@RoleID) )
                      OR @RoleID IS NULL
                    )
                AND S.USerID IN ( SELECT    USR.UserID
                                  FROM      dbo.syUsersRolesCampGrps USR
                                           ,tmPermissions TMP
                                  WHERE     USR.RoleId = TMP.RoleId
                                            AND USR.CampGrpId IN ( SELECT DISTINCT
                                                                            campgrpId
                                                                   FROM     dbo.syCmpGrpCmps
                                                                   WHERE    CampusId IN ( SELECT    CampusID
                                                                                          FROM      dbo.syCmpGrpCmps
                                                                                          WHERE     CampGrpId = TMP.CampGrpId ) )
                                            AND TMP.UserID = @UserID )
                AND (
                      @CampusDesc IS NULL
                      OR CampDescrip IN ( SELECT    strval
                                          FROM      dbo.SPLIT(@CampusDesc) )
                    )
                AND (
                      @Fullname IS NULL
                      OR FullNAME LIKE @Fullname
                    )
                AND Username <> 'super'
                AND UserName <> 'support'
                AND S.UserID <> @UserID
        ORDER BY Fullname;                               

    END;






GO
