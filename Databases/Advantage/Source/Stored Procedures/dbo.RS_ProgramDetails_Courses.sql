SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[RS_ProgramDetails_Courses]
	-- Add the parameters for the stored procedure here
    @PrgVerId VARCHAR(36) = '5FEF3CF7-5B12-4942-A1F1-8B87E01997C9' -- Electrolysis (historical)
   ,@CampusId VARCHAR(36) = 'DC42A60A-5EB9-49B6-ADF6-535966F2E34A' -- Hieleach
   ,@StatusId VARCHAR(36) = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Active
   ,@CoursesId VARCHAR(MAX) = '5BA9B098-D3D8-451D-A638-64E8C72EE425,BBD4989F-07E3-49CD-85DA-EC73C008EC32,7CCA4B9A-D5F2-46AA-A125-1C5B07FE6B4B,8D02B5EB-F46D-43B9-802E-190BD56AB0E0,00A65FB1-9E3B-4CFE-AF49-D5A8DB968EE3,34AF9A35-7927-410F-A51F-C959F96EBF9A,DF
4918B9-1495-43ED-B698-42C4DFE5F92D,D38E6886-65BE-46B6-9B28-1A08DC279EA2,AC5FCE97-92BC-4D6C-B358-FBC37A9F845B,8F136D0C-C726-44C1-BF18-A00D500F79EC'
AS
    BEGIN
        SET NOCOUNT ON;
	-- Security delete any preexist tempo table.
	--IF OBJECT_ID(N'tempdb..@MYTempoTable','U') IS NOT NULL DROP TABLE @MYTempoTable;
        IF OBJECT_ID(N'tempdb..#EfectivesDates','U') IS NOT NULL
            DROP TABLE #EfectivesDates;
	--IF OBJECT_ID(N'tempdb..@ResultTable', 'U') IS NOT NULL DROP TABLE @ResultTable;

	-- Process Parameters to Guid
        DECLARE @PrgVerGuid UNIQUEIDENTIFIER = @PrgVerId; 
        DECLARE @CampusGuid UNIQUEIDENTIFIER = @CampusId;

	-- Process Courses Id Parameter
        IF @CoursesId = ''
            SET @CoursesId = NULL;

-- Try
        BEGIN TRY
-- 1 Select Courses for program version and CampusId
            DECLARE @MYTempoTable TABLE
                (
                 IsRequired BIT
                ,Code VARCHAR(15)
                ,CourseDescript VARCHAR(100)
                ,Credits DECIMAL
                ,Hours DECIMAL(10,2)
                ,Weight DECIMAL(10,2)
                ,GradeDescript VARCHAR(50)
                ,EffectiveDate DATETIME
                ,Number DECIMAL
                ,Resource VARCHAR(200)
                ,ResourceID SMALLINT
                ,FinAidCredits DECIMAL
                );

            DECLARE @ResultTable TABLE
                (
                 IsRequired BIT
                ,Code VARCHAR(15)
                ,CourseDescript VARCHAR(100)
                ,Credits DECIMAL
                ,Hours DECIMAL(10,2)
                ,Weight DECIMAL(10,2)
                ,GradeDescript VARCHAR(50)
                ,EffectiveDate DATETIME
                ,Number DECIMAL
                ,Resource VARCHAR(200)
                ,ResourceID SMALLINT
                ,FinAidCredits DECIMAL
                );

            INSERT  INTO @MYTempoTable
                    EXEC dbo.RS_ProgramDetails_CoursesAllGrades @CampusId,@PrgVerId,@StatusId,@CoursesId;
  
-- 2 Get the list of Different EffectiveData in the query
            SELECT DISTINCT
                    EffectiveDate
            INTO    #EfectivesDates
            FROM    @MYTempoTable
            ORDER BY EffectiveDate DESC;

-- 3   Get The number of effective data in the table #FectivesDates
            DECLARE @DateCount INTEGER = (
                                           SELECT   COUNT(*)
                                           FROM     #EfectivesDates
                                         );
		
		-- The following line is only for debug purpose
		--SELECT * FROM @MYTempoTable
		
-- 4A  IF only exists 1 or 0 EffectiveDates return the table as is.
            IF @DateCount < 2 
-- 5A     Return Values
                SELECT  *
                FROM    @MYTempoTable
                ORDER BY CourseDescript
                       ,GradeDescript;
            ELSE
                BEGIN
-- 4B		If exists more....
                    INSERT  INTO @ResultTable
                            SELECT  *
                            FROM    @MYTempoTable
                            WHERE   EffectiveDate = (
                                                      SELECT TOP 1
                                                                EffectiveDate
                                                      FROM      #EfectivesDates
                                                    );
                    WITH    Q AS (
                                   SELECT TOP 1
                                            *
                                   FROM     #EfectivesDates
                                   ORDER BY EffectiveDate DESC
                                 )
                        DELETE  FROM Q;
                    SET @DateCount = (
                                       SELECT   COUNT(*)
                                       FROM     #EfectivesDates
                                     );
                    WHILE @DateCount > 0
                        BEGIN
                            INSERT  INTO @ResultTable
                                    SELECT  *
                                    FROM    @MYTempoTable
                                    WHERE   EffectiveDate = (
                                                              SELECT TOP 1
                                                                        EffectiveDate
                                                              FROM      #EfectivesDates
                                                            )
                                            AND CourseDescript NOT IN ( SELECT  CourseDescript
                                                                        FROM    @ResultTable );
                            WITH    Q AS (
                                           SELECT TOP 1
                                                    *
                                           FROM     #EfectivesDates
                                           ORDER BY EffectiveDate DESC
                                         )
                                DELETE  FROM Q;
                            SET @DateCount = (
                                               SELECT   COUNT(*)
                                               FROM     #EfectivesDates
                                             );
                        END;

                    INSERT  INTO @ResultTable
                            SELECT  *
                            FROM    @MYTempoTable
                            WHERE   EffectiveDate IS NULL;
-- 5B		Return the values
                    SELECT  *
                    FROM    @ResultTable
                    ORDER BY CourseDescript
                           ,Resource
                           ,GradeDescript;
                END;
        END TRY
        BEGIN CATCH
		--IF OBJECT_ID(N'tempdb..@MYTempoTable','U') IS NOT NULL DROP TABLE @MYTempoTable;
            IF OBJECT_ID(N'tempdb..#EfectivesDates','U') IS NOT NULL
                DROP TABLE #EfectivesDates;
		--IF OBJECT_ID(N'tempdb..@ResultTable', 'U') IS NOT NULL DROP TABLE @ResultTable;
		-- rethrow the error
            DECLARE @ErrorMessage NVARCHAR(4000);
            DECLARE @ErrorSeverity INT;
            DECLARE @ErrorState INT;
            SELECT  @ErrorMessage = ERROR_MESSAGE()
                   ,@ErrorSeverity = ERROR_SEVERITY()
                   ,@ErrorState = ERROR_STATE();

            RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
               );

        END CATCH;
-- 6  Destroy Temporaly Tables.
		--IF OBJECT_ID(N'tempdb..@MYTempoTable','U') IS NOT NULL DROP TABLE @MYTempoTable;
        IF OBJECT_ID(N'tempdb..#EfectivesDates','U') IS NOT NULL
            DROP TABLE #EfectivesDates;
		--IF OBJECT_ID(N'tempdb..@ResultTable', 'U') IS NOT NULL DROP TABLE @ResultTable;
    END;




GO
