SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetStudentLedgerDataGrid]
    @StudentEnrollmentId UNIQUEIDENTIFIER
AS
    SELECT    T.TransDate AS TransactionDate
             ,( CASE WHEN P.PaymentTypeId = 1 THEN 'Cash'
                     WHEN P.PaymentTypeId = 2 THEN 'Check Number:'
                     WHEN P.PaymentTypeId = 3 THEN 'C/C Authorization:'
                     WHEN P.PaymentTypeId = 4 THEN 'EFT Number:'
                     WHEN P.PaymentTypeId = 5 THEN 'Money Order Number:'
                     WHEN P.PaymentTypeId = 6 THEN 'Non Cash Reference #:'
                     ELSE ''
                END
              ) + P.CheckNumber AS Document
             ,CASE WHEN RTRIM(LTRIM(T.TransDescrip)) = '' THEN TC.TransCodeDescrip
                   ELSE RTRIM(LTRIM(T.TransDescrip)) + ( CASE WHEN TC.TransCodeId IS NOT NULL
                                                                   AND TC.SysTransCodeId IN ( 11, 16 ) THEN ' (' + TC.TransCodeDescrip + ')'
                                                              ELSE ''
                                                         END
                                                       )
              END AS TransactionDescription
             ,FORMAT(T.TransAmount, 'C') AS Amount
             ,FORMAT(   SUM(T.TransAmount) OVER ( ORDER BY T.TransDate asc
														  ,t.ModDate asc
														  ,T.TransactionId
                                                )
                       ,'C'
                    ) AS Balance
             ,CASE WHEN T.PaymentPeriodNumber IS NOT NULL THEN 'Payment Period'
                   ELSE ''
              END AS PeriodType
             ,CASE WHEN T.PaymentPeriodNumber IS NOT NULL THEN T.PaymentPeriodNumber
                   ELSE NULL
              END AS Period
    FROM      dbo.saTransactions T
    LEFT JOIN dbo.saPayments P ON P.TransactionId = T.TransactionId
    LEFT JOIN dbo.saTransCodes TC ON TC.TransCodeId = T.TransCodeId
    WHERE     T.StuEnrollId = @StudentEnrollmentId
              AND T.Voided = 0
    ORDER BY  TransactionDate ASC, t.ModDate ASC, T.TransactionId;
​

GO
​

GO
​

GO
