SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_Report_CreateParamDetail]
    @ItemId AS INT
   ,@SectionId AS INT
   ,@Sequence AS INT
-- WITH ENCRYPTION, RECOMPILE, EXECUTE AS CALLER|SELF|OWNER| 'user_name'
AS
    IF NOT EXISTS (
                  SELECT TOP 1 DetailId
                  FROM   dbo.ParamDetail
                  WHERE  SectionId = @SectionId
                         AND ItemId = @ItemId
                  )
        BEGIN
            INSERT INTO dbo.ParamDetail (
                                        SectionId
                                       ,ItemId
                                       ,CaptionOverride
                                       ,ItemSeq
                                        )
            VALUES ( @SectionId        -- SectionId - bigint
                    ,@ItemId           -- ItemId - bigint
                    ,N'Report Options' -- CaptionOverride - nvarchar(50)
                    ,@Sequence         -- ItemSeq - int
                );
        END;

GO
