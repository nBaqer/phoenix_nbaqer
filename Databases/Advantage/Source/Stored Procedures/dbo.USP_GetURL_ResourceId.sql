SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GetURL_ResourceId] @ResourceId INT
AS
    SELECT  ResourceURL
    FROM    syResources
    WHERE   ResourceId = @ResourceId;



GO
