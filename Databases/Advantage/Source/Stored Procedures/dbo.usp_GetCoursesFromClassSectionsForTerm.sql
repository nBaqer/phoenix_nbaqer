SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetCoursesFromClassSectionsForTerm] @TermId VARCHAR(50)
AS
    SET NOCOUNT ON; 
  
    BEGIN 
        SELECT DISTINCT
                arReqs.ReqId AS ReqId
               ,+'(' + arReqs.Code + ') ' + arReqs.Descrip AS Descrip
               ,arReqs.Descrip
        FROM    arReqs
               ,arClassSections
               ,arTerm
               ,syStatuses S
        WHERE   arReqs.StatusId = S.StatusId
                AND S.Status = 'Active'
                AND arReqs.ReqId = arClassSections.ReqId
                AND arClassSections.TermId = arTerm.TermId
                AND arTerm.TermId = @TermId
        ORDER BY arReqs.Descrip; 
      
    END; 



GO
