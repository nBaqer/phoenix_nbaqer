SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		FAME Inc.
-- Create date: 1/25/2019
-- Description:	Returns campus configuration mappings for 90/10 reports.
--Referenced in :DataExport9010Service
-- =============================================
CREATE   PROCEDURE [dbo].[USP_FA_9010GetCampusMappingsById]
    (
        @CampusId VARCHAR(50)
    )
AS
    BEGIN

        DECLARE @Campus9010mappings TABLE
            (
                Status VARCHAR(1000) ,
                EntityType VARCHAR(1000) ,
                Code VARCHAR(1000) ,
                EntityDescription VARCHAR(1000) ,
                AwardType VARCHAR(1000) ,
                Mapping9010Description VARCHAR(1000)
            );

        DECLARE @ActiveStatusId UNIQUEIDENTIFIER = (   SELECT TOP 1 StatusId
                                                       FROM   syStatuses
                                                       WHERE  StatusCode = 'A' );

        INSERT INTO @Campus9010mappings
                    SELECT CASE WHEN saFundSources.StatusId = @ActiveStatusId THEN
                                    'Active'
                                ELSE 'Inactive'
                           END ,
                           'FundSource' ,
                           FundSourceCode ,
                           FundSourceDescrip ,
                           dbo.saAwardTypes.Descrip ,
                           CASE WHEN ( syAwardTypes9010.AwardType9010Id IS NULL ) THEN
                                    ''
                                ELSE Description
                           END
                    FROM   dbo.saFundSources
                           LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                           LEFT JOIN dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                           LEFT JOIN dbo.saAwardTypes ON saAwardTypes.AwardTypeId = saFundSources.AwardTypeId
                    WHERE  CampGrpId IN (   SELECT CampGrpId
                                            FROM   dbo.syCmpGrpCmps
                                            WHERE  CampusId = @CampusId );

        INSERT INTO @Campus9010mappings
                    SELECT CASE WHEN saTransCodes.StatusId = @ActiveStatusId THEN
                                    'Active'
                                ELSE 'Inactive'
                           END ,
                           'TransactionType' ,
                           TransCodeCode ,
                           TransCodeDescrip ,
                           '' ,
                           CASE WHEN ( syAwardTypes9010.AwardType9010Id IS NULL ) THEN
                                    ''
                                ELSE Description
                           END
                    FROM   dbo.saTransCodes
                           LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.TransCodeId = saTransCodes.TransCodeId
                           LEFT JOIN dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                    WHERE  CampGrpId IN (   SELECT CampGrpId
                                            FROM   dbo.syCmpGrpCmps
                                            WHERE  CampusId = @CampusId );

        SELECT   Status ,
                 EntityType ,
                 Code ,
                 EntityDescription ,
                 AwardType ,
                 Mapping9010Description AS Mapping9010
        FROM     @Campus9010mappings
        ORDER BY EntityType ,
                 Status ,
                 EntityDescription;

    END;
GO
