SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_AR_PrgVerInstructionType_Insert]
    (
     @PrgVerID UNIQUEIDENTIFIER
    ,@InstructionTypeId UNIQUEIDENTIFIER
    ,@Hours DECIMAL(6,0)
    ,@UserName VARCHAR(50)
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Janet Robinson
    
    Create date		:	08/11/2011
    
	Procedure Name	:	[USP_AR_PrgVerInstructionType_Insert]

	Objective		:	Inserts into table arPrgVerInstructionType
	
	Parameters		:	Name				Type	Data Type				Required? 	
						=====				====	=========				=========	
						@PrgVerID			  In	UniqueIDENTIFIER		Required						
						@InstructionTypeId	  In	UniqueIDENTIFIER		Required
						@Hours				  In	decimal					Required
						@UserName			  In	varchar					Required
	
	Output			:			
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN

        INSERT  INTO arPrgVerInstructionType
                (
                 PrgVerInstructionTypeId
                ,PrgVerID
                ,InstructionTypeId
                ,Hours
                ,ModUser
                ,ModDate
                )
        VALUES  (
                 NEWID()
                ,@PrgVerID
                ,@InstructionTypeId
                ,@Hours
                ,@UserName
                ,GETDATE()
                );

    END;



GO
