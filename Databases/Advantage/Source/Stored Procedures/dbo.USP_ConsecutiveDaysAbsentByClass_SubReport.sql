SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_ConsecutiveDaysAbsentByClass_SubReport]
    @TermId VARCHAR(100) = NULL
   ,@ReqId VARCHAR(100) = NULL
   ,@ConsDays INT
   ,@CutoffDate DATETIME
   ,@ShowCalendarDays BIT = 0
AS
    BEGIN  
        SELECT  R.Student
               ,R.[Student ID]
               ,R.status
               ,CONVERT(DATE,R.[Start Date],101) AS [Start Date]
               ,CONVERT(DATE,R.LDA,101) AS LDA
               ,R.[Program Version]
               ,R.Instructor
               ,'' AS Period
               ,R.[Days Missed]
        FROM    (
                  SELECT    ST.LastName + ', ' + ST.FirstName + ' ' + ISNULL(ST.MiddleName,'') AS Student
                           ,ST.StudentNumber AS [Student ID]
                           ,SC.StatusCodeDescrip AS status
                           ,SE.StartDate AS [Start Date]      
   --           , (CASE WHEN SE.LDA IS NULL   
   --                       THEN CONVERT(DATE, (SELECT TOP 1 SSAS.StudentAttendedDate  
         --       FROM syStudentAttendanceSummary AS SSAS  
         --       WHERE SSAS.StuEnrollId = SE.StuEnrollId  
         --        AND SSAS.ActualDays <> 0  
         --        AND SSAS.ActualDays <> 9999  
         --        AND SSAS.ActualDays <> 99999  
         --       ORDER BY SSAS.StudentAttendedDate DESC), 101)  
         --  ELSE SE.LDA   
      --END) AS LDA  
                           ,(
                              SELECT    MAX(LDA)
                              FROM      (
                                          SELECT    MAX(AttendedDate) AS LDA
                                          FROM      arExternshipAttendance
                                          WHERE     StuEnrollId = SE.StuEnrollId
                                          UNION ALL
                                          SELECT    MAX(MeetDate) AS LDA
                                          FROM      atClsSectAttendance
                                          WHERE     StuEnrollId = SE.StuEnrollId
                                                    AND Actual > 0
                                                    AND Actual <> 9999.00
                                          UNION ALL
                                          SELECT    MAX(AttendanceDate) AS LDA
                                          FROM      atAttendance
                                          WHERE     EnrollId = SE.StuEnrollId
                                                    AND Actual > 0
                                                    AND Actual <> 9999.00
                                          UNION ALL
                                          SELECT    MAX(RecordDate) AS LDA
                                          FROM      arStudentClockAttendance
                                          WHERE     StuEnrollId = SE.StuEnrollId
                                                    AND (
                                                          ActualHours > 0
                                                          AND ActualHours <> 99.00
                                                          AND ActualHours <> 999.00
                                                          AND ActualHours <> 9999.00
                                                        )
                                          UNION ALL
                                          SELECT    MAX(MeetDate) AS LDA
                                          FROM      atConversionAttendance
                                          WHERE     StuEnrollId = SE.StuEnrollId
                                                    AND (
                                                          Actual > 0
                                                          AND Actual <> 99.00
                                                          AND Actual <> 999.00
                                                          AND Actual <> 9999.00
                                                        )
                                          UNION ALL
                                          SELECT TOP 1
                                                    LDA
                                          FROM      arStuEnrollments
                                          WHERE     StuEnrollId = SE.StuEnrollId
                                        ) TR
                            ) AS LDA
                           ,PV.PrgVerDescrip AS [Program Version]
                           ,SU.FullName AS Instructor
                           ,(
							  SELECT dbo.MaxConsecutiveDaysAbsentForProgram(se.StuEnrollId, @CutoffDate, @ShowCalendarDays, @ConsDays, cs.ClsSectionId)
                            ) AS [Days Missed]
                           ,rq.Descrip AS Course
                  FROM      adLeads AS ST
                  INNER JOIN dbo.arStuEnrollments AS SE ON ST.StudentId = SE.StudentId
                  INNER JOIN arPrgVersions AS PV ON SE.PrgVerId = PV.PrgVerId
                  INNER JOIN dbo.arResults AS RS ON SE.StuEnrollId = RS.StuEnrollId
                  INNER JOIN dbo.arClassSections AS CS ON RS.TestId = CS.ClsSectionId
                  INNER JOIN dbo.arTerm AS tm ON CS.TermId = tm.TermId
                  INNER JOIN arReqs AS rq ON CS.ReqId = rq.ReqId
                  INNER JOIN dbo.syStatusCodes AS SC ON SE.StatusCodeId = SC.StatusCodeId
                  INNER JOIN dbo.sySysStatus AS SS ON SC.SysStatusId = SS.SysStatusId
                  LEFT OUTER JOIN dbo.syUsers AS SU ON CS.InstructorId = SU.UserId
                  WHERE     (
                              @TermId IS NULL
                              OR tm.TermId = @TermId
                            )
                            AND (
                                  @ReqId IS NULL
                                  OR rq.ReqId = @ReqId
                                )
                            AND SS.InSchool = 1
                ) R
        WHERE   ( R.[Days Missed] >= @ConsDays )
        ORDER BY R.Student
               ,R.Course;  
  
    END;  
  
  
  
GO
