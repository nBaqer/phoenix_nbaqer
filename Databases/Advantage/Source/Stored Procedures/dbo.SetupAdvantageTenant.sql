SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SetupAdvantageTenant]
    @ServerName NVARCHAR(200)
   ,@DBUser NVARCHAR(100)
   ,@DBPassword NVARCHAR(100)
   ,@Environment INT
   ,@SiteURL NVARCHAR(300)
   ,@AdvantageApiURL NVARCHAR(300)
   ,@ServiceURL NVARCHAR(300)
   ,@DBOUser NVARCHAR(100)
   ,@ReportExecutionServices VARCHAR(100)
   ,@ReportServices VARCHAR(100)
AS
    BEGIN
        DECLARE @DatabaseName NVARCHAR(100);
        DECLARE @AuthKey UNIQUEIDENTIFIER = NEWID();
        DECLARE @SUPPORT_ID UNIQUEIDENTIFIER = '864335ee-c9c6-47c9-bbcb-deee766f27a1';
        DECLARE @DATA_CONNECTION_ID INT;
        DECLARE @TENANT_ID INT;
        SET @DatabaseName = (
                            SELECT DB_NAME() AS [Current Database]
                            );

        SET @TENANT_ID = (
                         SELECT TOP 1 TenantId
                         FROM   TenantAuthDB..Tenant
                         WHERE  [TenantName] = @DatabaseName
                         );



        IF ( @TENANT_ID IS NULL )
            BEGIN
                PRINT 'TENANT DOES NOT EXIST';
                BEGIN TRANSACTION;
                INSERT INTO TenantAuthDB..Tenant
                VALUES ( @DatabaseName, GETDATE(), GETDATE(), 'SA', @ServerName, @DatabaseName, @DBUser, @DBPassword, @Environment );

                SET @TENANT_ID = (
                                 SELECT TOP 1 TenantId
                                 FROM   TenantAuthDB..Tenant
                                 WHERE  [TenantName] = @DatabaseName
                                 );

                PRINT 'TENANT CREATED : ' + CONVERT(NVARCHAR, @TENANT_ID);
                COMMIT;
            END;

        SET @DATA_CONNECTION_ID = (
                                  SELECT TOP 1 [ConnectionStringId]
                                  FROM   TenantAuthDB..DataConnection
                                  WHERE  DatabaseName = @DatabaseName
                                  );
        IF ( @DATA_CONNECTION_ID IS NULL )
            BEGIN
                PRINT 'CONNECTION STRING DOES NOT EXIST';
                BEGIN TRANSACTION;
                INSERT INTO TenantAuthDB..[DataConnection]
                VALUES ( @ServerName, @DatabaseName, @DBUser, NULL, @DBPassword );

                SET @DATA_CONNECTION_ID = (
                                          SELECT TOP 1 [ConnectionStringId]
                                          FROM   TenantAuthDB..DataConnection
                                          WHERE  DatabaseName = @DatabaseName
                                          );

                PRINT 'CONNECTION STRING CREATED : ' + CONVERT(NVARCHAR, @DATA_CONNECTION_ID);
                COMMIT;
            END;

        IF NOT EXISTS (
                      SELECT *
                      FROM   TenantAuthDB..[TenantAccess]
                      WHERE  [TenantId] = @TENANT_ID
                             AND [EnvironmentId] = @Environment
                             AND [ConnectionStringId] = @DATA_CONNECTION_ID
                      )
            BEGIN
                PRINT 'TENANT ACCESS DOES NOT EXIST';
                BEGIN TRANSACTION;
                INSERT INTO TenantAuthDB..[TenantAccess]
                VALUES ( @TENANT_ID, @Environment, @DATA_CONNECTION_ID );

                PRINT 'TENANT ACCESS CREATED';
                COMMIT;
            END;
        IF NOT EXISTS (
                      SELECT *
                      FROM   TenantAuthDB..TenantUsers
                      WHERE  [TenantId] = @TENANT_ID
                             AND [UserId] = @SUPPORT_ID
                      )
            BEGIN
                PRINT 'TENANT SUPPORT USER DOES NOT EXIST';
                BEGIN TRANSACTION;
                INSERT INTO TenantAuthDB..[TenantUsers]
                VALUES ( @TENANT_ID, @SUPPORT_ID, 0, 1 );

                PRINT 'TENANT SUPPORT USER CREATED';
                COMMIT;
            END;
        IF NOT EXISTS (
                      SELECT *
                      FROM   TenantAuthDB..[ApiAuthenticationKey]
                      WHERE  [TenantId] = @TENANT_ID
                      )
            BEGIN
                PRINT 'TENANT ApiAuthenticationKey DOES NOT EXIST';
                BEGIN TRANSACTION;
                INSERT INTO TenantAuthDB..[ApiAuthenticationKey]
                VALUES ( @TENANT_ID, @AuthKey, GETDATE());

                PRINT 'TENANT ApiAuthenticationKey CREATED';
                COMMIT;
            END;
        ELSE
            BEGIN
                UPDATE A
                SET    [Key] = @AuthKey
                FROM   TenantAuthDB..[ApiAuthenticationKey] A
                WHERE  [TenantId] = @TENANT_ID;
            END;

        IF ( SYSTEM_USER <> @DBOUser )
            BEGIN

                EXEC dbo.sp_changedbowner @loginame = @DBOUser
                                         ,@map = false;
                PRINT 'DB Owner Changed';
            END;

        UPDATE [syConfigAppSetValues]
        SET    Value = @SiteURL
        WHERE  SettingId = (
                           SELECT val.[SettingId]
                           FROM   [dbo].[syConfigAppSetValues] val
                           JOIN   [dbo].[syConfigAppSettings] ON syConfigAppSettings.SettingId = val.SettingId
                           WHERE  KeyName = 'AdvantageSiteUri'
                           );

        UPDATE [syConfigAppSetValues]
        SET    Value = @ServiceURL
        WHERE  SettingId = (
                           SELECT val.[SettingId]
                           FROM   [dbo].[syConfigAppSetValues] val
                           JOIN   [dbo].[syConfigAppSettings] ON syConfigAppSettings.SettingId = val.SettingId
                           WHERE  KeyName = 'AdvantageServiceUri'
                           );

        UPDATE [syConfigAppSetValues]
        SET    Value = @AuthKey
        WHERE  SettingId = (
                           SELECT val.[SettingId]
                           FROM   [dbo].[syConfigAppSetValues] val
                           JOIN   [dbo].[syConfigAppSettings] ON syConfigAppSettings.SettingId = val.SettingId
                           WHERE  KeyName = 'AdvantageServiceAuthenticationKey'
                           );

        UPDATE [syConfigAppSetValues]
        SET    Value = @ReportExecutionServices
        WHERE  SettingId = (
                           SELECT val.[SettingId]
                           FROM   [dbo].[syConfigAppSetValues] val
                           JOIN   [dbo].[syConfigAppSettings] ON syConfigAppSettings.SettingId = val.SettingId
                           WHERE  KeyName = 'ReportExecutionServices'
                           );

        UPDATE [syConfigAppSetValues]
        SET    Value = @ReportServices
        WHERE  SettingId = (
                           SELECT val.[SettingId]
                           FROM   [dbo].[syConfigAppSetValues] val
                           JOIN   [dbo].[syConfigAppSettings] ON syConfigAppSettings.SettingId = val.SettingId
                           WHERE  KeyName = 'ReportServices'
                           );


        UPDATE [syConfigAppSetValues]
        SET    Value = @AdvantageApiURL
        WHERE  SettingId = (
                           SELECT val.[SettingId]
                           FROM   [dbo].[syConfigAppSetValues] val
                           JOIN   [dbo].[syConfigAppSettings] ON syConfigAppSettings.SettingId = val.SettingId
                           WHERE  KeyName = 'AdvantageApiURL'
                           );

        PRINT 'Site, Service and AuthKey Updated';

        SET @DatabaseName = '[' + @DatabaseName + ']';

        PRINT 'Executing SP UpdateAdvantageSupportUser';

        EXEC TenantAuthDB..UpdateAdvantageSupportUser @DatabaseName;

        PRINT 'SP UpdateAdvantageSupportUser Executed';
        SET NOCOUNT OFF;
    END;

GO
