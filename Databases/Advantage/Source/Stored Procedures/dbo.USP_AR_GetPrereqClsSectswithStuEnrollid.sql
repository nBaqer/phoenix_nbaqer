SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[USP_AR_GetPrereqClsSectswithStuEnrollid]
    (
     @PreReqID UNIQUEIDENTIFIER
    ,@StuEnrollID UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	02/03/2010
    
	Procedure Name	:	USP_AR_GetPrereqClsSectswithStuEnrollid

	Objective		:	To get the prerequisite Courses for a classsection and student
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@PreReqID		In		UniqueIdentifier	Required	
						@StuEnrollID	In		UniqueIdentifier	Required
*/-----------------------------------------------------------------------------------------------------

    BEGIN

        SELECT  b.PreCoReqId
               ,c.ClsSectionId
        FROM    arClassSections c
        INNER JOIN arCourseReqs b ON c.ReqId = b.PreCoReqId
        --INNER JOIN arStuEnrollments SE ON c.ShiftId = SE.ShiftId
                                          --AND C.CampusID = SE.CampusID
                                     AND c.StartDate <= GETDATE()
                                     AND c.StartDate >= (
                                                          SELECT    StartDate
                                                          FROM      dbo.arStuEnrollments
                                                          WHERE     StuEnrollId = @StuEnrollID
                                                        )--SE.StartDate
        WHERE   b.CourseReqTypId = 1
                AND b.PreCoReqId = @PreReqID;
                --AND SE.StuEnrollId = @StuEnrollID 

    END;


GO
