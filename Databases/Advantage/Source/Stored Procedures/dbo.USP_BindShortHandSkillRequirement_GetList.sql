SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_BindShortHandSkillRequirement_GetList]
    @SAPDetailId UNIQUEIDENTIFIER
AS
    SELECT DISTINCT
            GrdComponentTypeId
           ,CONVERT(CHAR(36),GrdComponentTypeId) + ':' + RTRIM(CONVERT(VARCHAR(20),Speed)) AS MentorValue
           ,Speed
           ,Operator
           ,MentorOperatorOrder = CASE Operator
                                    WHEN 'Select' THEN 0
                                    WHEN 'And' THEN 1
                                    WHEN 'Or' THEN 2
                                    ELSE 3
                                  END
           ,OperatorSequence
    FROM    arSAP_ShortHandSkillRequirement
    WHERE   SAPDetailId = @SAPDetailId
    ORDER BY OperatorSequence;



GO
