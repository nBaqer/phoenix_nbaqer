SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_FamilyIncome_GetList] @Descrip VARCHAR(50)
AS
    SELECT  FamilyIncomeId
    FROM    syFamilyIncome
    WHERE   FamilyIncomeDescrip LIKE @Descrip + '%';



GO
