SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetCoursesForTerm]
    @TermId VARCHAR(50)
   ,@campusId VARCHAR(50)
   ,@showActiveOnly BIT = 1


----- [dbo].[usp_GetCoursesForTerm] 'b9a3d3ed-b32d-45d9-9dff-208a7a3100d6','436bf45e-5420-4c1f-8687-cd32a60d64be'
AS
    SET NOCOUNT ON;
    DECLARE @progId VARCHAR(50);
    SET @progId = (
                  SELECT ISNULL(ProgId, '00000000-0000-0000-0000-000000000000')
                  FROM   arTerm
                  WHERE  TermId = @TermId
                  );
    IF @progId = '00000000-0000-0000-0000-000000000000'
        BEGIN
            IF @showActiveOnly = 1
                BEGIN
                    EXEC usp_GetAllActiveCoursesForTerm @campusId
                                                       ,@TermId;
                END;
            ELSE
                BEGIN
                    EXEC usp_GetAllCoursesForTerm @campusId
                                                 ,@TermId;
                END;

        END;
    ELSE
        BEGIN
            SELECT   ReqId
                    ,Descrip
            FROM     (
                     SELECT DISTINCT t400.ReqId AS ReqId
                           ,'(' + t3.Code + ') ' + t3.Descrip AS Descrip
                           ,t3.Descrip AS shortDesc
                           ,t3.Code AS code
                     FROM   arReqs t3
                           ,arProgVerDef t400
                     WHERE  t3.ReqId = t400.ReqId
                            AND t3.ReqTypeId = 1
                            AND t400.PrgVerId IN (
                                                 SELECT PrgVerId
                                                 FROM   arPrgVersions
                                                 WHERE  ProgId = @progId
                                                 )
                            AND t3.CampGrpId IN (
                                                SELECT CampGrpId
                                                FROM   syCmpGrpCmps
                                                WHERE  CampusId = @campusId
                                                )
                     UNION
                     SELECT DISTINCT t700.ReqId AS ReqId
                           ,(
                            SELECT '(' + Code + ') ' + Descrip
                            FROM   arReqs
                            WHERE  ReqId = t700.ReqId
                            ) AS Descrip
                           ,(
                            SELECT Descrip
                            FROM   arReqs
                            WHERE  ReqId = t700.ReqId
                            ) AS shortDesc
                           ,(
                            SELECT Code
                            FROM   arReqs
                            WHERE  ReqId = t700.ReqId
                            ) AS Code
                     FROM   arReqs t3
                           ,arProgVerDef t400
                           ,arReqGrpDef t700
                     WHERE  t3.ReqId = t400.ReqId
                            AND t3.ReqTypeId = 2
                            AND t400.ReqId = t700.GrpId
                            AND t400.PrgVerId IN (
                                                 SELECT PrgVerId
                                                 FROM   arPrgVersions
                                                 WHERE  ProgId = @progId
                                                 )
                            AND t3.CampGrpId IN (
                                                SELECT CampGrpId
                                                FROM   syCmpGrpCmps
                                                WHERE  CampusId = @campusId
                                                )
                     ) R
            ORDER BY shortDesc
                    ,code;

            SELECT StartDate
                  ,EndDate
            FROM   arTerm
            WHERE  TermId = @TermId;
        END;
GO
