SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


----------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[USP_ApplyFeesByProgramVersion]
    (
     @TermID AS UNIQUEIDENTIFIER
    ,@User VARCHAR(100)
    ,@CampusID UNIQUEIDENTIFIER
    ,@PostFeesDate DATETIME
    ,@Result INTEGER OUTPUT

    )
AS
    SET NOCOUNT ON;

    BEGIN 

    

        DECLARE @StuEnrollID UNIQUEIDENTIFIER;

        DECLARE @PrgVerFeeId AS UNIQUEIDENTIFIER;

        DECLARE @TransCodeID AS UNIQUEIDENTIFIER;

        DECLARE @TransCodeDescrip AS VARCHAR(50);

        DECLARE @PrgVerDescrip AS VARCHAR(50);

        DECLARE @TransDescrip AS VARCHAR(50);

        DECLARE @TransAmount DECIMAL(19,4);

        SET @TransAmount = 0;

       

    

        BEGIN TRANSACTION;

       

        DECLARE c1 CURSOR READ_ONLY
        FOR
            (
              SELECT DISTINCT
                        R.StuEnrollId
                       ,PVF.PrgVerFeeId
                       ,PVF.TransCodeId
                       ,( (
                            SELECT  TransCodeDescrip
                            FROM    saTransCodes
                            WHERE   TransCodeId = PVF.TransCodeId
                          ) + '-' + PV.PrgVerDescrip ) TransCodeDescrip
                       ,PV.PrgVerDescrip + ' ' + (
                                                   SELECT   LastName + ' ' + FirstName
                                                   FROM     adLeads --arStudent
                                                   WHERE    StudentId = (
                                                                          SELECT    StudentId
                                                                          FROM      arStuEnrollments
                                                                          WHERE     StuEnrollId = SE.StuEnrollId
                                                                        )
                                                 ) TransDescrip
                       ,( CASE LEN(PVF.RateScheduleId)
                            WHEN 36 THEN (
                                           SELECT   CASE RSD.FlatAmount
                                                      WHEN 0.00 THEN CASE RSD.UnitId
                                                                       WHEN 0 THEN Rate * PV.Credits
                                                                       WHEN 1 THEN Rate * PV.Hours
                                                                     END
                                                      ELSE FlatAmount
                                                    END
                                           FROM     saRateSchedules RS
                                                   ,saRateScheduleDetails RSD
                                           WHERE    RS.RateScheduleId = RSD.RateScheduleId
                                                    AND RS.RateScheduleId = PVF.RateScheduleId
                                                    AND (
                                                          RSD.TuitionCategoryId = SE.TuitionCategoryId
                                                          OR (
                                                               RSD.TuitionCategoryId IS NULL
                                                               AND SE.TuitionCategoryId IS NULL
                                                             )
                                                        )
                                                    AND (
                                                          (
                                                            RSD.FlatAmount = 0.00
                                                            AND RSD.UnitId = 0
                                                            AND RSD.MaxUnits = (
                                                                                 SELECT MIN(MaxUnits)
                                                                                 FROM   saRateScheduleDetails
                                                                                 WHERE  MaxUnits >= PV.Credits
                                                                                        AND RateScheduleId = RSD.RateScheduleId
                                                                                        AND (
                                                                                              RSD.TuitionCategoryId = SE.TuitionCategoryId
                                                                                              OR (
                                                                                                   RSD.TuitionCategoryId IS NULL
                                                                                                   AND SE.TuitionCategoryId IS NULL
                                                                                                 )
                                                                                            )
                                                                               )
                                                            AND RSD.MinUnits <= PV.Credits
                                                            AND RSD.MaxUnits >= PV.Credits
                                                          )
                                                          OR (
                                                               RSD.FlatAmount = 0.00
                                                               AND RSD.UnitId = 1
                                                               AND RSD.MaxUnits = (
                                                                                    SELECT  MIN(MaxUnits)
                                                                                    FROM    saRateScheduleDetails
                                                                                    WHERE   MaxUnits >= PV.Hours
                                                                                            AND RateScheduleId = RSD.RateScheduleId
                                                                                            AND (
                                                                                                  RSD.TuitionCategoryId = SE.TuitionCategoryId
                                                                                                  OR (
                                                                                                       RSD.TuitionCategoryId IS NULL
                                                                                                       AND SE.TuitionCategoryId IS NULL
                                                                                                     )
                                                                                                )
                                                                                  )
                                                               AND RSD.MinUnits <= PV.Hours
                                                               AND RSD.MaxUnits >= PV.Hours
                                                             )
                                                          OR (
                                                               RSD.FlatAmount > 0.00
                                                               AND RSD.MaxUnits = (
                                                                                    SELECT  MIN(MaxUnits)
                                                                                    FROM    saRateScheduleDetails
                                                                                    WHERE   MaxUnits >= PV.Credits
                                                                                            AND RateScheduleId = RSD.RateScheduleId
                                                                                            AND (
                                                                                                  RSD.TuitionCategoryId = SE.TuitionCategoryId
                                                                                                  OR (
                                                                                                       RSD.TuitionCategoryId IS NULL
                                                                                                       AND SE.TuitionCategoryId IS NULL
                                                                                                     )
                                                                                                )
                                                                                  )
                                                               AND RSD.MinUnits <= PV.Credits
                                                               AND RSD.MaxUnits >= PV.Credits
                                                             )
                                                        )
                                         )
                            ELSE ( CASE PVF.UnitId
                                     WHEN 0 THEN PVF.Amount * PV.Credits
                                     WHEN 1 THEN PVF.Amount * PV.Hours
                                     WHEN 2 THEN PVF.Amount
                                   END )
                          END ) AS TransAmount
              FROM      arResults R
                       ,arClassSections cs
                       ,arClassSectionTerms CST
                       ,arStuEnrollments SE
                       ,arPrgVersions PV
                       ,saProgramVersionFees PVF
                       ,arTerm T
                       ,syStatusCodes SC
              WHERE     R.StuEnrollId = SE.StuEnrollId
                        AND R.TestId = cs.ClsSectionId
                        AND SE.PrgVerId = PV.PrgVerId
                        AND PV.PrgVerId = PVF.PrgVerId
                        AND SE.CampusId = @CampusID
                        AND CST.ClsSectionId = cs.ClsSectionId
                        AND CST.TermId = @TermID
                        AND (
                              (
                                (
                                  PVF.RateScheduleId IS NULL
                                  AND SE.TuitionCategoryId IS NULL
                                  AND PVF.TuitionCategoryId IS NULL
                                )
                                OR (
                                     PVF.RateScheduleId IS NULL
                                     AND SE.TuitionCategoryId = PVF.TuitionCategoryId
                                   )
                              )
                              OR (
                                   PVF.RateScheduleId IS NOT NULL
                                   AND EXISTS ( SELECT  rsd.TuitionCategoryId
                                                FROM    saRateScheduleDetails rsd
                                                WHERE   rsd.RateScheduleId = PVF.RateScheduleId
                                                        AND rsd.TuitionCategoryId = SE.TuitionCategoryId )
                                 )
                              OR (
                                   PVF.RateScheduleId IS NOT NULL
                                   AND SE.TuitionCategoryId IS NULL
                                   AND EXISTS ( SELECT  rsd.*
                                                FROM    saRateScheduleDetails rsd
                                                WHERE   rsd.RateScheduleId = PVF.RateScheduleId
                                                        AND rsd.TuitionCategoryId IS NULL )
                                 )
                            )
                        AND CST.TermId = T.TermId
                        AND SE.StartDate = T.StartDate
                        AND SE.StatusCodeId = SC.StatusCodeId
                        AND SC.SysStatusId IN ( 7,9,13,20 )

				

				--We only want to get students registered for classes in the term that is passed in.

				--Otherwise it is going to end up posting zeroes for all the other students
                        AND EXISTS ( SELECT rs.StuEnrollId
                                     FROM   arResults rs
                                           ,arClassSections cs
                                     WHERE  rs.TestId = cs.ClsSectionId
                                            AND cs.TermId = @TermID
                                            AND rs.StuEnrollId = SE.StuEnrollId )
            ); 

  

        OPEN c1; 

  

        FETCH NEXT FROM c1 

        INTO @StuEnrollID,@PrgVerFeeId,@TransCodeID,@TransCodeDescrip,@TransDescrip,@TransAmount;

  

        WHILE @@FETCH_STATUS = 0
            BEGIN 

  

                IF ISNULL(@TransAmount,0) <> 0
                    BEGIN

                        INSERT  INTO dbo.saTransactions
                                (
                                 TransactionId
                                ,StuEnrollId
                                ,TermId
                                ,CampusId
                                ,TransDate
                                ,TransCodeId
                                ,TransReference
                                ,AcademicYearId
                                ,TransDescrip
                                ,TransAmount
                                ,TransTypeId
                                ,IsPosted
                                ,CreateDate
                                ,BatchPaymentId
                                ,ViewOrder
                                ,IsAutomatic
                                ,ModUser
                                ,ModDate
                                ,Voided
                                ,FeeLevelId
                                ,FeeId
                                ,PaymentCodeId
                                ,FundSourceId 

                                )
                        VALUES  (
                                 NEWID()
                                , -- TransactionId - uniqueidentifier
                                 @StuEnrollID
                                , -- StuEnrollId - uniqueidentifier
                                 @TermID
                                , -- TermId - uniqueidentifier
                                 @CampusID
                                , -- CampusId - uniqueidentifier
                                 @PostFeesDate
                                , -- TransDate - datetime
                                 @TransCodeID
                                , -- TransCodeId - uniqueidentifier
                                 @TransDescrip
                                , -- TransReference - varchar(50)
                                 NULL
                                , -- AcademicYearId - uniqueidentifier
                                 @TransCodeDescrip
                                , -- TransDescrip - varchar(50)
                                 ISNULL(@TransAmount,0)
                                , -- TransAmount - decimal
                                 0
                                , -- TransTypeId - int
                                 1
                                , -- IsPosted - bit
                                 GETDATE()
                                , -- CreateDate - datetime
                                 NULL
                                , -- BatchPaymentId - uniqueidentifier
                                 0
                                , -- ViewOrder - int
                                 1
                                , -- IsAutomatic - bit
                                 @User
                                , -- ModUser - varchar(50)
                                 GETDATE()
                                , -- ModDate - datetime
                                 0
                                , -- Voided - bit
                                 2
                                , -- FeeLevelId - tinyint
                                 @PrgVerFeeId
                                , -- FeeId - uniqueidentifier
                                 NULL
                                , -- PaymentCodeId - uniqueidentifier
                                 NULL  -- FundSourceId - uniqueidentifier

                         

                                );

  

                        IF @@ERROR <> 0
                            BEGIN

						-- Rollback the transaction

                                ROLLBACK; 

                                SET @Result = 0;

                                RETURN 0;--"Error while inserting"

                            END;

					

                    END;

                



                FETCH NEXT FROM c1 

            INTO @StuEnrollID,@PrgVerFeeId,@TransCodeID,@TransCodeDescrip,@TransDescrip,@TransAmount;

  

            END; 

        CLOSE c1; 

        DEALLOCATE c1; 

 

        IF @@ERROR = 0
            BEGIN

                COMMIT TRANSACTION;

                SET @Result = 1;

                RETURN 1;

            END;

        ELSE
            BEGIN

					-- Rollback the transaction

                ROLLBACK TRANSACTION;

                SET @Result = 0;	

                RETURN 0;--"Error while inserting"	

            END;

	  

    END;
GO
