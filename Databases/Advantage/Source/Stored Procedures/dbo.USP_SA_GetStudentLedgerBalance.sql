SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_SA_GetStudentLedgerBalance]
    (
     @StuEnrollId UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	04/12/2010
    
	Procedure Name	:	[USP_SA_GetStudentLedgerBalance]

	Objective		:	Get student ledger balance
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@StuEnrollId	In		Uniqueidentifier	Required
	
	Output			:	returns the sum of the balance amount for a given stuEnrollid
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN	
--Get Student Ledger Balance

        SELECT  ISNULL(SUM(TransAmount),0)
        FROM    saTransactions
        WHERE   StuEnrollId = @StuEnrollId
                AND Voided = 0;
    END;



GO
