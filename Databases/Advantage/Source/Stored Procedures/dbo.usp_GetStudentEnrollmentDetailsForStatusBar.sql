SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--==========================================================================================
-- USP_GetStudentEnrollmentDetailsForStatusBar
--==========================================================================================
CREATE PROCEDURE [dbo].[USP_GetStudentEnrollmentDetailsForStatusBar]
/* 
PURPOSE: 
Get active enrollments for selected student to be available to student info bar

MODIFIED:
11/5/2013	WP		US4380 Multiple Enrollment Indicator on Student Enrollment Bar
*/  @StudentId UNIQUEIDENTIFIER
   ,@CampusId UNIQUEIDENTIFIER = '00000000-0000-0000-0000-000000000000'
AS
    DECLARE @MinimunDate AS DATE;
    SET @MinimunDate = '1900-01-01';

    BEGIN
        SELECT  SC.CampDescrip AS CampusDescrip
               ,APV.PrgVerDescrip
               ,CASE WHEN ISNULL(APVT.ProgramVersionTypeId,0) = 0 THEN ''
                     ELSE APVT.DescriptionProgramVersionType
                END AS PrgVerTypeDescrip
               ,ASE.StartDate AS StartDate
               ,ASE.ExpGradDate AS GradDate
               ,SSC.StatusCodeDescrip AS EnrollmentStatus
               ,SSC.SysStatusId AS SystemStatus
               ,ASE.PrgVerId
               ,ASE.CampusId
               ,ASE.StuEnrollId
        FROM    arStuEnrollments AS ASE
        INNER JOIN arPrgVersions AS APV ON APV.PrgVerId = ASE.PrgVerId
        INNER JOIN syStatusCodes AS SSC ON SSC.StatusCodeId = ASE.StatusCodeId
        INNER JOIN syCampuses AS SC ON SC.CampusId = ASE.CampusId
        INNER JOIN arProgramVersionType AS APVT ON APVT.ProgramVersionTypeId = ASE.PrgVersionTypeId
        WHERE   ASE.StudentId = @StudentId
			--AND sc.SysStatusId IN (9,14) -- currently attending, graduated
                AND (
                      ISNULL(@CampusId,'00000000-0000-0000-0000-000000000000') = SC.CampusId
                      OR ISNULL(@CampusId,'00000000-0000-0000-0000-000000000000') = '00000000-0000-0000-0000-000000000000'
                    )
        ORDER BY ISNULL(ASE.StartDate,@MinimunDate) DESC; 
    END;
--==========================================================================================
-- END  --  USP_GetStudentEnrollmentDetailsForStatusBar
--==========================================================================================


GO
