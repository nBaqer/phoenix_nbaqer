SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_FallPartC3_GetList_Enrollment_Summary]
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@StartDate DATETIME = NULL
   ,@EndDate DATETIME = NULL
   ,@DateRangeText VARCHAR(100) = NULL
   ,@OrderBy VARCHAR(100)
   ,@AreasOfInterest VARCHAR(4000) = NULL
AS
    BEGIN
        DECLARE @ContinuingStartDate DATETIME;
	
        SET @ContinuingStartDate = @StartDate;
	
		-- For Academic Year Reporter, the End Date should be 10/15/Cohort Year
		-- and Start Date should be blank
        IF DAY(@EndDate) = 15
            AND MONTH(@EndDate) = 10
            BEGIN
                SET @EndDate = @StartDate; -- it will always be 10/15/cohort year example: for cohort year 2009, it is 10/15/2009
                SET @StartDate = @StartDate;
            END;

        DECLARE @ConfirmedMenApplicantCount INT
           ,@ConfirmedWomenApplicantCount INT;

        SET @ConfirmedMenApplicantCount = 0;
        SET @ConfirmedWomenApplicantCount = 0;
        SET @ConfirmedMenApplicantCount = (
                                            SELECT  COUNT(*)
                                            FROM    adLeads t1
                                            LEFT JOIN adGenders t3 ON t1.Gender = t3.GenderId
                                            LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race  
											-- For Degree Seeking --
                                            LEFT JOIN adDegCertSeeking t11 ON t1.DegCertSeekingId = t11.DegCertSeekingId
											-- For Degree Seeking -- 
                                            INNER JOIN syStatusCodes t5 ON t1.LeadStatus = t5.StatusCodeId
                                            INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId 
											--and t6.SysStatusId not in (7,8)
                                            INNER JOIN arPrgVersions t7 ON t1.PrgVerId = t7.PrgVerId
                                            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId  
											--Areas Of Intrest --
                                            INNER JOIN arPrgGrp t12 ON t12.PrgGrpId = t7.PrgGrpId 
											--Areas Of Intrest --
                                            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                            WHERE   t1.CampusId = @CampusId
                                                    AND t9.IPEDSValue = 58
                                                    AND ( t3.IPEDSValue = 30 ) -- Men 
                                                    AND ( t11.IPEDSValue = 11 ) -- First-Time Degree Certificate Seeking
                                                    AND t1.LeadId NOT IN ( SELECT DISTINCT
                                                                                    LeadId
                                                                           FROM     arStuEnrollments
                                                                           WHERE    LeadId IS NOT NULL )
                                          );
        SET @ConfirmedWomenApplicantCount = (
                                              SELECT    COUNT(*)
                                              FROM      adLeads t1
                                              LEFT JOIN adGenders t3 ON t1.Gender = t3.GenderId
                                              LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race  
											-- For Degree Seeking --
                                              LEFT JOIN adDegCertSeeking t11 ON t1.DegCertSeekingId = t11.DegCertSeekingId
											-- For Degree Seeking -- 
                                              INNER JOIN syStatusCodes t5 ON t1.LeadStatus = t5.StatusCodeId
                                              INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId 
											--and t6.SysStatusId not in (7,8)
                                              INNER JOIN arPrgVersions t7 ON t1.PrgVerId = t7.PrgVerId
                                              INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId  
											--Areas Of Intrest --
                                              INNER JOIN arPrgGrp t12 ON t12.PrgGrpId = t7.PrgGrpId 
											--Areas Of Intrest --
                                              INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                              WHERE     t1.CampusId = @CampusId
                                                        AND t9.IPEDSValue = 58
                                                        AND ( t3.IPEDSValue = 31 ) -- Men 
                                                        AND ( t11.IPEDSValue = 11 ) -- First-Time Degree Certificate Seeking
                                                        AND t1.LeadId NOT IN ( SELECT DISTINCT
                                                                                        LeadId
                                                                               FROM     arStuEnrollments
                                                                               WHERE    LeadId IS NOT NULL )
                                            );
        SELECT  SUM(ApplicantMenCount) + @ConfirmedMenApplicantCount AS ApplicantMenCount
               ,SUM(AdmissionMenCount) AS AdmissionMenCount
               ,SUM(FullTimeCountMen) AS FullTimeCountMen
               ,SUM(PartTimeCountMen) AS PartTimeCountMen
               ,SUM(ApplicantWomenCount) + @ConfirmedWomenApplicantCount AS ApplicantWomenCount
               ,SUM(AdmissionWomenCount) AS AdmissionWomenCount
               ,SUM(FullTimeCountWomen) AS FullTimeCountWomen
               ,SUM(PartTimeCountWomen) AS PartTimeCountWomen
               ,
					
						--Totals columns - includes unknown genders--------------------------------------------------
                ( SUM(ApplicantMenCount) + @ConfirmedMenApplicantCount ) + ( SUM(ApplicantWomenCount) + @ConfirmedWomenApplicantCount )
                + SUM(tt.ApplicantUnknownCount) AS ApplicantTotal
               ,( SUM(AdmissionMenCount) ) + ( SUM(AdmissionWomenCount) ) + SUM(tt.AdmissionUnknownCount) AS AmissionTotal
               ,SUM(FullTimeCountMen) + SUM(FullTimeCountWomen) + SUM(FullTimeCountUnknown) AS FullTimeTotal
               ,SUM(PartTimeCountMen) + SUM(PartTimeCountWomen) + SUM(PartTimeCountUnknown) AS PartTimeTotal
						---------------------------------------------------------------------------------------------
        FROM    (
                  SELECT    t3.IPEDSValue
                           ,t2.StartDate
                           ,t3.IPEDSSequence AS GenderSequence
                           ,
									
										-- Men --
                            CASE WHEN ( t3.IPEDSValue = 30 ) THEN 'X'
                                 ELSE ''
                            END AS ApplicantMen
                           ,CASE WHEN ( t3.IPEDSValue = 30 ) THEN 'X'
                                 ELSE ''
                            END AS AdmissionMen
                           ,CASE WHEN ( t3.IPEDSValue = 30 ) THEN 1
                                 ELSE 0
                            END AS ApplicantMenCount
                           ,CASE WHEN ( t3.IPEDSValue = 30 ) THEN 1
                                 ELSE 0
                            END AS AdmissionMenCount
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 30
                                      ) THEN 'X'
                                 ELSE ''
                            END AS FullTimeMen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 30
                                      ) THEN 'X'
                                 ELSE ''
                            END AS PartTimeMen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 30
                                      ) THEN 1
                                 ELSE 0
                            END AS FullTimeCountMen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 30
                                      ) THEN 1
                                 ELSE 0
                            END AS PartTimeCountMen
                           ,
									
										-- Women --
                            CASE WHEN ( t3.IPEDSValue = 31 ) THEN 'X'
                                 ELSE ''
                            END AS ApplicantWomen
                           ,CASE WHEN ( t3.IPEDSValue = 31 ) THEN 'X'
                                 ELSE ''
                            END AS AdmissionWomen
                           ,CASE WHEN ( t3.IPEDSValue = 31 ) THEN 1
                                 ELSE 0
                            END AS ApplicantWomenCount
                           ,CASE WHEN ( t3.IPEDSValue = 31 ) THEN 1
                                 ELSE 0
                            END AS AdmissionWomenCount
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 31
                                      ) THEN 'X'
                                 ELSE ''
                            END AS FullTimeWomen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 31
                                      ) THEN 'X'
                                 ELSE ''
                            END AS PartTimeWomen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 31
                                      ) THEN 1
                                 ELSE 0
                            END AS FullTimeCountWomen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 31
                                      ) THEN 1
                                 ELSE 0
                            END AS PartTimeCountWomen
                           ,
									
										-- unknown --
                            CASE WHEN ( t3.IPEDSValue = 32 ) THEN 'X'
                                 ELSE ''
                            END AS ApplicantUnknown
                           ,CASE WHEN ( t3.IPEDSValue = 32 ) THEN 'X'
                                 ELSE ''
                            END AS AdmissionUnknown
                           ,CASE WHEN ( t3.IPEDSValue = 32 ) THEN 1
                                 ELSE 0
                            END AS ApplicantUnknownCount
                           ,CASE WHEN ( t3.IPEDSValue = 32 ) THEN 1
                                 ELSE 0
                            END AS AdmissionUnknownCount
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 32
                                      ) THEN 'X'
                                 ELSE ''
                            END AS FullTimeUnknown
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 32
                                      ) THEN 'X'
                                 ELSE ''
                            END AS PartTimeUnknown
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 32
                                      ) THEN 1
                                 ELSE 0
                            END AS FullTimeCountUnknown
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 32
                                      ) THEN 1
                                 ELSE 0
                            END AS PartTimeCountUnknown
                  FROM      arStudent t1
                  LEFT JOIN adGenders t3 ON t1.Gender = t3.GenderId
                  LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                  INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                  LEFT JOIN adDegCertSeeking t11 ON t2.DegCertSeekingId = t11.DegCertSeekingId  ---- For Degree Seeking --
                  INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                  INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                  INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                  INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId  
						
							--Areas Of Intrest --
                  INNER JOIN arPrgGrp t12 ON t12.PrgGrpId = t7.PrgGrpId 
					
							--Areas Of Intrest --
                  INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                  LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                  WHERE     t2.CampusId = @CampusId
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t9.IPEDSValue = 58
                            AND (
                                  t10.IPEDSValue = 61
                                  OR t10.IPEDSValue = 62
                                )
                            AND ( t3.IPEDSValue IN ( 30,31,32 ) )
                            AND t1.Race IS NOT NULL
                            AND t6.SysStatusId NOT IN ( 8 )  -- Exclude students who are No Start Students
                            AND t2.StartDate <= @EndDate
                            AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
										-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
										( SELECT    t1.StuEnrollId
                                          FROM      arStuEnrollments t1
                                                   ,syStatusCodes t2
                                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                                    AND StartDate <= @EndDate
                                                    AND LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                    AND (
                                                          @ProgId IS NULL
                                                          OR t8.ProgId IN ( SELECT  Val
                                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                        )
                                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
										
											-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                                    AND (
                                                          t1.DateDetermined < @StartDate
                                                          OR ExpGradDate < @StartDate
                                                          OR LDA < @StartDate
                                                        ) )
							---DE8492
							-- If Student is enrolled in only program version and if that program version 
							-- happens to be a continuing ed program exclude the student
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd )
							-- Transfer-In Students need to be excluded
                            AND t2.StuEnrollId NOT IN ( SELECT DISTINCT
                                                                StuEnrollId
                                                        FROM    arStuEnrollments
                                                        WHERE   CampusId = LTRIM(RTRIM(@CampusId))
                                                                AND LeadId IS NOT NULL
                                                                AND StuEnrollId IN ( SELECT StuEnrollId
                                                                                     FROM   arStuEnrollments
                                                                                     WHERE  TransferHours > 0
                                                                                            AND CampusId = LTRIM(RTRIM(@CampusId))
                                                                                     UNION
                                                                                     SELECT StuEnrollId
                                                                                     FROM   arTransferGrades
                                                                                     WHERE  GrdSysDetailId IN ( SELECT  GrdSysDetailId
                                                                                                                FROM    arGradeSystemDetails
                                                                                                                WHERE   IsTransferGrade = 1 ) ) )
                            AND ( t11.IPEDSValue = 11 ) -- Degree/Cert Seeking
						--and (@AreasOfInterest is null or t12.PrgGrpId in (Select Val from [MultipleValuesForReportParameters](@AreasOfInterest,',',1))) --Areas Of Intrest --
                ) tt;
    END;




GO
