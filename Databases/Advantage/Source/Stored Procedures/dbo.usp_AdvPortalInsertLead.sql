SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_AdvPortalInsertLead]
    @CampusId UNIQUEIDENTIFIER
   ,@ProgId UNIQUEIDENTIFIER
   ,@SourceTypeId VARCHAR(50) = NULL
   ,@PrevEduLvlId VARCHAR(50) = NULL
   ,@FirstName VARCHAR(50)
   ,@MiddleName VARCHAR(50) = NULL
   ,@LastName VARCHAR(50)
   ,@BirthDate DATETIME = NULL
   ,@GenderId VARCHAR(50) = NULL
   ,@PhoneNumber VARCHAR(50) = NULL
   ,@ForeignPhone BIT = 0
   ,@PhoneNumber2 VARCHAR(50) = NULL
   ,@ForeignPhone2 BIT = 0
   ,@Email VARCHAR(50)
   ,@AlternativeEmail VARCHAR(50) = NULL
   ,@Address1 VARCHAR(50) = NULL
   ,@Address2 VARCHAR(50) = NULL
   ,@City VARCHAR(50) = NULL
   ,@StateId VARCHAR(50) = NULL
   ,@OtherState VARCHAR(50) = NULL
   ,@Zip VARCHAR(50) = NULL
   ,@ForeignZip BIT = 0
   ,@CountyId VARCHAR(50) = NULL
   ,@CountryId VARCHAR(50) = NULL
   ,@RaceId VARCHAR(50) = NULL
   ,@NationalityId VARCHAR(50) = NULL
   ,@Citizen VARCHAR(50) = NULL
   ,@MaritalStatus VARCHAR(50) = NULL
   ,@FamilyIncome VARCHAR(50) = NULL
   ,@Children VARCHAR(50) = NULL
   ,@DrivLicStateID VARCHAR(50) = NULL
   ,@DrivLicNumber VARCHAR(50) = NULL
   ,@Comments VARCHAR(250) = NULL
   ,@LeadEntryMsg VARCHAR(150) OUTPUT
AS
    BEGIN
        SET NOCOUNT ON;

		--/* Check if lead is duplicate */

        SET @LeadEntryMsg = '';

        IF EXISTS ( SELECT  *
                    FROM    dbo.adLeads
                    WHERE   FirstName = @FirstName
                            AND LastName = @LastName )
            BEGIN
                SET @LeadEntryMsg = 'Your first and last name already exists in our system. Please contact school associate for further information.';
            END;
        ELSE
            IF EXISTS ( SELECT  *
                        FROM    dbo.adLeads
                        WHERE   FirstName = @FirstName
                                AND (
                                      HomeEmail = @Email
                                      OR WorkEmail = @Email
                                    ) )
                BEGIN
                    SET @LeadEntryMsg = 'Your first name and email address already exists in our system. Please contact school associate for futher information.';
                END;
            ELSE 
			-- ==========================================================
			-- Use transaction to do this. Two tables must be modified
			-- ==========================================================
                BEGIN TRY
                    BEGIN TRANSACTION T1;

					-- INSERT new lead into Leads table 
                    BEGIN 
                        INSERT  INTO dbo.adLeads
                                (
                                 FirstName
                                ,MiddleName
                                ,LastName
                                ,BirthDate
                                ,Gender
                                ,ModUser
                                ,ModDate
                                ,Phone
                                ,ForeignPhone
                                ,Phone2
                                ,ForeignPhone2
                                ,HomeEmail
                                ,WorkEmail
                                ,Address1
                                ,Address2
                                ,City
                                ,StateId
                                ,OtherState
                                ,Zip
                                ,ForeignZip
                                ,County
                                ,LeadStatus
                                ,AdmissionsRep
                                ,AssignedDate
                                ,SourceCategoryID
                                ,SourceTypeID
                                ,SourceDate
                                ,ProgramID
                                ,AreaID
                                ,Comments
                                ,CampusId
                                ,Country
                                ,PreviousEducation
                                ,Race
                                ,Nationality
                                ,Citizen
                                ,MaritalStatus
                                ,FamilyIncome
                                ,Children
                                ,DrivLicStateID
                                ,DrivLicNumber
							    )
                        VALUES  (
                                 @FirstName
                                ,@MiddleName
                                ,@LastName
                                ,@BirthDate
                                ,@GenderId
                                ,'sa'
                                ,GETDATE()
                                ,CASE WHEN ( @PhoneNumber <> '' ) THEN @PhoneNumber
                                      ELSE NULL
                                 END
                                ,@ForeignPhone
                                ,CASE WHEN ( @PhoneNumber2 <> '' ) THEN @PhoneNumber2
                                      ELSE NULL
                                 END
                                ,@ForeignPhone2
                                ,@Email
                                ,CASE WHEN ( @AlternativeEmail <> '' ) THEN @AlternativeEmail
                                      ELSE NULL
                                 END
                                ,CASE WHEN ( @Address1 <> '' ) THEN @Address1
                                      ELSE NULL
                                 END
                                ,CASE WHEN ( @Address2 <> '' ) THEN @Address2
                                      ELSE NULL
                                 END
                                ,CASE WHEN ( @City <> '' ) THEN @City
                                      ELSE NULL
                                 END
                                ,CASE WHEN ( @StateId = '' ) THEN NULL
                                      ELSE @StateId
                                 END
                                ,@OtherState
                                ,CASE WHEN ( @Zip <> '' ) THEN @Zip
                                      ELSE NULL
                                 END
                                ,@ForeignZip
                                ,CASE WHEN ( @CountyId <> '' ) THEN @CountyId
                                      ELSE NULL
                                 END
                                ,(
                                   SELECT   UPPER(LeadStatusId)
                                   FROM     dbo.syCampuses
                                   WHERE    CampusId = @CampusId
                                 )
                                ,(
                                   SELECT   UPPER(AdmRepId)
                                   FROM     dbo.syCampuses
                                   WHERE    CampusId = @CampusId
                                 )
                                ,GETDATE()
                                ,CASE WHEN ( @SourceTypeId = '' ) THEN NULL
                                      ELSE (
                                             SELECT SourceCatagoryId
                                             FROM   dbo.adSourceType
                                             WHERE  SourceTypeId = @SourceTypeId
                                           )
                                 END
                                ,CASE WHEN ( @SourceTypeId = '' ) THEN NULL
                                      ELSE @SourceTypeId
                                 END
                                ,GETDATE()
                                ,UPPER(@ProgId)
                                ,(
                                   SELECT   PrgGrpId
                                   FROM     dbo.arPrgGrp
                                   WHERE    PrgGrpId IN ( SELECT TOP 1
                                                                    PrgGrpId
                                                          FROM      dbo.arPrgVersions
                                                          WHERE     ProgId = @ProgId )
                                 )
                                ,@Comments
                                ,UPPER(@CampusId)
                                ,CASE WHEN ( @CountryId = '' ) THEN NULL
                                      ELSE UPPER(@CountryId)
                                 END
                                ,CASE WHEN ( @PrevEduLvlId = '' ) THEN NULL
                                      ELSE UPPER(@PrevEduLvlId)
                                 END
                                ,CASE WHEN ( @RaceId <> '' ) THEN @RaceId
                                      ELSE NULL
                                 END
                                ,CASE WHEN ( @NationalityId <> '' ) THEN @NationalityId
                                      ELSE NULL
                                 END
                                ,CASE WHEN ( @Citizen <> '' ) THEN @Citizen
                                      ELSE NULL
                                 END
                                ,CASE WHEN ( @MaritalStatus <> '' ) THEN @MaritalStatus
                                      ELSE NULL
                                 END
                                ,CASE WHEN ( @FamilyIncome <> '' ) THEN @FamilyIncome
                                      ELSE NULL
                                 END
                                ,CASE WHEN ( @Children <> '' ) THEN @Children
                                      ELSE NULL
                                 END
                                ,CASE WHEN ( @DrivLicStateID <> '' ) THEN @DrivLicStateID
                                      ELSE NULL
                                 END
                                ,CASE WHEN ( @DrivLicNumber <> '' ) THEN @DrivLicNumber
                                      ELSE NULL
                                 END
                                );

					-- INSERT new prospect status into LeadStatusesChanges table 

                        INSERT  INTO syLeadStatusesChanges
                                (
                                 StatusChangeId
                                ,LeadId
                                ,OrigStatusId
                                ,NewStatusId
                                ,ModUser
                                ,ModDate
							    )
                        VALUES  (
                                 NEWID()
                                ,(
                                   SELECT   LeadId
                                   FROM     adLeads
                                   WHERE    FirstName = @FirstName
                                            AND LastName = @LastName
                                            AND HomeEmail = @Email
                                            AND ProgramID = UPPER(@ProgId)
                                 )
                                ,NULL
                                ,(
                                   SELECT   UPPER(LeadStatusId)
                                   FROM     dbo.syCampuses
                                   WHERE    CampusId = @CampusId
                                 )
                                ,'sa'
                                ,GETDATE()
                                );
                        SET @LeadEntryMsg = 'Your request for ''' + (
                                                                      SELECT    ProgDescrip
                                                                      FROM      arPrograms
                                                                      WHERE     ProgId = @ProgId
                                                                    ) + ''' program is received. You will receive a confirmation email shortly.';
                    END;

                    COMMIT TRANSACTION T1;
                END TRY
                BEGIN CATCH
                    IF @@TRANCOUNT > 0
                        ROLLBACK TRANSACTION T1;
                    SET @LeadEntryMsg = ERROR_MESSAGE();

                END CATCH;

        SELECT  @LeadEntryMsg AS LeadEntryMsg; 
    END;


GO
