SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[USP_GetPendingReqsByLeadandPrgVersion_Enroll]
    (
     @LeadID UNIQUEIDENTIFIER     
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	03/22/2011
    
	Procedure Name	:	[USP_GetPendingReqsByLeadandPrgVersion_Enroll]

	Objective		:	Gets the requirement, Startdate, Enddate, Doc Status,Passed the req or not based on Minscore obtained compared against actual score, Override status.
		
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						LeadID			In		UniqueIdentifier
	
	Output			:	Returns the requested details	
						
*/-----------------------------------------------------------------------------------------------------
    BEGIN

        DECLARE @ActiveStatusID AS UNIQUEIDENTIFIER;
        SET @ActiveStatusID = (
                                SELECT  StatusID
                                FROM    syStatuses
                                WHERE   Status = 'Active'
                              ); 
                              

        SELECT DISTINCT
                adReqId
               ,Descrip
               ,ReqGrpId
               ,StartDate
               ,EndDate
               ,Required
               ,CASE WHEN DocStatusDescrip = 'Approved' THEN 'Yes'
                     ELSE 'No'
                END AS DocStatusDescrip
               ,ReqTypeDescrip
               ,CASE WHEN ActualScore >= MinScore THEN 'True'
                     ELSE 'False'
                END AS Pass
               ,CASE WHEN (
                            SELECT  Override
                            FROM    adEntrTestOverRide
                            WHERE   EntrTestId = adReqId
                                    AND LeadId = @LeadID
                          ) = 1 THEN 'True'
                     ELSE 'False'
                END AS OverRide
        FROM    (
                  SELECT   DISTINCT
                            adReqId
                           ,Descrip
                           ,ReqGrpId
                           ,StartDate
                           ,EndDate
                           ,Required
                           ,DocStatusDescrip
                           ,ReqTypeDescrip
                           ,ActualScore
                           ,MinScore
                  FROM      (
                              SELECT DISTINCT
                                        t1.adReqId
                                       ,t1.Descrip
                                       ,CASE WHEN (
                                                    SELECT  COUNT(*)
                                                    FROM    adReqGroups
                                                    WHERE   IsMandatoryReqGrp = 1
                                                  ) >= 1 THEN (
                                                                SELECT  ReqGrpId
                                                                FROM    adReqGroups
                                                                WHERE   IsMandatoryReqGrp = 1
                                                              )
                                             ELSE '00000000-0000-0000-0000-000000000000'
                                        END AS ReqGrpId
                                       ,GETDATE() AS CurrentDate
                                       ,t2.StartDate
                                       ,t2.EndDate
                                       ,1 AS Required
                                       ,(
                                          SELECT DISTINCT
                                                    s1.DocStatusDescrip
                                          FROM      sySysDocStatuses s1
                                                   ,syDocStatuses s2
                                                   ,adLeadDocsReceived s3
                                          WHERE     s1.SysDocStatusId = s2.sysDocStatusId
                                                    AND s2.DocStatusId = s3.DocStatusId
                                                    AND s3.LeadId = @LeadID
                                                    AND s3.DocumentId = t1.adReqId
                                        ) AS DocStatusDescrip
                                       ,t3.Descrip AS ReqTypeDescrip
                                       ,(
                                          SELECT    ActualScore
                                          FROM      adLeadEntranceTest
                                          WHERE     LeadId = @LeadID
                                                    AND EntrTestId = t1.adReqId
                                        ) AS ActualScore
                                       ,t2.MinScore
                         
                                -- ,
                                --( SELECT    MinScore
                                --  FROM      adLeadEntranceTest
                                --  WHERE     LeadId = @LeadID
                                --            AND EntrTestId = t1.adReqId
                                --) AS MinScore
                              FROM      adReqs t1
                                       ,adReqsEffectiveDates t2
                                       ,adReqTypes t3
                              WHERE     t1.adReqId = t2.adReqId
                                        AND t1.adReqTypeId = t3.adReqTypeId
                                        AND t2.MandatoryRequirement = 1
                                        AND t1.adReqTypeId IN ( 1,3 )
                                        AND t1.ReqforEnrollment = 1
                                        AND StatusId = @ActiveStatusID
                            ) R1
                  WHERE     R1.CurrentDate >= R1.StartDate
                            AND (
                                  R1.CurrentDate <= R1.EndDate
                                  OR R1.EndDate IS NULL
                                )
                  UNION
                  SELECT DISTINCT
                            adReqId
                           ,Descrip
                           ,reqGrpId
                           ,StartDate
                           ,EndDate
                           ,Required
                           ,DocStatusDescrip
                           ,ReqTypeDescrip
                           ,ActualScore
                           ,MinScore
                  FROM      (
                              SELECT DISTINCT
                                        t1.adReqId
                                       ,t1.Descrip
                                       ,'00000000-0000-0000-0000-000000000000' AS reqGrpId
                                       ,GETDATE() AS CurrentDate
                                       ,t2.StartDate
                                       ,t2.EndDate
                                       ,1 AS Required
                                       ,(
                                          SELECT DISTINCT
                                                    s1.DocStatusDescrip
                                          FROM      sySysDocStatuses s1
                                                   ,syDocStatuses s2
                                                   ,adLeadDocsReceived s3
                                          WHERE     s1.SysDocStatusId = s2.sysDocStatusId
                                                    AND s2.DocStatusId = s3.DocStatusId
                                                    AND s3.LeadId = @LeadID
                                                    AND s3.DocumentId = t1.adReqId
                                        ) AS DocStatusDescrip
                                       ,t6.Descrip AS ReqTypeDescrip
                                       ,(
                                          SELECT    ActualScore
                                          FROM      adLeadEntranceTest
                                          WHERE     LeadId = @LeadID
                                                    AND EntrTestId = t1.adReqId
                                        ) AS ActualScore
                                       ,t2.MinScore
                                -- ,
                                --( SELECT    MinScore
                                --  FROM      adLeadEntranceTest
                                --  WHERE     LeadId = @LeadID
                                --            AND EntrTestId = t1.adReqId
                                --) AS MinScore
                              FROM      adReqs t1
                                       ,adReqsEffectiveDates t2
                                       ,adReqLeadGroups t3
                                       ,adLeads t4
                                       ,adPrgVerTestDetails t5
                                       ,adReqTypes t6
                              WHERE     t1.adReqId = t2.adReqId
                                        AND t2.MandatoryRequirement <> 1
                                        AND t1.adReqTypeId IN ( 1,3 )
                                        AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                                        AND t4.PrgVerId = t5.PrgVerId
                                        AND t1.adReqId = t5.adReqId
                                        AND t1.adReqTypeId = t6.adReqTypeId
                                        AND t5.PrgVerId IN ( SELECT DISTINCT
                                                                    PrgVerId
                                                             FROM   adLeads
                                                             WHERE  LeadId = @LeadID )
                                        --AND t3.LeadGrpId IN (
                                        --SELECT DISTINCT
                                        --        LeadGrpId
                                        --FROM    adLeadByLeadGroups
                                        --WHERE   LeadId = @LeadID )
                                        AND t5.adReqId IS NOT NULL
                                        AND t1.ReqforEnrollment = 1
                                        AND StatusId = @ActiveStatusID
                            ) R1
                  WHERE     R1.CurrentDate >= R1.StartDate
                            AND (
                                  R1.CurrentDate <= R1.EndDate
                                  OR R1.EndDate IS NULL
                                )
                  UNION
                  SELECT  DISTINCT
                            adReqId
                           ,Descrip
                           ,reqGrpId
                           ,StartDate
                           ,EndDate
                           ,Required
                           ,DocStatusDescrip
                           ,ReqTypeDescrip
                           ,ActualScore
                           ,MinScore
                  FROM      (
                              SELECT DISTINCT
                                        t1.adReqId
                                       ,t1.Descrip
                                       ,'00000000-0000-0000-0000-000000000000' AS reqGrpId
                                       ,GETDATE() AS CurrentDate
                                       ,t2.StartDate
                                       ,t2.EndDate
                                       ,1 AS Required
                                       ,(
                                          SELECT DISTINCT
                                                    s1.DocStatusDescrip
                                          FROM      sySysDocStatuses s1
                                                   ,syDocStatuses s2
                                                   ,adLeadDocsReceived s3
                                          WHERE     s1.SysDocStatusId = s2.sysDocStatusId
                                                    AND s2.DocStatusId = s3.DocStatusId
                                                    AND s3.LeadId = @LeadID
                                                    AND s3.DocumentId = t1.adReqId
                                        ) AS DocStatusDescrip
                                       ,t8.Descrip AS ReqTypeDescrip
                                       ,(
                                          SELECT    ActualScore
                                          FROM      adLeadEntranceTest
                                          WHERE     LeadId = @LeadID
                                                    AND EntrTestId = t1.adReqId
                                        ) AS ActualScore
                                       ,t2.MinScore
                                --,
                                --( SELECT    MinScore
                                --  FROM      adLeadEntranceTest
                                --  WHERE     LeadId = @LeadID
                                --            AND EntrTestId = t1.adReqId
                                --) AS MinScore
                              FROM      adReqs t1
                                       ,adReqsEffectiveDates t2
                                       ,adReqLeadGroups t3
                                       ,adPrgVerTestDetails t5
                                       ,adReqGrpDef t6
                                       ,adLeadByLeadGroups t7
                                       ,adReqTypes t8
                              WHERE     t1.adReqId = t2.adReqId
                                        AND t1.adReqTypeId IN ( 1,3 )
                                        AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                                        AND t5.ReqGrpId = t6.ReqGrpId
                                        AND t3.LeadGrpId = t7.LeadGrpId
                                        AND t6.LeadGrpId = t7.LeadGrpId
                                        AND t1.adReqId = t6.adReqId
                                        AND t1.adReqTypeId = t8.adReqTypeId
                                        AND t5.ReqGrpId IS NOT NULL
                                        AND t7.LeadId = @LeadID
                                        AND t5.PrgVerId IN ( SELECT DISTINCT
                                                                    PrgVerId
                                                             FROM   adLeads
                                                             WHERE  LeadId = @LeadID )
                                        AND t1.ReqforEnrollment = 1
                                        AND StatusId = @ActiveStatusID
                            ) R1
                  WHERE     R1.CurrentDate >= R1.StartDate
                            AND (
                                  R1.CurrentDate <= R1.EndDate
                                  OR R1.EndDate IS NULL
                                )
                  UNION
                  SELECT DISTINCT
                            adReqId
                           ,Descrip
                           ,ReqGrpId
                           ,StartDate
                           ,EndDate
                           ,Required
                           ,DocStatusDescrip
                           ,ReqTypeDescrip
                           ,ActualScore
                           ,MinScore
                  FROM      (
                              SELECT DISTINCT
                                        t1.adReqId
                                       ,t1.Descrip
                                       ,'00000000-0000-0000-0000-000000000000' AS ReqGrpId
                                       ,GETDATE() AS CurrentDate
                                       ,t2.StartDate
                                       ,t2.EndDate
                                       ,t3.IsRequired AS Required
                                       ,(
                                          SELECT DISTINCT
                                                    s1.DocStatusDescrip
                                          FROM      sySysDocStatuses s1
                                                   ,syDocStatuses s2
                                                   ,adLeadDocsReceived s3
                                          WHERE     s1.SysDocStatusId = s2.sysDocStatusId
                                                    AND s2.DocStatusId = s3.DocStatusId
                                                    AND s3.LeadId = @LeadID
                                                    AND s3.DocumentId = t1.adReqId
                                        ) AS DocStatusDescrip
                                       ,t4.Descrip AS ReqTypeDescrip
                                       ,(
                                          SELECT    ActualScore
                                          FROM      adLeadEntranceTest
                                          WHERE     LeadId = @LeadID
                                                    AND EntrTestId = t1.adReqId
                                        ) AS ActualScore
                                       ,t2.MinScore
                                -- ,
                                --( SELECT    MinScore
                                --  FROM      adLeadEntranceTest
                                --  WHERE     LeadId = @LeadID
                                --            AND EntrTestId = t1.adReqId
                                --) AS MinScore
                              FROM      adReqs t1
                                       ,adReqsEffectiveDates t2
                                       ,adReqLeadGroups t3
                                       ,adReqTypes t4
                              WHERE     t1.adReqId = t2.adReqId
                                        AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                                        AND t1.adReqTypeId = t4.adReqTypeId
                                        AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                                        s1.adReqId
                                                                FROM    adReqGrpDef s1
                                                                       ,adPrgVerTestDetails s2
                                                                WHERE   s1.ReqGrpId = s2.ReqGrpId
                                                                        AND s2.PrgVerId IN ( SELECT DISTINCT
                                                                                                    PrgVerId
                                                                                             FROM   adLeads
                                                                                             WHERE  LeadId = @LeadID )
                                                                        AND s2.adReqId IS NULL )
                                        AND t3.LeadGrpId IN ( SELECT    LeadGrpId
                                                              FROM      adLeadByLeadGroups
                                                              WHERE     LeadId = @LeadID )
                                        AND t1.adReqTypeId IN ( 1,3 )
                                        AND t2.MandatoryRequirement <> 1
                                        AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                                        adReqId
                                                                FROM    adPrgVerTestDetails
                                                                WHERE   PrgVerId IN ( SELECT DISTINCT
                                                                                                PrgVerId
                                                                                      FROM      adLeads
                                                                                      WHERE     LeadId = @LeadID ) )
                                        AND t1.ReqforEnrollment = 1
                                        AND StatusId = @ActiveStatusID
                            ) R1
                  WHERE     R1.CurrentDate >= R1.StartDate
                            AND (
                                  R1.CurrentDate <= R1.EndDate
                                  OR R1.EndDate IS NULL
                                )
                ) R2
        ORDER BY ReqTypeDescrip
               ,Descrip; 
        
        
    END;




GO
