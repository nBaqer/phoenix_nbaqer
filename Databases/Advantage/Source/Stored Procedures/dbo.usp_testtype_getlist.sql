SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_testtype_getlist]
AS
    SELECT DISTINCT
            GrdComponentTypeId
           ,Descrip
           ,CampGrpId
    FROM    arGrdComponentTypes
    WHERE   SysComponentTypeId = 612
    ORDER BY Descrip;



GO
