SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_IPEDS_BuildList] @pResourceId INT
AS
    SELECT DISTINCT
            t3.RptAgencyFldValId
           ,t3.AgencyDescrip
    FROM    syRptFields t1
    INNER JOIN syRptAgencyFields t2 ON t1.RptFldId = t2.RptFldId
    INNER JOIN dbo.syRptAgencyFldValues t3 ON t2.RptAgencyFldId = t3.RptAgencyFldId
    INNER JOIN syBridge_ResourceId_RptFldId t4 ON t2.RptFldId = t4.RptFldId
    WHERE   t2.RptAgencyId = 1
            AND t4.ResourceId = @pResourceId
    ORDER BY AgencyDescrip; 



GO
