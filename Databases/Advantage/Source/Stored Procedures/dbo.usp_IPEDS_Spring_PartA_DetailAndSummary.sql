SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/****
select * from arStudent where lastname='soares'
select * from arStuEnrollments where StudentId='17878602-70AF-497D-8DC8-49671BDE0BEC'

EXEC usp_IPEDS_Spring_PartA_DetailAndSummary '3F5E839A-589A-4B2A-B258-35A1A8B3B819',NULL,'2009','ssn','07/01/2009','06/30/2010','academic',1,null,
'10/15/2009'

EXEC usp_IPEDS_Spring_PartA_DetailAndSummary '3F5E839A-589A-4B2A-B258-35A1A8B3B819',NULL,'2010','ssn','07/01/2010','06/30/2011','program',1,'e711cc07-3806-4e01-ac15-8788bbdc0e4f',
'06/30/2011'

EXEC [usp_IPEDS_Spring_PartD_DetailAndSummary_Detail] '3f5e839a-589a-4b2a-b258-35a1a8b3b819',NULL,'2009','ssn','07/01/2009','6/30/2010',
'academic',1,'e711cc07-3806-4e01-ac15-8788bbdc0e4f','10/15/2009'
select * from arprograms
select * from sycampuses
EXEC usp_IPEDS_Spring_PartA_DetailAndSummary '48774CFE-52CC-488F-9DDD-000663942E9E',NULL,'2010','ssn','07/01/2010','06/30/2011','program',1,'D156394F-B4A4-422D-ADF8-4CA7EEF4E0E6',
'06/30/2011'
EXEC usp_IPEDS_Spring_PartA_DetailAndSummary '436BF45E-5420-4C1F-8687-CD32A60D64BE',NULL,'2010','ssn','07/01/2010','06/30/2011','program',1,'BFFAD764-DB97-42E8-8304-BB57BFE8EF81',
'06/30/2011'
*/

CREATE PROCEDURE [dbo].[usp_IPEDS_Spring_PartA_DetailAndSummary]
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@CohortYear VARCHAR(10) = NULL
   ,@OrderBy VARCHAR(100)
   ,@StartDate DATETIME
   ,@EndDate DATETIME
   ,@SchoolType VARCHAR(50)
   ,@InstitutionType VARCHAR(50)
   ,@LargestProgramID VARCHAR(50)
   ,@EndDateAcademic DATETIME  --NEW DD Rev 12/12/11
AS
    DECLARE @ReturnValue VARCHAR(100)
       ,@Value VARCHAR(100);  
    DECLARE @SSN VARCHAR(10)
       ,@FirstName VARCHAR(100)
       ,@LastName VARCHAR(100)
       ,@StudentNumber VARCHAR(50)
       ,@TransferredOut INT
       ,@Exclusions INT;  
    DECLARE @CitizenShip_IPEDSValue INT
       ,@ProgramType_IPEDSValue INT
       ,@Gender_IPEDSValue INT
       ,@FullTimePartTime_IPEDSValue INT
       ,@StudentId UNIQUEIDENTIFIER;   
  
    DECLARE @ProgramName VARCHAR(100)
       ,@StudentCount INT;
    DECLARE @LargeProgramName VARCHAR(100);
    DECLARE @AcademicEndDate DATETIME; 
    --12/05/2012 DE8824 - Academic year options should have a cutoff start date
    DECLARE @AcademicStartDate DATETIME;

--CREATE TABLE #LargestProgram
--(LargestProgramID VARCHAR(50), LargeProgramName VARCHAR(100))

    IF LOWER(@SchoolType) = 'academic'
        BEGIN
            SET @AcademicEndDate = @EndDateAcademic;
			--12/05/2012 DE8824 - Academic year options should have a cutoff start date
            SET @AcademicStartDate = DATEADD(YEAR,-1,@EndDateAcademic); 
        END;
    ELSE
        BEGIN
            SET @AcademicEndDate = @EndDate;          
        END;


    PRINT LOWER(@SchoolType); 

    BEGIN

--Here, we want to get the largest program from the programs selected.
        BEGIN
            SET @LargeProgramName = (
                                      SELECT    ProgDescrip
                                      FROM      dbo.arPrograms
                                      WHERE     ProgId = @LargestProgramID
                                    );
		--INSERT INTO #LargestProgram

			--SELECT TOP 1 * FROM
			--	(
			--	SELECT t3.ProgId, t3.ProgDescrip, COUNT(t1.StuEnrollId) AS StudentCount
			--	FROM dbo.arStuEnrollments t1, dbo.arPrgVersions t2, dbo.arPrograms t3
			--	WHERE t1.PrgVerId = t2.PrgVerId AND t2.ProgId = t3.ProgId AND
			--	(@progId IS NULL or t3.ProgId IN (Select Val from [MultipleValuesForReportParameters](@ProgId,',',1))) AND
			--	LTRIM(RTRIM(t1.CampusId))= LTRIM(RTRIM(@CampusId)) AND 
	  --t1.StartDate<=@EndDate   
   --   and StuEnrollId not in  
   --   -- Exclude students who are Dropped out/Transferred/Graduated/No Start   
   --   -- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate  
   --   (  
   --    Select t1.StuEnrollId from arStuEnrollments t1,SyStatusCodes t2, dbo.arPrgVersions t3, dbo.arPrograms t4   
   --    where t1.StatusCodeId=t2.StatusCodeId and StartDate <=@EndDate and -- Student started before the end date range  
   --    LTRIM(RTRIM(t1.CampusId))= LTRIM(RTRIM(@CampusId)) and  
   --    t1.PrgVerId = t3.PrgVerId AND t3.ProgId = t4.ProgId AND
	  -- (@progId IS NULL or t3.ProgId IN (Select Val from [MultipleValuesForReportParameters](@ProgId,',',1))) AND    
   --    t2.SysStatusId in (12,14,19,8) -- Dropped out/Transferred/Graduated/No Start  
   --    -- Date Determined or ExpGradDate or LDA falls before 08/31/2010  
   --    and (t1.DateDetermined<@StartDate or ExpGradDate<@StartDate or LDA<@StartDate)  
   --   )   
   --   -- If Student is enrolled in only one program version and if that program version   
   --   -- happens to be a continuing ed program exclude the student  
   --                     and t1.StudentId not in   
   --                                (  
   --                                       Select Distinct StudentId from  
   --                                       (  
   --                                             select StudentId,Count(*) as RowCounter from arStuENrollments   
   --                                             where PrgVerId in (select PrgVerId from arPrgVersions where IsContinuingEd=1)  
   --                                             Group by StudentId Having Count(*)=1  
   --                                       ) dtStudent_ContinuingEd  
   --                                 )
			--	GROUP BY t3.ProgId, t3.ProgDescrip
			--	) D1
			--ORDER BY D1.StudentCount DESC	
			
			--SELECT * FROM #LargestProgram
			--SELECT @LargeProgramName AS LargeProgramName
            SET @LargeProgramName = (
                                      SELECT    @LargeProgramName
                                    );
        END;
    
    END; 

--SET @LargestProgramID = (SELECT TOP 1 LargestProgramID FROM #LargestProgram)
  
-- Check if School tracks grades by letter or numeric   
    SET @Value = (
                   SELECT TOP 1
                            Value
                   FROM     dbo.syConfigAppSetValues
                   WHERE    SettingId = 47
                 );  
  
    IF @ProgId IS NOT NULL
        BEGIN  
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.ProgId IN ( SELECT   Val
                                   FROM     MultipleValuesForReportParameters(@ProgId,',',1) );  
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);  
        END;  
    ELSE
        BEGIN  
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND t1.CampGrpId IN ( SELECT    CampGrpId
                                          FROM      syCmpGrpCmps
                                          WHERE     CampusId = @CampusId );  
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);  
        END;  
  
/*********Get the list of students that will be shown in the report - Starts Here ******************/     
    CREATE TABLE #StudentsList
        (
         StudentId UNIQUEIDENTIFIER
        ,SSN VARCHAR(10)
        ,FirstName VARCHAR(100)
        ,LastName VARCHAR(100)
        ,MiddleName VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,TransferredOut INT
        ,Exclusions INT
        ,CitizenShip_IPEDSValue INT
        ,ProgramType_IPEDSValue INT
        ,Gender_IPEDSValue INT
        ,FullTimePartTime_IPEDSValue INT
        ,GenderId UNIQUEIDENTIFIER
        ,RaceId UNIQUEIDENTIFIER
        ,CitizenId UNIQUEIDENTIFIER
        ,GenderDescription VARCHAR(50)
        ,RaceDescription VARCHAR(50)
        ,StudentGraduatedStatusCount INT
        ,ClockHourProgramCount INT
        );  
    
-- The following table will store only Full Time First Time UnderGraduate Students  
    CREATE TABLE #UnderGraduateStudentsWhoReceivedFinancialAid
        (
         StudentId UNIQUEIDENTIFIER
        ,NetAmount DECIMAL(18,2)
        ,FinancialAidType INT
        ,TitleIV BIT
        ,AdvFundSourceId INT
        );  
  
  
-- Get the list of UnderGraduate Students 

    IF LOWER(@SchoolType) = 'academic'
        BEGIN
            PRINT 'academic';
            INSERT  INTO #StudentsList
                    SELECT DISTINCT
                            t1.StudentId
                           ,t1.SSN
                           ,t1.FirstName
                           ,t1.LastName
                           ,t1.MiddleName
                           ,t1.StudentNumber
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SQ1
                                       ,syStatusCodes SQ2
                                       ,dbo.sySysStatus SQ3
                              WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                        AND SQ2.SysStatusId = SQ3.SysStatusId
                                        AND SQ1.StudentId = t1.StudentId
                                        AND SQ3.SysStatusId = 19
                                        AND SQ1.TransferDate <= @AcademicEndDate
                            ) AS TransferredOut
                           ,(  
     -- Check if the student was either dropped and if the drop reason is either  
     -- deceased, active duty, foreign aid service, church mission  
                              CASE WHEN (
                                          SELECT    COUNT(*)
                                          FROM      arStuEnrollments SQ1
                                                   ,syStatusCodes SQ2
                                                   ,dbo.sySysStatus SQ3
                                                   ,arDropReasons SQ44
                                          WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                                    AND SQ2.SysStatusId = SQ3.SysStatusId
                                                    AND SQ1.DropReasonId = SQ44.DropReasonId
                                                    AND SQ1.StudentId = t1.StudentId
                                                    AND SQ3.SysStatusId IN ( 12 ) -- Dropped  
                                                    AND SQ1.DateDetermined <= @AcademicEndDate
                                                    AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                        ) >= 1 THEN 1
                                   ELSE 0
                              END ) AS Exclusions
                           ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                           ,t9.IPEDSValue AS ProgramType_IPEDSValue
                           ,t3.IPEDSValue AS Gender_IPEDSValue
                           ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                           ,t1.Gender
                           ,t1.Race
                           ,t1.Citizen
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t3.IPEDSValue
                            ) AS GenderDescription
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t4.IPEDSValue
                            ) AS RaceDescription
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,syStatusCodes SC
                              WHERE     SEC.StudentId = t1.StudentId
                                        AND SEC.StatusCodeId = SC.StatusCodeId
                                        AND SC.SysStatusId = 14
                            ) AS StudentGraduatedStatusCount
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,arPrograms P
                                       ,arPrgVersions PV
                              WHERE     SEC.PrgVerId = PV.PrgVerId
                                        AND P.ProgId = PV.ProgId
                                        AND SEC.StudentId = t1.StudentId
                                        AND P.ACId = 5
                            ) AS ClockHourProgramCount
                    FROM    adGenders t3
                    LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                    LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                    INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                    INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                 AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students  
                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                    LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.DegCertSeekingId = t12.DegCertSeekingId
                    INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                    WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t9.IPEDSValue = 58
                            AND -- Under Graduate  (SFA Part A - Only UnderGraduate Students)
                            t2.StartDate <= @AcademicEndDate
                            AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start   
      -- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate  
      ( SELECT  t1.StuEnrollId
        FROM    arStuEnrollments t1
               ,syStatusCodes t2
        WHERE   t1.StatusCodeId = t2.StatusCodeId
                AND StartDate <= @AcademicEndDate
                AND -- Student started before the end date range  
                LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                AND (
                      @ProgId IS NULL
                      OR t8.ProgId IN ( SELECT  Val
                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                    )
                AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start  
       -- Date Determined or ExpGradDate or LDA falls before 08/31/2010 
                AND (
                      t1.DateDetermined < @AcademicEndDate
                      OR ExpGradDate < @AcademicEndDate
                      OR LDA < @AcademicEndDate
                    ) )
                                     
      -- If Student is enrolled in only one program version and if that program version   
      -- happens to be a continuing ed program exclude the student  
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd );  

        END;

    IF LOWER(@SchoolType) = 'program'
        BEGIN

            INSERT  INTO #StudentsList
                    SELECT DISTINCT
                            t1.StudentId
                           ,t1.SSN
                           ,t1.FirstName
                           ,t1.LastName
                           ,t1.MiddleName
                           ,t1.StudentNumber
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SQ1
                                       ,syStatusCodes SQ2
                                       ,dbo.sySysStatus SQ3
                              WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                        AND SQ2.SysStatusId = SQ3.SysStatusId
                                        AND SQ1.StudentId = t1.StudentId
                                        AND SQ3.SysStatusId = 19
                                        AND SQ1.TransferDate <= @EndDate
                            ) AS TransferredOut
                           ,(  
     -- Check if the student was either dropped and if the drop reason is either  
     -- deceased, active duty, foreign aid service, church mission  
                              CASE WHEN (
                                          SELECT    COUNT(*)
                                          FROM      arStuEnrollments SQ1
                                                   ,syStatusCodes SQ2
                                                   ,dbo.sySysStatus SQ3
                                                   ,arDropReasons SQ44
                                          WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                                    AND SQ2.SysStatusId = SQ3.SysStatusId
                                                    AND SQ1.DropReasonId = SQ44.DropReasonId
                                                    AND SQ1.StudentId = t1.StudentId
                                                    AND SQ3.SysStatusId IN ( 12 ) -- Dropped  
                                                    AND SQ1.DateDetermined <= @EndDate
                                                    AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                        ) >= 1 THEN 1
                                   ELSE 0
                              END ) AS Exclusions
                           ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                           ,t9.IPEDSValue AS ProgramType_IPEDSValue
                           ,t3.IPEDSValue AS Gender_IPEDSValue
                           ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                           ,t1.Gender
                           ,t1.Race
                           ,t1.Citizen
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t3.IPEDSValue
                            ) AS GenderDescription
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t4.IPEDSValue
                            ) AS RaceDescription
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,syStatusCodes SC
                              WHERE     SEC.StudentId = t1.StudentId
                                        AND SEC.StatusCodeId = SC.StatusCodeId
                                        AND SC.SysStatusId = 14
                            ) AS StudentGraduatedStatusCount
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,arPrograms P
                                       ,arPrgVersions PV
                              WHERE     SEC.PrgVerId = PV.PrgVerId
                                        AND P.ProgId = PV.ProgId
                                        AND SEC.StudentId = t1.StudentId
                                        AND P.ACId = 5
                            ) AS ClockHourProgramCount
                    FROM    adGenders t3
                    LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                    LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                    INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                    INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                 AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students  
                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                    LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.DegCertSeekingId = t12.DegCertSeekingId
                    INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                    WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t9.IPEDSValue = 58
                            AND -- Under Graduate  (SFA Part A - Only UnderGraduate Students)
                        --(t2.StartDate>=@StartDate AND t2.StartDate<=@AcademicEndDate)
                            ( t2.StartDate <= @AcademicEndDate )
                            AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start   
      -- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate  
      ( SELECT  t1.StuEnrollId
        FROM    arStuEnrollments t1
               ,syStatusCodes t2
        WHERE   t1.StatusCodeId = t2.StatusCodeId
                AND StartDate <= @EndDate
                AND -- Student started before the end date range  
                LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                AND (
                      @ProgId IS NULL
                      OR t8.ProgId IN ( SELECT  Val
                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                    )
                AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start  
       -- Date Determined or ExpGradDate or LDA falls before 08/31/2010  
                AND (
                      t1.DateDetermined < @StartDate
                      OR ExpGradDate < @StartDate
                      OR LDA < @StartDate
                    ) )   
      -- If Student is enrolled in only one program version and if that program version   
      -- happens to be a continuing ed program exclude the student  
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd );  
        END; 
--SELECT * FROM #StudentsList WHERE SSN='022622355'

/*********Get the list of students that will be shown in the report - Ends Here *****************8*/  
  
-- Get all Full Time, First Time UnderGraduate Students  
-- Use #StudentsList as it pulls UnderGraduate Students  
    IF ( @SchoolType = 'program' )
        BEGIN
            INSERT  INTO #UnderGraduateStudentsWhoReceivedFinancialAid
                    SELECT  SE.StudentId
                           ,  
			--SUM(GrossAmount) AS NetAmount, 
                            NULL AS NetAmount
                           ,FS.IPEDSValue AS FinancialAidType
                           ,FS.TitleIV AS TitleIV
                           ,FS.AdvFundSourceId
                    FROM    dbo.saTransactions SA
                    INNER JOIN saFundSources FS ON SA.FundSourceId = FS.FundSourceId
                    INNER JOIN arStuEnrollments SE ON SA.StuEnrollId = SE.StuEnrollId
                    LEFT JOIN arAttendTypes t10 ON SE.attendtypeid = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON SE.degcertseekingid = t12.DegCertSeekingId
                    WHERE   SE.StudentId IN ( SELECT DISTINCT
                                                        StudentId
                                              FROM      #StudentsList )
                            AND (
                                  SE.StartDate >= @StartDate
                                  AND SE.StartDate <= @AcademicEndDate
                                )
			--AND (SE.StartDate<=@AcademicEndDate)
                            AND t10.IPEDSValue = 61  -- Full Time   
                            AND t12.IPEDSValue = 11 -- First Time 
                            AND FS.IPEDSValue IN ( 66,67,68,69,70,71 )
                            AND FS.AdvFundSourceId NOT IN ( 1,19,23 )
			-- This condition commented based on discussion with CK 
			--Part A doesn't explicitly state award year like Part B-E 
			--AND	
			--(
			--	(SA.AwardStartDate>=@StartDate AND SA.AwardstartDate<=@EndDate)
			--	OR
			--	(SA.AwardEndDate>=@StartDate AND SA.AwardEndDate<=@EndDate)
			--)
                            AND SE.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start   
		  -- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate  
		  ( SELECT  t1.StuEnrollId
            FROM    arStuEnrollments t1
                   ,syStatusCodes t2
            WHERE   t1.StatusCodeId = t2.StatusCodeId
                    AND StartDate <= @EndDate
                    AND -- Student started before the end date range  
                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start  
		   -- Date Determined or ExpGradDate or LDA falls before 08/31/2010  
                    AND (
                          t1.DateDetermined < @StartDate
                          OR ExpGradDate < @StartDate
                          OR LDA < @StartDate
                        ) )   
		  -- If Student is enrolled in only one program version and if that program version   
		  -- happens to be a continuing ed program exclude the student  
                            AND SE.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd );  
        END;
    IF ( @SchoolType <> 'program' )
        BEGIN
            INSERT  INTO #UnderGraduateStudentsWhoReceivedFinancialAid
                    SELECT  SE.StudentId
                           ,  
			--SUM(GrossAmount) AS NetAmount, 
                            NULL AS NetAmount
                           ,FS.IPEDSValue AS FinancialAidType
                           ,FS.TitleIV AS TitleIV
                           ,FS.AdvFundSourceId
                    FROM    dbo.saTransactions SA
                    INNER JOIN saFundSources FS ON SA.FundSourceId = FS.FundSourceId
                    INNER JOIN arStuEnrollments SE ON SA.StuEnrollId = SE.StuEnrollId
                    LEFT JOIN arAttendTypes t10 ON SE.attendtypeid = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON SE.degcertseekingid = t12.DegCertSeekingId
                    WHERE   SE.StudentId IN ( SELECT DISTINCT
                                                        StudentId
                                              FROM      #StudentsList )
                           -- AND ( SE.StartDate <= @AcademicEndDate )
                           --12/05/2012 DE8824 - Academic year options should have a cutoff start date
                            AND (
                                  SE.StartDate > @AcademicStartDate
                                  AND SE.StartDate <= @AcademicEndDate
                                )
                            AND t10.IPEDSValue = 61  -- Full Time   
                            AND t12.IPEDSValue = 11 -- First Time 
                            AND FS.IPEDSValue IN ( 66,67,68,69,70,71 )
                            AND FS.AdvFundSourceId NOT IN ( 1,19,23 )
			--AND	(SA.AwardstartDate<=@EndDate OR SA.AwardEndDate<=@EndDate)
			-- This condition commented based on discussion with CK 
			--Part A doesn't explicitly state award year like Part B-E
			 --AND 
				--			(
				--				(SA.AwardStartDate>=@StartDate AND SA.AwardstartDate<=@EndDate) 
				--				OR 
				--				(SA.AwardEndDate>=@StartDate AND SA.AwardEndDate<=@EndDate)
				--			)
                            AND SE.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start   
		  -- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate  
		  ( SELECT  t1.StuEnrollId
            FROM    arStuEnrollments t1
                   ,syStatusCodes t2
            WHERE   t1.StatusCodeId = t2.StatusCodeId
                    AND StartDate <= @AcademicEndDate
                    AND -- Student started before the end date range  
                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start  
		   -- Date Determined or ExpGradDate or LDA falls before 08/31/2010  
                    AND (
                          t1.DateDetermined < @AcademicEndDate
                          OR ExpGradDate < @AcademicEndDate
                          OR LDA < @AcademicEndDate
                        ) )   
		  -- If Student is enrolled in only one program version and if that program version   
		  -- happens to be a continuing ed program exclude the student  
                            AND SE.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd );  
        END;
 
--SELECT * FROM #UnderGraduateStudentsWhoReceivedFinancialAid WHERE StudentId='17878602-70AF-497D-8DC8-49671BDE0BEC'

--(SELECT COUNT(DISTINCT t2.StudentId) FROM arStuEnrollments t2 INNER JOIN #UnderGraduateStudentsWhoReceivedFinancialAid t3  
--	ON t2.StudentId=t3.StudentId AND t2.StudentId='17878602-70AF-497D-8DC8-49671BDE0BEC'
--	INNER JOIN dbo.saTuitionCategories t4 ON t2.TuitionCategoryId=t4.TuitionCategoryId 
--	WHERE t3.FinancialAidType IN (66,67,68)  
--	--AND t4.IPEDSValue IN (145,146) -- Only In-State and In-District (This is for Group 3 and 4)
--	AND ISNULL(t3.AdvFundSourceId,0) NOT IN (5,6)-- Exclude FWS and DL Plus Loan
--	--AND t2.PrgVerId IN --NEW DD Rev 12/12/11 
--	--(SELECT PrgVerId --NEW DD Rev 12/12/11  
--	-- FROM arPrgVersions --NEW DD Rev 12/12/11 
--	-- WHERE ProgId = @LargestProgramID) --NEW DD Rev 12/12/11 
--	)
  
    SELECT  RowNumber
           ,SSN
           ,StudentNumber
           ,StudentName
           ,'X' AS GROUP1
           ,CASE WHEN Group2Count >= 1 THEN 'X'
                 ELSE ''
            END AS GROUP2
           ,CASE WHEN Group2aCount >= 1 THEN 'X'
                 ELSE ''
            END AS GROUP2a
           ,CASE WHEN Group2bCount >= 1 THEN 'X'
                 ELSE ''
            END AS GROUP2b
           ,CASE WHEN Group3Count >= 1 THEN 'X'
                 ELSE ''
            END AS GROUP3
           ,CASE WHEN Group4Count >= 1 THEN 'X'
                 ELSE ''
            END AS GROUP4
           ,1 AS Group1Count
           ,Group2Count
           ,Group2aCount
           ,Group2bCount
           ,Group3Count
           ,Group4Count
    INTO    #Result
    FROM    (
              SELECT    NEWID() AS RowNumber
                       ,dbo.UDF_FormatSSN(SSN) AS SSN
                       ,StudentNumber
                       ,LastName + ', ' + FirstName + ' ' + ISNULL(MiddleName,'') AS StudentName
                       ,'X' AS GROUP1
                       ,CASE WHEN @SchoolType = 'program' THEN 
	-- Program Reporter
	-- Get Count of  FULL TIME First Time UnderGraduates Student List  
                                  (
                                    SELECT  COUNT(DISTINCT StudentId)
                                    FROM    arStuEnrollments t2
                                    LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                    WHERE   t2.StudentId = #StudentsList.StudentId
                                            AND t10.IPEDSValue = 61
                                            AND -- Full Time   
                                            t12.IPEDSValue = 11 -- First Time   
                                            AND t2.StartDate >= @StartDate
                                            AND t2.StartDate <= @EndDate
	--AND t2.StartDate<=@EndDate
                                  )
                             ELSE
	--Academic
                                  (
                                    SELECT  COUNT(DISTINCT StudentId)
                                    FROM    arStuEnrollments t2
                                    LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                    WHERE   t2.StudentId = #StudentsList.StudentId
                                            AND t10.IPEDSValue = 61
                                            AND -- Full Time   
                                            t12.IPEDSValue = 11 -- First Time  
                                            --12/05/2012 DE8824 - Academic year options should have a cutoff start date
                                            AND (
                                                  t2.StartDate > @AcademicStartDate
                                                  AND t2.StartDate <= @AcademicEndDate
                                                )
                                            --AND t2.StartDate <= @EndDate
                                  )
                        END AS Group2Count
                       ,  
  -- From FULL TIME First Time UnderGraduates Student List, Select Students who received those who received any Federal Work Study, loans to students,   
  -- or grant or scholarship aid from the federal government, state/local government, the institution, or other sources known to the institution.   
    
  --FinancialAidType (66) - Federal grants(grants/educational assistance funds)  
  --FinancialAidType (67) - State/local government grants(grants/scholarships/waivers)  
  --FinancialAidType (68) - Institutional grants  
  --FinancialAidType (69) - Private grants or scholarships  
  --FinancialAidType (70) - Loans to students  
  --FinancialAidType (71) - Other sources  
  -- Values are coming from saFundSources - IPEDSValue Field  
                        (
                          SELECT    COUNT(DISTINCT t2.StudentId)
                          FROM      arStuEnrollments t2
                          INNER JOIN #UnderGraduateStudentsWhoReceivedFinancialAid t3 ON t2.StudentId = t3.StudentId
                                                                                         AND t2.StudentId = #StudentsList.StudentId
                          WHERE     t3.FinancialAidType IN ( 66,67,68,69,70,71 )
                        ) AS Group2aCount
                       ,  
  -- From FULL TIME First Time UnderGraduates Student List, Select Students who received those who received any loans to students or grant or scholarship aid   
  --from the federal government, state/local government, or the institution.  
                        (
                          SELECT    COUNT(DISTINCT t2.StudentId)
                          FROM      arStuEnrollments t2
                          INNER JOIN #UnderGraduateStudentsWhoReceivedFinancialAid t3 ON t2.StudentId = t3.StudentId
                                                                                         AND t2.StudentId = #StudentsList.StudentId
                          WHERE     t3.FinancialAidType IN ( 66,67,68,70 )
                                    AND ISNULL(t3.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- Exclude FWS (5) and DL Plus (6) -- FFEL PLUS (9)(added for DE8816)
                        ) AS Group2bCount
                       ,  
  -- From FULL TIME First Time UnderGraduates Student List, Select Students who received grant or scholarship aid from the federal   
  --government, state/local government, or the institution. Exclude other sources of grants.
  -- For Academic Yr Reporter -Public owned school 
  -- Of those in Group 2, those paying the in-state or in-district tuition rate who received grant or scholarship aid from federal, 
  --state or local governments, or the institution.
  --Exclude other sources of grants.
  -- Institution Type = '0' is Public and '1' is Private
                        CASE WHEN (
                                    LOWER(@SchoolType) <> 'program'
                                    AND @InstitutionType = '1'
                                  )
                             THEN (
                                    SELECT  COUNT(DISTINCT t2.StudentId)
                                    FROM    arStuEnrollments t2
                                    INNER JOIN #UnderGraduateStudentsWhoReceivedFinancialAid t3 ON t2.StudentId = t3.StudentId
                                                                                                   AND t2.StudentId = #StudentsList.StudentId
											--DE8813 - Item 2
                                            -- INNER JOIN dbo.saTuitionCategories t4 ON t2.TuitionCategoryId = t4.TuitionCategoryId
                                    WHERE   t3.FinancialAidType IN ( 66,67,68 )  
	--AND t4.IPEDSValue IN (145,146) -- Only In-State and In-District (This is for Group 3 and 4)
                                            AND ISNULL(t3.AdvFundSourceId,0) NOT IN ( 5,6,9 )-- Exclude FWS and DL Plus Loan -- FFEL PLUS (9)(added for DE8816)
	--AND t2.PrgVerId IN --NEW DD Rev 12/12/11 
	--(SELECT PrgVerId --NEW DD Rev 12/12/11  
	-- FROM arPrgVersions --NEW DD Rev 12/12/11 
	-- WHERE ProgId = @LargestProgramID) --NEW DD Rev 12/12/11 
                                  )
                             ELSE (
                                    SELECT  COUNT(DISTINCT t2.StudentId)
                                    FROM    arStuEnrollments t2
                                    INNER JOIN #UnderGraduateStudentsWhoReceivedFinancialAid t3 ON t2.StudentId = t3.StudentId
                                                                                                   AND t2.StudentId = #StudentsList.StudentId
                                    WHERE   t3.FinancialAidType IN ( 66,67,68 )
                                            AND ISNULL(t3.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- Exclude FWS and DL Plus Loan -- FFEL PLUS (9)(added for DE8816)
                                            AND t2.PrgVerId IN --NEW DD Rev 12/12/11 
	( SELECT    PrgVerId --NEW DD Rev 12/12/11 
      FROM      arPrgVersions --NEW DD Rev 12/12/11 
      WHERE     ProgId = @LargestProgramID ) --NEW DD Rev 12/12/11 
                                  )
                        END AS Group3Count
                       ,  
  -- From FULL TIME First Time UnderGraduates =t4.TStudent List, Of those in Group 2, those who received Title IV federal student aid.   
   -- For Academic School and Public Schools consider "Instate" and "InDistrict"
                        CASE WHEN (
                                    LOWER(@SchoolType) <> 'program'
                                    AND @InstitutionType = '1'
                                  ) THEN --Academic and Private
                                  (
                                    SELECT  COUNT(DISTINCT t2.StudentId)
                                    FROM    arStuEnrollments t2
                                    INNER JOIN #UnderGraduateStudentsWhoReceivedFinancialAid t3 ON t2.StudentId = t3.StudentId
                                                                                                   AND t2.StudentId = #StudentsList.StudentId
                                            --DE8813 - Item 2
                                            --INNER JOIN dbo.saTuitionCategories t4 ON t2.TuitionCategoryId = t4.TuitionCategoryId
                                    WHERE   t3.TitleIV = 1  
	  --AND t4.IPEDSValue IN (145,146) -- Only In-State and In-District (This is for Group 3 and 4)
                                            AND ISNULL(t3.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- Exclude DL Plus Loan -- FFEL PLUS (9)(added for DE8816)
	  --AND t2.PrgVerId IN --NEW DD Rev 12/12/11 
	  -- (SELECT PrgVerId --NEW DD Rev 12/12/11 
	  --  FROM arPrgVersions --NEW DD Rev 12/12/11 
	  --  WHERE ProgId = @LargestProgramID) --NEW DD Rev 12/12/11 
                                  )
                             ELSE (
                                    SELECT  COUNT(DISTINCT t2.StudentId)
                                    FROM    arStuEnrollments t2
                                    INNER JOIN #UnderGraduateStudentsWhoReceivedFinancialAid t3 ON t2.StudentId = t3.StudentId
                                                                                                   AND t2.StudentId = #StudentsList.StudentId
                                    WHERE   t3.TitleIV = 1
                                            AND ISNULL(t3.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- Exclude FWS and DL Plus Loan -- FFEL PLUS (9)(added for DE8816)
                                            AND t2.PrgVerId IN --NEW DD Rev 12/12/11 
	   ( SELECT PrgVerId --NEW DD Rev 12/12/11 
         FROM   arPrgVersions --NEW DD Rev 12/12/11 
         WHERE  ProgId = @LargestProgramID ) --NEW DD Rev 12/12/11 
                                  )
                        END AS Group4Count
              FROM      #StudentsList
            ) dt
    ORDER BY CASE WHEN @OrderBy = 'SSN' THEN dt.SSN
             END
           ,CASE WHEN @OrderBy = 'LastName' THEN dt.StudentName
            END
           ,
            -- Updated 11/27/2012 - sort by student number not working
			--Case When @OrderBy='StudentNumber' Then Convert(int,dt.StudentNumber)
            CASE WHEN @OrderBy = 'Student Number' THEN dt.StudentNumber
            END;
  
    DECLARE @TotalGroup1 INT
       ,@TotalGroup2 INT
       ,@TotalGroup2a INT
       ,@TotalGroup2b INT
       ,@TotalGroup3 INT
       ,@TotalGroup4 INT;  
  
    SET @TotalGroup1 = (
                         SELECT SUM(Group1Count)
                         FROM   #Result
                       );  
    SET @TotalGroup2 = (
                         SELECT SUM(Group2Count)
                         FROM   #Result
                       );  
    SET @TotalGroup2a = (
                          SELECT    SUM(Group2aCount)
                          FROM      #Result
                        );  
    SET @TotalGroup2b = (
                          SELECT    SUM(Group2bCount)
                          FROM      #Result
                        );  
    SET @TotalGroup3 = (
                         SELECT SUM(Group3Count)
                         FROM   #Result
                       );  
    SET @TotalGroup4 = (
                         SELECT SUM(Group4Count)
                         FROM   #Result
                       ); 

--Need to comment the following line
--SELECT * FROM #StudentsList
--SELECT * FROM #UnderGraduateStudentsWhoReceivedFinancialAid 
  
    SELECT  *
           ,@TotalGroup1 AS TotalGroup1
           ,@TotalGroup2 AS TotalGroup2
           ,@TotalGroup2a AS TotalGroup2a
           ,@TotalGroup2b AS TotalGroup2b
           ,@TotalGroup3 AS TotalGroup3
           ,@TotalGroup4 AS TotalGroup4
    FROM    #Result; --WHERE GROUP4='X'
     
    DROP TABLE #UnderGraduateStudentsWhoReceivedFinancialAid;  
    DROP TABLE #StudentsList;  
    DROP TABLE #Result;  
  


GO
