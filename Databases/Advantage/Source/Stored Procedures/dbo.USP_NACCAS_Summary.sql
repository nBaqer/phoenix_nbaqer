SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_NACCAS_Summary]
    @PrgIdList VARCHAR(MAX) = NULL
   ,@CampusId VARCHAR(50)
   ,@ReportYear INT
   ,@ReportType VARCHAR(50)
AS
    BEGIN

           DECLARE @Summary TABLE
            (
                ProgramId UNIQUEIDENTIFIER
               ,ProgramDescrip VARCHAR(50)
               ,ProgramHours DECIMAL(9, 2)
               ,ProgramCredits DECIMAL(18, 2)
               ,IsClockHour BIT
               ,TotalStudentsEnrolledAsOf INT
               ,TotalStudentsStartedTrainingIn INT
               ,TotalStudentsStartedTrainingBetween INT
               ,Item1 INT
               ,Item2 INT
               ,Item3 INT
               ,Item4 INT
               ,Item5 INT
               ,Item6 INT
            );
        DECLARE @EnrollmentCounts TABLE
            (
                PrgID UNIQUEIDENTIFIER
               ,PrgDescrip VARCHAR(50)
               ,ProgramHours DECIMAL(9, 2)
               ,ProgramCredits DECIMAL(18, 2)
               ,SSN VARCHAR(50)
               ,Student VARCHAR(50)
               ,LeadId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,EnrollmentID NVARCHAR(50)
               ,Status VARCHAR(80)
               ,SysStatusId INT
               ,StartDate DATETIME
               ,LDA DATETIME
               ,StudentEnrolledAsOf BIT
               ,StudentStartedTrainingIn BIT
               ,StudentStartedTrainingBetween BIT
               ,RN INT
               ,cnt INT
            );

        DECLARE @CohortGrid TABLE
            (
                PrgID UNIQUEIDENTIFIER
               ,PrgDescrip VARCHAR(50)
               ,ProgramHours DECIMAL(9, 2)
               ,ProgramCredits DECIMAL(18, 2)
               ,SSN VARCHAR(50)
               ,PhoneNumber VARCHAR(50)
               ,Email VARCHAR(100)
               ,Student VARCHAR(50)
               ,LeadId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,EnrollmentID NVARCHAR(50)
               ,Status VARCHAR(80)
               ,SysStatusId INT
               ,DropReasonId UNIQUEIDENTIFIER
               ,StartDate DATETIME
               ,ContractedGradDate DATETIME
               ,ExpectedGradDate DATETIME
               ,LDA DATETIME
               ,LicensureWrittenAllParts BIT
               ,LicensureLastPartWrittenOn DATETIME
               ,LicensurePassedAllParts BIT
               ,AllowsMoneyOwed BIT
               ,AllowsIncompleteReq BIT
               ,Graduated BIT
               ,OwesMoney BIT
               ,MissingRequired BIT
               ,GraduatedProgram VARCHAR(5)
               ,DateGraduated VARCHAR(50)
               ,PlacementStatus VARCHAR(5)
               ,IneligibilityReason VARCHAR(100)
               ,Placed VARCHAR(5)
               ,SatForAllExamParts VARCHAR(5)
               ,PassedAllParts VARCHAR(5)
               ,EmployerName VARCHAR(150)
               ,EmployerAddress VARCHAR(150)
               ,EmployerCity VARCHAR(100)
               ,EmployerState VARCHAR(100)
               ,EmployerZip VARCHAR(25)
               ,EmployerPhone VARCHAR(25)
            );

        INSERT INTO @EnrollmentCounts (
                                      PrgID
                                     ,PrgDescrip
                                     ,ProgramHours
                                     ,ProgramCredits
                                     ,SSN
                                     ,Student
                                     ,LeadId
                                     ,StuEnrollId
                                     ,EnrollmentID
                                     ,Status
                                     ,SysStatusId
                                     ,StartDate
                                     ,LDA
                                     ,StudentEnrolledAsOf
                                     ,StudentStartedTrainingIn
                                     ,StudentStartedTrainingBetween
                                     ,RN
                                     ,cnt
                                      )
        EXEC dbo.USP_NACCAS_EnrollmentCount @PrgIdList = @PrgIdList -- varchar(max)
                                           ,@CampusId = @CampusId      -- varchar(50)
                                           ,@ReportYear = @ReportYear; -- int


        INSERT INTO @CohortGrid (
                                PrgID
                               ,PrgDescrip
                               ,ProgramHours
                               ,ProgramCredits
                               ,SSN
                               ,PhoneNumber
                               ,Email
                               ,Student
                               ,LeadId
                               ,StuEnrollId
                               ,EnrollmentID
                               ,Status
                               ,SysStatusId
                               ,DropReasonId
                               ,StartDate
                               ,ContractedGradDate
                               ,ExpectedGradDate
                               ,LDA
                               ,LicensureWrittenAllParts
                               ,LicensureLastPartWrittenOn
                               ,LicensurePassedAllParts
                               ,AllowsMoneyOwed
                               ,AllowsIncompleteReq
                               ,Graduated
                               ,OwesMoney
                               ,MissingRequired
                               ,GraduatedProgram
                               ,DateGraduated
                               ,PlacementStatus
                               ,IneligibilityReason
                               ,Placed
                               ,SatForAllExamParts
                               ,PassedAllParts
                               ,EmployerName
                               ,EmployerAddress
                               ,EmployerCity
                               ,EmployerState
                               ,EmployerZip
                               ,EmployerPhone
                                )
        EXEC dbo.USP_NACCAS_CohortGrid @PrgIdList = @PrgIdList    -- varchar(max)
                                      ,@CampusId = @CampusId      -- varchar(50)
                                      ,@ReportYear = @ReportYear  -- int
                                      ,@ReportType = @ReportType; -- varchar(50)

									  --SELECT * FROM @CohortGrid

        DECLARE @numOfEnrollments TABLE
            (
                prgId UNIQUEIDENTIFIER
               ,numOfEnrollments INT
            );
        INSERT INTO @numOfEnrollments (
                                      prgId
                                     ,numOfEnrollments
                                      )
                    (SELECT   cg.PrgID
                             ,COUNT(*)
                     FROM     @CohortGrid cg
                     GROUP BY cg.PrgID);


       
                                       
                                            WITH summ AS ( SELECT     *
                                                                ,CASE WHEN (
                                                                           SELECT AC.ACId
                                                                           FROM   dbo.syAcademicCalendars AC
                                                                           WHERE  AC.ACId = prog.ACId
                                                                           ) = 5 THEN 1
                                                                      ELSE 0
                                                                 END AS IsClockHour
                                                                ,ROW_NUMBER() OVER ( PARTITION BY ecCG.PrgID
                                                                                                 ,ecCG.PrgDescrip
                                                                                     ORDER BY ecCG.PrgID
                                                                                   ) AS RN
                                                      FROM       (
                                                                 SELECT ec.PrgID
                                                                       ,ec.PrgDescrip
                                                                       ,ec.ProgramHours
                                                                       ,ec.ProgramCredits
                                                                       ,ec.StudentEnrolledAsOf
                                                                       ,ec.StudentStartedTrainingIn
                                                                       ,ec.StudentStartedTrainingBetween
                                                                       ,NULL AS LicensureWrittenAllParts
                                                                       ,NULL AS LicensureLastPartWrittenOn
                                                                       ,NULL AS LicensurePassedAllParts
                                                                       ,NULL AS Graduated
                                                                       ,NULL AS GraduatedProgram
                                                                       ,NULL AS DateGraduated
                                                                       ,NULL AS PlacementStatus
                                                                       ,NULL AS IneligibilityReason
                                                                       ,NULL AS Placed
                                                                       ,NULL AS SatForAllExamParts
                                                                       ,NULL AS PassedAllParts
                                                                 FROM   @EnrollmentCounts ec
                                                                 UNION ALL
                                                                 SELECT CG.PrgID
                                                                       ,CG.PrgDescrip
                                                                       ,CG.ProgramHours
                                                                       ,CG.ProgramCredits
                                                                       ,NULL AS StudentEnrolledAsOf
                                                                       ,NULL AS StudentStartedTrainingIn
                                                                       ,NULL AS StudentStartedTrainingBetween
                                                                       ,CG.LicensureWrittenAllParts
                                                                       ,CG.LicensureLastPartWrittenOn
                                                                       ,CG.LicensurePassedAllParts
                                                                       ,CG.Graduated
                                                                       ,CG.GraduatedProgram
                                                                       ,CG.DateGraduated
                                                                       ,CG.PlacementStatus
                                                                       ,CG.IneligibilityReason
                                                                       ,CG.Placed
                                                                       ,CG.SatForAllExamParts
                                                                       ,CG.PassedAllParts
                                                                 FROM   @CohortGrid CG
                                                                 ) AS ecCG
                                                      INNER JOIN dbo.arPrograms prog ON ecCG.PrgID = prog.ProgId
													  ), programLength as
													  (SELECT summ.ProgId, summ.ProgramHours AS ProgramLengthHours, summ.ProgramCredits AS ProgramLengthCredits FROM summ WHERE rn = 1)


													   INSERT INTO @Summary (
                             ProgramId
                            ,ProgramDescrip
                            ,ProgramHours
                            ,ProgramCredits
                            ,IsClockHour
                            ,TotalStudentsEnrolledAsOf
                            ,TotalStudentsStartedTrainingIn
                            ,TotalStudentsStartedTrainingBetween
                            ,Item1
                            ,Item2
                            ,Item3
                            ,Item4
                            ,Item5
                            ,Item6
                             )
                    SELECT     summaryResult.ProgId
                              ,summaryResult.PrgDescrip
                              ,summaryResult.ProgramLengthHours
                              ,summaryResult.ProgramLengthCredits
                              ,summaryResult.IsClockHour
                              ,summaryResult.TotalStudentsEnrolledAsOf
                              ,summaryResult.TotalStudentsStartedTrainingIn
                              ,summaryResult.TotalStudentsStartedTrainingBetween
                              ,[@numOfEnrollments].numOfEnrollments
                              ,summaryResult.numOfStudentsGraduated
                              ,summaryResult.numOfStudentsEligible
                              ,summaryResult.numOfStudentsPlaced
                              ,summaryResult.numOfStudentsSatForAllExams
                              ,summaryResult.numOfStudentsPassedAllParts
                    FROM       (
                               SELECT   summed.ProgId
                                       ,summed.PrgDescrip
                                     ,summed.ProgramLengthHours
									 ,summed.ProgramLengthCredits
									 ,summed.IsClockHour
                                       ,SUM(   CASE WHEN summed.StudentEnrolledAsOf = 1 THEN 1
                                                    ELSE 0
                                               END
                                           ) TotalStudentsEnrolledAsOf
                                       ,SUM(   CASE WHEN summed.StudentStartedTrainingIn = 1 THEN 1
                                                    ELSE 0
                                               END
                                           ) TotalStudentsStartedTrainingIn
                                       ,SUM(   CASE WHEN summed.StudentStartedTrainingBetween = 1 THEN 1
                                                    ELSE 0
                                               END
                                           ) TotalStudentsStartedTrainingBetween
                                       ,SUM(   CASE WHEN summed.GraduatedProgram = 'Y' THEN 1
                                                    ELSE 0
                                               END
                                           ) AS numOfStudentsGraduated
                                       ,SUM(   CASE WHEN summed.PlacementStatus = 'E' THEN 1
                                                    ELSE 0
                                               END
                                           ) AS numOfStudentsEligible
                                       ,SUM(   CASE WHEN summed.Placed = 'Y' THEN 1
                                                    ELSE 0
                                               END
                                           ) AS numOfStudentsPlaced
                                       ,SUM(   CASE WHEN summed.SatForAllExamParts = 'Y' THEN 1
                                                    ELSE 0
                                               END
                                           ) AS numOfStudentsSatForAllExams
                                       ,SUM(   CASE WHEN summed.PassedAllParts = 'Y' THEN 1
                                                    ELSE 0
                                               END
                                           ) AS numOfStudentsPassedAllParts
                               FROM     (


										  SELECT programLength.ProgId
                                              ,summ.PrgDescrip
                                              ,programLength.ProgramLengthHours
                                              ,programLength.ProgramLengthCredits
                                              ,summ.IsClockHour
                                              ,summ.StudentEnrolledAsOf
                                              ,summ.StudentStartedTrainingIn
                                              ,summ.StudentStartedTrainingBetween
                                              ,summ.GraduatedProgram
                                              ,summ.PlacementStatus
                                              ,summ.Placed
                                              ,summ.SatForAllExamParts
                                              ,summ.PassedAllParts
											  FROM programLength
											  INNER JOIN summ ON summ.ProgId = programLength.ProgId                                                    
									
                                        ) AS summed
                               GROUP BY summed.ProgId
                                       ,summed.PrgDescrip
									,summed.ProgramLengthHours
									 ,summed.ProgramLengthCredits
									 ,summed.IsClockHour
                               ) AS summaryResult
                    INNER JOIN @numOfEnrollments ON [@numOfEnrollments].prgId = summaryResult.ProgId;

        SELECT *
        FROM   @Summary;
    END;



GO
