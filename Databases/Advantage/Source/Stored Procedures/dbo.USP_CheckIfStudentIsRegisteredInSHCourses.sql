SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_CheckIfStudentIsRegisteredInSHCourses]
    @StuEnrollId UNIQUEIDENTIFIER
AS
    SELECT  COUNT(*) AS rowcounter
    FROM    arResults t1
           ,arClassSections t2
    WHERE   t1.TestId = t2.ClsSectionId
            AND t1.StuEnrollId = @StuEnrollId
            AND t2.ReqId IN ( SELECT DISTINCT
                                        reqid
                              FROM      arBridge_GradeComponentTypes_Courses );



GO
