SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_ApplicantLedger_ByParameters_GetList]
    @CampusId UNIQUEIDENTIFIER
   ,@TransStartDate DATE
   ,@TransEndDate DATE
   ,@Transaction VARCHAR(50)
AS --Set @LeadId='43D6FD75-5453-4F22-9283-BC7A213128FB'  
    IF LOWER(@Transaction) = 'all'
        BEGIN
            SELECT  DISTINCT
			--LT.TransactionId,  
                    LT.LeadId
                   ,(
                      SELECT    L.Firstname + ' ' + L.LastName
                    ) AS LeadName
			--LT.TransCodeId,
			--LT.IsEnrolled,  
			--(select Distinct TransCodeDescrip from saTransCodes where TransCodeId=LT.TransCodeId) as TransactionCode,  
			--LT.TransDescrip as TransDescription,
			--Case when Voided=1 Then 'Payment (Voided)' else   
			--(select Distinct Description from saTransTypes where TransTypeId=LT.TransTypeId) end as TransactionType,  
			--LT.ModUser as Postedby,  
			--LT.TransAmount as TransactionAmount,  
			--Convert(Char(10),LT.CreatedDate,101) as TransDate,  
			--TransReference,  
			--(Select Top 1 CheckNumber from adLeadPayments where TransactionId=LT.TransactionId) as DocumentId,  
			--LT.Voided as Voided, LT.ReversalReason
            FROM    adLeadTransactions LT
            INNER JOIN adLeads L ON LT.LeadId = L.LeadId
            WHERE   L.CampusId = @CampusId
                    AND (
                          CONVERT(CHAR(10),LT.CreatedDate,101) >= @TransStartDate
                          AND CONVERT(CHAR(10),LT.CreatedDate,101) <= @TransEndDate
                        )
            ORDER BY LeadName;
		
        END;

    IF LOWER(@Transaction) = 'voided'
        BEGIN
            SELECT  
			--LT.TransactionId,  
                    LT.LeadId
                   ,(
                      SELECT    L.Firstname + ' ' + L.LastName
                    ) AS LeadName
			--LT.TransCodeId,
			--LT.IsEnrolled,  
			--(select Distinct TransCodeDescrip from saTransCodes where TransCodeId=LT.TransCodeId) as TransactionCode,  
			--LT.TransDescrip as TransDescription,
			--Case when Voided=1 Then 'Payment (Voided)' else   
			--(select Distinct Description from saTransTypes where TransTypeId=LT.TransTypeId) end as TransactionType,  
			--LT.ModUser as Postedby,  
			--LT.TransAmount as TransactionAmount,  
			--Convert(Char(10),LT.CreatedDate,101) as TransDate,  
			--TransReference,  
			--(Select Top 1 CheckNumber from adLeadPayments where TransactionId=LT.TransactionId) as DocumentId,  
			--LT.Voided as Voided, LT.ReversalReason
            FROM    adLeadTransactions LT
            INNER JOIN adLeads L ON LT.LeadId = L.LeadId
            WHERE   L.CampusId = @CampusId
                    AND (
                          CONVERT(CHAR(10),LT.CreatedDate,101) >= @TransStartDate
                          AND CONVERT(CHAR(10),LT.CreatedDate,101) <= @TransEndDate
                        )
                    AND LT.Voided = 1
            ORDER BY LeadName;
		
        END;

    IF LOWER(@Transaction) = 'reversed'
        BEGIN
            SELECT  
			--LT.TransactionId,  
                    LT.LeadId
                   ,(
                      SELECT    L.Firstname + ' ' + L.LastName
                    ) AS LeadName
			--LT.TransCodeId,
			--LT.IsEnrolled,  
			--(select Distinct TransCodeDescrip from saTransCodes where TransCodeId=LT.TransCodeId) as TransactionCode,  
			--LT.TransDescrip as TransDescription,
			--Case when Voided=1 Then 'Payment (Voided)' else   
			--(select Distinct Description from saTransTypes where TransTypeId=LT.TransTypeId) end as TransactionType,  
			--LT.ModUser as Postedby,  
			--LT.TransAmount as TransactionAmount,  
			--Convert(Char(10),LT.CreatedDate,101) as TransDate,  
			--TransReference,  
			--(Select Top 1 CheckNumber from adLeadPayments where TransactionId=LT.TransactionId) as DocumentId,  
			--LT.Voided as Voided, LT.ReversalReason
            FROM    adLeadTransactions LT
            INNER JOIN adLeads L ON LT.LeadId = L.LeadId
            WHERE   L.CampusId = @CampusId
                    AND (
                          CONVERT(CHAR(10),LT.CreatedDate,101) >= @TransStartDate
                          AND CONVERT(CHAR(10),LT.CreatedDate,101) <= @TransEndDate
                        )
                    AND LT.Voided = 0
                    AND (
                          LT.ReversalReason IS NOT NULL
                          OR LT.TransDescrip LIKE '%-Reversed'
                        )
            ORDER BY LeadName;
        END;



GO
