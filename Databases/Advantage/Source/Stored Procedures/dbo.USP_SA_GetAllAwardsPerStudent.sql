SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_SA_GetAllAwardsPerStudent]
    (
     @StuEnrollID UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	04/12/2010
    
	Procedure Name	:	[USP_SA_GetAllAwardsPerStudent]

	Objective		:	Get the awards for the student
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@StuEnrollID	In		Uniqueidentifier	Required
	
	Output			:	returns all the awards for a student
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN	
        SELECT  SA.StudentAwardId
               ,SA.AwardTypeId
               ,(
                  SELECT    FundSourceDescrip
                  FROM      saFundSources
                  WHERE     FundSourceId = SA.AwardTypeId
                ) + '/' + (
                            SELECT  AcademicYearDescrip
                            FROM    saAcademicYears
                            WHERE   AcademicYearId = SA.AcademicYearId
                          ) + '/$' + CAST(SA.GrossAmount AS VARCHAR(10)) AS AwardDescrip
        FROM    faStudentAwards SA
        WHERE   SA.StuEnrollId = @StuEnrollID
        ORDER BY AwardDescrip; 

    END; 



GO
