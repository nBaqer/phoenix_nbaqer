SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_mentorrequirementsbyeffectivedates_getlist]
    @ReqId UNIQUEIDENTIFIER
   ,@EffectiveDate DATETIME
AS
    SELECT  *
    FROM    arMentor_GradeComponentTypes_Courses
    WHERE   ReqId = @ReqId
            AND EffectiveDate = @EffectiveDate
    ORDER BY OperatorSequence;



GO
