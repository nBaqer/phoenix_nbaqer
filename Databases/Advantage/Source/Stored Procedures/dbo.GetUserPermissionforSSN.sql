SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--Exec GetUserPermissionforSSN '8E254155-A9E7-41FA-9B7B-75C5E7E0BBFB','436BF45E-5420-4C1F-8687-CD32A60D64BE'
CREATE PROCEDURE [dbo].[GetUserPermissionforSSN]
    @Userid VARCHAR(50)
   ,@CampusId VARCHAR(50)
AS
    BEGIN  
  
        SELECT  COUNT(*) AS CountRecords
        FROM    syUsers U
               ,syUsersRolesCampGrps URC
               ,syRoles R
        WHERE   (
                  U.Userid = @Userid
                  AND U.IsAdvantageSuperUser = 1
                )
                OR (
                     U.UserId = URC.UserId
                     AND URC.RoleId = R.RoleId
                 -- Director of Admissions, Director of Finanical Aid, Director of Academics
                     AND (
                           R.SysRoleId = 8
                           OR R.SysRoleId = 9
                           OR R.SysRoleId = 13
                         )
                     AND U.Userid = @Userid
                     AND URC.CampgrpId IN ( SELECT  CampgrpId
                                            FROM    syCmpGrpCmps
                                            WHERE   CampusId = @CampusId )
                   );                  
    END;




GO
