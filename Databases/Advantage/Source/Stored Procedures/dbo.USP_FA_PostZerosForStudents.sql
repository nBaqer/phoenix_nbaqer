SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		FAME Inc.
-- Create date: 5/23/2019
-- Description:	For arStudentClockAttendanceSchools posting zeros. Given enroll id list and record date, posts zeros for students on this date. Will post zero's for all students in @StuEnrollIdList
--				wihout checking students in school status. Filtering of students should be down prior to the execution of this SP.	   				
-- =============================================
CREATE PROCEDURE [dbo].[USP_FA_PostZerosForStudents]
    @StuEnrollIdList VARCHAR(MAX) = NULL
   ,@Date DATETIME
   ,@ModUser VARCHAR(50)
AS
    BEGIN
        DECLARE @ModDate DATETIME = GETDATE();

        --update placeholder records if they exist for given student enrollments on given day
        UPDATE dbo.arStudentClockAttendance
        SET    ActualHours = 0
              ,SchedHours = a.CalculatedScheduledHours
              ,ModDate = @ModDate
              ,ModUser = @ModUser
              ,isTardy = 0
              ,PostByException = 'no'
              ,comments = NULL
              ,TardyProcessed = 0
              ,Converted = 0
        FROM   dbo.arStudentClockAttendance
        JOIN   dbo.StudentScheduledHours(@StuEnrollIdList, @Date) a ON a.StuEnrollId = arStudentClockAttendance.StuEnrollId
        WHERE  CAST(RecordDate AS DATE) = CAST(@Date AS DATE)
               AND ActualHours IN ( 99.0, 999.0, 9999.0 )
               AND arStudentClockAttendance.StuEnrollId IN (
                                                           SELECT Val
                                                           FROM   MultipleValuesForReportParameters(@StuEnrollIdList, ',', 1)
                                                           );

        --create records if they do not exist for given student enrollments on given day 
        INSERT INTO dbo.arStudentClockAttendance (
                                                 StuEnrollId
                                                ,ScheduleId
                                                ,RecordDate
                                                ,SchedHours
                                                ,ActualHours
                                                ,ModDate
                                                ,ModUser
                                                ,isTardy
                                                ,PostByException
                                                ,comments
                                                ,TardyProcessed
                                                ,Converted
                                                 )
                    SELECT StuEnrollList.StuEnrollId  -- StuEnrollId - uniqueidentifier
                          ,a.ScheduleId               -- ScheduleId - uniqueidentifier
                          ,CAST(@Date AS DATE)        -- RecordDate - smalldatetime
                          ,a.CalculatedScheduledHours -- SchedHours - decimal(18, 2)
                          ,0                          -- ActualHours - decimal(18, 2)
                          ,@ModDate                   -- ModDate - smalldatetime
                          ,@ModUser                   -- ModUser - varchar(50)
                          ,0                          -- isTardy - bit
                          ,'no'                       -- PostByException - varchar(10)
                          ,NULL                       -- comments - varchar(240)
                          ,0                          -- TardyProcessed - bit
                          ,0                          -- Converted - bit
                    FROM   (
                           SELECT CAST(Val AS UNIQUEIDENTIFIER) AS StuEnrollId
                           FROM   MultipleValuesForReportParameters(@StuEnrollIdList, ',', 1)
                           ) StuEnrollList
                    JOIN   dbo.StudentScheduledHours(@StuEnrollIdList, @Date) a ON a.StuEnrollId = StuEnrollList.StuEnrollId
                    WHERE  NOT EXISTS (
                                      SELECT *
                                      FROM   dbo.arStudentClockAttendance
                                      WHERE  StuEnrollId = StuEnrollList.StuEnrollId
                                             AND CAST(RecordDate AS DATE) = CAST(@Date AS DATE)
                                      );
    END;

GO
