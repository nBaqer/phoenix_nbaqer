SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_GetAllActiveShiftsByCampus]
    (
     @CampusId UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Bruce Shanblatt
    
    Create date		:	02/20/2013
    
	Procedure Name	:	[USP_GetAllActiveShiftsByCampus]

	Objective		:	Get the active shifts for a given campus
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========						
						@CampusId       In      uniqueidentifier    required
	
	Output			:	Returns the active shiftid and description for a given campus	
						
*/-----------------------------------------------------------------------------------------------------


    SELECT  ARS.ShiftId
           ,ARS.ShiftDescrip
    FROM    dbo.arShifts ARS
    INNER JOIN dbo.syStatuses SS ON ARS.StatusId = SS.StatusId
    WHERE   SS.Status = 'Active'
            AND ARS.CampGrpId IN ( SELECT   CampGrpId
                                   FROM     dbo.syCmpGrpCmps
                                   WHERE    CampusId = @CampusId );




GO
