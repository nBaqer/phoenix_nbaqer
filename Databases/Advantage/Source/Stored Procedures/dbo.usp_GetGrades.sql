SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetGrades]
    (
     @grdSystemId UNIQUEIDENTIFIER
 

    )
AS
    SET NOCOUNT ON;
    SELECT DISTINCT
            (
              SELECT TOP 1
                        GrdSysDetailId
              FROM      arGradeSystemDetails
              WHERE     GrdSystemId = S.GrdSystemId
                        AND IsPass = 1
                        AND Grade LIKE 'P%'
            ) AS PassingGrade
           ,(
              SELECT TOP 1
                        GrdSysDetailId
              FROM      arGradeSystemDetails
              WHERE     GrdSystemId = S.GrdSystemId
                        AND IsPass = 0
                        AND Grade LIKE 'F%'
            ) AS FailingGrade
           ,(
              SELECT TOP 1
                        GrdSysDetailId
              FROM      arGradeSystemDetails
              WHERE     GrdSystemId = S.GrdSystemId
                        AND IsPass = 1
                        AND Grade LIKE 'S%'
            ) AS SatisfactoryGrade
           ,(
              SELECT TOP 1
                        GrdSysDetailId
              FROM      arGradeSystemDetails
              WHERE     GrdSystemId = S.GrdSystemId
                        AND IsPass = 0
                        AND Grade LIKE 'U%'
            ) AS UnSatisfactoryGrade
    FROM    arGradeSystemDetails S
    WHERE   S.GrdSystemId = @grdSystemId;



GO
