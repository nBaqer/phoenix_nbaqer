SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_HasStudentMetGraduationHoursRequirement]
    (
        @StuEnrollId UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	06/07/2010
    
	Procedure Name	:	[USP_HasStudentMetGraduationHoursRequirement]

	Objective		:	Find if the student has reached the graduation hour requirements
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@StuEnrollId	In		UniqueIdentifier	Required
						
	
	Output			: returns a row
						
*/
    -----------------------------------------------------------------------------------------------------

    BEGIN

        DECLARE @ScheduledHrs DECIMAL(18, 2);
        DECLARE @ActualHrs DECIMAL(18, 2);


        DECLARE @Programhrs DECIMAL(18, 2);

        SET @ScheduledHrs = (
                            SELECT (
                                   SELECT SUM(SchedHours)
                                   FROM   arStudentClockAttendance SCA
                                   WHERE  SCA.StuEnrollId = @StuEnrollId
                                          AND SchedHours <> 9999.0
                                          AND SchedHours <> 999.0
                                   ) + (
                                       SELECT ISNULL(TransferHours, 0)
                                       FROM   arStuEnrollments
                                       WHERE  StuEnrollId = @StuEnrollId
                                       )
                            );

        SET @ActualHrs = (
                         SELECT (
                                SELECT SUM(ActualHours)
                                FROM   arStudentClockAttendance SCA
                                WHERE  SCA.StuEnrollId = @StuEnrollId
                                       AND ActualHours <> 9999.0
                                       AND ActualHours <> 999.0
                                ) + (
                                    SELECT ISNULL(TransferHours, 0)
                                    FROM   arStuEnrollments
                                    WHERE  StuEnrollId = @StuEnrollId
                                    )
                         );

        SET @Programhrs = (
                          SELECT PV.Hours
                          FROM   arPrgVersions PV
                                ,arStuEnrollments SE
                          WHERE  SE.StuEnrollId = @StuEnrollId
                                 AND SE.PrgVerId = PV.PrgVerId
                          );


        IF ( @ActualHrs >= @Programhrs )
            BEGIN
                SELECT 'Satisfied';
            END;

    END;





GO
