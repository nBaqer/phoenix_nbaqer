SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_AR_TimeClockSpecialCode_Update]
    (
     @TCSId UNIQUEIDENTIFIER
    ,@TCSpecialCode VARCHAR(10)
    ,@StatusId UNIQUEIDENTIFIER
    ,@CampusId UNIQUEIDENTIFIER
    ,@TCSPunchType SMALLINT
    ,@InstructionTypeId UNIQUEIDENTIFIER
    ,@UserName VARCHAR(50)
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Janet Robinson
    
    Create date		:	09/22/2011
    
	Procedure Name	:	[USP_AR_TimeClockSpecialCode_Update]

	Objective		:	Updates table arTimeClockSpecialCode
	
	Parameters		:	Name						Type	Data Type				Required? 	
						=====						====	=========				=========	
						@TCSId						In		UniqueIDENTIFIER		Required
						@TCSpecialCode				In		varchar					Required		
						@StatusId					In		UniqueIDENTIFIER		Required
						@CampusId					In		UniqueIDENTIFIER		Required
						@TCSPunchType				In		smallint				Required
						@InstructionTypeId			In		UniqueIDENTIFIER		Required
						@UserName					In		varchar					Required
	Output			:			
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN

        UPDATE  arTimeClockSpecialCode
        SET     TCSpecialCode = @TCSpecialCode
               ,StatusId = @StatusId
               ,CampusId = @CampusId
               ,TCSPunchType = @TCSPunchType
               ,InstructionTypeId = @InstructionTypeId
               ,ModUser = @UserName
               ,ModDate = GETDATE()
        WHERE   TCSId = @TCSId;
		 
    END;



GO
