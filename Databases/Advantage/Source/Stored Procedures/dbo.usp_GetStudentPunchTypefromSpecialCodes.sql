SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[usp_GetStudentPunchTypefromSpecialCodes]
    (
     @SpecialCode AS VARCHAR(5)
    ,@CampusID AS VARCHAR(50)
    )
AS
    BEGIN 
  
        SELECT  TCSPunchType
        FROM    dbo.arTimeClockSpecialCode
        WHERE   (TCSpecialCode = @SpecialCode OR (TCSpecialCode = CASE WHEN ISNUMERIC(@SpecialCode) = 1 THEN CAST(@SpecialCode AS INT) ELSE 0 END))
                AND CampusId = @CampusID;
    END;






GO
