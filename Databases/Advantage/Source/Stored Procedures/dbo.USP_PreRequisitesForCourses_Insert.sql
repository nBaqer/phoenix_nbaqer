SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_PreRequisitesForCourses_Insert]
    @OldEffectiveDate DATETIME
   ,@EffectiveDate DATETIME
   ,@ReqId UNIQUEIDENTIFIER
   ,@MinperCategory INT
   ,@MaxperCategory INT
   ,@MinperCombination INT
   ,@MaxperCombination INT
   ,@PreReqId UNIQUEIDENTIFIER
   ,@mentorproctored BIT
   ,@moduser VARCHAR(50)
   ,@moddate DATETIME
AS
    DECLARE @hDoc INT;
        
        -- Delete all records from bridge table based on GrdComponentTypeId
    IF YEAR(@OldEffectiveDate) <> 1900
        BEGIN
            DELETE  FROM arPrerequisites_GradeComponentTypes_Courses
            WHERE   EffectiveDate = @OldEffectiveDate
                    AND ReqId = @ReqId;
        END;
		
    INSERT  INTO arPrerequisites_GradeComponentTypes_Courses
    VALUES  ( NEWID(),@ReqId,@EffectiveDate,@MinperCategory,@MaxperCategory,@MinperCombination,@MaxperCombination,@PreReqId,@mentorproctored,@moduser,@moddate );



GO
