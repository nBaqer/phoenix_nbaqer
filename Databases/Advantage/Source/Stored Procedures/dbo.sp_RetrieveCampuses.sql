SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author: Dennis D
-- Create date: 4/6/10
-- Description: This sp will return all active campuses. 
-- We will require a bit param to allow for the description of All campuses. 
-- This is needed for the Manage Configuration Settings screen.
-- =============================================
CREATE PROCEDURE [dbo].[sp_RetrieveCampuses] 
    -- Add the parameters for the stored procedure here
    @IncludeAll AS BIT
AS
    BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
        SET NOCOUNT ON;

        CREATE TABLE #TEMPSETTINGS
            (
             CampusId UNIQUEIDENTIFIER
            ,CampusDescrip VARCHAR(100)
            );

    -- Insert statements for procedure here
        IF @IncludeAll = 1
            BEGIN
                INSERT  INTO #TEMPSETTINGS
                        SELECT  CampusId
                               ,CampDescrip
                        FROM    syCampuses
                        WHERE   StatusId = (
                                             SELECT StatusId
                                             FROM   syStatuses
                                             WHERE  Status = 'Active'
                                           );
            --Add the All Campus
                INSERT  INTO #TEMPSETTINGS
                        SELECT  NULL
                               ,'ALL';
            END;
        ELSE
            BEGIN
                INSERT  INTO #TEMPSETTINGS
                        SELECT  CampusId
                               ,CampDescrip
                        FROM    syCampuses
                        WHERE   StatusId = (
                                             SELECT StatusId
                                             FROM   syStatuses
                                             WHERE  Status = 'Active'
                                           );
            END;
    END;

    SELECT  *
    FROM    #TEMPSETTINGS
    ORDER BY CampusDescrip;

    DROP TABLE #TEMPSETTINGS;



GO
