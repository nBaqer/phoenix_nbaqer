SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--=================================================================================================
-- USP_AT_Step06_InsertAttendance_Class_ClockHour
--=================================================================================================
CREATE PROCEDURE [dbo].[USP_AT_Step06_InsertAttendance_Class_ClockHour]
    (
        @StuEnrollIdList AS VARCHAR(MAX) = NULL
    )
AS --
    -- Step 6  --  InsertAttendance_Class_ClockHour  -- UnitTypeDescrip =  'Clock Hours' 
    --         --  TrackSapAttendance = 'byclass'
    BEGIN -- Step 6  --  InsertAttendance_Class_ClockHour
        DECLARE @StuEnrollId UNIQUEIDENTIFIER
               ,@MeetDate DATETIME
               ,@WeekDay VARCHAR(15)
               ,@StartDate DATETIME
               ,@EndDate DATETIME
               ,@PeriodDescrip VARCHAR(50)
               ,@Actual DECIMAL(18, 2)
               ,@Excused DECIMAL(18, 2)
               ,@ClsSectionId UNIQUEIDENTIFIER
               ,@Absent DECIMAL(18, 2)
               ,@ScheduledMinutes DECIMAL(18, 2)
               ,@TardyMinutes DECIMAL(18, 2)
               ,@tardy DECIMAL(18, 2)
               ,@tracktardies INT
               ,@tardiesMakingAbsence INT
               ,@PrgVerId UNIQUEIDENTIFIER
               ,@rownumber INT
               ,@ActualRunningPresentHours DECIMAL(18, 2)
               ,@ActualRunningAbsentHours DECIMAL(18, 2)
               ,@ActualRunningTardyHours DECIMAL(18, 2)
               ,@ActualRunningMakeupHours DECIMAL(18, 2)
               ,@PrevStuEnrollId UNIQUEIDENTIFIER
               ,@intTardyBreakPoint INT
               ,@AdjustedRunningPresentHours DECIMAL(18, 2)
               ,@AdjustedRunningAbsentHours DECIMAL(18, 2)
               ,@ActualRunningScheduledDays DECIMAL(18, 2)
               ,@ActualRunningExcusedDays INT
               ,@AdjustedPresentDaysComputed DECIMAL(18, 2)
               ,@PeriodDescrip1 VARCHAR(500)
               ,@MakeupHours DECIMAL(18, 2);
        DECLARE @MyEnrollments TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
            );


        DECLARE @attendanceSummary TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ClsSectionId UNIQUEIDENTIFIER
               ,PeriodDescrip1 VARCHAR(50)
               ,MeetDate DATETIME
               ,WeekDay NVARCHAR(25)
               ,StartDate DATETIME
               ,EndDate DATETIME
               ,PeriodDescrip VARCHAR(50)
               ,Actual DECIMAL(18, 2)
               ,Excused BIT
               ,Absent DECIMAL(18, 2)
               ,ScheduledMinutes DECIMAL(18, 2)
               ,TardyMinutes DECIMAL(18, 2)
               ,Tardy BIT
               ,TrackTardies BIT
               ,TardiesMakingAbsence INT
               ,PrgVerId UNIQUEIDENTIFIER
               ,RowNumber INT
            );

        INSERT INTO @MyEnrollments
                    SELECT enrollments.StuEnrollId
                    FROM   dbo.arStuEnrollments enrollments
                    WHERE  (
                           @StuEnrollIdList IS NULL
                           OR ( enrollments.StuEnrollId IN (
                                                           SELECT Val
                                                           FROM   MultipleValuesForReportParameters(@StuEnrollIdList, ',', 1)
                                                           )
                              )
                           );




        INSERT INTO @attendanceSummary (
                                       StuEnrollId
                                      ,ClsSectionId
                                      ,PeriodDescrip1
                                      ,MeetDate
                                      ,WeekDay
                                      ,StartDate
                                      ,EndDate
                                      ,PeriodDescrip
                                      ,Actual
                                      ,Excused
                                      ,Absent
                                      ,ScheduledMinutes
                                      ,TardyMinutes
                                      ,Tardy
                                      ,TrackTardies
                                      ,TardiesMakingAbsence
                                      ,PrgVerId
                                      ,RowNumber
                                       )
                    SELECT   *
                            ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                    FROM     (
                             SELECT     DISTINCT t1.StuEnrollId
                                                ,t1.ClsSectionId
                                                ,(
                                                 SELECT Descrip
                                                 FROM   arReqs A1
                                                       ,dbo.arClassSections A2
                                                 WHERE  A1.ReqId = A2.ReqId
                                                        AND A2.ClsSectionId = t1.ClsSectionId
                                                 ) + '  ' + t5.PeriodDescrip AS PeriodDescrip1
                                                ,t1.MeetDate
                                                ,DATENAME(dw, t1.MeetDate) AS WeekDay
                                                ,t4.StartDate
                                                ,t4.EndDate
                                                ,t5.PeriodDescrip
                                                ,CASE WHEN (
                                                           t1.Actual >= 0
                                                           AND t1.Actual <> 9999
                                                           ) THEN t1.Actual / CAST(60 AS FLOAT)
                                                      ELSE t1.Actual
                                                 END AS Actual
                                                ,t1.Excused
                                                ,CASE WHEN (
                                                           t1.Actual >= 0
                                                           AND t1.Actual <> 9999
                                                           AND ( t1.Actual / CAST(60 AS FLOAT)) < t1.Scheduled / CAST(60 AS FLOAT)
                                                           ) THEN (( t1.Scheduled / CAST(60 AS FLOAT)) - ( t1.Actual / CAST(60 AS FLOAT)))
                                                      ELSE 0
                                                 END AS Absent
                                                ,CASE WHEN t1.Scheduled > 0 THEN ( t1.Scheduled / CAST(60 AS FLOAT))
                                                      ELSE 0
                                                 END AS ScheduledMinutes
                                                ,NULL AS TardyMinutes
                                                ,CASE WHEN t1.Tardy = 1 THEN 1
                                                      ELSE 0
                                                 END AS Tardy
                                                ,t3.TrackTardies
                                                ,t3.TardiesMakingAbsence
                                                ,t3.PrgVerId
                             FROM       atClsSectAttendance t1
                             INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                             INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                             INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                             INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                AND (
                                                                    CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                                    AND DATEDIFF(DAY, t1.MeetDate, t4.EndDate) >= 0
                                                                    )
                                                                AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8522 line added
                             INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                             INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                             INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                             INNER JOIN syPeriodsWorkDays PWD ON t5.PeriodId = PWD.PeriodId
                             INNER JOIN plWorkDays WD ON WD.WorkDaysId = PWD.WorkDayId
                             --inner join Inserted t20 on t20.ClsSectAttId = t1.ClsSectAttId --Inserted Table
                             INNER JOIN dbo.arClassSections t8 ON t8.ClsSectionId = t1.ClsSectionId
                             INNER JOIN dbo.arReqs t9 ON t9.ReqId = t8.ReqId
                             INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t9.UnitTypeId
                             INNER JOIN @MyEnrollments ON [@MyEnrollments].StuEnrollId = t1.StuEnrollId
                             WHERE --t2.StuEnrollId='525B5DB6-6EF8-47AE-A5C3-8DAFC71FB471' and
                                        (
                                        --t9.UnitTypeId IN ( 'B937C92E-FD7A-455E-A731-527A9918C734' )  -- AAUT1.UnitTypeDescrip IN ( 'Clock Hours' )
                                        --OR t3.UnitTypeId IN ( 'B937C92E-FD7A-455E-A731-527A9918C734' )  -- AAUT2.UnitTypeDescrip IN ( 'Clock Hours' )
                                        AAUT1.UnitTypeDescrip IN ( 'Clock Hours' )
                                        OR AAUT2.UnitTypeDescrip IN ( 'Clock Hours' )
                                        ) -- Clock Hours
                                        AND t1.Actual <> 9999.00
                                        AND SUBSTRING(DATENAME(dw, t1.MeetDate), 1, 3) = SUBSTRING(WD.WorkDaysDescrip, 1, 3)

                             -- Some times Makeup days don't fall inside the schedule
                             -- ex: there may be a schedule for T-Fri and school may mark attendance for saturday
                             -- the following query will bring whatever was left out in above query

                             ) dt
                    ORDER BY StuEnrollId
                            ,MeetDate;


        DELETE FROM dbo.syStudentAttendanceSummary
        WHERE StuEnrollId IN (
                             SELECT DISTINCT StuEnrollId
                             FROM   @attendanceSummary
                             );

        DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD FOR
            SELECT   *
            FROM     @attendanceSummary
            ORDER BY StuEnrollId
                    ,MeetDate;
        OPEN GetAttendance_Cursor;
        FETCH NEXT FROM GetAttendance_Cursor
        INTO @StuEnrollId
            ,@ClsSectionId
            ,@PeriodDescrip1
            ,@MeetDate
            ,@WeekDay
            ,@StartDate
            ,@EndDate
            ,@PeriodDescrip
            ,@Actual
            ,@Excused
            ,@Absent
            ,@ScheduledMinutes
            ,@TardyMinutes
            ,@tardy
            ,@tracktardies
            ,@tardiesMakingAbsence
            ,@PrgVerId
            ,@rownumber;
        SET @ActualRunningPresentHours = 0;
        SET @ActualRunningPresentHours = 0;
        SET @ActualRunningAbsentHours = 0;
        SET @ActualRunningTardyHours = 0;
        SET @ActualRunningMakeupHours = 0;
        SET @intTardyBreakPoint = 0;
        SET @AdjustedRunningPresentHours = 0;
        SET @AdjustedRunningAbsentHours = 0;
        SET @ActualRunningScheduledDays = 0;
        SET @MakeupHours = 0;
        SET @ActualRunningExcusedDays = 0;
        WHILE @@FETCH_STATUS = 0
            BEGIN

                IF @PrevStuEnrollId <> @StuEnrollId
                    BEGIN
                        SET @ActualRunningPresentHours = 0;
                        SET @ActualRunningAbsentHours = 0;
                        SET @intTardyBreakPoint = 0;
                        SET @ActualRunningTardyHours = 0;
                        SET @AdjustedRunningPresentHours = 0;
                        SET @AdjustedRunningAbsentHours = 0;
                        SET @ActualRunningScheduledDays = 0;
                        SET @MakeupHours = 0;
                        SET @ActualRunningExcusedDays = 0;
                    END;

                -- Calculate : Actual Running Scheduled Days
                SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;

                -- Calculate: Actual and Adjusted Running Present Hours
                IF @Actual <> 9999
                    BEGIN
                        IF (
                           @Actual > 0
                           AND @Actual > @ScheduledMinutes
                           ) -- Makeup Hours
                            BEGIN
                                SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual - ( @Actual - @ScheduledMinutes ); -- Subtract Makeup Hours from Actual
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual - ( @Actual - @ScheduledMinutes ); -- Subtract Makeup Hours from Actual
                            END;
                        ELSE
                            BEGIN
                                SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual;
                            END;
                    END;

                -- Calculate: Adjusted Running Absent Hours
                SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
                SET @TardyMinutes = ( @ScheduledMinutes - @Actual );
                IF ( @Excused = 1 )
                    BEGIN
                        SET @ActualRunningExcusedDays = @ActualRunningExcusedDays + 1;
                    END;

                IF (
                   @Actual > 0
                   AND @Actual < @ScheduledMinutes
                   AND @tardy = 1
                   )
                    BEGIN
                        SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                    END;

                IF (
                   @Actual > 0
                   AND @Actual > @ScheduledMinutes
                   )
                    BEGIN
                        SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                    END;
                IF (
                   @tracktardies = 1
                   AND @TardyMinutes > 0
                   AND @tardy = 1
                   )
                    BEGIN
                        SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                    END;
                --IF (@tracktardies = 1 AND @intTardyBreakPoint = @tardiesMakingAbsence) 
                --    BEGIN
                --  SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual
                --        SET @AdjustedRunningAbsentHours = (@AdjustedRunningAbsentHours - @Absent) + @ScheduledMinutes --@TardyMinutes
                --        SET @ActualRunningTardyHours = @ActualRunningTardyHours - (@ScheduledMinutes - @Actual)
                --        SET @intTardyBreakPoint = 0
                --    END


                -- if (@tracktardies=1)
                --  begin
                --	set @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours --+IsNULL(@ActualRunningTardyHours,0)
                --  end
                -- else
                BEGIN
                    SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                END;



                INSERT INTO syStudentAttendanceSummary (
                                                       StuEnrollId
                                                      ,ClsSectionId
                                                      ,StudentAttendedDate
                                                      ,ScheduledDays
                                                      ,ActualDays
                                                      ,ActualRunningScheduledDays
                                                      ,ActualRunningPresentDays
                                                      ,ActualRunningAbsentDays
                                                      ,ActualRunningMakeupDays
                                                      ,ActualRunningTardyDays
                                                      ,AdjustedPresentDays
                                                      ,AdjustedAbsentDays
                                                      ,AttendanceTrackType
                                                      ,ModUser
                                                      ,ModDate
                                                      ,IsExcused
                                                      ,ActualRunningExcusedDays
                                                      ,IsTardy
                                                       )
                VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, @ScheduledMinutes, @Actual, @ActualRunningScheduledDays, @ActualRunningPresentHours
                        ,@ActualRunningAbsentHours, ISNULL(@MakeupHours, 0), @ActualRunningTardyHours, @AdjustedPresentDaysComputed
                        ,@AdjustedRunningAbsentHours, 'Post Attendance by Class Clock', 'sa', GETDATE(), @Excused, @ActualRunningExcusedDays, @tardy );

                UPDATE syStudentAttendanceSummary
                SET    tardiesmakingabsence = @tardiesMakingAbsence
                WHERE  StuEnrollId = @StuEnrollId;
                SET @PrevStuEnrollId = @StuEnrollId;

                FETCH NEXT FROM GetAttendance_Cursor
                INTO @StuEnrollId
                    ,@ClsSectionId
                    ,@PeriodDescrip1
                    ,@MeetDate
                    ,@WeekDay
                    ,@StartDate
                    ,@EndDate
                    ,@PeriodDescrip
                    ,@Actual
                    ,@Excused
                    ,@Absent
                    ,@ScheduledMinutes
                    ,@TardyMinutes
                    ,@tardy
                    ,@tracktardies
                    ,@tardiesMakingAbsence
                    ,@PrgVerId
                    ,@rownumber;
            END;
        CLOSE GetAttendance_Cursor;
        DEALLOCATE GetAttendance_Cursor;

        DECLARE @MyTardyTable TABLE
            (
                ClsSectionId UNIQUEIDENTIFIER
               ,TardiesMakingAbsence INT
               ,AbsentHours DECIMAL(18, 2)
            );

        INSERT INTO @MyTardyTable
                    SELECT     ClsSectionId
                              ,PV.TardiesMakingAbsence
                              ,CASE WHEN ( COUNT(*) >= PV.TardiesMakingAbsence ) THEN (( COUNT(*) / PV.TardiesMakingAbsence ) * SAS.ScheduledDays )
                                    ELSE 0
                               END AS AbsentHours
                    --Count(*) as NumberofTimesTardy 
                    FROM       syStudentAttendanceSummary SAS
                    INNER JOIN arStuEnrollments SE ON SAS.StuEnrollId = SE.StuEnrollId
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    WHERE      SE.StuEnrollId = @StuEnrollId
                               AND SAS.IsTardy = 1
                               AND PV.TrackTardies = 1
                    GROUP BY   ClsSectionId
                              ,PV.TardiesMakingAbsence
                              ,SAS.ScheduledDays;

        --Drop table @MyTardyTable
        DECLARE @TotalTardyAbsentDays DECIMAL(18, 2);
        SET @TotalTardyAbsentDays = (
                                    SELECT ISNULL(SUM(AbsentHours), 0)
                                    FROM   @MyTardyTable
                                    );

        --Print @TotalTardyAbsentDays

        UPDATE syStudentAttendanceSummary
        SET    AdjustedPresentDays = AdjustedPresentDays - @TotalTardyAbsentDays
              ,AdjustedAbsentDays = AdjustedAbsentDays + @TotalTardyAbsentDays
        WHERE  StuEnrollId = @StuEnrollId
               AND StudentAttendedDate = (
                                         SELECT   TOP 1 StudentAttendedDate
                                         FROM     syStudentAttendanceSummary
                                         WHERE    StuEnrollId = @StuEnrollId
                                         ORDER BY StudentAttendedDate DESC
                                         );

    END;
--=================================================================================================
-- END  --  USP_AT_Step06_InsertAttendance_Class_ClockHour
--=================================================================================================
GO
