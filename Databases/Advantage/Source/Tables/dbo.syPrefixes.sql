CREATE TABLE [dbo].[syPrefixes]
(
[PrefixId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syPrefixes_PrefixId] DEFAULT (newid()),
[PrefixCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[PrefixDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syPrefixes] ADD CONSTRAINT [PK_syPrefixes_PrefixId] PRIMARY KEY CLUSTERED  ([PrefixId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syPrefixes] ADD CONSTRAINT [FK_syPrefixes_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[syPrefixes] ADD CONSTRAINT [FK_syPrefixes_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
