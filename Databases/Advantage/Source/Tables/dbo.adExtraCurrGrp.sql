CREATE TABLE [dbo].[adExtraCurrGrp]
(
[ExtraCurrGrpId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_adExtraCurrGrp_ExtraCurrGrpId] DEFAULT (newid()),
[ExtraCurrGrpCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExtraCurrGrpDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusId] [uniqueidentifier] NULL,
[CampGrpId] [uniqueidentifier] NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[adExtraCurrGrp_Audit_Delete] ON [dbo].[adExtraCurrGrp]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adExtraCurrGrp','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ExtraCurrGrpId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ExtraCurrGrpId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ExtraCurrGrpId
                               ,'ExtraCurrGrpCode'
                               ,CONVERT(VARCHAR(8000),Old.ExtraCurrGrpCode,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ExtraCurrGrpId
                               ,'ExtraCurrGrpDescrip'
                               ,CONVERT(VARCHAR(8000),Old.ExtraCurrGrpDescrip,121)
                        FROM    Deleted Old; 
            END; 
        END;



    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[adExtraCurrGrp_Audit_Insert] ON [dbo].[adExtraCurrGrp]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adExtraCurrGrp','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ExtraCurrGrpId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ExtraCurrGrpId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ExtraCurrGrpId
                               ,'ExtraCurrGrpCode'
                               ,CONVERT(VARCHAR(8000),New.ExtraCurrGrpCode,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ExtraCurrGrpId
                               ,'ExtraCurrGrpDescrip'
                               ,CONVERT(VARCHAR(8000),New.ExtraCurrGrpDescrip,121)
                        FROM    Inserted New; 
            END; 
        END;



    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[adExtraCurrGrp_Audit_Update] ON [dbo].[adExtraCurrGrp]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adExtraCurrGrp','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ExtraCurrGrpId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ExtraCurrGrpId = New.ExtraCurrGrpId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ExtraCurrGrpId
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ExtraCurrGrpId = New.ExtraCurrGrpId
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 
                IF UPDATE(ExtraCurrGrpCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ExtraCurrGrpId
                                   ,'ExtraCurrGrpCode'
                                   ,CONVERT(VARCHAR(8000),Old.ExtraCurrGrpCode,121)
                                   ,CONVERT(VARCHAR(8000),New.ExtraCurrGrpCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ExtraCurrGrpId = New.ExtraCurrGrpId
                            WHERE   Old.ExtraCurrGrpCode <> New.ExtraCurrGrpCode
                                    OR (
                                         Old.ExtraCurrGrpCode IS NULL
                                         AND New.ExtraCurrGrpCode IS NOT NULL
                                       )
                                    OR (
                                         New.ExtraCurrGrpCode IS NULL
                                         AND Old.ExtraCurrGrpCode IS NOT NULL
                                       ); 
                IF UPDATE(ExtraCurrGrpDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ExtraCurrGrpId
                                   ,'ExtraCurrGrpDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.ExtraCurrGrpDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.ExtraCurrGrpDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ExtraCurrGrpId = New.ExtraCurrGrpId
                            WHERE   Old.ExtraCurrGrpDescrip <> New.ExtraCurrGrpDescrip
                                    OR (
                                         Old.ExtraCurrGrpDescrip IS NULL
                                         AND New.ExtraCurrGrpDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.ExtraCurrGrpDescrip IS NULL
                                         AND Old.ExtraCurrGrpDescrip IS NOT NULL
                                       ); 
            END; 
        END;



    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[adExtraCurrGrp] ADD CONSTRAINT [PK_adExtraCurrGrp_ExtraCurrGrpId] PRIMARY KEY CLUSTERED  ([ExtraCurrGrpId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_adExtraCurrGrp_ExtraCurrGrpCode_ExtraCurrGrpDescrip] ON [dbo].[adExtraCurrGrp] ([ExtraCurrGrpCode], [ExtraCurrGrpDescrip]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adExtraCurrGrp] ADD CONSTRAINT [FK_adExtraCurrGrp_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[adExtraCurrGrp] ADD CONSTRAINT [FK_adExtraCurrGrp_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
