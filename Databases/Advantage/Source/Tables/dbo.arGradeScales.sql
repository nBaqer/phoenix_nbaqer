CREATE TABLE [dbo].[arGradeScales]
(
[GrdScaleId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arGradeScales_GrdScaleId] DEFAULT (newid()),
[InstructorId] [uniqueidentifier] NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[Descrip] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[GrdSystemId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[arGradeScales_Audit_Delete] ON [dbo].[arGradeScales]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arGradeScales','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.GrdScaleId
                               ,'GrdSystemId'
                               ,CONVERT(VARCHAR(8000),Old.GrdSystemId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.GrdScaleId
                               ,'InstructorId'
                               ,CONVERT(VARCHAR(8000),Old.InstructorId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.GrdScaleId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.GrdScaleId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.GrdScaleId
                               ,'Descrip'
                               ,CONVERT(VARCHAR(8000),Old.Descrip,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[arGradeScales_Audit_Insert] ON [dbo].[arGradeScales]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arGradeScales','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.GrdScaleId
                               ,'GrdSystemId'
                               ,CONVERT(VARCHAR(8000),New.GrdSystemId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.GrdScaleId
                               ,'InstructorId'
                               ,CONVERT(VARCHAR(8000),New.InstructorId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.GrdScaleId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.GrdScaleId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.GrdScaleId
                               ,'Descrip'
                               ,CONVERT(VARCHAR(8000),New.Descrip,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[arGradeScales_Audit_Update] ON [dbo].[arGradeScales]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arGradeScales','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(GrdSystemId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.GrdScaleId
                                   ,'GrdSystemId'
                                   ,CONVERT(VARCHAR(8000),Old.GrdSystemId,121)
                                   ,CONVERT(VARCHAR(8000),New.GrdSystemId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.GrdScaleId = New.GrdScaleId
                            WHERE   Old.GrdSystemId <> New.GrdSystemId
                                    OR (
                                         Old.GrdSystemId IS NULL
                                         AND New.GrdSystemId IS NOT NULL
                                       )
                                    OR (
                                         New.GrdSystemId IS NULL
                                         AND Old.GrdSystemId IS NOT NULL
                                       ); 
                IF UPDATE(InstructorId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.GrdScaleId
                                   ,'InstructorId'
                                   ,CONVERT(VARCHAR(8000),Old.InstructorId,121)
                                   ,CONVERT(VARCHAR(8000),New.InstructorId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.GrdScaleId = New.GrdScaleId
                            WHERE   Old.InstructorId <> New.InstructorId
                                    OR (
                                         Old.InstructorId IS NULL
                                         AND New.InstructorId IS NOT NULL
                                       )
                                    OR (
                                         New.InstructorId IS NULL
                                         AND Old.InstructorId IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.GrdScaleId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.GrdScaleId = New.GrdScaleId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.GrdScaleId
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.GrdScaleId = New.GrdScaleId
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 
                IF UPDATE(Descrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.GrdScaleId
                                   ,'Descrip'
                                   ,CONVERT(VARCHAR(8000),Old.Descrip,121)
                                   ,CONVERT(VARCHAR(8000),New.Descrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.GrdScaleId = New.GrdScaleId
                            WHERE   Old.Descrip <> New.Descrip
                                    OR (
                                         Old.Descrip IS NULL
                                         AND New.Descrip IS NOT NULL
                                       )
                                    OR (
                                         New.Descrip IS NULL
                                         AND Old.Descrip IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[arGradeScales] ADD CONSTRAINT [PK_arGradeScales_GrdScaleId] PRIMARY KEY CLUSTERED  ([GrdScaleId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_arGradeScales_Descrip_CampGrpId] ON [dbo].[arGradeScales] ([Descrip], [CampGrpId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arGradeScales] ADD CONSTRAINT [FK_arGradeScales_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[arGradeScales] ADD CONSTRAINT [FK_arGradeScales_arGradeSystems_GrdSystemId_GrdSystemId] FOREIGN KEY ([GrdSystemId]) REFERENCES [dbo].[arGradeSystems] ([GrdSystemId])
GO
ALTER TABLE [dbo].[arGradeScales] ADD CONSTRAINT [FK_arGradeScales_syUsers_InstructorId_UserId] FOREIGN KEY ([InstructorId]) REFERENCES [dbo].[syUsers] ([UserId])
GO
ALTER TABLE [dbo].[arGradeScales] ADD CONSTRAINT [FK_arGradeScales_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
