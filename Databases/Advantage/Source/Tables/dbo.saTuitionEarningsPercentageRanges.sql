CREATE TABLE [dbo].[saTuitionEarningsPercentageRanges]
(
[TuitionEarningsPercentageRangeId] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[TuitionEarningId] [uniqueidentifier] NOT NULL,
[UpTo] [decimal] (5, 2) NOT NULL,
[EarnPercent] [decimal] (5, 2) NOT NULL,
[ViewOrder] [int] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saTuitionEarningsPercentageRanges] ADD CONSTRAINT [PK_saTuitionEarningsPercentageRanges_TuitionEarningsPercentageRangeId] PRIMARY KEY CLUSTERED  ([TuitionEarningsPercentageRangeId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saTuitionEarningsPercentageRanges] ADD CONSTRAINT [FK_saTuitionEarningsPercentageRanges_saTuitionEarnings_TuitionEarningId_TuitionEarningId] FOREIGN KEY ([TuitionEarningId]) REFERENCES [dbo].[saTuitionEarnings] ([TuitionEarningId])
GO
