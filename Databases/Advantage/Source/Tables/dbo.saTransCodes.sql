CREATE TABLE [dbo].[saTransCodes]
(
[TransCodeId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_saTransCodes_TransCodeId] DEFAULT (newid()),
[TransCodeCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[TransCodeDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[BillTypeId] [uniqueidentifier] NOT NULL,
[DefEarnings] [bit] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[IsInstCharge] [bit] NOT NULL CONSTRAINT [DF_saTransCodes_IsInstCharge] DEFAULT ((0)),
[SysTransCodeId] [int] NULL,
[Is1098T] [bit] NOT NULL CONSTRAINT [DF_saTransCodes_Is1098T] DEFAULT ((0))
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saTransCodes_Audit_Delete] ON [dbo].[saTransCodes]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saTransCodes','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransCodeId
                               ,'BillTypeId'
                               ,CONVERT(VARCHAR(8000),Old.BillTypeId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransCodeId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransCodeId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransCodeId
                               ,'TransCodeCode'
                               ,CONVERT(VARCHAR(8000),Old.TransCodeCode,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransCodeId
                               ,'TransCodeDescrip'
                               ,CONVERT(VARCHAR(8000),Old.TransCodeDescrip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransCodeId
                               ,'DefEarnings'
                               ,CONVERT(VARCHAR(8000),Old.DefEarnings,121)
                        FROM    Deleted Old; 
            END; 
        END;



    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saTransCodes_Audit_Insert] ON [dbo].[saTransCodes]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saTransCodes','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransCodeId
                               ,'BillTypeId'
                               ,CONVERT(VARCHAR(8000),New.BillTypeId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransCodeId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransCodeId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransCodeId
                               ,'TransCodeCode'
                               ,CONVERT(VARCHAR(8000),New.TransCodeCode,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransCodeId
                               ,'TransCodeDescrip'
                               ,CONVERT(VARCHAR(8000),New.TransCodeDescrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransCodeId
                               ,'DefEarnings'
                               ,CONVERT(VARCHAR(8000),New.DefEarnings,121)
                        FROM    Inserted New; 
            END; 
        END;



    SET NOCOUNT OFF; 
GO
GO

GO

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saTransCodes_Audit_Update] ON [dbo].[saTransCodes]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saTransCodes','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(BillTypeId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TransCodeId
                                   ,'BillTypeId'
                                   ,CONVERT(VARCHAR(8000),Old.BillTypeId,121)
                                   ,CONVERT(VARCHAR(8000),New.BillTypeId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TransCodeId = New.TransCodeId
                            WHERE   Old.BillTypeId <> New.BillTypeId
                                    OR (
                                         Old.BillTypeId IS NULL
                                         AND New.BillTypeId IS NOT NULL
                                       )
                                    OR (
                                         New.BillTypeId IS NULL
                                         AND Old.BillTypeId IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TransCodeId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TransCodeId = New.TransCodeId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TransCodeId
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TransCodeId = New.TransCodeId
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 
                IF UPDATE(TransCodeCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TransCodeId
                                   ,'TransCodeCode'
                                   ,CONVERT(VARCHAR(8000),Old.TransCodeCode,121)
                                   ,CONVERT(VARCHAR(8000),New.TransCodeCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TransCodeId = New.TransCodeId
                            WHERE   Old.TransCodeCode <> New.TransCodeCode
                                    OR (
                                         Old.TransCodeCode IS NULL
                                         AND New.TransCodeCode IS NOT NULL
                                       )
                                    OR (
                                         New.TransCodeCode IS NULL
                                         AND Old.TransCodeCode IS NOT NULL
                                       ); 
                IF UPDATE(TransCodeDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TransCodeId
                                   ,'TransCodeDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.TransCodeDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.TransCodeDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TransCodeId = New.TransCodeId
                            WHERE   Old.TransCodeDescrip <> New.TransCodeDescrip
                                    OR (
                                         Old.TransCodeDescrip IS NULL
                                         AND New.TransCodeDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.TransCodeDescrip IS NULL
                                         AND Old.TransCodeDescrip IS NOT NULL
                                       ); 
                IF UPDATE(DefEarnings)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TransCodeId
                                   ,'DefEarnings'
                                   ,CONVERT(VARCHAR(8000),Old.DefEarnings,121)
                                   ,CONVERT(VARCHAR(8000),New.DefEarnings,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TransCodeId = New.TransCodeId
                            WHERE   Old.DefEarnings <> New.DefEarnings
                                    OR (
                                         Old.DefEarnings IS NULL
                                         AND New.DefEarnings IS NOT NULL
                                       )
                                    OR (
                                         New.DefEarnings IS NULL
                                         AND Old.DefEarnings IS NOT NULL
                                       ); 
            END; 
        END;



    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[saTransCodes] ADD CONSTRAINT [PK_saTransCodes_TransCodeId] PRIMARY KEY CLUSTERED  ([TransCodeId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_saTransCodes_TransCodeCode_TransCodeDescrip_CampGrpId] ON [dbo].[saTransCodes] ([TransCodeCode], [TransCodeDescrip], [CampGrpId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saTransCodes] ADD CONSTRAINT [FK_saTransCodes_saBillTypes_BillTypeId_BillTypeId] FOREIGN KEY ([BillTypeId]) REFERENCES [dbo].[saBillTypes] ([BillTypeId])
GO
ALTER TABLE [dbo].[saTransCodes] ADD CONSTRAINT [FK_saTransCodes_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[saTransCodes] ADD CONSTRAINT [FK_saTransCodes_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
ALTER TABLE [dbo].[saTransCodes] ADD CONSTRAINT [FK_saTransCodes_saSysTransCodes_SysTransCodeId_SysTransCodeId] FOREIGN KEY ([SysTransCodeId]) REFERENCES [dbo].[saSysTransCodes] ([SysTransCodeId])
GO
