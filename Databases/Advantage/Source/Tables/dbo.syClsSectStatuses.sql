CREATE TABLE [dbo].[syClsSectStatuses]
(
[ClsSectStatusId] [int] NOT NULL,
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syClsSectStatuses] ADD CONSTRAINT [PK_syClsSectStatuses_ClsSectStatusId] PRIMARY KEY CLUSTERED  ([ClsSectStatusId]) ON [PRIMARY]
GO
