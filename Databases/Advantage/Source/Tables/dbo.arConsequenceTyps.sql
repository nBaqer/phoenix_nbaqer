CREATE TABLE [dbo].[arConsequenceTyps]
(
[ConsequenceTypId] [tinyint] NOT NULL,
[ConseqTypDesc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arConsequenceTyps] ADD CONSTRAINT [PK_arConsequenceTyps_ConsequenceTypId] PRIMARY KEY CLUSTERED  ([ConsequenceTypId]) ON [PRIMARY]
GO
