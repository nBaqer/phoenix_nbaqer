CREATE TABLE [dbo].[arTrigUnitTyps]
(
[TrigUnitTypId] [tinyint] NOT NULL,
[TrigUnitTypDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arTrigUnitTyps] ADD CONSTRAINT [PK_arTrigUnitTyps_TrigUnitTypId] PRIMARY KEY CLUSTERED  ([TrigUnitTypId]) ON [PRIMARY]
GO
