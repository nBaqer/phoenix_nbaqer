CREATE TABLE [dbo].[syStates]
(
[StateId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syStates_StateId] DEFAULT (newid()),
[StateCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NULL,
[StateDescrip] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NULL,
[fipscode] [int] NULL,
[IsStateBoard] [bit] NULL,
[CountryId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syStates] ADD CONSTRAINT [PK_syStates_StateId] PRIMARY KEY CLUSTERED  ([StateId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syStates] ADD CONSTRAINT [FK_syStates_adCountries_countryId_countryId] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[adCountries] ([CountryId])
GO
ALTER TABLE [dbo].[syStates] ADD CONSTRAINT [FK_syStates_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[syStates] ADD CONSTRAINT [FK_syStates_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
