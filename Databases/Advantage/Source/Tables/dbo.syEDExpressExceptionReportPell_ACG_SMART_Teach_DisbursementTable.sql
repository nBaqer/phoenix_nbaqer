CREATE TABLE [dbo].[syEDExpressExceptionReportPell_ACG_SMART_Teach_DisbursementTable]
(
[DetailId] [uniqueidentifier] NOT NULL,
[ExceptionReportId] [uniqueidentifier] NOT NULL,
[DbIn] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Filter] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccDisbAmount] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisbDate] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisbNum] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisbRelIndi] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DusbSeqNum] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SimittedDisbAmount] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActionStatusDisb] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginalSSN] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[ErrorMsg] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExceptionGUID] [uniqueidentifier] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[syEDExpressExceptionReportPell_ACG_SMART_Teach_DisbursementTable] ADD CONSTRAINT [PK_syEDExpressExceptionReportPell_ACG_SMART_Teach_DisbursementTable_DetailId] PRIMARY KEY CLUSTERED  ([DetailId]) ON [PRIMARY]
GO
