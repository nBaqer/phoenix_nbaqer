CREATE TABLE [dbo].[syCertifications]
(
[CertificationId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syCertifications_CertificationId] DEFAULT (newid()),
[CertificationCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[CertificationDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syCertifications] ADD CONSTRAINT [PK_syCertifications_CertificationId] PRIMARY KEY CLUSTERED  ([CertificationId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_syCertifications_CertificationCode_CertificationDescrip_CampGrpId] ON [dbo].[syCertifications] ([CertificationCode], [CertificationDescrip], [CampGrpId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syCertifications] ADD CONSTRAINT [FK_syCertifications_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[syCertifications] ADD CONSTRAINT [FK_syCertifications_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
