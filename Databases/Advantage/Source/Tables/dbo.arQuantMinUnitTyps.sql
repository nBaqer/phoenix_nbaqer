CREATE TABLE [dbo].[arQuantMinUnitTyps]
(
[QuantMinUnitTypId] [tinyint] NOT NULL,
[QuantMinTypDesc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arQuantMinUnitTyps] ADD CONSTRAINT [PK_arQuantMinUnitTyps_QuantMinUnitTypId] PRIMARY KEY CLUSTERED  ([QuantMinUnitTypId]) ON [PRIMARY]
GO
