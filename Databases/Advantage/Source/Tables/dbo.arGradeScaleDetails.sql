CREATE TABLE [dbo].[arGradeScaleDetails]
(
[GrdScaleDetailId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arGradeScaleDetails_GrdScaleDetailId] DEFAULT (newid()),
[GrdScaleId] [uniqueidentifier] NOT NULL,
[MinVal] [smallint] NOT NULL,
[MaxVal] [smallint] NOT NULL,
[GrdSysDetailId] [uniqueidentifier] NOT NULL,
[ViewOrder] [int] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arGradeScaleDetails] ADD CONSTRAINT [PK_arGradeScaleDetails_GrdScaleDetailId] PRIMARY KEY CLUSTERED  ([GrdScaleDetailId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arGradeScaleDetails] ADD CONSTRAINT [FK_arGradeScaleDetails_arGradeScales_GrdScaleId_GrdScaleId] FOREIGN KEY ([GrdScaleId]) REFERENCES [dbo].[arGradeScales] ([GrdScaleId])
GO
ALTER TABLE [dbo].[arGradeScaleDetails] ADD CONSTRAINT [FK_arGradeScaleDetails_arGradeSystemDetails_GrdSysDetailId_GrdSysDetailId] FOREIGN KEY ([GrdSysDetailId]) REFERENCES [dbo].[arGradeSystemDetails] ([GrdSysDetailId])
GO
