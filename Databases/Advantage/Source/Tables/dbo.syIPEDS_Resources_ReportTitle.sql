CREATE TABLE [dbo].[syIPEDS_Resources_ReportTitle]
(
[Id] [uniqueidentifier] NOT NULL,
[ResourceId] [int] NULL,
[SurveyTitle] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReportTitle] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syIPEDS_Resources_ReportTitle] ADD CONSTRAINT [PK_syIPEDS_Resources_ReportTitle_Id] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
