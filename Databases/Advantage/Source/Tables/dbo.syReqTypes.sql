CREATE TABLE [dbo].[syReqTypes]
(
[ReqTypeId] [int] NOT NULL,
[ReqType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syReqTypes] ADD CONSTRAINT [PK_syReqTypes_ReqTypeId] PRIMARY KEY CLUSTERED  ([ReqTypeId]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[syReqTypes] TO [AdvantageRole]
GO
