CREATE TABLE [dbo].[adLeadOtherContactsPhone]
(
[OtherContactsPhoneId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_adLeadOtherContactsPhone_OtherContactsPhoneId] DEFAULT (newid()),
[OtherContactId] [uniqueidentifier] NOT NULL,
[LeadId] [uniqueidentifier] NOT NULL,
[PhoneTypeId] [uniqueidentifier] NOT NULL,
[Phone] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Extension] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_adLeadOtherContactsPhone_extension] DEFAULT (''),
[IsForeignPhone] [bit] NOT NULL CONSTRAINT [DF_adLeadOtherContactsPhone_IsForeignPhone] DEFAULT ((0)),
[StatusId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
--========================================================================================== 
-- TRIGGER adLeadOtherContactsPhone_Audit_Delete 
-- DELETE  add Audit History when delete records in adLeadOtherContactsPhone
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadOtherContactsPhone_Audit_Delete] ON [dbo].[adLeadOtherContactsPhone]
    FOR DELETE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Deleted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Deleted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Deleted
                        );
 
        IF @EventRows > 0
            BEGIN
                EXEC fmAuditHistAdd @AuditHistId,'adLeadOtherContactsPhone','D',@EventRows,@EventDate,@UserName;
                -- OtherContactId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsPhoneId
                               ,'OtherContactId'
                               ,CONVERT(VARCHAR(MAX),Old.OtherContactId)
                        FROM    Deleted AS Old;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsPhoneId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),Old.LeadId)
                        FROM    Deleted AS Old;
                -- PhoneTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsPhoneId
                               ,'PhoneTypeId'
                               ,CONVERT(VARCHAR(MAX),Old.PhoneTypeId)
                        FROM    Deleted AS Old;
                -- Phone 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsPhoneId
                               ,'Phone'
                               ,CONVERT(VARCHAR(MAX),Old.Phone)
                        FROM    Deleted AS Old;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsPhoneId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                        FROM    Deleted AS Old;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsPhoneId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),Old.ModUser)
                        FROM    Deleted AS Old;
                -- Extension 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsPhoneId
                               ,'Extension'
                               ,CONVERT(VARCHAR(MAX),Old.Extension)
                        FROM    Deleted AS Old;
                -- IsForeignPhone 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsPhoneId
                               ,'IsForeignPhone'
                               ,CONVERT(VARCHAR(MAX),Old.IsForeignPhone)
                        FROM    Deleted AS Old;
                -- StatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsPhoneId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(MAX),Old.StatusId)
                        FROM    Deleted AS Old;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadOtherContactsPhone_Audit_Delete 
--========================================================================================== 
 

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
--========================================================================================== 
-- TRIGGER adLeadOtherContactsPhone_Audit_Insert 
-- INSERT  add Audit History when insert records in adLeadOtherContactsPhone
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadOtherContactsPhone_Audit_Insert] ON [dbo].[adLeadOtherContactsPhone]
    FOR INSERT
AS
    BEGIN 
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadOtherContactsPhone','I',@EventRows,@EventDate,@UserName;
                -- OtherContactId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsPhoneId
                               ,'OtherContactId'
                               ,CONVERT(VARCHAR(MAX),New.OtherContactId)
                        FROM    Inserted AS New;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsPhoneId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),New.LeadId)
                        FROM    Inserted AS New;
                -- PhoneTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsPhoneId
                               ,'PhoneTypeId'
                               ,CONVERT(VARCHAR(MAX),New.PhoneTypeId)
                        FROM    Inserted AS New;
                -- Phone 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsPhoneId
                               ,'Phone'
                               ,CONVERT(VARCHAR(MAX),New.Phone)
                        FROM    Inserted AS New;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsPhoneId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                        FROM    Inserted AS New;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsPhoneId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),New.ModUser)
                        FROM    Inserted AS New;
                -- Extension 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsPhoneId
                               ,'Extension'
                               ,CONVERT(VARCHAR(MAX),New.Extension)
                        FROM    Inserted AS New;
                -- IsForeignPhone 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsPhoneId
                               ,'IsForeignPhone'
                               ,CONVERT(VARCHAR(MAX),New.IsForeignPhone)
                        FROM    Inserted AS New;
                -- StatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsPhoneId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(MAX),New.StatusId)
                        FROM    Inserted AS New;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadOtherContactsPhone_Audit_Insert 
--========================================================================================== 
 

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--========================================================================================== 
-- TRIGGER adLeadOtherContactsPhone_Audit_Update 
-- UPDATE  add Audit History when update fields in adLeadOtherContactsPhone
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadOtherContactsPhone_Audit_Update] ON [dbo].[adLeadOtherContactsPhone]
    FOR UPDATE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadOtherContactsPhone','U',@EventRows,@EventDate,@UserName;
                -- OtherContactId 
                IF UPDATE(OtherContactId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsPhoneId
                                       ,'OtherContactId'
                                       ,CONVERT(VARCHAR(MAX),Old.OtherContactId)
                                       ,CONVERT(VARCHAR(MAX),New.OtherContactId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsPhoneId = New.OtherContactsPhoneId
                                WHERE   Old.OtherContactId <> New.OtherContactId
                                        OR (
                                             Old.OtherContactId IS NULL
                                             AND New.OtherContactId IS NOT NULL
                                           )
                                        OR (
                                             New.OtherContactId IS NULL
                                             AND Old.OtherContactId IS NOT NULL
                                           );
                    END;
                -- LeadId 
                IF UPDATE(LeadId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsPhoneId
                                       ,'LeadId'
                                       ,CONVERT(VARCHAR(MAX),Old.LeadId)
                                       ,CONVERT(VARCHAR(MAX),New.LeadId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsPhoneId = New.OtherContactsPhoneId
                                WHERE   Old.LeadId <> New.LeadId
                                        OR (
                                             Old.LeadId IS NULL
                                             AND New.LeadId IS NOT NULL
                                           )
                                        OR (
                                             New.LeadId IS NULL
                                             AND Old.LeadId IS NOT NULL
                                           );
                    END;
                -- PhoneTypeId 
                IF UPDATE(PhoneTypeId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsPhoneId
                                       ,'PhoneTypeId'
                                       ,CONVERT(VARCHAR(MAX),Old.PhoneTypeId)
                                       ,CONVERT(VARCHAR(MAX),New.PhoneTypeId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsPhoneId = New.OtherContactsPhoneId
                                WHERE   Old.PhoneTypeId <> New.PhoneTypeId
                                        OR (
                                             Old.PhoneTypeId IS NULL
                                             AND New.PhoneTypeId IS NOT NULL
                                           )
                                        OR (
                                             New.PhoneTypeId IS NULL
                                             AND Old.PhoneTypeId IS NOT NULL
                                           );
                    END;
                -- Phone 
                IF UPDATE(Phone)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsPhoneId
                                       ,'Phone'
                                       ,CONVERT(VARCHAR(MAX),Old.Phone)
                                       ,CONVERT(VARCHAR(MAX),New.Phone)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsPhoneId = New.OtherContactsPhoneId
                                WHERE   Old.Phone <> New.Phone
                                        OR (
                                             Old.Phone IS NULL
                                             AND New.Phone IS NOT NULL
                                           )
                                        OR (
                                             New.Phone IS NULL
                                             AND Old.Phone IS NOT NULL
                                           );
                    END;
                -- ModDate 
                IF UPDATE(ModDate)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsPhoneId
                                       ,'ModDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsPhoneId = New.OtherContactsPhoneId
                                WHERE   Old.ModDate <> New.ModDate
                                        OR (
                                             Old.ModDate IS NULL
                                             AND New.ModDate IS NOT NULL
                                           )
                                        OR (
                                             New.ModDate IS NULL
                                             AND Old.ModDate IS NOT NULL
                                           );
                    END;
                -- ModUser 
                IF UPDATE(ModUser)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsPhoneId
                                       ,'ModUser'
                                       ,CONVERT(VARCHAR(MAX),Old.ModUser)
                                       ,CONVERT(VARCHAR(MAX),New.ModUser)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsPhoneId = New.OtherContactsPhoneId
                                WHERE   Old.ModUser <> New.ModUser
                                        OR (
                                             Old.ModUser IS NULL
                                             AND New.ModUser IS NOT NULL
                                           )
                                        OR (
                                             New.ModUser IS NULL
                                             AND Old.ModUser IS NOT NULL
                                           );
                    END;
                -- Extension 
                IF UPDATE(Extension)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsPhoneId
                                       ,'Extension'
                                       ,CONVERT(VARCHAR(MAX),Old.Extension)
                                       ,CONVERT(VARCHAR(MAX),New.Extension)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsPhoneId = New.OtherContactsPhoneId
                                WHERE   Old.Extension <> New.Extension
                                        OR (
                                             Old.Extension IS NULL
                                             AND New.Extension IS NOT NULL
                                           )
                                        OR (
                                             New.Extension IS NULL
                                             AND Old.Extension IS NOT NULL
                                           );
                    END;
                -- IsForeignPhone 
                IF UPDATE(IsForeignPhone)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsPhoneId
                                       ,'IsForeignPhone'
                                       ,CONVERT(VARCHAR(MAX),Old.IsForeignPhone)
                                       ,CONVERT(VARCHAR(MAX),New.IsForeignPhone)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsPhoneId = New.OtherContactsPhoneId
                                WHERE   Old.IsForeignPhone <> New.IsForeignPhone
                                        OR (
                                             Old.IsForeignPhone IS NULL
                                             AND New.IsForeignPhone IS NOT NULL
                                           )
                                        OR (
                                             New.IsForeignPhone IS NULL
                                             AND Old.IsForeignPhone IS NOT NULL
                                           );
                    END;
                -- StatusId 
                IF UPDATE(StatusId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsPhoneId
                                       ,'StatusId'
                                       ,CONVERT(VARCHAR(MAX),Old.StatusId)
                                       ,CONVERT(VARCHAR(MAX),New.StatusId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsPhoneId = New.OtherContactsPhoneId
                                WHERE   Old.StatusId <> New.StatusId
                                        OR (
                                             Old.StatusId IS NULL
                                             AND New.StatusId IS NOT NULL
                                           )
                                        OR (
                                             New.StatusId IS NULL
                                             AND Old.StatusId IS NOT NULL
                                           );
                    END;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadOtherContactsPhone_Audit_Update 
--========================================================================================== 
 

GO
ALTER TABLE [dbo].[adLeadOtherContactsPhone] ADD CONSTRAINT [PK_adLeadOtherContactsPhone_OtherContactsPhoneId] PRIMARY KEY CLUSTERED  ([OtherContactsPhoneId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adLeadOtherContactsPhone] ADD CONSTRAINT [FK_adLeadOtherContactsPhone_adLeadOtherContacts_OtherContactId_OtherContactId] FOREIGN KEY ([OtherContactId]) REFERENCES [dbo].[adLeadOtherContacts] ([OtherContactId])
GO
ALTER TABLE [dbo].[adLeadOtherContactsPhone] ADD CONSTRAINT [FK_adLeadOtherContactsPhone_adLeads_LeadId_LeadId] FOREIGN KEY ([LeadId]) REFERENCES [dbo].[adLeads] ([LeadId])
GO
ALTER TABLE [dbo].[adLeadOtherContactsPhone] ADD CONSTRAINT [FK_adLeadOtherContactsPhone_syPhoneType_PhoneTypeId_PhoneTypeId] FOREIGN KEY ([PhoneTypeId]) REFERENCES [dbo].[syPhoneType] ([PhoneTypeId])
GO
ALTER TABLE [dbo].[adLeadOtherContactsPhone] ADD CONSTRAINT [FK_adLeadOtherContactsPhone_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
