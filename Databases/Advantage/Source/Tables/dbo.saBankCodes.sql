CREATE TABLE [dbo].[saBankCodes]
(
[BankId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_saBankCodes_BankId] DEFAULT (newid()),
[StatusId] [uniqueidentifier] NOT NULL,
[Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BankDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NULL,
[Address1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateId] [uniqueidentifier] NULL,
[Zip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fax] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Title] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[ForeignPhone] [bit] NULL CONSTRAINT [DF_saBankCodes_ForeignPhone] DEFAULT ((0)),
[ForeignFax] [bit] NULL CONSTRAINT [DF_saBankCodes_ForeignFax] DEFAULT ((0)),
[ForeignZip] [bit] NULL,
[OtherState] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saBankCodes_Audit_Delete] ON [dbo].[saBankCodes]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saBankCodes','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BankId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BankId
                               ,'Code'
                               ,CONVERT(VARCHAR(8000),Old.Code,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BankId
                               ,'BankDescrip'
                               ,CONVERT(VARCHAR(8000),Old.BankDescrip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BankId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BankId
                               ,'Address1'
                               ,CONVERT(VARCHAR(8000),Old.Address1,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BankId
                               ,'Address2'
                               ,CONVERT(VARCHAR(8000),Old.Address2,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BankId
                               ,'City'
                               ,CONVERT(VARCHAR(8000),Old.City,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BankId
                               ,'StateId'
                               ,CONVERT(VARCHAR(8000),Old.StateId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BankId
                               ,'Zip'
                               ,CONVERT(VARCHAR(8000),Old.Zip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BankId
                               ,'Phone'
                               ,CONVERT(VARCHAR(8000),Old.Phone,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BankId
                               ,'Fax'
                               ,CONVERT(VARCHAR(8000),Old.Fax,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BankId
                               ,'Email'
                               ,CONVERT(VARCHAR(8000),Old.Email,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BankId
                               ,'FirstName'
                               ,CONVERT(VARCHAR(8000),Old.FirstName,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BankId
                               ,'LastName'
                               ,CONVERT(VARCHAR(8000),Old.LastName,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BankId
                               ,'Title'
                               ,CONVERT(VARCHAR(8000),Old.Title,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BankId
                               ,'ForeignPhone'
                               ,CONVERT(VARCHAR(8000),Old.ForeignPhone,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BankId
                               ,'ForeignFax'
                               ,CONVERT(VARCHAR(8000),Old.ForeignFax,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO
GO

GO

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saBankCodes_Audit_Insert] ON [dbo].[saBankCodes]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saBankCodes','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BankId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BankId
                               ,'Code'
                               ,CONVERT(VARCHAR(8000),New.Code,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BankId
                               ,'BankDescrip'
                               ,CONVERT(VARCHAR(8000),New.BankDescrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BankId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BankId
                               ,'Address1'
                               ,CONVERT(VARCHAR(8000),New.Address1,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BankId
                               ,'Address2'
                               ,CONVERT(VARCHAR(8000),New.Address2,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BankId
                               ,'City'
                               ,CONVERT(VARCHAR(8000),New.City,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BankId
                               ,'StateId'
                               ,CONVERT(VARCHAR(8000),New.StateId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BankId
                               ,'Zip'
                               ,CONVERT(VARCHAR(8000),New.Zip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BankId
                               ,'Phone'
                               ,CONVERT(VARCHAR(8000),New.Phone,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BankId
                               ,'Fax'
                               ,CONVERT(VARCHAR(8000),New.Fax,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BankId
                               ,'Email'
                               ,CONVERT(VARCHAR(8000),New.Email,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BankId
                               ,'FirstName'
                               ,CONVERT(VARCHAR(8000),New.FirstName,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BankId
                               ,'LastName'
                               ,CONVERT(VARCHAR(8000),New.LastName,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BankId
                               ,'Title'
                               ,CONVERT(VARCHAR(8000),New.Title,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BankId
                               ,'ForeignPhone'
                               ,CONVERT(VARCHAR(8000),New.ForeignPhone,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BankId
                               ,'ForeignFax'
                               ,CONVERT(VARCHAR(8000),New.ForeignFax,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saBankCodes_Audit_Update] ON [dbo].[saBankCodes]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saBankCodes','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BankId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BankId = New.BankId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(Code)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BankId
                                   ,'Code'
                                   ,CONVERT(VARCHAR(8000),Old.Code,121)
                                   ,CONVERT(VARCHAR(8000),New.Code,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BankId = New.BankId
                            WHERE   Old.Code <> New.Code
                                    OR (
                                         Old.Code IS NULL
                                         AND New.Code IS NOT NULL
                                       )
                                    OR (
                                         New.Code IS NULL
                                         AND Old.Code IS NOT NULL
                                       ); 
                IF UPDATE(BankDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BankId
                                   ,'BankDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.BankDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.BankDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BankId = New.BankId
                            WHERE   Old.BankDescrip <> New.BankDescrip
                                    OR (
                                         Old.BankDescrip IS NULL
                                         AND New.BankDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.BankDescrip IS NULL
                                         AND Old.BankDescrip IS NOT NULL
                                       ); 
                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BankId
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BankId = New.BankId
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 
                IF UPDATE(Address1)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BankId
                                   ,'Address1'
                                   ,CONVERT(VARCHAR(8000),Old.Address1,121)
                                   ,CONVERT(VARCHAR(8000),New.Address1,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BankId = New.BankId
                            WHERE   Old.Address1 <> New.Address1
                                    OR (
                                         Old.Address1 IS NULL
                                         AND New.Address1 IS NOT NULL
                                       )
                                    OR (
                                         New.Address1 IS NULL
                                         AND Old.Address1 IS NOT NULL
                                       ); 
                IF UPDATE(Address2)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BankId
                                   ,'Address2'
                                   ,CONVERT(VARCHAR(8000),Old.Address2,121)
                                   ,CONVERT(VARCHAR(8000),New.Address2,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BankId = New.BankId
                            WHERE   Old.Address2 <> New.Address2
                                    OR (
                                         Old.Address2 IS NULL
                                         AND New.Address2 IS NOT NULL
                                       )
                                    OR (
                                         New.Address2 IS NULL
                                         AND Old.Address2 IS NOT NULL
                                       ); 
                IF UPDATE(City)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BankId
                                   ,'City'
                                   ,CONVERT(VARCHAR(8000),Old.City,121)
                                   ,CONVERT(VARCHAR(8000),New.City,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BankId = New.BankId
                            WHERE   Old.City <> New.City
                                    OR (
                                         Old.City IS NULL
                                         AND New.City IS NOT NULL
                                       )
                                    OR (
                                         New.City IS NULL
                                         AND Old.City IS NOT NULL
                                       ); 
                IF UPDATE(StateId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BankId
                                   ,'StateId'
                                   ,CONVERT(VARCHAR(8000),Old.StateId,121)
                                   ,CONVERT(VARCHAR(8000),New.StateId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BankId = New.BankId
                            WHERE   Old.StateId <> New.StateId
                                    OR (
                                         Old.StateId IS NULL
                                         AND New.StateId IS NOT NULL
                                       )
                                    OR (
                                         New.StateId IS NULL
                                         AND Old.StateId IS NOT NULL
                                       ); 
                IF UPDATE(Zip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BankId
                                   ,'Zip'
                                   ,CONVERT(VARCHAR(8000),Old.Zip,121)
                                   ,CONVERT(VARCHAR(8000),New.Zip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BankId = New.BankId
                            WHERE   Old.Zip <> New.Zip
                                    OR (
                                         Old.Zip IS NULL
                                         AND New.Zip IS NOT NULL
                                       )
                                    OR (
                                         New.Zip IS NULL
                                         AND Old.Zip IS NOT NULL
                                       ); 
                IF UPDATE(Phone)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BankId
                                   ,'Phone'
                                   ,CONVERT(VARCHAR(8000),Old.Phone,121)
                                   ,CONVERT(VARCHAR(8000),New.Phone,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BankId = New.BankId
                            WHERE   Old.Phone <> New.Phone
                                    OR (
                                         Old.Phone IS NULL
                                         AND New.Phone IS NOT NULL
                                       )
                                    OR (
                                         New.Phone IS NULL
                                         AND Old.Phone IS NOT NULL
                                       ); 
                IF UPDATE(Fax)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BankId
                                   ,'Fax'
                                   ,CONVERT(VARCHAR(8000),Old.Fax,121)
                                   ,CONVERT(VARCHAR(8000),New.Fax,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BankId = New.BankId
                            WHERE   Old.Fax <> New.Fax
                                    OR (
                                         Old.Fax IS NULL
                                         AND New.Fax IS NOT NULL
                                       )
                                    OR (
                                         New.Fax IS NULL
                                         AND Old.Fax IS NOT NULL
                                       ); 
                IF UPDATE(Email)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BankId
                                   ,'Email'
                                   ,CONVERT(VARCHAR(8000),Old.Email,121)
                                   ,CONVERT(VARCHAR(8000),New.Email,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BankId = New.BankId
                            WHERE   Old.Email <> New.Email
                                    OR (
                                         Old.Email IS NULL
                                         AND New.Email IS NOT NULL
                                       )
                                    OR (
                                         New.Email IS NULL
                                         AND Old.Email IS NOT NULL
                                       ); 
                IF UPDATE(FirstName)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BankId
                                   ,'FirstName'
                                   ,CONVERT(VARCHAR(8000),Old.FirstName,121)
                                   ,CONVERT(VARCHAR(8000),New.FirstName,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BankId = New.BankId
                            WHERE   Old.FirstName <> New.FirstName
                                    OR (
                                         Old.FirstName IS NULL
                                         AND New.FirstName IS NOT NULL
                                       )
                                    OR (
                                         New.FirstName IS NULL
                                         AND Old.FirstName IS NOT NULL
                                       ); 
                IF UPDATE(LastName)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BankId
                                   ,'LastName'
                                   ,CONVERT(VARCHAR(8000),Old.LastName,121)
                                   ,CONVERT(VARCHAR(8000),New.LastName,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BankId = New.BankId
                            WHERE   Old.LastName <> New.LastName
                                    OR (
                                         Old.LastName IS NULL
                                         AND New.LastName IS NOT NULL
                                       )
                                    OR (
                                         New.LastName IS NULL
                                         AND Old.LastName IS NOT NULL
                                       ); 
                IF UPDATE(Title)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BankId
                                   ,'Title'
                                   ,CONVERT(VARCHAR(8000),Old.Title,121)
                                   ,CONVERT(VARCHAR(8000),New.Title,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BankId = New.BankId
                            WHERE   Old.Title <> New.Title
                                    OR (
                                         Old.Title IS NULL
                                         AND New.Title IS NOT NULL
                                       )
                                    OR (
                                         New.Title IS NULL
                                         AND Old.Title IS NOT NULL
                                       ); 
                IF UPDATE(ForeignPhone)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BankId
                                   ,'ForeignPhone'
                                   ,CONVERT(VARCHAR(8000),Old.ForeignPhone,121)
                                   ,CONVERT(VARCHAR(8000),New.ForeignPhone,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BankId = New.BankId
                            WHERE   Old.ForeignPhone <> New.ForeignPhone
                                    OR (
                                         Old.ForeignPhone IS NULL
                                         AND New.ForeignPhone IS NOT NULL
                                       )
                                    OR (
                                         New.ForeignPhone IS NULL
                                         AND Old.ForeignPhone IS NOT NULL
                                       ); 
                IF UPDATE(ForeignFax)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BankId
                                   ,'ForeignFax'
                                   ,CONVERT(VARCHAR(8000),Old.ForeignFax,121)
                                   ,CONVERT(VARCHAR(8000),New.ForeignFax,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BankId = New.BankId
                            WHERE   Old.ForeignFax <> New.ForeignFax
                                    OR (
                                         Old.ForeignFax IS NULL
                                         AND New.ForeignFax IS NOT NULL
                                       )
                                    OR (
                                         New.ForeignFax IS NULL
                                         AND Old.ForeignFax IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[saBankCodes] ADD CONSTRAINT [PK_saBankCodes_BankId] PRIMARY KEY CLUSTERED  ([BankId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_saBankCodes_Code_BankDescrip_CampGrpId] ON [dbo].[saBankCodes] ([Code], [BankDescrip], [CampGrpId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saBankCodes] ADD CONSTRAINT [FK_saBankCodes_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[saBankCodes] ADD CONSTRAINT [FK_saBankCodes_syStates_StateId_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[syStates] ([StateId])
GO
ALTER TABLE [dbo].[saBankCodes] ADD CONSTRAINT [FK_saBankCodes_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
