CREATE TABLE [dbo].[syAccreditingAgencies]
(
[AccreditingAgencyId] [int] NOT NULL,
[Code] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[StatusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syAccreditingAgencies] ADD CONSTRAINT [PK__syAccred__3D7480ACEC715508] PRIMARY KEY CLUSTERED  ([AccreditingAgencyId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syAccreditingAgencies] ADD CONSTRAINT [UQ__syAccred__A25C5AA7771F9A19] UNIQUE NONCLUSTERED  ([Code]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syAccreditingAgencies] ADD CONSTRAINT [FK_syAccreditingAgencies_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
