CREATE TABLE [dbo].[syKlassEntity]
(
[KlassEntityId] [int] NOT NULL,
[Code] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syKlassEntity] ADD CONSTRAINT [PK_syKlassEntity_KlassEntityId] PRIMARY KEY CLUSTERED  ([KlassEntityId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syKlassEntity] ADD CONSTRAINT [UIX_syKlassEntity_Code] UNIQUE NONCLUSTERED  ([Code]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', 'This is use when is a custom_field to set in the output object the type of object. For Now it is only student', 'SCHEMA', N'dbo', 'TABLE', N'syKlassEntity', NULL, NULL
GO
