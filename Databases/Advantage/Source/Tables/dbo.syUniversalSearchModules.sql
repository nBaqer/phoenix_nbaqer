CREATE TABLE [dbo].[syUniversalSearchModules]
(
[UniversalSearchId] [tinyint] NOT NULL,
[Code] [varchar] (70) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (70) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModifiedDate] [datetime] NULL CONSTRAINT [DF_syUniversalSearchModules_ModifiedDate] DEFAULT (getdate()),
[ModifiedUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResourceId] [smallint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syUniversalSearchModules] ADD CONSTRAINT [PK_syUniversalSearchModules_UniversalSearchId] PRIMARY KEY CLUSTERED  ([UniversalSearchId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syUniversalSearchModules] ADD CONSTRAINT [UIX_syUniversalSearchModules_Code] UNIQUE NONCLUSTERED  ([Code]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syUniversalSearchModules] ADD CONSTRAINT [FK_syUniversalSearchModules_syResources_ResourceId_ResourceID] FOREIGN KEY ([ResourceId]) REFERENCES [dbo].[syResources] ([ResourceID])
GO
ALTER TABLE [dbo].[syUniversalSearchModules] ADD CONSTRAINT [FK_syUniversalSearchModules_syUniversalSearchModules_UniversalSearchId_UniversalSearchId] FOREIGN KEY ([UniversalSearchId]) REFERENCES [dbo].[syUniversalSearchModules] ([UniversalSearchId])
GO
