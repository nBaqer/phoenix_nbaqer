CREATE TABLE [dbo].[syHRDepartments]
(
[HRDepartmentId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syHRDepartments_HRDepartmentId] DEFAULT (newid()),
[HRDepartmentCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[HRDepartmentDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syHRDepartments] ADD CONSTRAINT [PK_syHRDepartments_HRDepartmentId] PRIMARY KEY CLUSTERED  ([HRDepartmentId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_syHRDepartments_HRDepartmentCode_HRDepartmentDescrip_CampGrpId] ON [dbo].[syHRDepartments] ([HRDepartmentCode], [HRDepartmentDescrip], [CampGrpId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syHRDepartments] ADD CONSTRAINT [FK_syHRDepartments_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[syHRDepartments] ADD CONSTRAINT [FK_syHRDepartments_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
