CREATE TABLE [dbo].[syStateBoardProgramCourseMappings]
(
[StateBoardProgramCourseMappingId] [int] NOT NULL IDENTITY(1, 1),
[SchoolStateBoardReportId] [uniqueidentifier] NOT NULL,
[ProgramId] [uniqueidentifier] NOT NULL,
[StateBoardCourseId] [int] NOT NULL,
[CreatedById] [uniqueidentifier] NOT NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syStateBoardProgramCourseMappings] ADD CONSTRAINT [PK_syStateBoardProgramCourseMappings_StateBoardProgramCourseMappingId] PRIMARY KEY CLUSTERED  ([StateBoardProgramCourseMappingId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syStateBoardProgramCourseMappings] ADD CONSTRAINT [FK_syStateBoardProgramCourseMappings_arProgram_ProgramId_ProgId] FOREIGN KEY ([ProgramId]) REFERENCES [dbo].[arPrograms] ([ProgId])
GO
ALTER TABLE [dbo].[syStateBoardProgramCourseMappings] ADD CONSTRAINT [FK_syStateBoardProgramCourseMappings_sySchoolStateBoardReports_SchoolStateBoardReportId_SchoolStateBoardReportId] FOREIGN KEY ([SchoolStateBoardReportId]) REFERENCES [dbo].[sySchoolStateBoardReports] ([SchoolStateBoardReportId])
GO
ALTER TABLE [dbo].[syStateBoardProgramCourseMappings] ADD CONSTRAINT [FK_syStateBoardProgramCourseMappings_syStateBoardCourses_StateBoardCourseId_StateBoardCourseId] FOREIGN KEY ([StateBoardCourseId]) REFERENCES [dbo].[syStateBoardCourses] ([StateBoardCourseId])
GO
ALTER TABLE [dbo].[syStateBoardProgramCourseMappings] ADD CONSTRAINT [FK_syStateBoardProgramCourseMappings_syUsers_CreatedById_UserId] FOREIGN KEY ([CreatedById]) REFERENCES [dbo].[syUsers] ([UserId])
GO
