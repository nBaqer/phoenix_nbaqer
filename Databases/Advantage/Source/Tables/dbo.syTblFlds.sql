CREATE TABLE [dbo].[syTblFlds]
(
[TblFldsId] [int] NOT NULL,
[TblId] [int] NULL,
[FldId] [int] NOT NULL,
[CategoryId] [int] NULL,
[FKColDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syTblFlds] ADD CONSTRAINT [PK_syTblFlds_TblFldsId] PRIMARY KEY CLUSTERED  ([TblFldsId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syTblFlds] ADD CONSTRAINT [FK_syTblFlds_syFields_FldId_FldId] FOREIGN KEY ([FldId]) REFERENCES [dbo].[syFields] ([FldId])
GO
ALTER TABLE [dbo].[syTblFlds] ADD CONSTRAINT [FK_syTblFlds_syTables_TblId_TblId] FOREIGN KEY ([TblId]) REFERENCES [dbo].[syTables] ([TblId])
GO
GRANT SELECT ON  [dbo].[syTblFlds] TO [AdvantageRole]
GO
