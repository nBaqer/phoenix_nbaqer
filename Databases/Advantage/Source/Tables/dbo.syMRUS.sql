CREATE TABLE [dbo].[syMRUS]
(
[MRUId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syMRUS_MRUId] DEFAULT (newid()),
[Counter] [int] NOT NULL IDENTITY(1, 1),
[ChildId] [uniqueidentifier] NOT NULL,
[MRUTypeId] [tinyint] NOT NULL,
[UserId] [uniqueidentifier] NOT NULL,
[CampusId] [uniqueidentifier] NULL,
[SortOrder] [int] NULL,
[IsSticky] [bit] NULL CONSTRAINT [DF_syMRUS_IsSticky] DEFAULT ((0)),
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--==========================================================================================
-- TRIGGER syMRUS_AfterInsert
-- AFTER INSERT  
--==========================================================================================
CREATE TRIGGER [dbo].[syMRUS_AfterInsert] ON [dbo].[syMRUS]
    AFTER INSERT
AS
    BEGIN
        SET NOCOUNT ON; 
        DECLARE @MaxMRUsForUser AS INTEGER;
        DECLARE @UserId AS UNIQUEIDENTIFIER; 
        DECLARE @CampusId AS UNIQUEIDENTIFIER;
        DECLARE @MRUTypeId AS TINYINT;
        DECLARE @MRUList AS TABLE
            (
             MRUId UNIQUEIDENTIFIER
            ,MRUTypeId TINYINT
            ,UserId UNIQUEIDENTIFIER
            ,CampusId UNIQUEIDENTIFIER
            ,ModDate DATETIME
            );
        SET @MaxMRUsForUser = 20;
        SELECT  @UserId = New.UserId
               ,@CampusId = New.CampusId
               ,@MRUTypeId = New.MRUTypeId
        FROM    Inserted AS New;

        INSERT  INTO @MRUList
                (
                 MRUId
                ,MRUTypeId
                ,UserId
                ,CampusId
                ,ModDate
				 )
                SELECT TOP ( @MaxMRUsForUser )
                        SM.MRUId
                       ,SM.MRUTypeId
                       ,SM.UserId
                       ,SM.CampusId
                       ,SM.ModDate
                FROM    dbo.syMRUS AS SM
                INNER JOIN dbo.syMRUTypes AS SMT ON SMT.MRUTypeId = SM.MRUTypeId
                INNER JOIN dbo.syCampuses AS C ON C.CampusId = SM.CampusId
                WHERE   SM.MRUTypeId = @MRUTypeId
                        AND UserId = @UserId
                ORDER BY SM.ModDate DESC;

        DELETE  FROM syMRUS
        WHERE   UserId = @UserId
                AND MRUTypeId = @MRUTypeId
                AND MRUId NOT IN ( SELECT DISTINCT
                                            M.MRUId
                                   FROM     @MRUList AS M );
        SET NOCOUNT OFF; 
    END;
--==========================================================================================
-- END  --  TRIGGER syMRUS_AfterInsert
-- AFTER INSERT  
--==========================================================================================

GO
ALTER TABLE [dbo].[syMRUS] ADD CONSTRAINT [PK_syMRUS_MRUId] PRIMARY KEY CLUSTERED  ([MRUId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syMRUS] ADD CONSTRAINT [FK_syMRUS_syCampuses_CampusId_CampusId] FOREIGN KEY ([CampusId]) REFERENCES [dbo].[syCampuses] ([CampusId])
GO
ALTER TABLE [dbo].[syMRUS] ADD CONSTRAINT [FK_syMRUS_syUsers_UserId_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[syUsers] ([UserId]) ON DELETE CASCADE
GO
