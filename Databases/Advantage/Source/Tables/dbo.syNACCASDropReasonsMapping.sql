CREATE TABLE [dbo].[syNACCASDropReasonsMapping]
(
[NACCASDropReasonId] [uniqueidentifier] NULL,
[ADVDropReasonId] [uniqueidentifier] NULL,
[NaccasSettingId] [int] NOT NULL,
[DropReasonMappingId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_syNACCASDropReasonsMapping_DropReasonMappingId] DEFAULT (newsequentialid())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syNACCASDropReasonsMapping] ADD CONSTRAINT [PK_syNACCASDropReasonsMapping_DropReasonMappingId] PRIMARY KEY CLUSTERED  ([DropReasonMappingId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syNACCASDropReasonsMapping] ADD CONSTRAINT [FK_syNACCASDropReasonsMapping_syNaccasSettings_NaccasSettingId_NaccasSettingId] FOREIGN KEY ([NaccasSettingId]) REFERENCES [dbo].[syNaccasSettings] ([NaccasSettingId])
GO
ALTER TABLE [dbo].[syNACCASDropReasonsMapping] ADD CONSTRAINT [FK__syNACCASD__ADVDr__1254C35D] FOREIGN KEY ([ADVDropReasonId]) REFERENCES [dbo].[arDropReasons] ([DropReasonId])
GO
ALTER TABLE [dbo].[syNACCASDropReasonsMapping] ADD CONSTRAINT [FK__syNACCASD__NACCA__1348E796] FOREIGN KEY ([NACCASDropReasonId]) REFERENCES [dbo].[syNACCASDropReasons] ([NACCASDropReasonId])
GO
