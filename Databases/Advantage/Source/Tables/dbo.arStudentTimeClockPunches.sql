CREATE TABLE [dbo].[arStudentTimeClockPunches]
(
[BadgeId] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PunchTime] [smalldatetime] NOT NULL,
[PunchType] [smallint] NOT NULL,
[ClockId] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [smallint] NULL,
[StuEnrollId] [uniqueidentifier] NULL,
[FromSystem] [bit] NULL,
[ID] [int] NOT NULL IDENTITY(1, 1),
[ClsSectMeetingID] [uniqueidentifier] NULL,
[SpecialCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		FAME Inc.
-- Create date: 5/17/2019
-- Description:	When time clock punches updated, trigger arStudentClockAttendance trigger for rounding. Does
-- not change any data, just triggers rounding for related arStudentClockAttendanceRecords
-- =============================================
CREATE   TRIGGER [dbo].[tr_TimeClockHours]
ON [dbo].[arStudentTimeClockPunches]
AFTER INSERT, UPDATE	
AS
    BEGIN
		UPDATE A
		SET A.ModDate = A.ModDate
		FROM   inserted
		JOIN   dbo.arStudentClockAttendance A ON A.StuEnrollId = Inserted.StuEnrollId
												 AND CAST(A.RecordDate AS DATE) = CAST(Inserted.PunchTime AS DATE);
    END;
GO
ALTER TABLE [dbo].[arStudentTimeClockPunches] ADD CONSTRAINT [PK_arStudentTimeClockPunches_BadgeId_PunchTime_PunchType] PRIMARY KEY CLUSTERED  ([BadgeId], [PunchTime], [PunchType]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arStudentTimeClockPunches] ADD CONSTRAINT [FK_arStudentTimeClockPunches_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY ([StuEnrollId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
