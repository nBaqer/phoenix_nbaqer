CREATE TABLE [dbo].[tblAwardYearMapping]
(
[MappingId] [int] NOT NULL IDENTITY(1, 1),
[FameESPAwardYear] [int] NULL,
[AdvantageAwardYear] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblAwardYearMapping] ADD CONSTRAINT [PK_tblAwardYearMapping_MappingId] PRIMARY KEY CLUSTERED  ([MappingId]) ON [PRIMARY]
GO
