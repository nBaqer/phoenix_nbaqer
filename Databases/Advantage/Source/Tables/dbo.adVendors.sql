CREATE TABLE [dbo].[adVendors]
(
[VendorId] [int] NOT NULL IDENTITY(1, 1),
[VendorName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[VendorCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateOperationBegin] [datetime] NOT NULL,
[DateOperationEnd] [datetime] NULL,
[IsActive] [bit] NOT NULL,
[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_adVendors_IsDeleted] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adVendors] ADD CONSTRAINT [PK_adVendors_VendorId] PRIMARY KEY CLUSTERED  ([VendorId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adVendors] ADD CONSTRAINT [UIX_adVendors_VendorCode] UNIQUE NONCLUSTERED  ([VendorCode]) ON [PRIMARY]
GO
