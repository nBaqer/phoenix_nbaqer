CREATE TABLE [dbo].[AdLeadEmail]
(
[LeadEMailId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_AdLeadEmail_LeadEMailId] DEFAULT (newid()),
[LeadId] [uniqueidentifier] NOT NULL,
[EMail] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EMailTypeId] [uniqueidentifier] NOT NULL,
[IsPreferred] [bit] NOT NULL CONSTRAINT [DF_AdLeadEmail_IsPreferred] DEFAULT ((0)),
[IsPortalUserName] [bit] NOT NULL CONSTRAINT [DF_AdLeadEmail_IsPortalUserName] DEFAULT ((0)),
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_adLeadEmail_StatusId] DEFAULT ([dbo].[UDF_GetSyStatusesValue]('A')),
[IsShowOnLeadPage] [bit] NOT NULL CONSTRAINT [DF_adLeadEmail_IsShowOnLeadPage] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
--========================================================================================== 
-- TRIGGER AdLeadEmail_Audit_Delete 
-- DELETE  add Audit History when delete records in AdLeadEmail
--========================================================================================== 
CREATE TRIGGER [dbo].[AdLeadEmail_Audit_Delete] ON [dbo].[AdLeadEmail]
    FOR DELETE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Deleted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Deleted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Deleted
                        );
 
        IF @EventRows > 0
            BEGIN
                EXEC fmAuditHistAdd @AuditHistId,'AdLeadEmail','D',@EventRows,@EventDate,@UserName;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEMailId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),Old.LeadId)
                        FROM    Deleted AS Old;
                -- EMail 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEMailId
                               ,'EMail'
                               ,CONVERT(VARCHAR(MAX),Old.EMail)
                        FROM    Deleted AS Old;
                -- EMailTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEMailId
                               ,'EMailTypeId'
                               ,CONVERT(VARCHAR(MAX),Old.EMailTypeId)
                        FROM    Deleted AS Old;
                -- IsPreferred 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEMailId
                               ,'IsPreferred'
                               ,CONVERT(VARCHAR(MAX),Old.IsPreferred)
                        FROM    Deleted AS Old;
                -- IsPortalUserName 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEMailId
                               ,'IsPortalUserName'
                               ,CONVERT(VARCHAR(MAX),Old.IsPortalUserName)
                        FROM    Deleted AS Old;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEMailId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),Old.ModUser)
                        FROM    Deleted AS Old;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEMailId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                        FROM    Deleted AS Old;
                -- StatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEMailId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(MAX),Old.StatusId)
                        FROM    Deleted AS Old;
                -- IsShowOnLeadPage 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEMailId
                               ,'IsShowOnLeadPage'
                               ,CONVERT(VARCHAR(MAX),Old.IsShowOnLeadPage)
                        FROM    Deleted AS Old;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER AdLeadEmail_Audit_Delete 
--========================================================================================== 
 

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
--========================================================================================== 
-- TRIGGER AdLeadEmail_Audit_Insert 
-- INSERT  add Audit History when insert records in AdLeadEmail
--========================================================================================== 
CREATE TRIGGER [dbo].[AdLeadEmail_Audit_Insert] ON [dbo].[AdLeadEmail]
    FOR INSERT
AS
    BEGIN 
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'AdLeadEmail','I',@EventRows,@EventDate,@UserName;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEMailId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),New.LeadId)
                        FROM    Inserted AS New;
                -- EMail 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEMailId
                               ,'EMail'
                               ,CONVERT(VARCHAR(MAX),New.EMail)
                        FROM    Inserted AS New;
                -- EMailTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEMailId
                               ,'EMailTypeId'
                               ,CONVERT(VARCHAR(MAX),New.EMailTypeId)
                        FROM    Inserted AS New;
                -- IsPreferred 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEMailId
                               ,'IsPreferred'
                               ,CONVERT(VARCHAR(MAX),New.IsPreferred)
                        FROM    Inserted AS New;
                -- IsPortalUserName 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEMailId
                               ,'IsPortalUserName'
                               ,CONVERT(VARCHAR(MAX),New.IsPortalUserName)
                        FROM    Inserted AS New;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEMailId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),New.ModUser)
                        FROM    Inserted AS New;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEMailId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                        FROM    Inserted AS New;
                -- StatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEMailId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(MAX),New.StatusId)
                        FROM    Inserted AS New;
                -- IsShowOnLeadPage 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEMailId
                               ,'IsShowOnLeadPage'
                               ,CONVERT(VARCHAR(MAX),New.IsShowOnLeadPage)
                        FROM    Inserted AS New;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER AdLeadEmail_Audit_Insert 
--========================================================================================== 
 

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--========================================================================================== 
-- TRIGGER AdLeadEmail_Audit_Update 
-- UPDATE  add Audit History when update fields in AdLeadEmail
--========================================================================================== 
CREATE TRIGGER [dbo].[AdLeadEmail_Audit_Update] ON [dbo].[AdLeadEmail]
    FOR UPDATE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'AdLeadEmail','U',@EventRows,@EventDate,@UserName;
                -- LeadId 
                IF UPDATE(LeadId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEMailId
                                       ,'LeadId'
                                       ,CONVERT(VARCHAR(MAX),Old.LeadId)
                                       ,CONVERT(VARCHAR(MAX),New.LeadId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEMailId = New.LeadEMailId
                                WHERE   Old.LeadId <> New.LeadId
                                        OR (
                                             Old.LeadId IS NULL
                                             AND New.LeadId IS NOT NULL
                                           )
                                        OR (
                                             New.LeadId IS NULL
                                             AND Old.LeadId IS NOT NULL
                                           );
                    END;
                -- EMail 
                IF UPDATE(EMail)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEMailId
                                       ,'EMail'
                                       ,CONVERT(VARCHAR(MAX),Old.EMail)
                                       ,CONVERT(VARCHAR(MAX),New.EMail)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEMailId = New.LeadEMailId
                                WHERE   Old.EMail <> New.EMail
                                        OR (
                                             Old.EMail IS NULL
                                             AND New.EMail IS NOT NULL
                                           )
                                        OR (
                                             New.EMail IS NULL
                                             AND Old.EMail IS NOT NULL
                                           );
                    END;
                -- EMailTypeId 
                IF UPDATE(EMailTypeId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEMailId
                                       ,'EMailTypeId'
                                       ,CONVERT(VARCHAR(MAX),Old.EMailTypeId)
                                       ,CONVERT(VARCHAR(MAX),New.EMailTypeId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEMailId = New.LeadEMailId
                                WHERE   Old.EMailTypeId <> New.EMailTypeId
                                        OR (
                                             Old.EMailTypeId IS NULL
                                             AND New.EMailTypeId IS NOT NULL
                                           )
                                        OR (
                                             New.EMailTypeId IS NULL
                                             AND Old.EMailTypeId IS NOT NULL
                                           );
                    END;
                -- IsPreferred 
                IF UPDATE(IsPreferred)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEMailId
                                       ,'IsPreferred'
                                       ,CONVERT(VARCHAR(MAX),Old.IsPreferred)
                                       ,CONVERT(VARCHAR(MAX),New.IsPreferred)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEMailId = New.LeadEMailId
                                WHERE   Old.IsPreferred <> New.IsPreferred
                                        OR (
                                             Old.IsPreferred IS NULL
                                             AND New.IsPreferred IS NOT NULL
                                           )
                                        OR (
                                             New.IsPreferred IS NULL
                                             AND Old.IsPreferred IS NOT NULL
                                           );
                    END;
                -- IsPortalUserName 
                IF UPDATE(IsPortalUserName)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEMailId
                                       ,'IsPortalUserName'
                                       ,CONVERT(VARCHAR(MAX),Old.IsPortalUserName)
                                       ,CONVERT(VARCHAR(MAX),New.IsPortalUserName)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEMailId = New.LeadEMailId
                                WHERE   Old.IsPortalUserName <> New.IsPortalUserName
                                        OR (
                                             Old.IsPortalUserName IS NULL
                                             AND New.IsPortalUserName IS NOT NULL
                                           )
                                        OR (
                                             New.IsPortalUserName IS NULL
                                             AND Old.IsPortalUserName IS NOT NULL
                                           );
                    END;
                -- ModUser 
                IF UPDATE(ModUser)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEMailId
                                       ,'ModUser'
                                       ,CONVERT(VARCHAR(MAX),Old.ModUser)
                                       ,CONVERT(VARCHAR(MAX),New.ModUser)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEMailId = New.LeadEMailId
                                WHERE   Old.ModUser <> New.ModUser
                                        OR (
                                             Old.ModUser IS NULL
                                             AND New.ModUser IS NOT NULL
                                           )
                                        OR (
                                             New.ModUser IS NULL
                                             AND Old.ModUser IS NOT NULL
                                           );
                    END;
                -- ModDate 
                IF UPDATE(ModDate)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEMailId
                                       ,'ModDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEMailId = New.LeadEMailId
                                WHERE   Old.ModDate <> New.ModDate
                                        OR (
                                             Old.ModDate IS NULL
                                             AND New.ModDate IS NOT NULL
                                           )
                                        OR (
                                             New.ModDate IS NULL
                                             AND Old.ModDate IS NOT NULL
                                           );
                    END;
                -- StatusId 
                IF UPDATE(StatusId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEMailId
                                       ,'StatusId'
                                       ,CONVERT(VARCHAR(MAX),Old.StatusId)
                                       ,CONVERT(VARCHAR(MAX),New.StatusId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEMailId = New.LeadEMailId
                                WHERE   Old.StatusId <> New.StatusId
                                        OR (
                                             Old.StatusId IS NULL
                                             AND New.StatusId IS NOT NULL
                                           )
                                        OR (
                                             New.StatusId IS NULL
                                             AND Old.StatusId IS NOT NULL
                                           );
                    END;
                -- IsShowOnLeadPage 
                IF UPDATE(IsShowOnLeadPage)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEMailId
                                       ,'IsShowOnLeadPage'
                                       ,CONVERT(VARCHAR(MAX),Old.IsShowOnLeadPage)
                                       ,CONVERT(VARCHAR(MAX),New.IsShowOnLeadPage)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEMailId = New.LeadEMailId
                                WHERE   Old.IsShowOnLeadPage <> New.IsShowOnLeadPage
                                        OR (
                                             Old.IsShowOnLeadPage IS NULL
                                             AND New.IsShowOnLeadPage IS NOT NULL
                                           )
                                        OR (
                                             New.IsShowOnLeadPage IS NULL
                                             AND Old.IsShowOnLeadPage IS NOT NULL
                                           );
                    END;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER AdLeadEmail_Audit_Update 
--========================================================================================== 
 

GO
ALTER TABLE [dbo].[AdLeadEmail] ADD CONSTRAINT [PK_AdLeadEmail_LeadEMailId] PRIMARY KEY CLUSTERED  ([LeadEMailId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AdLeadEmail] ADD CONSTRAINT [FK_AdLeadEmail_adLeads_LeadId_LeadId] FOREIGN KEY ([LeadId]) REFERENCES [dbo].[adLeads] ([LeadId])
GO
ALTER TABLE [dbo].[AdLeadEmail] ADD CONSTRAINT [FK_AdLeadEmail_syEmailType_EmailTypeId_EmailTypeId] FOREIGN KEY ([EMailTypeId]) REFERENCES [dbo].[syEmailType] ([EMailTypeId])
GO
ALTER TABLE [dbo].[AdLeadEmail] ADD CONSTRAINT [FK_AdLeadEmail_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
