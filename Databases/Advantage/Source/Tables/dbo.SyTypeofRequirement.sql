CREATE TABLE [dbo].[SyTypeofRequirement]
(
[RequirementID] [uniqueidentifier] NULL,
[TypeOfReq] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CampGrpID] [uniqueidentifier] NULL,
[TypeOfRequirementId] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SyTypeofRequirement] ADD CONSTRAINT [PK_SyTypeofRequirement] PRIMARY KEY CLUSTERED  ([TypeOfRequirementId]) ON [PRIMARY]
GO
