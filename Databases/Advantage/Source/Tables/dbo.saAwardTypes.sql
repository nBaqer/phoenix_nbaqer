CREATE TABLE [dbo].[saAwardTypes]
(
[AwardTypeId] [int] NOT NULL,
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saAwardTypes] ADD CONSTRAINT [PK_saAwardTypes_AwardTypeId] PRIMARY KEY CLUSTERED  ([AwardTypeId]) ON [PRIMARY]
GO
