CREATE TABLE [dbo].[AfaPaymentPeriodStaging]
(
[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF__AfaPaymentPe__Id__40DAA81C] DEFAULT (newid()),
[AfaStudentId] [bigint] NOT NULL,
[StudentEnrollmentId] [uniqueidentifier] NOT NULL,
[AcademicYearSequenceNum] [int] NOT NULL,
[Description] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StartDate] [datetime] NOT NULL,
[EndDate] [datetime] NOT NULL,
[WeeksInstructionalTime] [decimal] (18, 2) NOT NULL,
[CreditOrHours] [decimal] (18, 2) NOT NULL,
[Status] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreditOrHoursEarned] [decimal] (18, 2) NULL,
[CreditOrHoursEarnedEffectiveDate] [datetime] NULL,
[TitleIVSAPResult] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateCreated] [datetime] NOT NULL,
[UserCreated] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DeleteRecord] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AfaPaymentPeriodStaging] ADD CONSTRAINT [PK_AfaPaymentPeriodStaging_Id] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AfaPaymentPeriodStaging] ADD CONSTRAINT [FK_AfaPaymentPeriodStaging_arStuEnrollments_StudentEnrollmentId_StuEnrollId] FOREIGN KEY ([StudentEnrollmentId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
