CREATE TABLE [dbo].[syRelationshipStatus]
(
[RelationshipStatusId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syRelationshipStatus_RelationshipStatusId] DEFAULT (newid()),
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Code] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusId] [uniqueidentifier] NULL,
[CampGrpId] [uniqueidentifier] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syRelationshipStatus] ADD CONSTRAINT [PK_syRelationshipStatus_RelationshipStatusId] PRIMARY KEY CLUSTERED  ([RelationshipStatusId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syRelationshipStatus] ADD CONSTRAINT [FK_syRelationshipStatus_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[syRelationshipStatus] ADD CONSTRAINT [FK_syRelationshipStatus_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
