CREATE TABLE [dbo].[syFamilyIncome]
(
[FamilyIncomeID] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syFamilyIncome_FamilyIncomeID] DEFAULT (newid()),
[FamilyIncomeDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusID] [uniqueidentifier] NULL,
[CampGrpId] [uniqueidentifier] NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FamilyIncomeCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ViewOrder] [int] NOT NULL,
[IPEDSValue] [int] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[syFamilyIncome_Audit_Delete] ON [dbo].[syFamilyIncome]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syFamilyIncome','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.FamilyIncomeID
                               ,'ViewOrder'
                               ,CONVERT(VARCHAR(8000),Old.ViewOrder,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.FamilyIncomeID
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusID,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.FamilyIncomeID
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.FamilyIncomeID
                               ,'FamilyIncomeDescrip'
                               ,CONVERT(VARCHAR(8000),Old.FamilyIncomeDescrip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.FamilyIncomeID
                               ,'FamilyIncomeCode'
                               ,CONVERT(VARCHAR(8000),Old.FamilyIncomeCode,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[syFamilyIncome_Audit_Insert] ON [dbo].[syFamilyIncome]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syFamilyIncome','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.FamilyIncomeID
                               ,'ViewOrder'
                               ,CONVERT(VARCHAR(8000),New.ViewOrder,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.FamilyIncomeID
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusID,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.FamilyIncomeID
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.FamilyIncomeID
                               ,'FamilyIncomeDescrip'
                               ,CONVERT(VARCHAR(8000),New.FamilyIncomeDescrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.FamilyIncomeID
                               ,'FamilyIncomeCode'
                               ,CONVERT(VARCHAR(8000),New.FamilyIncomeCode,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[syFamilyIncome_Audit_Update] ON [dbo].[syFamilyIncome]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syFamilyIncome','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(ViewOrder)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.FamilyIncomeID
                                   ,'ViewOrder'
                                   ,CONVERT(VARCHAR(8000),Old.ViewOrder,121)
                                   ,CONVERT(VARCHAR(8000),New.ViewOrder,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.FamilyIncomeID = New.FamilyIncomeID
                            WHERE   Old.ViewOrder <> New.ViewOrder
                                    OR (
                                         Old.ViewOrder IS NULL
                                         AND New.ViewOrder IS NOT NULL
                                       )
                                    OR (
                                         New.ViewOrder IS NULL
                                         AND Old.ViewOrder IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.FamilyIncomeID
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusID,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusID,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.FamilyIncomeID = New.FamilyIncomeID
                            WHERE   Old.StatusID <> New.StatusID
                                    OR (
                                         Old.StatusID IS NULL
                                         AND New.StatusID IS NOT NULL
                                       )
                                    OR (
                                         New.StatusID IS NULL
                                         AND Old.StatusID IS NOT NULL
                                       ); 
                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.FamilyIncomeID
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.FamilyIncomeID = New.FamilyIncomeID
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 
                IF UPDATE(FamilyIncomeDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.FamilyIncomeID
                                   ,'FamilyIncomeDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.FamilyIncomeDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.FamilyIncomeDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.FamilyIncomeID = New.FamilyIncomeID
                            WHERE   Old.FamilyIncomeDescrip <> New.FamilyIncomeDescrip
                                    OR (
                                         Old.FamilyIncomeDescrip IS NULL
                                         AND New.FamilyIncomeDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.FamilyIncomeDescrip IS NULL
                                         AND Old.FamilyIncomeDescrip IS NOT NULL
                                       ); 
                IF UPDATE(FamilyIncomeCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.FamilyIncomeID
                                   ,'FamilyIncomeCode'
                                   ,CONVERT(VARCHAR(8000),Old.FamilyIncomeCode,121)
                                   ,CONVERT(VARCHAR(8000),New.FamilyIncomeCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.FamilyIncomeID = New.FamilyIncomeID
                            WHERE   Old.FamilyIncomeCode <> New.FamilyIncomeCode
                                    OR (
                                         Old.FamilyIncomeCode IS NULL
                                         AND New.FamilyIncomeCode IS NOT NULL
                                       )
                                    OR (
                                         New.FamilyIncomeCode IS NULL
                                         AND Old.FamilyIncomeCode IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO
ALTER TABLE [dbo].[syFamilyIncome] ADD CONSTRAINT [PK_syFamilyIncome_FamilyIncomeID] PRIMARY KEY CLUSTERED  ([FamilyIncomeID]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_syFamilyIncome_FamilyIncomeCode_FamilyIncomeDescrip_CampGrpId] ON [dbo].[syFamilyIncome] ([FamilyIncomeCode], [FamilyIncomeDescrip], [CampGrpId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syFamilyIncome] ADD CONSTRAINT [FK_syFamilyIncome_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[syFamilyIncome] ADD CONSTRAINT [FK_syFamilyIncome_syStatuses_StatusID_StatusId] FOREIGN KEY ([StatusID]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
