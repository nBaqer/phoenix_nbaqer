CREATE TABLE [dbo].[faLenders]
(
[LenderId] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[LenderDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ForeignAddress] [bit] NOT NULL,
[Address1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateId] [uniqueidentifier] NULL,
[OtherState] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Zip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CountryId] [uniqueidentifier] NULL,
[Email] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrimaryContact] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OtherContact] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsLender] [bit] NOT NULL,
[IsServicer] [bit] NOT NULL,
[IsGuarantor] [bit] NOT NULL,
[ForeignPayAddress] [bit] NOT NULL,
[PayAddress1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayAddress2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayCity] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayStateId] [uniqueidentifier] NULL,
[OtherPayState] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayZip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PayCountryId] [uniqueidentifier] NULL,
[CustService] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ForeignCustService] [bit] NOT NULL,
[Fax] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ForeignFax] [bit] NOT NULL,
[PreClaim] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ForeignPreClaim] [bit] NOT NULL,
[PostClaim] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ForeignPostClaim] [bit] NOT NULL,
[Comments] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CampGrpId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[faLenders_Audit_Delete] ON [dbo].[faLenders]
    FOR DELETE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'faLenders','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'PayStateId'
                               ,CONVERT(VARCHAR(8000),Old.PayStateId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'CountryId'
                               ,CONVERT(VARCHAR(8000),Old.CountryId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'PayCountryId'
                               ,CONVERT(VARCHAR(8000),Old.PayCountryId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'StateId'
                               ,CONVERT(VARCHAR(8000),Old.StateId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'Fax'
                               ,CONVERT(VARCHAR(8000),Old.Fax,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'OtherPayState'
                               ,CONVERT(VARCHAR(8000),Old.OtherPayState,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'PreClaim'
                               ,CONVERT(VARCHAR(8000),Old.PreClaim,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'Zip'
                               ,CONVERT(VARCHAR(8000),Old.Zip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'PayZip'
                               ,CONVERT(VARCHAR(8000),Old.PayZip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'OtherState'
                               ,CONVERT(VARCHAR(8000),Old.OtherState,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'PostClaim'
                               ,CONVERT(VARCHAR(8000),Old.PostClaim,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'LenderDescrip'
                               ,CONVERT(VARCHAR(8000),Old.LenderDescrip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'PayCity'
                               ,CONVERT(VARCHAR(8000),Old.PayCity,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'CustService'
                               ,CONVERT(VARCHAR(8000),Old.CustService,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'Comments'
                               ,CONVERT(VARCHAR(8000),Old.Comments,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'OtherContact'
                               ,CONVERT(VARCHAR(8000),Old.OtherContact,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'PayAddress1'
                               ,CONVERT(VARCHAR(8000),Old.PayAddress1,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'PayAddress2'
                               ,CONVERT(VARCHAR(8000),Old.PayAddress2,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'City'
                               ,CONVERT(VARCHAR(8000),Old.City,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'Email'
                               ,CONVERT(VARCHAR(8000),Old.Email,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'PrimaryContact'
                               ,CONVERT(VARCHAR(8000),Old.PrimaryContact,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'Code'
                               ,CONVERT(VARCHAR(8000),Old.Code,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'Address1'
                               ,CONVERT(VARCHAR(8000),Old.Address1,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'Address2'
                               ,CONVERT(VARCHAR(8000),Old.Address2,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'IsGuarantor'
                               ,CONVERT(VARCHAR(8000),Old.IsGuarantor,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'ForeignPreClaim'
                               ,CONVERT(VARCHAR(8000),Old.ForeignPreClaim,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'IsServicer'
                               ,CONVERT(VARCHAR(8000),Old.IsServicer,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'ForeignPostClaim'
                               ,CONVERT(VARCHAR(8000),Old.ForeignPostClaim,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'ForeignAddress'
                               ,CONVERT(VARCHAR(8000),Old.ForeignAddress,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'ForeignPayAddress'
                               ,CONVERT(VARCHAR(8000),Old.ForeignPayAddress,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'IsLender'
                               ,CONVERT(VARCHAR(8000),Old.IsLender,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'ForeignCustService'
                               ,CONVERT(VARCHAR(8000),Old.ForeignCustService,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LenderId
                               ,'ForeignFax'
                               ,CONVERT(VARCHAR(8000),Old.ForeignFax,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[faLenders_Audit_Insert] ON [dbo].[faLenders]
    FOR INSERT
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'faLenders','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'PayStateId'
                               ,CONVERT(VARCHAR(8000),New.PayStateId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'CountryId'
                               ,CONVERT(VARCHAR(8000),New.CountryId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'PayCountryId'
                               ,CONVERT(VARCHAR(8000),New.PayCountryId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'StateId'
                               ,CONVERT(VARCHAR(8000),New.StateId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'Fax'
                               ,CONVERT(VARCHAR(8000),New.Fax,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'OtherPayState'
                               ,CONVERT(VARCHAR(8000),New.OtherPayState,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'PreClaim'
                               ,CONVERT(VARCHAR(8000),New.PreClaim,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'Zip'
                               ,CONVERT(VARCHAR(8000),New.Zip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'PayZip'
                               ,CONVERT(VARCHAR(8000),New.PayZip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'OtherState'
                               ,CONVERT(VARCHAR(8000),New.OtherState,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'PostClaim'
                               ,CONVERT(VARCHAR(8000),New.PostClaim,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'LenderDescrip'
                               ,CONVERT(VARCHAR(8000),New.LenderDescrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'PayCity'
                               ,CONVERT(VARCHAR(8000),New.PayCity,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'CustService'
                               ,CONVERT(VARCHAR(8000),New.CustService,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'Comments'
                               ,CONVERT(VARCHAR(8000),New.Comments,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'OtherContact'
                               ,CONVERT(VARCHAR(8000),New.OtherContact,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'PayAddress1'
                               ,CONVERT(VARCHAR(8000),New.PayAddress1,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'PayAddress2'
                               ,CONVERT(VARCHAR(8000),New.PayAddress2,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'City'
                               ,CONVERT(VARCHAR(8000),New.City,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'Email'
                               ,CONVERT(VARCHAR(8000),New.Email,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'PrimaryContact'
                               ,CONVERT(VARCHAR(8000),New.PrimaryContact,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'Code'
                               ,CONVERT(VARCHAR(8000),New.Code,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'Address1'
                               ,CONVERT(VARCHAR(8000),New.Address1,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'Address2'
                               ,CONVERT(VARCHAR(8000),New.Address2,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'IsGuarantor'
                               ,CONVERT(VARCHAR(8000),New.IsGuarantor,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'ForeignPreClaim'
                               ,CONVERT(VARCHAR(8000),New.ForeignPreClaim,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'IsServicer'
                               ,CONVERT(VARCHAR(8000),New.IsServicer,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'ForeignPostClaim'
                               ,CONVERT(VARCHAR(8000),New.ForeignPostClaim,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'ForeignAddress'
                               ,CONVERT(VARCHAR(8000),New.ForeignAddress,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'ForeignPayAddress'
                               ,CONVERT(VARCHAR(8000),New.ForeignPayAddress,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'IsLender'
                               ,CONVERT(VARCHAR(8000),New.IsLender,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'ForeignCustService'
                               ,CONVERT(VARCHAR(8000),New.ForeignCustService,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LenderId
                               ,'ForeignFax'
                               ,CONVERT(VARCHAR(8000),New.ForeignFax,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
CREATE TRIGGER [dbo].[faLenders_Audit_Update] ON [dbo].[faLenders] 
    FOR UPDATE 
AS 
    SET NOCOUNT ON;  
 
    DECLARE @AuditHistId AS UNIQUEIDENTIFIER;  
    DECLARE @EventRows AS INT;  
    DECLARE @EventDate AS DATETIME;  
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();  
    SET @EventRows = ( 
                       SELECT   COUNT(*) 
                       FROM     Inserted 
                     );  
    SET @EventDate = ( 
                       SELECT TOP 1 
                                ModDate 
                       FROM     Inserted 
                     );  
    SET @UserName = ( 
                      SELECT TOP 1 
                                ModUser 
                      FROM      Inserted 
                    );  
    IF @EventRows > 0 
        BEGIN  
            EXEC fmAuditHistAdd @AuditHistId,'faLenders','U',@EventRows,@EventDate,@UserName;  
            BEGIN  
                IF UPDATE(PayStateId) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'PayStateId' 
                                   ,CONVERT(VARCHAR(8000),Old.PayStateId,121) 
                                   ,CONVERT(VARCHAR(8000),New.PayStateId,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.PayStateId <> New.PayStateId 
                                    OR ( 
                                         Old.PayStateId IS NULL 
                                         AND New.PayStateId IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.PayStateId IS NULL 
                                         AND Old.PayStateId IS NOT NULL 
                                       );  
                IF UPDATE(CountryId) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'CountryId' 
                                   ,CONVERT(VARCHAR(8000),Old.CountryId,121) 
                                   ,CONVERT(VARCHAR(8000),New.CountryId,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.CountryId <> New.CountryId 
                                    OR ( 
                                         Old.CountryId IS NULL 
                                         AND New.CountryId IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.CountryId IS NULL 
                                         AND Old.CountryId IS NOT NULL 
                                       );  
                IF UPDATE(PayCountryId) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'PayCountryId' 
                                   ,CONVERT(VARCHAR(8000),Old.PayCountryId,121) 
                                   ,CONVERT(VARCHAR(8000),New.PayCountryId,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.PayCountryId <> New.PayCountryId 
                                    OR ( 
                                         Old.PayCountryId IS NULL 
                                         AND New.PayCountryId IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.PayCountryId IS NULL 
                                         AND Old.PayCountryId IS NOT NULL 
                                       );  
                IF UPDATE(StatusId) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'StatusId' 
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121) 
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.StatusId <> New.StatusId 
                                    OR ( 
                                         Old.StatusId IS NULL 
                                         AND New.StatusId IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.StatusId IS NULL 
                                         AND Old.StatusId IS NOT NULL 
                                       );  
                IF UPDATE(StateId) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'StateId' 
                                   ,CONVERT(VARCHAR(8000),Old.StateId,121) 
                                   ,CONVERT(VARCHAR(8000),New.StateId,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.StateId <> New.StateId 
                                    OR ( 
                                         Old.StateId IS NULL 
                                         AND New.StateId IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.StateId IS NULL 
                                         AND Old.StateId IS NOT NULL 
                                       );  
                IF UPDATE(Fax) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'Fax' 
                                   ,CONVERT(VARCHAR(8000),Old.Fax,121) 
                                   ,CONVERT(VARCHAR(8000),New.Fax,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.Fax <> New.Fax 
                                    OR ( 
                                         Old.Fax IS NULL 
                                         AND New.Fax IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.Fax IS NULL 
                                         AND Old.Fax IS NOT NULL 
                                       );  
                IF UPDATE(OtherPayState) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'OtherPayState' 
                                   ,CONVERT(VARCHAR(8000),Old.OtherPayState,121) 
                                   ,CONVERT(VARCHAR(8000),New.OtherPayState,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.OtherPayState <> New.OtherPayState 
                                    OR ( 
                                         Old.OtherPayState IS NULL 
                                         AND New.OtherPayState IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.OtherPayState IS NULL 
                                         AND Old.OtherPayState IS NOT NULL 
                                       );  
                IF UPDATE(PreClaim) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'PreClaim' 
                                   ,CONVERT(VARCHAR(8000),Old.PreClaim,121) 
                                   ,CONVERT(VARCHAR(8000),New.PreClaim,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.PreClaim <> New.PreClaim 
                                    OR ( 
                                         Old.PreClaim IS NULL 
                                         AND New.PreClaim IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.PreClaim IS NULL 
                                         AND Old.PreClaim IS NOT NULL 
                                       );  
                IF UPDATE(Zip) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'Zip' 
                                   ,CONVERT(VARCHAR(8000),Old.Zip,121) 
                                   ,CONVERT(VARCHAR(8000),New.Zip,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.Zip <> New.Zip 
                                    OR ( 
                                         Old.Zip IS NULL 
                                         AND New.Zip IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.Zip IS NULL 
                                         AND Old.Zip IS NOT NULL 
                                       );  
                IF UPDATE(PayZip) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'PayZip' 
                                   ,CONVERT(VARCHAR(8000),Old.PayZip,121) 
                                   ,CONVERT(VARCHAR(8000),New.PayZip,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.PayZip <> New.PayZip 
                                    OR ( 
                                         Old.PayZip IS NULL 
                                         AND New.PayZip IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.PayZip IS NULL 
                                         AND Old.PayZip IS NOT NULL 
                                       );  
                IF UPDATE(OtherState) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'OtherState' 
                                   ,CONVERT(VARCHAR(8000),Old.OtherState,121) 
                                   ,CONVERT(VARCHAR(8000),New.OtherState,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.OtherState <> New.OtherState 
                                    OR ( 
                                         Old.OtherState IS NULL 
                                         AND New.OtherState IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.OtherState IS NULL 
                                         AND Old.OtherState IS NOT NULL 
                                       );  
                IF UPDATE(PostClaim) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'PostClaim' 
                                   ,CONVERT(VARCHAR(8000),Old.PostClaim,121) 
                                   ,CONVERT(VARCHAR(8000),New.PostClaim,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.PostClaim <> New.PostClaim 
                                    OR ( 
                                         Old.PostClaim IS NULL 
                                         AND New.PostClaim IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.PostClaim IS NULL 
                                         AND Old.PostClaim IS NOT NULL 
                                       );  
                IF UPDATE(LenderDescrip) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'LenderDescrip' 
                                   ,CONVERT(VARCHAR(8000),Old.LenderDescrip,121) 
                                   ,CONVERT(VARCHAR(8000),New.LenderDescrip,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.LenderDescrip <> New.LenderDescrip 
                                    OR ( 
                                         Old.LenderDescrip IS NULL 
                                         AND New.LenderDescrip IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.LenderDescrip IS NULL 
                                         AND Old.LenderDescrip IS NOT NULL 
                                       );  
                IF UPDATE(PayCity) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'PayCity' 
                                   ,CONVERT(VARCHAR(8000),Old.PayCity,121) 
                                   ,CONVERT(VARCHAR(8000),New.PayCity,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.PayCity <> New.PayCity 
                                    OR ( 
                                         Old.PayCity IS NULL 
                                         AND New.PayCity IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.PayCity IS NULL 
                                         AND Old.PayCity IS NOT NULL 
                                       );  
                IF UPDATE(CustService) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'CustService' 
                                   ,CONVERT(VARCHAR(8000),Old.CustService,121) 
                                   ,CONVERT(VARCHAR(8000),New.CustService,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.CustService <> New.CustService 
                                    OR ( 
                                         Old.CustService IS NULL 
                                         AND New.CustService IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.CustService IS NULL 
                                         AND Old.CustService IS NOT NULL 
                                       );  
                IF UPDATE(comments) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'Comments' 
                                   ,CONVERT(VARCHAR(8000),Old.Comments,121) 
                                   ,CONVERT(VARCHAR(8000),New.Comments,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.Comments <> New.Comments 
                                    OR ( 
                                         Old.Comments IS NULL 
                                         AND New.Comments IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.Comments IS NULL 
                                         AND Old.Comments IS NOT NULL 
                                       );  
                IF UPDATE(OtherContact) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'OtherContact' 
                                   ,CONVERT(VARCHAR(8000),Old.OtherContact,121) 
                                   ,CONVERT(VARCHAR(8000),New.OtherContact,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.OtherContact <> New.OtherContact 
                                    OR ( 
                                         Old.OtherContact IS NULL 
                                         AND New.OtherContact IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.OtherContact IS NULL 
                                         AND Old.OtherContact IS NOT NULL 
                                       );  
                IF UPDATE(PayAddress1) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'PayAddress1' 
                                   ,CONVERT(VARCHAR(8000),Old.PayAddress1,121) 
                                   ,CONVERT(VARCHAR(8000),New.PayAddress1,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.PayAddress1 <> New.PayAddress1 
                                    OR ( 
                                         Old.PayAddress1 IS NULL 
                                         AND New.PayAddress1 IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.PayAddress1 IS NULL 
                                         AND Old.PayAddress1 IS NOT NULL 
                                       );  
                IF UPDATE(PayAddress2) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'PayAddress2' 
                                   ,CONVERT(VARCHAR(8000),Old.PayAddress2,121) 
                                   ,CONVERT(VARCHAR(8000),New.PayAddress2,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.PayAddress2 <> New.PayAddress2 
                                    OR ( 
                                         Old.PayAddress2 IS NULL 
                                         AND New.PayAddress2 IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.PayAddress2 IS NULL 
                                         AND Old.PayAddress2 IS NOT NULL 
                                       );  
                IF UPDATE(City) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'City' 
                                   ,CONVERT(VARCHAR(8000),Old.City,121) 
                                   ,CONVERT(VARCHAR(8000),New.City,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.City <> New.City 
                                    OR ( 
                                         Old.City IS NULL 
                                         AND New.City IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.City IS NULL 
                                         AND Old.City IS NOT NULL 
                                       );  
                IF UPDATE(Email) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'Email' 
                                   ,CONVERT(VARCHAR(8000),Old.Email,121) 
                                   ,CONVERT(VARCHAR(8000),New.Email,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.Email <> New.Email 
                                    OR ( 
                                         Old.Email IS NULL 
                                         AND New.Email IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.Email IS NULL 
                                         AND Old.Email IS NOT NULL 
                                       );  
                IF UPDATE(PrimaryContact) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'PrimaryContact' 
                                   ,CONVERT(VARCHAR(8000),Old.PrimaryContact,121) 
                                   ,CONVERT(VARCHAR(8000),New.PrimaryContact,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.PrimaryContact <> New.PrimaryContact 
                                    OR ( 
                                         Old.PrimaryContact IS NULL 
                                         AND New.PrimaryContact IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.PrimaryContact IS NULL 
                                         AND Old.PrimaryContact IS NOT NULL 
                                       );  
                IF UPDATE(Code) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'Code' 
                                   ,CONVERT(VARCHAR(8000),Old.Code,121) 
                                   ,CONVERT(VARCHAR(8000),New.Code,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.Code <> New.Code 
                                    OR ( 
                                         Old.Code IS NULL 
                                         AND New.Code IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.Code IS NULL 
                                         AND Old.Code IS NOT NULL 
                                       );  
                IF UPDATE(Address1) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'Address1' 
                                   ,CONVERT(VARCHAR(8000),Old.Address1,121) 
                                   ,CONVERT(VARCHAR(8000),New.Address1,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.Address1 <> New.Address1 
                                    OR ( 
                                         Old.Address1 IS NULL 
                                         AND New.Address1 IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.Address1 IS NULL 
                                         AND Old.Address1 IS NOT NULL 
                                       );  
                IF UPDATE(Address2) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'Address2' 
                                   ,CONVERT(VARCHAR(8000),Old.Address2,121) 
                                   ,CONVERT(VARCHAR(8000),New.Address2,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.Address2 <> New.Address2 
                                    OR ( 
                                         Old.Address2 IS NULL 
                                         AND New.Address2 IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.Address2 IS NULL 
                                         AND Old.Address2 IS NOT NULL 
                                       );  
                IF UPDATE(IsGuarantor) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'IsGuarantor' 
                                   ,CONVERT(VARCHAR(8000),Old.IsGuarantor,121) 
                                   ,CONVERT(VARCHAR(8000),New.IsGuarantor,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.IsGuarantor <> New.IsGuarantor 
                                    OR ( 
                                         Old.IsGuarantor IS NULL 
                                         AND New.IsGuarantor IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.IsGuarantor IS NULL 
                                         AND Old.IsGuarantor IS NOT NULL 
                                       );  
                IF UPDATE(ForeignPreClaim) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'ForeignPreClaim' 
                                   ,CONVERT(VARCHAR(8000),Old.ForeignPreClaim,121) 
                                   ,CONVERT(VARCHAR(8000),New.ForeignPreClaim,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.ForeignPreClaim <> New.ForeignPreClaim 
                                    OR ( 
                                         Old.ForeignPreClaim IS NULL 
                                         AND New.ForeignPreClaim IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.ForeignPreClaim IS NULL 
                                         AND Old.ForeignPreClaim IS NOT NULL 
                                       );  
                IF UPDATE(IsServicer) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'IsServicer' 
                                   ,CONVERT(VARCHAR(8000),Old.IsServicer,121) 
                                   ,CONVERT(VARCHAR(8000),New.IsServicer,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.IsServicer <> New.IsServicer 
                                    OR ( 
                                         Old.IsServicer IS NULL 
                                         AND New.IsServicer IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.IsServicer IS NULL 
                                         AND Old.IsServicer IS NOT NULL 
                                       );  
                IF UPDATE(ForeignPostClaim) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'ForeignPostClaim' 
                                   ,CONVERT(VARCHAR(8000),Old.ForeignPostClaim,121) 
                                   ,CONVERT(VARCHAR(8000),New.ForeignPostClaim,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.ForeignPostClaim <> New.ForeignPostClaim 
                                    OR ( 
                                         Old.ForeignPostClaim IS NULL 
                                         AND New.ForeignPostClaim IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.ForeignPostClaim IS NULL 
                                         AND Old.ForeignPostClaim IS NOT NULL 
                                       );  
                IF UPDATE(ForeignAddress) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'ForeignAddress' 
                                   ,CONVERT(VARCHAR(8000),Old.ForeignAddress,121) 
                                   ,CONVERT(VARCHAR(8000),New.ForeignAddress,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.ForeignAddress <> New.ForeignAddress 
                                    OR ( 
                                         Old.ForeignAddress IS NULL 
                                         AND New.ForeignAddress IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.ForeignAddress IS NULL 
                                         AND Old.ForeignAddress IS NOT NULL 
                                       );  
                IF UPDATE(ForeignPayAddress) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'ForeignPayAddress' 
                                   ,CONVERT(VARCHAR(8000),Old.ForeignPayAddress,121) 
                                   ,CONVERT(VARCHAR(8000),New.ForeignPayAddress,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.ForeignPayAddress <> New.ForeignPayAddress 
                                    OR ( 
                                         Old.ForeignPayAddress IS NULL 
                                         AND New.ForeignPayAddress IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.ForeignPayAddress IS NULL 
                                         AND Old.ForeignPayAddress IS NOT NULL 
                                       );  
                IF UPDATE(IsLender) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'IsLender' 
                                   ,CONVERT(VARCHAR(8000),Old.IsLender,121) 
                                   ,CONVERT(VARCHAR(8000),New.IsLender,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.IsLender <> New.IsLender 
                                    OR ( 
                                         Old.IsLender IS NULL 
                                         AND New.IsLender IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.IsLender IS NULL 
                                         AND Old.IsLender IS NOT NULL 
                                       );  
                IF UPDATE(ForeignCustService) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'ForeignCustService' 
                                   ,CONVERT(VARCHAR(8000),Old.ForeignCustService,121) 
                                   ,CONVERT(VARCHAR(8000),New.ForeignCustService,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.ForeignCustService <> New.ForeignCustService 
                                    OR ( 
                                         Old.ForeignCustService IS NULL 
                                         AND New.ForeignCustService IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.ForeignCustService IS NULL 
                                         AND Old.ForeignCustService IS NOT NULL 
                                       );  
                IF UPDATE(ForeignFax) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.LenderId 
                                   ,'ForeignFax' 
                                   ,CONVERT(VARCHAR(8000),Old.ForeignFax,121) 
                                   ,CONVERT(VARCHAR(8000),New.ForeignFax,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.LenderId = New.LenderId 
                            WHERE   Old.ForeignFax <> New.ForeignFax 
                                    OR ( 
                                         Old.ForeignFax IS NULL 
                                         AND New.ForeignFax IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.ForeignFax IS NULL 
                                         AND Old.ForeignFax IS NOT NULL 
                                       );  
            END;  
        END;  
 
 
    SET NOCOUNT OFF;  
GO
ALTER TABLE [dbo].[faLenders] ADD CONSTRAINT [PK_faLenders_LenderId] PRIMARY KEY CLUSTERED  ([LenderId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_faLenders_Code_LenderDescrip_PrimaryContact_CampGrpId] ON [dbo].[faLenders] ([Code], [LenderDescrip], [PrimaryContact], [CampGrpId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[faLenders] ADD CONSTRAINT [FK_faLenders_adCountries_CountryId_CountryId] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[adCountries] ([CountryId])
GO
ALTER TABLE [dbo].[faLenders] ADD CONSTRAINT [FK_faLenders_adCountries_PayCountryId_CountryId] FOREIGN KEY ([PayCountryId]) REFERENCES [dbo].[adCountries] ([CountryId])
GO
ALTER TABLE [dbo].[faLenders] ADD CONSTRAINT [FK_faLenders_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[faLenders] ADD CONSTRAINT [FK_faLenders_syStates_PayStateId_StateId] FOREIGN KEY ([PayStateId]) REFERENCES [dbo].[syStates] ([StateId])
GO
ALTER TABLE [dbo].[faLenders] ADD CONSTRAINT [FK_faLenders_syStates_StateId_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[syStates] ([StateId])
GO
ALTER TABLE [dbo].[faLenders] ADD CONSTRAINT [FK_faLenders_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
