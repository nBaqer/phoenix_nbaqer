CREATE TABLE [dbo].[syWidgetUserResourceSettings]
(
[WidgetUserResourceSettingId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_syWidgetUserResourceSettings_WidgetUserResourceSettingId] DEFAULT (newsequentialid()),
[UserId] [uniqueidentifier] NOT NULL,
[WidgetId] [uniqueidentifier] NULL,
[ResourceId] [smallint] NULL,
[PositionX] [int] NOT NULL,
[PositionY] [int] NOT NULL,
[IsVisible] [bit] NOT NULL,
[StatusId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syWidgetUserResourceSettings] ADD CONSTRAINT [PK_syWidgetUserResourceSettings_WidgetUserResourceSettingId] PRIMARY KEY CLUSTERED  ([WidgetUserResourceSettingId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syWidgetUserResourceSettings] ADD CONSTRAINT [FK_syWidgetUserResourceSettings_syResources_ResourceId_ResourceId] FOREIGN KEY ([ResourceId]) REFERENCES [dbo].[syResources] ([ResourceID])
GO
ALTER TABLE [dbo].[syWidgetUserResourceSettings] ADD CONSTRAINT [FK_syWidgetUserResourceSettings_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
ALTER TABLE [dbo].[syWidgetUserResourceSettings] ADD CONSTRAINT [FK_syWidgetUserResourceSettings_syUsers_UserId_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[syUsers] ([UserId])
GO
ALTER TABLE [dbo].[syWidgetUserResourceSettings] ADD CONSTRAINT [FK_syWidgetUserResourceSettings_syWidgets_WidgetId_WidgetId] FOREIGN KEY ([WidgetId]) REFERENCES [dbo].[syWidgets] ([WidgetId])
GO
