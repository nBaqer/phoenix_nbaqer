CREATE TABLE [dbo].[adLeadByLeadGroups]
(
[LeadGrpLeadId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_adLeadByLeadGroups_LeadGrpLeadId] DEFAULT (newid()),
[LeadId] [uniqueidentifier] NULL,
[LeadGrpId] [uniqueidentifier] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[StudentId] [uniqueidentifier] NULL,
[StuEnrollId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adLeadByLeadGroups] ADD CONSTRAINT [PK_adLeadByLeadGroups_LeadGrpLeadId] PRIMARY KEY CLUSTERED  ([LeadGrpLeadId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_adLeadByLeadGroups_StuEnrollId_LeadGrpId] ON [dbo].[adLeadByLeadGroups] ([StuEnrollId], [LeadGrpId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adLeadByLeadGroups] ADD CONSTRAINT [FK_adLeadByLeadGroups_adLeadGroups_LeadGrpId_LeadGrpId] FOREIGN KEY ([LeadGrpId]) REFERENCES [dbo].[adLeadGroups] ([LeadGrpId])
GO
ALTER TABLE [dbo].[adLeadByLeadGroups] ADD CONSTRAINT [FK_adLeadByLeadGroups_adLeads_LeadId_LeadId] FOREIGN KEY ([LeadId]) REFERENCES [dbo].[adLeads] ([LeadId])
GO
ALTER TABLE [dbo].[adLeadByLeadGroups] ADD CONSTRAINT [FK_adLeadByLeadGroups_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY ([StuEnrollId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId]) ON DELETE CASCADE
GO
