CREATE TABLE [dbo].[saBillingMethods]
(
[BillingMethodId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_saBillingMethods_BillingMethodId] DEFAULT (newid()),
[BillingMethodCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[BillingMethodDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NULL,
[BillingMethod] [smallint] NOT NULL,
[AutoBill] [bit] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saBillingMethods_Audit_Delete] ON [dbo].[saBillingMethods]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saBillingMethods','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BillingMethodId
                               ,'BillingMethod'
                               ,CONVERT(VARCHAR(8000),Old.BillingMethod,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BillingMethodId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BillingMethodId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BillingMethodId
                               ,'BillingMethodCode'
                               ,CONVERT(VARCHAR(8000),Old.BillingMethodCode,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BillingMethodId
                               ,'BillingMethodDescrip'
                               ,CONVERT(VARCHAR(8000),Old.BillingMethodDescrip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BillingMethodId
                               ,'AutoBill'
                               ,CONVERT(VARCHAR(8000),Old.AutoBill,121)
                        FROM    Deleted Old; 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saBillingMethods_Audit_Insert] ON [dbo].[saBillingMethods]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saBillingMethods','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BillingMethodId
                               ,'BillingMethod'
                               ,CONVERT(VARCHAR(8000),New.BillingMethod,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BillingMethodId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BillingMethodId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BillingMethodId
                               ,'BillingMethodCode'
                               ,CONVERT(VARCHAR(8000),New.BillingMethodCode,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BillingMethodId
                               ,'BillingMethodDescrip'
                               ,CONVERT(VARCHAR(8000),New.BillingMethodDescrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BillingMethodId
                               ,'AutoBill'
                               ,CONVERT(VARCHAR(8000),New.AutoBill,121)
                        FROM    Inserted New; 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO
GO

GO

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saBillingMethods_Audit_Update] ON [dbo].[saBillingMethods]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saBillingMethods','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(BillingMethod)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BillingMethodId
                                   ,'BillingMethod'
                                   ,CONVERT(VARCHAR(8000),Old.BillingMethod,121)
                                   ,CONVERT(VARCHAR(8000),New.BillingMethod,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BillingMethodId = New.BillingMethodId
                            WHERE   Old.BillingMethod <> New.BillingMethod
                                    OR (
                                         Old.BillingMethod IS NULL
                                         AND New.BillingMethod IS NOT NULL
                                       )
                                    OR (
                                         New.BillingMethod IS NULL
                                         AND Old.BillingMethod IS NOT NULL
                                       ); 
                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BillingMethodId
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BillingMethodId = New.BillingMethodId
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BillingMethodId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BillingMethodId = New.BillingMethodId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(BillingMethodCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BillingMethodId
                                   ,'BillingMethodCode'
                                   ,CONVERT(VARCHAR(8000),Old.BillingMethodCode,121)
                                   ,CONVERT(VARCHAR(8000),New.BillingMethodCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BillingMethodId = New.BillingMethodId
                            WHERE   Old.BillingMethodCode <> New.BillingMethodCode
                                    OR (
                                         Old.BillingMethodCode IS NULL
                                         AND New.BillingMethodCode IS NOT NULL
                                       )
                                    OR (
                                         New.BillingMethodCode IS NULL
                                         AND Old.BillingMethodCode IS NOT NULL
                                       ); 
                IF UPDATE(BillingMethodDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BillingMethodId
                                   ,'BillingMethodDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.BillingMethodDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.BillingMethodDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BillingMethodId = New.BillingMethodId
                            WHERE   Old.BillingMethodDescrip <> New.BillingMethodDescrip
                                    OR (
                                         Old.BillingMethodDescrip IS NULL
                                         AND New.BillingMethodDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.BillingMethodDescrip IS NULL
                                         AND Old.BillingMethodDescrip IS NOT NULL
                                       ); 
                IF UPDATE(AutoBill)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BillingMethodId
                                   ,'AutoBill'
                                   ,CONVERT(VARCHAR(8000),Old.AutoBill,121)
                                   ,CONVERT(VARCHAR(8000),New.AutoBill,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BillingMethodId = New.BillingMethodId
                            WHERE   Old.AutoBill <> New.AutoBill
                                    OR (
                                         Old.AutoBill IS NULL
                                         AND New.AutoBill IS NOT NULL
                                       )
                                    OR (
                                         New.AutoBill IS NULL
                                         AND Old.AutoBill IS NOT NULL
                                       ); 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[saBillingMethods] ADD CONSTRAINT [PK_saBillingMethods_BillingMethodId] PRIMARY KEY CLUSTERED  ([BillingMethodId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_saBillingMethods_BillingMethodCode_BillingMethodDescrip_CampGrpId] ON [dbo].[saBillingMethods] ([BillingMethodCode], [BillingMethodDescrip], [CampGrpId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saBillingMethods] ADD CONSTRAINT [FK_saBillingMethods_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[saBillingMethods] ADD CONSTRAINT [FK_saBillingMethods_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
