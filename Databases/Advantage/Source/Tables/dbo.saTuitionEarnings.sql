CREATE TABLE [dbo].[saTuitionEarnings]
(
[TuitionEarningId] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[TuitionEarningsCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[TuitionEarningsDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NULL,
[RevenueBasisIdx] [tinyint] NOT NULL,
[PercentageRangeBasisIdx] [tinyint] NOT NULL CONSTRAINT [DF_saTuitionEarnings_PercentageRangeBasisIdx] DEFAULT ((0)),
[PercentToEarnIdx] [tinyint] NOT NULL,
[FullMonthBeforeDay] [tinyint] NULL,
[HalfMonthBeforeDay] [tinyint] NULL,
[IsAttendanceRequired] [bit] NULL,
[RequiredAttendancePercent] [tinyint] NULL,
[RequiredCumulativeHoursAttended] [smallint] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[TakeHolidays] [bit] NULL,
[RemainingMethod] [bit] NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saTuitionEarnings_Audit_Delete] ON [dbo].[saTuitionEarnings]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saTuitionEarnings','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TuitionEarningId
                               ,'RequiredCumulativeHoursAttended'
                               ,CONVERT(VARCHAR(8000),Old.RequiredCumulativeHoursAttended,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TuitionEarningId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TuitionEarningId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TuitionEarningId
                               ,'TuitionEarningsCode'
                               ,CONVERT(VARCHAR(8000),Old.TuitionEarningsCode,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TuitionEarningId
                               ,'TuitionEarningsDescrip'
                               ,CONVERT(VARCHAR(8000),Old.TuitionEarningsDescrip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TuitionEarningId
                               ,'RevenueBasisIdx'
                               ,CONVERT(VARCHAR(8000),Old.RevenueBasisIdx,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TuitionEarningId
                               ,'RequiredAttendancePercent'
                               ,CONVERT(VARCHAR(8000),Old.RequiredAttendancePercent,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TuitionEarningId
                               ,'PercentageRangeBasisIdx'
                               ,CONVERT(VARCHAR(8000),Old.PercentageRangeBasisIdx,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TuitionEarningId
                               ,'PercentToEarnIdx'
                               ,CONVERT(VARCHAR(8000),Old.PercentToEarnIdx,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TuitionEarningId
                               ,'HalfMonthBeforeDay'
                               ,CONVERT(VARCHAR(8000),Old.HalfMonthBeforeDay,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TuitionEarningId
                               ,'FullMonthBeforeDay'
                               ,CONVERT(VARCHAR(8000),Old.FullMonthBeforeDay,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TuitionEarningId
                               ,'IsAttendanceRequired'
                               ,CONVERT(VARCHAR(8000),Old.IsAttendanceRequired,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO
GO

GO

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saTuitionEarnings_Audit_Insert] ON [dbo].[saTuitionEarnings]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saTuitionEarnings','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TuitionEarningId
                               ,'RequiredCumulativeHoursAttended'
                               ,CONVERT(VARCHAR(8000),New.RequiredCumulativeHoursAttended,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TuitionEarningId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TuitionEarningId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TuitionEarningId
                               ,'TuitionEarningsCode'
                               ,CONVERT(VARCHAR(8000),New.TuitionEarningsCode,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TuitionEarningId
                               ,'TuitionEarningsDescrip'
                               ,CONVERT(VARCHAR(8000),New.TuitionEarningsDescrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TuitionEarningId
                               ,'RevenueBasisIdx'
                               ,CONVERT(VARCHAR(8000),New.RevenueBasisIdx,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TuitionEarningId
                               ,'RequiredAttendancePercent'
                               ,CONVERT(VARCHAR(8000),New.RequiredAttendancePercent,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TuitionEarningId
                               ,'PercentageRangeBasisIdx'
                               ,CONVERT(VARCHAR(8000),New.PercentageRangeBasisIdx,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TuitionEarningId
                               ,'PercentToEarnIdx'
                               ,CONVERT(VARCHAR(8000),New.PercentToEarnIdx,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TuitionEarningId
                               ,'HalfMonthBeforeDay'
                               ,CONVERT(VARCHAR(8000),New.HalfMonthBeforeDay,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TuitionEarningId
                               ,'FullMonthBeforeDay'
                               ,CONVERT(VARCHAR(8000),New.FullMonthBeforeDay,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TuitionEarningId
                               ,'IsAttendanceRequired'
                               ,CONVERT(VARCHAR(8000),New.IsAttendanceRequired,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO
GO

GO

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saTuitionEarnings_Audit_Update] ON [dbo].[saTuitionEarnings]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saTuitionEarnings','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(RequiredCumulativeHoursAttended)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TuitionEarningId
                                   ,'RequiredCumulativeHoursAttended'
                                   ,CONVERT(VARCHAR(8000),Old.RequiredCumulativeHoursAttended,121)
                                   ,CONVERT(VARCHAR(8000),New.RequiredCumulativeHoursAttended,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TuitionEarningId = New.TuitionEarningId
                            WHERE   Old.RequiredCumulativeHoursAttended <> New.RequiredCumulativeHoursAttended
                                    OR (
                                         Old.RequiredCumulativeHoursAttended IS NULL
                                         AND New.RequiredCumulativeHoursAttended IS NOT NULL
                                       )
                                    OR (
                                         New.RequiredCumulativeHoursAttended IS NULL
                                         AND Old.RequiredCumulativeHoursAttended IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TuitionEarningId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TuitionEarningId = New.TuitionEarningId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TuitionEarningId
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TuitionEarningId = New.TuitionEarningId
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 
                IF UPDATE(TuitionEarningsCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TuitionEarningId
                                   ,'TuitionEarningsCode'
                                   ,CONVERT(VARCHAR(8000),Old.TuitionEarningsCode,121)
                                   ,CONVERT(VARCHAR(8000),New.TuitionEarningsCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TuitionEarningId = New.TuitionEarningId
                            WHERE   Old.TuitionEarningsCode <> New.TuitionEarningsCode
                                    OR (
                                         Old.TuitionEarningsCode IS NULL
                                         AND New.TuitionEarningsCode IS NOT NULL
                                       )
                                    OR (
                                         New.TuitionEarningsCode IS NULL
                                         AND Old.TuitionEarningsCode IS NOT NULL
                                       ); 
                IF UPDATE(TuitionEarningsDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TuitionEarningId
                                   ,'TuitionEarningsDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.TuitionEarningsDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.TuitionEarningsDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TuitionEarningId = New.TuitionEarningId
                            WHERE   Old.TuitionEarningsDescrip <> New.TuitionEarningsDescrip
                                    OR (
                                         Old.TuitionEarningsDescrip IS NULL
                                         AND New.TuitionEarningsDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.TuitionEarningsDescrip IS NULL
                                         AND Old.TuitionEarningsDescrip IS NOT NULL
                                       ); 
                IF UPDATE(RevenueBasisIdx)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TuitionEarningId
                                   ,'RevenueBasisIdx'
                                   ,CONVERT(VARCHAR(8000),Old.RevenueBasisIdx,121)
                                   ,CONVERT(VARCHAR(8000),New.RevenueBasisIdx,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TuitionEarningId = New.TuitionEarningId
                            WHERE   Old.RevenueBasisIdx <> New.RevenueBasisIdx
                                    OR (
                                         Old.RevenueBasisIdx IS NULL
                                         AND New.RevenueBasisIdx IS NOT NULL
                                       )
                                    OR (
                                         New.RevenueBasisIdx IS NULL
                                         AND Old.RevenueBasisIdx IS NOT NULL
                                       ); 
                IF UPDATE(RequiredAttendancePercent)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TuitionEarningId
                                   ,'RequiredAttendancePercent'
                                   ,CONVERT(VARCHAR(8000),Old.RequiredAttendancePercent,121)
                                   ,CONVERT(VARCHAR(8000),New.RequiredAttendancePercent,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TuitionEarningId = New.TuitionEarningId
                            WHERE   Old.RequiredAttendancePercent <> New.RequiredAttendancePercent
                                    OR (
                                         Old.RequiredAttendancePercent IS NULL
                                         AND New.RequiredAttendancePercent IS NOT NULL
                                       )
                                    OR (
                                         New.RequiredAttendancePercent IS NULL
                                         AND Old.RequiredAttendancePercent IS NOT NULL
                                       ); 
                IF UPDATE(PercentageRangeBasisIdx)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TuitionEarningId
                                   ,'PercentageRangeBasisIdx'
                                   ,CONVERT(VARCHAR(8000),Old.PercentageRangeBasisIdx,121)
                                   ,CONVERT(VARCHAR(8000),New.PercentageRangeBasisIdx,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TuitionEarningId = New.TuitionEarningId
                            WHERE   Old.PercentageRangeBasisIdx <> New.PercentageRangeBasisIdx
                                    OR (
                                         Old.PercentageRangeBasisIdx IS NULL
                                         AND New.PercentageRangeBasisIdx IS NOT NULL
                                       )
                                    OR (
                                         New.PercentageRangeBasisIdx IS NULL
                                         AND Old.PercentageRangeBasisIdx IS NOT NULL
                                       ); 
                IF UPDATE(PercentToEarnIdx)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TuitionEarningId
                                   ,'PercentToEarnIdx'
                                   ,CONVERT(VARCHAR(8000),Old.PercentToEarnIdx,121)
                                   ,CONVERT(VARCHAR(8000),New.PercentToEarnIdx,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TuitionEarningId = New.TuitionEarningId
                            WHERE   Old.PercentToEarnIdx <> New.PercentToEarnIdx
                                    OR (
                                         Old.PercentToEarnIdx IS NULL
                                         AND New.PercentToEarnIdx IS NOT NULL
                                       )
                                    OR (
                                         New.PercentToEarnIdx IS NULL
                                         AND Old.PercentToEarnIdx IS NOT NULL
                                       ); 
                IF UPDATE(HalfMonthBeforeDay)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TuitionEarningId
                                   ,'HalfMonthBeforeDay'
                                   ,CONVERT(VARCHAR(8000),Old.HalfMonthBeforeDay,121)
                                   ,CONVERT(VARCHAR(8000),New.HalfMonthBeforeDay,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TuitionEarningId = New.TuitionEarningId
                            WHERE   Old.HalfMonthBeforeDay <> New.HalfMonthBeforeDay
                                    OR (
                                         Old.HalfMonthBeforeDay IS NULL
                                         AND New.HalfMonthBeforeDay IS NOT NULL
                                       )
                                    OR (
                                         New.HalfMonthBeforeDay IS NULL
                                         AND Old.HalfMonthBeforeDay IS NOT NULL
                                       ); 
                IF UPDATE(FullMonthBeforeDay)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TuitionEarningId
                                   ,'FullMonthBeforeDay'
                                   ,CONVERT(VARCHAR(8000),Old.FullMonthBeforeDay,121)
                                   ,CONVERT(VARCHAR(8000),New.FullMonthBeforeDay,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TuitionEarningId = New.TuitionEarningId
                            WHERE   Old.FullMonthBeforeDay <> New.FullMonthBeforeDay
                                    OR (
                                         Old.FullMonthBeforeDay IS NULL
                                         AND New.FullMonthBeforeDay IS NOT NULL
                                       )
                                    OR (
                                         New.FullMonthBeforeDay IS NULL
                                         AND Old.FullMonthBeforeDay IS NOT NULL
                                       ); 
                IF UPDATE(IsAttendanceRequired)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TuitionEarningId
                                   ,'IsAttendanceRequired'
                                   ,CONVERT(VARCHAR(8000),Old.IsAttendanceRequired,121)
                                   ,CONVERT(VARCHAR(8000),New.IsAttendanceRequired,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TuitionEarningId = New.TuitionEarningId
                            WHERE   Old.IsAttendanceRequired <> New.IsAttendanceRequired
                                    OR (
                                         Old.IsAttendanceRequired IS NULL
                                         AND New.IsAttendanceRequired IS NOT NULL
                                       )
                                    OR (
                                         New.IsAttendanceRequired IS NULL
                                         AND Old.IsAttendanceRequired IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[saTuitionEarnings] ADD CONSTRAINT [PK_saTuitionEarnings_TuitionEarningId] PRIMARY KEY CLUSTERED  ([TuitionEarningId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_saTuitionEarnings_TuitionEarningsCode_TuitionEarningsDescrip_CampGrpId] ON [dbo].[saTuitionEarnings] ([TuitionEarningsCode], [TuitionEarningsDescrip], [CampGrpId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saTuitionEarnings] ADD CONSTRAINT [FK_saTuitionEarnings_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[saTuitionEarnings] ADD CONSTRAINT [FK_saTuitionEarnings_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
