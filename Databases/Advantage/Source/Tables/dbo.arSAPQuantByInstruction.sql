CREATE TABLE [dbo].[arSAPQuantByInstruction]
(
[SAPQuantInsTypeID] [uniqueidentifier] NOT NULL,
[QuantMinValue] [decimal] (18, 2) NOT NULL,
[SAPDetailId] [uniqueidentifier] NOT NULL,
[InstructionTypeId] [uniqueidentifier] NOT NULL,
[moddate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arSAPQuantByInstruction] ADD CONSTRAINT [PK_arSAPQuantByInstruction_SAPQuantInsTypeID] PRIMARY KEY CLUSTERED  ([SAPQuantInsTypeID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arSAPQuantByInstruction] ADD CONSTRAINT [FK_arSAPQuantByInstruction_arInstructionType_InstructionTypeId_InstructionTypeId] FOREIGN KEY ([InstructionTypeId]) REFERENCES [dbo].[arInstructionType] ([InstructionTypeId])
GO
ALTER TABLE [dbo].[arSAPQuantByInstruction] ADD CONSTRAINT [FK_arSAPQuantByInstruction_arSAPDetails_SAPDetailId_SAPDetailId] FOREIGN KEY ([SAPDetailId]) REFERENCES [dbo].[arSAPDetails] ([SAPDetailId])
GO
