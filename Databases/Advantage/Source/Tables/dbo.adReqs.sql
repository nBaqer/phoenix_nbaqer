CREATE TABLE [dbo].[adReqs]
(
[adReqId] [uniqueidentifier] NOT NULL,
[Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Descrip] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusId] [uniqueidentifier] NULL,
[CampGrpId] [uniqueidentifier] NULL,
[ModuleId] [int] NULL,
[adReqTypeId] [int] NULL,
[ModUser] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[EdLvlId] [uniqueidentifier] NULL,
[ReqforEnrollment] [bit] NULL,
[ReqforFinancialAid] [bit] NULL,
[ReqforGraduation] [bit] NULL,
[IPEDSValue] [int] NULL,
[IsSystemRequirement] [bit] NULL,
[RequiredForTermination] [bit] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[adReqs_Audit_Delete] ON [dbo].[adReqs]
    FOR DELETE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adReqs','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adReqId
                               ,'Code'
                               ,CONVERT(VARCHAR(8000),Old.Code,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adReqId
                               ,'Descrip'
                               ,CONVERT(VARCHAR(8000),Old.Descrip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adReqId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adReqId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adReqId
                               ,'ModuleId'
                               ,CONVERT(VARCHAR(8000),Old.ModuleId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adReqId
                               ,'adReqTypeId'
                               ,CONVERT(VARCHAR(8000),Old.adReqTypeId,121)
                        FROM    Deleted Old; 
		INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adReqId
                               ,'IsSystemRequirement'
                               ,CONVERT(VARCHAR(8000),Old.IsSystemRequirement,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adReqId
                               ,'RequiredForTermination'
                               ,CONVERT(VARCHAR(8000),Old.RequiredForTermination,121)
                        FROM    Deleted Old;
--INSERT INTO syAuditHistDetail (AuditHistId, RowId, ColumnName, OldValue) 
--SELECT @AuditHistId, Old.[adReqId], 'MinScore', CONVERT(varchar(8000), Old.[MinScore], 121) 
--FROM Deleted Old 
--INSERT INTO syAuditHistDetail (AuditHistId, RowId, ColumnName, OldValue) 
--SELECT @AuditHistId, Old.[adReqId], 'MaxScore', CONVERT(varchar(8000), Old.[MaxScore], 121) 
--FROM Deleted Old 
            END; 
        END;



    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[adReqs_Audit_Insert] ON [dbo].[adReqs]
    FOR INSERT
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adReqs','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adReqId
                               ,'Code'
                               ,CONVERT(VARCHAR(8000),New.Code,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adReqId
                               ,'Descrip'
                               ,CONVERT(VARCHAR(8000),New.Descrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adReqId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adReqId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adReqId
                               ,'ModuleId'
                               ,CONVERT(VARCHAR(8000),New.ModuleId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adReqId
                               ,'adReqTypeId'
                               ,CONVERT(VARCHAR(8000),New.adReqTypeId,121)
                        FROM    Inserted New; 
		INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adReqId
                               ,'IsSystemRequirement'
                               ,CONVERT(VARCHAR(8000),New.IsSystemRequirement,121)
                        FROM    Inserted New; 
		INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adReqId
                               ,'RequiredForTermination'
                               ,CONVERT(VARCHAR(8000),New.RequiredForTermination,121)
                        FROM    Inserted New; 

--INSERT INTO syAuditHistDetail (AuditHistId, RowId, ColumnName, NewValue) 
--SELECT @AuditHistId, New.[adReqId], 'MinScore', CONVERT(varchar(8000), New.[MinScore], 121) 
--FROM Inserted New 
--INSERT INTO syAuditHistDetail (AuditHistId, RowId, ColumnName, NewValue) 
--SELECT @AuditHistId, New.[adReqId], 'MaxScore', CONVERT(varchar(8000), New.[MaxScore], 121) 
--FROM Inserted New 
            END; 
        END;



    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[adReqs_Audit_Update] ON [dbo].[adReqs]
    FOR UPDATE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adReqs','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(Code)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.adReqId
                                   ,'Code'
                                   ,CONVERT(VARCHAR(8000),Old.Code,121)
                                   ,CONVERT(VARCHAR(8000),New.Code,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.adReqId = New.adReqId
                            WHERE   Old.Code <> New.Code
                                    OR (
                                         Old.Code IS NULL
                                         AND New.Code IS NOT NULL
                                       )
                                    OR (
                                         New.Code IS NULL
                                         AND Old.Code IS NOT NULL
                                       ); 
                IF UPDATE(Descrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.adReqId
                                   ,'Descrip'
                                   ,CONVERT(VARCHAR(8000),Old.Descrip,121)
                                   ,CONVERT(VARCHAR(8000),New.Descrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.adReqId = New.adReqId
                            WHERE   Old.Descrip <> New.Descrip
                                    OR (
                                         Old.Descrip IS NULL
                                         AND New.Descrip IS NOT NULL
                                       )
                                    OR (
                                         New.Descrip IS NULL
                                         AND Old.Descrip IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.adReqId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.adReqId = New.adReqId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.adReqId
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.adReqId = New.adReqId
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 
                IF UPDATE(ModuleId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.adReqId
                                   ,'ModuleId'
                                   ,CONVERT(VARCHAR(8000),Old.ModuleId,121)
                                   ,CONVERT(VARCHAR(8000),New.ModuleId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.adReqId = New.adReqId
                            WHERE   Old.ModuleId <> New.ModuleId
                                    OR (
                                         Old.ModuleId IS NULL
                                         AND New.ModuleId IS NOT NULL
                                       )
                                    OR (
                                         New.ModuleId IS NULL
                                         AND Old.ModuleId IS NOT NULL
                                       ); 
                IF UPDATE(adReqTypeId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.adReqId
                                   ,'adReqTypeId'
                                   ,CONVERT(VARCHAR(8000),Old.adReqTypeId,121)
                                   ,CONVERT(VARCHAR(8000),New.adReqTypeId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.adReqId = New.adReqId
                            WHERE   Old.adReqTypeId <> New.adReqTypeId
                                    OR (
                                         Old.adReqTypeId IS NULL
                                         AND New.adReqTypeId IS NOT NULL
                                       )
                                    OR (
                                         New.adReqTypeId IS NULL
                                         AND Old.adReqTypeId IS NOT NULL
                                       ); 
		IF UPDATE(adReqTypeId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.adReqId
                                   ,'IsSystemRequirement'
                                   ,CONVERT(VARCHAR(8000),Old.IsSystemRequirement,121)
                                   ,CONVERT(VARCHAR(8000),New.IsSystemRequirement,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.adReqId = New.adReqId
                            WHERE   Old.IsSystemRequirement <> New.IsSystemRequirement
                                    OR (
                                         Old.IsSystemRequirement IS NULL
                                         AND New.IsSystemRequirement IS NOT NULL
                                       )
                                    OR (
                                         New.IsSystemRequirement IS NULL
                                         AND Old.IsSystemRequirement IS NOT NULL
                                       ); 
		IF UPDATE(adReqTypeId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.adReqId
                                   ,'RequiredForTermination'
                                   ,CONVERT(VARCHAR(8000),Old.RequiredForTermination,121)
                                   ,CONVERT(VARCHAR(8000),New.RequiredForTermination,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.adReqId = New.adReqId
                            WHERE   Old.RequiredForTermination <> New.RequiredForTermination
                                    OR (
                                         Old.RequiredForTermination IS NULL
                                         AND New.RequiredForTermination IS NOT NULL
                                       )
                                    OR (
                                         New.RequiredForTermination IS NULL
                                         AND Old.RequiredForTermination IS NOT NULL
                                       ); 
--IF UPDATE ([MinScore]) 
--INSERT INTO syAuditHistDetail (AuditHistId, RowId, ColumnName, OldValue, NewValue) 
--SELECT @AuditHistId, New.[adReqId], 'MinScore', CONVERT(varchar(8000), Old.[MinScore], 121), CONVERT(varchar(8000), New.[MinScore], 121) 
--FROM Inserted New FULL OUTER JOIN Deleted Old ON Old.[adReqId] = New.[adReqId] 
--WHERE Old.[MinScore] <> New.[MinScore] OR (Old.[MinScore] IS NULL AND New.[MinScore] IS NOT NULL) OR (New.[MinScore] IS NULL AND Old.[MinScore] IS NOT NULL) 
--IF UPDATE ([MaxScore]) 
--INSERT INTO syAuditHistDetail (AuditHistId, RowId, ColumnName, OldValue, NewValue) 
--SELECT @AuditHistId, New.[adReqId], 'MaxScore', CONVERT(varchar(8000), Old.[MaxScore], 121), CONVERT(varchar(8000), New.[MaxScore], 121) 
--FROM Inserted New FULL OUTER JOIN Deleted Old ON Old.[adReqId] = New.[adReqId] 
--WHERE Old.[MaxScore] <> New.[MaxScore] OR (Old.[MaxScore] IS NULL AND New.[MaxScore] IS NOT NULL) OR (New.[MaxScore] IS NULL AND Old.[MaxScore] IS NOT NULL) 
            END; 
        END;



    SET NOCOUNT OFF;

GO
ALTER TABLE [dbo].[adReqs] ADD CONSTRAINT [PK_adReqs_adReqId] PRIMARY KEY CLUSTERED  ([adReqId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_adReqs_Code_Descrip_CampGrpId] ON [dbo].[adReqs] ([Code], [Descrip], [CampGrpId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adReqs] ADD CONSTRAINT [FK_adReqs_adEdLvls_EdLvlId_EdLvlId] FOREIGN KEY ([EdLvlId]) REFERENCES [dbo].[adEdLvls] ([EdLvlId])
GO
ALTER TABLE [dbo].[adReqs] ADD CONSTRAINT [FK_adReqs_adReqTypes_adReqTypeId_adReqTypeId] FOREIGN KEY ([adReqTypeId]) REFERENCES [dbo].[adReqTypes] ([adReqTypeId])
GO
ALTER TABLE [dbo].[adReqs] ADD CONSTRAINT [FK_adReqs_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[adReqs] ADD CONSTRAINT [FK_adReqs_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
