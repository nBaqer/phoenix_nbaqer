CREATE TABLE [dbo].[syFieldTypes]
(
[FldTypeId] [int] NOT NULL,
[FldType] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syFieldTypes] ADD CONSTRAINT [PK_syFieldTypes_FldTypeId] PRIMARY KEY CLUSTERED  ([FldTypeId]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[syFieldTypes] TO [AdvantageRole]
GO
