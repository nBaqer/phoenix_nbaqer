CREATE TABLE [dbo].[syLeadStatusesChanges]
(
[StatusChangeId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syLeadStatusesChanges_StatusChangeId] DEFAULT (newid()),
[LeadId] [uniqueidentifier] NOT NULL,
[OrigStatusId] [uniqueidentifier] NULL,
[NewStatusId] [uniqueidentifier] NOT NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateOfChange] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[syLeadStatusesChanges_Audit_Delete] ON [dbo].[syLeadStatusesChanges]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syLeadStatusesChanges','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StatusChangeId
                               ,'LeadID'
                               ,CONVERT(VARCHAR(8000),Old.LeadId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StatusChangeId
                               ,'OrigStatusId'
                               ,CONVERT(VARCHAR(8000),Old.OrigStatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StatusChangeId
                               ,'NewStatusId'
                               ,CONVERT(VARCHAR(8000),Old.NewStatusId,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[syLeadStatusesChanges_Audit_Insert] ON [dbo].[syLeadStatusesChanges]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syLeadStatusesChanges','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StatusChangeId
                               ,'LeadID'
                               ,CONVERT(VARCHAR(8000),New.LeadId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StatusChangeId
                               ,'OrigStatusId'
                               ,CONVERT(VARCHAR(8000),New.OrigStatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StatusChangeId
                               ,'NewStatusId'
                               ,CONVERT(VARCHAR(8000),New.NewStatusId,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[syLeadStatusesChanges_Audit_Update] ON [dbo].[syLeadStatusesChanges]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syLeadStatusesChanges','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(LeadId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StatusChangeId
                                   ,'LeadID'
                                   ,CONVERT(VARCHAR(8000),Old.LeadId,121)
                                   ,CONVERT(VARCHAR(8000),New.LeadId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StatusChangeId = New.StatusChangeId
                            WHERE   Old.LeadId <> New.LeadId
                                    OR (
                                         Old.LeadId IS NULL
                                         AND New.LeadId IS NOT NULL
                                       )
                                    OR (
                                         New.LeadId IS NULL
                                         AND Old.LeadId IS NOT NULL
                                       ); 
                IF UPDATE(OrigStatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StatusChangeId
                                   ,'OrigStatusId'
                                   ,CONVERT(VARCHAR(8000),Old.OrigStatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.OrigStatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StatusChangeId = New.StatusChangeId
                            WHERE   Old.OrigStatusId <> New.OrigStatusId
                                    OR (
                                         Old.OrigStatusId IS NULL
                                         AND New.OrigStatusId IS NOT NULL
                                       )
                                    OR (
                                         New.OrigStatusId IS NULL
                                         AND Old.OrigStatusId IS NOT NULL
                                       ); 
                IF UPDATE(NewStatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StatusChangeId
                                   ,'NewStatusId'
                                   ,CONVERT(VARCHAR(8000),Old.NewStatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.NewStatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StatusChangeId = New.StatusChangeId
                            WHERE   Old.NewStatusId <> New.NewStatusId
                                    OR (
                                         Old.NewStatusId IS NULL
                                         AND New.NewStatusId IS NOT NULL
                                       )
                                    OR (
                                         New.NewStatusId IS NULL
                                         AND Old.NewStatusId IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO
ALTER TABLE [dbo].[syLeadStatusesChanges] ADD CONSTRAINT [PK_syLeadStatusesChanges_StatusChangeId] PRIMARY KEY CLUSTERED  ([StatusChangeId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syLeadStatusesChanges] ADD CONSTRAINT [FK_syLeadStatusesChanges_adLeads_LeadId_LeadId] FOREIGN KEY ([LeadId]) REFERENCES [dbo].[adLeads] ([LeadId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[syLeadStatusesChanges] ADD CONSTRAINT [FK_syLeadStatusesChanges_syStatusCodes_NewStatusId_StatusCodeId] FOREIGN KEY ([NewStatusId]) REFERENCES [dbo].[syStatusCodes] ([StatusCodeId])
GO
ALTER TABLE [dbo].[syLeadStatusesChanges] ADD CONSTRAINT [FK_syLeadStatusesChanges_syStatusCodes_OrigStatusId_StatusCodeId] FOREIGN KEY ([OrigStatusId]) REFERENCES [dbo].[syStatusCodes] ([StatusCodeId])
GO
