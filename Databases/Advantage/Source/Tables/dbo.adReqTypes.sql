CREATE TABLE [dbo].[adReqTypes]
(
[adReqTypeId] [int] NOT NULL,
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adReqTypes] ADD CONSTRAINT [PK_adReqTypes_adReqTypeId] PRIMARY KEY CLUSTERED  ([adReqTypeId]) ON [PRIMARY]
GO
