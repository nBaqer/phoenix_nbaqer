CREATE TABLE [dbo].[plJobStatus]
(
[JobStatusId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_plJobStatus_JobStatusId] DEFAULT (newid()),
[JobStatusDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[JobCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CampGrpId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[plJobStatus_Audit_Delete] ON [dbo].[plJobStatus]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'plJobStatus','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.JobStatusId
                               ,'JobStatusDescrip'
                               ,CONVERT(VARCHAR(8000),Old.JobStatusDescrip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.JobStatusId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.JobStatusId
                               ,'JobCode'
                               ,CONVERT(VARCHAR(8000),Old.JobCode,121)
                        FROM    Deleted Old; 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[plJobStatus_Audit_Insert] ON [dbo].[plJobStatus]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'plJobStatus','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.JobStatusId
                               ,'JobStatusDescrip'
                               ,CONVERT(VARCHAR(8000),New.JobStatusDescrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.JobStatusId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.JobStatusId
                               ,'JobCode'
                               ,CONVERT(VARCHAR(8000),New.JobCode,121)
                        FROM    Inserted New; 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[plJobStatus_Audit_Update] ON [dbo].[plJobStatus]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'plJobStatus','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(JobStatusDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.JobStatusId
                                   ,'JobStatusDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.JobStatusDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.JobStatusDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.JobStatusId = New.JobStatusId
                            WHERE   Old.JobStatusDescrip <> New.JobStatusDescrip
                                    OR (
                                         Old.JobStatusDescrip IS NULL
                                         AND New.JobStatusDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.JobStatusDescrip IS NULL
                                         AND Old.JobStatusDescrip IS NOT NULL
                                       ); 

                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.JobStatusId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.JobStatusId = New.JobStatusId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 

                IF UPDATE(JobCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.JobStatusId
                                   ,'JobCode'
                                   ,CONVERT(VARCHAR(8000),Old.JobCode,121)
                                   ,CONVERT(VARCHAR(8000),New.JobCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.JobStatusId = New.JobStatusId
                            WHERE   Old.JobCode <> New.JobCode
                                    OR (
                                         Old.JobCode IS NULL
                                         AND New.JobCode IS NOT NULL
                                       )
                                    OR (
                                         New.JobCode IS NULL
                                         AND Old.JobCode IS NOT NULL
                                       ); 

                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.JobStatusId
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.JobStatusId = New.JobStatusId
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 

            END; 
        END; 



    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[plJobStatus] ADD CONSTRAINT [PK_plJobStatus_JobStatusId] PRIMARY KEY CLUSTERED  ([JobStatusId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_plJobStatus_JobStatusDescrip_JobCode] ON [dbo].[plJobStatus] ([JobStatusDescrip], [JobCode]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[plJobStatus] ADD CONSTRAINT [FK_plJobStatus_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[plJobStatus] ADD CONSTRAINT [FK_plJobStatus_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
