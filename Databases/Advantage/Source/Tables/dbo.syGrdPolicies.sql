CREATE TABLE [dbo].[syGrdPolicies]
(
[GrdPolicyId] [int] NOT NULL,
[Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllowParam] [bit] NOT NULL,
[StatusId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syGrdPolicies] ADD CONSTRAINT [PK_syGrdPolicies_GrdPolicyId] PRIMARY KEY CLUSTERED  ([GrdPolicyId]) ON [PRIMARY]
GO
