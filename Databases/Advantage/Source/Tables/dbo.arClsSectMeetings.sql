CREATE TABLE [dbo].[arClsSectMeetings]
(
[ClsSectMeetingId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arClsSectMeetings_ClsSectMeetingId] DEFAULT (newid()),
[WorkDaysId] [uniqueidentifier] NULL,
[RoomId] [uniqueidentifier] NULL,
[TimeIntervalId] [uniqueidentifier] NULL,
[ClsSectionId] [uniqueidentifier] NOT NULL,
[EndIntervalId] [uniqueidentifier] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[PeriodId] [uniqueidentifier] NULL,
[AltPeriodId] [uniqueidentifier] NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[InstructionTypeID] [uniqueidentifier] NOT NULL,
[IsMeetingRescheduled] [bit] NULL CONSTRAINT [DF_arClsSectMeetings_IsMeetingRescheduled] DEFAULT ((0)),
[BreakDuration] [int] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[arClsSectMeetings_Audit_Delete] ON [dbo].[arClsSectMeetings]
    FOR DELETE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arClsSectMeetings','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ClsSectMeetingId
                               ,'RoomId'
                               ,CONVERT(VARCHAR(8000),Old.RoomId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ClsSectMeetingId
                               ,'ClsSectionId'
                               ,CONVERT(VARCHAR(8000),Old.ClsSectionId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ClsSectMeetingId
                               ,'EndIntervalId'
                               ,CONVERT(VARCHAR(8000),Old.EndIntervalId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ClsSectMeetingId
                               ,'TimeIntervalId'
                               ,CONVERT(VARCHAR(8000),Old.TimeIntervalId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ClsSectMeetingId
                               ,'WorkDaysId'
                               ,CONVERT(VARCHAR(8000),Old.WorkDaysId,121)
                        FROM    Deleted Old; 
            END; 
        END;



    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[arClsSectMeetings_Audit_Insert] ON [dbo].[arClsSectMeetings]
    FOR INSERT
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arClsSectMeetings','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ClsSectMeetingId
                               ,'RoomId'
                               ,CONVERT(VARCHAR(8000),New.RoomId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ClsSectMeetingId
                               ,'ClsSectionId'
                               ,CONVERT(VARCHAR(8000),New.ClsSectionId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ClsSectMeetingId
                               ,'EndIntervalId'
                               ,CONVERT(VARCHAR(8000),New.EndIntervalId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ClsSectMeetingId
                               ,'TimeIntervalId'
                               ,CONVERT(VARCHAR(8000),New.TimeIntervalId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ClsSectMeetingId
                               ,'WorkDaysId'
                               ,CONVERT(VARCHAR(8000),New.WorkDaysId,121)
                        FROM    Inserted New; 
            END; 
        END;



    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[arClsSectMeetings_Audit_Update] ON [dbo].[arClsSectMeetings]
    FOR UPDATE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arClsSectMeetings','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(RoomId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ClsSectMeetingId
                                   ,'RoomId'
                                   ,CONVERT(VARCHAR(8000),Old.RoomId,121)
                                   ,CONVERT(VARCHAR(8000),New.RoomId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ClsSectMeetingId = New.ClsSectMeetingId
                            WHERE   Old.RoomId <> New.RoomId
                                    OR (
                                         Old.RoomId IS NULL
                                         AND New.RoomId IS NOT NULL
                                       )
                                    OR (
                                         New.RoomId IS NULL
                                         AND Old.RoomId IS NOT NULL
                                       ); 
                IF UPDATE(ClsSectionId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ClsSectMeetingId
                                   ,'ClsSectionId'
                                   ,CONVERT(VARCHAR(8000),Old.ClsSectionId,121)
                                   ,CONVERT(VARCHAR(8000),New.ClsSectionId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ClsSectMeetingId = New.ClsSectMeetingId
                            WHERE   Old.ClsSectionId <> New.ClsSectionId
                                    OR (
                                         Old.ClsSectionId IS NULL
                                         AND New.ClsSectionId IS NOT NULL
                                       )
                                    OR (
                                         New.ClsSectionId IS NULL
                                         AND Old.ClsSectionId IS NOT NULL
                                       ); 
                IF UPDATE(EndIntervalId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ClsSectMeetingId
                                   ,'EndIntervalId'
                                   ,CONVERT(VARCHAR(8000),Old.EndIntervalId,121)
                                   ,CONVERT(VARCHAR(8000),New.EndIntervalId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ClsSectMeetingId = New.ClsSectMeetingId
                            WHERE   Old.EndIntervalId <> New.EndIntervalId
                                    OR (
                                         Old.EndIntervalId IS NULL
                                         AND New.EndIntervalId IS NOT NULL
                                       )
                                    OR (
                                         New.EndIntervalId IS NULL
                                         AND Old.EndIntervalId IS NOT NULL
                                       ); 
                IF UPDATE(TimeIntervalId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ClsSectMeetingId
                                   ,'TimeIntervalId'
                                   ,CONVERT(VARCHAR(8000),Old.TimeIntervalId,121)
                                   ,CONVERT(VARCHAR(8000),New.TimeIntervalId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ClsSectMeetingId = New.ClsSectMeetingId
                            WHERE   Old.TimeIntervalId <> New.TimeIntervalId
                                    OR (
                                         Old.TimeIntervalId IS NULL
                                         AND New.TimeIntervalId IS NOT NULL
                                       )
                                    OR (
                                         New.TimeIntervalId IS NULL
                                         AND Old.TimeIntervalId IS NOT NULL
                                       ); 
                IF UPDATE(WorkDaysId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ClsSectMeetingId
                                   ,'WorkDaysId'
                                   ,CONVERT(VARCHAR(8000),Old.WorkDaysId,121)
                                   ,CONVERT(VARCHAR(8000),New.WorkDaysId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ClsSectMeetingId = New.ClsSectMeetingId
                            WHERE   Old.WorkDaysId <> New.WorkDaysId
                                    OR (
                                         Old.WorkDaysId IS NULL
                                         AND New.WorkDaysId IS NOT NULL
                                       )
                                    OR (
                                         New.WorkDaysId IS NULL
                                         AND Old.WorkDaysId IS NOT NULL
                                       ); 
            END; 
        END;



    SET NOCOUNT OFF;

GO
ALTER TABLE [dbo].[arClsSectMeetings] ADD CONSTRAINT [PK_arClsSectMeetings_ClsSectMeetingId] PRIMARY KEY CLUSTERED  ([ClsSectMeetingId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_arClsSectMeetings_ClsSectionId_StartDate_EndDate_PeriodId] ON [dbo].[arClsSectMeetings] ([ClsSectionId], [StartDate], [EndDate], [PeriodId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arClsSectMeetings] ADD CONSTRAINT [FK_arClsSectMeetings_arClassSections_ClsSectionId_ClsSectionId] FOREIGN KEY ([ClsSectionId]) REFERENCES [dbo].[arClassSections] ([ClsSectionId])
GO
ALTER TABLE [dbo].[arClsSectMeetings] ADD CONSTRAINT [FK_arClsSectMeetings_arInstructionType_InstructionTypeID_InstructionTypeId] FOREIGN KEY ([InstructionTypeID]) REFERENCES [dbo].[arInstructionType] ([InstructionTypeId])
GO
ALTER TABLE [dbo].[arClsSectMeetings] ADD CONSTRAINT [FK_arClsSectMeetings_arRooms_RoomId_RoomId] FOREIGN KEY ([RoomId]) REFERENCES [dbo].[arRooms] ([RoomId])
GO
ALTER TABLE [dbo].[arClsSectMeetings] ADD CONSTRAINT [FK_arClsSectMeetings_cmTimeInterval_EndIntervalId_TimeIntervalId] FOREIGN KEY ([EndIntervalId]) REFERENCES [dbo].[cmTimeInterval] ([TimeIntervalId])
GO
ALTER TABLE [dbo].[arClsSectMeetings] ADD CONSTRAINT [FK_arClsSectMeetings_cmTimeInterval_TimeIntervalId_TimeIntervalId] FOREIGN KEY ([TimeIntervalId]) REFERENCES [dbo].[cmTimeInterval] ([TimeIntervalId])
GO
ALTER TABLE [dbo].[arClsSectMeetings] ADD CONSTRAINT [FK_arClsSectMeetings_plWorkDays_WorkDaysId_WorkDaysId] FOREIGN KEY ([WorkDaysId]) REFERENCES [dbo].[plWorkDays] ([WorkDaysId])
GO
ALTER TABLE [dbo].[arClsSectMeetings] ADD CONSTRAINT [FK_arClsSectMeetings_syPeriods_PeriodId_PeriodId] FOREIGN KEY ([PeriodId]) REFERENCES [dbo].[syPeriods] ([PeriodId])
GO
