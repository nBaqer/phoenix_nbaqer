CREATE TABLE [dbo].[saRefunds]
(
[TransactionId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_saRefunds_TransactionId] DEFAULT (newid()),
[RefundTypeId] [int] NOT NULL,
[BankAcctId] [uniqueidentifier] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[FundSourceId] [uniqueidentifier] NULL,
[StudentAwardId] [uniqueidentifier] NULL,
[IsInstCharge] [bit] NOT NULL CONSTRAINT [DF_saRefunds_IsInstCharge] DEFAULT ((0)),
[AwardScheduleId] [uniqueidentifier] NULL,
[RefundAmount] [money] NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[saRefunds_Audit_Delete] ON [dbo].[saRefunds]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saRefunds','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'RefundTypeId'
                               ,CONVERT(VARCHAR(8000),Old.RefundTypeId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'StudentAwardId'
                               ,CONVERT(VARCHAR(8000),Old.StudentAwardId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'FundSourceId'
                               ,CONVERT(VARCHAR(8000),Old.FundSourceId,121)
                        FROM    Deleted Old; 
            END; 
        END;



    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[saRefunds_Audit_Insert] ON [dbo].[saRefunds]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saRefunds','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'RefundTypeId'
                               ,CONVERT(VARCHAR(8000),New.RefundTypeId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'StudentAwardId'
                               ,CONVERT(VARCHAR(8000),New.StudentAwardId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'FundSourceId'
                               ,CONVERT(VARCHAR(8000),New.FundSourceId,121)
                        FROM    Inserted New; 
            END; 
        END;



    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[saRefunds_Audit_Update] ON [dbo].[saRefunds]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saRefunds','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(RefundTypeId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TransactionId
                                   ,'RefundTypeId'
                                   ,CONVERT(VARCHAR(8000),Old.RefundTypeId,121)
                                   ,CONVERT(VARCHAR(8000),New.RefundTypeId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TransactionId = New.TransactionId
                            WHERE   Old.RefundTypeId <> New.RefundTypeId
                                    OR (
                                         Old.RefundTypeId IS NULL
                                         AND New.RefundTypeId IS NOT NULL
                                       )
                                    OR (
                                         New.RefundTypeId IS NULL
                                         AND Old.RefundTypeId IS NOT NULL
                                       ); 
                IF UPDATE(StudentAwardId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TransactionId
                                   ,'StudentAwardId'
                                   ,CONVERT(VARCHAR(8000),Old.StudentAwardId,121)
                                   ,CONVERT(VARCHAR(8000),New.StudentAwardId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TransactionId = New.TransactionId
                            WHERE   Old.StudentAwardId <> New.StudentAwardId
                                    OR (
                                         Old.StudentAwardId IS NULL
                                         AND New.StudentAwardId IS NOT NULL
                                       )
                                    OR (
                                         New.StudentAwardId IS NULL
                                         AND Old.StudentAwardId IS NOT NULL
                                       ); 
                IF UPDATE(FundSourceId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TransactionId
                                   ,'FundSourceId'
                                   ,CONVERT(VARCHAR(8000),Old.FundSourceId,121)
                                   ,CONVERT(VARCHAR(8000),New.FundSourceId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TransactionId = New.TransactionId
                            WHERE   Old.FundSourceId <> New.FundSourceId
                                    OR (
                                         Old.FundSourceId IS NULL
                                         AND New.FundSourceId IS NOT NULL
                                       )
                                    OR (
                                         New.FundSourceId IS NULL
                                         AND Old.FundSourceId IS NOT NULL
                                       ); 
            END; 
        END;



    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[saRefunds] ADD CONSTRAINT [PK_saRefunds_TransactionId] PRIMARY KEY CLUSTERED  ([TransactionId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saRefunds] ADD CONSTRAINT [FK_saRefunds_saBankAccounts_BankAcctId_BankAcctId] FOREIGN KEY ([BankAcctId]) REFERENCES [dbo].[saBankAccounts] ([BankAcctId])
GO
ALTER TABLE [dbo].[saRefunds] ADD CONSTRAINT [FK_saRefunds_saFundSources_FundSourceId_FundSourceId] FOREIGN KEY ([FundSourceId]) REFERENCES [dbo].[saFundSources] ([FundSourceId])
GO
ALTER TABLE [dbo].[saRefunds] ADD CONSTRAINT [FK_saRefunds_saTransactions_TransactionId_TransactionId] FOREIGN KEY ([TransactionId]) REFERENCES [dbo].[saTransactions] ([TransactionId])
GO
