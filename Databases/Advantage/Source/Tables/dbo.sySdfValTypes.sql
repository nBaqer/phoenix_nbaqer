CREATE TABLE [dbo].[sySdfValTypes]
(
[ValTypeId] [tinyint] NOT NULL,
[ValType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[sySdfValTypes] ADD CONSTRAINT [PK_sySdfValTypes_ValTypeId] PRIMARY KEY CLUSTERED  ([ValTypeId]) ON [PRIMARY]
GO
