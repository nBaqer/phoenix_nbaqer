CREATE TABLE [dbo].[arR2T4AdditionalInformation]
(
[AdditionalInfoId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_arR2T4AdditionalInformation_AdditionalInfoId] DEFAULT (newsequentialid()),
[TerminationId] [uniqueidentifier] NOT NULL,
[StepNo] [smallint] NULL,
[AdditionalInformtion] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDate] [datetime] NULL CONSTRAINT [DF_arR2T4AdditionalInformation_CreatedDate] DEFAULT (getdate()),
[CreatedById] [uniqueidentifier] NOT NULL,
[UpdatedDate] [datetime] NULL,
[UpdatedById] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arR2T4AdditionalInformation] ADD CONSTRAINT [PK_arR2T4AdditionalInformation_AdditionalInfoId] PRIMARY KEY CLUSTERED  ([AdditionalInfoId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arR2T4AdditionalInformation] ADD CONSTRAINT [FK_arR2T4AdditionalInformation_arR2T4TerminationDetails_TerminationId_TerminationId] FOREIGN KEY ([TerminationId]) REFERENCES [dbo].[arR2T4TerminationDetails] ([TerminationId])
GO
ALTER TABLE [dbo].[arR2T4AdditionalInformation] ADD CONSTRAINT [FK_arR2T4AdditionalInformation_syUsers_CreatedBy_UserId] FOREIGN KEY ([CreatedById]) REFERENCES [dbo].[syUsers] ([UserId])
GO
ALTER TABLE [dbo].[arR2T4AdditionalInformation] ADD CONSTRAINT [FK_arR2T4AdditionalInformation_syUsers_UpdatedBy_UserId] FOREIGN KEY ([UpdatedById]) REFERENCES [dbo].[syUsers] ([UserId])
GO
