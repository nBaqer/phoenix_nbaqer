CREATE TABLE [dbo].[adLeadExtraCurriculars]
(
[LeadExtraCurricularID] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[ExtraCurricularID] [uniqueidentifier] NULL,
[LeadID] [uniqueidentifier] NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
 
--========================================================================================== 
-- TRIGGER adLeadExtraCurriculars_Audit_Delete 
-- INSERT  add Audit History when delete records in adLeadExtraCurriculars 
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadExtraCurriculars_Audit_Delete] ON [dbo].[adLeadExtraCurriculars]
    FOR DELETE
AS
    BEGIN 
        SET NOCOUNT ON; 
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
        DECLARE @EventRows AS INT; 
        DECLARE @EventDate AS DATETIME; 
        DECLARE @UserName AS VARCHAR(50); 
	
        SET @AuditHistId = NEWID(); 
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Deleted
                         ); 
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Deleted
                         ); 
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Deleted
                        ); 
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadExtraCurriculars','D',@EventRows,@EventDate,@UserName; 
                -- ExtraCurricularID 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadExtraCurricularID
                               ,'ExtraCurricularID'
                               ,CONVERT(VARCHAR(MAX),Old.ExtraCurricularID)
                        FROM    Deleted AS Old; 
                -- LeadID 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadExtraCurricularID
                               ,'LeadID'
                               ,CONVERT(VARCHAR(MAX),Old.LeadID)
                        FROM    Deleted AS Old; 
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadExtraCurricularID
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                        FROM    Deleted AS Old; 
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadExtraCurricularID
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),Old.ModUser)
                        FROM    Deleted AS Old; 
            END; 
        SET NOCOUNT OFF; 
    END; 
--========================================================================================== 
-- END TRIGGER adLeadExtraCurriculars_Audit_Delete 
--========================================================================================== 
 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
 
--========================================================================================== 
-- TRIGGER adLeadExtraCurriculars_Audit_Insert 
-- INSERT  add Audit History when insert records in adLeadExtraCurriculars 
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadExtraCurriculars_Audit_Insert] ON [dbo].[adLeadExtraCurriculars]
    FOR INSERT
AS
    BEGIN 
        SET NOCOUNT ON; 
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
        DECLARE @EventRows AS INT; 
        DECLARE @EventDate AS DATETIME; 
        DECLARE @UserName AS VARCHAR(50); 
	
        SET @AuditHistId = NEWID(); 
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         ); 
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         ); 
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        ); 
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadExtraCurriculars','I',@EventRows,@EventDate,@UserName; 
                -- ExtraCurricularID 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadExtraCurricularID
                               ,'ExtraCurricularID'
                               ,CONVERT(VARCHAR(MAX),New.ExtraCurricularID)
                        FROM    Inserted AS New; 
                -- LeadID 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadExtraCurricularID
                               ,'LeadID'
                               ,CONVERT(VARCHAR(MAX),New.LeadID)
                        FROM    Inserted AS New; 
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadExtraCurricularID
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                        FROM    Inserted AS New; 
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadExtraCurricularID
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),New.ModUser)
                        FROM    Inserted AS New; 
            END; 
        SET NOCOUNT OFF; 
    END; 
--========================================================================================== 
-- END TRIGGER adLeadExtraCurriculars_Audit_Insert 
--========================================================================================== 
 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
--========================================================================================== 
-- TRIGGER adLeadExtraCurriculars_Audit_Update 
-- UPDATE  add Audit History when update fields in adLeadExtraCurriculars 
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadExtraCurriculars_Audit_Update] ON [dbo].[adLeadExtraCurriculars]
    FOR UPDATE
AS
    BEGIN 
        SET NOCOUNT ON; 
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
        DECLARE @EventRows AS INT; 
        DECLARE @EventDate AS DATETIME; 
        DECLARE @UserName AS VARCHAR(50); 
	
        SET @AuditHistId = NEWID(); 
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         ); 
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         ); 
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        ); 
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadExtraCurriculars','U',@EventRows,@EventDate,@UserName; 
                -- ExtraCurricularID 
                IF UPDATE(ExtraCurricularID)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadExtraCurricularID
                                       ,'ExtraCurricularID'
                                       ,CONVERT(VARCHAR(MAX),Old.ExtraCurricularID)
                                       ,CONVERT(VARCHAR(MAX),New.ExtraCurricularID)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadExtraCurricularID = New.LeadExtraCurricularID
                                WHERE   Old.ExtraCurricularID <> New.ExtraCurricularID
                                        OR (
                                             Old.ExtraCurricularID IS NULL
                                             AND New.ExtraCurricularID IS NOT NULL
                                           )
                                        OR (
                                             New.ExtraCurricularID IS NULL
                                             AND Old.ExtraCurricularID IS NOT NULL
                                           ); 
                    END; 
                -- LeadID 
                IF UPDATE(LeadID)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadExtraCurricularID
                                       ,'LeadID'
                                       ,CONVERT(VARCHAR(MAX),Old.LeadID)
                                       ,CONVERT(VARCHAR(MAX),New.LeadID)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadExtraCurricularID = New.LeadExtraCurricularID
                                WHERE   Old.LeadID <> New.LeadID
                                        OR (
                                             Old.LeadID IS NULL
                                             AND New.LeadID IS NOT NULL
                                           )
                                        OR (
                                             New.LeadID IS NULL
                                             AND Old.LeadID IS NOT NULL
                                           ); 
                    END; 
                -- ModDate 
                IF UPDATE(ModDate)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadExtraCurricularID
                                       ,'ModDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadExtraCurricularID = New.LeadExtraCurricularID
                                WHERE   Old.ModDate <> New.ModDate
                                        OR (
                                             Old.ModDate IS NULL
                                             AND New.ModDate IS NOT NULL
                                           )
                                        OR (
                                             New.ModDate IS NULL
                                             AND Old.ModDate IS NOT NULL
                                           ); 
                    END; 
                -- ModUser 
                IF UPDATE(ModUser)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadExtraCurricularID
                                       ,'ModUser'
                                       ,CONVERT(VARCHAR(MAX),Old.ModUser)
                                       ,CONVERT(VARCHAR(MAX),New.ModUser)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadExtraCurricularID = New.LeadExtraCurricularID
                                WHERE   Old.ModUser <> New.ModUser
                                        OR (
                                             Old.ModUser IS NULL
                                             AND New.ModUser IS NOT NULL
                                           )
                                        OR (
                                             New.ModUser IS NULL
                                             AND Old.ModUser IS NOT NULL
                                           ); 
                    END; 
            END; 
        SET NOCOUNT OFF; 
    END; 
--========================================================================================== 
-- END TRIGGER adLeadExtraCurriculars_Audit_Update 
--========================================================================================== 
 
GO
ALTER TABLE [dbo].[adLeadExtraCurriculars] ADD CONSTRAINT [PK_adLeadExtraCurriculars_LeadExtraCurricularID] PRIMARY KEY CLUSTERED  ([LeadExtraCurricularID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adLeadExtraCurriculars] ADD CONSTRAINT [FK_adLeadExtraCurriculars_adLeads_LeadID_LeadId] FOREIGN KEY ([LeadID]) REFERENCES [dbo].[adLeads] ([LeadId])
GO
