CREATE TABLE [dbo].[saFundSources]
(
[FundSourceId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_saFundSources_FundSourceId] DEFAULT (newid()),
[FundSourceCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[FundSourceDescrip] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[TitleIV] [bit] NULL CONSTRAINT [DF_saFundSources_TitleIV] DEFAULT ((0)),
[AwardTypeId] [int] NULL,
[AwardYear] [char] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdvFundSourceId] [tinyint] NULL,
[CutoffDate] [datetime] NULL,
[IPEDSValue] [int] NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saFundSources_Audit_Delete] ON [dbo].[saFundSources]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saFundSources','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.FundSourceId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.FundSourceId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.FundSourceId
                               ,'FundSourceCode'
                               ,CONVERT(VARCHAR(8000),Old.FundSourceCode,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.FundSourceId
                               ,'FundSourceDescrip'
                               ,CONVERT(VARCHAR(8000),Old.FundSourceDescrip,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saFundSources_Audit_Insert] ON [dbo].[saFundSources]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saFundSources','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.FundSourceId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.FundSourceId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.FundSourceId
                               ,'FundSourceCode'
                               ,CONVERT(VARCHAR(8000),New.FundSourceCode,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.FundSourceId
                               ,'FundSourceDescrip'
                               ,CONVERT(VARCHAR(8000),New.FundSourceDescrip,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saFundSources_Audit_Update] ON [dbo].[saFundSources]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saFundSources','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.FundSourceId
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.FundSourceId = New.FundSourceId
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.FundSourceId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.FundSourceId = New.FundSourceId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(FundSourceCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.FundSourceId
                                   ,'FundSourceCode'
                                   ,CONVERT(VARCHAR(8000),Old.FundSourceCode,121)
                                   ,CONVERT(VARCHAR(8000),New.FundSourceCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.FundSourceId = New.FundSourceId
                            WHERE   Old.FundSourceCode <> New.FundSourceCode
                                    OR (
                                         Old.FundSourceCode IS NULL
                                         AND New.FundSourceCode IS NOT NULL
                                       )
                                    OR (
                                         New.FundSourceCode IS NULL
                                         AND Old.FundSourceCode IS NOT NULL
                                       ); 
                IF UPDATE(FundSourceDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.FundSourceId
                                   ,'FundSourceDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.FundSourceDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.FundSourceDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.FundSourceId = New.FundSourceId
                            WHERE   Old.FundSourceDescrip <> New.FundSourceDescrip
                                    OR (
                                         Old.FundSourceDescrip IS NULL
                                         AND New.FundSourceDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.FundSourceDescrip IS NULL
                                         AND Old.FundSourceDescrip IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[saFundSources] ADD CONSTRAINT [PK_saFundSources_FundSourceId] PRIMARY KEY CLUSTERED  ([FundSourceId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_saFundSources_FundSourceCode_FundSourceDescrip_CampGrpId] ON [dbo].[saFundSources] ([FundSourceCode], [FundSourceDescrip], [CampGrpId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saFundSources] ADD CONSTRAINT [FK_saFundSources_syAdvFundSources_AdvFundSourceId_AdvFundSourceId] FOREIGN KEY ([AdvFundSourceId]) REFERENCES [dbo].[syAdvFundSources] ([AdvFundSourceId])
GO
ALTER TABLE [dbo].[saFundSources] ADD CONSTRAINT [FK_saFundSources_saAwardTypes_AwardTypeId_AwardTypeId] FOREIGN KEY ([AwardTypeId]) REFERENCES [dbo].[saAwardTypes] ([AwardTypeId])
GO
ALTER TABLE [dbo].[saFundSources] ADD CONSTRAINT [FK_saFundSources_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[saFundSources] ADD CONSTRAINT [FK_saFundSources_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
