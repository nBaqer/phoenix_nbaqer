CREATE TABLE [dbo].[msgGroups]
(
[GroupId] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[Code] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CampGroupId] [uniqueidentifier] NULL,
[ModDate] [datetime] NULL,
[ModUser] [uniqueidentifier] NULL,
[Active] [tinyint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[msgGroups] ADD CONSTRAINT [PK_msgGroups_GroupId] PRIMARY KEY CLUSTERED  ([GroupId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_msgGroups_Code_Descrip_CampGroupId] ON [dbo].[msgGroups] ([Code], [Descrip], [CampGroupId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[msgGroups] ADD CONSTRAINT [FK_msgGroups_syCampGrps_CampGroupId_CampGrpId] FOREIGN KEY ([CampGroupId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
