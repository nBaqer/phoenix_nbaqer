CREATE TABLE [dbo].[faStuPaymentPlanSchedule]
(
[PayPlanScheduleId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_faStuPaymentPlanSchedule_PayPlanScheduleId] DEFAULT (newid()),
[PaymentPlanId] [uniqueidentifier] NULL,
[ExpectedDate] [datetime] NULL,
[Amount] [decimal] (19, 4) NULL,
[Reference] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransactionId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[faStuPaymentPlanSchedule_Audit_Delete] ON [dbo].[faStuPaymentPlanSchedule]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 

    DECLARE @stuEnrollId AS UNIQUEIDENTIFIER;

    DECLARE @PaymentPlanId AS UNIQUEIDENTIFIER; 
 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 

    SET @PaymentPlanId = (
                           SELECT TOP 1
                                    PaymentPlanId
                           FROM     Deleted
                         );

    SET @stuEnrollId = (
                         SELECT TOP 1
                                StuEnrollId
                         FROM   faStudentPaymentPlans
                         WHERE  PaymentPlanId = @PaymentPlanId
                       ); 
 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'faStuPaymentPlanSchedule','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PayPlanScheduleId
                               ,'Amount'
                               ,CONVERT(VARCHAR(8000),Old.Amount,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PayPlanScheduleId
                               ,'PaymentPlanId'
                               ,CONVERT(VARCHAR(8000),Old.PaymentPlanId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PayPlanScheduleId
                               ,'ExpectedDate'
                               ,CONVERT(VARCHAR(8000),Old.ExpectedDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PayPlanScheduleId
                               ,'Reference'
                               ,CONVERT(VARCHAR(8000),Old.Reference,121)
                        FROM    Deleted Old; 
            END; 
--EXEC dbo.USP_AR_StuEnrollPayPeriods @stuEnrollId
        END; 



    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[faStuPaymentPlanSchedule_Audit_Insert] ON [dbo].[faStuPaymentPlanSchedule]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 

    DECLARE @stuEnrollId AS UNIQUEIDENTIFIER;

    DECLARE @PaymentPlanId AS UNIQUEIDENTIFIER; 
  
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 

    SET @PaymentPlanId = (
                           SELECT TOP 1
                                    PaymentPlanId
                           FROM     Inserted
                         );

    SET @stuEnrollId = (
                         SELECT TOP 1
                                StuEnrollId
                         FROM   faStudentPaymentPlans
                         WHERE  PaymentPlanId = @PaymentPlanId
                       ); 
  
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'faStuPaymentPlanSchedule','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PayPlanScheduleId
                               ,'Amount'
                               ,CONVERT(VARCHAR(8000),New.Amount,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PayPlanScheduleId
                               ,'PaymentPlanId'
                               ,CONVERT(VARCHAR(8000),New.PaymentPlanId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PayPlanScheduleId
                               ,'ExpectedDate'
                               ,CONVERT(VARCHAR(8000),New.ExpectedDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PayPlanScheduleId
                               ,'Reference'
                               ,CONVERT(VARCHAR(8000),New.Reference,121)
                        FROM    Inserted New; 
            END; 
--EXEC dbo.USP_AR_StuEnrollPayPeriods @stuEnrollId
        END; 



    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[faStuPaymentPlanSchedule_Audit_Update] ON [dbo].[faStuPaymentPlanSchedule]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 

    DECLARE @stuEnrollId AS UNIQUEIDENTIFIER;

    DECLARE @PaymentPlanId AS UNIQUEIDENTIFIER; 
 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 

    SET @PaymentPlanId = (
                           SELECT TOP 1
                                    PaymentPlanId
                           FROM     Inserted
                         );

    SET @stuEnrollId = (
                         SELECT TOP 1
                                StuEnrollId
                         FROM   faStudentPaymentPlans
                         WHERE  PaymentPlanId = @PaymentPlanId
                       ); 
  
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'faStuPaymentPlanSchedule','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(Amount)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PayPlanScheduleId
                                   ,'Amount'
                                   ,CONVERT(VARCHAR(8000),Old.Amount,121)
                                   ,CONVERT(VARCHAR(8000),New.Amount,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PayPlanScheduleId = New.PayPlanScheduleId
                            WHERE   Old.Amount <> New.Amount
                                    OR (
                                         Old.Amount IS NULL
                                         AND New.Amount IS NOT NULL
                                       )
                                    OR (
                                         New.Amount IS NULL
                                         AND Old.Amount IS NOT NULL
                                       ); 
                IF UPDATE(PaymentPlanId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PayPlanScheduleId
                                   ,'PaymentPlanId'
                                   ,CONVERT(VARCHAR(8000),Old.PaymentPlanId,121)
                                   ,CONVERT(VARCHAR(8000),New.PaymentPlanId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PayPlanScheduleId = New.PayPlanScheduleId
                            WHERE   Old.PaymentPlanId <> New.PaymentPlanId
                                    OR (
                                         Old.PaymentPlanId IS NULL
                                         AND New.PaymentPlanId IS NOT NULL
                                       )
                                    OR (
                                         New.PaymentPlanId IS NULL
                                         AND Old.PaymentPlanId IS NOT NULL
                                       ); 
                IF UPDATE(ExpectedDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PayPlanScheduleId
                                   ,'ExpectedDate'
                                   ,CONVERT(VARCHAR(8000),Old.ExpectedDate,121)
                                   ,CONVERT(VARCHAR(8000),New.ExpectedDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PayPlanScheduleId = New.PayPlanScheduleId
                            WHERE   Old.ExpectedDate <> New.ExpectedDate
                                    OR (
                                         Old.ExpectedDate IS NULL
                                         AND New.ExpectedDate IS NOT NULL
                                       )
                                    OR (
                                         New.ExpectedDate IS NULL
                                         AND Old.ExpectedDate IS NOT NULL
                                       ); 
                IF UPDATE(Reference)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PayPlanScheduleId
                                   ,'Reference'
                                   ,CONVERT(VARCHAR(8000),Old.Reference,121)
                                   ,CONVERT(VARCHAR(8000),New.Reference,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PayPlanScheduleId = New.PayPlanScheduleId
                            WHERE   Old.Reference <> New.Reference
                                    OR (
                                         Old.Reference IS NULL
                                         AND New.Reference IS NOT NULL
                                       )
                                    OR (
                                         New.Reference IS NULL
                                         AND Old.Reference IS NOT NULL
                                       ); 
            END; 
--EXEC dbo.USP_AR_StuEnrollPayPeriods @stuEnrollId
        END; 



    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[faStuPaymentPlanSchedule] ADD CONSTRAINT [PK_faStuPaymentPlanSchedule_PayPlanScheduleId] PRIMARY KEY CLUSTERED  ([PayPlanScheduleId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[faStuPaymentPlanSchedule] ADD CONSTRAINT [FK_faStuPaymentPlanSchedule_faStudentPaymentPlans_PaymentPlanId_PaymentPlanId] FOREIGN KEY ([PaymentPlanId]) REFERENCES [dbo].[faStudentPaymentPlans] ([PaymentPlanId])
GO
