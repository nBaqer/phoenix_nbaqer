CREATE TABLE [dbo].[syWapiExternalCompanies]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsActive] [bit] NOT NULL CONSTRAINT [DF_syWapiExternalCompanies_IsActive] DEFAULT ((1))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syWapiExternalCompanies] ADD CONSTRAINT [PK_syWapiExternalCompanies_Id] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_syWapiExternalCompanies_Code] ON [dbo].[syWapiExternalCompanies] ([Code]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_syWapiExternalCompanies_Description] ON [dbo].[syWapiExternalCompanies] ([Description]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', 'this table store the code and name of the companies that used the web api.
Example:
FAME
VOYANT', 'SCHEMA', N'dbo', 'TABLE', N'syWapiExternalCompanies', NULL, NULL
GO
