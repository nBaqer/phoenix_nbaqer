CREATE TABLE [dbo].[syMenuItemType]
(
[MenuItemTypeId] [smallint] NOT NULL,
[MenuItemType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syMenuItemType] ADD CONSTRAINT [PK_syMenuItemType_MenuItemTypeId] PRIMARY KEY CLUSTERED  ([MenuItemTypeId]) ON [PRIMARY]
GO
