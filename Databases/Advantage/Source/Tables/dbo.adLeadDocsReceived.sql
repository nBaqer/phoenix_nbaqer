CREATE TABLE [dbo].[adLeadDocsReceived]
(
[LeadDocId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_adLeadDocsReceived_LeadDocId] DEFAULT (newid()),
[LeadId] [uniqueidentifier] NOT NULL,
[DocumentId] [uniqueidentifier] NOT NULL,
[ReceiveDate] [datetime] NULL,
[DocStatusId] [uniqueidentifier] NULL,
[ModDate] [datetime] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Override] [bit] NOT NULL CONSTRAINT [DF_adLeadDocsReceived_Override] DEFAULT ((0)),
[OverrideReason] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ApprovalDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
 
--========================================================================================== 
-- TRIGGER adLeadDocsReceived_Audit_Delete 
-- INSERT  add Audit History when delete records in adLeadDocsReceived 
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadDocsReceived_Audit_Delete] ON [dbo].[adLeadDocsReceived]
    FOR DELETE
AS
    BEGIN 
        SET NOCOUNT ON; 
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
        DECLARE @EventRows AS INT; 
        DECLARE @EventDate AS DATETIME; 
        DECLARE @UserName AS VARCHAR(50); 
	
        SET @AuditHistId = NEWID(); 
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Deleted
                         ); 
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Deleted
                         ); 
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Deleted
                        ); 
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadDocsReceived','D',@EventRows,@EventDate,@UserName; 
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadDocId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),Old.LeadId)
                        FROM    Deleted AS Old; 
                -- DocumentId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadDocId
                               ,'DocumentId'
                               ,CONVERT(VARCHAR(MAX),Old.DocumentId)
                        FROM    Deleted AS Old; 
                -- ReceiveDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadDocId
                               ,'ReceiveDate'
                               ,CONVERT(VARCHAR(MAX),Old.ReceiveDate,121)
                        FROM    Deleted AS Old; 
                -- DocStatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadDocId
                               ,'DocStatusId'
                               ,CONVERT(VARCHAR(MAX),Old.DocStatusId)
                        FROM    Deleted AS Old; 
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadDocId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                        FROM    Deleted AS Old; 
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadDocId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),Old.ModUser)
                        FROM    Deleted AS Old; 
                -- Override 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadDocId
                               ,'Override'
                               ,CONVERT(VARCHAR(MAX),Old.Override)
                        FROM    Deleted AS Old; 
                -- OverrideReason 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadDocId
                               ,'OverrideReason'
                               ,CONVERT(VARCHAR(MAX),Old.OverrideReason)
                        FROM    Deleted AS Old; 
                -- ApprovalDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadDocId
                               ,'ApprovalDate'
                               ,CONVERT(VARCHAR(MAX),Old.ApprovalDate,121)
                        FROM    Deleted AS Old; 
            END; 
        SET NOCOUNT OFF; 
    END; 
--========================================================================================== 
-- END TRIGGER adLeadDocsReceived_Audit_Delete 
--========================================================================================== 
 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
 
--========================================================================================== 
-- TRIGGER adLeadDocsReceived_Audit_Insert 
-- INSERT  add Audit History when insert records in adLeadDocsReceived 
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadDocsReceived_Audit_Insert] ON [dbo].[adLeadDocsReceived]
    FOR INSERT
AS
    BEGIN 
        SET NOCOUNT ON; 
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
        DECLARE @EventRows AS INT; 
        DECLARE @EventDate AS DATETIME; 
        DECLARE @UserName AS VARCHAR(50); 
	
        SET @AuditHistId = NEWID(); 
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         ); 
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         ); 
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        ); 
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadDocsReceived','I',@EventRows,@EventDate,@UserName; 
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadDocId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),New.LeadId)
                        FROM    Inserted AS New; 
                -- DocumentId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadDocId
                               ,'DocumentId'
                               ,CONVERT(VARCHAR(MAX),New.DocumentId)
                        FROM    Inserted AS New; 
                -- ReceiveDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadDocId
                               ,'ReceiveDate'
                               ,CONVERT(VARCHAR(MAX),New.ReceiveDate,121)
                        FROM    Inserted AS New; 
                -- DocStatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadDocId
                               ,'DocStatusId'
                               ,CONVERT(VARCHAR(MAX),New.DocStatusId)
                        FROM    Inserted AS New; 
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadDocId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                        FROM    Inserted AS New; 
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadDocId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),New.ModUser)
                        FROM    Inserted AS New; 
                -- Override 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadDocId
                               ,'Override'
                               ,CONVERT(VARCHAR(MAX),New.Override)
                        FROM    Inserted AS New; 
                -- OverrideReason 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadDocId
                               ,'OverrideReason'
                               ,CONVERT(VARCHAR(MAX),New.OverrideReason)
                        FROM    Inserted AS New; 
                -- ApprovalDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadDocId
                               ,'ApprovalDate'
                               ,CONVERT(VARCHAR(MAX),New.ApprovalDate,121)
                        FROM    Inserted AS New; 
            END; 
        SET NOCOUNT OFF; 
    END; 
--========================================================================================== 
-- END TRIGGER adLeadDocsReceived_Audit_Insert 
--========================================================================================== 
 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
--========================================================================================== 
-- TRIGGER adLeadDocsReceived_Audit_Update 
-- UPDATE  add Audit History when update fields in adLeadDocsReceived 
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadDocsReceived_Audit_Update] ON [dbo].[adLeadDocsReceived]
    FOR UPDATE
AS
    BEGIN 
        SET NOCOUNT ON; 
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
        DECLARE @EventRows AS INT; 
        DECLARE @EventDate AS DATETIME; 
        DECLARE @UserName AS VARCHAR(50); 
	
        SET @AuditHistId = NEWID(); 
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         ); 
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         ); 
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        ); 
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadDocsReceived','U',@EventRows,@EventDate,@UserName; 
                -- LeadId 
                IF UPDATE(LeadId)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadDocId
                                       ,'LeadId'
                                       ,CONVERT(VARCHAR(MAX),Old.LeadId)
                                       ,CONVERT(VARCHAR(MAX),New.LeadId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadDocId = New.LeadDocId
                                WHERE   Old.LeadId <> New.LeadId
                                        OR (
                                             Old.LeadId IS NULL
                                             AND New.LeadId IS NOT NULL
                                           )
                                        OR (
                                             New.LeadId IS NULL
                                             AND Old.LeadId IS NOT NULL
                                           ); 
                    END; 
                -- DocumentId 
                IF UPDATE(DocumentId)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadDocId
                                       ,'DocumentId'
                                       ,CONVERT(VARCHAR(MAX),Old.DocumentId)
                                       ,CONVERT(VARCHAR(MAX),New.DocumentId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadDocId = New.LeadDocId
                                WHERE   Old.DocumentId <> New.DocumentId
                                        OR (
                                             Old.DocumentId IS NULL
                                             AND New.DocumentId IS NOT NULL
                                           )
                                        OR (
                                             New.DocumentId IS NULL
                                             AND Old.DocumentId IS NOT NULL
                                           ); 
                    END; 
                -- ReceiveDate 
                IF UPDATE(ReceiveDate)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadDocId
                                       ,'ReceiveDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ReceiveDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ReceiveDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadDocId = New.LeadDocId
                                WHERE   Old.ReceiveDate <> New.ReceiveDate
                                        OR (
                                             Old.ReceiveDate IS NULL
                                             AND New.ReceiveDate IS NOT NULL
                                           )
                                        OR (
                                             New.ReceiveDate IS NULL
                                             AND Old.ReceiveDate IS NOT NULL
                                           ); 
                    END; 
                -- DocStatusId 
                IF UPDATE(DocStatusId)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadDocId
                                       ,'DocStatusId'
                                       ,CONVERT(VARCHAR(MAX),Old.DocStatusId)
                                       ,CONVERT(VARCHAR(MAX),New.DocStatusId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadDocId = New.LeadDocId
                                WHERE   Old.DocStatusId <> New.DocStatusId
                                        OR (
                                             Old.DocStatusId IS NULL
                                             AND New.DocStatusId IS NOT NULL
                                           )
                                        OR (
                                             New.DocStatusId IS NULL
                                             AND Old.DocStatusId IS NOT NULL
                                           ); 
                    END; 
                -- ModDate 
                IF UPDATE(ModDate)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadDocId
                                       ,'ModDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadDocId = New.LeadDocId
                                WHERE   Old.ModDate <> New.ModDate
                                        OR (
                                             Old.ModDate IS NULL
                                             AND New.ModDate IS NOT NULL
                                           )
                                        OR (
                                             New.ModDate IS NULL
                                             AND Old.ModDate IS NOT NULL
                                           ); 
                    END; 
                -- ModUser 
                IF UPDATE(ModUser)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadDocId
                                       ,'ModUser'
                                       ,CONVERT(VARCHAR(MAX),Old.ModUser)
                                       ,CONVERT(VARCHAR(MAX),New.ModUser)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadDocId = New.LeadDocId
                                WHERE   Old.ModUser <> New.ModUser
                                        OR (
                                             Old.ModUser IS NULL
                                             AND New.ModUser IS NOT NULL
                                           )
                                        OR (
                                             New.ModUser IS NULL
                                             AND Old.ModUser IS NOT NULL
                                           ); 
                    END; 
                -- Override 
                IF UPDATE(Override)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadDocId
                                       ,'Override'
                                       ,CONVERT(VARCHAR(MAX),Old.Override)
                                       ,CONVERT(VARCHAR(MAX),New.Override)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadDocId = New.LeadDocId
                                WHERE   Old.Override <> New.Override
                                        OR (
                                             Old.Override IS NULL
                                             AND New.Override IS NOT NULL
                                           )
                                        OR (
                                             New.Override IS NULL
                                             AND Old.Override IS NOT NULL
                                           ); 
                    END; 
                -- OverrideReason 
                IF UPDATE(OverrideReason)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadDocId
                                       ,'OverrideReason'
                                       ,CONVERT(VARCHAR(MAX),Old.OverrideReason)
                                       ,CONVERT(VARCHAR(MAX),New.OverrideReason)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadDocId = New.LeadDocId
                                WHERE   Old.OverrideReason <> New.OverrideReason
                                        OR (
                                             Old.OverrideReason IS NULL
                                             AND New.OverrideReason IS NOT NULL
                                           )
                                        OR (
                                             New.OverrideReason IS NULL
                                             AND Old.OverrideReason IS NOT NULL
                                           ); 
                    END; 
                -- ApprovalDate 
                IF UPDATE(ApprovalDate)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadDocId
                                       ,'ApprovalDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ApprovalDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ApprovalDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadDocId = New.LeadDocId
                                WHERE   Old.ApprovalDate <> New.ApprovalDate
                                        OR (
                                             Old.ApprovalDate IS NULL
                                             AND New.ApprovalDate IS NOT NULL
                                           )
                                        OR (
                                             New.ApprovalDate IS NULL
                                             AND Old.ApprovalDate IS NOT NULL
                                           ); 
                    END; 
            END; 
        SET NOCOUNT OFF; 
    END; 
--========================================================================================== 
-- END TRIGGER adLeadDocsReceived_Audit_Update 
--========================================================================================== 
 
GO
ALTER TABLE [dbo].[adLeadDocsReceived] ADD CONSTRAINT [PK_adLeadDocsReceived_LeadDocId] PRIMARY KEY CLUSTERED  ([LeadDocId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_adLeadDocsReceived_LeadId_DocumentId] ON [dbo].[adLeadDocsReceived] ([LeadId], [DocumentId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adLeadDocsReceived] ADD CONSTRAINT [FK_adLeadDocsReceived_adLeads_LeadId_LeadId] FOREIGN KEY ([LeadId]) REFERENCES [dbo].[adLeads] ([LeadId])
GO
ALTER TABLE [dbo].[adLeadDocsReceived] ADD CONSTRAINT [FK_adLeadDocsReceived_adReqs_DocumentId_adReqId] FOREIGN KEY ([DocumentId]) REFERENCES [dbo].[adReqs] ([adReqId])
GO
ALTER TABLE [dbo].[adLeadDocsReceived] ADD CONSTRAINT [FK_adLeadDocsReceived_syDocStatuses_DocStatusId_DocStatusId] FOREIGN KEY ([DocStatusId]) REFERENCES [dbo].[syDocStatuses] ([DocStatusId])
GO
