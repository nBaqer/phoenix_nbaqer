CREATE TABLE [dbo].[plCorpHQs]
(
[CorpHQId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_plCorpHQs_CorpHQId] DEFAULT (newid()),
[CorpHQCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[CorpHQName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Address1] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Address2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StateId] [uniqueidentifier] NOT NULL,
[Zip] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[plCorpHQs_Audit_Delete] ON [dbo].[plCorpHQs]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'plCorpHQs','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CorpHQId
                               ,'StateId'
                               ,CONVERT(VARCHAR(8000),Old.StateId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CorpHQId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CorpHQId
                               ,'Address1'
                               ,CONVERT(VARCHAR(8000),Old.Address1,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CorpHQId
                               ,'Address2'
                               ,CONVERT(VARCHAR(8000),Old.Address2,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CorpHQId
                               ,'City'
                               ,CONVERT(VARCHAR(8000),Old.City,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CorpHQId
                               ,'CorpHQCode'
                               ,CONVERT(VARCHAR(8000),Old.CorpHQCode,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CorpHQId
                               ,'Zip'
                               ,CONVERT(VARCHAR(8000),Old.Zip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CorpHQId
                               ,'CorpHQName'
                               ,CONVERT(VARCHAR(8000),Old.CorpHQName,121)
                        FROM    Deleted Old; 
            END; 
        END;



    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[plCorpHQs_Audit_Insert] ON [dbo].[plCorpHQs]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'plCorpHQs','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CorpHQId
                               ,'StateId'
                               ,CONVERT(VARCHAR(8000),New.StateId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CorpHQId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CorpHQId
                               ,'Address1'
                               ,CONVERT(VARCHAR(8000),New.Address1,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CorpHQId
                               ,'Address2'
                               ,CONVERT(VARCHAR(8000),New.Address2,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CorpHQId
                               ,'City'
                               ,CONVERT(VARCHAR(8000),New.City,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CorpHQId
                               ,'CorpHQCode'
                               ,CONVERT(VARCHAR(8000),New.CorpHQCode,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CorpHQId
                               ,'Zip'
                               ,CONVERT(VARCHAR(8000),New.Zip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CorpHQId
                               ,'CorpHQName'
                               ,CONVERT(VARCHAR(8000),New.CorpHQName,121)
                        FROM    Inserted New; 
            END; 
        END;



    SET NOCOUNT OFF; 
GO
GO

GO

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[plCorpHQs_Audit_Update] ON [dbo].[plCorpHQs]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'plCorpHQs','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(StateId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CorpHQId
                                   ,'StateId'
                                   ,CONVERT(VARCHAR(8000),Old.StateId,121)
                                   ,CONVERT(VARCHAR(8000),New.StateId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CorpHQId = New.CorpHQId
                            WHERE   Old.StateId <> New.StateId
                                    OR (
                                         Old.StateId IS NULL
                                         AND New.StateId IS NOT NULL
                                       )
                                    OR (
                                         New.StateId IS NULL
                                         AND Old.StateId IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CorpHQId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CorpHQId = New.CorpHQId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(Address1)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CorpHQId
                                   ,'Address1'
                                   ,CONVERT(VARCHAR(8000),Old.Address1,121)
                                   ,CONVERT(VARCHAR(8000),New.Address1,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CorpHQId = New.CorpHQId
                            WHERE   Old.Address1 <> New.Address1
                                    OR (
                                         Old.Address1 IS NULL
                                         AND New.Address1 IS NOT NULL
                                       )
                                    OR (
                                         New.Address1 IS NULL
                                         AND Old.Address1 IS NOT NULL
                                       ); 
                IF UPDATE(Address2)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CorpHQId
                                   ,'Address2'
                                   ,CONVERT(VARCHAR(8000),Old.Address2,121)
                                   ,CONVERT(VARCHAR(8000),New.Address2,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CorpHQId = New.CorpHQId
                            WHERE   Old.Address2 <> New.Address2
                                    OR (
                                         Old.Address2 IS NULL
                                         AND New.Address2 IS NOT NULL
                                       )
                                    OR (
                                         New.Address2 IS NULL
                                         AND Old.Address2 IS NOT NULL
                                       ); 
                IF UPDATE(City)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CorpHQId
                                   ,'City'
                                   ,CONVERT(VARCHAR(8000),Old.City,121)
                                   ,CONVERT(VARCHAR(8000),New.City,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CorpHQId = New.CorpHQId
                            WHERE   Old.City <> New.City
                                    OR (
                                         Old.City IS NULL
                                         AND New.City IS NOT NULL
                                       )
                                    OR (
                                         New.City IS NULL
                                         AND Old.City IS NOT NULL
                                       ); 
                IF UPDATE(CorpHQCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CorpHQId
                                   ,'CorpHQCode'
                                   ,CONVERT(VARCHAR(8000),Old.CorpHQCode,121)
                                   ,CONVERT(VARCHAR(8000),New.CorpHQCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CorpHQId = New.CorpHQId
                            WHERE   Old.CorpHQCode <> New.CorpHQCode
                                    OR (
                                         Old.CorpHQCode IS NULL
                                         AND New.CorpHQCode IS NOT NULL
                                       )
                                    OR (
                                         New.CorpHQCode IS NULL
                                         AND Old.CorpHQCode IS NOT NULL
                                       ); 
                IF UPDATE(Zip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CorpHQId
                                   ,'Zip'
                                   ,CONVERT(VARCHAR(8000),Old.Zip,121)
                                   ,CONVERT(VARCHAR(8000),New.Zip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CorpHQId = New.CorpHQId
                            WHERE   Old.Zip <> New.Zip
                                    OR (
                                         Old.Zip IS NULL
                                         AND New.Zip IS NOT NULL
                                       )
                                    OR (
                                         New.Zip IS NULL
                                         AND Old.Zip IS NOT NULL
                                       ); 
                IF UPDATE(CorpHQName)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CorpHQId
                                   ,'CorpHQName'
                                   ,CONVERT(VARCHAR(8000),Old.CorpHQName,121)
                                   ,CONVERT(VARCHAR(8000),New.CorpHQName,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CorpHQId = New.CorpHQId
                            WHERE   Old.CorpHQName <> New.CorpHQName
                                    OR (
                                         Old.CorpHQName IS NULL
                                         AND New.CorpHQName IS NOT NULL
                                       )
                                    OR (
                                         New.CorpHQName IS NULL
                                         AND Old.CorpHQName IS NOT NULL
                                       ); 
            END; 
        END;



    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[plCorpHQs] ADD CONSTRAINT [PK_plCorpHQs_CorpHQId] PRIMARY KEY CLUSTERED  ([CorpHQId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[plCorpHQs] ADD CONSTRAINT [FK_plCorpHQs_syStates_StateId_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[syStates] ([StateId])
GO
