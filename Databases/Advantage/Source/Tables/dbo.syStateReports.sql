CREATE TABLE [dbo].[syStateReports]
(
[StateReportId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_syStateReports_StateReportId] DEFAULT (newsequentialid()),
[StateId] [uniqueidentifier] NOT NULL,
[ReportId] [uniqueidentifier] NOT NULL,
[ModifiedUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModifiedDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syStateReports] ADD CONSTRAINT [PK_syStateReports_StateReportId] PRIMARY KEY CLUSTERED  ([StateReportId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syStateReports] ADD CONSTRAINT [FK_syStateReports_syReports_ReportId_ReportId] FOREIGN KEY ([ReportId]) REFERENCES [dbo].[syReports] ([ReportId])
GO
ALTER TABLE [dbo].[syStateReports] ADD CONSTRAINT [FK_syStateReports_syStates_StateId_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[syStates] ([StateId])
GO
