CREATE TABLE [dbo].[adLeadTransactions]
(
[TransactionId] [uniqueidentifier] NOT NULL,
[LeadId] [uniqueidentifier] NOT NULL,
[TransCodeId] [uniqueidentifier] NOT NULL,
[TransReference] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TransAmount] [decimal] (19, 4) NOT NULL,
[TransDate] [datetime] NOT NULL,
[TransTypeId] [int] NOT NULL,
[isEnrolled] [bit] NULL,
[CreatedDate] [datetime] NOT NULL,
[Voided] [bit] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NULL,
[CampusId] [uniqueidentifier] NULL,
[ReversalReason] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisplaySequence] [int] NULL,
[SecondDisplaySequence] [int] NULL,
[MapTransactionId] [uniqueidentifier] NULL,
[LeadRequirementId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
--========================================================================================== 
-- TRIGGER adLeadTransactions_Audit_Delete 
-- DELETE  add Audit History when delete records in adLeadTransactions
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadTransactions_Audit_Delete] ON [dbo].[adLeadTransactions]
    FOR DELETE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Deleted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Deleted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Deleted
                        );
 
        IF @EventRows > 0
            BEGIN
                EXEC fmAuditHistAdd @AuditHistId,'adLeadTransactions','D',@EventRows,@EventDate,@UserName;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),Old.LeadId)
                        FROM    Deleted AS Old;
                -- TransCodeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'TransCodeId'
                               ,CONVERT(VARCHAR(MAX),Old.TransCodeId)
                        FROM    Deleted AS Old;
                -- TransReference 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'TransReference'
                               ,CONVERT(VARCHAR(MAX),Old.TransReference)
                        FROM    Deleted AS Old;
                -- TransDescrip 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'TransDescrip'
                               ,CONVERT(VARCHAR(MAX),Old.TransDescrip)
                        FROM    Deleted AS Old;
                -- TransAmount 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'TransAmount'
                               ,CONVERT(VARCHAR(MAX),Old.TransAmount)
                        FROM    Deleted AS Old;
                -- TransDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'TransDate'
                               ,CONVERT(VARCHAR(MAX),Old.TransDate,121)
                        FROM    Deleted AS Old;
                -- TransTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'TransTypeId'
                               ,CONVERT(VARCHAR(MAX),Old.TransTypeId)
                        FROM    Deleted AS Old;
                -- isEnrolled 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'isEnrolled'
                               ,CONVERT(VARCHAR(MAX),Old.isEnrolled)
                        FROM    Deleted AS Old;
                -- CreatedDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'CreatedDate'
                               ,CONVERT(VARCHAR(MAX),Old.CreatedDate,121)
                        FROM    Deleted AS Old;
                -- Voided 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'Voided'
                               ,CONVERT(VARCHAR(MAX),Old.Voided)
                        FROM    Deleted AS Old;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),Old.ModUser)
                        FROM    Deleted AS Old;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                        FROM    Deleted AS Old;
                -- CampusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'CampusId'
                               ,CONVERT(VARCHAR(MAX),Old.CampusId)
                        FROM    Deleted AS Old;
                -- ReversalReason 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'ReversalReason'
                               ,CONVERT(VARCHAR(MAX),Old.ReversalReason)
                        FROM    Deleted AS Old;
                -- DisplaySequence 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'DisplaySequence'
                               ,CONVERT(VARCHAR(MAX),Old.DisplaySequence)
                        FROM    Deleted AS Old;
                -- SecondDisplaySequence 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'SecondDisplaySequence'
                               ,CONVERT(VARCHAR(MAX),Old.SecondDisplaySequence)
                        FROM    Deleted AS Old;
                -- MapTransactionId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'MapTransactionId'
                               ,CONVERT(VARCHAR(MAX),Old.MapTransactionId)
                        FROM    Deleted AS Old;
                -- LeadRequirementId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'LeadRequirementId'
                               ,CONVERT(VARCHAR(MAX),Old.LeadRequirementId)
                        FROM    Deleted AS Old;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadTransactions_Audit_Delete 
--========================================================================================== 
 

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
--========================================================================================== 
-- TRIGGER adLeadTransactions_Audit_Insert 
-- INSERT  add Audit History when insert records in adLeadTransactions
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadTransactions_Audit_Insert] ON [dbo].[adLeadTransactions]
    FOR INSERT
AS
    BEGIN 
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadTransactions','I',@EventRows,@EventDate,@UserName;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),New.LeadId)
                        FROM    Inserted AS New;
                -- TransCodeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'TransCodeId'
                               ,CONVERT(VARCHAR(MAX),New.TransCodeId)
                        FROM    Inserted AS New;
                -- TransReference 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'TransReference'
                               ,CONVERT(VARCHAR(MAX),New.TransReference)
                        FROM    Inserted AS New;
                -- TransDescrip 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'TransDescrip'
                               ,CONVERT(VARCHAR(MAX),New.TransDescrip)
                        FROM    Inserted AS New;
                -- TransAmount 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'TransAmount'
                               ,CONVERT(VARCHAR(MAX),New.TransAmount)
                        FROM    Inserted AS New;
                -- TransDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'TransDate'
                               ,CONVERT(VARCHAR(MAX),New.TransDate,121)
                        FROM    Inserted AS New;
                -- TransTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'TransTypeId'
                               ,CONVERT(VARCHAR(MAX),New.TransTypeId)
                        FROM    Inserted AS New;
                -- isEnrolled 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'isEnrolled'
                               ,CONVERT(VARCHAR(MAX),New.isEnrolled)
                        FROM    Inserted AS New;
                -- CreatedDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'CreatedDate'
                               ,CONVERT(VARCHAR(MAX),New.CreatedDate,121)
                        FROM    Inserted AS New;
                -- Voided 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'Voided'
                               ,CONVERT(VARCHAR(MAX),New.Voided)
                        FROM    Inserted AS New;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),New.ModUser)
                        FROM    Inserted AS New;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                        FROM    Inserted AS New;
                -- CampusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'CampusId'
                               ,CONVERT(VARCHAR(MAX),New.CampusId)
                        FROM    Inserted AS New;
                -- ReversalReason 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'ReversalReason'
                               ,CONVERT(VARCHAR(MAX),New.ReversalReason)
                        FROM    Inserted AS New;
                -- DisplaySequence 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'DisplaySequence'
                               ,CONVERT(VARCHAR(MAX),New.DisplaySequence)
                        FROM    Inserted AS New;
                -- SecondDisplaySequence 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'SecondDisplaySequence'
                               ,CONVERT(VARCHAR(MAX),New.SecondDisplaySequence)
                        FROM    Inserted AS New;
                -- MapTransactionId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'MapTransactionId'
                               ,CONVERT(VARCHAR(MAX),New.MapTransactionId)
                        FROM    Inserted AS New;
                -- LeadRequirementId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'LeadRequirementId'
                               ,CONVERT(VARCHAR(MAX),New.LeadRequirementId)
                        FROM    Inserted AS New;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadTransactions_Audit_Insert 
--========================================================================================== 
 

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--========================================================================================== 
-- TRIGGER adLeadTransactions_Audit_Update 
-- UPDATE  add Audit History when update fields in adLeadTransactions
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadTransactions_Audit_Update] ON [dbo].[adLeadTransactions]
    FOR UPDATE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadTransactions','U',@EventRows,@EventDate,@UserName;
                -- LeadId 
                IF UPDATE(LeadId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'LeadId'
                                       ,CONVERT(VARCHAR(MAX),Old.LeadId)
                                       ,CONVERT(VARCHAR(MAX),New.LeadId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.LeadId <> New.LeadId
                                        OR (
                                             Old.LeadId IS NULL
                                             AND New.LeadId IS NOT NULL
                                           )
                                        OR (
                                             New.LeadId IS NULL
                                             AND Old.LeadId IS NOT NULL
                                           );
                    END;
                -- TransCodeId 
                IF UPDATE(TransCodeId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'TransCodeId'
                                       ,CONVERT(VARCHAR(MAX),Old.TransCodeId)
                                       ,CONVERT(VARCHAR(MAX),New.TransCodeId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.TransCodeId <> New.TransCodeId
                                        OR (
                                             Old.TransCodeId IS NULL
                                             AND New.TransCodeId IS NOT NULL
                                           )
                                        OR (
                                             New.TransCodeId IS NULL
                                             AND Old.TransCodeId IS NOT NULL
                                           );
                    END;
                -- TransReference 
                IF UPDATE(TransReference)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'TransReference'
                                       ,CONVERT(VARCHAR(MAX),Old.TransReference)
                                       ,CONVERT(VARCHAR(MAX),New.TransReference)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.TransReference <> New.TransReference
                                        OR (
                                             Old.TransReference IS NULL
                                             AND New.TransReference IS NOT NULL
                                           )
                                        OR (
                                             New.TransReference IS NULL
                                             AND Old.TransReference IS NOT NULL
                                           );
                    END;
                -- TransDescrip 
                IF UPDATE(TransDescrip)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'TransDescrip'
                                       ,CONVERT(VARCHAR(MAX),Old.TransDescrip)
                                       ,CONVERT(VARCHAR(MAX),New.TransDescrip)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.TransDescrip <> New.TransDescrip
                                        OR (
                                             Old.TransDescrip IS NULL
                                             AND New.TransDescrip IS NOT NULL
                                           )
                                        OR (
                                             New.TransDescrip IS NULL
                                             AND Old.TransDescrip IS NOT NULL
                                           );
                    END;
                -- TransAmount 
                IF UPDATE(TransAmount)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'TransAmount'
                                       ,CONVERT(VARCHAR(MAX),Old.TransAmount)
                                       ,CONVERT(VARCHAR(MAX),New.TransAmount)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.TransAmount <> New.TransAmount
                                        OR (
                                             Old.TransAmount IS NULL
                                             AND New.TransAmount IS NOT NULL
                                           )
                                        OR (
                                             New.TransAmount IS NULL
                                             AND Old.TransAmount IS NOT NULL
                                           );
                    END;
                -- TransDate 
                IF UPDATE(TransDate)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'TransDate'
                                       ,CONVERT(VARCHAR(MAX),Old.TransDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.TransDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.TransDate <> New.TransDate
                                        OR (
                                             Old.TransDate IS NULL
                                             AND New.TransDate IS NOT NULL
                                           )
                                        OR (
                                             New.TransDate IS NULL
                                             AND Old.TransDate IS NOT NULL
                                           );
                    END;
                -- TransTypeId 
                IF UPDATE(TransTypeId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'TransTypeId'
                                       ,CONVERT(VARCHAR(MAX),Old.TransTypeId)
                                       ,CONVERT(VARCHAR(MAX),New.TransTypeId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.TransTypeId <> New.TransTypeId
                                        OR (
                                             Old.TransTypeId IS NULL
                                             AND New.TransTypeId IS NOT NULL
                                           )
                                        OR (
                                             New.TransTypeId IS NULL
                                             AND Old.TransTypeId IS NOT NULL
                                           );
                    END;
                -- isEnrolled 
                IF UPDATE(isEnrolled)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'isEnrolled'
                                       ,CONVERT(VARCHAR(MAX),Old.isEnrolled)
                                       ,CONVERT(VARCHAR(MAX),New.isEnrolled)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.isEnrolled <> New.isEnrolled
                                        OR (
                                             Old.isEnrolled IS NULL
                                             AND New.isEnrolled IS NOT NULL
                                           )
                                        OR (
                                             New.isEnrolled IS NULL
                                             AND Old.isEnrolled IS NOT NULL
                                           );
                    END;
                -- CreatedDate 
                IF UPDATE(CreatedDate)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'CreatedDate'
                                       ,CONVERT(VARCHAR(MAX),Old.CreatedDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.CreatedDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.CreatedDate <> New.CreatedDate
                                        OR (
                                             Old.CreatedDate IS NULL
                                             AND New.CreatedDate IS NOT NULL
                                           )
                                        OR (
                                             New.CreatedDate IS NULL
                                             AND Old.CreatedDate IS NOT NULL
                                           );
                    END;
                -- Voided 
                IF UPDATE(Voided)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'Voided'
                                       ,CONVERT(VARCHAR(MAX),Old.Voided)
                                       ,CONVERT(VARCHAR(MAX),New.Voided)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.Voided <> New.Voided
                                        OR (
                                             Old.Voided IS NULL
                                             AND New.Voided IS NOT NULL
                                           )
                                        OR (
                                             New.Voided IS NULL
                                             AND Old.Voided IS NOT NULL
                                           );
                    END;
                -- ModUser 
                IF UPDATE(ModUser)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'ModUser'
                                       ,CONVERT(VARCHAR(MAX),Old.ModUser)
                                       ,CONVERT(VARCHAR(MAX),New.ModUser)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.ModUser <> New.ModUser
                                        OR (
                                             Old.ModUser IS NULL
                                             AND New.ModUser IS NOT NULL
                                           )
                                        OR (
                                             New.ModUser IS NULL
                                             AND Old.ModUser IS NOT NULL
                                           );
                    END;
                -- ModDate 
                IF UPDATE(ModDate)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'ModDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.ModDate <> New.ModDate
                                        OR (
                                             Old.ModDate IS NULL
                                             AND New.ModDate IS NOT NULL
                                           )
                                        OR (
                                             New.ModDate IS NULL
                                             AND Old.ModDate IS NOT NULL
                                           );
                    END;
                -- CampusId 
                IF UPDATE(CampusId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'CampusId'
                                       ,CONVERT(VARCHAR(MAX),Old.CampusId)
                                       ,CONVERT(VARCHAR(MAX),New.CampusId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.CampusId <> New.CampusId
                                        OR (
                                             Old.CampusId IS NULL
                                             AND New.CampusId IS NOT NULL
                                           )
                                        OR (
                                             New.CampusId IS NULL
                                             AND Old.CampusId IS NOT NULL
                                           );
                    END;
                -- ReversalReason 
                IF UPDATE(ReversalReason)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'ReversalReason'
                                       ,CONVERT(VARCHAR(MAX),Old.ReversalReason)
                                       ,CONVERT(VARCHAR(MAX),New.ReversalReason)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.ReversalReason <> New.ReversalReason
                                        OR (
                                             Old.ReversalReason IS NULL
                                             AND New.ReversalReason IS NOT NULL
                                           )
                                        OR (
                                             New.ReversalReason IS NULL
                                             AND Old.ReversalReason IS NOT NULL
                                           );
                    END;
                -- DisplaySequence 
                IF UPDATE(DisplaySequence)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'DisplaySequence'
                                       ,CONVERT(VARCHAR(MAX),Old.DisplaySequence)
                                       ,CONVERT(VARCHAR(MAX),New.DisplaySequence)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.DisplaySequence <> New.DisplaySequence
                                        OR (
                                             Old.DisplaySequence IS NULL
                                             AND New.DisplaySequence IS NOT NULL
                                           )
                                        OR (
                                             New.DisplaySequence IS NULL
                                             AND Old.DisplaySequence IS NOT NULL
                                           );
                    END;
                -- SecondDisplaySequence 
                IF UPDATE(SecondDisplaySequence)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'SecondDisplaySequence'
                                       ,CONVERT(VARCHAR(MAX),Old.SecondDisplaySequence)
                                       ,CONVERT(VARCHAR(MAX),New.SecondDisplaySequence)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.SecondDisplaySequence <> New.SecondDisplaySequence
                                        OR (
                                             Old.SecondDisplaySequence IS NULL
                                             AND New.SecondDisplaySequence IS NOT NULL
                                           )
                                        OR (
                                             New.SecondDisplaySequence IS NULL
                                             AND Old.SecondDisplaySequence IS NOT NULL
                                           );
                    END;
                -- MapTransactionId 
                IF UPDATE(MapTransactionId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'MapTransactionId'
                                       ,CONVERT(VARCHAR(MAX),Old.MapTransactionId)
                                       ,CONVERT(VARCHAR(MAX),New.MapTransactionId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.MapTransactionId <> New.MapTransactionId
                                        OR (
                                             Old.MapTransactionId IS NULL
                                             AND New.MapTransactionId IS NOT NULL
                                           )
                                        OR (
                                             New.MapTransactionId IS NULL
                                             AND Old.MapTransactionId IS NOT NULL
                                           );
                    END;
                -- LeadRequirementId 
                IF UPDATE(LeadRequirementId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'LeadRequirementId'
                                       ,CONVERT(VARCHAR(MAX),Old.LeadRequirementId)
                                       ,CONVERT(VARCHAR(MAX),New.LeadRequirementId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.LeadRequirementId <> New.LeadRequirementId
                                        OR (
                                             Old.LeadRequirementId IS NULL
                                             AND New.LeadRequirementId IS NOT NULL
                                           )
                                        OR (
                                             New.LeadRequirementId IS NULL
                                             AND Old.LeadRequirementId IS NOT NULL
                                           );
                    END;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadTransactions_Audit_Update 
--========================================================================================== 
 

GO
ALTER TABLE [dbo].[adLeadTransactions] ADD CONSTRAINT [PK_adLeadTransactions_TransactionId] PRIMARY KEY CLUSTERED  ([TransactionId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adLeadTransactions] ADD CONSTRAINT [FK_adLeadTransactions_adLeads_LeadId_LeadId] FOREIGN KEY ([LeadId]) REFERENCES [dbo].[adLeads] ([LeadId])
GO
ALTER TABLE [dbo].[adLeadTransactions] ADD CONSTRAINT [FK_adLeadTransactions_adReqs_LeadRequirementId_adReqId] FOREIGN KEY ([LeadRequirementId]) REFERENCES [dbo].[adReqs] ([adReqId])
GO
ALTER TABLE [dbo].[adLeadTransactions] ADD CONSTRAINT [FK_adLeadTransactions_saTransCodes_TransCodeId_TransCodeId] FOREIGN KEY ([TransCodeId]) REFERENCES [dbo].[saTransCodes] ([TransCodeId])
GO
ALTER TABLE [dbo].[adLeadTransactions] ADD CONSTRAINT [FK_adLeadTransactions_saTransTypes_TransTypeId_TransTypeId] FOREIGN KEY ([TransTypeId]) REFERENCES [dbo].[saTransTypes] ([TransTypeId])
GO
