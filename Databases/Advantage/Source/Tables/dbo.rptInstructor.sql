CREATE TABLE [dbo].[rptInstructor]
(
[InstructorID] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_rptInstructor_InstructorID] DEFAULT (newid()),
[InstructorDescrip] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[rptInstructor] ADD CONSTRAINT [PK_rptInstructor_InstructorID] PRIMARY KEY CLUSTERED  ([InstructorID]) ON [PRIMARY]
GO
