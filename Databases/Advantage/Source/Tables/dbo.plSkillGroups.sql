CREATE TABLE [dbo].[plSkillGroups]
(
[SkillGrpId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_plSkillGroups_SkillGrpId] DEFAULT (newid()),
[SkillGrpCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SkillGrpName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusId] [uniqueidentifier] NULL,
[CampGrpId] [uniqueidentifier] NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[plSkillGroups_Audit_Delete] ON [dbo].[plSkillGroups]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'plSkillGroups','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SkillGrpId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SkillGrpId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SkillGrpId
                               ,'SkillGrpCode'
                               ,CONVERT(VARCHAR(8000),Old.SkillGrpCode,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SkillGrpId
                               ,'SkillGrpName'
                               ,CONVERT(VARCHAR(8000),Old.SkillGrpName,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[plSkillGroups_Audit_Insert] ON [dbo].[plSkillGroups]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'plSkillGroups','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SkillGrpId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SkillGrpId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SkillGrpId
                               ,'SkillGrpCode'
                               ,CONVERT(VARCHAR(8000),New.SkillGrpCode,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SkillGrpId
                               ,'SkillGrpName'
                               ,CONVERT(VARCHAR(8000),New.SkillGrpName,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[plSkillGroups_Audit_Update] ON [dbo].[plSkillGroups]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'plSkillGroups','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SkillGrpId
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SkillGrpId = New.SkillGrpId
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SkillGrpId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SkillGrpId = New.SkillGrpId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(SkillGrpCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SkillGrpId
                                   ,'SkillGrpCode'
                                   ,CONVERT(VARCHAR(8000),Old.SkillGrpCode,121)
                                   ,CONVERT(VARCHAR(8000),New.SkillGrpCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SkillGrpId = New.SkillGrpId
                            WHERE   Old.SkillGrpCode <> New.SkillGrpCode
                                    OR (
                                         Old.SkillGrpCode IS NULL
                                         AND New.SkillGrpCode IS NOT NULL
                                       )
                                    OR (
                                         New.SkillGrpCode IS NULL
                                         AND Old.SkillGrpCode IS NOT NULL
                                       ); 
                IF UPDATE(SkillGrpName)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SkillGrpId
                                   ,'SkillGrpName'
                                   ,CONVERT(VARCHAR(8000),Old.SkillGrpName,121)
                                   ,CONVERT(VARCHAR(8000),New.SkillGrpName,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SkillGrpId = New.SkillGrpId
                            WHERE   Old.SkillGrpName <> New.SkillGrpName
                                    OR (
                                         Old.SkillGrpName IS NULL
                                         AND New.SkillGrpName IS NOT NULL
                                       )
                                    OR (
                                         New.SkillGrpName IS NULL
                                         AND Old.SkillGrpName IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[plSkillGroups] ADD CONSTRAINT [PK_plSkillGroups_SkillGrpId] PRIMARY KEY CLUSTERED  ([SkillGrpId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_plSkillGroups_SkillGrpCode_SkillGrpName_CampGrpId] ON [dbo].[plSkillGroups] ([SkillGrpCode], [SkillGrpName], [CampGrpId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[plSkillGroups] ADD CONSTRAINT [FK_plSkillGroups_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[plSkillGroups] ADD CONSTRAINT [FK_plSkillGroups_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
