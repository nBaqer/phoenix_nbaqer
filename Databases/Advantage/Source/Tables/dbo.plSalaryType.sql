CREATE TABLE [dbo].[plSalaryType]
(
[SalaryTypeId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_plSalaryType_SalaryTypeId] DEFAULT (newid()),
[SalaryTypeDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[SalaryTypeCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[plSalaryType] ADD CONSTRAINT [PK_plSalaryType_SalaryTypeId] PRIMARY KEY CLUSTERED  ([SalaryTypeId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[plSalaryType] ADD CONSTRAINT [FK_plSalaryType_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[plSalaryType] ADD CONSTRAINT [FK_plSalaryType_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
