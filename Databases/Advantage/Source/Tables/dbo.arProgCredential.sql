CREATE TABLE [dbo].[arProgCredential]
(
[CredentialId] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[CredentialCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[CredentialDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[IPEDSSequence] [int] NULL,
[IPEDSValue] [int] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[GESequence] [int] NULL,
[GERptAgencyFldValId] [int] NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[arProgCredential_Audit_Delete] ON [dbo].[arProgCredential]
    AFTER DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId UNIQUEIDENTIFIER; 
    DECLARE @EventRows INT; 
    DECLARE @EventDate DATETIME; 
    DECLARE @UserName VARCHAR(50);		 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arProgCredential','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CredentialId
                               ,'CredentialCode'
                               ,CONVERT(VARCHAR(8000),Old.CredentialCode,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CredentialId
                               ,'CredentialDescription'
                               ,CONVERT(VARCHAR(8000),Old.CredentialDescription,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CredentialId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CredentialId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
            END; 
        END; 




    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

    
CREATE TRIGGER [dbo].[arProgCredential_Audit_Insert] ON [dbo].[arProgCredential]
    AFTER INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId UNIQUEIDENTIFIER; 
    DECLARE @EventRows INT; 
    DECLARE @EventDate DATETIME; 
    DECLARE @UserName VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arProgCredential','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CredentialId
                               ,'CredentialCode'
                               ,CONVERT(VARCHAR(8000),New.CredentialCode,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CredentialId
                               ,'CredentialDescription'
                               ,CONVERT(VARCHAR(8000),New.CredentialDescription,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CredentialId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CredentialId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New; 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[arProgCredential_Audit_Update] ON [dbo].[arProgCredential]
    AFTER UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId UNIQUEIDENTIFIER; 
    DECLARE @EventRows INT; 
    DECLARE @EventDate DATETIME; 
    DECLARE @UserName VARCHAR(50);			
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arProgCredential','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(CredentialCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CredentialId
                                   ,'CredentialCode'
                                   ,CONVERT(VARCHAR(8000),Old.CredentialCode,121)
                                   ,CONVERT(VARCHAR(8000),New.CredentialCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CredentialId = New.CredentialId
                            WHERE   Old.CredentialCode <> New.CredentialCode
                                    OR (
                                         Old.CredentialCode IS NULL
                                         AND New.CredentialCode IS NOT NULL
                                       )
                                    OR (
                                         New.CredentialCode IS NULL
                                         AND Old.CredentialCode IS NOT NULL
                                       ); 
                IF UPDATE(CredentialDescription)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CredentialId
                                   ,'CredentialDescription'
                                   ,CONVERT(VARCHAR(8000),Old.CredentialDescription,121)
                                   ,CONVERT(VARCHAR(8000),New.CredentialDescription,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CredentialId = New.CredentialId
                            WHERE   Old.CredentialDescription <> New.CredentialDescription
                                    OR (
                                         Old.CredentialDescription IS NULL
                                         AND New.CredentialDescription IS NOT NULL
                                       )
                                    OR (
                                         New.CredentialDescription IS NULL
                                         AND Old.CredentialDescription IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CredentialId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CredentialId = New.CredentialId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CredentialId
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CredentialId = New.CredentialId
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 
            END;  
        END; 



    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[arProgCredential] ADD CONSTRAINT [PK_arProgCredential_CredentialId] PRIMARY KEY CLUSTERED  ([CredentialId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arProgCredential] ADD CONSTRAINT [FK_arProgCredential_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[arProgCredential] ADD CONSTRAINT [FK_arProgCredential_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
