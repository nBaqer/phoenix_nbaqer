CREATE TABLE [dbo].[syTermTypes]
(
[TermTypeId] [int] NOT NULL,
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syTermTypes] ADD CONSTRAINT [PK_syTermTypes_TermTypeId] PRIMARY KEY CLUSTERED  ([TermTypeId]) ON [PRIMARY]
GO
