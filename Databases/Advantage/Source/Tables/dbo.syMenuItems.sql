CREATE TABLE [dbo].[syMenuItems]
(
[MenuItemId] [int] NOT NULL IDENTITY(1, 1),
[MenuName] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DisplayName] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Url] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MenuItemTypeId] [smallint] NOT NULL,
[ParentId] [int] NULL,
[DisplayOrder] [int] NULL,
[IsPopup] [bit] NOT NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsActive] [bit] NOT NULL CONSTRAINT [DF_syMenuItems_IsActive] DEFAULT ((0)),
[ResourceId] [smallint] NULL,
[HierarchyId] [uniqueidentifier] NULL,
[ModuleCode] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MRUType] [int] NULL,
[HideStatusBar] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syMenuItems] ADD CONSTRAINT [PK_syMenuItems_MenuItemId] PRIMARY KEY CLUSTERED  ([MenuItemId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syMenuItems] WITH NOCHECK ADD CONSTRAINT [FK_syMenuItems_syMenuItems_ParentId_MenuItemId] FOREIGN KEY ([ParentId]) REFERENCES [dbo].[syMenuItems] ([MenuItemId])
GO
ALTER TABLE [dbo].[syMenuItems] ADD CONSTRAINT [FK_syMenuItems_syMenuItemType_MenuItemTypeId_MenuItemTypeId] FOREIGN KEY ([MenuItemTypeId]) REFERENCES [dbo].[syMenuItemType] ([MenuItemTypeId])
GO
