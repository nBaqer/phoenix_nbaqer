CREATE TABLE [dbo].[syMessageGroupSchemas]
(
[MessageSchemaId] [uniqueidentifier] NOT NULL,
[SpsData] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[XmlDefaultData] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SqlStatement] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Html_Xslt] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[syMessageGroupSchemas] ADD CONSTRAINT [PK_syMessageGroupSchemas_MessageSchemaId] PRIMARY KEY CLUSTERED  ([MessageSchemaId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syMessageGroupSchemas] ADD CONSTRAINT [FK_syMessageGroupSchemas_syMessageSchemas_MessageSchemaId_MessageSchemaId] FOREIGN KEY ([MessageSchemaId]) REFERENCES [dbo].[syMessageSchemas] ([MessageSchemaId])
GO
