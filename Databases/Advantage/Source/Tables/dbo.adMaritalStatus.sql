CREATE TABLE [dbo].[adMaritalStatus]
(
[MaritalStatId] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[MaritalStatCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[MaritalStatDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[AfaMappingId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adMaritalStatus] ADD CONSTRAINT [PK_adMaritalStatus_MaritalStatId] PRIMARY KEY CLUSTERED  ([MaritalStatId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adMaritalStatus] ADD CONSTRAINT [FK_adMaritalStatus_AfaCatalogMapping_afaMappingId_Id] FOREIGN KEY ([AfaMappingId]) REFERENCES [dbo].[AfaCatalogMapping] ([Id])
GO
ALTER TABLE [dbo].[adMaritalStatus] ADD CONSTRAINT [FK_adMaritalStatus_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[adMaritalStatus] ADD CONSTRAINT [FK_adMaritalStatus_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
