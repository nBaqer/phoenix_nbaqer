CREATE TABLE [dbo].[saBankAccounts]
(
[BankAcctId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_saBankAccounts_BankAcctId] DEFAULT (newid()),
[BankId] [uniqueidentifier] NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[BankAcctDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BankAcctNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BankRoutingNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsDefaultBankAcct] [bit] NULL,
[CampGrpId] [uniqueidentifier] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saBankAccounts] ADD CONSTRAINT [PK_saBankAccounts_BankAcctId] PRIMARY KEY CLUSTERED  ([BankAcctId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_saBankAccounts_BankAcctDescrip_StatusId_CampGrpId] ON [dbo].[saBankAccounts] ([BankAcctDescrip], [StatusId], [CampGrpId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_saBankAccounts_BankAcctNumber_StatusId_CampGrpId] ON [dbo].[saBankAccounts] ([BankAcctNumber], [StatusId], [CampGrpId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_saBankAccounts_BankRoutingNumber_BankAcctNumber] ON [dbo].[saBankAccounts] ([BankRoutingNumber], [BankAcctNumber]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saBankAccounts] ADD CONSTRAINT [FK_saBankAccounts_saBankCodes_BankId_BankId] FOREIGN KEY ([BankId]) REFERENCES [dbo].[saBankCodes] ([BankId])
GO
ALTER TABLE [dbo].[saBankAccounts] ADD CONSTRAINT [FK_saBankAccounts_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[saBankAccounts] ADD CONSTRAINT [FK_saBankAccounts_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
