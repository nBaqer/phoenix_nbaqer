CREATE TABLE [dbo].[saPrgVerDefaultChargePeriods]
(
[PrgVerPmtId] [uniqueidentifier] NOT NULL,
[FeeLevelID] [tinyint] NOT NULL,
[SysTransCodeID] [int] NOT NULL,
[PrgVerID] [uniqueidentifier] NOT NULL,
[PeriodCanChange] [bit] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[ProgramVersionDefaultChargePeriodsId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_saPrgVerDefaultChargePeriods_ProgramVersionDefaultChargePeriodsId] DEFAULT (newsequentialid())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saPrgVerDefaultChargePeriods] ADD CONSTRAINT [PK_saPrgVerDefaultChargePeriods_ProgramVersionDefaultChargePeriodsId] PRIMARY KEY CLUSTERED  ([ProgramVersionDefaultChargePeriodsId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saPrgVerDefaultChargePeriods] ADD CONSTRAINT [FK_saPrgVerDefaultChargePeriods_arPrgVersions_PrgVerID_PrgVerId] FOREIGN KEY ([PrgVerID]) REFERENCES [dbo].[arPrgVersions] ([PrgVerId])
GO
ALTER TABLE [dbo].[saPrgVerDefaultChargePeriods] ADD CONSTRAINT [FK_saPrgVerDefaultChargePeriods_saFeeLevels_FeeLevelID_FeeLevelId] FOREIGN KEY ([FeeLevelID]) REFERENCES [dbo].[saFeeLevels] ([FeeLevelId])
GO
ALTER TABLE [dbo].[saPrgVerDefaultChargePeriods] ADD CONSTRAINT [FK_saPrgVerDefaultChargePeriods_saSysTransCodes_SysTransCodeID_SysTransCodeId] FOREIGN KEY ([SysTransCodeID]) REFERENCES [dbo].[saSysTransCodes] ([SysTransCodeId])
GO
