CREATE TABLE [dbo].[syFldCategories]
(
[CategoryId] [int] NOT NULL,
[EntityId] [smallint] NOT NULL,
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syFldCategories] ADD CONSTRAINT [PK_syFldCategories_CategoryId_EntityId] PRIMARY KEY CLUSTERED  ([CategoryId], [EntityId]) ON [PRIMARY]
GO
