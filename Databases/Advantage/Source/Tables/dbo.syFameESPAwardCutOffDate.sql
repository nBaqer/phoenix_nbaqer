CREATE TABLE [dbo].[syFameESPAwardCutOffDate]
(
[AwardCutOffDateGUID] [uniqueidentifier] NOT NULL,
[AwardCutOffDateCount] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syFameESPAwardCutOffDate] ADD CONSTRAINT [PK_syFameESPAwardCutOffDate_AwardCutOffDateGUID] PRIMARY KEY CLUSTERED  ([AwardCutOffDateGUID]) ON [PRIMARY]
GO
