CREATE TABLE [dbo].[saTransCodeGLAccounts]
(
[TransCodeGLAccountId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_saTransCodeGLAccounts_TransCodeGLAccountId] DEFAULT (newid()),
[TransCodeId] [uniqueidentifier] NOT NULL,
[GLAccount] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Percentage] [money] NOT NULL CONSTRAINT [DF_saTransCodeGLAccounts_Percentage] DEFAULT ((100.00)),
[TypeId] [tinyint] NOT NULL,
[ViewOrder] [int] NOT NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saTransCodeGLAccounts] ADD CONSTRAINT [PK_saTransCodeGLAccounts_TransCodeGLAccountId] PRIMARY KEY CLUSTERED  ([TransCodeGLAccountId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saTransCodeGLAccounts] ADD CONSTRAINT [FK_saTransCodeGLAccounts_saTransCodes_TransCodeId_TransCodeId] FOREIGN KEY ([TransCodeId]) REFERENCES [dbo].[saTransCodes] ([TransCodeId])
GO
