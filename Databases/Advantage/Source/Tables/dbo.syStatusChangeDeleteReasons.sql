CREATE TABLE [dbo].[syStatusChangeDeleteReasons]
(
[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_syStatusChangeDeleteReasons_Id] DEFAULT (newid()),
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syStatusChangeDeleteReasons] ADD CONSTRAINT [PK_syStatusChangeDeleteReasons_Id] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
