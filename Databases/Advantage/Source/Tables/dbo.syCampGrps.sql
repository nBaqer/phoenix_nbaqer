CREATE TABLE [dbo].[syCampGrps]
(
[CampGrpId] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[CampGrpCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL,
[IsAllCampusGrp] [bit] NULL CONSTRAINT [DF_syCampGrps_IsAllCampusGrp] DEFAULT ((0)),
[CampusId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syCampGrps] ADD CONSTRAINT [PK_syCampGrps_CampGrpId] PRIMARY KEY CLUSTERED  ([CampGrpId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_syCampGrps_CampGrpCode_CampGrpDescrip] ON [dbo].[syCampGrps] ([CampGrpCode], [CampGrpDescrip]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syCampGrps] ADD CONSTRAINT [FK_syCampGrps_syCampuses_CampusId_CampusId] FOREIGN KEY ([CampusId]) REFERENCES [dbo].[syCampuses] ([CampusId])
GO
ALTER TABLE [dbo].[syCampGrps] ADD CONSTRAINT [FK_syCampGrps_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
