CREATE TABLE [dbo].[syUserSpecQuestions]
(
[UserName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UserQuestionId] [uniqueidentifier] NOT NULL,
[UserAnswer] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserSpecQuestionId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_syUserSpecQuestions_UserSpecQuestionId] DEFAULT (newsequentialid())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syUserSpecQuestions] ADD CONSTRAINT [PK_syUserSpecQuestions_UserSpecQuestionId] PRIMARY KEY CLUSTERED  ([UserSpecQuestionId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syUserSpecQuestions] ADD CONSTRAINT [FK_syUserSpecQuestions_syUserQuestions_UserQuestionId_UserQuestionId] FOREIGN KEY ([UserQuestionId]) REFERENCES [dbo].[syUserQuestions] ([UserQuestionId])
GO
