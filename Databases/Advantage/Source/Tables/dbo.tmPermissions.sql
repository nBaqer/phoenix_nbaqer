CREATE TABLE [dbo].[tmPermissions]
(
[SID] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_tmPermissions_SID] DEFAULT (newid()),
[UserID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RoleId] [uniqueidentifier] NULL,
[CampGrpId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tmPermissions] ADD CONSTRAINT [PK_tmPermissions_SID] PRIMARY KEY CLUSTERED  ([SID]) ON [PRIMARY]
GO
