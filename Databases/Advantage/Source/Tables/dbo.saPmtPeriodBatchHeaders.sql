CREATE TABLE [dbo].[saPmtPeriodBatchHeaders]
(
[PmtPeriodBatchHeaderId] [int] NOT NULL IDENTITY(1, 1),
[BatchName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BatchCreationDate] [datetime2] NOT NULL CONSTRAINT [DF_saPmtPeriodBatchHeaders_BatchCreationDate] DEFAULT (getdate()),
[IsPosted] [bit] NOT NULL CONSTRAINT [DF_saPmtPeriodBatchHeaders_IsPosted] DEFAULT ((0)),
[PostedDate] [datetime] NULL,
[BatchItemCount] [int] NOT NULL CONSTRAINT [DF_saPmtPeriodBatchHeaders_BatchItemCount] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saPmtPeriodBatchHeaders] ADD CONSTRAINT [PK_saPmtPeriodBatchHeaders_PmtPeriodBatchHeaderId] PRIMARY KEY CLUSTERED  ([PmtPeriodBatchHeaderId]) ON [PRIMARY]
GO
