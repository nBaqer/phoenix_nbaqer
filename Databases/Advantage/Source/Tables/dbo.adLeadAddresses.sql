CREATE TABLE [dbo].[adLeadAddresses]
(
[adLeadAddressId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_adLeadAddresses_adLeadAddressId] DEFAULT (newid()),
[LeadId] [uniqueidentifier] NOT NULL,
[AddressTypeId] [uniqueidentifier] NOT NULL,
[Address1] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_adLeadAddresses_Address1] DEFAULT (''),
[Address2] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateId] [uniqueidentifier] NULL,
[ZipCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CountryId] [uniqueidentifier] NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[IsMailingAddress] [bit] NOT NULL CONSTRAINT [DF_adLeadAddresses_IsMailingAddress] DEFAULT ((0)),
[IsShowOnLeadPage] [bit] NOT NULL CONSTRAINT [DF_adLeadAddresses_IsShowOnLeadPage] DEFAULT ((0)),
[ModDate] [datetime] NOT NULL CONSTRAINT [DF_adLeadAddresses_ModDate] DEFAULT (getdate()),
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[State] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsInternational] [bit] NULL CONSTRAINT [DF_adLeadAddresses_IsInternational] DEFAULT ((0)),
[CountyId] [uniqueidentifier] NULL,
[ForeignCountyStr] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ForeignCountryStr] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddressApto] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_adLeadAddresses_AddressApto] DEFAULT ('')
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
--========================================================================================== 
-- TRIGGER adLeadAddresses_Audit_Delete 
-- DELETE  add Audit History when delete records in adLeadAddresses
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadAddresses_Audit_Delete] ON [dbo].[adLeadAddresses]
    FOR DELETE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Deleted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Deleted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Deleted
                        );
 
        IF @EventRows > 0
            BEGIN
                EXEC fmAuditHistAdd @AuditHistId,'adLeadAddresses','D',@EventRows,@EventDate,@UserName;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),Old.LeadId)
                        FROM    Deleted AS Old;
                -- AddressTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'AddressTypeId'
                               ,CONVERT(VARCHAR(MAX),Old.AddressTypeId)
                        FROM    Deleted AS Old;
                -- Address1 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'Address1'
                               ,CONVERT(VARCHAR(MAX),Old.Address1)
                        FROM    Deleted AS Old;
                -- Address2 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'Address2'
                               ,CONVERT(VARCHAR(MAX),Old.Address2)
                        FROM    Deleted AS Old;
                -- City 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'City'
                               ,CONVERT(VARCHAR(MAX),Old.City)
                        FROM    Deleted AS Old;
                -- StateId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'StateId'
                               ,CONVERT(VARCHAR(MAX),Old.StateId)
                        FROM    Deleted AS Old;
                -- ZipCode 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'ZipCode'
                               ,CONVERT(VARCHAR(MAX),Old.ZipCode)
                        FROM    Deleted AS Old;
                -- CountryId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'CountryId'
                               ,CONVERT(VARCHAR(MAX),Old.CountryId)
                        FROM    Deleted AS Old;
                -- StatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(MAX),Old.StatusId)
                        FROM    Deleted AS Old;
                -- IsMailingAddress 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'IsMailingAddress'
                               ,CONVERT(VARCHAR(MAX),Old.IsMailingAddress)
                        FROM    Deleted AS Old;
                -- IsShowOnLeadPage 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'IsShowOnLeadPage'
                               ,CONVERT(VARCHAR(MAX),Old.IsShowOnLeadPage)
                        FROM    Deleted AS Old;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                        FROM    Deleted AS Old;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),Old.ModUser)
                        FROM    Deleted AS Old;
                -- State 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'State'
                               ,CONVERT(VARCHAR(MAX),Old.State)
                        FROM    Deleted AS Old;
                -- IsInternational 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'IsInternational'
                               ,CONVERT(VARCHAR(MAX),Old.IsInternational)
                        FROM    Deleted AS Old;
                -- CountyId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'CountyId'
                               ,CONVERT(VARCHAR(MAX),Old.CountyId)
                        FROM    Deleted AS Old;
                -- ForeignCountyStr 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'ForeignCountyStr'
                               ,CONVERT(VARCHAR(MAX),Old.ForeignCountyStr)
                        FROM    Deleted AS Old;
                -- ForeignCountryStr 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'ForeignCountryStr'
                               ,CONVERT(VARCHAR(MAX),Old.ForeignCountryStr)
                        FROM    Deleted AS Old;
                -- AddressApto 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.adLeadAddressId
                               ,'AddressApto'
                               ,CONVERT(VARCHAR(MAX),Old.AddressApto)
                        FROM    Deleted AS Old;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadAddresses_Audit_Delete 
--========================================================================================== 
 


GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
--========================================================================================== 
-- TRIGGER adLeadAddresses_Audit_Insert 
-- INSERT  add Audit History when insert records in adLeadAddresses
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadAddresses_Audit_Insert] ON [dbo].[adLeadAddresses]
    FOR INSERT
AS
    BEGIN 
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadAddresses','I',@EventRows,@EventDate,@UserName;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),New.LeadId)
                        FROM    Inserted AS New;
                -- AddressTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'AddressTypeId'
                               ,CONVERT(VARCHAR(MAX),New.AddressTypeId)
                        FROM    Inserted AS New;
                -- Address1 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'Address1'
                               ,CONVERT(VARCHAR(MAX),New.Address1)
                        FROM    Inserted AS New;
                -- Address2 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'Address2'
                               ,CONVERT(VARCHAR(MAX),New.Address2)
                        FROM    Inserted AS New;
                -- City 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'City'
                               ,CONVERT(VARCHAR(MAX),New.City)
                        FROM    Inserted AS New;
                -- StateId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'StateId'
                               ,CONVERT(VARCHAR(MAX),New.StateId)
                        FROM    Inserted AS New;
                -- ZipCode 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'ZipCode'
                               ,CONVERT(VARCHAR(MAX),New.ZipCode)
                        FROM    Inserted AS New;
                -- CountryId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'CountryId'
                               ,CONVERT(VARCHAR(MAX),New.CountryId)
                        FROM    Inserted AS New;
                -- StatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(MAX),New.StatusId)
                        FROM    Inserted AS New;
                -- IsMailingAddress 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'IsMailingAddress'
                               ,CONVERT(VARCHAR(MAX),New.IsMailingAddress)
                        FROM    Inserted AS New;
                -- IsShowOnLeadPage 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'IsShowOnLeadPage'
                               ,CONVERT(VARCHAR(MAX),New.IsShowOnLeadPage)
                        FROM    Inserted AS New;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                        FROM    Inserted AS New;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),New.ModUser)
                        FROM    Inserted AS New;
                -- State 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'State'
                               ,CONVERT(VARCHAR(MAX),New.State)
                        FROM    Inserted AS New;
                -- IsInternational 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'IsInternational'
                               ,CONVERT(VARCHAR(MAX),New.IsInternational)
                        FROM    Inserted AS New;
                -- CountyId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'CountyId'
                               ,CONVERT(VARCHAR(MAX),New.CountyId)
                        FROM    Inserted AS New;
                -- ForeignCountyStr 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'ForeignCountyStr'
                               ,CONVERT(VARCHAR(MAX),New.ForeignCountyStr)
                        FROM    Inserted AS New;
                -- ForeignCountryStr 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'ForeignCountryStr'
                               ,CONVERT(VARCHAR(MAX),New.ForeignCountryStr)
                        FROM    Inserted AS New;
                -- AddressApto 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.adLeadAddressId
                               ,'AddressApto'
                               ,CONVERT(VARCHAR(MAX),New.AddressApto)
                        FROM    Inserted AS New;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadAddresses_Audit_Insert 
--========================================================================================== 
 


GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--========================================================================================== 
-- TRIGGER adLeadAddresses_Audit_Update 
-- UPDATE  add Audit History when update fields in adLeadAddresses
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadAddresses_Audit_Update] ON [dbo].[adLeadAddresses]
    FOR UPDATE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadAddresses','U',@EventRows,@EventDate,@UserName;
                -- LeadId 
                IF UPDATE(LeadId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'LeadId'
                                       ,CONVERT(VARCHAR(MAX),Old.LeadId)
                                       ,CONVERT(VARCHAR(MAX),New.LeadId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.LeadId <> New.LeadId
                                        OR (
                                             Old.LeadId IS NULL
                                             AND New.LeadId IS NOT NULL
                                           )
                                        OR (
                                             New.LeadId IS NULL
                                             AND Old.LeadId IS NOT NULL
                                           );
                    END;
                -- AddressTypeId 
                IF UPDATE(AddressTypeId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'AddressTypeId'
                                       ,CONVERT(VARCHAR(MAX),Old.AddressTypeId)
                                       ,CONVERT(VARCHAR(MAX),New.AddressTypeId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.AddressTypeId <> New.AddressTypeId
                                        OR (
                                             Old.AddressTypeId IS NULL
                                             AND New.AddressTypeId IS NOT NULL
                                           )
                                        OR (
                                             New.AddressTypeId IS NULL
                                             AND Old.AddressTypeId IS NOT NULL
                                           );
                    END;
                -- Address1 
                IF UPDATE(Address1)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'Address1'
                                       ,CONVERT(VARCHAR(MAX),Old.Address1)
                                       ,CONVERT(VARCHAR(MAX),New.Address1)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.Address1 <> New.Address1
                                        OR (
                                             Old.Address1 IS NULL
                                             AND New.Address1 IS NOT NULL
                                           )
                                        OR (
                                             New.Address1 IS NULL
                                             AND Old.Address1 IS NOT NULL
                                           );
                    END;
                -- Address2 
                IF UPDATE(Address2)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'Address2'
                                       ,CONVERT(VARCHAR(MAX),Old.Address2)
                                       ,CONVERT(VARCHAR(MAX),New.Address2)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.Address2 <> New.Address2
                                        OR (
                                             Old.Address2 IS NULL
                                             AND New.Address2 IS NOT NULL
                                           )
                                        OR (
                                             New.Address2 IS NULL
                                             AND Old.Address2 IS NOT NULL
                                           );
                    END;
                -- City 
                IF UPDATE(City)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'City'
                                       ,CONVERT(VARCHAR(MAX),Old.City)
                                       ,CONVERT(VARCHAR(MAX),New.City)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.City <> New.City
                                        OR (
                                             Old.City IS NULL
                                             AND New.City IS NOT NULL
                                           )
                                        OR (
                                             New.City IS NULL
                                             AND Old.City IS NOT NULL
                                           );
                    END;
                -- StateId 
                IF UPDATE(StateId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'StateId'
                                       ,CONVERT(VARCHAR(MAX),Old.StateId)
                                       ,CONVERT(VARCHAR(MAX),New.StateId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.StateId <> New.StateId
                                        OR (
                                             Old.StateId IS NULL
                                             AND New.StateId IS NOT NULL
                                           )
                                        OR (
                                             New.StateId IS NULL
                                             AND Old.StateId IS NOT NULL
                                           );
                    END;
                -- ZipCode 
                IF UPDATE(ZipCode)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'ZipCode'
                                       ,CONVERT(VARCHAR(MAX),Old.ZipCode)
                                       ,CONVERT(VARCHAR(MAX),New.ZipCode)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.ZipCode <> New.ZipCode
                                        OR (
                                             Old.ZipCode IS NULL
                                             AND New.ZipCode IS NOT NULL
                                           )
                                        OR (
                                             New.ZipCode IS NULL
                                             AND Old.ZipCode IS NOT NULL
                                           );
                    END;
                -- CountryId 
                IF UPDATE(CountryId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'CountryId'
                                       ,CONVERT(VARCHAR(MAX),Old.CountryId)
                                       ,CONVERT(VARCHAR(MAX),New.CountryId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.CountryId <> New.CountryId
                                        OR (
                                             Old.CountryId IS NULL
                                             AND New.CountryId IS NOT NULL
                                           )
                                        OR (
                                             New.CountryId IS NULL
                                             AND Old.CountryId IS NOT NULL
                                           );
                    END;
                -- StatusId 
                IF UPDATE(StatusId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'StatusId'
                                       ,CONVERT(VARCHAR(MAX),Old.StatusId)
                                       ,CONVERT(VARCHAR(MAX),New.StatusId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.StatusId <> New.StatusId
                                        OR (
                                             Old.StatusId IS NULL
                                             AND New.StatusId IS NOT NULL
                                           )
                                        OR (
                                             New.StatusId IS NULL
                                             AND Old.StatusId IS NOT NULL
                                           );
                    END;
                -- IsMailingAddress 
                IF UPDATE(IsMailingAddress)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'IsMailingAddress'
                                       ,CONVERT(VARCHAR(MAX),Old.IsMailingAddress)
                                       ,CONVERT(VARCHAR(MAX),New.IsMailingAddress)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.IsMailingAddress <> New.IsMailingAddress
                                        OR (
                                             Old.IsMailingAddress IS NULL
                                             AND New.IsMailingAddress IS NOT NULL
                                           )
                                        OR (
                                             New.IsMailingAddress IS NULL
                                             AND Old.IsMailingAddress IS NOT NULL
                                           );
                    END;
                -- IsShowOnLeadPage 
                IF UPDATE(IsShowOnLeadPage)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'IsShowOnLeadPage'
                                       ,CONVERT(VARCHAR(MAX),Old.IsShowOnLeadPage)
                                       ,CONVERT(VARCHAR(MAX),New.IsShowOnLeadPage)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.IsShowOnLeadPage <> New.IsShowOnLeadPage
                                        OR (
                                             Old.IsShowOnLeadPage IS NULL
                                             AND New.IsShowOnLeadPage IS NOT NULL
                                           )
                                        OR (
                                             New.IsShowOnLeadPage IS NULL
                                             AND Old.IsShowOnLeadPage IS NOT NULL
                                           );
                    END;
                -- ModDate 
                IF UPDATE(ModDate)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'ModDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.ModDate <> New.ModDate
                                        OR (
                                             Old.ModDate IS NULL
                                             AND New.ModDate IS NOT NULL
                                           )
                                        OR (
                                             New.ModDate IS NULL
                                             AND Old.ModDate IS NOT NULL
                                           );
                    END;
                -- ModUser 
                IF UPDATE(ModUser)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'ModUser'
                                       ,CONVERT(VARCHAR(MAX),Old.ModUser)
                                       ,CONVERT(VARCHAR(MAX),New.ModUser)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.ModUser <> New.ModUser
                                        OR (
                                             Old.ModUser IS NULL
                                             AND New.ModUser IS NOT NULL
                                           )
                                        OR (
                                             New.ModUser IS NULL
                                             AND Old.ModUser IS NOT NULL
                                           );
                    END;
                -- State 
                IF UPDATE(State)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'State'
                                       ,CONVERT(VARCHAR(MAX),Old.State)
                                       ,CONVERT(VARCHAR(MAX),New.State)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.State <> New.State
                                        OR (
                                             Old.State IS NULL
                                             AND New.State IS NOT NULL
                                           )
                                        OR (
                                             New.State IS NULL
                                             AND Old.State IS NOT NULL
                                           );
                    END;
                -- IsInternational 
                IF UPDATE(IsInternational)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'IsInternational'
                                       ,CONVERT(VARCHAR(MAX),Old.IsInternational)
                                       ,CONVERT(VARCHAR(MAX),New.IsInternational)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.IsInternational <> New.IsInternational
                                        OR (
                                             Old.IsInternational IS NULL
                                             AND New.IsInternational IS NOT NULL
                                           )
                                        OR (
                                             New.IsInternational IS NULL
                                             AND Old.IsInternational IS NOT NULL
                                           );
                    END;
                -- CountyId 
                IF UPDATE(CountyId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'CountyId'
                                       ,CONVERT(VARCHAR(MAX),Old.CountyId)
                                       ,CONVERT(VARCHAR(MAX),New.CountyId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.CountyId <> New.CountyId
                                        OR (
                                             Old.CountyId IS NULL
                                             AND New.CountyId IS NOT NULL
                                           )
                                        OR (
                                             New.CountyId IS NULL
                                             AND Old.CountyId IS NOT NULL
                                           );
                    END;
                -- ForeignCountyStr 
                IF UPDATE(ForeignCountyStr)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'ForeignCountyStr'
                                       ,CONVERT(VARCHAR(MAX),Old.ForeignCountyStr)
                                       ,CONVERT(VARCHAR(MAX),New.ForeignCountyStr)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.ForeignCountyStr <> New.ForeignCountyStr
                                        OR (
                                             Old.ForeignCountyStr IS NULL
                                             AND New.ForeignCountyStr IS NOT NULL
                                           )
                                        OR (
                                             New.ForeignCountyStr IS NULL
                                             AND Old.ForeignCountyStr IS NOT NULL
                                           );
                    END;
                -- ForeignCountryStr 
                IF UPDATE(ForeignCountryStr)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'ForeignCountryStr'
                                       ,CONVERT(VARCHAR(MAX),Old.ForeignCountryStr)
                                       ,CONVERT(VARCHAR(MAX),New.ForeignCountryStr)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.ForeignCountryStr <> New.ForeignCountryStr
                                        OR (
                                             Old.ForeignCountryStr IS NULL
                                             AND New.ForeignCountryStr IS NOT NULL
                                           )
                                        OR (
                                             New.ForeignCountryStr IS NULL
                                             AND Old.ForeignCountryStr IS NOT NULL
                                           );
                    END;
                -- AddressApto 
                IF UPDATE(AddressApto)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.adLeadAddressId
                                       ,'AddressApto'
                                       ,CONVERT(VARCHAR(MAX),Old.AddressApto)
                                       ,CONVERT(VARCHAR(MAX),New.AddressApto)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.adLeadAddressId = New.adLeadAddressId
                                WHERE   Old.AddressApto <> New.AddressApto
                                        OR (
                                             Old.AddressApto IS NULL
                                             AND New.AddressApto IS NOT NULL
                                           )
                                        OR (
                                             New.AddressApto IS NULL
                                             AND Old.AddressApto IS NOT NULL
                                           );
                    END;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadAddresses_Audit_Update 
--========================================================================================== 
 


GO
ALTER TABLE [dbo].[adLeadAddresses] ADD CONSTRAINT [PK_adLeadAddresses_adLeadAddressId] PRIMARY KEY CLUSTERED  ([adLeadAddressId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_adLeadAddresses_LeadId_StatusId_IsShowOnLeadPage] ON [dbo].[adLeadAddresses] ([LeadId], [StatusId], [IsShowOnLeadPage]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adLeadAddresses] ADD CONSTRAINT [FK_adLeadAddresses_adCounties_CountyId_CountyId] FOREIGN KEY ([CountyId]) REFERENCES [dbo].[adCounties] ([CountyId])
GO
ALTER TABLE [dbo].[adLeadAddresses] ADD CONSTRAINT [FK_adLeadAddresses_adCountries_CountryId_CountryId] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[adCountries] ([CountryId])
GO
ALTER TABLE [dbo].[adLeadAddresses] ADD CONSTRAINT [FK_adLeadAddresses_adLeads_LeadId_LeadId] FOREIGN KEY ([LeadId]) REFERENCES [dbo].[adLeads] ([LeadId])
GO
ALTER TABLE [dbo].[adLeadAddresses] ADD CONSTRAINT [FK_adLeadAddresses_plAddressTypes_AddressTypeId_AddressTypeId] FOREIGN KEY ([AddressTypeId]) REFERENCES [dbo].[plAddressTypes] ([AddressTypeId])
GO
ALTER TABLE [dbo].[adLeadAddresses] ADD CONSTRAINT [FK_adLeadAddresses_syStates_StateId_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[syStates] ([StateId])
GO
ALTER TABLE [dbo].[adLeadAddresses] ADD CONSTRAINT [FK_adLeadAddresses_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
