CREATE TABLE [dbo].[syProgramUnitTypes]
(
[UnitId] [smallint] NOT NULL,
[UnitDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syProgramUnitTypes] ADD CONSTRAINT [PK_syProgramUnitTypes_UnitId] PRIMARY KEY CLUSTERED  ([UnitId]) ON [PRIMARY]
GO
