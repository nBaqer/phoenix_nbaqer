CREATE TABLE [dbo].[syRptSortPrefs]
(
[SortPrefId] [uniqueidentifier] NOT NULL,
[PrefId] [uniqueidentifier] NOT NULL,
[RptParamId] [smallint] NOT NULL,
[Seq] [tinyint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syRptSortPrefs] ADD CONSTRAINT [PK_syRptSortPrefs_SortPrefId] PRIMARY KEY CLUSTERED  ([SortPrefId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syRptSortPrefs] ADD CONSTRAINT [FK_syRptSortPrefs_syRptUserPrefs_PrefId_PrefId] FOREIGN KEY ([PrefId]) REFERENCES [dbo].[syRptUserPrefs] ([PrefId]) ON DELETE CASCADE ON UPDATE CASCADE
GO
GRANT SELECT ON  [dbo].[syRptSortPrefs] TO [AdvantageRole]
GO
