CREATE TABLE [dbo].[saLateFees]
(
[LateFeesId] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[LateFeesCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[LateFeesDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NULL,
[TransCodeId] [uniqueidentifier] NOT NULL,
[EffectiveDate] [datetime] NOT NULL,
[FlatAmount] [numeric] (9, 2) NOT NULL CONSTRAINT [DF_saLateFees_FlatAmount] DEFAULT ((0.00)),
[Rate] [numeric] (6, 2) NOT NULL CONSTRAINT [DF_saLateFees_Rate] DEFAULT ((0)),
[GracePeriod] [smallint] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[saLateFees_Audit_Delete] ON [dbo].[saLateFees]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saLateFees','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LateFeesId
                               ,'FlatAmount'
                               ,CONVERT(VARCHAR(8000),Old.FlatAmount,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LateFeesId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LateFeesId
                               ,'TransCodeId'
                               ,CONVERT(VARCHAR(8000),Old.TransCodeId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LateFeesId
                               ,'Rate'
                               ,CONVERT(VARCHAR(8000),Old.Rate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LateFeesId
                               ,'GracePeriod'
                               ,CONVERT(VARCHAR(8000),Old.GracePeriod,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LateFeesId
                               ,'LateFeesCode'
                               ,CONVERT(VARCHAR(8000),Old.LateFeesCode,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LateFeesId
                               ,'LateFeesDescrip'
                               ,CONVERT(VARCHAR(8000),Old.LateFeesDescrip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LateFeesId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LateFeesId
                               ,'EffectiveDate'
                               ,CONVERT(VARCHAR(8000),Old.EffectiveDate,121)
                        FROM    Deleted Old; 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[saLateFees_Audit_Insert] ON [dbo].[saLateFees]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saLateFees','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LateFeesId
                               ,'FlatAmount'
                               ,CONVERT(VARCHAR(8000),New.FlatAmount,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LateFeesId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LateFeesId
                               ,'TransCodeId'
                               ,CONVERT(VARCHAR(8000),New.TransCodeId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LateFeesId
                               ,'Rate'
                               ,CONVERT(VARCHAR(8000),New.Rate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LateFeesId
                               ,'GracePeriod'
                               ,CONVERT(VARCHAR(8000),New.GracePeriod,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LateFeesId
                               ,'LateFeesCode'
                               ,CONVERT(VARCHAR(8000),New.LateFeesCode,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LateFeesId
                               ,'LateFeesDescrip'
                               ,CONVERT(VARCHAR(8000),New.LateFeesDescrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LateFeesId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LateFeesId
                               ,'EffectiveDate'
                               ,CONVERT(VARCHAR(8000),New.EffectiveDate,121)
                        FROM    Inserted New; 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO
GO

GO

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[saLateFees_Audit_Update] ON [dbo].[saLateFees]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saLateFees','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(FlatAmount)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.LateFeesId
                                   ,'FlatAmount'
                                   ,CONVERT(VARCHAR(8000),Old.FlatAmount,121)
                                   ,CONVERT(VARCHAR(8000),New.FlatAmount,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.LateFeesId = New.LateFeesId
                            WHERE   Old.FlatAmount <> New.FlatAmount
                                    OR (
                                         Old.FlatAmount IS NULL
                                         AND New.FlatAmount IS NOT NULL
                                       )
                                    OR (
                                         New.FlatAmount IS NULL
                                         AND Old.FlatAmount IS NOT NULL
                                       ); 
                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.LateFeesId
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.LateFeesId = New.LateFeesId
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 
                IF UPDATE(TransCodeId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.LateFeesId
                                   ,'TransCodeId'
                                   ,CONVERT(VARCHAR(8000),Old.TransCodeId,121)
                                   ,CONVERT(VARCHAR(8000),New.TransCodeId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.LateFeesId = New.LateFeesId
                            WHERE   Old.TransCodeId <> New.TransCodeId
                                    OR (
                                         Old.TransCodeId IS NULL
                                         AND New.TransCodeId IS NOT NULL
                                       )
                                    OR (
                                         New.TransCodeId IS NULL
                                         AND Old.TransCodeId IS NOT NULL
                                       ); 
                IF UPDATE(Rate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.LateFeesId
                                   ,'Rate'
                                   ,CONVERT(VARCHAR(8000),Old.Rate,121)
                                   ,CONVERT(VARCHAR(8000),New.Rate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.LateFeesId = New.LateFeesId
                            WHERE   Old.Rate <> New.Rate
                                    OR (
                                         Old.Rate IS NULL
                                         AND New.Rate IS NOT NULL
                                       )
                                    OR (
                                         New.Rate IS NULL
                                         AND Old.Rate IS NOT NULL
                                       ); 
                IF UPDATE(GracePeriod)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.LateFeesId
                                   ,'GracePeriod'
                                   ,CONVERT(VARCHAR(8000),Old.GracePeriod,121)
                                   ,CONVERT(VARCHAR(8000),New.GracePeriod,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.LateFeesId = New.LateFeesId
                            WHERE   Old.GracePeriod <> New.GracePeriod
                                    OR (
                                         Old.GracePeriod IS NULL
                                         AND New.GracePeriod IS NOT NULL
                                       )
                                    OR (
                                         New.GracePeriod IS NULL
                                         AND Old.GracePeriod IS NOT NULL
                                       ); 
                IF UPDATE(LateFeesCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.LateFeesId
                                   ,'LateFeesCode'
                                   ,CONVERT(VARCHAR(8000),Old.LateFeesCode,121)
                                   ,CONVERT(VARCHAR(8000),New.LateFeesCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.LateFeesId = New.LateFeesId
                            WHERE   Old.LateFeesCode <> New.LateFeesCode
                                    OR (
                                         Old.LateFeesCode IS NULL
                                         AND New.LateFeesCode IS NOT NULL
                                       )
                                    OR (
                                         New.LateFeesCode IS NULL
                                         AND Old.LateFeesCode IS NOT NULL
                                       ); 
                IF UPDATE(LateFeesDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.LateFeesId
                                   ,'LateFeesDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.LateFeesDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.LateFeesDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.LateFeesId = New.LateFeesId
                            WHERE   Old.LateFeesDescrip <> New.LateFeesDescrip
                                    OR (
                                         Old.LateFeesDescrip IS NULL
                                         AND New.LateFeesDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.LateFeesDescrip IS NULL
                                         AND Old.LateFeesDescrip IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.LateFeesId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.LateFeesId = New.LateFeesId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(EffectiveDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.LateFeesId
                                   ,'EffectiveDate'
                                   ,CONVERT(VARCHAR(8000),Old.EffectiveDate,121)
                                   ,CONVERT(VARCHAR(8000),New.EffectiveDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.LateFeesId = New.LateFeesId
                            WHERE   Old.EffectiveDate <> New.EffectiveDate
                                    OR (
                                         Old.EffectiveDate IS NULL
                                         AND New.EffectiveDate IS NOT NULL
                                       )
                                    OR (
                                         New.EffectiveDate IS NULL
                                         AND Old.EffectiveDate IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[saLateFees] ADD CONSTRAINT [PK_saLateFees_LateFeesId] PRIMARY KEY CLUSTERED  ([LateFeesId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saLateFees] ADD CONSTRAINT [FK_saLateFees_saTransCodes_TransCodeId_TransCodeId] FOREIGN KEY ([TransCodeId]) REFERENCES [dbo].[saTransCodes] ([TransCodeId])
GO
