CREATE TABLE [dbo].[saSysTransCodes]
(
[SysTransCodeId] [int] NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saSysTransCodes] ADD CONSTRAINT [PK_saSysTransCodes_SysTransCodeId] PRIMARY KEY CLUSTERED  ([SysTransCodeId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saSysTransCodes] ADD CONSTRAINT [FK_saSysTransCodes_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[saSysTransCodes] ADD CONSTRAINT [FK_saSysTransCodes_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
