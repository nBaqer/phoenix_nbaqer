CREATE TABLE [dbo].[msgTemplates]
(
[TemplateId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_msgTemplates_TemplateId] DEFAULT (newid()),
[GroupId] [uniqueidentifier] NULL,
[ModuleEntityId] [smallint] NULL,
[CampGroupId] [uniqueidentifier] NULL,
[Code] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Data] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[ModUser] [uniqueidentifier] NULL,
[Active] [tinyint] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[msgTemplates] ADD CONSTRAINT [PK_msgTemplates_TemplateId] PRIMARY KEY CLUSTERED  ([TemplateId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_msgTemplates_CampGroupId_Code_Descrip] ON [dbo].[msgTemplates] ([CampGroupId], [Code], [Descrip]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[msgTemplates] ADD CONSTRAINT [FK_msgTemplates_syCampGrps_CampGroupId_CampGrpId] FOREIGN KEY ([CampGroupId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
