CREATE TABLE [dbo].[syRptAgencies]
(
[RptAgencyId] [int] NOT NULL,
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syRptAgencies] ADD CONSTRAINT [PK_syRptAgencies_RptAgencyId] PRIMARY KEY CLUSTERED  ([RptAgencyId]) ON [PRIMARY]
GO
