CREATE TABLE [dbo].[syEDExpressExceptionReportDirectLoan_DisbursementTable]
(
[DetailId] [uniqueidentifier] NOT NULL,
[ExceptionReportId] [uniqueidentifier] NOT NULL,
[DbIn] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Filter] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisbDate] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisbGrossAmount] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisbIntRebAmount] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisbLoanFeeAmount] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisbNetAdjAmount] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisbNetAmount] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisbNum] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisbSeqNum] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisbStatus_RelInd] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisbType] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoanId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginalSSN] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[ErrorMsg] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExceptionGUID] [uniqueidentifier] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[syEDExpressExceptionReportDirectLoan_DisbursementTable] ADD CONSTRAINT [PK_syEDExpressExceptionReportDirectLoan_DisbursementTable_DetailId] PRIMARY KEY CLUSTERED  ([DetailId]) ON [PRIMARY]
GO
