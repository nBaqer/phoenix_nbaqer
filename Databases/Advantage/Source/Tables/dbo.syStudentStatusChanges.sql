CREATE TABLE [dbo].[syStudentStatusChanges]
(
[StudentStatusChangeId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_syStudentStatusChanges_StudentStatusChangeId] DEFAULT (newid()),
[StuEnrollId] [uniqueidentifier] NOT NULL,
[OrigStatusId] [uniqueidentifier] NULL,
[NewStatusId] [uniqueidentifier] NULL,
[CampusId] [uniqueidentifier] NOT NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsReversal] [bit] NOT NULL CONSTRAINT [DF_syStudentStatusChanges_IsReversal] DEFAULT ((0)),
[DropReasonId] [uniqueidentifier] NULL,
[DateOfChange] [datetime] NULL,
[Lda] [datetime2] NULL,
[CaseNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RequestedBy] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HaveBackup] [bit] NULL CONSTRAINT [DF_syStudentStatusChanges_HaveBackup] DEFAULT ((0)),
[HaveClientConfirmation] [bit] NULL CONSTRAINT [DF_syStudentStatusChanges_HaveClientConfirmation] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
  
  
--==========================================================================================  
-- TRIGGER SyStudentStatusChanges_Audit_Delete  
-- INSERT  add Audit History when delete records in SyStudentStatusChanges  
--==========================================================================================  
CREATE TRIGGER [dbo].[SyStudentStatusChanges_Audit_Delete] ON [dbo].[syStudentStatusChanges]
    FOR DELETE
AS
    BEGIN  
        SET NOCOUNT ON;  
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;  
        DECLARE @EventRows AS INT;  
        DECLARE @EventDate AS DATETIME;  
        DECLARE @UserName AS VARCHAR(50);  
	 
        SET @AuditHistId = NEWID();  
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Deleted
                         );  
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Deleted
                         );  
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Deleted
                        );  
  
        IF @EventRows > 0
            BEGIN  
                EXEC fmAuditHistAdd @AuditHistId,'SyStudentStatusChanges','D',@EventRows,@EventDate,@UserName;  
                -- StuEnrollId  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue 
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentStatusChangeId
                               ,'StuEnrollId'
                               ,CONVERT(VARCHAR(MAX),Old.StuEnrollId)
                        FROM    Deleted AS Old;  
                -- OrigStatusId  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue 
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentStatusChangeId
                               ,'OrigStatusId'
                               ,CONVERT(VARCHAR(MAX),Old.OrigStatusId)
                        FROM    Deleted AS Old;  
                -- NewStatusId  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue 
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentStatusChangeId
                               ,'NewStatusId'
                               ,CONVERT(VARCHAR(MAX),Old.NewStatusId)
                        FROM    Deleted AS Old;  
                -- CampusId  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue 
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentStatusChangeId
                               ,'CampusId'
                               ,CONVERT(VARCHAR(MAX),Old.CampusId)
                        FROM    Deleted AS Old;  
                -- ModDate  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue 
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentStatusChangeId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                        FROM    Deleted AS Old;  
                -- ModUser  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue 
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentStatusChangeId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),Old.ModUser)
                        FROM    Deleted AS Old;  
                -- IsReversal  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue 
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentStatusChangeId
                               ,'IsReversal'
                               ,CONVERT(VARCHAR(MAX),Old.IsReversal)
                        FROM    Deleted AS Old;  
                -- DropReasonId  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue 
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentStatusChangeId
                               ,'DropReasonId'
                               ,CONVERT(VARCHAR(MAX),Old.DropReasonId)
                        FROM    Deleted AS Old;  
                -- DateOfChange  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue 
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentStatusChangeId
                               ,'DateOfChange'
                               ,CONVERT(VARCHAR(MAX),Old.DateOfChange,121)
                        FROM    Deleted AS Old;  
                -- Lda  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue 
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentStatusChangeId
                               ,'Lda'
                               ,CONVERT(VARCHAR(MAX),Old.Lda,121)
                        FROM    Deleted AS Old;  
                -- CaseNumber  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue 
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentStatusChangeId
                               ,'CaseNumber'
                               ,CONVERT(VARCHAR(MAX),Old.CaseNumber)
                        FROM    Deleted AS Old;  
                -- RequestedBy  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue 
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentStatusChangeId
                               ,'RequestedBy'
                               ,CONVERT(VARCHAR(MAX),Old.RequestedBy)
                        FROM    Deleted AS Old;  
                -- HaveBackup  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue 
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentStatusChangeId
                               ,'HaveBackup'
                               ,CONVERT(VARCHAR(MAX),Old.HaveBackup)
                        FROM    Deleted AS Old;  
                -- HaveClientConfirmation  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue 
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentStatusChangeId
                               ,'HaveClientConfirmation'
                               ,CONVERT(VARCHAR(MAX),Old.HaveClientConfirmation)
                        FROM    Deleted AS Old;  
            END;  
        SET NOCOUNT OFF;  
    END;  
--==========================================================================================  
-- END TRIGGER SyStudentStatusChanges_Audit_Delete  
--==========================================================================================  
 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
  
  
--==========================================================================================  
-- TRIGGER SyStudentStatusChanges_Audit_Insert  
-- INSERT  add Audit History when insert records in SyStudentStatusChanges  
--==========================================================================================  
CREATE TRIGGER [dbo].[SyStudentStatusChanges_Audit_Insert] ON [dbo].[syStudentStatusChanges]
    FOR INSERT
AS
    BEGIN  
        SET NOCOUNT ON;  
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;  
        DECLARE @EventRows AS INT;  
        DECLARE @EventDate AS DATETIME;  
        DECLARE @UserName AS VARCHAR(50);  
	 
        SET @AuditHistId = NEWID();  
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );  
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );  
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );  
  
        IF @EventRows > 0
            BEGIN  
                EXEC fmAuditHistAdd @AuditHistId,'SyStudentStatusChanges','I',@EventRows,@EventDate,@UserName;  
                -- StuEnrollId  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue 
                        )
                        SELECT  @AuditHistId
                               ,New.StudentStatusChangeId
                               ,'StuEnrollId'
                               ,CONVERT(VARCHAR(MAX),New.StuEnrollId)
                        FROM    Inserted AS New;  
                -- OrigStatusId  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue 
                        )
                        SELECT  @AuditHistId
                               ,New.StudentStatusChangeId
                               ,'OrigStatusId'
                               ,CONVERT(VARCHAR(MAX),New.OrigStatusId)
                        FROM    Inserted AS New;  
                -- NewStatusId  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue 
                        )
                        SELECT  @AuditHistId
                               ,New.StudentStatusChangeId
                               ,'NewStatusId'
                               ,CONVERT(VARCHAR(MAX),New.NewStatusId)
                        FROM    Inserted AS New;  
                -- CampusId  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue 
                        )
                        SELECT  @AuditHistId
                               ,New.StudentStatusChangeId
                               ,'CampusId'
                               ,CONVERT(VARCHAR(MAX),New.CampusId)
                        FROM    Inserted AS New;  
                -- ModDate  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue 
                        )
                        SELECT  @AuditHistId
                               ,New.StudentStatusChangeId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                        FROM    Inserted AS New;  
                -- ModUser  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue 
                        )
                        SELECT  @AuditHistId
                               ,New.StudentStatusChangeId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),New.ModUser)
                        FROM    Inserted AS New;  
                -- IsReversal  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue 
                        )
                        SELECT  @AuditHistId
                               ,New.StudentStatusChangeId
                               ,'IsReversal'
                               ,CONVERT(VARCHAR(MAX),New.IsReversal)
                        FROM    Inserted AS New;  
                -- DropReasonId  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue 
                        )
                        SELECT  @AuditHistId
                               ,New.StudentStatusChangeId
                               ,'DropReasonId'
                               ,CONVERT(VARCHAR(MAX),New.DropReasonId)
                        FROM    Inserted AS New;  
                -- DateOfChange  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue 
                        )
                        SELECT  @AuditHistId
                               ,New.StudentStatusChangeId
                               ,'DateOfChange'
                               ,CONVERT(VARCHAR(MAX),New.DateOfChange,121)
                        FROM    Inserted AS New;  
                -- Lda  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue 
                        )
                        SELECT  @AuditHistId
                               ,New.StudentStatusChangeId
                               ,'Lda'
                               ,CONVERT(VARCHAR(MAX),New.Lda,121)
                        FROM    Inserted AS New;  
                -- CaseNumber  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue 
                        )
                        SELECT  @AuditHistId
                               ,New.StudentStatusChangeId
                               ,'CaseNumber'
                               ,CONVERT(VARCHAR(MAX),New.CaseNumber)
                        FROM    Inserted AS New;  
                -- RequestedBy  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue 
                        )
                        SELECT  @AuditHistId
                               ,New.StudentStatusChangeId
                               ,'RequestedBy'
                               ,CONVERT(VARCHAR(MAX),New.RequestedBy)
                        FROM    Inserted AS New;  
                -- HaveBackup  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue 
                        )
                        SELECT  @AuditHistId
                               ,New.StudentStatusChangeId
                               ,'HaveBackup'
                               ,CONVERT(VARCHAR(MAX),New.HaveBackup)
                        FROM    Inserted AS New;  
                -- HaveClientConfirmation  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue 
                        )
                        SELECT  @AuditHistId
                               ,New.StudentStatusChangeId
                               ,'HaveClientConfirmation'
                               ,CONVERT(VARCHAR(MAX),New.HaveClientConfirmation)
                        FROM    Inserted AS New;  
            END;  
        SET NOCOUNT OFF;  
    END;  
--==========================================================================================  
-- END TRIGGER SyStudentStatusChanges_Audit_Insert  
--==========================================================================================  
 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
  
--==========================================================================================  
-- TRIGGER SyStudentStatusChanges_Audit_Update  
-- UPDATE  add Audit History when update fields in SyStudentStatusChanges  
--==========================================================================================  
CREATE TRIGGER [dbo].[SyStudentStatusChanges_Audit_Update] ON [dbo].[syStudentStatusChanges]
    FOR UPDATE
AS
    BEGIN  
        SET NOCOUNT ON;  
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;  
        DECLARE @EventRows AS INT;  
        DECLARE @EventDate AS DATETIME;  
        DECLARE @UserName AS VARCHAR(50);  
	 
        SET @AuditHistId = NEWID();  
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );  
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );  
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );  
  
        IF @EventRows > 0
            BEGIN  
                EXEC fmAuditHistAdd @AuditHistId,'SyStudentStatusChanges','U',@EventRows,@EventDate,@UserName;  
                -- StuEnrollId  
                IF UPDATE(StuEnrollId)
                    BEGIN  
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue 
                                )
                                SELECT  @AuditHistId
                                       ,New.StudentStatusChangeId
                                       ,'StuEnrollId'
                                       ,CONVERT(VARCHAR(MAX),Old.StuEnrollId)
                                       ,CONVERT(VARCHAR(MAX),New.StuEnrollId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StudentStatusChangeId = New.StudentStatusChangeId
                                WHERE   Old.StuEnrollId <> New.StuEnrollId
                                        OR (
                                             Old.StuEnrollId IS NULL
                                             AND New.StuEnrollId IS NOT NULL
                                           )
                                        OR (
                                             New.StuEnrollId IS NULL
                                             AND Old.StuEnrollId IS NOT NULL
                                           );  
                    END;  
                -- OrigStatusId  
                IF UPDATE(OrigStatusId)
                    BEGIN  
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue 
                                )
                                SELECT  @AuditHistId
                                       ,New.StudentStatusChangeId
                                       ,'OrigStatusId'
                                       ,CONVERT(VARCHAR(MAX),Old.OrigStatusId)
                                       ,CONVERT(VARCHAR(MAX),New.OrigStatusId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StudentStatusChangeId = New.StudentStatusChangeId
                                WHERE   Old.OrigStatusId <> New.OrigStatusId
                                        OR (
                                             Old.OrigStatusId IS NULL
                                             AND New.OrigStatusId IS NOT NULL
                                           )
                                        OR (
                                             New.OrigStatusId IS NULL
                                             AND Old.OrigStatusId IS NOT NULL
                                           );  
                    END;  
                -- NewStatusId  
                IF UPDATE(NewStatusId)
                    BEGIN  
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue 
                                )
                                SELECT  @AuditHistId
                                       ,New.StudentStatusChangeId
                                       ,'NewStatusId'
                                       ,CONVERT(VARCHAR(MAX),Old.NewStatusId)
                                       ,CONVERT(VARCHAR(MAX),New.NewStatusId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StudentStatusChangeId = New.StudentStatusChangeId
                                WHERE   Old.NewStatusId <> New.NewStatusId
                                        OR (
                                             Old.NewStatusId IS NULL
                                             AND New.NewStatusId IS NOT NULL
                                           )
                                        OR (
                                             New.NewStatusId IS NULL
                                             AND Old.NewStatusId IS NOT NULL
                                           );  
                    END;  
                -- CampusId  
                IF UPDATE(CampusId)
                    BEGIN  
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue 
                                )
                                SELECT  @AuditHistId
                                       ,New.StudentStatusChangeId
                                       ,'CampusId'
                                       ,CONVERT(VARCHAR(MAX),Old.CampusId)
                                       ,CONVERT(VARCHAR(MAX),New.CampusId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StudentStatusChangeId = New.StudentStatusChangeId
                                WHERE   Old.CampusId <> New.CampusId
                                        OR (
                                             Old.CampusId IS NULL
                                             AND New.CampusId IS NOT NULL
                                           )
                                        OR (
                                             New.CampusId IS NULL
                                             AND Old.CampusId IS NOT NULL
                                           );  
                    END;  
                -- ModDate  
                IF UPDATE(ModDate)
                    BEGIN  
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue 
                                )
                                SELECT  @AuditHistId
                                       ,New.StudentStatusChangeId
                                       ,'ModDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StudentStatusChangeId = New.StudentStatusChangeId
                                WHERE   Old.ModDate <> New.ModDate
                                        OR (
                                             Old.ModDate IS NULL
                                             AND New.ModDate IS NOT NULL
                                           )
                                        OR (
                                             New.ModDate IS NULL
                                             AND Old.ModDate IS NOT NULL
                                           );  
                    END;  
                -- ModUser  
                IF UPDATE(ModUser)
                    BEGIN  
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue 
                                )
                                SELECT  @AuditHistId
                                       ,New.StudentStatusChangeId
                                       ,'ModUser'
                                       ,CONVERT(VARCHAR(MAX),Old.ModUser)
                                       ,CONVERT(VARCHAR(MAX),New.ModUser)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StudentStatusChangeId = New.StudentStatusChangeId
                                WHERE   Old.ModUser <> New.ModUser
                                        OR (
                                             Old.ModUser IS NULL
                                             AND New.ModUser IS NOT NULL
                                           )
                                        OR (
                                             New.ModUser IS NULL
                                             AND Old.ModUser IS NOT NULL
                                           );  
                    END;  
                -- IsReversal  
                IF UPDATE(IsReversal)
                    BEGIN  
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue 
                                )
                                SELECT  @AuditHistId
                                       ,New.StudentStatusChangeId
                                       ,'IsReversal'
                                       ,CONVERT(VARCHAR(MAX),Old.IsReversal)
                                       ,CONVERT(VARCHAR(MAX),New.IsReversal)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StudentStatusChangeId = New.StudentStatusChangeId
                                WHERE   Old.IsReversal <> New.IsReversal
                                        OR (
                                             Old.IsReversal IS NULL
                                             AND New.IsReversal IS NOT NULL
                                           )
                                        OR (
                                             New.IsReversal IS NULL
                                             AND Old.IsReversal IS NOT NULL
                                           );  
                    END;  
                -- DropReasonId  
                IF UPDATE(DropReasonId)
                    BEGIN  
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue 
                                )
                                SELECT  @AuditHistId
                                       ,New.StudentStatusChangeId
                                       ,'DropReasonId'
                                       ,CONVERT(VARCHAR(MAX),Old.DropReasonId)
                                       ,CONVERT(VARCHAR(MAX),New.DropReasonId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StudentStatusChangeId = New.StudentStatusChangeId
                                WHERE   Old.DropReasonId <> New.DropReasonId
                                        OR (
                                             Old.DropReasonId IS NULL
                                             AND New.DropReasonId IS NOT NULL
                                           )
                                        OR (
                                             New.DropReasonId IS NULL
                                             AND Old.DropReasonId IS NOT NULL
                                           );  
                    END;  
                -- DateOfChange  
                IF UPDATE(DateOfChange)
                    BEGIN  
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue 
                                )
                                SELECT  @AuditHistId
                                       ,New.StudentStatusChangeId
                                       ,'DateOfChange'
                                       ,CONVERT(VARCHAR(MAX),Old.DateOfChange,121)
                                       ,CONVERT(VARCHAR(MAX),New.DateOfChange,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StudentStatusChangeId = New.StudentStatusChangeId
                                WHERE   Old.DateOfChange <> New.DateOfChange
                                        OR (
                                             Old.DateOfChange IS NULL
                                             AND New.DateOfChange IS NOT NULL
                                           )
                                        OR (
                                             New.DateOfChange IS NULL
                                             AND Old.DateOfChange IS NOT NULL
                                           );  
                    END;  
                -- Lda  
                IF UPDATE(Lda)
                    BEGIN  
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue 
                                )
                                SELECT  @AuditHistId
                                       ,New.StudentStatusChangeId
                                       ,'Lda'
                                       ,CONVERT(VARCHAR(MAX),Old.Lda,121)
                                       ,CONVERT(VARCHAR(MAX),New.Lda,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StudentStatusChangeId = New.StudentStatusChangeId
                                WHERE   Old.Lda <> New.Lda
                                        OR (
                                             Old.Lda IS NULL
                                             AND New.Lda IS NOT NULL
                                           )
                                        OR (
                                             New.Lda IS NULL
                                             AND Old.Lda IS NOT NULL
                                           );  
                    END;  
                -- CaseNumber  
                IF UPDATE(CaseNumber)
                    BEGIN  
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue 
                                )
                                SELECT  @AuditHistId
                                       ,New.StudentStatusChangeId
                                       ,'CaseNumber'
                                       ,CONVERT(VARCHAR(MAX),Old.CaseNumber)
                                       ,CONVERT(VARCHAR(MAX),New.CaseNumber)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StudentStatusChangeId = New.StudentStatusChangeId
                                WHERE   Old.CaseNumber <> New.CaseNumber
                                        OR (
                                             Old.CaseNumber IS NULL
                                             AND New.CaseNumber IS NOT NULL
                                           )
                                        OR (
                                             New.CaseNumber IS NULL
                                             AND Old.CaseNumber IS NOT NULL
                                           );  
                    END;  
                -- RequestedBy  
                IF UPDATE(RequestedBy)
                    BEGIN  
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue 
                                )
                                SELECT  @AuditHistId
                                       ,New.StudentStatusChangeId
                                       ,'RequestedBy'
                                       ,CONVERT(VARCHAR(MAX),Old.RequestedBy)
                                       ,CONVERT(VARCHAR(MAX),New.RequestedBy)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StudentStatusChangeId = New.StudentStatusChangeId
                                WHERE   Old.RequestedBy <> New.RequestedBy
                                        OR (
                                             Old.RequestedBy IS NULL
                                             AND New.RequestedBy IS NOT NULL
                                           )
                                        OR (
                                             New.RequestedBy IS NULL
                                             AND Old.RequestedBy IS NOT NULL
                                           );  
                    END;  
                -- HaveBackup  
                IF UPDATE(HaveBackup)
                    BEGIN  
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue 
                                )
                                SELECT  @AuditHistId
                                       ,New.StudentStatusChangeId
                                       ,'HaveBackup'
                                       ,CONVERT(VARCHAR(MAX),Old.HaveBackup)
                                       ,CONVERT(VARCHAR(MAX),New.HaveBackup)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StudentStatusChangeId = New.StudentStatusChangeId
                                WHERE   Old.HaveBackup <> New.HaveBackup
                                        OR (
                                             Old.HaveBackup IS NULL
                                             AND New.HaveBackup IS NOT NULL
                                           )
                                        OR (
                                             New.HaveBackup IS NULL
                                             AND Old.HaveBackup IS NOT NULL
                                           );  
                    END;  
                -- HaveClientConfirmation  
                IF UPDATE(HaveClientConfirmation)
                    BEGIN  
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue 
                                )
                                SELECT  @AuditHistId
                                       ,New.StudentStatusChangeId
                                       ,'HaveClientConfirmation'
                                       ,CONVERT(VARCHAR(MAX),Old.HaveClientConfirmation)
                                       ,CONVERT(VARCHAR(MAX),New.HaveClientConfirmation)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StudentStatusChangeId = New.StudentStatusChangeId
                                WHERE   Old.HaveClientConfirmation <> New.HaveClientConfirmation
                                        OR (
                                             Old.HaveClientConfirmation IS NULL
                                             AND New.HaveClientConfirmation IS NOT NULL
                                           )
                                        OR (
                                             New.HaveClientConfirmation IS NULL
                                             AND Old.HaveClientConfirmation IS NOT NULL
                                           );  
                    END;  
            END;  
        SET NOCOUNT OFF;  
    END;  
--==========================================================================================  
-- END TRIGGER SyStudentStatusChanges_Audit_Update  
--==========================================================================================  
 
 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


--==========================================================================================  
-- TRIGGER [SyStudentStatusChanges_RemoveScheduledForTerminatedOrGrad]e  
-- UPDATE,INSERT  Remove Scheduled and Absent Hours After LDA 
--==========================================================================================  
CREATE   TRIGGER [dbo].[SyStudentStatusChanges_RemoveScheduledForTerminatedOrGrad]
ON [dbo].[syStudentStatusChanges]
FOR UPDATE, INSERT
AS
BEGIN

    SET NOCOUNT ON;
    --Remove Scheduled and Absent Hours After LDA

    BEGIN
		
        UPDATE asca
        SET SchedHoursOnTermination = SchedHours
        FROM arStudentClockAttendance asca
            JOIN syStudentStatusChanges ssc ON ssc.StuEnrollId = asca.StuEnrollId
            JOIN Inserted ins ON ins.StuEnrollId = asca.StuEnrollId
			JOIN dbo.syStatusCodes sco ON sco.StatusCodeId = ins.OrigStatusId
			JOIN dbo.syStatusCodes scn ON scn.StatusCodeId = ins.NewStatusId
			JOIN dbo.sySysStatus sst ON sst.SysStatusId = sco.SysStatusId
        WHERE scn.SysStatusId IN ( 14, 12 )
			  AND sst.InSchool = 1 
              AND RecordDate >
              (
                  SELECT dbo.GetLDA(ssc.StuEnrollId)
              );


        UPDATE asca
        SET SchedHours = 0
        FROM arStudentClockAttendance asca
            JOIN syStudentStatusChanges ssc ON ssc.StuEnrollId = asca.StuEnrollId
            JOIN Inserted ins ON ins.StuEnrollId = asca.StuEnrollId
            JOIN syStatusCodes sc ON sc.StatusCodeId = ins.NewStatusId
        WHERE sc.SysStatusId IN ( 14, 12 )
              AND RecordDate >
              (
                  SELECT dbo.GetLDA(ssc.StuEnrollId)
              );

    END;


    SET NOCOUNT OFF;
END;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--==========================================================================================  
-- TRIGGER [SyStudentStatusChanges_RestoreSchedHoursAfterUndoTermination]  
-- DELETE  Restore Scheduled Hours After Undoing Termination
--========================================================================================== 
CREATE TRIGGER [dbo].[SyStudentStatusChanges_RestoreSchedHoursAfterUndoTermination]
ON [dbo].[syStudentStatusChanges]
FOR DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    BEGIN
	
        UPDATE asca
        SET SchedHours = 
			CASE  
                WHEN SchedHoursOnTermination > 0.0 THEN SchedHoursOnTermination 
                ELSE SchedHours
            END
			,asca.SchedHoursOnTermination = 0
        FROM arStudentClockAttendance asca
            JOIN syStudentStatusChanges ssc ON ssc.StuEnrollId = asca.StuEnrollId
            JOIN Deleted del ON del.StuEnrollId = asca.StuEnrollId
			JOIN dbo.syStatusCodes sco ON sco.StatusCodeId = del.OrigStatusId
			JOIN dbo.syStatusCodes scn ON scn.StatusCodeId = del.NewStatusId
			JOIN dbo.sySysStatus sst ON sst.SysStatusId = sco.SysStatusId
        WHERE scn.SysStatusId IN ( 14, 12 )
			  AND sst.InSchool = 1 
              AND RecordDate >
              (
                  SELECT dbo.GetLDA(ssc.StuEnrollId)
              );
	END
	
    SET NOCOUNT OFF;
END
GO
ALTER TABLE [dbo].[syStudentStatusChanges] ADD CONSTRAINT [PK_syStudentStatusChanges_StudentStatusChangeId] PRIMARY KEY CLUSTERED  ([StudentStatusChangeId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_syStudentStatusChanges_StuEnrollId] ON [dbo].[syStudentStatusChanges] ([StuEnrollId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syStudentStatusChanges] ADD CONSTRAINT [FK_syStudentStatusChanges_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY ([StuEnrollId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
ALTER TABLE [dbo].[syStudentStatusChanges] ADD CONSTRAINT [FK_syStudentStatusChanges_syCampuses_CampusId_CampusId] FOREIGN KEY ([CampusId]) REFERENCES [dbo].[syCampuses] ([CampusId])
GO
ALTER TABLE [dbo].[syStudentStatusChanges] ADD CONSTRAINT [FK_syStudentStatusChanges_syStatusCodes_NewStatusId_StatusCodeId] FOREIGN KEY ([NewStatusId]) REFERENCES [dbo].[syStatusCodes] ([StatusCodeId])
GO
ALTER TABLE [dbo].[syStudentStatusChanges] ADD CONSTRAINT [FK_syStudentStatusChanges_syStatusCodes_OrigStatusId_StatusCodeId] FOREIGN KEY ([OrigStatusId]) REFERENCES [dbo].[syStatusCodes] ([StatusCodeId])
GO
