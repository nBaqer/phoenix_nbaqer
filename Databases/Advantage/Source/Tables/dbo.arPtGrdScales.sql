CREATE TABLE [dbo].[arPtGrdScales]
(
[PtGrdScaleId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arPtGrdScales_PtGrdScaleId] DEFAULT (newid()),
[PtGrdScaleDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PtGrdScalePerc] [decimal] (4, 0) NOT NULL,
[Letter] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GPA] [decimal] (18, 0) NOT NULL,
[CredtsEarnd] [tinyint] NOT NULL,
[CredtsAttemptd] [tinyint] NOT NULL,
[IncInGPA] [bit] NOT NULL,
[IncInSAP] [bit] NOT NULL,
[TransGrd] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arPtGrdScales] ADD CONSTRAINT [PK_arPtGrdScales_PtGrdScaleId] PRIMARY KEY CLUSTERED  ([PtGrdScaleId]) ON [PRIMARY]
GO
