CREATE TABLE [dbo].[syFields]
(
[FldId] [int] NOT NULL,
[FldName] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FldTypeId] [int] NOT NULL,
[FldLen] [int] NOT NULL,
[DDLId] [int] NULL,
[DerivedFld] [bit] NULL CONSTRAINT [DF_syFields_DerivedFld] DEFAULT ((0)),
[SchlReq] [bit] NULL,
[LogChanges] [bit] NULL,
[Mask] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syFields] ADD CONSTRAINT [PK_syFields_FldId] PRIMARY KEY CLUSTERED  ([FldId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syFields] ADD CONSTRAINT [FK_syFields_syFieldTypes_FldTypeId_FldTypeId] FOREIGN KEY ([FldTypeId]) REFERENCES [dbo].[syFieldTypes] ([FldTypeId])
GO
GRANT SELECT ON  [dbo].[syFields] TO [AdvantageRole]
GO
