CREATE TABLE [dbo].[syReports]
(
[ReportId] [uniqueidentifier] NOT NULL,
[ResourceId] [int] NOT NULL,
[ReportName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReportDescription] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RDLName] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AssemblyFilePath] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ReportClass] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreationMethod] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WebServiceUrl] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RecordLimit] [bigint] NOT NULL,
[AllowedExportTypes] [int] NOT NULL,
[ReportTabLayout] [int] NULL,
[ShowPerformanceMsg] [bit] NULL CONSTRAINT [DF_syReports_ShowPerformanceMsg] DEFAULT ((0)),
[ShowFilterMode] [bit] NULL CONSTRAINT [DF_syReports_ShowFilterMode] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syReports] ADD CONSTRAINT [PK_syReports_ReportId] PRIMARY KEY CLUSTERED  ([ReportId]) ON [PRIMARY]
GO
