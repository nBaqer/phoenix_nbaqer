CREATE TABLE [dbo].[arTriggerOffsetTyps]
(
[TrigOffsetTypeId] [int] NOT NULL,
[TrigOffTypDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arTriggerOffsetTyps] ADD CONSTRAINT [PK_arTriggerOffsetTyps_TrigOffsetTypeId] PRIMARY KEY CLUSTERED  ([TrigOffsetTypeId]) ON [PRIMARY]
GO
