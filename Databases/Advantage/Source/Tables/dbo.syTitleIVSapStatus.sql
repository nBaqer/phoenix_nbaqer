CREATE TABLE [dbo].[syTitleIVSapStatus]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreatedById] [uniqueidentifier] NOT NULL,
[CreatedDate] [datetime] NULL CONSTRAINT [DF_syTitleIVSapStatus_CreatedDate] DEFAULT (getdate()),
[UpdatedById] [uniqueidentifier] NULL,
[UpdatedDate] [datetime] NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[DefaultMessage] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syTitleIVSapStatus] ADD CONSTRAINT [PK_syTitleIVSapStatus_Id] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syTitleIVSapStatus] ADD CONSTRAINT [FK_syTitleIVSapStatus_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
ALTER TABLE [dbo].[syTitleIVSapStatus] ADD CONSTRAINT [FK_syTitleIVSapStatus_syUsers_CreatedBy_UserId] FOREIGN KEY ([CreatedById]) REFERENCES [dbo].[syUsers] ([UserId])
GO
ALTER TABLE [dbo].[syTitleIVSapStatus] ADD CONSTRAINT [FK_syTitleIVSapStatus_syUsers_UpdatedBy_UserId] FOREIGN KEY ([UpdatedById]) REFERENCES [dbo].[syUsers] ([UserId])
GO
