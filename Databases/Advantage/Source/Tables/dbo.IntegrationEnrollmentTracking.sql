CREATE TABLE [dbo].[IntegrationEnrollmentTracking]
(
[EnrollmentTrackingId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_IntegrationEnrollmentTracking_EnrollmentTrackingId] DEFAULT (newid()),
[StuEnrollId] [uniqueidentifier] NOT NULL,
[EnrollmentStatus] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NOT NULL,
[ModUser] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Processed] [bit] NOT NULL CONSTRAINT [DF_IntegrationEnrollmentTracking_Processed] DEFAULT ((0)),
[RetryCount] [int] NOT NULL CONSTRAINT [DF_IntegrationEnrollmentTracking_RetryCount] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[IntegrationEnrollmentTracking] ADD CONSTRAINT [PK_IntegrationEnrollmentTracking_EnrollmentTrackingId] PRIMARY KEY CLUSTERED  ([EnrollmentTrackingId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[IntegrationEnrollmentTracking] ADD CONSTRAINT [FK_IntegrationEnrollmentTracking_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY ([StuEnrollId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
