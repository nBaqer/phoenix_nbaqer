CREATE TABLE [dbo].[arProgVerDef]
(
[ProgVerDefId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arProgVerDef_ProgVerDefId] DEFAULT (newid()),
[PrgVerId] [uniqueidentifier] NOT NULL,
[ReqId] [uniqueidentifier] NOT NULL,
[ReqSeq] [int] NOT NULL,
[IsRequired] [bit] NOT NULL,
[Cnt] [smallint] NULL,
[Hours] [decimal] (18, 0) NULL,
[TrkForCompletion] [bit] NOT NULL CONSTRAINT [DF_arProgVerDef_TrkForCompletion] DEFAULT ((1)),
[Credits] [decimal] (18, 0) NULL,
[GrdSysDetailId] [uniqueidentifier] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL,
[TermNo] [int] NULL,
[CourseWeight] [float] NOT NULL CONSTRAINT [DF_arProgVerDef_CourseWeight] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[arProgVerDef_Audit_Delete] ON [dbo].[arProgVerDef]
    FOR DELETE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arProgVerDef','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ProgVerDefId
                               ,'Cnt'
                               ,CONVERT(VARCHAR(8000),Old.Cnt,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ProgVerDefId
                               ,'ReqId'
                               ,CONVERT(VARCHAR(8000),Old.ReqId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ProgVerDefId
                               ,'PrgVerId'
                               ,CONVERT(VARCHAR(8000),Old.PrgVerId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ProgVerDefId
                               ,'GrdSysDetailId'
                               ,CONVERT(VARCHAR(8000),Old.GrdSysDetailId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ProgVerDefId
                               ,'ReqSeq'
                               ,CONVERT(VARCHAR(8000),Old.ReqSeq,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ProgVerDefId
                               ,'TrkForCompletion'
                               ,CONVERT(VARCHAR(8000),Old.TrkForCompletion,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ProgVerDefId
                               ,'IsRequired'
                               ,CONVERT(VARCHAR(8000),Old.IsRequired,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ProgVerDefId
                               ,'Hours'
                               ,CONVERT(VARCHAR(8000),Old.Hours,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ProgVerDefId
                               ,'Credits'
                               ,CONVERT(VARCHAR(8000),Old.Credits,121)
                        FROM    Deleted Old; 
            END; 
        END;



    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[arProgVerDef_Audit_Insert] ON [dbo].[arProgVerDef]
    FOR INSERT
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arProgVerDef','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ProgVerDefId
                               ,'Cnt'
                               ,CONVERT(VARCHAR(8000),New.Cnt,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ProgVerDefId
                               ,'ReqId'
                               ,CONVERT(VARCHAR(8000),New.ReqId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ProgVerDefId
                               ,'PrgVerId'
                               ,CONVERT(VARCHAR(8000),New.PrgVerId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ProgVerDefId
                               ,'GrdSysDetailId'
                               ,CONVERT(VARCHAR(8000),New.GrdSysDetailId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ProgVerDefId
                               ,'ReqSeq'
                               ,CONVERT(VARCHAR(8000),New.ReqSeq,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ProgVerDefId
                               ,'TrkForCompletion'
                               ,CONVERT(VARCHAR(8000),New.TrkForCompletion,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ProgVerDefId
                               ,'IsRequired'
                               ,CONVERT(VARCHAR(8000),New.IsRequired,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ProgVerDefId
                               ,'Hours'
                               ,CONVERT(VARCHAR(8000),New.Hours,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ProgVerDefId
                               ,'Credits'
                               ,CONVERT(VARCHAR(8000),New.Credits,121)
                        FROM    Inserted New; 
            END; 
        END;



    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[arProgVerDef_Audit_Update] ON [dbo].[arProgVerDef]
    FOR UPDATE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arProgVerDef','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(Cnt)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ProgVerDefId
                                   ,'Cnt'
                                   ,CONVERT(VARCHAR(8000),Old.Cnt,121)
                                   ,CONVERT(VARCHAR(8000),New.Cnt,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ProgVerDefId = New.ProgVerDefId
                            WHERE   Old.Cnt <> New.Cnt
                                    OR (
                                         Old.Cnt IS NULL
                                         AND New.Cnt IS NOT NULL
                                       )
                                    OR (
                                         New.Cnt IS NULL
                                         AND Old.Cnt IS NOT NULL
                                       ); 
                IF UPDATE(ReqId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ProgVerDefId
                                   ,'ReqId'
                                   ,CONVERT(VARCHAR(8000),Old.ReqId,121)
                                   ,CONVERT(VARCHAR(8000),New.ReqId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ProgVerDefId = New.ProgVerDefId
                            WHERE   Old.ReqId <> New.ReqId
                                    OR (
                                         Old.ReqId IS NULL
                                         AND New.ReqId IS NOT NULL
                                       )
                                    OR (
                                         New.ReqId IS NULL
                                         AND Old.ReqId IS NOT NULL
                                       ); 
                IF UPDATE(PrgVerId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ProgVerDefId
                                   ,'PrgVerId'
                                   ,CONVERT(VARCHAR(8000),Old.PrgVerId,121)
                                   ,CONVERT(VARCHAR(8000),New.PrgVerId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ProgVerDefId = New.ProgVerDefId
                            WHERE   Old.PrgVerId <> New.PrgVerId
                                    OR (
                                         Old.PrgVerId IS NULL
                                         AND New.PrgVerId IS NOT NULL
                                       )
                                    OR (
                                         New.PrgVerId IS NULL
                                         AND Old.PrgVerId IS NOT NULL
                                       ); 
                IF UPDATE(GrdSysDetailId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ProgVerDefId
                                   ,'GrdSysDetailId'
                                   ,CONVERT(VARCHAR(8000),Old.GrdSysDetailId,121)
                                   ,CONVERT(VARCHAR(8000),New.GrdSysDetailId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ProgVerDefId = New.ProgVerDefId
                            WHERE   Old.GrdSysDetailId <> New.GrdSysDetailId
                                    OR (
                                         Old.GrdSysDetailId IS NULL
                                         AND New.GrdSysDetailId IS NOT NULL
                                       )
                                    OR (
                                         New.GrdSysDetailId IS NULL
                                         AND Old.GrdSysDetailId IS NOT NULL
                                       ); 
                IF UPDATE(ReqSeq)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ProgVerDefId
                                   ,'ReqSeq'
                                   ,CONVERT(VARCHAR(8000),Old.ReqSeq,121)
                                   ,CONVERT(VARCHAR(8000),New.ReqSeq,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ProgVerDefId = New.ProgVerDefId
                            WHERE   Old.ReqSeq <> New.ReqSeq
                                    OR (
                                         Old.ReqSeq IS NULL
                                         AND New.ReqSeq IS NOT NULL
                                       )
                                    OR (
                                         New.ReqSeq IS NULL
                                         AND Old.ReqSeq IS NOT NULL
                                       ); 
                IF UPDATE(TrkForCompletion)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ProgVerDefId
                                   ,'TrkForCompletion'
                                   ,CONVERT(VARCHAR(8000),Old.TrkForCompletion,121)
                                   ,CONVERT(VARCHAR(8000),New.TrkForCompletion,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ProgVerDefId = New.ProgVerDefId
                            WHERE   Old.TrkForCompletion <> New.TrkForCompletion
                                    OR (
                                         Old.TrkForCompletion IS NULL
                                         AND New.TrkForCompletion IS NOT NULL
                                       )
                                    OR (
                                         New.TrkForCompletion IS NULL
                                         AND Old.TrkForCompletion IS NOT NULL
                                       ); 
                IF UPDATE(IsRequired)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ProgVerDefId
                                   ,'IsRequired'
                                   ,CONVERT(VARCHAR(8000),Old.IsRequired,121)
                                   ,CONVERT(VARCHAR(8000),New.IsRequired,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ProgVerDefId = New.ProgVerDefId
                            WHERE   Old.IsRequired <> New.IsRequired
                                    OR (
                                         Old.IsRequired IS NULL
                                         AND New.IsRequired IS NOT NULL
                                       )
                                    OR (
                                         New.IsRequired IS NULL
                                         AND Old.IsRequired IS NOT NULL
                                       ); 
                IF UPDATE(Hours)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ProgVerDefId
                                   ,'Hours'
                                   ,CONVERT(VARCHAR(8000),Old.Hours,121)
                                   ,CONVERT(VARCHAR(8000),New.Hours,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ProgVerDefId = New.ProgVerDefId
                            WHERE   Old.Hours <> New.Hours
                                    OR (
                                         Old.Hours IS NULL
                                         AND New.Hours IS NOT NULL
                                       )
                                    OR (
                                         New.Hours IS NULL
                                         AND Old.Hours IS NOT NULL
                                       ); 
                IF UPDATE(Credits)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ProgVerDefId
                                   ,'Credits'
                                   ,CONVERT(VARCHAR(8000),Old.Credits,121)
                                   ,CONVERT(VARCHAR(8000),New.Credits,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ProgVerDefId = New.ProgVerDefId
                            WHERE   Old.Credits <> New.Credits
                                    OR (
                                         Old.Credits IS NULL
                                         AND New.Credits IS NOT NULL
                                       )
                                    OR (
                                         New.Credits IS NULL
                                         AND Old.Credits IS NOT NULL
                                       ); 
            END; 
        END;



    SET NOCOUNT OFF;

GO
ALTER TABLE [dbo].[arProgVerDef] ADD CONSTRAINT [PK_arProgVerDef_ProgVerDefId] PRIMARY KEY CLUSTERED  ([ProgVerDefId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arProgVerDef] ADD CONSTRAINT [FK_arProgVerDef_arGradeSystemDetails_GrdSysDetailId_GrdSysDetailId] FOREIGN KEY ([GrdSysDetailId]) REFERENCES [dbo].[arGradeSystemDetails] ([GrdSysDetailId])
GO
ALTER TABLE [dbo].[arProgVerDef] ADD CONSTRAINT [FK_arProgVerDef_arPrgVersions_PrgVerId_PrgVerId] FOREIGN KEY ([PrgVerId]) REFERENCES [dbo].[arPrgVersions] ([PrgVerId])
GO
ALTER TABLE [dbo].[arProgVerDef] ADD CONSTRAINT [FK_arProgVerDef_arReqs_ReqId_ReqId] FOREIGN KEY ([ReqId]) REFERENCES [dbo].[arReqs] ([ReqId])
GO
