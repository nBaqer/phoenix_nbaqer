CREATE TABLE [dbo].[arSAP]
(
[SAPId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arSAP_SAPId] DEFAULT (newid()),
[SAPCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SAPDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SAPCriteriaId] [uniqueidentifier] NULL,
[MinTermGPA] [decimal] (6, 2) NULL,
[MinTermAv] [decimal] (18, 0) NULL,
[TermGPAOver] [decimal] (6, 2) NULL,
[TermAvOver] [decimal] (18, 0) NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[TrigUnitTypId] [tinyint] NULL,
[TrigOffsetTypId] [tinyint] NULL,
[CampGrpId] [uniqueidentifier] NULL,
[TerminationProbationCnt] [int] NULL,
[TrackExternAttendance] [bit] NULL CONSTRAINT [DF_arSAP_TrackExternAttendance] DEFAULT ((0)),
[IncludeTransferHours] [bit] NULL,
[FaSapPolicy] [bit] NOT NULL CONSTRAINT [DF_arSAP_FaSapPolicy] DEFAULT ((0)),
[PayOnProbation] [bit] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE TRIGGER [dbo].[arSAP_Audit_Delete] ON [dbo].[arSAP]
    FOR DELETE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arSAP','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SAPId
                               ,'SAPCode'
                               ,CONVERT(VARCHAR(8000),Old.SAPCode,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SAPId
                               ,'SAPDescrip'
                               ,CONVERT(VARCHAR(8000),Old.SAPDescrip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SAPId
                               ,'MinTermGPA'
                               ,CONVERT(VARCHAR(8000),Old.MinTermGPA,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SAPId
                               ,'MinTermAv'
                               ,CONVERT(VARCHAR(8000),Old.MinTermAv,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SAPId
                               ,'TermAvOver'
                               ,CONVERT(VARCHAR(8000),Old.TermAvOver,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SAPId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SAPId
                               ,'SAPCriteriaId'
                               ,CONVERT(VARCHAR(8000),Old.SAPCriteriaId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SAPId
                               ,'TermGPAOver'
                               ,CONVERT(VARCHAR(8000),Old.TermGPAOver,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SAPId
                               ,'TrigUnitTypId'
                               ,CONVERT(VARCHAR(8000),Old.TrigUnitTypId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SAPId
                               ,'TrigOffsetTypId'
                               ,CONVERT(VARCHAR(8000),Old.TrigOffsetTypId,121)
                        FROM    Deleted Old; 
            END; 
        END;



    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE TRIGGER [dbo].[arSAP_Audit_Insert] ON [dbo].[arSAP]
    FOR INSERT
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arSAP','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SAPId
                               ,'SAPCode'
                               ,CONVERT(VARCHAR(8000),New.SAPCode,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SAPId
                               ,'SAPDescrip'
                               ,CONVERT(VARCHAR(8000),New.SAPDescrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SAPId
                               ,'MinTermGPA'
                               ,CONVERT(VARCHAR(8000),New.MinTermGPA,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SAPId
                               ,'MinTermAv'
                               ,CONVERT(VARCHAR(8000),New.MinTermAv,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SAPId
                               ,'TermAvOver'
                               ,CONVERT(VARCHAR(8000),New.TermAvOver,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SAPId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SAPId
                               ,'SAPCriteriaId'
                               ,CONVERT(VARCHAR(8000),New.SAPCriteriaId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SAPId
                               ,'TermGPAOver'
                               ,CONVERT(VARCHAR(8000),New.TermGPAOver,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SAPId
                               ,'TrigUnitTypId'
                               ,CONVERT(VARCHAR(8000),New.TrigUnitTypId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SAPId
                               ,'TrigOffsetTypId'
                               ,CONVERT(VARCHAR(8000),New.TrigOffsetTypId,121)
                        FROM    Inserted New; 
            END; 
        END;



    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE TRIGGER [dbo].[arSAP_Audit_Update] ON [dbo].[arSAP]
    FOR UPDATE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arSAP','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(SAPCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SAPId
                                   ,'SAPCode'
                                   ,CONVERT(VARCHAR(8000),Old.SAPCode,121)
                                   ,CONVERT(VARCHAR(8000),New.SAPCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SAPId = New.SAPId
                            WHERE   Old.SAPCode <> New.SAPCode
                                    OR (
                                         Old.SAPCode IS NULL
                                         AND New.SAPCode IS NOT NULL
                                       )
                                    OR (
                                         New.SAPCode IS NULL
                                         AND Old.SAPCode IS NOT NULL
                                       ); 
                IF UPDATE(SAPDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SAPId
                                   ,'SAPDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.SAPDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.SAPDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SAPId = New.SAPId
                            WHERE   Old.SAPDescrip <> New.SAPDescrip
                                    OR (
                                         Old.SAPDescrip IS NULL
                                         AND New.SAPDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.SAPDescrip IS NULL
                                         AND Old.SAPDescrip IS NOT NULL
                                       ); 
                IF UPDATE(MinTermGPA)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SAPId
                                   ,'MinTermGPA'
                                   ,CONVERT(VARCHAR(8000),Old.MinTermGPA,121)
                                   ,CONVERT(VARCHAR(8000),New.MinTermGPA,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SAPId = New.SAPId
                            WHERE   Old.MinTermGPA <> New.MinTermGPA
                                    OR (
                                         Old.MinTermGPA IS NULL
                                         AND New.MinTermGPA IS NOT NULL
                                       )
                                    OR (
                                         New.MinTermGPA IS NULL
                                         AND Old.MinTermGPA IS NOT NULL
                                       ); 
                IF UPDATE(MinTermAv)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SAPId
                                   ,'MinTermAv'
                                   ,CONVERT(VARCHAR(8000),Old.MinTermAv,121)
                                   ,CONVERT(VARCHAR(8000),New.MinTermAv,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SAPId = New.SAPId
                            WHERE   Old.MinTermAv <> New.MinTermAv
                                    OR (
                                         Old.MinTermAv IS NULL
                                         AND New.MinTermAv IS NOT NULL
                                       )
                                    OR (
                                         New.MinTermAv IS NULL
                                         AND Old.MinTermAv IS NOT NULL
                                       ); 
                IF UPDATE(TermAvOver)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SAPId
                                   ,'TermAvOver'
                                   ,CONVERT(VARCHAR(8000),Old.TermAvOver,121)
                                   ,CONVERT(VARCHAR(8000),New.TermAvOver,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SAPId = New.SAPId
                            WHERE   Old.TermAvOver <> New.TermAvOver
                                    OR (
                                         Old.TermAvOver IS NULL
                                         AND New.TermAvOver IS NOT NULL
                                       )
                                    OR (
                                         New.TermAvOver IS NULL
                                         AND Old.TermAvOver IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SAPId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SAPId = New.SAPId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(SAPCriteriaId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SAPId
                                   ,'SAPCriteriaId'
                                   ,CONVERT(VARCHAR(8000),Old.SAPCriteriaId,121)
                                   ,CONVERT(VARCHAR(8000),New.SAPCriteriaId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SAPId = New.SAPId
                            WHERE   Old.SAPCriteriaId <> New.SAPCriteriaId
                                    OR (
                                         Old.SAPCriteriaId IS NULL
                                         AND New.SAPCriteriaId IS NOT NULL
                                       )
                                    OR (
                                         New.SAPCriteriaId IS NULL
                                         AND Old.SAPCriteriaId IS NOT NULL
                                       ); 
                IF UPDATE(TermGPAOver)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SAPId
                                   ,'TermGPAOver'
                                   ,CONVERT(VARCHAR(8000),Old.TermGPAOver,121)
                                   ,CONVERT(VARCHAR(8000),New.TermGPAOver,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SAPId = New.SAPId
                            WHERE   Old.TermGPAOver <> New.TermGPAOver
                                    OR (
                                         Old.TermGPAOver IS NULL
                                         AND New.TermGPAOver IS NOT NULL
                                       )
                                    OR (
                                         New.TermGPAOver IS NULL
                                         AND Old.TermGPAOver IS NOT NULL
                                       ); 
                IF UPDATE(TrigUnitTypId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SAPId
                                   ,'TrigUnitTypId'
                                   ,CONVERT(VARCHAR(8000),Old.TrigUnitTypId,121)
                                   ,CONVERT(VARCHAR(8000),New.TrigUnitTypId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SAPId = New.SAPId
                            WHERE   Old.TrigUnitTypId <> New.TrigUnitTypId
                                    OR (
                                         Old.TrigUnitTypId IS NULL
                                         AND New.TrigUnitTypId IS NOT NULL
                                       )
                                    OR (
                                         New.TrigUnitTypId IS NULL
                                         AND Old.TrigUnitTypId IS NOT NULL
                                       ); 
                IF UPDATE(TrigOffsetTypId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SAPId
                                   ,'TrigOffsetTypId'
                                   ,CONVERT(VARCHAR(8000),Old.TrigOffsetTypId,121)
                                   ,CONVERT(VARCHAR(8000),New.TrigOffsetTypId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SAPId = New.SAPId
                            WHERE   Old.TrigOffsetTypId <> New.TrigOffsetTypId
                                    OR (
                                         Old.TrigOffsetTypId IS NULL
                                         AND New.TrigOffsetTypId IS NOT NULL
                                       )
                                    OR (
                                         New.TrigOffsetTypId IS NULL
                                         AND Old.TrigOffsetTypId IS NOT NULL
                                       ); 
            END; 
        END;



    SET NOCOUNT OFF;

GO
ALTER TABLE [dbo].[arSAP] ADD CONSTRAINT [PK_arSAP_SAPId] PRIMARY KEY CLUSTERED  ([SAPId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arSAP] ADD CONSTRAINT [FK_arSAP_arTrigOffsetTyps_TrigOffsetTypId_TrigOffsetTypId] FOREIGN KEY ([TrigOffsetTypId]) REFERENCES [dbo].[arTrigOffsetTyps] ([TrigOffsetTypId])
GO
ALTER TABLE [dbo].[arSAP] ADD CONSTRAINT [FK_arSAP_arTrigUnitTyps_TrigUnitTypId_TrigUnitTypId] FOREIGN KEY ([TrigUnitTypId]) REFERENCES [dbo].[arTrigUnitTyps] ([TrigUnitTypId])
GO
ALTER TABLE [dbo].[arSAP] ADD CONSTRAINT [FK_arSAP_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[arSAP] ADD CONSTRAINT [FK_arSAP_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
