CREATE TABLE [dbo].[syTblCaptions]
(
[TblCapId] [int] NOT NULL,
[TblId] [int] NOT NULL,
[LangId] [tinyint] NOT NULL,
[Caption] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TblDescrip] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syTblCaptions] ADD CONSTRAINT [PK_syTblCaptions_TblCapId] PRIMARY KEY CLUSTERED  ([TblCapId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syTblCaptions] ADD CONSTRAINT [FK_syTblCaptions_syTables_TblId_TblId] FOREIGN KEY ([TblId]) REFERENCES [dbo].[syTables] ([TblId])
GO
GRANT SELECT ON  [dbo].[syTblCaptions] TO [AdvantageRole]
GO
