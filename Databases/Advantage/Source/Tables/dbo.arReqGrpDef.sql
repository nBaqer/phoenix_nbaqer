CREATE TABLE [dbo].[arReqGrpDef]
(
[ReqGrpId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arReqGrpDef_ReqGrpId] DEFAULT (newid()),
[GrpId] [uniqueidentifier] NOT NULL,
[ReqId] [uniqueidentifier] NOT NULL,
[ReqSeq] [tinyint] NOT NULL,
[IsRequired] [bit] NOT NULL,
[Cnt] [smallint] NULL,
[Hours] [decimal] (18, 0) NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[arReqGrpDef_Audit_Delete] ON [dbo].[arReqGrpDef]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arReqGrpDef','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ReqGrpId
                               ,'Cnt'
                               ,CONVERT(VARCHAR(8000),Old.Cnt,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ReqGrpId
                               ,'GrpId'
                               ,CONVERT(VARCHAR(8000),Old.GrpId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ReqGrpId
                               ,'ReqId'
                               ,CONVERT(VARCHAR(8000),Old.ReqId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ReqGrpId
                               ,'ReqSeq'
                               ,CONVERT(VARCHAR(8000),Old.ReqSeq,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ReqGrpId
                               ,'IsRequired'
                               ,CONVERT(VARCHAR(8000),Old.IsRequired,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ReqGrpId
                               ,'Hours'
                               ,CONVERT(VARCHAR(8000),Old.Hours,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[arReqGrpDef_Audit_Insert] ON [dbo].[arReqGrpDef]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arReqGrpDef','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ReqGrpId
                               ,'Cnt'
                               ,CONVERT(VARCHAR(8000),New.Cnt,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ReqGrpId
                               ,'GrpId'
                               ,CONVERT(VARCHAR(8000),New.GrpId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ReqGrpId
                               ,'ReqId'
                               ,CONVERT(VARCHAR(8000),New.ReqId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ReqGrpId
                               ,'ReqSeq'
                               ,CONVERT(VARCHAR(8000),New.ReqSeq,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ReqGrpId
                               ,'IsRequired'
                               ,CONVERT(VARCHAR(8000),New.IsRequired,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ReqGrpId
                               ,'Hours'
                               ,CONVERT(VARCHAR(8000),New.Hours,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO
GO

GO

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[arReqGrpDef_Audit_Update] ON [dbo].[arReqGrpDef]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arReqGrpDef','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(Cnt)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ReqGrpId
                                   ,'Cnt'
                                   ,CONVERT(VARCHAR(8000),Old.Cnt,121)
                                   ,CONVERT(VARCHAR(8000),New.Cnt,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ReqGrpId = New.ReqGrpId
                            WHERE   Old.Cnt <> New.Cnt
                                    OR (
                                         Old.Cnt IS NULL
                                         AND New.Cnt IS NOT NULL
                                       )
                                    OR (
                                         New.Cnt IS NULL
                                         AND Old.Cnt IS NOT NULL
                                       ); 
                IF UPDATE(GrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ReqGrpId
                                   ,'GrpId'
                                   ,CONVERT(VARCHAR(8000),Old.GrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.GrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ReqGrpId = New.ReqGrpId
                            WHERE   Old.GrpId <> New.GrpId
                                    OR (
                                         Old.GrpId IS NULL
                                         AND New.GrpId IS NOT NULL
                                       )
                                    OR (
                                         New.GrpId IS NULL
                                         AND Old.GrpId IS NOT NULL
                                       ); 
                IF UPDATE(ReqId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ReqGrpId
                                   ,'ReqId'
                                   ,CONVERT(VARCHAR(8000),Old.ReqId,121)
                                   ,CONVERT(VARCHAR(8000),New.ReqId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ReqGrpId = New.ReqGrpId
                            WHERE   Old.ReqId <> New.ReqId
                                    OR (
                                         Old.ReqId IS NULL
                                         AND New.ReqId IS NOT NULL
                                       )
                                    OR (
                                         New.ReqId IS NULL
                                         AND Old.ReqId IS NOT NULL
                                       ); 
                IF UPDATE(ReqSeq)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ReqGrpId
                                   ,'ReqSeq'
                                   ,CONVERT(VARCHAR(8000),Old.ReqSeq,121)
                                   ,CONVERT(VARCHAR(8000),New.ReqSeq,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ReqGrpId = New.ReqGrpId
                            WHERE   Old.ReqSeq <> New.ReqSeq
                                    OR (
                                         Old.ReqSeq IS NULL
                                         AND New.ReqSeq IS NOT NULL
                                       )
                                    OR (
                                         New.ReqSeq IS NULL
                                         AND Old.ReqSeq IS NOT NULL
                                       ); 
                IF UPDATE(IsRequired)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ReqGrpId
                                   ,'IsRequired'
                                   ,CONVERT(VARCHAR(8000),Old.IsRequired,121)
                                   ,CONVERT(VARCHAR(8000),New.IsRequired,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ReqGrpId = New.ReqGrpId
                            WHERE   Old.IsRequired <> New.IsRequired
                                    OR (
                                         Old.IsRequired IS NULL
                                         AND New.IsRequired IS NOT NULL
                                       )
                                    OR (
                                         New.IsRequired IS NULL
                                         AND Old.IsRequired IS NOT NULL
                                       ); 
                IF UPDATE(Hours)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ReqGrpId
                                   ,'Hours'
                                   ,CONVERT(VARCHAR(8000),Old.Hours,121)
                                   ,CONVERT(VARCHAR(8000),New.Hours,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ReqGrpId = New.ReqGrpId
                            WHERE   Old.Hours <> New.Hours
                                    OR (
                                         Old.Hours IS NULL
                                         AND New.Hours IS NOT NULL
                                       )
                                    OR (
                                         New.Hours IS NULL
                                         AND Old.Hours IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[arReqGrpDef] ADD CONSTRAINT [PK_arReqGrpDef_ReqGrpId] PRIMARY KEY CLUSTERED  ([ReqGrpId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arReqGrpDef] ADD CONSTRAINT [FK_arReqGrpDef_arReqs_GrpId_ReqId] FOREIGN KEY ([GrpId]) REFERENCES [dbo].[arReqs] ([ReqId]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[arReqGrpDef] ADD CONSTRAINT [FK_arReqGrpDef_arReqs_ReqId_ReqId] FOREIGN KEY ([ReqId]) REFERENCES [dbo].[arReqs] ([ReqId])
GO
