CREATE TABLE [dbo].[syWapiAllowedServices]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Url] [varchar] (2048) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsActive] [bit] NOT NULL CONSTRAINT [DF_syWapiAllowedServices_IsActive] DEFAULT ((1))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syWapiAllowedServices] ADD CONSTRAINT [PK_syWapiAllowedServices_Id] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_syWapiAllowedServices_Code] ON [dbo].[syWapiAllowedServices] ([Code]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', 'This table hold the services available thru the web proxy layer.
If a service is not more usable, use IsActive to disable it.
Do not delete rows here, this table works with the log table and should maintain all records 

Actual values:
WAPILOGIN    ''Authentification (Login) in the system''
RSETTING      ''Read the windows service operation settings''
RFLAG            ''Read the on-demand flags''
WLOG        ''Write the log file''
PUSHSTDS    ''Push students information''', 'SCHEMA', N'dbo', 'TABLE', N'syWapiAllowedServices', NULL, NULL
GO
