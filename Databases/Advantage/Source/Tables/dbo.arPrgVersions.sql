--added new columns
CREATE TABLE [dbo].[arPrgVersions]
(
[PrgVerId] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[ProgId] [uniqueidentifier] NULL,
[PrgVerCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NULL,
[CampGrpId] [uniqueidentifier] NULL,
[PrgGrpId] [uniqueidentifier] NULL,
[PrgVerDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DegreeId] [uniqueidentifier] NULL,
[SAPId] [uniqueidentifier] NULL,
[ThGrdScaleId] [uniqueidentifier] NULL,
[TestingModelId] [tinyint] NULL,
[Weeks] [smallint] NOT NULL,
[Terms] [smallint] NOT NULL,
[Hours] [decimal] (9, 2) NOT NULL,
[Credits] [decimal] (9, 2) NOT NULL,
[LTHalfTime] [smallint] NULL,
[HalfTime] [smallint] NULL,
[ThreeQuartTime] [smallint] NULL,
[FullTime] [bit] NOT NULL,
[DeptId] [uniqueidentifier] NOT NULL,
[GrdSystemId] [uniqueidentifier] NOT NULL,
[Weighted GPA] [bit] NULL,
[BillingMethodId] [uniqueidentifier] NULL,
[TuitionEarningId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[AttendanceLevel] [bit] NULL,
[CustomAttendance] [bit] NULL,
[ProgTypId] [uniqueidentifier] NOT NULL,
[IsContinuingEd] [bit] NULL CONSTRAINT [DF_arPrgVersions_IsContinuingEd] DEFAULT ((0)),
[UnitTypeId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_arPrgVersions_UnitTypeId] DEFAULT ('{00000000-0000-0000-0000-000000000000}'),
[TrackTardies] [bit] NOT NULL CONSTRAINT [DF_arPrgVersions_TrackTardies] DEFAULT ((0)),
[TardiesMakingAbsence] [int] NULL,
[SchedMethodId] [int] NOT NULL CONSTRAINT [DF_arPrgVersions_SchedMethodId] DEFAULT ((4)),
[UseTimeClock] [bit] NULL,
[TotalCost] [decimal] (9, 2) NULL CONSTRAINT [DF_arPrgVersions_TotalCost] DEFAULT ((0)),
[AcademicYearLength] [decimal] (9, 2) NULL,
[AcademicYearWeeks] [smallint] NULL,
[PayPeriodPerAcYear] [smallint] NULL,
[FASAPId] [uniqueidentifier] NULL CONSTRAINT [DF_arPrgVersions_FASAPId] DEFAULT (NULL),
[IsBillingMethodCharged] [bit] NOT NULL CONSTRAINT [DF_arPrgVersions_IsBillingMethodCharged] DEFAULT ((0)),
[DoCourseWeightOverallGPA] [bit] NOT NULL CONSTRAINT [DF_arPrgVersions_DoCourseWeightOverallGPA] DEFAULT ((0)),
[ProgramRegistrationType] [int] NULL CONSTRAINT [DF_arPrgVersions_ProgramRegistrationType] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[arPrgVersions_Audit_Delete] ON [dbo].[arPrgVersions]
    FOR DELETE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arPrgVersions','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerId
                               ,'ProgId'
                               ,CONVERT(VARCHAR(8000),Old.ProgId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerId
                               ,'PrgVerCode'
                               ,CONVERT(VARCHAR(8000),Old.PrgVerCode,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerId
                               ,'PrgGrpId'
                               ,CONVERT(VARCHAR(8000),Old.PrgGrpId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerId
                               ,'PrgVerDescrip'
                               ,CONVERT(VARCHAR(8000),Old.PrgVerDescrip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerId
                               ,'DegreeId'
                               ,CONVERT(VARCHAR(8000),Old.DegreeId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerId
                               ,'SAPId'
                               ,CONVERT(VARCHAR(8000),Old.SAPId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerId
                               ,'TestingModelId'
                               ,CONVERT(VARCHAR(8000),Old.TestingModelId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerId
                               ,'Weeks'
                               ,CONVERT(VARCHAR(8000),Old.Weeks,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerId
                               ,'Terms'
                               ,CONVERT(VARCHAR(8000),Old.Terms,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerId
                               ,'Hours'
                               ,CONVERT(VARCHAR(8000),Old.Hours,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerId
                               ,'Credits'
                               ,CONVERT(VARCHAR(8000),Old.Credits,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerId
                               ,'LTHalfTime'
                               ,CONVERT(VARCHAR(8000),Old.LTHalfTime,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerId
                               ,'HalfTime'
                               ,CONVERT(VARCHAR(8000),Old.HalfTime,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerId
                               ,'ThreeQuartTime'
                               ,CONVERT(VARCHAR(8000),Old.ThreeQuartTime,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerId
                               ,'FullTime'
                               ,CONVERT(VARCHAR(8000),Old.FullTime,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerId
                               ,'DeptId'
                               ,CONVERT(VARCHAR(8000),Old.DeptId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerId
                               ,'GrdSystemId'
                               ,CONVERT(VARCHAR(8000),Old.GrdSystemId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerId
                               ,'Weighted GPA'
                               ,CONVERT(VARCHAR(8000),Old.[Weighted GPA],121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerId
                               ,'BillingMethodId'
                               ,CONVERT(VARCHAR(8000),Old.BillingMethodId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerId
                               ,'TuitionEarningId'
                               ,CONVERT(VARCHAR(8000),Old.TuitionEarningId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerId
                               ,'AttendanceLevel'
                               ,CONVERT(VARCHAR(8000),Old.AttendanceLevel,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerId
                               ,'CustomAttendance'
                               ,CONVERT(VARCHAR(8000),Old.CustomAttendance,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerId
                               ,'ProgTypId'
                               ,CONVERT(VARCHAR(8000),Old.ProgTypId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerId
                               ,'IsContinuingEd'
                               ,CONVERT(VARCHAR(8000),Old.IsContinuingEd,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerId
                               ,'UnitTypeId'
                               ,CONVERT(VARCHAR(8000),Old.UnitTypeId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerId
                               ,'TrackTardies'
                               ,CONVERT(VARCHAR(8000),Old.TrackTardies,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerId
                               ,'TardiesMakingAbsence'
                               ,CONVERT(VARCHAR(8000),Old.TardiesMakingAbsence,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerId
                               ,'SchedMethodId'
                               ,CONVERT(VARCHAR(8000),Old.SchedMethodId,121)
                        FROM    Deleted Old; 
            END; 
        END;



    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[arPrgVersions_Audit_Insert] ON [dbo].[arPrgVersions]
    FOR INSERT
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arPrgVersions','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerId
                               ,'ProgId'
                               ,CONVERT(VARCHAR(8000),New.ProgId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerId
                               ,'PrgVerCode'
                               ,CONVERT(VARCHAR(8000),New.PrgVerCode,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerId
                               ,'PrgGrpId'
                               ,CONVERT(VARCHAR(8000),New.PrgGrpId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerId
                               ,'PrgVerDescrip'
                               ,CONVERT(VARCHAR(8000),New.PrgVerDescrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerId
                               ,'DegreeId'
                               ,CONVERT(VARCHAR(8000),New.DegreeId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerId
                               ,'SAPId'
                               ,CONVERT(VARCHAR(8000),New.SAPId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerId
                               ,'TestingModelId'
                               ,CONVERT(VARCHAR(8000),New.TestingModelId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerId
                               ,'Weeks'
                               ,CONVERT(VARCHAR(8000),New.Weeks,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerId
                               ,'Terms'
                               ,CONVERT(VARCHAR(8000),New.Terms,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerId
                               ,'Hours'
                               ,CONVERT(VARCHAR(8000),New.Hours,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerId
                               ,'Credits'
                               ,CONVERT(VARCHAR(8000),New.Credits,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerId
                               ,'LTHalfTime'
                               ,CONVERT(VARCHAR(8000),New.LTHalfTime,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerId
                               ,'HalfTime'
                               ,CONVERT(VARCHAR(8000),New.HalfTime,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerId
                               ,'ThreeQuartTime'
                               ,CONVERT(VARCHAR(8000),New.ThreeQuartTime,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerId
                               ,'FullTime'
                               ,CONVERT(VARCHAR(8000),New.FullTime,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerId
                               ,'DeptId'
                               ,CONVERT(VARCHAR(8000),New.DeptId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerId
                               ,'GrdSystemId'
                               ,CONVERT(VARCHAR(8000),New.GrdSystemId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerId
                               ,'Weighted GPA'
                               ,CONVERT(VARCHAR(8000),New.[Weighted GPA],121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerId
                               ,'BillingMethodId'
                               ,CONVERT(VARCHAR(8000),New.BillingMethodId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerId
                               ,'TuitionEarningId'
                               ,CONVERT(VARCHAR(8000),New.TuitionEarningId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerId
                               ,'AttendanceLevel'
                               ,CONVERT(VARCHAR(8000),New.AttendanceLevel,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerId
                               ,'CustomAttendance'
                               ,CONVERT(VARCHAR(8000),New.CustomAttendance,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerId
                               ,'ProgTypId'
                               ,CONVERT(VARCHAR(8000),New.ProgTypId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerId
                               ,'IsContinuingEd'
                               ,CONVERT(VARCHAR(8000),New.IsContinuingEd,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerId
                               ,'UnitTypeId'
                               ,CONVERT(VARCHAR(8000),New.UnitTypeId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerId
                               ,'TrackTardies'
                               ,CONVERT(VARCHAR(8000),New.TrackTardies,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerId
                               ,'TardiesMakingAbsence'
                               ,CONVERT(VARCHAR(8000),New.TardiesMakingAbsence,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerId
                               ,'SchedMethodId'
                               ,CONVERT(VARCHAR(8000),New.SchedMethodId,121)
                        FROM    Inserted New; 
            END; 
        END;



    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[arPrgVersions_Audit_Update] ON [dbo].[arPrgVersions]
    FOR UPDATE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arPrgVersions','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(ProgId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'ProgId'
                                   ,CONVERT(VARCHAR(8000),Old.ProgId,121)
                                   ,CONVERT(VARCHAR(8000),New.ProgId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.ProgId <> New.ProgId
                                    OR (
                                         Old.ProgId IS NULL
                                         AND New.ProgId IS NOT NULL
                                       )
                                    OR (
                                         New.ProgId IS NULL
                                         AND Old.ProgId IS NOT NULL
                                       ); 
                IF UPDATE(PrgVerCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'PrgVerCode'
                                   ,CONVERT(VARCHAR(8000),Old.PrgVerCode,121)
                                   ,CONVERT(VARCHAR(8000),New.PrgVerCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.PrgVerCode <> New.PrgVerCode
                                    OR (
                                         Old.PrgVerCode IS NULL
                                         AND New.PrgVerCode IS NOT NULL
                                       )
                                    OR (
                                         New.PrgVerCode IS NULL
                                         AND Old.PrgVerCode IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 
                IF UPDATE(PrgGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'PrgGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.PrgGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.PrgGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.PrgGrpId <> New.PrgGrpId
                                    OR (
                                         Old.PrgGrpId IS NULL
                                         AND New.PrgGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.PrgGrpId IS NULL
                                         AND Old.PrgGrpId IS NOT NULL
                                       ); 
                IF UPDATE(PrgVerDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'PrgVerDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.PrgVerDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.PrgVerDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.PrgVerDescrip <> New.PrgVerDescrip
                                    OR (
                                         Old.PrgVerDescrip IS NULL
                                         AND New.PrgVerDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.PrgVerDescrip IS NULL
                                         AND Old.PrgVerDescrip IS NOT NULL
                                       ); 
                IF UPDATE(DegreeId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'DegreeId'
                                   ,CONVERT(VARCHAR(8000),Old.DegreeId,121)
                                   ,CONVERT(VARCHAR(8000),New.DegreeId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.DegreeId <> New.DegreeId
                                    OR (
                                         Old.DegreeId IS NULL
                                         AND New.DegreeId IS NOT NULL
                                       )
                                    OR (
                                         New.DegreeId IS NULL
                                         AND Old.DegreeId IS NOT NULL
                                       ); 
                IF UPDATE(SAPId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'SAPId'
                                   ,CONVERT(VARCHAR(8000),Old.SAPId,121)
                                   ,CONVERT(VARCHAR(8000),New.SAPId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.SAPId <> New.SAPId
                                    OR (
                                         Old.SAPId IS NULL
                                         AND New.SAPId IS NOT NULL
                                       )
                                    OR (
                                         New.SAPId IS NULL
                                         AND Old.SAPId IS NOT NULL
                                       ); 
                IF UPDATE(TestingModelId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'TestingModelId'
                                   ,CONVERT(VARCHAR(8000),Old.TestingModelId,121)
                                   ,CONVERT(VARCHAR(8000),New.TestingModelId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.TestingModelId <> New.TestingModelId
                                    OR (
                                         Old.TestingModelId IS NULL
                                         AND New.TestingModelId IS NOT NULL
                                       )
                                    OR (
                                         New.TestingModelId IS NULL
                                         AND Old.TestingModelId IS NOT NULL
                                       ); 
                IF UPDATE(Weeks)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'Weeks'
                                   ,CONVERT(VARCHAR(8000),Old.Weeks,121)
                                   ,CONVERT(VARCHAR(8000),New.Weeks,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.Weeks <> New.Weeks
                                    OR (
                                         Old.Weeks IS NULL
                                         AND New.Weeks IS NOT NULL
                                       )
                                    OR (
                                         New.Weeks IS NULL
                                         AND Old.Weeks IS NOT NULL
                                       ); 
                IF UPDATE(Terms)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'Terms'
                                   ,CONVERT(VARCHAR(8000),Old.Terms,121)
                                   ,CONVERT(VARCHAR(8000),New.Terms,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.Terms <> New.Terms
                                    OR (
                                         Old.Terms IS NULL
                                         AND New.Terms IS NOT NULL
                                       )
                                    OR (
                                         New.Terms IS NULL
                                         AND Old.Terms IS NOT NULL
                                       ); 
                IF UPDATE(Hours)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'Hours'
                                   ,CONVERT(VARCHAR(8000),Old.Hours,121)
                                   ,CONVERT(VARCHAR(8000),New.Hours,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.Hours <> New.Hours
                                    OR (
                                         Old.Hours IS NULL
                                         AND New.Hours IS NOT NULL
                                       )
                                    OR (
                                         New.Hours IS NULL
                                         AND Old.Hours IS NOT NULL
                                       ); 
                IF UPDATE(Credits)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'Credits'
                                   ,CONVERT(VARCHAR(8000),Old.Credits,121)
                                   ,CONVERT(VARCHAR(8000),New.Credits,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.Credits <> New.Credits
                                    OR (
                                         Old.Credits IS NULL
                                         AND New.Credits IS NOT NULL
                                       )
                                    OR (
                                         New.Credits IS NULL
                                         AND Old.Credits IS NOT NULL
                                       ); 
                IF UPDATE(LTHalfTime)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'LTHalfTime'
                                   ,CONVERT(VARCHAR(8000),Old.LTHalfTime,121)
                                   ,CONVERT(VARCHAR(8000),New.LTHalfTime,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.LTHalfTime <> New.LTHalfTime
                                    OR (
                                         Old.LTHalfTime IS NULL
                                         AND New.LTHalfTime IS NOT NULL
                                       )
                                    OR (
                                         New.LTHalfTime IS NULL
                                         AND Old.LTHalfTime IS NOT NULL
                                       ); 
                IF UPDATE(HalfTime)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'HalfTime'
                                   ,CONVERT(VARCHAR(8000),Old.HalfTime,121)
                                   ,CONVERT(VARCHAR(8000),New.HalfTime,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.HalfTime <> New.HalfTime
                                    OR (
                                         Old.HalfTime IS NULL
                                         AND New.HalfTime IS NOT NULL
                                       )
                                    OR (
                                         New.HalfTime IS NULL
                                         AND Old.HalfTime IS NOT NULL
                                       ); 
                IF UPDATE(ThreeQuartTime)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'ThreeQuartTime'
                                   ,CONVERT(VARCHAR(8000),Old.ThreeQuartTime,121)
                                   ,CONVERT(VARCHAR(8000),New.ThreeQuartTime,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.ThreeQuartTime <> New.ThreeQuartTime
                                    OR (
                                         Old.ThreeQuartTime IS NULL
                                         AND New.ThreeQuartTime IS NOT NULL
                                       )
                                    OR (
                                         New.ThreeQuartTime IS NULL
                                         AND Old.ThreeQuartTime IS NOT NULL
                                       ); 
                IF UPDATE(FullTime)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'FullTime'
                                   ,CONVERT(VARCHAR(8000),Old.FullTime,121)
                                   ,CONVERT(VARCHAR(8000),New.FullTime,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.FullTime <> New.FullTime
                                    OR (
                                         Old.FullTime IS NULL
                                         AND New.FullTime IS NOT NULL
                                       )
                                    OR (
                                         New.FullTime IS NULL
                                         AND Old.FullTime IS NOT NULL
                                       ); 
                IF UPDATE(DeptId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'DeptId'
                                   ,CONVERT(VARCHAR(8000),Old.DeptId,121)
                                   ,CONVERT(VARCHAR(8000),New.DeptId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.DeptId <> New.DeptId
                                    OR (
                                         Old.DeptId IS NULL
                                         AND New.DeptId IS NOT NULL
                                       )
                                    OR (
                                         New.DeptId IS NULL
                                         AND Old.DeptId IS NOT NULL
                                       ); 
                IF UPDATE(GrdSystemId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'GrdSystemId'
                                   ,CONVERT(VARCHAR(8000),Old.GrdSystemId,121)
                                   ,CONVERT(VARCHAR(8000),New.GrdSystemId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.GrdSystemId <> New.GrdSystemId
                                    OR (
                                         Old.GrdSystemId IS NULL
                                         AND New.GrdSystemId IS NOT NULL
                                       )
                                    OR (
                                         New.GrdSystemId IS NULL
                                         AND Old.GrdSystemId IS NOT NULL
                                       ); 
                IF UPDATE([Weighted GPA])
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'Weighted GPA'
                                   ,CONVERT(VARCHAR(8000),Old.[Weighted GPA],121)
                                   ,CONVERT(VARCHAR(8000),New.[Weighted GPA],121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.[Weighted GPA] <> New.[Weighted GPA]
                                    OR (
                                         Old.[Weighted GPA] IS NULL
                                         AND New.[Weighted GPA] IS NOT NULL
                                       )
                                    OR (
                                         New.[Weighted GPA] IS NULL
                                         AND Old.[Weighted GPA] IS NOT NULL
                                       ); 
                IF UPDATE(BillingMethodId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'BillingMethodId'
                                   ,CONVERT(VARCHAR(8000),Old.BillingMethodId,121)
                                   ,CONVERT(VARCHAR(8000),New.BillingMethodId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.BillingMethodId <> New.BillingMethodId
                                    OR (
                                         Old.BillingMethodId IS NULL
                                         AND New.BillingMethodId IS NOT NULL
                                       )
                                    OR (
                                         New.BillingMethodId IS NULL
                                         AND Old.BillingMethodId IS NOT NULL
                                       ); 
                IF UPDATE(TuitionEarningId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'TuitionEarningId'
                                   ,CONVERT(VARCHAR(8000),Old.TuitionEarningId,121)
                                   ,CONVERT(VARCHAR(8000),New.TuitionEarningId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.TuitionEarningId <> New.TuitionEarningId
                                    OR (
                                         Old.TuitionEarningId IS NULL
                                         AND New.TuitionEarningId IS NOT NULL
                                       )
                                    OR (
                                         New.TuitionEarningId IS NULL
                                         AND Old.TuitionEarningId IS NOT NULL
                                       ); 
                IF UPDATE(AttendanceLevel)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'AttendanceLevel'
                                   ,CONVERT(VARCHAR(8000),Old.AttendanceLevel,121)
                                   ,CONVERT(VARCHAR(8000),New.AttendanceLevel,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.AttendanceLevel <> New.AttendanceLevel
                                    OR (
                                         Old.AttendanceLevel IS NULL
                                         AND New.AttendanceLevel IS NOT NULL
                                       )
                                    OR (
                                         New.AttendanceLevel IS NULL
                                         AND Old.AttendanceLevel IS NOT NULL
                                       ); 
                IF UPDATE(CustomAttendance)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'CustomAttendance'
                                   ,CONVERT(VARCHAR(8000),Old.CustomAttendance,121)
                                   ,CONVERT(VARCHAR(8000),New.CustomAttendance,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.CustomAttendance <> New.CustomAttendance
                                    OR (
                                         Old.CustomAttendance IS NULL
                                         AND New.CustomAttendance IS NOT NULL
                                       )
                                    OR (
                                         New.CustomAttendance IS NULL
                                         AND Old.CustomAttendance IS NOT NULL
                                       ); 
                IF UPDATE(ProgTypId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'ProgTypId'
                                   ,CONVERT(VARCHAR(8000),Old.ProgTypId,121)
                                   ,CONVERT(VARCHAR(8000),New.ProgTypId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.ProgTypId <> New.ProgTypId
                                    OR (
                                         Old.ProgTypId IS NULL
                                         AND New.ProgTypId IS NOT NULL
                                       )
                                    OR (
                                         New.ProgTypId IS NULL
                                         AND Old.ProgTypId IS NOT NULL
                                       ); 
                IF UPDATE(IsContinuingEd)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'IsContinuingEd'
                                   ,CONVERT(VARCHAR(8000),Old.IsContinuingEd,121)
                                   ,CONVERT(VARCHAR(8000),New.IsContinuingEd,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.IsContinuingEd <> New.IsContinuingEd
                                    OR (
                                         Old.IsContinuingEd IS NULL
                                         AND New.IsContinuingEd IS NOT NULL
                                       )
                                    OR (
                                         New.IsContinuingEd IS NULL
                                         AND Old.IsContinuingEd IS NOT NULL
                                       ); 
                IF UPDATE(UnitTypeId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'UnitTypeId'
                                   ,CONVERT(VARCHAR(8000),Old.UnitTypeId,121)
                                   ,CONVERT(VARCHAR(8000),New.UnitTypeId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.UnitTypeId <> New.UnitTypeId
                                    OR (
                                         Old.UnitTypeId IS NULL
                                         AND New.UnitTypeId IS NOT NULL
                                       )
                                    OR (
                                         New.UnitTypeId IS NULL
                                         AND Old.UnitTypeId IS NOT NULL
                                       ); 
                IF UPDATE(TrackTardies)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'TrackTardies'
                                   ,CONVERT(VARCHAR(8000),Old.TrackTardies,121)
                                   ,CONVERT(VARCHAR(8000),New.TrackTardies,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.TrackTardies <> New.TrackTardies
                                    OR (
                                         Old.TrackTardies IS NULL
                                         AND New.TrackTardies IS NOT NULL
                                       )
                                    OR (
                                         New.TrackTardies IS NULL
                                         AND Old.TrackTardies IS NOT NULL
                                       ); 
                IF UPDATE(TardiesMakingAbsence)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'TardiesMakingAbsence'
                                   ,CONVERT(VARCHAR(8000),Old.TardiesMakingAbsence,121)
                                   ,CONVERT(VARCHAR(8000),New.TardiesMakingAbsence,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.TardiesMakingAbsence <> New.TardiesMakingAbsence
                                    OR (
                                         Old.TardiesMakingAbsence IS NULL
                                         AND New.TardiesMakingAbsence IS NOT NULL
                                       )
                                    OR (
                                         New.TardiesMakingAbsence IS NULL
                                         AND Old.TardiesMakingAbsence IS NOT NULL
                                       ); 
                IF UPDATE(SchedMethodId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'SchedMethodId'
                                   ,CONVERT(VARCHAR(8000),Old.SchedMethodId,121)
                                   ,CONVERT(VARCHAR(8000),New.SchedMethodId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.SchedMethodId <> New.SchedMethodId
                                    OR (
                                         Old.SchedMethodId IS NULL
                                         AND New.SchedMethodId IS NOT NULL
                                       )
                                    OR (
                                         New.SchedMethodId IS NULL
                                         AND Old.SchedMethodId IS NOT NULL
                                       ); 
            END; 
        END;



    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--==========================================================================================
-- TRIGGER arPrgVersions_KlassApp_Update
-- AFTER UPDATE  arPrgVersions, syKlassAppConfigurationSetting when change fields for Klass Apps
--==========================================================================================
CREATE TRIGGER [dbo].[arPrgVersions_KlassApp_Update] ON [dbo].[arPrgVersions]
    AFTER UPDATE
	--   DB             --   MAP        --  Klase Apps           department
	-- PrgVerId         --              --                       AdvantageId
	-- PrgVerDescrip	-- Description	--> name
	--                  --              --> value
	--                  --              --> type
	--                  --              --> deleted
	--					--				--> created_at	
	-- ModDate			-- ModDate		--> updated_at
	--                  --              --> cauto_disable
	-- Klass Apps
	-- department  is kte   --> program is Lable
AS
    BEGIN
        SET NOCOUNT ON;

        UPDATE  SKACS
        SET     SKACS.ItemStatus = 'U'
               ,SKACS.ModDate = I.ModDate
               ,SKACS.ModUser = I.ModUser
        FROM    syKlassAppConfigurationSetting AS SKACS
        INNER JOIN syKlassOperationType AS SKOT ON SKOT.KlassOperationTypeId = SKACS.KlassOperationTypeId
        INNER JOIN Inserted AS I ON I.PrgVerId = SKACS.AdvantageId
        WHERE   SKOT.Code = 'department'
                AND SKACS.ItemStatus <> 'I'
                AND ( UPDATE(PrgVerDescrip) );

        SET NOCOUNT OFF; 
    END;
--==========================================================================================
-- END  --  TRIGGER arPrgVersions_KlassApp_Update
--==========================================================================================
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--==========================================================================================
-- TRIGGER arPrgVersions_KlassApps_Delete
-- AFTER DELETE arPrgVersions, marks syKlassAppConfigurationSetting record as deleted for Klass Apps
--==========================================================================================
CREATE TRIGGER [dbo].[arPrgVersions_KlassApps_Delete] ON [dbo].[arPrgVersions]
    AFTER DELETE
	-- Klass Apps
	-- department  is kte   --> program is Lable
AS
    BEGIN
        SET NOCOUNT ON;

        UPDATE  SKACS
        SET     SKACS.ItemStatus = 'D'
               ,SKACS.ModDate = GETDATE() -- D.ModDate
               ,SKACS.ModUser = 'Support' -- D.ModUser
        FROM    syKlassAppConfigurationSetting AS SKACS
        INNER JOIN syKlassOperationType AS SKOT ON SKOT.KlassOperationTypeId = SKACS.KlassOperationTypeId
        INNER JOIN Deleted AS D ON D.PrgVerId = SKACS.AdvantageId
        WHERE   SKOT.Code = 'department';

        SET NOCOUNT OFF; 
    END;
--==========================================================================================
-- END  --  TRIGGER arPrgVersions_KlassApps_Delete
--==========================================================================================
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--==========================================================================================
-- TRIGGER arPrgVersions_KlassApps_Insert
-- AFTER INSERT arPrgVersions, insert syKlassAppConfigurationSetting record for Klass Apps
--==========================================================================================
CREATE TRIGGER [dbo].[arPrgVersions_KlassApps_Insert] ON [dbo].[arPrgVersions]
    AFTER INSERT
	-- Klass Apps
	-- department  is kte   --> program is Lable
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @KlassOperationTypeId AS INTEGER;
        SELECT  @KlassOperationTypeId = SKOT.KlassOperationTypeId
        FROM    syKlassOperationType AS SKOT
        WHERE   SKOT.Code = 'department';

        INSERT  INTO syKlassAppConfigurationSetting
                (
                 AdvantageId
                ,KlassAppId
                ,KlassEntityId
                ,KlassOperationTypeId
                ,IsCustomField
                ,IsActive
                ,ItemStatus
                ,ItemValue
                ,ItemLabel
                ,ItemValueType
                ,CreationDate
                ,ModDate
                ,ModUser
                ,LastOperationLog
		        )
                SELECT  I.PrgVerId -- AdvantageId - varchar(38)
                       ,0    -- KlassAppId - int
                       ,1    -- KlassEntityId - int
                       ,@KlassOperationTypeId -- KlassOperationTypeId - int
                       ,0    -- IsCustomField - bit
                       ,1    -- IsActive - bit
                       ,'I'  -- ItemStatus - char(1)
                       ,I.PrgVerDescrip  -- ItemValue - varchar(200)
                       ,'program'  -- ItemLabel - varchar(50)
                       ,'String'   -- ItemValueType - varchar(50)
                       ,I.ModDate  -- CreationDate - datetime
                       ,I.ModDate  -- ModDate - datetime
                       ,I.ModUser  -- ModUser - varchar(50)
                       ,''  -- LastOperationLog - varchar(1000)
                FROM    Inserted AS I;

        SET NOCOUNT OFF; 
    END;
--==========================================================================================
-- END  --  TRIGGER syCampuses_KlassApps_Insert
--==========================================================================================
GO
ALTER TABLE [dbo].[arPrgVersions] ADD CONSTRAINT [PK_arPrgVersions_PrgVerId] PRIMARY KEY CLUSTERED  ([PrgVerId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arPrgVersions] ADD CONSTRAINT [FK_arPrgVersions_arAttUnitType_UnitTypeId_UnitTypeId] FOREIGN KEY ([UnitTypeId]) REFERENCES [dbo].[arAttUnitType] ([UnitTypeId])
GO
ALTER TABLE [dbo].[arPrgVersions] ADD CONSTRAINT [FK_arPrgVersions_arDegrees_DegreeId_DegreeId] FOREIGN KEY ([DegreeId]) REFERENCES [dbo].[arDegrees] ([DegreeId])
GO
ALTER TABLE [dbo].[arPrgVersions] ADD CONSTRAINT [FK_arPrgVersions_arDepartments_DeptId_DeptId] FOREIGN KEY ([DeptId]) REFERENCES [dbo].[arDepartments] ([DeptId])
GO
ALTER TABLE [dbo].[arPrgVersions] ADD CONSTRAINT [FK_arPrgVersions_arGradeScales_ThGrdScaleId_GrdScaleId] FOREIGN KEY ([ThGrdScaleId]) REFERENCES [dbo].[arGradeScales] ([GrdScaleId])
GO
ALTER TABLE [dbo].[arPrgVersions] ADD CONSTRAINT [FK_arPrgVersions_arGradeSystems_GrdSystemId_GrdSystemId] FOREIGN KEY ([GrdSystemId]) REFERENCES [dbo].[arGradeSystems] ([GrdSystemId])
GO
ALTER TABLE [dbo].[arPrgVersions] ADD CONSTRAINT [FK_arPrgVersions_arPrgGrp_PrgGrpId_PrgGrpId] FOREIGN KEY ([PrgGrpId]) REFERENCES [dbo].[arPrgGrp] ([PrgGrpId])
GO
ALTER TABLE [dbo].[arPrgVersions] ADD CONSTRAINT [FK_arPrgVersions_arPrograms_ProgId_ProgId] FOREIGN KEY ([ProgId]) REFERENCES [dbo].[arPrograms] ([ProgId]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[arPrgVersions] ADD CONSTRAINT [FK_arPrgVersions_arProgTypes_ProgTypId_ProgTypId] FOREIGN KEY ([ProgTypId]) REFERENCES [dbo].[arProgTypes] ([ProgTypId])
GO
ALTER TABLE [dbo].[arPrgVersions] ADD CONSTRAINT [FK_arPrgVersions_arSap_FASAPId_SAPId] FOREIGN KEY ([FASAPId]) REFERENCES [dbo].[arSAP] ([SAPId])
GO
ALTER TABLE [dbo].[arPrgVersions] ADD CONSTRAINT [FK_arPrgVersions_arSAP_SAPId_SAPId] FOREIGN KEY ([SAPId]) REFERENCES [dbo].[arSAP] ([SAPId])
GO
ALTER TABLE [dbo].[arPrgVersions] ADD CONSTRAINT [FK_arPrgVersions_saBillingMethods_BillingMethodId_BillingMethodId] FOREIGN KEY ([BillingMethodId]) REFERENCES [dbo].[saBillingMethods] ([BillingMethodId])
GO
ALTER TABLE [dbo].[arPrgVersions] ADD CONSTRAINT [FK_arPrgVersions_saTuitionEarnings_TuitionEarningId_TuitionEarningId] FOREIGN KEY ([TuitionEarningId]) REFERENCES [dbo].[saTuitionEarnings] ([TuitionEarningId])
GO
ALTER TABLE [dbo].[arPrgVersions] ADD CONSTRAINT [FK_arPrgVersions_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[arPrgVersions] ADD CONSTRAINT [FK_arPrgVersions_sySchedulingMethods_SchedMethodId_SchedMethodId] FOREIGN KEY ([SchedMethodId]) REFERENCES [dbo].[sySchedulingMethods] ([SchedMethodId])
GO
ALTER TABLE [dbo].[arPrgVersions] ADD CONSTRAINT [FK_arPrgVersions_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
