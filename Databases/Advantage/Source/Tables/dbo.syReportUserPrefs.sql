CREATE TABLE [dbo].[syReportUserPrefs]
(
[PrefId] [uniqueidentifier] NOT NULL,
[UserId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResourceId] [int] NULL,
[PrefName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PrefDescrip] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrefData] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[syReportUserPrefs] ADD CONSTRAINT [PK_syReportUserPrefs_PrefId] PRIMARY KEY CLUSTERED  ([PrefId]) ON [PRIMARY]
GO
