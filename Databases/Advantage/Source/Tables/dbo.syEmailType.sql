CREATE TABLE [dbo].[syEmailType]
(
[EMailTypeId] [uniqueidentifier] NOT NULL,
[EMailTypeCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EMailTypeDescription] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syEmailType] ADD CONSTRAINT [PK_syEmailType_EMailTypeId] PRIMARY KEY CLUSTERED  ([EMailTypeId]) ON [PRIMARY]
GO
