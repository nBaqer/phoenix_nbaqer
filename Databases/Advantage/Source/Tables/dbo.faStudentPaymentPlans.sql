CREATE TABLE [dbo].[faStudentPaymentPlans]
(
[PaymentPlanId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_faStudentPaymentPlans_PaymentPlanId] DEFAULT (newid()),
[StuEnrollId] [uniqueidentifier] NOT NULL,
[PayPlanStartDate] [datetime] NOT NULL,
[PayPlanEndDate] [datetime] NULL,
[AcademicYearId] [uniqueidentifier] NULL,
[TotalAmountDue] [decimal] (19, 4) NOT NULL,
[PayPlanDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[Disbursements] [int] NULL,
[AwardId] [int] NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[faStudentPaymentPlans_Audit_Delete] ON [dbo].[faStudentPaymentPlans]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 

    DECLARE @stuenrollid AS UNIQUEIDENTIFIER;

    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 

    SET @stuenrollid = (
                         SELECT TOP 1
                                StuEnrollId
                         FROM   DELETED
                       ); 

    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'faStudentPaymentPlans','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PaymentPlanId
                               ,'Disbursements'
                               ,CONVERT(VARCHAR(8000),Old.Disbursements,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PaymentPlanId
                               ,'StuEnrollId'
                               ,CONVERT(VARCHAR(8000),Old.StuEnrollId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PaymentPlanId
                               ,'AcademicYearId'
                               ,CONVERT(VARCHAR(8000),Old.AcademicYearId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PaymentPlanId
                               ,'PayPlanStartDate'
                               ,CONVERT(VARCHAR(8000),Old.PayPlanStartDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PaymentPlanId
                               ,'PayPlanEndDate'
                               ,CONVERT(VARCHAR(8000),Old.PayPlanEndDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PaymentPlanId
                               ,'PayPlanDescrip'
                               ,CONVERT(VARCHAR(8000),Old.PayPlanDescrip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PaymentPlanId
                               ,'TotalAmountDue'
                               ,CONVERT(VARCHAR(8000),Old.TotalAmountDue,121)
                        FROM    Deleted Old; 
            END; 
--EXEC dbo.USP_AR_StuEnrollPayPeriods @stuEnrollId
        END;



    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[faStudentPaymentPlans_Audit_Insert] ON [dbo].[faStudentPaymentPlans]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);

    DECLARE @stuEnrollId AS UNIQUEIDENTIFIER;
 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 

    SET @stuEnrollId = (
                         SELECT TOP 1
                                StuEnrollId
                         FROM   INSERTED
                       ); 
 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'faStudentPaymentPlans','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PaymentPlanId
                               ,'Disbursements'
                               ,CONVERT(VARCHAR(8000),New.Disbursements,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PaymentPlanId
                               ,'StuEnrollId'
                               ,CONVERT(VARCHAR(8000),New.StuEnrollId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PaymentPlanId
                               ,'AcademicYearId'
                               ,CONVERT(VARCHAR(8000),New.AcademicYearId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PaymentPlanId
                               ,'PayPlanStartDate'
                               ,CONVERT(VARCHAR(8000),New.PayPlanStartDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PaymentPlanId
                               ,'PayPlanEndDate'
                               ,CONVERT(VARCHAR(8000),New.PayPlanEndDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PaymentPlanId
                               ,'PayPlanDescrip'
                               ,CONVERT(VARCHAR(8000),New.PayPlanDescrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PaymentPlanId
                               ,'TotalAmountDue'
                               ,CONVERT(VARCHAR(8000),New.TotalAmountDue,121)
                        FROM    Inserted New; 
            END; 
--EXEC dbo.USP_AR_StuEnrollPayPeriods @stuEnrollId
        END;



    SET NOCOUNT OFF; 
GO
GO

GO

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[faStudentPaymentPlans_Audit_Update] ON [dbo].[faStudentPaymentPlans]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);

    DECLARE @stuEnrollId AS UNIQUEIDENTIFIER;

    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 

    SET @stuEnrollId = (
                         SELECT TOP 1
                                StuEnrollId
                         FROM   INSERTED
                       ); 

    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'faStudentPaymentPlans','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(Disbursements)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PaymentPlanId
                                   ,'Disbursements'
                                   ,CONVERT(VARCHAR(8000),Old.Disbursements,121)
                                   ,CONVERT(VARCHAR(8000),New.Disbursements,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PaymentPlanId = New.PaymentPlanId
                            WHERE   Old.Disbursements <> New.Disbursements
                                    OR (
                                         Old.Disbursements IS NULL
                                         AND New.Disbursements IS NOT NULL
                                       )
                                    OR (
                                         New.Disbursements IS NULL
                                         AND Old.Disbursements IS NOT NULL
                                       ); 
                IF UPDATE(StuEnrollId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PaymentPlanId
                                   ,'StuEnrollId'
                                   ,CONVERT(VARCHAR(8000),Old.StuEnrollId,121)
                                   ,CONVERT(VARCHAR(8000),New.StuEnrollId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PaymentPlanId = New.PaymentPlanId
                            WHERE   Old.StuEnrollId <> New.StuEnrollId
                                    OR (
                                         Old.StuEnrollId IS NULL
                                         AND New.StuEnrollId IS NOT NULL
                                       )
                                    OR (
                                         New.StuEnrollId IS NULL
                                         AND Old.StuEnrollId IS NOT NULL
                                       ); 
                IF UPDATE(AcademicYearId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PaymentPlanId
                                   ,'AcademicYearId'
                                   ,CONVERT(VARCHAR(8000),Old.AcademicYearId,121)
                                   ,CONVERT(VARCHAR(8000),New.AcademicYearId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PaymentPlanId = New.PaymentPlanId
                            WHERE   Old.AcademicYearId <> New.AcademicYearId
                                    OR (
                                         Old.AcademicYearId IS NULL
                                         AND New.AcademicYearId IS NOT NULL
                                       )
                                    OR (
                                         New.AcademicYearId IS NULL
                                         AND Old.AcademicYearId IS NOT NULL
                                       ); 
                IF UPDATE(PayPlanStartDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PaymentPlanId
                                   ,'PayPlanStartDate'
                                   ,CONVERT(VARCHAR(8000),Old.PayPlanStartDate,121)
                                   ,CONVERT(VARCHAR(8000),New.PayPlanStartDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PaymentPlanId = New.PaymentPlanId
                            WHERE   Old.PayPlanStartDate <> New.PayPlanStartDate
                                    OR (
                                         Old.PayPlanStartDate IS NULL
                                         AND New.PayPlanStartDate IS NOT NULL
                                       )
                                    OR (
                                         New.PayPlanStartDate IS NULL
                                         AND Old.PayPlanStartDate IS NOT NULL
                                       ); 
                IF UPDATE(PayPlanEndDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PaymentPlanId
                                   ,'PayPlanEndDate'
                                   ,CONVERT(VARCHAR(8000),Old.PayPlanEndDate,121)
                                   ,CONVERT(VARCHAR(8000),New.PayPlanEndDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PaymentPlanId = New.PaymentPlanId
                            WHERE   Old.PayPlanEndDate <> New.PayPlanEndDate
                                    OR (
                                         Old.PayPlanEndDate IS NULL
                                         AND New.PayPlanEndDate IS NOT NULL
                                       )
                                    OR (
                                         New.PayPlanEndDate IS NULL
                                         AND Old.PayPlanEndDate IS NOT NULL
                                       ); 
                IF UPDATE(PayPlanDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PaymentPlanId
                                   ,'PayPlanDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.PayPlanDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.PayPlanDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PaymentPlanId = New.PaymentPlanId
                            WHERE   Old.PayPlanDescrip <> New.PayPlanDescrip
                                    OR (
                                         Old.PayPlanDescrip IS NULL
                                         AND New.PayPlanDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.PayPlanDescrip IS NULL
                                         AND Old.PayPlanDescrip IS NOT NULL
                                       ); 
                IF UPDATE(TotalAmountDue)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PaymentPlanId
                                   ,'TotalAmountDue'
                                   ,CONVERT(VARCHAR(8000),Old.TotalAmountDue,121)
                                   ,CONVERT(VARCHAR(8000),New.TotalAmountDue,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PaymentPlanId = New.PaymentPlanId
                            WHERE   Old.TotalAmountDue <> New.TotalAmountDue
                                    OR (
                                         Old.TotalAmountDue IS NULL
                                         AND New.TotalAmountDue IS NOT NULL
                                       )
                                    OR (
                                         New.TotalAmountDue IS NULL
                                         AND Old.TotalAmountDue IS NOT NULL
                                       ); 
            END; 
--EXEC dbo.USP_AR_StuEnrollPayPeriods @stuEnrollId
        END;



    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[faStudentPaymentPlans] ADD CONSTRAINT [PK_faStudentPaymentPlans_PaymentPlanId] PRIMARY KEY CLUSTERED  ([PaymentPlanId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[faStudentPaymentPlans] ADD CONSTRAINT [FK_faStudentPaymentPlans_saAcademicYears_AcademicYearId_AcademicYearId] FOREIGN KEY ([AcademicYearId]) REFERENCES [dbo].[saAcademicYears] ([AcademicYearId])
GO
ALTER TABLE [dbo].[faStudentPaymentPlans] ADD CONSTRAINT [FK_faStudentPaymentPlans_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY ([StuEnrollId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
