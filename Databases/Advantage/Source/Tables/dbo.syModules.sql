CREATE TABLE [dbo].[syModules]
(
[ModuleID] [tinyint] NOT NULL,
[ModuleCode] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModuleName] [varchar] (70) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NULL CONSTRAINT [DF_syModules_ModDate] DEFAULT (getdate()),
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllowSDF] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syModules] ADD CONSTRAINT [PK_syModules_ModuleID] PRIMARY KEY CLUSTERED  ([ModuleID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syModules] ADD CONSTRAINT [UIX_syModules_ModuleCode] UNIQUE NONCLUSTERED  ([ModuleCode]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[syModules] TO [AdvantageRole]
GO
