CREATE TABLE [dbo].[saBillTypes]
(
[BillTypeId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_saBillTypes_BillTypeId] DEFAULT (newid()),
[BillTypeDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saBillTypes] ADD CONSTRAINT [PK_saBillTypes_BillTypeId] PRIMARY KEY CLUSTERED  ([BillTypeId]) ON [PRIMARY]
GO
