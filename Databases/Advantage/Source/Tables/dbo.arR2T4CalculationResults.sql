CREATE TABLE [dbo].[arR2T4CalculationResults]
(
[R2T4CalculationResultsId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_arR2T4CalculationResults_R2T4CalculationResultsId] DEFAULT (newsequentialid()),
[TerminationId] [uniqueidentifier] NOT NULL,
[TotalCharges] [money] NOT NULL,
[TotalTitleIVAid] [money] NOT NULL,
[TotalTitleIVAidDisbursed] [money] NOT NULL,
[PercentageOfTitleIVAidEarned] [money] NOT NULL,
[TotalTitleIVAidToReturn] [money] NOT NULL,
[UnsubDirectLoanReturnedBySchool] [money] NOT NULL,
[SubDirectLoanReturnedBySchool] [money] NOT NULL,
[PerkinsLoanReturnedBySchool] [money] NOT NULL,
[DirectGraduatePlusLoanReturnedBySchool] [money] NOT NULL,
[DirectParentPlusLoanReturnedBySchool] [money] NOT NULL,
[PellGrantReturnedBySchool] [money] NOT NULL,
[FSEOGReturnedBySchool] [money] NOT NULL,
[TeachGrantReturnedBySchool] [money] NOT NULL,
[IraqAfgGrantReturnedBySchool] [money] NOT NULL,
[TotalAmountToBeReturnedBySchool] [money] NOT NULL,
[UnsubDirectLoanReturnedByStudent] [money] NOT NULL,
[SubDirectLoanReturnedByStudent] [money] NOT NULL,
[PerkinsLoanReturnedByStudent] [money] NOT NULL,
[DirectGraduatePlusLoanReturnedByStudent] [money] NOT NULL,
[DirectParentPlusLoanReturnedByStudent] [money] NOT NULL,
[FSEOGReturnedByStudent] [money] NOT NULL,
[TeachGrantReturnedByStudent] [money] NOT NULL,
[IraqAfgGrantReturnedByStudent] [money] NOT NULL,
[TotalAmountToBeReturnedByStudent] [money] NOT NULL,
[CreatedById] [uniqueidentifier] NULL,
[CreatedDate] [datetime] NULL CONSTRAINT [DF_arR2T4CalculationResults_CreatedDate] DEFAULT (getdate()),
[UpdatedById] [uniqueidentifier] NULL,
[UpdatedDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arR2T4CalculationResults] ADD CONSTRAINT [PK_arR2T4CalculationResults_R2T4CalculationResultsId] PRIMARY KEY CLUSTERED  ([R2T4CalculationResultsId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arR2T4CalculationResults] ADD CONSTRAINT [FK_arR2T4CalculationResults_arR2T4TerminationDetails_TerminationId_TerminationId] FOREIGN KEY ([TerminationId]) REFERENCES [dbo].[arR2T4TerminationDetails] ([TerminationId])
GO
ALTER TABLE [dbo].[arR2T4CalculationResults] ADD CONSTRAINT [FK_arR2T4CalculationResults_syUsers_CreatedBy_UserId] FOREIGN KEY ([CreatedById]) REFERENCES [dbo].[syUsers] ([UserId])
GO
ALTER TABLE [dbo].[arR2T4CalculationResults] ADD CONSTRAINT [FK_arR2T4CalculationResults_syUsers_UpdatedBy_UserId] FOREIGN KEY ([UpdatedById]) REFERENCES [dbo].[syUsers] ([UserId])
GO
