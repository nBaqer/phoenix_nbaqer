CREATE TABLE [dbo].[syResTblFlds]
(
[ResDefId] [int] NOT NULL,
[ResourceId] [smallint] NOT NULL,
[TblFldsId] [int] NOT NULL,
[Required] [bit] NOT NULL CONSTRAINT [DF_syResTblFlds_Required] DEFAULT ((0)),
[SchlReq] [bit] NOT NULL CONSTRAINT [DF_syResTblFlds_SchlReq] DEFAULT ((0)),
[ControlName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UsePageSetup] [bit] NOT NULL CONSTRAINT [DF_syResTblFlds_UsePageSetup] DEFAULT ((1))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syResTblFlds] ADD CONSTRAINT [PK_syResTblFlds_ResDefId] PRIMARY KEY CLUSTERED  ([ResDefId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syResTblFlds] ADD CONSTRAINT [FK_syResTblFlds_syTblFlds_TblFldsId_TblFldsId] FOREIGN KEY ([TblFldsId]) REFERENCES [dbo].[syTblFlds] ([TblFldsId])
GO
GRANT SELECT ON  [dbo].[syResTblFlds] TO [AdvantageRole]
GO
