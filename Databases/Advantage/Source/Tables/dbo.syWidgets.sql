CREATE TABLE [dbo].[syWidgets]
(
[WidgetId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_syWidgets_WidgetId] DEFAULT (newsequentialid()),
[Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Rows] [int] NOT NULL,
[Columns] [int] NOT NULL,
[StatusId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syWidgets] ADD CONSTRAINT [PK_syWidgets_WidgetId] PRIMARY KEY CLUSTERED  ([WidgetId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syWidgets] ADD CONSTRAINT [FK_syWidgets_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
