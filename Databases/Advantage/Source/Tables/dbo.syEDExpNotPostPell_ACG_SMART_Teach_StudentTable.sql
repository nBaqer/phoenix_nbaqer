CREATE TABLE [dbo].[syEDExpNotPostPell_ACG_SMART_Teach_StudentTable]
(
[ParentId] [uniqueidentifier] NOT NULL,
[StudentAwardId] [uniqueidentifier] NULL,
[AwardTypeId] [uniqueidentifier] NULL,
[DbIn] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Filter] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SSN] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CampusName] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CampusId] [uniqueidentifier] NULL,
[StuEnrollmentId] [uniqueidentifier] NULL,
[DbIndicator] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AwardAmount] [decimal] (19, 4) NULL,
[AwardId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GrantType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddDate] [datetime] NULL,
[AddTime] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginalSSN] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateDate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginationStatus] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcademicYearId] [uniqueidentifier] NULL,
[AwardYearStartDate] [datetime] NULL,
[AwardYearEndDate] [datetime] NULL,
[ModDate] [datetime] NULL,
[FileName] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsInSchool] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syEDExpNotPostPell_ACG_SMART_Teach_StudentTable] ADD CONSTRAINT [PK_syEDExpNotPostPell_ACG_SMART_Teach_StudentTable_ParentId] PRIMARY KEY CLUSTERED  ([ParentId]) ON [PRIMARY]
GO
