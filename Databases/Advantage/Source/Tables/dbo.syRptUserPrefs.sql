CREATE TABLE [dbo].[syRptUserPrefs]
(
[PrefId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syRptUserPrefs_PrefId] DEFAULT (newid()),
[UserId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ResourceId] [int] NULL,
[PrefName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UserResourceId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syRptUserPrefs] ADD CONSTRAINT [PK_syRptUserPrefs_PrefId] PRIMARY KEY CLUSTERED  ([PrefId]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[syRptUserPrefs] TO [AdvantageRole]
GO
