CREATE TABLE [dbo].[saProgramVersionFees]
(
[PrgVerFeeId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_saProgramVersionFees_PrgVerFeeId] DEFAULT (newid()),
[StatusId] [uniqueidentifier] NOT NULL,
[PrgVerId] [uniqueidentifier] NOT NULL,
[TransCodeId] [uniqueidentifier] NOT NULL,
[TuitionCategoryId] [uniqueidentifier] NULL,
[Amount] [money] NOT NULL,
[RateScheduleId] [uniqueidentifier] NULL,
[UnitId] [smallint] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saProgramVersionFees_Audit_Delete] ON [dbo].[saProgramVersionFees]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saProgramVersionFees','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerFeeId
                               ,'UnitId'
                               ,CONVERT(VARCHAR(8000),Old.UnitId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerFeeId
                               ,'Amount'
                               ,CONVERT(VARCHAR(8000),Old.Amount,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerFeeId
                               ,'TransCodeId'
                               ,CONVERT(VARCHAR(8000),Old.TransCodeId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerFeeId
                               ,'TuitionCategoryId'
                               ,CONVERT(VARCHAR(8000),Old.TuitionCategoryId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerFeeId
                               ,'RateScheduleId'
                               ,CONVERT(VARCHAR(8000),Old.RateScheduleId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerFeeId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerFeeId
                               ,'PrgVerId'
                               ,CONVERT(VARCHAR(8000),Old.PrgVerId,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saProgramVersionFees_Audit_Insert] ON [dbo].[saProgramVersionFees]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saProgramVersionFees','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerFeeId
                               ,'UnitId'
                               ,CONVERT(VARCHAR(8000),New.UnitId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerFeeId
                               ,'Amount'
                               ,CONVERT(VARCHAR(8000),New.Amount,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerFeeId
                               ,'TransCodeId'
                               ,CONVERT(VARCHAR(8000),New.TransCodeId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerFeeId
                               ,'TuitionCategoryId'
                               ,CONVERT(VARCHAR(8000),New.TuitionCategoryId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerFeeId
                               ,'RateScheduleId'
                               ,CONVERT(VARCHAR(8000),New.RateScheduleId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerFeeId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerFeeId
                               ,'PrgVerId'
                               ,CONVERT(VARCHAR(8000),New.PrgVerId,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO
GO

GO

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saProgramVersionFees_Audit_Update] ON [dbo].[saProgramVersionFees]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saProgramVersionFees','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(UnitId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerFeeId
                                   ,'UnitId'
                                   ,CONVERT(VARCHAR(8000),Old.UnitId,121)
                                   ,CONVERT(VARCHAR(8000),New.UnitId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerFeeId = New.PrgVerFeeId
                            WHERE   Old.UnitId <> New.UnitId
                                    OR (
                                         Old.UnitId IS NULL
                                         AND New.UnitId IS NOT NULL
                                       )
                                    OR (
                                         New.UnitId IS NULL
                                         AND Old.UnitId IS NOT NULL
                                       ); 
                IF UPDATE(Amount)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerFeeId
                                   ,'Amount'
                                   ,CONVERT(VARCHAR(8000),Old.Amount,121)
                                   ,CONVERT(VARCHAR(8000),New.Amount,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerFeeId = New.PrgVerFeeId
                            WHERE   Old.Amount <> New.Amount
                                    OR (
                                         Old.Amount IS NULL
                                         AND New.Amount IS NOT NULL
                                       )
                                    OR (
                                         New.Amount IS NULL
                                         AND Old.Amount IS NOT NULL
                                       ); 
                IF UPDATE(TransCodeId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerFeeId
                                   ,'TransCodeId'
                                   ,CONVERT(VARCHAR(8000),Old.TransCodeId,121)
                                   ,CONVERT(VARCHAR(8000),New.TransCodeId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerFeeId = New.PrgVerFeeId
                            WHERE   Old.TransCodeId <> New.TransCodeId
                                    OR (
                                         Old.TransCodeId IS NULL
                                         AND New.TransCodeId IS NOT NULL
                                       )
                                    OR (
                                         New.TransCodeId IS NULL
                                         AND Old.TransCodeId IS NOT NULL
                                       ); 
                IF UPDATE(TuitionCategoryId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerFeeId
                                   ,'TuitionCategoryId'
                                   ,CONVERT(VARCHAR(8000),Old.TuitionCategoryId,121)
                                   ,CONVERT(VARCHAR(8000),New.TuitionCategoryId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerFeeId = New.PrgVerFeeId
                            WHERE   Old.TuitionCategoryId <> New.TuitionCategoryId
                                    OR (
                                         Old.TuitionCategoryId IS NULL
                                         AND New.TuitionCategoryId IS NOT NULL
                                       )
                                    OR (
                                         New.TuitionCategoryId IS NULL
                                         AND Old.TuitionCategoryId IS NOT NULL
                                       ); 
                IF UPDATE(RateScheduleId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerFeeId
                                   ,'RateScheduleId'
                                   ,CONVERT(VARCHAR(8000),Old.RateScheduleId,121)
                                   ,CONVERT(VARCHAR(8000),New.RateScheduleId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerFeeId = New.PrgVerFeeId
                            WHERE   Old.RateScheduleId <> New.RateScheduleId
                                    OR (
                                         Old.RateScheduleId IS NULL
                                         AND New.RateScheduleId IS NOT NULL
                                       )
                                    OR (
                                         New.RateScheduleId IS NULL
                                         AND Old.RateScheduleId IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerFeeId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerFeeId = New.PrgVerFeeId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(PrgVerId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerFeeId
                                   ,'PrgVerId'
                                   ,CONVERT(VARCHAR(8000),Old.PrgVerId,121)
                                   ,CONVERT(VARCHAR(8000),New.PrgVerId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerFeeId = New.PrgVerFeeId
                            WHERE   Old.PrgVerId <> New.PrgVerId
                                    OR (
                                         Old.PrgVerId IS NULL
                                         AND New.PrgVerId IS NOT NULL
                                       )
                                    OR (
                                         New.PrgVerId IS NULL
                                         AND Old.PrgVerId IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[saProgramVersionFees] ADD CONSTRAINT [PK_saProgramVersionFees_PrgVerFeeId] PRIMARY KEY CLUSTERED  ([PrgVerFeeId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saProgramVersionFees] ADD CONSTRAINT [FK_saProgramVersionFees_arPrgVersions_PrgVerId_PrgVerId] FOREIGN KEY ([PrgVerId]) REFERENCES [dbo].[arPrgVersions] ([PrgVerId])
GO
ALTER TABLE [dbo].[saProgramVersionFees] ADD CONSTRAINT [FK_saProgramVersionFees_saRateSchedules_RateScheduleId_RateScheduleId] FOREIGN KEY ([RateScheduleId]) REFERENCES [dbo].[saRateSchedules] ([RateScheduleId])
GO
ALTER TABLE [dbo].[saProgramVersionFees] ADD CONSTRAINT [FK_saProgramVersionFees_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
ALTER TABLE [dbo].[saProgramVersionFees] ADD CONSTRAINT [FK_saProgramVersionFees_saTransCodes_TransCodeId_TransCodeId] FOREIGN KEY ([TransCodeId]) REFERENCES [dbo].[saTransCodes] ([TransCodeId])
GO
ALTER TABLE [dbo].[saProgramVersionFees] ADD CONSTRAINT [FK_saProgramVersionFees_saTuitionCategories_TuitionCategoryId_TuitionCategoryId] FOREIGN KEY ([TuitionCategoryId]) REFERENCES [dbo].[saTuitionCategories] ([TuitionCategoryId])
GO
ALTER TABLE [dbo].[saProgramVersionFees] ADD CONSTRAINT [FK_saProgramVersionFees_syProgramUnitTypes_UnitId_UnitId] FOREIGN KEY ([UnitId]) REFERENCES [dbo].[syProgramUnitTypes] ([UnitId])
GO
