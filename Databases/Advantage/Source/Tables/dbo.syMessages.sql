CREATE TABLE [dbo].[syMessages]
(
[MessageId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syMessages_MessageId] DEFAULT (newid()),
[MessageTemplateId] [uniqueidentifier] NOT NULL,
[RecipientId] [uniqueidentifier] NOT NULL,
[StudentId] [uniqueidentifier] NULL,
[XmlContent] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[ActuallySentOn] [datetime] NULL,
[ModDate] [datetime] NOT NULL CONSTRAINT [DF_syMessages_ModDate] DEFAULT ('2005-01-01'),
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HtmlContent] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[syMessages] ADD CONSTRAINT [PK_syMessages_MessageId] PRIMARY KEY CLUSTERED  ([MessageId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syMessages] ADD CONSTRAINT [FK_syMessages_syMessageTemplates_MessageTemplateId_MessageTemplateId] FOREIGN KEY ([MessageTemplateId]) REFERENCES [dbo].[syMessageTemplates] ([MessageTemplateId])
GO
