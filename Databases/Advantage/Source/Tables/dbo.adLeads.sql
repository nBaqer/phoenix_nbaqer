CREATE TABLE [dbo].[adLeads]
(
[LeadId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_adLeads_LeadId] DEFAULT (newid()),
[ProspectID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MiddleName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SSN] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[Phone] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HomeEmail] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateId] [uniqueidentifier] NULL,
[Zip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeadStatus] [uniqueidentifier] NOT NULL,
[WorkEmail] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddressType] [uniqueidentifier] NULL,
[Prefix] [uniqueidentifier] NULL,
[Suffix] [uniqueidentifier] NULL,
[BirthDate] [datetime] NULL,
[Sponsor] [uniqueidentifier] NULL,
[AdmissionsRep] [uniqueidentifier] NULL,
[AssignedDate] [datetime] NULL CONSTRAINT [DF_adLeads_AssignedDate] DEFAULT (getdate()),
[Gender] [uniqueidentifier] NULL,
[Race] [uniqueidentifier] NULL,
[MaritalStatus] [uniqueidentifier] NULL,
[FamilyIncome] [uniqueidentifier] NULL,
[Children] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneType] [uniqueidentifier] NULL,
[PhoneStatus] [uniqueidentifier] NULL,
[SourceCategoryID] [uniqueidentifier] NULL,
[SourceTypeID] [uniqueidentifier] NULL,
[SourceDate] [datetime] NULL,
[AreaID] [uniqueidentifier] NULL,
[ProgramID] [uniqueidentifier] NULL,
[ExpectedStart] [datetime] NULL,
[ShiftID] [uniqueidentifier] NULL,
[Nationality] [uniqueidentifier] NULL,
[Citizen] [uniqueidentifier] NULL,
[DrivLicStateID] [uniqueidentifier] NULL,
[DrivLicNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AlienNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comments] [varchar] (240) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceAdvertisement] [uniqueidentifier] NULL,
[CampusId] [uniqueidentifier] NULL,
[PrgVerId] [uniqueidentifier] NULL,
[Country] [uniqueidentifier] NULL,
[County] [uniqueidentifier] NULL,
[Age] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PreviousEducation] [uniqueidentifier] NULL,
[AddressStatus] [uniqueidentifier] NULL,
[CreatedDate] [datetime] NULL,
[RecruitmentOffice] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OtherState] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ForeignPhone] [bit] NOT NULL CONSTRAINT [DF_adLeads_ForeignPhone] DEFAULT ((0)),
[ForeignZip] [bit] NOT NULL CONSTRAINT [DF_adLeads_ForeignZip] DEFAULT ((0)),
[LeadgrpId] [uniqueidentifier] NULL,
[DependencyTypeId] [uniqueidentifier] NULL,
[DegCertSeekingId] [uniqueidentifier] NULL,
[GeographicTypeId] [uniqueidentifier] NULL,
[HousingId] [uniqueidentifier] NULL,
[admincriteriaid] [uniqueidentifier] NULL,
[DateApplied] [datetime] NULL,
[InquiryTime] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdvertisementNote] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneType2] [uniqueidentifier] NULL,
[PhoneStatus2] [uniqueidentifier] NULL,
[ForeignPhone2] [bit] NULL CONSTRAINT [DF_adLeads_ForeignPhone2] DEFAULT ((0)),
[DefaultPhone] [tinyint] NULL CONSTRAINT [DF_adLeads_DefaultPhone] DEFAULT ((1)),
[IsDisabled] [bit] NULL,
[IsFirstTimeInSchool] [bit] NOT NULL CONSTRAINT [DF_adLeads_IsFirstTimeInSchool] DEFAULT ((0)),
[IsFirstTimePostSecSchool] [bit] NOT NULL CONSTRAINT [DF_adLeads_IsFirstTimePostSecSchool] DEFAULT ((0)),
[EntranceInterviewDate] [datetime] NULL,
[ProgramScheduleId] [uniqueidentifier] NULL,
[BestTime] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DistanceToSchool] [int] NULL,
[CampaignId] [int] NULL,
[ProgramOfInterest] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CampusOfInterest] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransportationId] [uniqueidentifier] NULL,
[NickName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_adLeads_NickName] DEFAULT (''),
[AttendTypeId] [uniqueidentifier] NULL,
[PreferredContactId] [int] NOT NULL CONSTRAINT [DF_adLeads_PreferredContactId] DEFAULT ((1)),
[AddressApt] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_adLeads_AddressApt] DEFAULT (''),
[NoneEmail] [bit] NOT NULL CONSTRAINT [DF_adLeads_NoneEmail] DEFAULT ((0)),
[HighSchoolId] [uniqueidentifier] NULL,
[HighSchoolGradDate] [datetime] NULL,
[AttendingHs] [bit] NULL,
[StudentId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_adLeads_StudentId] DEFAULT ('00000000-0000-0000-0000-000000000000'),
[StudentNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_adLeads_StudentNumber] DEFAULT (''),
[StudentStatusId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_adLeads_StudentStatusId] DEFAULT ('1AF592A6-8790-48EC-9916-5412C25EF49F'),
[ReasonNotEnrolledId] [uniqueidentifier] NULL,
[EnrollStateId] [uniqueidentifier] NULL,
[VendorId] [uniqueidentifier] NULL,
[AfaStudentId] [bigint] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--========================================================================================== 
-- TRIGGER adLeads_Audit_Delete 
-- INSERT  add Audit History when delete records in adLeads 
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeads_Audit_Delete]
ON [dbo].[adLeads]
FOR DELETE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);

        SET @AuditHistId = NEWID();
        SET @EventRows = (
                         SELECT COUNT(*)
                         FROM   Deleted
                         );
        SET @EventDate = (
                         SELECT TOP 1 ModDate
                         FROM   Deleted
                         );
        SET @UserName = (
                        SELECT TOP 1 ModUser
                        FROM   Deleted
                        );

        IF @EventRows > 0
            BEGIN
                EXEC fmAuditHistAdd @AuditHistId
                                   ,'adLeads'
                                   ,'D'
                                   ,@EventRows
                                   ,@EventDate
                                   ,@UserName;
                -- ProspectID 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'ProspectID'
                                  ,CONVERT(VARCHAR(MAX), Old.ProspectID)
                            FROM   Deleted AS Old;
                -- FirstName 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'FirstName'
                                  ,CONVERT(VARCHAR(MAX), Old.FirstName)
                            FROM   Deleted AS Old;
                -- LastName 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'LastName'
                                  ,CONVERT(VARCHAR(MAX), Old.LastName)
                            FROM   Deleted AS Old;
                -- MiddleName 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'MiddleName'
                                  ,CONVERT(VARCHAR(MAX), Old.MiddleName)
                            FROM   Deleted AS Old;
                -- SSN 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'SSN'
                                  ,CONVERT(VARCHAR(MAX), Old.SSN)
                            FROM   Deleted AS Old;
                -- ModUser 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'ModUser'
                                  ,CONVERT(VARCHAR(MAX), Old.ModUser)
                            FROM   Deleted AS Old;
                -- ModDate 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'ModDate'
                                  ,CONVERT(VARCHAR(MAX), Old.ModDate, 121)
                            FROM   Deleted AS Old;
                -- Phone 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'Phone'
                                  ,CONVERT(VARCHAR(MAX), Old.Phone)
                            FROM   Deleted AS Old;
                -- HomeEmail 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'HomeEmail'
                                  ,CONVERT(VARCHAR(MAX), Old.HomeEmail)
                            FROM   Deleted AS Old;
                -- Address1 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'Address1'
                                  ,CONVERT(VARCHAR(MAX), Old.Address1)
                            FROM   Deleted AS Old;
                -- Address2 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'Address2'
                                  ,CONVERT(VARCHAR(MAX), Old.Address2)
                            FROM   Deleted AS Old;
                -- City 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'City'
                                  ,CONVERT(VARCHAR(MAX), Old.City)
                            FROM   Deleted AS Old;
                -- StateId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'StateId'
                                  ,CONVERT(VARCHAR(MAX), Old.StateId)
                            FROM   Deleted AS Old;
                -- Zip 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'Zip'
                                  ,CONVERT(VARCHAR(MAX), Old.Zip)
                            FROM   Deleted AS Old;
                -- LeadStatus 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'LeadStatus'
                                  ,CONVERT(VARCHAR(MAX), Old.LeadStatus)
                            FROM   Deleted AS Old;
                -- WorkEmail 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'WorkEmail'
                                  ,CONVERT(VARCHAR(MAX), Old.WorkEmail)
                            FROM   Deleted AS Old;
                -- AddressType 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'AddressType'
                                  ,CONVERT(VARCHAR(MAX), Old.AddressType)
                            FROM   Deleted AS Old;
                -- Prefix 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'Prefix'
                                  ,CONVERT(VARCHAR(MAX), Old.Prefix)
                            FROM   Deleted AS Old;
                -- Suffix 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'Suffix'
                                  ,CONVERT(VARCHAR(MAX), Old.Suffix)
                            FROM   Deleted AS Old;
                -- BirthDate 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'BirthDate'
                                  ,CONVERT(VARCHAR(MAX), Old.BirthDate, 121)
                            FROM   Deleted AS Old;
                -- Sponsor 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'Sponsor'
                                  ,CONVERT(VARCHAR(MAX), Old.Sponsor)
                            FROM   Deleted AS Old;
                -- AdmissionsRep 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'AdmissionsRep'
                                  ,CONVERT(VARCHAR(MAX), Old.AdmissionsRep)
                            FROM   Deleted AS Old;
                -- AssignedDate 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'AssignedDate'
                                  ,CONVERT(VARCHAR(MAX), Old.AssignedDate, 121)
                            FROM   Deleted AS Old;
                -- Gender 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'Gender'
                                  ,CONVERT(VARCHAR(MAX), Old.Gender)
                            FROM   Deleted AS Old;
                -- Race 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'Race'
                                  ,CONVERT(VARCHAR(MAX), Old.Race)
                            FROM   Deleted AS Old;
                -- MaritalStatus 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'MaritalStatus'
                                  ,CONVERT(VARCHAR(MAX), Old.MaritalStatus)
                            FROM   Deleted AS Old;
                -- FamilyIncome 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'FamilyIncome'
                                  ,CONVERT(VARCHAR(MAX), Old.FamilyIncome)
                            FROM   Deleted AS Old;
                -- Children 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'Children'
                                  ,CONVERT(VARCHAR(MAX), Old.Children)
                            FROM   Deleted AS Old;
                -- PhoneType 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'PhoneType'
                                  ,CONVERT(VARCHAR(MAX), Old.PhoneType)
                            FROM   Deleted AS Old;
                -- PhoneStatus 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'PhoneStatus'
                                  ,CONVERT(VARCHAR(MAX), Old.PhoneStatus)
                            FROM   Deleted AS Old;
                -- SourceCategoryID 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'SourceCategoryID'
                                  ,CONVERT(VARCHAR(MAX), Old.SourceCategoryID)
                            FROM   Deleted AS Old;
                -- SourceTypeID 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'SourceTypeID'
                                  ,CONVERT(VARCHAR(MAX), Old.SourceTypeID)
                            FROM   Deleted AS Old;
                -- SourceDate 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'SourceDate'
                                  ,CONVERT(VARCHAR(MAX), Old.SourceDate, 121)
                            FROM   Deleted AS Old;
                -- AreaID 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'AreaID'
                                  ,CONVERT(VARCHAR(MAX), Old.AreaID)
                            FROM   Deleted AS Old;
                -- ProgramID 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'ProgramID'
                                  ,CONVERT(VARCHAR(MAX), Old.ProgramID)
                            FROM   Deleted AS Old;
                -- ExpectedStart 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'ExpectedStart'
                                  ,CONVERT(VARCHAR(MAX), Old.ExpectedStart, 121)
                            FROM   Deleted AS Old;
                -- ShiftID 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'ShiftID'
                                  ,CONVERT(VARCHAR(MAX), Old.ShiftID)
                            FROM   Deleted AS Old;
                -- Nationality 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'Nationality'
                                  ,CONVERT(VARCHAR(MAX), Old.Nationality)
                            FROM   Deleted AS Old;
                -- Citizen 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'Citizen'
                                  ,CONVERT(VARCHAR(MAX), Old.Citizen)
                            FROM   Deleted AS Old;
                -- DrivLicStateID 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'DrivLicStateID'
                                  ,CONVERT(VARCHAR(MAX), Old.DrivLicStateID)
                            FROM   Deleted AS Old;
                -- DrivLicNumber 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'DrivLicNumber'
                                  ,CONVERT(VARCHAR(MAX), Old.DrivLicNumber)
                            FROM   Deleted AS Old;
                -- AlienNumber 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'AlienNumber'
                                  ,CONVERT(VARCHAR(MAX), Old.AlienNumber)
                            FROM   Deleted AS Old;
                -- Comments 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'Comments'
                                  ,CONVERT(VARCHAR(MAX), Old.Comments)
                            FROM   Deleted AS Old;
                -- SourceAdvertisement 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'SourceAdvertisement'
                                  ,CONVERT(VARCHAR(MAX), Old.SourceAdvertisement)
                            FROM   Deleted AS Old;
                -- CampusId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'CampusId'
                                  ,CONVERT(VARCHAR(MAX), Old.CampusId)
                            FROM   Deleted AS Old;
                -- PrgVerId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'PrgVerId'
                                  ,CONVERT(VARCHAR(MAX), Old.PrgVerId)
                            FROM   Deleted AS Old;
                -- Country 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'Country'
                                  ,CONVERT(VARCHAR(MAX), Old.Country)
                            FROM   Deleted AS Old;
                -- County 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'County'
                                  ,CONVERT(VARCHAR(MAX), Old.County)
                            FROM   Deleted AS Old;
                -- Age 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'Age'
                                  ,CONVERT(VARCHAR(MAX), Old.Age)
                            FROM   Deleted AS Old;
                -- PreviousEducation 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'PreviousEducation'
                                  ,CONVERT(VARCHAR(MAX), Old.PreviousEducation)
                            FROM   Deleted AS Old;
                -- AddressStatus 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'AddressStatus'
                                  ,CONVERT(VARCHAR(MAX), Old.AddressStatus)
                            FROM   Deleted AS Old;
                -- CreatedDate 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'CreatedDate'
                                  ,CONVERT(VARCHAR(MAX), Old.CreatedDate, 121)
                            FROM   Deleted AS Old;
                -- RecruitmentOffice 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'RecruitmentOffice'
                                  ,CONVERT(VARCHAR(MAX), Old.RecruitmentOffice)
                            FROM   Deleted AS Old;
                -- OtherState 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'OtherState'
                                  ,CONVERT(VARCHAR(MAX), Old.OtherState)
                            FROM   Deleted AS Old;
                -- ForeignPhone 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'ForeignPhone'
                                  ,CONVERT(VARCHAR(MAX), Old.ForeignPhone)
                            FROM   Deleted AS Old;
                -- ForeignZip 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'ForeignZip'
                                  ,CONVERT(VARCHAR(MAX), Old.ForeignZip)
                            FROM   Deleted AS Old;
                -- LeadgrpId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'LeadgrpId'
                                  ,CONVERT(VARCHAR(MAX), Old.LeadgrpId)
                            FROM   Deleted AS Old;
                -- DependencyTypeId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'DependencyTypeId'
                                  ,CONVERT(VARCHAR(MAX), Old.DependencyTypeId)
                            FROM   Deleted AS Old;
                -- DegCertSeekingId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'DegCertSeekingId'
                                  ,CONVERT(VARCHAR(MAX), Old.DegCertSeekingId)
                            FROM   Deleted AS Old;
                -- GeographicTypeId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'GeographicTypeId'
                                  ,CONVERT(VARCHAR(MAX), Old.GeographicTypeId)
                            FROM   Deleted AS Old;
                -- HousingId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'HousingId'
                                  ,CONVERT(VARCHAR(MAX), Old.HousingId)
                            FROM   Deleted AS Old;
                -- admincriteriaid 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'admincriteriaid'
                                  ,CONVERT(VARCHAR(MAX), Old.admincriteriaid)
                            FROM   Deleted AS Old;
                -- DateApplied 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'DateApplied'
                                  ,CONVERT(VARCHAR(MAX), Old.DateApplied, 121)
                            FROM   Deleted AS Old;
                -- InquiryTime 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'InquiryTime'
                                  ,CONVERT(VARCHAR(MAX), Old.InquiryTime)
                            FROM   Deleted AS Old;
                -- AdvertisementNote 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'AdvertisementNote'
                                  ,CONVERT(VARCHAR(MAX), Old.AdvertisementNote)
                            FROM   Deleted AS Old;
                -- Phone2 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'Phone2'
                                  ,CONVERT(VARCHAR(MAX), Old.Phone2)
                            FROM   Deleted AS Old;
                -- PhoneType2 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'PhoneType2'
                                  ,CONVERT(VARCHAR(MAX), Old.PhoneType2)
                            FROM   Deleted AS Old;
                -- PhoneStatus2 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'PhoneStatus2'
                                  ,CONVERT(VARCHAR(MAX), Old.PhoneStatus2)
                            FROM   Deleted AS Old;
                -- ForeignPhone2 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'ForeignPhone2'
                                  ,CONVERT(VARCHAR(MAX), Old.ForeignPhone2)
                            FROM   Deleted AS Old;
                -- DefaultPhone 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'DefaultPhone'
                                  ,CONVERT(VARCHAR(MAX), Old.DefaultPhone)
                            FROM   Deleted AS Old;
                -- IsDisabled 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'IsDisabled'
                                  ,CONVERT(VARCHAR(MAX), Old.IsDisabled)
                            FROM   Deleted AS Old;
                -- IsFirstTimeInSchool 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'IsFirstTimeInSchool'
                                  ,CONVERT(VARCHAR(MAX), Old.IsFirstTimeInSchool)
                            FROM   Deleted AS Old;
                -- IsFirstTimePostSecSchool 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'IsFirstTimePostSecSchool'
                                  ,CONVERT(VARCHAR(MAX), Old.IsFirstTimePostSecSchool)
                            FROM   Deleted AS Old;
                -- EntranceInterviewDate 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'EntranceInterviewDate'
                                  ,CONVERT(VARCHAR(MAX), Old.EntranceInterviewDate, 121)
                            FROM   Deleted AS Old;
                -- ProgramScheduleId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'ProgramScheduleId'
                                  ,CONVERT(VARCHAR(MAX), Old.ProgramScheduleId)
                            FROM   Deleted AS Old;
                -- BestTime 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'BestTime'
                                  ,CONVERT(VARCHAR(MAX), Old.BestTime)
                            FROM   Deleted AS Old;
                -- DistanceToSchool 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'DistanceToSchool'
                                  ,CONVERT(VARCHAR(MAX), Old.DistanceToSchool)
                            FROM   Deleted AS Old;
                -- CampaignId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'CampaignId'
                                  ,CONVERT(VARCHAR(MAX), Old.CampaignId)
                            FROM   Deleted AS Old;
                -- ProgramOfInterest 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'ProgramOfInterest'
                                  ,CONVERT(VARCHAR(MAX), Old.ProgramOfInterest)
                            FROM   Deleted AS Old;
                -- CampusOfInterest 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'CampusOfInterest'
                                  ,CONVERT(VARCHAR(MAX), Old.CampusOfInterest)
                            FROM   Deleted AS Old;
                -- TransportationId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'TransportationId'
                                  ,CONVERT(VARCHAR(MAX), Old.TransportationId)
                            FROM   Deleted AS Old;
                -- NickName 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'NickName'
                                  ,CONVERT(VARCHAR(MAX), Old.NickName)
                            FROM   Deleted AS Old;
                -- AttendTypeId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'AttendTypeId'
                                  ,CONVERT(VARCHAR(MAX), Old.AttendTypeId)
                            FROM   Deleted AS Old;
                -- PreferredContactId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'PreferredContactId'
                                  ,CONVERT(VARCHAR(MAX), Old.PreferredContactId)
                            FROM   Deleted AS Old;
                -- AddressApt 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'AddressApt'
                                  ,CONVERT(VARCHAR(MAX), Old.AddressApt)
                            FROM   Deleted AS Old;
                -- NoneEmail 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'NoneEmail'
                                  ,CONVERT(VARCHAR(MAX), Old.NoneEmail)
                            FROM   Deleted AS Old;
                -- HighSchoolId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'HighSchoolId'
                                  ,CONVERT(VARCHAR(MAX), Old.HighSchoolId)
                            FROM   Deleted AS Old;
                -- HighSchoolGradDate 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'HighSchoolGradDate'
                                  ,CONVERT(VARCHAR(MAX), Old.HighSchoolGradDate, 121)
                            FROM   Deleted AS Old;
                -- AttendingHs 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'AttendingHs'
                                  ,CONVERT(VARCHAR(MAX), Old.AttendingHs)
                            FROM   Deleted AS Old;
                -- StudentId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'StudentId'
                                  ,CONVERT(VARCHAR(MAX), Old.StudentId)
                            FROM   Deleted AS Old;
                -- StudentNumber 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'StudentNumber'
                                  ,CONVERT(VARCHAR(MAX), Old.StudentNumber)
                            FROM   Deleted AS Old;
                -- StudentStatusId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'StudentStatusId'
                                  ,CONVERT(VARCHAR(MAX), Old.StudentStatusId)
                            FROM   Deleted AS Old;
                -- ReasonNotEnrolledId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'ReasonNotEnrolledId'
                                  ,CONVERT(VARCHAR(MAX), Old.ReasonNotEnrolledId)
                            FROM   Deleted AS Old;
                -- EnrollStateId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'EnrollStateId'
                                  ,CONVERT(VARCHAR(MAX), Old.EnrollStateId)
                            FROM   Deleted AS Old;
                -- VendorId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.LeadId
                                  ,'VendorId'
                                  ,CONVERT(VARCHAR(MAX), Old.VendorId)
                            FROM   Deleted AS Old;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END TRIGGER adLeads_Audit_Delete 
--========================================================================================== 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--========================================================================================== 
-- TRIGGER adLeads_Audit_Insert 
-- INSERT  add Audit History when insert records in adLeads 
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeads_Audit_Insert]
ON [dbo].[adLeads]
FOR INSERT
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);

        SET @AuditHistId = NEWID();
        SET @EventRows = (
                         SELECT COUNT(*)
                         FROM   Inserted
                         );
        SET @EventDate = (
                         SELECT TOP 1 ModDate
                         FROM   Inserted
                         );
        SET @UserName = (
                        SELECT TOP 1 ModUser
                        FROM   Inserted
                        );

        IF @EventRows > 0
            BEGIN
                EXEC fmAuditHistAdd @AuditHistId
                                   ,'adLeads'
                                   ,'I'
                                   ,@EventRows
                                   ,@EventDate
                                   ,@UserName;
                -- ProspectID 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'ProspectID'
                                  ,CONVERT(VARCHAR(MAX), New.ProspectID)
                            FROM   Inserted AS New;
                -- FirstName 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'FirstName'
                                  ,CONVERT(VARCHAR(MAX), New.FirstName)
                            FROM   Inserted AS New;
                -- LastName 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'LastName'
                                  ,CONVERT(VARCHAR(MAX), New.LastName)
                            FROM   Inserted AS New;
                -- MiddleName 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'MiddleName'
                                  ,CONVERT(VARCHAR(MAX), New.MiddleName)
                            FROM   Inserted AS New;
                -- SSN 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'SSN'
                                  ,CONVERT(VARCHAR(MAX), New.SSN)
                            FROM   Inserted AS New;
                -- ModUser 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'ModUser'
                                  ,CONVERT(VARCHAR(MAX), New.ModUser)
                            FROM   Inserted AS New;
                -- ModDate 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'ModDate'
                                  ,CONVERT(VARCHAR(MAX), New.ModDate, 121)
                            FROM   Inserted AS New;
                -- Phone 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'Phone'
                                  ,CONVERT(VARCHAR(MAX), New.Phone)
                            FROM   Inserted AS New;
                -- HomeEmail 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'HomeEmail'
                                  ,CONVERT(VARCHAR(MAX), New.HomeEmail)
                            FROM   Inserted AS New;
                -- Address1 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'Address1'
                                  ,CONVERT(VARCHAR(MAX), New.Address1)
                            FROM   Inserted AS New;
                -- Address2 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'Address2'
                                  ,CONVERT(VARCHAR(MAX), New.Address2)
                            FROM   Inserted AS New;
                -- City 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'City'
                                  ,CONVERT(VARCHAR(MAX), New.City)
                            FROM   Inserted AS New;
                -- StateId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'StateId'
                                  ,CONVERT(VARCHAR(MAX), New.StateId)
                            FROM   Inserted AS New;
                -- Zip 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'Zip'
                                  ,CONVERT(VARCHAR(MAX), New.Zip)
                            FROM   Inserted AS New;
                -- LeadStatus 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'LeadStatus'
                                  ,CONVERT(VARCHAR(MAX), New.LeadStatus)
                            FROM   Inserted AS New;
                -- WorkEmail 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'WorkEmail'
                                  ,CONVERT(VARCHAR(MAX), New.WorkEmail)
                            FROM   Inserted AS New;
                -- AddressType 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'AddressType'
                                  ,CONVERT(VARCHAR(MAX), New.AddressType)
                            FROM   Inserted AS New;
                -- Prefix 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'Prefix'
                                  ,CONVERT(VARCHAR(MAX), New.Prefix)
                            FROM   Inserted AS New;
                -- Suffix 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'Suffix'
                                  ,CONVERT(VARCHAR(MAX), New.Suffix)
                            FROM   Inserted AS New;
                -- BirthDate 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'BirthDate'
                                  ,CONVERT(VARCHAR(MAX), New.BirthDate, 121)
                            FROM   Inserted AS New;
                -- Sponsor 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'Sponsor'
                                  ,CONVERT(VARCHAR(MAX), New.Sponsor)
                            FROM   Inserted AS New;
                -- AdmissionsRep 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'AdmissionsRep'
                                  ,CONVERT(VARCHAR(MAX), New.AdmissionsRep)
                            FROM   Inserted AS New;
                -- AssignedDate 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'AssignedDate'
                                  ,CONVERT(VARCHAR(MAX), New.AssignedDate, 121)
                            FROM   Inserted AS New;
                -- Gender 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'Gender'
                                  ,CONVERT(VARCHAR(MAX), New.Gender)
                            FROM   Inserted AS New;
                -- Race 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'Race'
                                  ,CONVERT(VARCHAR(MAX), New.Race)
                            FROM   Inserted AS New;
                -- MaritalStatus 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'MaritalStatus'
                                  ,CONVERT(VARCHAR(MAX), New.MaritalStatus)
                            FROM   Inserted AS New;
                -- FamilyIncome 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'FamilyIncome'
                                  ,CONVERT(VARCHAR(MAX), New.FamilyIncome)
                            FROM   Inserted AS New;
                -- Children 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'Children'
                                  ,CONVERT(VARCHAR(MAX), New.Children)
                            FROM   Inserted AS New;
                -- PhoneType 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'PhoneType'
                                  ,CONVERT(VARCHAR(MAX), New.PhoneType)
                            FROM   Inserted AS New;
                -- PhoneStatus 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'PhoneStatus'
                                  ,CONVERT(VARCHAR(MAX), New.PhoneStatus)
                            FROM   Inserted AS New;
                -- SourceCategoryID 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'SourceCategoryID'
                                  ,CONVERT(VARCHAR(MAX), New.SourceCategoryID)
                            FROM   Inserted AS New;
                -- SourceTypeID 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'SourceTypeID'
                                  ,CONVERT(VARCHAR(MAX), New.SourceTypeID)
                            FROM   Inserted AS New;
                -- SourceDate 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'SourceDate'
                                  ,CONVERT(VARCHAR(MAX), New.SourceDate, 121)
                            FROM   Inserted AS New;
                -- AreaID 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'AreaID'
                                  ,CONVERT(VARCHAR(MAX), New.AreaID)
                            FROM   Inserted AS New;
                -- ProgramID 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'ProgramID'
                                  ,CONVERT(VARCHAR(MAX), New.ProgramID)
                            FROM   Inserted AS New;
                -- ExpectedStart 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'ExpectedStart'
                                  ,CONVERT(VARCHAR(MAX), New.ExpectedStart, 121)
                            FROM   Inserted AS New;
                -- ShiftID 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'ShiftID'
                                  ,CONVERT(VARCHAR(MAX), New.ShiftID)
                            FROM   Inserted AS New;
                -- Nationality 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'Nationality'
                                  ,CONVERT(VARCHAR(MAX), New.Nationality)
                            FROM   Inserted AS New;
                -- Citizen 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'Citizen'
                                  ,CONVERT(VARCHAR(MAX), New.Citizen)
                            FROM   Inserted AS New;
                -- DrivLicStateID 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'DrivLicStateID'
                                  ,CONVERT(VARCHAR(MAX), New.DrivLicStateID)
                            FROM   Inserted AS New;
                -- DrivLicNumber 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'DrivLicNumber'
                                  ,CONVERT(VARCHAR(MAX), New.DrivLicNumber)
                            FROM   Inserted AS New;
                -- AlienNumber 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'AlienNumber'
                                  ,CONVERT(VARCHAR(MAX), New.AlienNumber)
                            FROM   Inserted AS New;
                -- Comments 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'Comments'
                                  ,CONVERT(VARCHAR(MAX), New.Comments)
                            FROM   Inserted AS New;
                -- SourceAdvertisement 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'SourceAdvertisement'
                                  ,CONVERT(VARCHAR(MAX), New.SourceAdvertisement)
                            FROM   Inserted AS New;
                -- CampusId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'CampusId'
                                  ,CONVERT(VARCHAR(MAX), New.CampusId)
                            FROM   Inserted AS New;
                -- PrgVerId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'PrgVerId'
                                  ,CONVERT(VARCHAR(MAX), New.PrgVerId)
                            FROM   Inserted AS New;
                -- Country 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'Country'
                                  ,CONVERT(VARCHAR(MAX), New.Country)
                            FROM   Inserted AS New;
                -- County 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'County'
                                  ,CONVERT(VARCHAR(MAX), New.County)
                            FROM   Inserted AS New;
                -- Age 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'Age'
                                  ,CONVERT(VARCHAR(MAX), New.Age)
                            FROM   Inserted AS New;
                -- PreviousEducation 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'PreviousEducation'
                                  ,CONVERT(VARCHAR(MAX), New.PreviousEducation)
                            FROM   Inserted AS New;
                -- AddressStatus 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'AddressStatus'
                                  ,CONVERT(VARCHAR(MAX), New.AddressStatus)
                            FROM   Inserted AS New;
                -- CreatedDate 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'CreatedDate'
                                  ,CONVERT(VARCHAR(MAX), New.CreatedDate, 121)
                            FROM   Inserted AS New;
                -- RecruitmentOffice 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'RecruitmentOffice'
                                  ,CONVERT(VARCHAR(MAX), New.RecruitmentOffice)
                            FROM   Inserted AS New;
                -- OtherState 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'OtherState'
                                  ,CONVERT(VARCHAR(MAX), New.OtherState)
                            FROM   Inserted AS New;
                -- ForeignPhone 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'ForeignPhone'
                                  ,CONVERT(VARCHAR(MAX), New.ForeignPhone)
                            FROM   Inserted AS New;
                -- ForeignZip 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'ForeignZip'
                                  ,CONVERT(VARCHAR(MAX), New.ForeignZip)
                            FROM   Inserted AS New;
                -- LeadgrpId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'LeadgrpId'
                                  ,CONVERT(VARCHAR(MAX), New.LeadgrpId)
                            FROM   Inserted AS New;
                -- DependencyTypeId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'DependencyTypeId'
                                  ,CONVERT(VARCHAR(MAX), New.DependencyTypeId)
                            FROM   Inserted AS New;
                -- DegCertSeekingId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'DegCertSeekingId'
                                  ,CONVERT(VARCHAR(MAX), New.DegCertSeekingId)
                            FROM   Inserted AS New;
                -- GeographicTypeId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'GeographicTypeId'
                                  ,CONVERT(VARCHAR(MAX), New.GeographicTypeId)
                            FROM   Inserted AS New;
                -- HousingId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'HousingId'
                                  ,CONVERT(VARCHAR(MAX), New.HousingId)
                            FROM   Inserted AS New;
                -- admincriteriaid 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'admincriteriaid'
                                  ,CONVERT(VARCHAR(MAX), New.admincriteriaid)
                            FROM   Inserted AS New;
                -- DateApplied 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'DateApplied'
                                  ,CONVERT(VARCHAR(MAX), New.DateApplied, 121)
                            FROM   Inserted AS New;
                -- InquiryTime 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'InquiryTime'
                                  ,CONVERT(VARCHAR(MAX), New.InquiryTime)
                            FROM   Inserted AS New;
                -- AdvertisementNote 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'AdvertisementNote'
                                  ,CONVERT(VARCHAR(MAX), New.AdvertisementNote)
                            FROM   Inserted AS New;
                -- Phone2 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'Phone2'
                                  ,CONVERT(VARCHAR(MAX), New.Phone2)
                            FROM   Inserted AS New;
                -- PhoneType2 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'PhoneType2'
                                  ,CONVERT(VARCHAR(MAX), New.PhoneType2)
                            FROM   Inserted AS New;
                -- PhoneStatus2 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'PhoneStatus2'
                                  ,CONVERT(VARCHAR(MAX), New.PhoneStatus2)
                            FROM   Inserted AS New;
                -- ForeignPhone2 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'ForeignPhone2'
                                  ,CONVERT(VARCHAR(MAX), New.ForeignPhone2)
                            FROM   Inserted AS New;
                -- DefaultPhone 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'DefaultPhone'
                                  ,CONVERT(VARCHAR(MAX), New.DefaultPhone)
                            FROM   Inserted AS New;
                -- IsDisabled 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'IsDisabled'
                                  ,CONVERT(VARCHAR(MAX), New.IsDisabled)
                            FROM   Inserted AS New;
                -- IsFirstTimeInSchool 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'IsFirstTimeInSchool'
                                  ,CONVERT(VARCHAR(MAX), New.IsFirstTimeInSchool)
                            FROM   Inserted AS New;
                -- IsFirstTimePostSecSchool 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'IsFirstTimePostSecSchool'
                                  ,CONVERT(VARCHAR(MAX), New.IsFirstTimePostSecSchool)
                            FROM   Inserted AS New;
                -- EntranceInterviewDate 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'EntranceInterviewDate'
                                  ,CONVERT(VARCHAR(MAX), New.EntranceInterviewDate, 121)
                            FROM   Inserted AS New;
                -- ProgramScheduleId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'ProgramScheduleId'
                                  ,CONVERT(VARCHAR(MAX), New.ProgramScheduleId)
                            FROM   Inserted AS New;
                -- BestTime 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'BestTime'
                                  ,CONVERT(VARCHAR(MAX), New.BestTime)
                            FROM   Inserted AS New;
                -- DistanceToSchool 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'DistanceToSchool'
                                  ,CONVERT(VARCHAR(MAX), New.DistanceToSchool)
                            FROM   Inserted AS New;
                -- CampaignId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'CampaignId'
                                  ,CONVERT(VARCHAR(MAX), New.CampaignId)
                            FROM   Inserted AS New;
                -- ProgramOfInterest 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'ProgramOfInterest'
                                  ,CONVERT(VARCHAR(MAX), New.ProgramOfInterest)
                            FROM   Inserted AS New;
                -- CampusOfInterest 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'CampusOfInterest'
                                  ,CONVERT(VARCHAR(MAX), New.CampusOfInterest)
                            FROM   Inserted AS New;
                -- TransportationId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'TransportationId'
                                  ,CONVERT(VARCHAR(MAX), New.TransportationId)
                            FROM   Inserted AS New;
                -- NickName 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'NickName'
                                  ,CONVERT(VARCHAR(MAX), New.NickName)
                            FROM   Inserted AS New;
                -- AttendTypeId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'AttendTypeId'
                                  ,CONVERT(VARCHAR(MAX), New.AttendTypeId)
                            FROM   Inserted AS New;
                -- PreferredContactId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'PreferredContactId'
                                  ,CONVERT(VARCHAR(MAX), New.PreferredContactId)
                            FROM   Inserted AS New;
                -- AddressApt 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'AddressApt'
                                  ,CONVERT(VARCHAR(MAX), New.AddressApt)
                            FROM   Inserted AS New;
                -- NoneEmail 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'NoneEmail'
                                  ,CONVERT(VARCHAR(MAX), New.NoneEmail)
                            FROM   Inserted AS New;
                -- HighSchoolId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'HighSchoolId'
                                  ,CONVERT(VARCHAR(MAX), New.HighSchoolId)
                            FROM   Inserted AS New;
                -- HighSchoolGradDate 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'HighSchoolGradDate'
                                  ,CONVERT(VARCHAR(MAX), New.HighSchoolGradDate, 121)
                            FROM   Inserted AS New;
                -- AttendingHs 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'AttendingHs'
                                  ,CONVERT(VARCHAR(MAX), New.AttendingHs)
                            FROM   Inserted AS New;
                -- StudentId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'StudentId'
                                  ,CONVERT(VARCHAR(MAX), New.StudentId)
                            FROM   Inserted AS New;
                -- StudentNumber 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'StudentNumber'
                                  ,CONVERT(VARCHAR(MAX), New.StudentNumber)
                            FROM   Inserted AS New;
                -- StudentStatusId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'StudentStatusId'
                                  ,CONVERT(VARCHAR(MAX), New.StudentStatusId)
                            FROM   Inserted AS New;
                -- ReasonNotEnrolledId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'ReasonNotEnrolledId'
                                  ,CONVERT(VARCHAR(MAX), New.ReasonNotEnrolledId)
                            FROM   Inserted AS New;
                -- EnrollStateId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'EnrollStateId'
                                  ,CONVERT(VARCHAR(MAX), New.EnrollStateId)
                            FROM   Inserted AS New;
                -- VendorId 
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.LeadId
                                  ,'VendorId'
                                  ,CONVERT(VARCHAR(MAX), New.VendorId)
                            FROM   Inserted AS New;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END TRIGGER adLeads_Audit_Insert 
--========================================================================================== 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--========================================================================================== 
-- TRIGGER adLeads_Audit_Update 
-- UPDATE  add Audit History when update fields in adLeads 
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeads_Audit_Update]
ON [dbo].[adLeads]
FOR UPDATE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);

        SET @AuditHistId = NEWID();
        SET @EventRows = (
                         SELECT COUNT(*)
                         FROM   Inserted
                         );
        SET @EventDate = (
                         SELECT TOP 1 ModDate
                         FROM   Inserted
                         );
        SET @UserName = (
                        SELECT TOP 1 ModUser
                        FROM   Inserted
                        );

        IF @EventRows > 0
            BEGIN
                EXEC fmAuditHistAdd @AuditHistId
                                   ,'adLeads'
                                   ,'U'
                                   ,@EventRows
                                   ,@EventDate
                                   ,@UserName;
                -- ProspectID 
                IF UPDATE(ProspectID)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'ProspectID'
                                          ,CONVERT(VARCHAR(MAX), Old.ProspectID)
                                          ,CONVERT(VARCHAR(MAX), New.ProspectID)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.ProspectID <> New.ProspectID
                                           OR (
                                              Old.ProspectID IS NULL
                                              AND New.ProspectID IS NOT NULL
                                              )
                                           OR (
                                              New.ProspectID IS NULL
                                              AND Old.ProspectID IS NOT NULL
                                              );
                    END;
                -- FirstName 
                IF UPDATE(FirstName)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'FirstName'
                                          ,CONVERT(VARCHAR(MAX), Old.FirstName)
                                          ,CONVERT(VARCHAR(MAX), New.FirstName)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.FirstName <> New.FirstName
                                           OR (
                                              Old.FirstName IS NULL
                                              AND New.FirstName IS NOT NULL
                                              )
                                           OR (
                                              New.FirstName IS NULL
                                              AND Old.FirstName IS NOT NULL
                                              );
                    END;
                -- LastName 
                IF UPDATE(LastName)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'LastName'
                                          ,CONVERT(VARCHAR(MAX), Old.LastName)
                                          ,CONVERT(VARCHAR(MAX), New.LastName)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.LastName <> New.LastName
                                           OR (
                                              Old.LastName IS NULL
                                              AND New.LastName IS NOT NULL
                                              )
                                           OR (
                                              New.LastName IS NULL
                                              AND Old.LastName IS NOT NULL
                                              );
                    END;
                -- MiddleName 
                IF UPDATE(MiddleName)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'MiddleName'
                                          ,CONVERT(VARCHAR(MAX), Old.MiddleName)
                                          ,CONVERT(VARCHAR(MAX), New.MiddleName)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.MiddleName <> New.MiddleName
                                           OR (
                                              Old.MiddleName IS NULL
                                              AND New.MiddleName IS NOT NULL
                                              )
                                           OR (
                                              New.MiddleName IS NULL
                                              AND Old.MiddleName IS NOT NULL
                                              );
                    END;
                -- SSN 
                IF UPDATE(SSN)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'SSN'
                                          ,CONVERT(VARCHAR(MAX), Old.SSN)
                                          ,CONVERT(VARCHAR(MAX), New.SSN)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.SSN <> New.SSN
                                           OR (
                                              Old.SSN IS NULL
                                              AND New.SSN IS NOT NULL
                                              )
                                           OR (
                                              New.SSN IS NULL
                                              AND Old.SSN IS NOT NULL
                                              );
                    END;
                -- ModUser 
                IF UPDATE(ModUser)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'ModUser'
                                          ,CONVERT(VARCHAR(MAX), Old.ModUser)
                                          ,CONVERT(VARCHAR(MAX), New.ModUser)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.ModUser <> New.ModUser
                                           OR (
                                              Old.ModUser IS NULL
                                              AND New.ModUser IS NOT NULL
                                              )
                                           OR (
                                              New.ModUser IS NULL
                                              AND Old.ModUser IS NOT NULL
                                              );
                    END;
                -- ModDate 
                IF UPDATE(ModDate)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'ModDate'
                                          ,CONVERT(VARCHAR(MAX), Old.ModDate, 121)
                                          ,CONVERT(VARCHAR(MAX), New.ModDate, 121)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.ModDate <> New.ModDate
                                           OR (
                                              Old.ModDate IS NULL
                                              AND New.ModDate IS NOT NULL
                                              )
                                           OR (
                                              New.ModDate IS NULL
                                              AND Old.ModDate IS NOT NULL
                                              );
                    END;
                -- Phone 
                IF UPDATE(Phone)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'Phone'
                                          ,CONVERT(VARCHAR(MAX), Old.Phone)
                                          ,CONVERT(VARCHAR(MAX), New.Phone)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.Phone <> New.Phone
                                           OR (
                                              Old.Phone IS NULL
                                              AND New.Phone IS NOT NULL
                                              )
                                           OR (
                                              New.Phone IS NULL
                                              AND Old.Phone IS NOT NULL
                                              );
                    END;
                -- HomeEmail 
                IF UPDATE(HomeEmail)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'HomeEmail'
                                          ,CONVERT(VARCHAR(MAX), Old.HomeEmail)
                                          ,CONVERT(VARCHAR(MAX), New.HomeEmail)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.HomeEmail <> New.HomeEmail
                                           OR (
                                              Old.HomeEmail IS NULL
                                              AND New.HomeEmail IS NOT NULL
                                              )
                                           OR (
                                              New.HomeEmail IS NULL
                                              AND Old.HomeEmail IS NOT NULL
                                              );
                    END;
                -- Address1 
                IF UPDATE(Address1)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'Address1'
                                          ,CONVERT(VARCHAR(MAX), Old.Address1)
                                          ,CONVERT(VARCHAR(MAX), New.Address1)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.Address1 <> New.Address1
                                           OR (
                                              Old.Address1 IS NULL
                                              AND New.Address1 IS NOT NULL
                                              )
                                           OR (
                                              New.Address1 IS NULL
                                              AND Old.Address1 IS NOT NULL
                                              );
                    END;
                -- Address2 
                IF UPDATE(Address2)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'Address2'
                                          ,CONVERT(VARCHAR(MAX), Old.Address2)
                                          ,CONVERT(VARCHAR(MAX), New.Address2)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.Address2 <> New.Address2
                                           OR (
                                              Old.Address2 IS NULL
                                              AND New.Address2 IS NOT NULL
                                              )
                                           OR (
                                              New.Address2 IS NULL
                                              AND Old.Address2 IS NOT NULL
                                              );
                    END;
                -- City 
                IF UPDATE(City)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'City'
                                          ,CONVERT(VARCHAR(MAX), Old.City)
                                          ,CONVERT(VARCHAR(MAX), New.City)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.City <> New.City
                                           OR (
                                              Old.City IS NULL
                                              AND New.City IS NOT NULL
                                              )
                                           OR (
                                              New.City IS NULL
                                              AND Old.City IS NOT NULL
                                              );
                    END;
                -- StateId 
                IF UPDATE(StateId)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'StateId'
                                          ,CONVERT(VARCHAR(MAX), Old.StateId)
                                          ,CONVERT(VARCHAR(MAX), New.StateId)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.StateId <> New.StateId
                                           OR (
                                              Old.StateId IS NULL
                                              AND New.StateId IS NOT NULL
                                              )
                                           OR (
                                              New.StateId IS NULL
                                              AND Old.StateId IS NOT NULL
                                              );
                    END;
                -- Zip 
                IF UPDATE(Zip)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'Zip'
                                          ,CONVERT(VARCHAR(MAX), Old.Zip)
                                          ,CONVERT(VARCHAR(MAX), New.Zip)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.Zip <> New.Zip
                                           OR (
                                              Old.Zip IS NULL
                                              AND New.Zip IS NOT NULL
                                              )
                                           OR (
                                              New.Zip IS NULL
                                              AND Old.Zip IS NOT NULL
                                              );
                    END;
                -- LeadStatus 
                IF UPDATE(LeadStatus)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'LeadStatus'
                                          ,CONVERT(VARCHAR(MAX), Old.LeadStatus)
                                          ,CONVERT(VARCHAR(MAX), New.LeadStatus)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.LeadStatus <> New.LeadStatus
                                           OR (
                                              Old.LeadStatus IS NULL
                                              AND New.LeadStatus IS NOT NULL
                                              )
                                           OR (
                                              New.LeadStatus IS NULL
                                              AND Old.LeadStatus IS NOT NULL
                                              );
                    END;
                -- WorkEmail 
                IF UPDATE(WorkEmail)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'WorkEmail'
                                          ,CONVERT(VARCHAR(MAX), Old.WorkEmail)
                                          ,CONVERT(VARCHAR(MAX), New.WorkEmail)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.WorkEmail <> New.WorkEmail
                                           OR (
                                              Old.WorkEmail IS NULL
                                              AND New.WorkEmail IS NOT NULL
                                              )
                                           OR (
                                              New.WorkEmail IS NULL
                                              AND Old.WorkEmail IS NOT NULL
                                              );
                    END;
                -- AddressType 
                IF UPDATE(AddressType)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'AddressType'
                                          ,CONVERT(VARCHAR(MAX), Old.AddressType)
                                          ,CONVERT(VARCHAR(MAX), New.AddressType)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.AddressType <> New.AddressType
                                           OR (
                                              Old.AddressType IS NULL
                                              AND New.AddressType IS NOT NULL
                                              )
                                           OR (
                                              New.AddressType IS NULL
                                              AND Old.AddressType IS NOT NULL
                                              );
                    END;
                -- Prefix 
                IF UPDATE(Prefix)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'Prefix'
                                          ,CONVERT(VARCHAR(MAX), Old.Prefix)
                                          ,CONVERT(VARCHAR(MAX), New.Prefix)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.Prefix <> New.Prefix
                                           OR (
                                              Old.Prefix IS NULL
                                              AND New.Prefix IS NOT NULL
                                              )
                                           OR (
                                              New.Prefix IS NULL
                                              AND Old.Prefix IS NOT NULL
                                              );
                    END;
                -- Suffix 
                IF UPDATE(Suffix)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'Suffix'
                                          ,CONVERT(VARCHAR(MAX), Old.Suffix)
                                          ,CONVERT(VARCHAR(MAX), New.Suffix)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.Suffix <> New.Suffix
                                           OR (
                                              Old.Suffix IS NULL
                                              AND New.Suffix IS NOT NULL
                                              )
                                           OR (
                                              New.Suffix IS NULL
                                              AND Old.Suffix IS NOT NULL
                                              );
                    END;
                -- BirthDate 
                IF UPDATE(BirthDate)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'BirthDate'
                                          ,CONVERT(VARCHAR(MAX), Old.BirthDate, 121)
                                          ,CONVERT(VARCHAR(MAX), New.BirthDate, 121)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.BirthDate <> New.BirthDate
                                           OR (
                                              Old.BirthDate IS NULL
                                              AND New.BirthDate IS NOT NULL
                                              )
                                           OR (
                                              New.BirthDate IS NULL
                                              AND Old.BirthDate IS NOT NULL
                                              );
                    END;
                -- Sponsor 
                IF UPDATE(Sponsor)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'Sponsor'
                                          ,CONVERT(VARCHAR(MAX), Old.Sponsor)
                                          ,CONVERT(VARCHAR(MAX), New.Sponsor)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.Sponsor <> New.Sponsor
                                           OR (
                                              Old.Sponsor IS NULL
                                              AND New.Sponsor IS NOT NULL
                                              )
                                           OR (
                                              New.Sponsor IS NULL
                                              AND Old.Sponsor IS NOT NULL
                                              );
                    END;
                -- AdmissionsRep 
                IF UPDATE(AdmissionsRep)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'AdmissionsRep'
                                          ,CONVERT(VARCHAR(MAX), Old.AdmissionsRep)
                                          ,CONVERT(VARCHAR(MAX), New.AdmissionsRep)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.AdmissionsRep <> New.AdmissionsRep
                                           OR (
                                              Old.AdmissionsRep IS NULL
                                              AND New.AdmissionsRep IS NOT NULL
                                              )
                                           OR (
                                              New.AdmissionsRep IS NULL
                                              AND Old.AdmissionsRep IS NOT NULL
                                              );
                    END;
                -- AssignedDate 
                IF UPDATE(AssignedDate)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'AssignedDate'
                                          ,CONVERT(VARCHAR(MAX), Old.AssignedDate, 121)
                                          ,CONVERT(VARCHAR(MAX), New.AssignedDate, 121)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.AssignedDate <> New.AssignedDate
                                           OR (
                                              Old.AssignedDate IS NULL
                                              AND New.AssignedDate IS NOT NULL
                                              )
                                           OR (
                                              New.AssignedDate IS NULL
                                              AND Old.AssignedDate IS NOT NULL
                                              );
                    END;
                -- Gender 
                IF UPDATE(Gender)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'Gender'
                                          ,CONVERT(VARCHAR(MAX), Old.Gender)
                                          ,CONVERT(VARCHAR(MAX), New.Gender)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.Gender <> New.Gender
                                           OR (
                                              Old.Gender IS NULL
                                              AND New.Gender IS NOT NULL
                                              )
                                           OR (
                                              New.Gender IS NULL
                                              AND Old.Gender IS NOT NULL
                                              );
                    END;
                -- Race 
                IF UPDATE(Race)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'Race'
                                          ,CONVERT(VARCHAR(MAX), Old.Race)
                                          ,CONVERT(VARCHAR(MAX), New.Race)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.Race <> New.Race
                                           OR (
                                              Old.Race IS NULL
                                              AND New.Race IS NOT NULL
                                              )
                                           OR (
                                              New.Race IS NULL
                                              AND Old.Race IS NOT NULL
                                              );
                    END;
                -- MaritalStatus 
                IF UPDATE(MaritalStatus)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'MaritalStatus'
                                          ,CONVERT(VARCHAR(MAX), Old.MaritalStatus)
                                          ,CONVERT(VARCHAR(MAX), New.MaritalStatus)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.MaritalStatus <> New.MaritalStatus
                                           OR (
                                              Old.MaritalStatus IS NULL
                                              AND New.MaritalStatus IS NOT NULL
                                              )
                                           OR (
                                              New.MaritalStatus IS NULL
                                              AND Old.MaritalStatus IS NOT NULL
                                              );
                    END;
                -- FamilyIncome 
                IF UPDATE(FamilyIncome)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'FamilyIncome'
                                          ,CONVERT(VARCHAR(MAX), Old.FamilyIncome)
                                          ,CONVERT(VARCHAR(MAX), New.FamilyIncome)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.FamilyIncome <> New.FamilyIncome
                                           OR (
                                              Old.FamilyIncome IS NULL
                                              AND New.FamilyIncome IS NOT NULL
                                              )
                                           OR (
                                              New.FamilyIncome IS NULL
                                              AND Old.FamilyIncome IS NOT NULL
                                              );
                    END;
                -- Children 
                IF UPDATE(Children)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'Children'
                                          ,CONVERT(VARCHAR(MAX), Old.Children)
                                          ,CONVERT(VARCHAR(MAX), New.Children)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.Children <> New.Children
                                           OR (
                                              Old.Children IS NULL
                                              AND New.Children IS NOT NULL
                                              )
                                           OR (
                                              New.Children IS NULL
                                              AND Old.Children IS NOT NULL
                                              );
                    END;
                -- PhoneType 
                IF UPDATE(PhoneType)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'PhoneType'
                                          ,CONVERT(VARCHAR(MAX), Old.PhoneType)
                                          ,CONVERT(VARCHAR(MAX), New.PhoneType)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.PhoneType <> New.PhoneType
                                           OR (
                                              Old.PhoneType IS NULL
                                              AND New.PhoneType IS NOT NULL
                                              )
                                           OR (
                                              New.PhoneType IS NULL
                                              AND Old.PhoneType IS NOT NULL
                                              );
                    END;
                -- PhoneStatus 
                IF UPDATE(PhoneStatus)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'PhoneStatus'
                                          ,CONVERT(VARCHAR(MAX), Old.PhoneStatus)
                                          ,CONVERT(VARCHAR(MAX), New.PhoneStatus)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.PhoneStatus <> New.PhoneStatus
                                           OR (
                                              Old.PhoneStatus IS NULL
                                              AND New.PhoneStatus IS NOT NULL
                                              )
                                           OR (
                                              New.PhoneStatus IS NULL
                                              AND Old.PhoneStatus IS NOT NULL
                                              );
                    END;
                -- SourceCategoryID 
                IF UPDATE(SourceCategoryID)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'SourceCategoryID'
                                          ,CONVERT(VARCHAR(MAX), Old.SourceCategoryID)
                                          ,CONVERT(VARCHAR(MAX), New.SourceCategoryID)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.SourceCategoryID <> New.SourceCategoryID
                                           OR (
                                              Old.SourceCategoryID IS NULL
                                              AND New.SourceCategoryID IS NOT NULL
                                              )
                                           OR (
                                              New.SourceCategoryID IS NULL
                                              AND Old.SourceCategoryID IS NOT NULL
                                              );
                    END;
                -- SourceTypeID 
                IF UPDATE(SourceTypeID)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'SourceTypeID'
                                          ,CONVERT(VARCHAR(MAX), Old.SourceTypeID)
                                          ,CONVERT(VARCHAR(MAX), New.SourceTypeID)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.SourceTypeID <> New.SourceTypeID
                                           OR (
                                              Old.SourceTypeID IS NULL
                                              AND New.SourceTypeID IS NOT NULL
                                              )
                                           OR (
                                              New.SourceTypeID IS NULL
                                              AND Old.SourceTypeID IS NOT NULL
                                              );
                    END;
                -- SourceDate 
                IF UPDATE(SourceDate)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'SourceDate'
                                          ,CONVERT(VARCHAR(MAX), Old.SourceDate, 121)
                                          ,CONVERT(VARCHAR(MAX), New.SourceDate, 121)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.SourceDate <> New.SourceDate
                                           OR (
                                              Old.SourceDate IS NULL
                                              AND New.SourceDate IS NOT NULL
                                              )
                                           OR (
                                              New.SourceDate IS NULL
                                              AND Old.SourceDate IS NOT NULL
                                              );
                    END;
                -- AreaID 
                IF UPDATE(AreaID)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'AreaID'
                                          ,CONVERT(VARCHAR(MAX), Old.AreaID)
                                          ,CONVERT(VARCHAR(MAX), New.AreaID)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.AreaID <> New.AreaID
                                           OR (
                                              Old.AreaID IS NULL
                                              AND New.AreaID IS NOT NULL
                                              )
                                           OR (
                                              New.AreaID IS NULL
                                              AND Old.AreaID IS NOT NULL
                                              );
                    END;
                -- ProgramID 
                IF UPDATE(ProgramID)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'ProgramID'
                                          ,CONVERT(VARCHAR(MAX), Old.ProgramID)
                                          ,CONVERT(VARCHAR(MAX), New.ProgramID)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.ProgramID <> New.ProgramID
                                           OR (
                                              Old.ProgramID IS NULL
                                              AND New.ProgramID IS NOT NULL
                                              )
                                           OR (
                                              New.ProgramID IS NULL
                                              AND Old.ProgramID IS NOT NULL
                                              );
                    END;
                -- ExpectedStart 
                IF UPDATE(ExpectedStart)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'ExpectedStart'
                                          ,CONVERT(VARCHAR(MAX), Old.ExpectedStart, 121)
                                          ,CONVERT(VARCHAR(MAX), New.ExpectedStart, 121)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.ExpectedStart <> New.ExpectedStart
                                           OR (
                                              Old.ExpectedStart IS NULL
                                              AND New.ExpectedStart IS NOT NULL
                                              )
                                           OR (
                                              New.ExpectedStart IS NULL
                                              AND Old.ExpectedStart IS NOT NULL
                                              );
                    END;
                -- ShiftID 
                IF UPDATE(ShiftID)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'ShiftID'
                                          ,CONVERT(VARCHAR(MAX), Old.ShiftID)
                                          ,CONVERT(VARCHAR(MAX), New.ShiftID)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.ShiftID <> New.ShiftID
                                           OR (
                                              Old.ShiftID IS NULL
                                              AND New.ShiftID IS NOT NULL
                                              )
                                           OR (
                                              New.ShiftID IS NULL
                                              AND Old.ShiftID IS NOT NULL
                                              );
                    END;
                -- Nationality 
                IF UPDATE(Nationality)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'Nationality'
                                          ,CONVERT(VARCHAR(MAX), Old.Nationality)
                                          ,CONVERT(VARCHAR(MAX), New.Nationality)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.Nationality <> New.Nationality
                                           OR (
                                              Old.Nationality IS NULL
                                              AND New.Nationality IS NOT NULL
                                              )
                                           OR (
                                              New.Nationality IS NULL
                                              AND Old.Nationality IS NOT NULL
                                              );
                    END;
                -- Citizen 
                IF UPDATE(Citizen)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'Citizen'
                                          ,CONVERT(VARCHAR(MAX), Old.Citizen)
                                          ,CONVERT(VARCHAR(MAX), New.Citizen)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.Citizen <> New.Citizen
                                           OR (
                                              Old.Citizen IS NULL
                                              AND New.Citizen IS NOT NULL
                                              )
                                           OR (
                                              New.Citizen IS NULL
                                              AND Old.Citizen IS NOT NULL
                                              );
                    END;
                -- DrivLicStateID 
                IF UPDATE(DrivLicStateID)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'DrivLicStateID'
                                          ,CONVERT(VARCHAR(MAX), Old.DrivLicStateID)
                                          ,CONVERT(VARCHAR(MAX), New.DrivLicStateID)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.DrivLicStateID <> New.DrivLicStateID
                                           OR (
                                              Old.DrivLicStateID IS NULL
                                              AND New.DrivLicStateID IS NOT NULL
                                              )
                                           OR (
                                              New.DrivLicStateID IS NULL
                                              AND Old.DrivLicStateID IS NOT NULL
                                              );
                    END;
                -- DrivLicNumber 
                IF UPDATE(DrivLicNumber)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'DrivLicNumber'
                                          ,CONVERT(VARCHAR(MAX), Old.DrivLicNumber)
                                          ,CONVERT(VARCHAR(MAX), New.DrivLicNumber)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.DrivLicNumber <> New.DrivLicNumber
                                           OR (
                                              Old.DrivLicNumber IS NULL
                                              AND New.DrivLicNumber IS NOT NULL
                                              )
                                           OR (
                                              New.DrivLicNumber IS NULL
                                              AND Old.DrivLicNumber IS NOT NULL
                                              );
                    END;
                -- AlienNumber 
                IF UPDATE(AlienNumber)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'AlienNumber'
                                          ,CONVERT(VARCHAR(MAX), Old.AlienNumber)
                                          ,CONVERT(VARCHAR(MAX), New.AlienNumber)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.AlienNumber <> New.AlienNumber
                                           OR (
                                              Old.AlienNumber IS NULL
                                              AND New.AlienNumber IS NOT NULL
                                              )
                                           OR (
                                              New.AlienNumber IS NULL
                                              AND Old.AlienNumber IS NOT NULL
                                              );
                    END;
                -- Comments 
                IF UPDATE(Comments)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'Comments'
                                          ,CONVERT(VARCHAR(MAX), Old.Comments)
                                          ,CONVERT(VARCHAR(MAX), New.Comments)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.Comments <> New.Comments
                                           OR (
                                              Old.Comments IS NULL
                                              AND New.Comments IS NOT NULL
                                              )
                                           OR (
                                              New.Comments IS NULL
                                              AND Old.Comments IS NOT NULL
                                              );
                    END;
                -- SourceAdvertisement 
                IF UPDATE(SourceAdvertisement)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'SourceAdvertisement'
                                          ,CONVERT(VARCHAR(MAX), Old.SourceAdvertisement)
                                          ,CONVERT(VARCHAR(MAX), New.SourceAdvertisement)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.SourceAdvertisement <> New.SourceAdvertisement
                                           OR (
                                              Old.SourceAdvertisement IS NULL
                                              AND New.SourceAdvertisement IS NOT NULL
                                              )
                                           OR (
                                              New.SourceAdvertisement IS NULL
                                              AND Old.SourceAdvertisement IS NOT NULL
                                              );
                    END;
                -- CampusId 
                IF UPDATE(CampusId)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'CampusId'
                                          ,CONVERT(VARCHAR(MAX), Old.CampusId)
                                          ,CONVERT(VARCHAR(MAX), New.CampusId)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.CampusId <> New.CampusId
                                           OR (
                                              Old.CampusId IS NULL
                                              AND New.CampusId IS NOT NULL
                                              )
                                           OR (
                                              New.CampusId IS NULL
                                              AND Old.CampusId IS NOT NULL
                                              );
                    END;
                -- PrgVerId 
                IF UPDATE(PrgVerId)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'PrgVerId'
                                          ,CONVERT(VARCHAR(MAX), Old.PrgVerId)
                                          ,CONVERT(VARCHAR(MAX), New.PrgVerId)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.PrgVerId <> New.PrgVerId
                                           OR (
                                              Old.PrgVerId IS NULL
                                              AND New.PrgVerId IS NOT NULL
                                              )
                                           OR (
                                              New.PrgVerId IS NULL
                                              AND Old.PrgVerId IS NOT NULL
                                              );
                    END;
                -- Country 
                IF UPDATE(Country)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'Country'
                                          ,CONVERT(VARCHAR(MAX), Old.Country)
                                          ,CONVERT(VARCHAR(MAX), New.Country)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.Country <> New.Country
                                           OR (
                                              Old.Country IS NULL
                                              AND New.Country IS NOT NULL
                                              )
                                           OR (
                                              New.Country IS NULL
                                              AND Old.Country IS NOT NULL
                                              );
                    END;
                -- County 
                IF UPDATE(County)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'County'
                                          ,CONVERT(VARCHAR(MAX), Old.County)
                                          ,CONVERT(VARCHAR(MAX), New.County)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.County <> New.County
                                           OR (
                                              Old.County IS NULL
                                              AND New.County IS NOT NULL
                                              )
                                           OR (
                                              New.County IS NULL
                                              AND Old.County IS NOT NULL
                                              );
                    END;
                -- Age 
                IF UPDATE(Age)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'Age'
                                          ,CONVERT(VARCHAR(MAX), Old.Age)
                                          ,CONVERT(VARCHAR(MAX), New.Age)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.Age <> New.Age
                                           OR (
                                              Old.Age IS NULL
                                              AND New.Age IS NOT NULL
                                              )
                                           OR (
                                              New.Age IS NULL
                                              AND Old.Age IS NOT NULL
                                              );
                    END;
                -- PreviousEducation 
                IF UPDATE(PreviousEducation)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'PreviousEducation'
                                          ,CONVERT(VARCHAR(MAX), Old.PreviousEducation)
                                          ,CONVERT(VARCHAR(MAX), New.PreviousEducation)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.PreviousEducation <> New.PreviousEducation
                                           OR (
                                              Old.PreviousEducation IS NULL
                                              AND New.PreviousEducation IS NOT NULL
                                              )
                                           OR (
                                              New.PreviousEducation IS NULL
                                              AND Old.PreviousEducation IS NOT NULL
                                              );
                    END;
                -- AddressStatus 
                IF UPDATE(AddressStatus)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'AddressStatus'
                                          ,CONVERT(VARCHAR(MAX), Old.AddressStatus)
                                          ,CONVERT(VARCHAR(MAX), New.AddressStatus)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.AddressStatus <> New.AddressStatus
                                           OR (
                                              Old.AddressStatus IS NULL
                                              AND New.AddressStatus IS NOT NULL
                                              )
                                           OR (
                                              New.AddressStatus IS NULL
                                              AND Old.AddressStatus IS NOT NULL
                                              );
                    END;
                -- CreatedDate 
                IF UPDATE(CreatedDate)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'CreatedDate'
                                          ,CONVERT(VARCHAR(MAX), Old.CreatedDate, 121)
                                          ,CONVERT(VARCHAR(MAX), New.CreatedDate, 121)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.CreatedDate <> New.CreatedDate
                                           OR (
                                              Old.CreatedDate IS NULL
                                              AND New.CreatedDate IS NOT NULL
                                              )
                                           OR (
                                              New.CreatedDate IS NULL
                                              AND Old.CreatedDate IS NOT NULL
                                              );
                    END;
                -- RecruitmentOffice 
                IF UPDATE(RecruitmentOffice)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'RecruitmentOffice'
                                          ,CONVERT(VARCHAR(MAX), Old.RecruitmentOffice)
                                          ,CONVERT(VARCHAR(MAX), New.RecruitmentOffice)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.RecruitmentOffice <> New.RecruitmentOffice
                                           OR (
                                              Old.RecruitmentOffice IS NULL
                                              AND New.RecruitmentOffice IS NOT NULL
                                              )
                                           OR (
                                              New.RecruitmentOffice IS NULL
                                              AND Old.RecruitmentOffice IS NOT NULL
                                              );
                    END;
                -- OtherState 
                IF UPDATE(OtherState)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'OtherState'
                                          ,CONVERT(VARCHAR(MAX), Old.OtherState)
                                          ,CONVERT(VARCHAR(MAX), New.OtherState)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.OtherState <> New.OtherState
                                           OR (
                                              Old.OtherState IS NULL
                                              AND New.OtherState IS NOT NULL
                                              )
                                           OR (
                                              New.OtherState IS NULL
                                              AND Old.OtherState IS NOT NULL
                                              );
                    END;
                -- ForeignPhone 
                IF UPDATE(ForeignPhone)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'ForeignPhone'
                                          ,CONVERT(VARCHAR(MAX), Old.ForeignPhone)
                                          ,CONVERT(VARCHAR(MAX), New.ForeignPhone)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.ForeignPhone <> New.ForeignPhone
                                           OR (
                                              Old.ForeignPhone IS NULL
                                              AND New.ForeignPhone IS NOT NULL
                                              )
                                           OR (
                                              New.ForeignPhone IS NULL
                                              AND Old.ForeignPhone IS NOT NULL
                                              );
                    END;
                -- ForeignZip 
                IF UPDATE(ForeignZip)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'ForeignZip'
                                          ,CONVERT(VARCHAR(MAX), Old.ForeignZip)
                                          ,CONVERT(VARCHAR(MAX), New.ForeignZip)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.ForeignZip <> New.ForeignZip
                                           OR (
                                              Old.ForeignZip IS NULL
                                              AND New.ForeignZip IS NOT NULL
                                              )
                                           OR (
                                              New.ForeignZip IS NULL
                                              AND Old.ForeignZip IS NOT NULL
                                              );
                    END;
                -- LeadgrpId 
                IF UPDATE(LeadgrpId)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'LeadgrpId'
                                          ,CONVERT(VARCHAR(MAX), Old.LeadgrpId)
                                          ,CONVERT(VARCHAR(MAX), New.LeadgrpId)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.LeadgrpId <> New.LeadgrpId
                                           OR (
                                              Old.LeadgrpId IS NULL
                                              AND New.LeadgrpId IS NOT NULL
                                              )
                                           OR (
                                              New.LeadgrpId IS NULL
                                              AND Old.LeadgrpId IS NOT NULL
                                              );
                    END;
                -- DependencyTypeId 
                IF UPDATE(DependencyTypeId)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'DependencyTypeId'
                                          ,CONVERT(VARCHAR(MAX), Old.DependencyTypeId)
                                          ,CONVERT(VARCHAR(MAX), New.DependencyTypeId)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.DependencyTypeId <> New.DependencyTypeId
                                           OR (
                                              Old.DependencyTypeId IS NULL
                                              AND New.DependencyTypeId IS NOT NULL
                                              )
                                           OR (
                                              New.DependencyTypeId IS NULL
                                              AND Old.DependencyTypeId IS NOT NULL
                                              );
                    END;
                -- DegCertSeekingId 
                IF UPDATE(DegCertSeekingId)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'DegCertSeekingId'
                                          ,CONVERT(VARCHAR(MAX), Old.DegCertSeekingId)
                                          ,CONVERT(VARCHAR(MAX), New.DegCertSeekingId)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.DegCertSeekingId <> New.DegCertSeekingId
                                           OR (
                                              Old.DegCertSeekingId IS NULL
                                              AND New.DegCertSeekingId IS NOT NULL
                                              )
                                           OR (
                                              New.DegCertSeekingId IS NULL
                                              AND Old.DegCertSeekingId IS NOT NULL
                                              );
                    END;
                -- GeographicTypeId 
                IF UPDATE(GeographicTypeId)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'GeographicTypeId'
                                          ,CONVERT(VARCHAR(MAX), Old.GeographicTypeId)
                                          ,CONVERT(VARCHAR(MAX), New.GeographicTypeId)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.GeographicTypeId <> New.GeographicTypeId
                                           OR (
                                              Old.GeographicTypeId IS NULL
                                              AND New.GeographicTypeId IS NOT NULL
                                              )
                                           OR (
                                              New.GeographicTypeId IS NULL
                                              AND Old.GeographicTypeId IS NOT NULL
                                              );
                    END;
                -- HousingId 
                IF UPDATE(HousingId)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'HousingId'
                                          ,CONVERT(VARCHAR(MAX), Old.HousingId)
                                          ,CONVERT(VARCHAR(MAX), New.HousingId)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.HousingId <> New.HousingId
                                           OR (
                                              Old.HousingId IS NULL
                                              AND New.HousingId IS NOT NULL
                                              )
                                           OR (
                                              New.HousingId IS NULL
                                              AND Old.HousingId IS NOT NULL
                                              );
                    END;
                -- admincriteriaid 
                IF UPDATE(admincriteriaid)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'admincriteriaid'
                                          ,CONVERT(VARCHAR(MAX), Old.admincriteriaid)
                                          ,CONVERT(VARCHAR(MAX), New.admincriteriaid)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.admincriteriaid <> New.admincriteriaid
                                           OR (
                                              Old.admincriteriaid IS NULL
                                              AND New.admincriteriaid IS NOT NULL
                                              )
                                           OR (
                                              New.admincriteriaid IS NULL
                                              AND Old.admincriteriaid IS NOT NULL
                                              );
                    END;
                -- DateApplied 
                IF UPDATE(DateApplied)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'DateApplied'
                                          ,CONVERT(VARCHAR(MAX), Old.DateApplied, 121)
                                          ,CONVERT(VARCHAR(MAX), New.DateApplied, 121)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.DateApplied <> New.DateApplied
                                           OR (
                                              Old.DateApplied IS NULL
                                              AND New.DateApplied IS NOT NULL
                                              )
                                           OR (
                                              New.DateApplied IS NULL
                                              AND Old.DateApplied IS NOT NULL
                                              );
                    END;
                -- InquiryTime 
                IF UPDATE(InquiryTime)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'InquiryTime'
                                          ,CONVERT(VARCHAR(MAX), Old.InquiryTime)
                                          ,CONVERT(VARCHAR(MAX), New.InquiryTime)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.InquiryTime <> New.InquiryTime
                                           OR (
                                              Old.InquiryTime IS NULL
                                              AND New.InquiryTime IS NOT NULL
                                              )
                                           OR (
                                              New.InquiryTime IS NULL
                                              AND Old.InquiryTime IS NOT NULL
                                              );
                    END;
                -- AdvertisementNote 
                IF UPDATE(AdvertisementNote)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'AdvertisementNote'
                                          ,CONVERT(VARCHAR(MAX), Old.AdvertisementNote)
                                          ,CONVERT(VARCHAR(MAX), New.AdvertisementNote)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.AdvertisementNote <> New.AdvertisementNote
                                           OR (
                                              Old.AdvertisementNote IS NULL
                                              AND New.AdvertisementNote IS NOT NULL
                                              )
                                           OR (
                                              New.AdvertisementNote IS NULL
                                              AND Old.AdvertisementNote IS NOT NULL
                                              );
                    END;
                -- Phone2 
                IF UPDATE(Phone2)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'Phone2'
                                          ,CONVERT(VARCHAR(MAX), Old.Phone2)
                                          ,CONVERT(VARCHAR(MAX), New.Phone2)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.Phone2 <> New.Phone2
                                           OR (
                                              Old.Phone2 IS NULL
                                              AND New.Phone2 IS NOT NULL
                                              )
                                           OR (
                                              New.Phone2 IS NULL
                                              AND Old.Phone2 IS NOT NULL
                                              );
                    END;
                -- PhoneType2 
                IF UPDATE(PhoneType2)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'PhoneType2'
                                          ,CONVERT(VARCHAR(MAX), Old.PhoneType2)
                                          ,CONVERT(VARCHAR(MAX), New.PhoneType2)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.PhoneType2 <> New.PhoneType2
                                           OR (
                                              Old.PhoneType2 IS NULL
                                              AND New.PhoneType2 IS NOT NULL
                                              )
                                           OR (
                                              New.PhoneType2 IS NULL
                                              AND Old.PhoneType2 IS NOT NULL
                                              );
                    END;
                -- PhoneStatus2 
                IF UPDATE(PhoneStatus2)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'PhoneStatus2'
                                          ,CONVERT(VARCHAR(MAX), Old.PhoneStatus2)
                                          ,CONVERT(VARCHAR(MAX), New.PhoneStatus2)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.PhoneStatus2 <> New.PhoneStatus2
                                           OR (
                                              Old.PhoneStatus2 IS NULL
                                              AND New.PhoneStatus2 IS NOT NULL
                                              )
                                           OR (
                                              New.PhoneStatus2 IS NULL
                                              AND Old.PhoneStatus2 IS NOT NULL
                                              );
                    END;
                -- ForeignPhone2 
                IF UPDATE(ForeignPhone2)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'ForeignPhone2'
                                          ,CONVERT(VARCHAR(MAX), Old.ForeignPhone2)
                                          ,CONVERT(VARCHAR(MAX), New.ForeignPhone2)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.ForeignPhone2 <> New.ForeignPhone2
                                           OR (
                                              Old.ForeignPhone2 IS NULL
                                              AND New.ForeignPhone2 IS NOT NULL
                                              )
                                           OR (
                                              New.ForeignPhone2 IS NULL
                                              AND Old.ForeignPhone2 IS NOT NULL
                                              );
                    END;
                -- DefaultPhone 
                IF UPDATE(DefaultPhone)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'DefaultPhone'
                                          ,CONVERT(VARCHAR(MAX), Old.DefaultPhone)
                                          ,CONVERT(VARCHAR(MAX), New.DefaultPhone)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.DefaultPhone <> New.DefaultPhone
                                           OR (
                                              Old.DefaultPhone IS NULL
                                              AND New.DefaultPhone IS NOT NULL
                                              )
                                           OR (
                                              New.DefaultPhone IS NULL
                                              AND Old.DefaultPhone IS NOT NULL
                                              );
                    END;
                -- IsDisabled 
                IF UPDATE(IsDisabled)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'IsDisabled'
                                          ,CONVERT(VARCHAR(MAX), Old.IsDisabled)
                                          ,CONVERT(VARCHAR(MAX), New.IsDisabled)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.IsDisabled <> New.IsDisabled
                                           OR (
                                              Old.IsDisabled IS NULL
                                              AND New.IsDisabled IS NOT NULL
                                              )
                                           OR (
                                              New.IsDisabled IS NULL
                                              AND Old.IsDisabled IS NOT NULL
                                              );
                    END;
                -- IsFirstTimeInSchool 
                IF UPDATE(IsFirstTimeInSchool)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'IsFirstTimeInSchool'
                                          ,CONVERT(VARCHAR(MAX), Old.IsFirstTimeInSchool)
                                          ,CONVERT(VARCHAR(MAX), New.IsFirstTimeInSchool)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.IsFirstTimeInSchool <> New.IsFirstTimeInSchool
                                           OR (
                                              Old.IsFirstTimeInSchool IS NULL
                                              AND New.IsFirstTimeInSchool IS NOT NULL
                                              )
                                           OR (
                                              New.IsFirstTimeInSchool IS NULL
                                              AND Old.IsFirstTimeInSchool IS NOT NULL
                                              );
                    END;
                -- IsFirstTimePostSecSchool 
                IF UPDATE(IsFirstTimePostSecSchool)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'IsFirstTimePostSecSchool'
                                          ,CONVERT(VARCHAR(MAX), Old.IsFirstTimePostSecSchool)
                                          ,CONVERT(VARCHAR(MAX), New.IsFirstTimePostSecSchool)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.IsFirstTimePostSecSchool <> New.IsFirstTimePostSecSchool
                                           OR (
                                              Old.IsFirstTimePostSecSchool IS NULL
                                              AND New.IsFirstTimePostSecSchool IS NOT NULL
                                              )
                                           OR (
                                              New.IsFirstTimePostSecSchool IS NULL
                                              AND Old.IsFirstTimePostSecSchool IS NOT NULL
                                              );
                    END;
                -- EntranceInterviewDate 
                IF UPDATE(EntranceInterviewDate)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'EntranceInterviewDate'
                                          ,CONVERT(VARCHAR(MAX), Old.EntranceInterviewDate, 121)
                                          ,CONVERT(VARCHAR(MAX), New.EntranceInterviewDate, 121)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.EntranceInterviewDate <> New.EntranceInterviewDate
                                           OR (
                                              Old.EntranceInterviewDate IS NULL
                                              AND New.EntranceInterviewDate IS NOT NULL
                                              )
                                           OR (
                                              New.EntranceInterviewDate IS NULL
                                              AND Old.EntranceInterviewDate IS NOT NULL
                                              );
                    END;
                -- ProgramScheduleId 
                IF UPDATE(ProgramScheduleId)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'ProgramScheduleId'
                                          ,CONVERT(VARCHAR(MAX), Old.ProgramScheduleId)
                                          ,CONVERT(VARCHAR(MAX), New.ProgramScheduleId)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.ProgramScheduleId <> New.ProgramScheduleId
                                           OR (
                                              Old.ProgramScheduleId IS NULL
                                              AND New.ProgramScheduleId IS NOT NULL
                                              )
                                           OR (
                                              New.ProgramScheduleId IS NULL
                                              AND Old.ProgramScheduleId IS NOT NULL
                                              );
                    END;
                -- BestTime 
                IF UPDATE(BestTime)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'BestTime'
                                          ,CONVERT(VARCHAR(MAX), Old.BestTime)
                                          ,CONVERT(VARCHAR(MAX), New.BestTime)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.BestTime <> New.BestTime
                                           OR (
                                              Old.BestTime IS NULL
                                              AND New.BestTime IS NOT NULL
                                              )
                                           OR (
                                              New.BestTime IS NULL
                                              AND Old.BestTime IS NOT NULL
                                              );
                    END;
                -- DistanceToSchool 
                IF UPDATE(DistanceToSchool)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'DistanceToSchool'
                                          ,CONVERT(VARCHAR(MAX), Old.DistanceToSchool)
                                          ,CONVERT(VARCHAR(MAX), New.DistanceToSchool)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.DistanceToSchool <> New.DistanceToSchool
                                           OR (
                                              Old.DistanceToSchool IS NULL
                                              AND New.DistanceToSchool IS NOT NULL
                                              )
                                           OR (
                                              New.DistanceToSchool IS NULL
                                              AND Old.DistanceToSchool IS NOT NULL
                                              );
                    END;
                -- CampaignId 
                IF UPDATE(CampaignId)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'CampaignId'
                                          ,CONVERT(VARCHAR(MAX), Old.CampaignId)
                                          ,CONVERT(VARCHAR(MAX), New.CampaignId)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.CampaignId <> New.CampaignId
                                           OR (
                                              Old.CampaignId IS NULL
                                              AND New.CampaignId IS NOT NULL
                                              )
                                           OR (
                                              New.CampaignId IS NULL
                                              AND Old.CampaignId IS NOT NULL
                                              );
                    END;
                -- ProgramOfInterest 
                IF UPDATE(ProgramOfInterest)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'ProgramOfInterest'
                                          ,CONVERT(VARCHAR(MAX), Old.ProgramOfInterest)
                                          ,CONVERT(VARCHAR(MAX), New.ProgramOfInterest)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.ProgramOfInterest <> New.ProgramOfInterest
                                           OR (
                                              Old.ProgramOfInterest IS NULL
                                              AND New.ProgramOfInterest IS NOT NULL
                                              )
                                           OR (
                                              New.ProgramOfInterest IS NULL
                                              AND Old.ProgramOfInterest IS NOT NULL
                                              );
                    END;
                -- CampusOfInterest 
                IF UPDATE(CampusOfInterest)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'CampusOfInterest'
                                          ,CONVERT(VARCHAR(MAX), Old.CampusOfInterest)
                                          ,CONVERT(VARCHAR(MAX), New.CampusOfInterest)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.CampusOfInterest <> New.CampusOfInterest
                                           OR (
                                              Old.CampusOfInterest IS NULL
                                              AND New.CampusOfInterest IS NOT NULL
                                              )
                                           OR (
                                              New.CampusOfInterest IS NULL
                                              AND Old.CampusOfInterest IS NOT NULL
                                              );
                    END;
                -- TransportationId 
                IF UPDATE(TransportationId)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'TransportationId'
                                          ,CONVERT(VARCHAR(MAX), Old.TransportationId)
                                          ,CONVERT(VARCHAR(MAX), New.TransportationId)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.TransportationId <> New.TransportationId
                                           OR (
                                              Old.TransportationId IS NULL
                                              AND New.TransportationId IS NOT NULL
                                              )
                                           OR (
                                              New.TransportationId IS NULL
                                              AND Old.TransportationId IS NOT NULL
                                              );
                    END;
                -- NickName 
                IF UPDATE(NickName)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'NickName'
                                          ,CONVERT(VARCHAR(MAX), Old.NickName)
                                          ,CONVERT(VARCHAR(MAX), New.NickName)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.NickName <> New.NickName
                                           OR (
                                              Old.NickName IS NULL
                                              AND New.NickName IS NOT NULL
                                              )
                                           OR (
                                              New.NickName IS NULL
                                              AND Old.NickName IS NOT NULL
                                              );
                    END;
                -- AttendTypeId 
                IF UPDATE(AttendTypeId)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'AttendTypeId'
                                          ,CONVERT(VARCHAR(MAX), Old.AttendTypeId)
                                          ,CONVERT(VARCHAR(MAX), New.AttendTypeId)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.AttendTypeId <> New.AttendTypeId
                                           OR (
                                              Old.AttendTypeId IS NULL
                                              AND New.AttendTypeId IS NOT NULL
                                              )
                                           OR (
                                              New.AttendTypeId IS NULL
                                              AND Old.AttendTypeId IS NOT NULL
                                              );
                    END;
                -- PreferredContactId 
                IF UPDATE(PreferredContactId)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'PreferredContactId'
                                          ,CONVERT(VARCHAR(MAX), Old.PreferredContactId)
                                          ,CONVERT(VARCHAR(MAX), New.PreferredContactId)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.PreferredContactId <> New.PreferredContactId
                                           OR (
                                              Old.PreferredContactId IS NULL
                                              AND New.PreferredContactId IS NOT NULL
                                              )
                                           OR (
                                              New.PreferredContactId IS NULL
                                              AND Old.PreferredContactId IS NOT NULL
                                              );
                    END;
                -- AddressApt 
                IF UPDATE(AddressApt)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'AddressApt'
                                          ,CONVERT(VARCHAR(MAX), Old.AddressApt)
                                          ,CONVERT(VARCHAR(MAX), New.AddressApt)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.AddressApt <> New.AddressApt
                                           OR (
                                              Old.AddressApt IS NULL
                                              AND New.AddressApt IS NOT NULL
                                              )
                                           OR (
                                              New.AddressApt IS NULL
                                              AND Old.AddressApt IS NOT NULL
                                              );
                    END;
                -- NoneEmail 
                IF UPDATE(NoneEmail)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'NoneEmail'
                                          ,CONVERT(VARCHAR(MAX), Old.NoneEmail)
                                          ,CONVERT(VARCHAR(MAX), New.NoneEmail)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.NoneEmail <> New.NoneEmail
                                           OR (
                                              Old.NoneEmail IS NULL
                                              AND New.NoneEmail IS NOT NULL
                                              )
                                           OR (
                                              New.NoneEmail IS NULL
                                              AND Old.NoneEmail IS NOT NULL
                                              );
                    END;
                -- HighSchoolId 
                IF UPDATE(HighSchoolId)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'HighSchoolId'
                                          ,CONVERT(VARCHAR(MAX), Old.HighSchoolId)
                                          ,CONVERT(VARCHAR(MAX), New.HighSchoolId)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.HighSchoolId <> New.HighSchoolId
                                           OR (
                                              Old.HighSchoolId IS NULL
                                              AND New.HighSchoolId IS NOT NULL
                                              )
                                           OR (
                                              New.HighSchoolId IS NULL
                                              AND Old.HighSchoolId IS NOT NULL
                                              );
                    END;
                -- HighSchoolGradDate 
                IF UPDATE(HighSchoolGradDate)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'HighSchoolGradDate'
                                          ,CONVERT(VARCHAR(MAX), Old.HighSchoolGradDate, 121)
                                          ,CONVERT(VARCHAR(MAX), New.HighSchoolGradDate, 121)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.HighSchoolGradDate <> New.HighSchoolGradDate
                                           OR (
                                              Old.HighSchoolGradDate IS NULL
                                              AND New.HighSchoolGradDate IS NOT NULL
                                              )
                                           OR (
                                              New.HighSchoolGradDate IS NULL
                                              AND Old.HighSchoolGradDate IS NOT NULL
                                              );
                    END;
                -- AttendingHs 
                IF UPDATE(AttendingHs)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'AttendingHs'
                                          ,CONVERT(VARCHAR(MAX), Old.AttendingHs)
                                          ,CONVERT(VARCHAR(MAX), New.AttendingHs)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.AttendingHs <> New.AttendingHs
                                           OR (
                                              Old.AttendingHs IS NULL
                                              AND New.AttendingHs IS NOT NULL
                                              )
                                           OR (
                                              New.AttendingHs IS NULL
                                              AND Old.AttendingHs IS NOT NULL
                                              );
                    END;
                -- StudentId 
                IF UPDATE(StudentId)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'StudentId'
                                          ,CONVERT(VARCHAR(MAX), Old.StudentId)
                                          ,CONVERT(VARCHAR(MAX), New.StudentId)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.StudentId <> New.StudentId
                                           OR (
                                              Old.StudentId IS NULL
                                              AND New.StudentId IS NOT NULL
                                              )
                                           OR (
                                              New.StudentId IS NULL
                                              AND Old.StudentId IS NOT NULL
                                              );
                    END;
                -- StudentNumber 
                IF UPDATE(StudentNumber)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'StudentNumber'
                                          ,CONVERT(VARCHAR(MAX), Old.StudentNumber)
                                          ,CONVERT(VARCHAR(MAX), New.StudentNumber)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.StudentNumber <> New.StudentNumber
                                           OR (
                                              Old.StudentNumber IS NULL
                                              AND New.StudentNumber IS NOT NULL
                                              )
                                           OR (
                                              New.StudentNumber IS NULL
                                              AND Old.StudentNumber IS NOT NULL
                                              );
                    END;
                -- StudentStatusId 
                IF UPDATE(StudentStatusId)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'StudentStatusId'
                                          ,CONVERT(VARCHAR(MAX), Old.StudentStatusId)
                                          ,CONVERT(VARCHAR(MAX), New.StudentStatusId)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.StudentStatusId <> New.StudentStatusId
                                           OR (
                                              Old.StudentStatusId IS NULL
                                              AND New.StudentStatusId IS NOT NULL
                                              )
                                           OR (
                                              New.StudentStatusId IS NULL
                                              AND Old.StudentStatusId IS NOT NULL
                                              );
                    END;
                -- ReasonNotEnrolledId 
                IF UPDATE(ReasonNotEnrolledId)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'ReasonNotEnrolledId'
                                          ,CONVERT(VARCHAR(MAX), Old.ReasonNotEnrolledId)
                                          ,CONVERT(VARCHAR(MAX), New.ReasonNotEnrolledId)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.ReasonNotEnrolledId <> New.ReasonNotEnrolledId
                                           OR (
                                              Old.ReasonNotEnrolledId IS NULL
                                              AND New.ReasonNotEnrolledId IS NOT NULL
                                              )
                                           OR (
                                              New.ReasonNotEnrolledId IS NULL
                                              AND Old.ReasonNotEnrolledId IS NOT NULL
                                              );
                    END;
                -- EnrollStateId 
                IF UPDATE(EnrollStateId)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'EnrollStateId'
                                          ,CONVERT(VARCHAR(MAX), Old.EnrollStateId)
                                          ,CONVERT(VARCHAR(MAX), New.EnrollStateId)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.EnrollStateId <> New.EnrollStateId
                                           OR (
                                              Old.EnrollStateId IS NULL
                                              AND New.EnrollStateId IS NOT NULL
                                              )
                                           OR (
                                              New.EnrollStateId IS NULL
                                              AND Old.EnrollStateId IS NOT NULL
                                              );
                    END;
                -- VendorId 
                IF UPDATE(VendorId)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT @AuditHistId
                                          ,New.LeadId
                                          ,'VendorId'
                                          ,CONVERT(VARCHAR(MAX), Old.VendorId)
                                          ,CONVERT(VARCHAR(MAX), New.VendorId)
                                    FROM   Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.LeadId = New.LeadId
                                    WHERE  Old.VendorId <> New.VendorId
                                           OR (
                                              Old.VendorId IS NULL
                                              AND New.VendorId IS NOT NULL
                                              )
                                           OR (
                                              New.VendorId IS NULL
                                              AND Old.VendorId IS NOT NULL
                                              );
                    END;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END TRIGGER adLeads_Audit_Update 
--========================================================================================== 
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [PK_adLeads_LeadId] PRIMARY KEY CLUSTERED  ([LeadId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_adLeads_CampusId_LeadId_ModDate] ON [dbo].[adLeads] ([CampusId]) INCLUDE ([LeadId], [ModDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_adLeads_FirstName] ON [dbo].[adLeads] ([FirstName]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_adLeads_LastName] ON [dbo].[adLeads] ([LastName]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_adLeads_SSN] ON [dbo].[adLeads] ([SSN]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_adLeads_StudentId] ON [dbo].[adLeads] ([StudentId]) ON [PRIMARY]
GO
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET CONCAT_NULL_YIELDS_NULL ON
GO
SET ARITHABORT ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_adLeads_StudentId_ExcludeStudentIdWithZeros] ON [dbo].[adLeads] ([StudentId]) WHERE ([StudentId]<>'00000000-0000-0000-0000-000000000000') ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_adLeads_StudentNumber] ON [dbo].[adLeads] ([StudentNumber]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_adAdminCriteria_admincriteriaid_admincriteriaid] FOREIGN KEY ([admincriteriaid]) REFERENCES [dbo].[adAdminCriteria] ([admincriteriaid])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_adAgencySponsors_Sponsor_AgencySpId] FOREIGN KEY ([Sponsor]) REFERENCES [dbo].[adAgencySponsors] ([AgencySpId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_adCitizenships_Citizen_CitizenshipId] FOREIGN KEY ([Citizen]) REFERENCES [dbo].[adCitizenships] ([CitizenshipId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_adCounties_County_CountyId] FOREIGN KEY ([County]) REFERENCES [dbo].[adCounties] ([CountyId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_adCountries_Country_CountryId] FOREIGN KEY ([Country]) REFERENCES [dbo].[adCountries] ([CountryId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_adDegCertSeeking_DegCertSeekingId_DegCertSeekingId] FOREIGN KEY ([DegCertSeekingId]) REFERENCES [dbo].[adDegCertSeeking] ([DegCertSeekingId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_adDependencyTypes_DependencyTypeId_DependencyTypeId] FOREIGN KEY ([DependencyTypeId]) REFERENCES [dbo].[adDependencyTypes] ([DependencyTypeId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_adEdLvls_PreviousEducation_EdLvlId] FOREIGN KEY ([PreviousEducation]) REFERENCES [dbo].[adEdLvls] ([EdLvlId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_adEthCodes_Race_EthCodeId] FOREIGN KEY ([Race]) REFERENCES [dbo].[adEthCodes] ([EthCodeId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_adGenders_Gender_GenderId] FOREIGN KEY ([Gender]) REFERENCES [dbo].[adGenders] ([GenderId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_adGeographicTypes_GeographicTypeId_GeographicTypeId] FOREIGN KEY ([GeographicTypeId]) REFERENCES [dbo].[adGeographicTypes] ([GeographicTypeId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_adLeadGroups_LeadGrpId_LeadGrpId] FOREIGN KEY ([LeadgrpId]) REFERENCES [dbo].[adLeadGroups] ([LeadGrpId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_adMaritalStatus_MaritalStatus_MaritalStatId] FOREIGN KEY ([MaritalStatus]) REFERENCES [dbo].[adMaritalStatus] ([MaritalStatId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_adNationalities_Nationality_NationalityId] FOREIGN KEY ([Nationality]) REFERENCES [dbo].[adNationalities] ([NationalityId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_adSourceAdvertisement_SourceAdvertisement_SourceAdvId] FOREIGN KEY ([SourceAdvertisement]) REFERENCES [dbo].[adSourceAdvertisement] ([SourceAdvId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_adSourceCatagory_SourceCategoryID_SourceCatagoryId] FOREIGN KEY ([SourceCategoryID]) REFERENCES [dbo].[adSourceCatagory] ([SourceCatagoryId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_adSourceType_SourceTypeID_SourceTypeId] FOREIGN KEY ([SourceTypeID]) REFERENCES [dbo].[adSourceType] ([SourceTypeId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_adVendorCampaign_CampaignId_CampaignId] FOREIGN KEY ([CampaignId]) REFERENCES [dbo].[adVendorCampaign] ([CampaignId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_arAttendTypes_AttendTypeId_AttendTypeId] FOREIGN KEY ([AttendTypeId]) REFERENCES [dbo].[arAttendTypes] ([AttendTypeId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_arHousing_HousingId_HousingId] FOREIGN KEY ([HousingId]) REFERENCES [dbo].[arHousing] ([HousingId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_arPrgGrp_AreaID_PrgGrpId] FOREIGN KEY ([AreaID]) REFERENCES [dbo].[arPrgGrp] ([PrgGrpId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_arPrgVersions_PrgVerId_PrgVerId] FOREIGN KEY ([PrgVerId]) REFERENCES [dbo].[arPrgVersions] ([PrgVerId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_arPrograms_ProgramID_ProgId] FOREIGN KEY ([ProgramID]) REFERENCES [dbo].[arPrograms] ([ProgId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_arProgSchedules_ProgramScheduleId_ScheduleId] FOREIGN KEY ([ProgramScheduleId]) REFERENCES [dbo].[arProgSchedules] ([ScheduleId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_arShifts_ShiftID_ShiftId] FOREIGN KEY ([ShiftID]) REFERENCES [dbo].[arShifts] ([ShiftId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_plAddressTypes_AddressType_AddressTypeId] FOREIGN KEY ([AddressType]) REFERENCES [dbo].[plAddressTypes] ([AddressTypeId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_syCampuses_CampusId_CampusId] FOREIGN KEY ([CampusId]) REFERENCES [dbo].[syCampuses] ([CampusId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_syFamilyIncome_FamilyIncome_FamilyIncomeID] FOREIGN KEY ([FamilyIncome]) REFERENCES [dbo].[syFamilyIncome] ([FamilyIncomeID])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_SyInstitutions_HighSchoolId_HSId] FOREIGN KEY ([HighSchoolId]) REFERENCES [dbo].[syInstitutions] ([HSId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_syPrefixes_Prefix_PrefixId] FOREIGN KEY ([Prefix]) REFERENCES [dbo].[syPrefixes] ([PrefixId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_syStates_DrivLicStateID_StateId] FOREIGN KEY ([DrivLicStateID]) REFERENCES [dbo].[syStates] ([StateId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_syStates_EnrollStateId_StateId] FOREIGN KEY ([EnrollStateId]) REFERENCES [dbo].[syStates] ([StateId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_syStates_StateId_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[syStates] ([StateId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_syStatusCodes_LeadStatus_StatusCodeId] FOREIGN KEY ([LeadStatus]) REFERENCES [dbo].[syStatusCodes] ([StatusCodeId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_syStatuses_AddressStatus_StatusId] FOREIGN KEY ([AddressStatus]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_syStatuses_PhoneStatus_StatusId] FOREIGN KEY ([PhoneStatus]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_AdLeads_syStatuses_StudentStatusId_StatusId] FOREIGN KEY ([StudentStatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_sySuffixes_Suffix_SuffixId] FOREIGN KEY ([Suffix]) REFERENCES [dbo].[sySuffixes] ([SuffixId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_syUsers_AdmissionsRep_UserId] FOREIGN KEY ([AdmissionsRep]) REFERENCES [dbo].[syUsers] ([UserId])
GO
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_syUsers_VendorId_UserId] FOREIGN KEY ([VendorId]) REFERENCES [dbo].[syUsers] ([UserId])
GO
