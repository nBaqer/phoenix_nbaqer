CREATE TABLE [dbo].[arHousing]
(
[HousingId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_arHousing_HousingId] DEFAULT (newid()),
[Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IPEDSValue] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arHousing] ADD CONSTRAINT [PK_arHousing_HousingId] PRIMARY KEY CLUSTERED  ([HousingId]) ON [PRIMARY]
GO
