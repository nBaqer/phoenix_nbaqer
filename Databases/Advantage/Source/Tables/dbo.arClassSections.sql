CREATE TABLE [dbo].[arClassSections]
(
[ClsSectionId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arClassSections_ClsSectionId] DEFAULT (newid()),
[TermId] [uniqueidentifier] NOT NULL,
[ReqId] [uniqueidentifier] NOT NULL,
[ClsSection] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InstructorId] [uniqueidentifier] NULL,
[StartDate] [datetime] NOT NULL,
[EndDate] [datetime] NOT NULL,
[MaxStud] [int] NOT NULL,
[InstrGrdBkWgtId] [uniqueidentifier] NULL,
[GrdScaleId] [uniqueidentifier] NOT NULL,
[CampusId] [uniqueidentifier] NULL,
[IsGraded] [bit] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL,
[CourseId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TermGUID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Session] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClassRoom] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ShiftId] [uniqueidentifier] NULL,
[PeriodId] [uniqueidentifier] NULL,
[StudentStartDate] [datetime] NULL,
[LeadGrpId] [uniqueidentifier] NULL,
[CohortStartDate] [datetime] NULL,
[ProgramVersionDefinitionId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[arClassSections_Audit_Delete] ON [dbo].[arClassSections]
    FOR DELETE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arClassSections','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ClsSectionId
                               ,'MaxStud'
                               ,CONVERT(VARCHAR(8000),Old.MaxStud,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ClsSectionId
                               ,'GrdScaleId'
                               ,CONVERT(VARCHAR(8000),Old.GrdScaleId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ClsSectionId
                               ,'InstructorId'
                               ,CONVERT(VARCHAR(8000),Old.InstructorId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ClsSectionId
                               ,'ReqId'
                               ,CONVERT(VARCHAR(8000),Old.ReqId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ClsSectionId
                               ,'TermId'
                               ,CONVERT(VARCHAR(8000),Old.TermId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ClsSectionId
                               ,'InstrGrdBkWgtId'
                               ,CONVERT(VARCHAR(8000),Old.InstrGrdBkWgtId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ClsSectionId
                               ,'CampusId'
                               ,CONVERT(VARCHAR(8000),Old.CampusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ClsSectionId
                               ,'StartDate'
                               ,CONVERT(VARCHAR(8000),Old.StartDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ClsSectionId
                               ,'EndDate'
                               ,CONVERT(VARCHAR(8000),Old.EndDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ClsSectionId
                               ,'ClsSection'
                               ,CONVERT(VARCHAR(8000),Old.ClsSection,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ClsSectionId
                               ,'IsGraded'
                               ,CONVERT(VARCHAR(8000),Old.IsGraded,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ClsSectionId
                               ,'ShiftId'
                               ,CONVERT(VARCHAR(8000),Old.ShiftId,121)
                        FROM    Deleted Old; 
            END; 
        END; 



    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[arClassSections_Audit_Insert] ON [dbo].[arClassSections]
    FOR INSERT
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arClassSections','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ClsSectionId
                               ,'MaxStud'
                               ,CONVERT(VARCHAR(8000),New.MaxStud,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ClsSectionId
                               ,'GrdScaleId'
                               ,CONVERT(VARCHAR(8000),New.GrdScaleId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ClsSectionId
                               ,'InstructorId'
                               ,CONVERT(VARCHAR(8000),New.InstructorId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ClsSectionId
                               ,'ReqId'
                               ,CONVERT(VARCHAR(8000),New.ReqId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ClsSectionId
                               ,'TermId'
                               ,CONVERT(VARCHAR(8000),New.TermId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ClsSectionId
                               ,'InstrGrdBkWgtId'
                               ,CONVERT(VARCHAR(8000),New.InstrGrdBkWgtId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ClsSectionId
                               ,'CampusId'
                               ,CONVERT(VARCHAR(8000),New.CampusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ClsSectionId
                               ,'StartDate'
                               ,CONVERT(VARCHAR(8000),New.StartDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ClsSectionId
                               ,'EndDate'
                               ,CONVERT(VARCHAR(8000),New.EndDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ClsSectionId
                               ,'ClsSection'
                               ,CONVERT(VARCHAR(8000),New.ClsSection,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ClsSectionId
                               ,'IsGraded'
                               ,CONVERT(VARCHAR(8000),New.IsGraded,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ClsSectionId
                               ,'ShiftId'
                               ,CONVERT(VARCHAR(8000),New.ShiftId,121)
                        FROM    Inserted New; 
            END; 
        END; 



    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[arClassSections_Audit_Update] ON [dbo].[arClassSections]
    FOR UPDATE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arClassSections','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(MaxStud)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ClsSectionId
                                   ,'MaxStud'
                                   ,CONVERT(VARCHAR(8000),Old.MaxStud,121)
                                   ,CONVERT(VARCHAR(8000),New.MaxStud,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ClsSectionId = New.ClsSectionId
                            WHERE   Old.MaxStud <> New.MaxStud
                                    OR (
                                         Old.MaxStud IS NULL
                                         AND New.MaxStud IS NOT NULL
                                       )
                                    OR (
                                         New.MaxStud IS NULL
                                         AND Old.MaxStud IS NOT NULL
                                       ); 
                IF UPDATE(GrdScaleId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ClsSectionId
                                   ,'GrdScaleId'
                                   ,CONVERT(VARCHAR(8000),Old.GrdScaleId,121)
                                   ,CONVERT(VARCHAR(8000),New.GrdScaleId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ClsSectionId = New.ClsSectionId
                            WHERE   Old.GrdScaleId <> New.GrdScaleId
                                    OR (
                                         Old.GrdScaleId IS NULL
                                         AND New.GrdScaleId IS NOT NULL
                                       )
                                    OR (
                                         New.GrdScaleId IS NULL
                                         AND Old.GrdScaleId IS NOT NULL
                                       ); 
                IF UPDATE(InstructorId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ClsSectionId
                                   ,'InstructorId'
                                   ,CONVERT(VARCHAR(8000),Old.InstructorId,121)
                                   ,CONVERT(VARCHAR(8000),New.InstructorId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ClsSectionId = New.ClsSectionId
                            WHERE   Old.InstructorId <> New.InstructorId
                                    OR (
                                         Old.InstructorId IS NULL
                                         AND New.InstructorId IS NOT NULL
                                       )
                                    OR (
                                         New.InstructorId IS NULL
                                         AND Old.InstructorId IS NOT NULL
                                       ); 
                IF UPDATE(ReqId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ClsSectionId
                                   ,'ReqId'
                                   ,CONVERT(VARCHAR(8000),Old.ReqId,121)
                                   ,CONVERT(VARCHAR(8000),New.ReqId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ClsSectionId = New.ClsSectionId
                            WHERE   Old.ReqId <> New.ReqId
                                    OR (
                                         Old.ReqId IS NULL
                                         AND New.ReqId IS NOT NULL
                                       )
                                    OR (
                                         New.ReqId IS NULL
                                         AND Old.ReqId IS NOT NULL
                                       ); 
                IF UPDATE(TermId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ClsSectionId
                                   ,'TermId'
                                   ,CONVERT(VARCHAR(8000),Old.TermId,121)
                                   ,CONVERT(VARCHAR(8000),New.TermId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ClsSectionId = New.ClsSectionId
                            WHERE   Old.TermId <> New.TermId
                                    OR (
                                         Old.TermId IS NULL
                                         AND New.TermId IS NOT NULL
                                       )
                                    OR (
                                         New.TermId IS NULL
                                         AND Old.TermId IS NOT NULL
                                       ); 
                IF UPDATE(InstrGrdBkWgtId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ClsSectionId
                                   ,'InstrGrdBkWgtId'
                                   ,CONVERT(VARCHAR(8000),Old.InstrGrdBkWgtId,121)
                                   ,CONVERT(VARCHAR(8000),New.InstrGrdBkWgtId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ClsSectionId = New.ClsSectionId
                            WHERE   Old.InstrGrdBkWgtId <> New.InstrGrdBkWgtId
                                    OR (
                                         Old.InstrGrdBkWgtId IS NULL
                                         AND New.InstrGrdBkWgtId IS NOT NULL
                                       )
                                    OR (
                                         New.InstrGrdBkWgtId IS NULL
                                         AND Old.InstrGrdBkWgtId IS NOT NULL
                                       ); 
                IF UPDATE(CampusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ClsSectionId
                                   ,'CampusId'
                                   ,CONVERT(VARCHAR(8000),Old.CampusId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ClsSectionId = New.ClsSectionId
                            WHERE   Old.CampusId <> New.CampusId
                                    OR (
                                         Old.CampusId IS NULL
                                         AND New.CampusId IS NOT NULL
                                       )
                                    OR (
                                         New.CampusId IS NULL
                                         AND Old.CampusId IS NOT NULL
                                       ); 
                IF UPDATE(StartDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ClsSectionId
                                   ,'StartDate'
                                   ,CONVERT(VARCHAR(8000),Old.StartDate,121)
                                   ,CONVERT(VARCHAR(8000),New.StartDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ClsSectionId = New.ClsSectionId
                            WHERE   Old.StartDate <> New.StartDate
                                    OR (
                                         Old.StartDate IS NULL
                                         AND New.StartDate IS NOT NULL
                                       )
                                    OR (
                                         New.StartDate IS NULL
                                         AND Old.StartDate IS NOT NULL
                                       ); 
                IF UPDATE(EndDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ClsSectionId
                                   ,'EndDate'
                                   ,CONVERT(VARCHAR(8000),Old.EndDate,121)
                                   ,CONVERT(VARCHAR(8000),New.EndDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ClsSectionId = New.ClsSectionId
                            WHERE   Old.EndDate <> New.EndDate
                                    OR (
                                         Old.EndDate IS NULL
                                         AND New.EndDate IS NOT NULL
                                       )
                                    OR (
                                         New.EndDate IS NULL
                                         AND Old.EndDate IS NOT NULL
                                       ); 
                IF UPDATE(ClsSection)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ClsSectionId
                                   ,'ClsSection'
                                   ,CONVERT(VARCHAR(8000),Old.ClsSection,121)
                                   ,CONVERT(VARCHAR(8000),New.ClsSection,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ClsSectionId = New.ClsSectionId
                            WHERE   Old.ClsSection <> New.ClsSection
                                    OR (
                                         Old.ClsSection IS NULL
                                         AND New.ClsSection IS NOT NULL
                                       )
                                    OR (
                                         New.ClsSection IS NULL
                                         AND Old.ClsSection IS NOT NULL
                                       ); 
                IF UPDATE(IsGraded)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ClsSectionId
                                   ,'IsGraded'
                                   ,CONVERT(VARCHAR(8000),Old.IsGraded,121)
                                   ,CONVERT(VARCHAR(8000),New.IsGraded,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ClsSectionId = New.ClsSectionId
                            WHERE   Old.IsGraded <> New.IsGraded
                                    OR (
                                         Old.IsGraded IS NULL
                                         AND New.IsGraded IS NOT NULL
                                       )
                                    OR (
                                         New.IsGraded IS NULL
                                         AND Old.IsGraded IS NOT NULL
                                       ); 
                IF UPDATE(ShiftId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ClsSectionId
                                   ,'ShiftId'
                                   ,CONVERT(VARCHAR(8000),Old.ShiftId,121)
                                   ,CONVERT(VARCHAR(8000),New.ShiftId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ClsSectionId = New.ClsSectionId
                            WHERE   Old.ShiftId <> New.ShiftId
                                    OR (
                                         Old.ShiftId IS NULL
                                         AND New.ShiftId IS NOT NULL
                                       )
                                    OR (
                                         New.ShiftId IS NULL
                                         AND Old.ShiftId IS NOT NULL
                                       ); 
            END; 
        END; 



    SET NOCOUNT OFF;

GO
ALTER TABLE [dbo].[arClassSections] ADD CONSTRAINT [PK_arClassSections_ClsSectionId] PRIMARY KEY CLUSTERED  ([ClsSectionId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_arClassSections_ClsSectionId_ReqId] ON [dbo].[arClassSections] ([ClsSectionId], [ReqId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_arClassSections_ClsSectionId_TermId_ReqId_GrdScaleId] ON [dbo].[arClassSections] ([ClsSectionId], [TermId], [ReqId], [GrdScaleId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_arClassSections_ReqId_TermId_ClsSectionId_InstrGrdBkWgtId_GrdScaleId] ON [dbo].[arClassSections] ([ReqId], [TermId], [ClsSectionId], [InstrGrdBkWgtId], [GrdScaleId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arClassSections] ADD CONSTRAINT [FK_arClassSections_adLeadGroups_LeadGrpId_LeadGrpId] FOREIGN KEY ([LeadGrpId]) REFERENCES [dbo].[adLeadGroups] ([LeadGrpId])
GO
ALTER TABLE [dbo].[arClassSections] ADD CONSTRAINT [FK_arClassSections_arGradeScales_GrdScaleId_GrdScaleId] FOREIGN KEY ([GrdScaleId]) REFERENCES [dbo].[arGradeScales] ([GrdScaleId])
GO
ALTER TABLE [dbo].[arClassSections] ADD CONSTRAINT [FK_arClassSections_arGrdBkWeights_InstrGrdBkWgtId_InstrGrdBkWgtId] FOREIGN KEY ([InstrGrdBkWgtId]) REFERENCES [dbo].[arGrdBkWeights] ([InstrGrdBkWgtId])
GO
ALTER TABLE [dbo].[arClassSections] ADD CONSTRAINT [FK_arClassSections_arProgVerDef_ProgramVersionDefinitionId_ProgVerDefId] FOREIGN KEY ([ProgramVersionDefinitionId]) REFERENCES [dbo].[arProgVerDef] ([ProgVerDefId])
GO
ALTER TABLE [dbo].[arClassSections] ADD CONSTRAINT [FK_arClassSections_arReqs_ReqId_ReqId] FOREIGN KEY ([ReqId]) REFERENCES [dbo].[arReqs] ([ReqId])
GO
ALTER TABLE [dbo].[arClassSections] ADD CONSTRAINT [FK_arClassSections_arShifts_shiftid_ShiftId] FOREIGN KEY ([ShiftId]) REFERENCES [dbo].[arShifts] ([ShiftId])
GO
ALTER TABLE [dbo].[arClassSections] ADD CONSTRAINT [FK_arClassSections_arTerm_TermId_TermId] FOREIGN KEY ([TermId]) REFERENCES [dbo].[arTerm] ([TermId])
GO
ALTER TABLE [dbo].[arClassSections] ADD CONSTRAINT [FK_arClassSections_syCampuses_CampusId_CampusId] FOREIGN KEY ([CampusId]) REFERENCES [dbo].[syCampuses] ([CampusId])
GO
ALTER TABLE [dbo].[arClassSections] ADD CONSTRAINT [FK_arClassSections_syUsers_InstructorId_UserId] FOREIGN KEY ([InstructorId]) REFERENCES [dbo].[syUsers] ([UserId])
GO
