CREATE TABLE [dbo].[arAttUnitType]
(
[UnitTypeId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arAttUnitType_UnitTypeId] DEFAULT (newid()),
[UnitTypeDescrip] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arAttUnitType] ADD CONSTRAINT [PK_arAttUnitType_UnitTypeId] PRIMARY KEY CLUSTERED  ([UnitTypeId]) ON [PRIMARY]
GO
