CREATE TABLE [dbo].[arGrdBkResults]
(
[GrdBkResultId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arGrdBkResults_GrdBkResultId] DEFAULT (newid()),
[ClsSectionId] [uniqueidentifier] NOT NULL,
[InstrGrdBkWgtDetailId] [uniqueidentifier] NOT NULL,
[Score] [decimal] (6, 2) NULL,
[Comments] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StuEnrollId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL,
[ResNum] [int] NOT NULL CONSTRAINT [DF_arGrdBkResults_ResNum] DEFAULT ((0)),
[PostDate] [smalldatetime] NULL,
[IsCompGraded] [bit] NULL CONSTRAINT [DF_arGrdBkResults_IsCompGraded] DEFAULT ((0)),
[isCourseCredited] [bit] NULL CONSTRAINT [DF_arGrdBkResults_isCourseCredited] DEFAULT ((0)),
[DateCompleted] [date] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
 
CREATE TRIGGER [dbo].[arGrdBkResults_Audit_Delete] ON [dbo].[arGrdBkResults]
    FOR DELETE
AS
    SET NOCOUNT ON;  
 
    DECLARE @AuditHistId AS UNIQUEIDENTIFIER;  
    DECLARE @EventRows AS INT;  
    DECLARE @EventDate AS DATETIME;  
    DECLARE @UserName AS VARCHAR(50);  
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     );  
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     );  
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    );  
    IF @EventRows > 0
        BEGIN  
            EXEC fmAuditHistAdd @AuditHistId,'arGrdBkResults','D',@EventRows,@EventDate,@UserName;  
            BEGIN  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue 
                        )
                        SELECT  @AuditHistId
                               ,Old.GrdBkResultId
                               ,'Score'
                               ,CONVERT(VARCHAR(8000),Old.Score,121)
                        FROM    Deleted Old;  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue 
                        )
                        SELECT  @AuditHistId
                               ,Old.GrdBkResultId
                               ,'ClsSectionId'
                               ,CONVERT(VARCHAR(8000),Old.ClsSectionId,121)
                        FROM    Deleted Old;  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue 
                        )
                        SELECT  @AuditHistId
                               ,Old.GrdBkResultId
                               ,'StuEnrollId'
                               ,CONVERT(VARCHAR(8000),Old.StuEnrollId,121)
                        FROM    Deleted Old;  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue 
                        )
                        SELECT  @AuditHistId
                               ,Old.GrdBkResultId
                               ,'InstrGrdBkWgtDetailId'
                               ,CONVERT(VARCHAR(8000),Old.InstrGrdBkWgtDetailId,121)
                        FROM    Deleted Old;  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue 
                        )
                        SELECT  @AuditHistId
                               ,Old.GrdBkResultId
                               ,'Comments'
                               ,CONVERT(VARCHAR(8000),Old.Comments,121)
                        FROM    Deleted Old;  
            END;  
        END;  
 
    SET NOCOUNT OFF;  
 
    SET NOCOUNT OFF;  
 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



---- note: back to old one
CREATE TRIGGER [dbo].[arGrdBkResults_Audit_Insert] ON [dbo].[arGrdBkResults]
    FOR INSERT
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arGrdBkResults','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.GrdBkResultId
                               ,'Score'
                               ,CONVERT(VARCHAR(8000),New.Score,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.GrdBkResultId
                               ,'ClsSectionId'
                               ,CONVERT(VARCHAR(8000),New.ClsSectionId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.GrdBkResultId
                               ,'StuEnrollId'
                               ,CONVERT(VARCHAR(8000),New.StuEnrollId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.GrdBkResultId
                               ,'InstrGrdBkWgtDetailId'
                               ,CONVERT(VARCHAR(8000),New.InstrGrdBkWgtDetailId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.GrdBkResultId
                               ,'Comments'
                               ,CONVERT(VARCHAR(8000),New.Comments,121)
                        FROM    Inserted New; 
            END; 
        END; 



    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



---- note: back to old one 
CREATE TRIGGER [dbo].[arGrdBkResults_Audit_Update] ON [dbo].[arGrdBkResults]
    FOR UPDATE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arGrdBkResults','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(Score)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.GrdBkResultId
                                   ,'Score'
                                   ,CONVERT(VARCHAR(8000),Old.Score,121)
                                   ,CONVERT(VARCHAR(8000),New.Score,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.GrdBkResultId = New.GrdBkResultId
                            WHERE   Old.Score <> New.Score
                                    OR (
                                         Old.Score IS NULL
                                         AND New.Score IS NOT NULL
                                       )
                                    OR (
                                         New.Score IS NULL
                                         AND Old.Score IS NOT NULL
                                       ); 
                IF UPDATE(ClsSectionId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.GrdBkResultId
                                   ,'ClsSectionId'
                                   ,CONVERT(VARCHAR(8000),Old.ClsSectionId,121)
                                   ,CONVERT(VARCHAR(8000),New.ClsSectionId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.GrdBkResultId = New.GrdBkResultId
                            WHERE   Old.ClsSectionId <> New.ClsSectionId
                                    OR (
                                         Old.ClsSectionId IS NULL
                                         AND New.ClsSectionId IS NOT NULL
                                       )
                                    OR (
                                         New.ClsSectionId IS NULL
                                         AND Old.ClsSectionId IS NOT NULL
                                       ); 
                IF UPDATE(StuEnrollId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.GrdBkResultId
                                   ,'StuEnrollId'
                                   ,CONVERT(VARCHAR(8000),Old.StuEnrollId,121)
                                   ,CONVERT(VARCHAR(8000),New.StuEnrollId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.GrdBkResultId = New.GrdBkResultId
                            WHERE   Old.StuEnrollId <> New.StuEnrollId
                                    OR (
                                         Old.StuEnrollId IS NULL
                                         AND New.StuEnrollId IS NOT NULL
                                       )
                                    OR (
                                         New.StuEnrollId IS NULL
                                         AND Old.StuEnrollId IS NOT NULL
                                       ); 
                IF UPDATE(InstrGrdBkWgtDetailId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.GrdBkResultId
                                   ,'InstrGrdBkWgtDetailId'
                                   ,CONVERT(VARCHAR(8000),Old.InstrGrdBkWgtDetailId,121)
                                   ,CONVERT(VARCHAR(8000),New.InstrGrdBkWgtDetailId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.GrdBkResultId = New.GrdBkResultId
                            WHERE   Old.InstrGrdBkWgtDetailId <> New.InstrGrdBkWgtDetailId
                                    OR (
                                         Old.InstrGrdBkWgtDetailId IS NULL
                                         AND New.InstrGrdBkWgtDetailId IS NOT NULL
                                       )
                                    OR (
                                         New.InstrGrdBkWgtDetailId IS NULL
                                         AND Old.InstrGrdBkWgtDetailId IS NOT NULL
                                       ); 
                IF UPDATE(Comments)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.GrdBkResultId
                                   ,'Comments'
                                   ,CONVERT(VARCHAR(8000),Old.Comments,121)
                                   ,CONVERT(VARCHAR(8000),New.Comments,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.GrdBkResultId = New.GrdBkResultId
                            WHERE   Old.Comments <> New.Comments
                                    OR (
                                         Old.Comments IS NULL
                                         AND New.Comments IS NOT NULL
                                       )
                                    OR (
                                         New.Comments IS NULL
                                         AND Old.Comments IS NOT NULL
                                       ); 
            END; 
        END; 



    SET NOCOUNT OFF;


GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
  
--==========================================================================================  
-- TRIGGER [dbo].[arGrdBkResults_Update] 
-- UPDATE  add GrdBkResultsHistory when update fields in arGrdBkResults 
--==========================================================================================  
CREATE TRIGGER [dbo].[arGrdBkResults_Update] ON [dbo].[arGrdBkResults]
    FOR UPDATE
AS
    SET NOCOUNT ON;  
 
    BEGIN  
        SET NOCOUNT ON;  
        DECLARE @EventDate AS DATETIME;  
        DECLARE @UserName AS VARCHAR(50);  

       
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );  
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );  

                IF UPDATE(Score)
                    BEGIN  
                        INSERT  INTO arGrdBkResultsHistory
                                
                                SELECT  NEWID() 
								       ,@UserName AS ModUserHistory
                                       ,@EventDate AS ModDateHistory
									   ,old.GrdBkResultId
									   ,old.Score
									   ,old.ModUser
									   ,old.ModDate
									   ,old.ResNum
									   ,old.PostDate
                                FROM    Deleted AS old
                                 
                    END; 


        SET NOCOUNT OFF;  
    END;  
--==========================================================================================  
-- END TRIGGER arGrdBkResults_Update 
--==========================================================================================  
  
 
    SET NOCOUNT OFF;  
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE TRIGGER [dbo].[TR_ClinicWork] ON [dbo].[arGrdBkResults]
    AFTER INSERT,UPDATE
AS
    SET NOCOUNT ON;

    DECLARE @PrgVerId UNIQUEIDENTIFIER
       ,@rownumber INT
       ,@StuEnrollId UNIQUEIDENTIFIER;
    DECLARE @PrevStuEnrollId UNIQUEIDENTIFIER
       ,@PrevReqId UNIQUEIDENTIFIER
       ,@PrevTermId UNIQUEIDENTIFIER
       ,@CreditsAttempted DECIMAL(18,2)
       ,@CreditsEarned DECIMAL(18,2)
       ,@TermId UNIQUEIDENTIFIER
       ,@TermDescrip VARCHAR(50);
    DECLARE @reqid UNIQUEIDENTIFIER
       ,@CourseCodeDescrip VARCHAR(50)
       ,@FinalGrade UNIQUEIDENTIFIER
       ,@FinalScore DECIMAL(18,2)
       ,@ClsSectionId UNIQUEIDENTIFIER
       ,@Grade VARCHAR(50)
       ,@IsGradeBookNotSatisified BIT
       ,@TermStartDate DATETIME;
    DECLARE @IsPass BIT
       ,@IsCreditsAttempted BIT
       ,@IsCreditsEarned BIT
       ,@Completed BIT
       ,@CurrentScore DECIMAL(18,2)
       ,@CurrentGrade VARCHAR(10)
       ,@FinalGradeDesc VARCHAR(50)
       ,@FinalGPA DECIMAL(18,2)
       ,@GrdBkResultId UNIQUEIDENTIFIER;
    DECLARE @Product_WeightedAverage_Credits_GPA DECIMAL(18,2)
       ,@Count_WeightedAverage_Credits DECIMAL(18,2)
       ,@Product_SimpleAverage_Credits_GPA DECIMAL(18,2)
       ,@Count_SimpleAverage_Credits DECIMAL(18,2);
    DECLARE @CreditsPerService DECIMAL(18,2)
       ,@NumberOfServicesAttempted INT
       ,@boolCourseHasLabWorkOrLabHours INT
       ,@sysComponentTypeId INT
       ,@RowCount INT;
    DECLARE @decGPALoop DECIMAL(18,2)
       ,@intCourseCount INT
       ,@decWeightedGPALoop DECIMAL(18,2)
       ,@IsInGPA BIT
       ,@isGradeEligibleForCreditsEarned BIT
       ,@isGradeEligibleForCreditsAttempted BIT;
    DECLARE @ComputedSimpleGPA DECIMAL(18,2)
       ,@ComputedWeightedGPA DECIMAL(18,2)
       ,@CourseCredits DECIMAL(18,2);
    DECLARE @FinAidCreditsEarned DECIMAL(18,2)
       ,@FinAidCredits DECIMAL(18,2)
       ,@TermAverage DECIMAL(18,2)
       ,@TermAverageCount INT;
    DECLARE @IsWeighted INT;
    SET @decGPALoop = 0;
    SET @intCourseCount = 0;
    SET @decWeightedGPALoop = 0;
    SET @ComputedSimpleGPA = 0;
    SET @ComputedWeightedGPA = 0;
    SET @CourseCredits = 0;
    DECLARE GetExternshipAttendance_Cursor CURSOR
    FOR
        SELECT	DISTINCT
                SE.StuEnrollId
               ,T.TermId
               ,T.TermDescrip
               ,T.StartDate
               ,R.ReqId
               ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
               ,NULL AS FinalScore
               ,NULL AS FinalGrade
               ,GCT.SysComponentTypeId
               ,R.Credits AS CreditsAttempted
               ,CS.ClsSectionId
               ,SE.PrgVerId
               ,R.FinAidCredits AS FinAidCredits
        FROM    arStuEnrollments SE
        INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
        INNER JOIN arGrdBkResults GBR ON SE.StuEnrollId = GBR.StuEnrollId
        INNER JOIN Inserted t4 ON GBR.GrdBkResultId = t4.GrdBkResultId
        INNER JOIN arClassSections CS ON GBR.ClsSectionId = CS.ClsSectionId
        INNER JOIN arTerm T ON CS.TermId = T.TermId
        INNER JOIN arReqs R ON CS.ReqId = R.ReqId
        LEFT JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
        LEFT JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
        WHERE   --SE.StuENrollId='68F828FB-735E-4F90-B774-5482A7D89142' AND CS.TermId='2038D5BB-AF3A-4918-82D6-911DA20E35E9' 
					--AND CS.ReqId='E1B286AB-F280-450F-A9F5-A8712763C3EE' AND 
                GCT.SysComponentTypeId = 503
        ORDER BY SE.StuEnrollId
               ,T.StartDate
               ,T.TermDescrip
               ,R.ReqId; 
				
				
    OPEN GetExternshipAttendance_Cursor;
    SET @PrevStuEnrollId = NULL;
    SET @PrevTermId = NULL;
    SET @PrevReqId = NULL;
    SET @RowCount = 0;
    FETCH NEXT FROM GetExternshipAttendance_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@TermStartDate,@reqid,@CourseCodeDescrip,@FinalScore,@FinalGrade,
        @sysComponentTypeId,@CreditsAttempted,@ClsSectionId,@PrgVerId,@FinAidCredits; 
    WHILE @@FETCH_STATUS = 0
        BEGIN
	
            DECLARE @ShowROSSOnlyTabsForStudent_Value BIT
               ,@BoolWorkUnitNotSatisfied BIT;
            SET @ShowROSSOnlyTabsForStudent_Value = 0;
            SET @BoolWorkUnitNotSatisfied = 0;
	
	
	-- If value of @BoolWorkUnitNotSatisfied is greater than or equal to 1 then there are work units not satisfied
            SET @BoolWorkUnitNotSatisfied = (
                                              SELECT    COUNT(*) AS UnsatisfiedWorkUnits
                                              FROM      (
                                                          SELECT    D.StuEnrollId
                                                                   ,D.TermId
                                                                   ,D.ReqId
                                                                   ,CASE WHEN D.MinimumScore > D.Score THEN ( D.MinimumScore - D.Score )
                                                                         ELSE 0
                                                                    END AS Remaining
                                                                   ,CASE WHEN ( D.MinimumScore > D.Score )
                                                                              AND ( D.MustPass = 1 ) THEN 0
                                                                         WHEN (
                                                                                SysComponentTypeId = 500
                                                                                OR SysComponentTypeId = 503
                                                                                OR SysComponentTypeId = 544
                                                                              )
                                                                              AND ( @ShowROSSOnlyTabsForStudent_Value = 0 )
                                                                              AND ( D.Score IS NOT NULL )
                                                                              AND ( D.MinimumScore > D.Score ) THEN 0
                                                                         WHEN @ShowROSSOnlyTabsForStudent_Value = 1
                                                                              AND D.Score IS NULL
                                                                              AND D.Required = 1 THEN 0
                                                                         WHEN D.Score IS NULL
                                                                              AND D.FinalScore IS NULL
                                                                              AND ( D.Required = 1 ) THEN 0
                                                                         ELSE 1
                                                                    END AS IsWorkUnitSatisfied
                                                          FROM      (
                                                                      SELECT	DISTINCT
                                                                                T.TermId
                                                                               ,T.TermDescrip
                                                                               ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                                                               ,R.ReqId
                                                                               ,RTRIM(GCT.Descrip) AS GradeBookDescription
                                                                               ,( CASE WHEN GCT.SysComponentTypeId IN ( 500,503,544 ) THEN GBWD.Number
                                                                                       ELSE (
                                                                                              SELECT    MIN(MinVal)
                                                                                              FROM      arGradeScaleDetails GSD
                                                                                                       ,arGradeSystemDetails GSS
                                                                                              WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                                                        AND GSS.IsPass = 1
                                                                                            )
                                                                                  END ) AS MinimumScore
                                                                               ,GBWD.Weight AS Weight
                                                                               ,NULL AS FinalScore
                                                                               ,NULL AS FinalGrade
                                                                               ,GBWD.Required
                                                                               ,GBWD.MustPass
                                                                               ,GBWD.GrdPolicyId
                                                                               ,( CASE GCT.SysComponentTypeId
                                                                                    WHEN 544 THEN (
                                                                                                    SELECT  SUM(HoursAttended)
                                                                                                    FROM    arExternshipAttendance
                                                                                                    WHERE   StuEnrollId = SE.StuEnrollId
                                                                                                  )
                                                                                    WHEN 503
                                                                                    THEN (
                                                                                           SELECT   SUM(Score)
                                                                                           FROM     arGrdBkResults
                                                                                           WHERE    StuEnrollId = SE.StuEnrollId
                                                                                                    AND ClsSectionId = CS.ClsSectionId
                                                                                                    AND InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                                                                         )
                                                                                    ELSE GBR.Score
                                                                                  END ) AS Score
                                                                               ,GCT.SysComponentTypeId
                                                                               ,SE.StuEnrollId
                                                                               ,R.Credits AS CreditsAttempted
                                                                               ,CS.ClsSectionId
                                                                      FROM      arStuEnrollments SE
                                                                      INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                                                      INNER JOIN arGrdBkResults GBR ON GBR.StuEnrollId = SE.StuEnrollId
                                                                      INNER JOIN Inserted t4 ON GBR.GrdBkResultId = t4.GrdBkResultId
                                                                      INNER JOIN arClassSections CS ON GBR.ClsSectionId = CS.ClsSectionId
                                                                      INNER JOIN arTerm T ON CS.TermId = T.TermId
                                                                      INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                                                                      LEFT JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                                                      LEFT JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                                      WHERE     SE.StuEnrollId = @StuEnrollId
                                                                                AND CS.TermId = @TermId
                                                                                AND CS.ReqId = @reqid
                                                                                AND GCT.SysComponentTypeId = 503
                                                                    ) D
                                                        ) E
                                              WHERE     IsWorkUnitSatisfied = 0
                                            );
														
		--PRINT '@BoolWorkUnitNotSatisfied='
		--PRINT @BoolWorkUnitNotSatisfied
		
            DECLARE @DoesCourseHaveOtherComponents INT; 											
            SET @DoesCourseHaveOtherComponents = (
                                                   SELECT   COUNT(*)
                                                   FROM     arStuEnrollments SE
                                                   INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                                   INNER JOIN arGrdBkResults GBR ON SE.StuEnrollId = GBR.StuEnrollId 
						--INNER join Inserted t4 on RES1.ExternshipAttendanceId = t4.ExternshipAttendanceId 
                                                   INNER JOIN arClassSections CS ON GBR.ClsSectionId = CS.ClsSectionId
                                                   INNER JOIN arTerm T ON CS.TermId = T.TermId
                                                   INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                                                   LEFT JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                                   LEFT JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                   WHERE    SE.StuEnrollId = @StuEnrollId
                                                            AND CS.TermId = @TermId
                                                            AND CS.ReqId = @reqid
                                                            AND GCT.SysComponentTypeId <> 503
                                                 );
		
		--PRINT '@DoesCourseHaveOtherComponents='
		--PRINT @DoesCourseHaveOtherComponents
		
		--if all lab work is satisfied
            IF @BoolWorkUnitNotSatisfied = 0
                BEGIN
				-- If there are no other components for this course, set completed to 1
                    IF ( @DoesCourseHaveOtherComponents = 0 )
                        BEGIN
                            UPDATE  syCreditSummary
                            SET     Completed = 1
                            WHERE   StuEnrollId = @StuEnrollId
                                    AND TermId = @TermId
                                    AND ReqId = @reqid;
                        END;
                END;
		
		-- If lab work is not satisfied set completed to zero (InComplete)
            IF @BoolWorkUnitNotSatisfied >= 1
                BEGIN
                    UPDATE  syCreditSummary
                    SET     Completed = 0
                    WHERE   StuEnrollId = @StuEnrollId
                            AND TermId = @TermId
                            AND ReqId = @reqid;
                END;
		

            FETCH NEXT FROM GetExternshipAttendance_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@TermStartDate,@reqid,@CourseCodeDescrip,@FinalScore,
                @FinalGrade,@sysComponentTypeId,@CreditsAttempted,@ClsSectionId,@PrgVerId,@FinAidCredits; 
        END;
    CLOSE GetExternshipAttendance_Cursor;
    DEALLOCATE GetExternshipAttendance_Cursor;




    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
CREATE TRIGGER [dbo].[Trigger_DeleteDuplicates] ON [dbo].[arGrdBkResults]
    FOR INSERT,UPDATE
AS
    SET NOCOUNT ON;  
 
    BEGIN 
        DECLARE @ClsSectionId UNIQUEIDENTIFIER
           ,@StuEnrollId UNIQUEIDENTIFIER; 
        DECLARE @newClsSectionId UNIQUEIDENTIFIER
           ,@newStuEnrollId UNIQUEIDENTIFIER; 
        DECLARE @newInstrGrdBkWgtDetailId UNIQUEIDENTIFIER; 
        DECLARE @ResultId UNIQUEIDENTIFIER
           ,@GrdSystemDetailId UNIQUEIDENTIFIER; 
        DECLARE @courseisclinic INT
           ,@reqid UNIQUEIDENTIFIER
           ,@counter INT; 
        DECLARE @MinVal DECIMAL(18,2); 
        DECLARE @MaxVal DECIMAL(18,2)
           ,@boolval INT; 
        DECLARE @score DECIMAL(18,2)
           ,@required DECIMAL(18,2)
           ,@studentscore DECIMAL(18,2); 
        DECLARE @graderounding VARCHAR(50); 
        SET @ClsSectionId = (
                              SELECT TOP 1
                                        ClsSectionId
                              FROM      Inserted
                            ); 
        --print @ClsSectionId 
        SET @StuEnrollId = (
                             SELECT TOP 1
                                    StuEnrollId
                             FROM   Inserted
                           ); 
        --print @StuEnrollId 
        SET @score = (
                       SELECT TOP 1
                                Score
                       FROM     Inserted
                     ); 
        --print 'score=' 
       -- Print @score 
        IF @score IS NOT NULL
            BEGIN 
                IF @score >= 0
                    BEGIN 
                        SET @reqid = (
                                       SELECT DISTINCT
                                                ReqId
                                       FROM     arClassSections
                                       WHERE    ClsSectionId = @ClsSectionId
                                     ); 
                        SET @courseisclinic = (
                                                SELECT DISTINCT
                                                        COUNT(GC.Descrip)
                                                FROM    arGrdBkWeights GBW
                                                       ,arGrdComponentTypes GC
                                                       ,arGrdBkWgtDetails GD
                                                WHERE   GBW.InstrGrdBkWgtId = GD.InstrGrdBkWgtId
                                                        AND GC.GrdComponentTypeId = GD.GrdComponentTypeId
                                                        AND GBW.ReqId = @reqid
                                                        AND GC.SysComponentTypeId IS NOT NULL
                                                        AND GC.SysComponentTypeId IN ( SELECT DISTINCT
                                                                                                ResourceID
                                                                                       FROM     syResources
                                                                                       WHERE    Resource IN ( 'Lab Work','Lab Hours','Practical Exams' ) )
                                              ); 
                        DECLARE @boolflag INT; 
                        SET @boolflag = 0; 
             
            -- if course has lab work or lab hours check if there is a combination 
            -- @courseacombination will be greater than 1 if there is a combination 
                        DECLARE @courseacombination INT; 
                        IF @courseisclinic >= 1
                            BEGIN 
                                SET @courseacombination = (
                                                            SELECT DISTINCT
                                                                    COUNT(GC.Descrip)
                                                            FROM    arGrdBkWeights GBW
                                                                   ,arGrdComponentTypes GC
                                                                   ,arGrdBkWgtDetails GD
                                                            WHERE   GBW.InstrGrdBkWgtId = GD.InstrGrdBkWgtId
                                                                    AND GC.GrdComponentTypeId = GD.GrdComponentTypeId
                                                                    AND GBW.ReqId = @reqid
                                                                    AND GC.SysComponentTypeId IS NOT NULL
                                                                    AND GC.SysComponentTypeId IN ( 499,501,502,544 )
                                                          ); 
                            END; 
             
            -- set the isClinicsSatisfied to 1, only if the course has lab work or lab hours 
            -- if its a combination, don't set isclinicssatisfied to 1  
                        IF @courseisclinic >= 1
                            AND @courseacombination = 0
                            BEGIN 
                                DECLARE GetMinMaxValueCursor2 CURSOR
                                FOR
                                    SELECT  Required
                                           ,SUM(TotalScore) AS totalscore
                                    FROM    (
                                              SELECT    ClsSectionId
                                                       ,t1.StuEnrollId
                                                       ,t1.InstrGrdBkWgtDetailId
                                                       ,(
                                                          SELECT    SUM(GBWD.Number) AS Required
                                                          FROM      arGrdComponentTypes GCT
                                                                   ,arGrdBkWgtDetails GBWD
                                                                   ,arGrdBkWeights GBW
                                                                   ,arClassSections CS
                                                                   ,arResults R
                                                          WHERE     GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                                    AND GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                                                                    AND GBW.ReqId = CS.ReqId
                                                                    AND CS.StartDate >= GBW.EffectiveDate
                                                                    AND GBWD.Number >= 1
                                                                    AND CS.ClsSectionId = R.TestId
                                                                    AND R.TestId = t1.ClsSectionId
                                                                    AND R.StuEnrollId = t1.StuEnrollId
                                                                    AND GBW.EffectiveDate = (
                                                                                              SELECT    MAX(arGrdBkWeights.EffectiveDate)
                                                                                              FROM      arGrdBkWeights
                                                                                              WHERE     ReqId = CS.ReqId
                                                                                            )
                                                        ) AS Required
                                                       ,t1.Score
                                                       ,SUM(Score) AS TotalScore
                                              FROM      arGrdBkResults t1
                                              WHERE     t1.Score IS NOT NULL
                                                        AND ClsSectionId = @ClsSectionId
                                                        AND StuEnrollId = @StuEnrollId
                                              GROUP BY  ClsSectionId
                                                       ,t1.StuEnrollId
                                                       ,t1.InstrGrdBkWgtDetailId
                                                       ,t1.Score
                                            ) R1
                                    GROUP BY Required; 
 
                                OPEN GetMinMaxValueCursor2; 
                                FETCH NEXT FROM GetMinMaxValueCursor2 INTO @required,@studentscore; 
                                WHILE @@FETCH_STATUS = 0
                                    BEGIN 
-- print 'required' 
-- print @required 
-- print '@studentscore' 
-- print @studentscore 
                                        IF ( @required IS NULL )
                                            BEGIN 
                                                SET @boolflag = 1; 
                                            END; 
                                        IF ( @studentscore < @required )
                                            BEGIN 
                                                SET @boolflag = 1; -- comp not satisfied 
-- print 'bool flg' 
-- print @boolflag 
                                            END; 
                         
                                        FETCH NEXT FROM GetMinMaxValueCursor2 INTO @required,@studentscore; 
                                    END; 
                                CLOSE GetMinMaxValueCursor2; 
                                DEALLOCATE GetMinMaxValueCursor2; 
-- print 'after dellocate bool flag' 
-- print @boolflag 
-- print 'stuenrollid=' 
-- print @StuEnrollId 
-- print 'TestId' 
-- print @ClsSectionId 
                                IF @boolflag = 0
                                    BEGIN 
                                        UPDATE  arResults
                                        SET     isClinicsSatisfied = 1
                                        WHERE   StuEnrollId = @StuEnrollId
                                                AND TestId = @ClsSectionId; 
                                    END; 
                            END; 
                    END; 
            END; 
    END; 
 
 
    SET NOCOUNT OFF;  
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[Trigger_UpdateProgressOfCourses]
ON [dbo].[arGrdBkResults]
FOR INSERT, UPDATE
AS
SET NOCOUNT ON;

    BEGIN
        DECLARE @Error AS INTEGER;

        SET @Error = 0;

        BEGIN TRANSACTION FixCoursesScore;
        BEGIN TRY

            DECLARE @CourseResultsToUpdate AS TABLE
                (
                    CourseScore DECIMAL(18, 2) NULL
                   ,NumberOfComponents INT NULL
                   ,NumberOfComponentsScored INT NULL
                   ,ClsSectionId UNIQUEIDENTIFIER NULL
                   ,StuEnrollId UNIQUEIDENTIFIER NULL
                   ,GrdSystemId UNIQUEIDENTIFIER NULL
                );

            DECLARE @FilteredCourses AS TABLE
                (
                    StuEnrollId UNIQUEIDENTIFIER NOT NULL
                   ,ClsSectionId VARCHAR(50) NOT NULL
                );

            DECLARE @CampusId UNIQUEIDENTIFIER = (
                                                 SELECT TOP ( 1 ) CampusId
                                                 FROM   dbo.arStuEnrollments
                                                 WHERE  StuEnrollId = (
                                                                      SELECT TOP 1 Inserted.StuEnrollId
                                                                      FROM   Inserted
                                                                      )
                                                 );

            DECLARE @GradeRounding BIT = ( CASE WHEN LOWER(ISNULL(dbo.GetAppSettingValueByKeyName('GradeRounding', @CampusId), '')) = 'yes' THEN 1
                                                ELSE 0
                                           END
                                         );

            INSERT INTO @FilteredCourses
                        SELECT DISTINCT StuEnrollId
                                       ,ClsSectionId
                        FROM   Inserted;

            INSERT INTO @CourseResultsToUpdate (
                                               CourseScore
                                              ,NumberOfComponents
                                              ,NumberOfComponentsScored
                                              ,ClsSectionId
                                              ,StuEnrollId
                                              ,GrdSystemId
                                               )
                        SELECT     CASE WHEN @GradeRounding = 1 THEN
                                            ROUND(dbo.CalculateStudentAverage(courses.StuEnrollId, NULL, NULL, courses.ClsSectionId, NULL, NULL), 2)
                                        ELSE dbo.CalculateStudentAverage(courses.StuEnrollId, NULL, NULL, courses.ClsSectionId, NULL, NULL)
                                   END AS CourseScore
                                  ,(
                                   SELECT COUNT(*)
                                   FROM   (
                                          SELECT   StuEnrollId
                                          FROM     dbo.arGrdBkResults
                                          WHERE    StuEnrollId = courses.StuEnrollId
                                                   AND ClsSectionId = courses.ClsSectionId
                                          GROUP BY StuEnrollId
                                                  ,ClsSectionId
                                                  ,InstrGrdBkWgtDetailId
                                          ) components
                                   ) AS NumberOfComponents
                                  ,(
                                   SELECT COUNT(*)
                                   FROM   (
                                          SELECT   StuEnrollId
                                          FROM     dbo.arGrdBkResults
                                          WHERE    StuEnrollId = courses.StuEnrollId
                                                   AND ClsSectionId = courses.ClsSectionId
                                                   AND Score IS NOT NULL
                                                   AND PostDate IS NOT NULL
                                          GROUP BY StuEnrollId
                                                  ,ClsSectionId
                                                  ,InstrGrdBkWgtDetailId
                                          ) components
                                   ) AS NumberOfComponentsScored
                                  ,courses.ClsSectionId
                                  ,courses.StuEnrollId
                                  ,gradeSystem.GrdSystemId
                        FROM       @FilteredCourses courses
                        JOIN       dbo.arStuEnrollments enrollments ON enrollments.StuEnrollId = courses.StuEnrollId
                        INNER JOIN dbo.arPrgVersions programVersion ON programVersion.PrgVerId = enrollments.PrgVerId
                        INNER JOIN dbo.arGradeSystems gradeSystem ON gradeSystem.GrdSystemId = programVersion.GrdSystemId
                        WHERE      programVersion.ProgramRegistrationType = 1;

            UPDATE     courses
            SET        courses.Score = coursesToUpdate.CourseScore
                      ,courses.IsInComplete = CASE WHEN coursesToUpdate.NumberOfComponents = coursesToUpdate.NumberOfComponentsScored THEN 0
                                                   ELSE 1
                                              END
                      ,courses.DateCompleted = CASE WHEN courses.DateCompleted IS NULL
                                                         AND coursesToUpdate.NumberOfComponents = coursesToUpdate.NumberOfComponentsScored THEN GETDATE()
                                                    ELSE courses.DateCompleted
                                               END
                      ,courses.ModDate = GETDATE()
                      ,courses.IsCourseCompleted = CASE WHEN coursesToUpdate.NumberOfComponents = coursesToUpdate.NumberOfComponentsScored THEN 1
                                                        ELSE 0
                                                   END
                      ,courses.GrdSysDetailId = (
                                                SELECT TOP ( 1 ) gradeScale.GrdSysDetailId
                                                FROM   dbo.arGradeSystemDetails gsrdSysDetail
                                                JOIN   dbo.arGradeScaleDetails gradeScale ON gradeScale.GrdSysDetailId = gsrdSysDetail.GrdSysDetailId
                                                WHERE  gsrdSysDetail.GrdSystemId = coursesToUpdate.GrdSystemId
                                                       AND gradeScale.MaxVal >= coursesToUpdate.CourseScore
                                                       AND gradeScale.MinVal <= coursesToUpdate.CourseScore
                                                )
            FROM       dbo.arResults courses
            INNER JOIN @CourseResultsToUpdate coursesToUpdate ON coursesToUpdate.ClsSectionId = courses.TestId
                                                                 AND coursesToUpdate.StuEnrollId = courses.StuEnrollId
            WHERE      courses.IsGradeOverridden = 0;

            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;

        END TRY
        BEGIN CATCH
            DECLARE @msg NVARCHAR(MAX);
            DECLARE @severity INT;
            DECLARE @state INT;
            SELECT @msg = ERROR_MESSAGE()
                  ,@severity = ERROR_SEVERITY()
                  ,@state = ERROR_STATE();
            RAISERROR(@msg, @severity, @state);
            SET @Error = 1;
        END CATCH;

        IF ( @Error = 0 )
            BEGIN
                COMMIT TRANSACTION FixCoursesScore;

            END;
        ELSE
            BEGIN
                ROLLBACK TRANSACTION FixCoursesScore;
            END;

    END;


SET NOCOUNT OFF;
GO
ALTER TABLE [dbo].[arGrdBkResults] ADD CONSTRAINT [PK_arGrdBkResults_GrdBkResultId] PRIMARY KEY CLUSTERED  ([GrdBkResultId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_arGrdBkResults_ClsSectionId_GrdBkResultId_InstrGrdBkWgtDetailId_StuEnrollId_Score] ON [dbo].[arGrdBkResults] ([ClsSectionId], [GrdBkResultId], [InstrGrdBkWgtDetailId], [StuEnrollId]) INCLUDE ([Score]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_arGrdBkResults_ClsSectionId_InstrGrdBkWgtDetailId] ON [dbo].[arGrdBkResults] ([ClsSectionId], [InstrGrdBkWgtDetailId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_arGrdBkResults_Score_ClsSectionId_InstrGrdBkWgtDetailId] ON [dbo].[arGrdBkResults] ([Score]) INCLUDE ([ClsSectionId], [InstrGrdBkWgtDetailId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_arGrdBkResults_StuEnrollId_ClsSectionId] ON [dbo].[arGrdBkResults] ([StuEnrollId], [ClsSectionId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_arGrdBkResults_StuEnrollId_InstrGrdBkWgtDetailId_ClsSectionId_ResNum_PostDate] ON [dbo].[arGrdBkResults] ([StuEnrollId], [InstrGrdBkWgtDetailId], [ClsSectionId], [ResNum], [PostDate]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arGrdBkResults] ADD CONSTRAINT [FK_arGrdBkResults_arClassSections_ClsSectionId_ClsSectionId] FOREIGN KEY ([ClsSectionId]) REFERENCES [dbo].[arClassSections] ([ClsSectionId])
GO
ALTER TABLE [dbo].[arGrdBkResults] ADD CONSTRAINT [FK_arGrdBkResults_arGrdBkWgtDetails_InstrGrdBkWgtDetailId_InstrGrdBkWgtDetailId] FOREIGN KEY ([InstrGrdBkWgtDetailId]) REFERENCES [dbo].[arGrdBkWgtDetails] ([InstrGrdBkWgtDetailId])
GO
ALTER TABLE [dbo].[arGrdBkResults] ADD CONSTRAINT [FK_arGrdBkResults_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY ([StuEnrollId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
