CREATE TABLE [dbo].[arQualMinTyps]
(
[QualMinTypId] [tinyint] NOT NULL,
[QualMinTypDesc] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arQualMinTyps] ADD CONSTRAINT [PK_arQualMinTyps_QualMinTypId] PRIMARY KEY CLUSTERED  ([QualMinTypId]) ON [PRIMARY]
GO
