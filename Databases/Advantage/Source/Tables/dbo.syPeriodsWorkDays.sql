CREATE TABLE [dbo].[syPeriodsWorkDays]
(
[PeriodIdWorkDayId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_syPeriodsWorkDays_PeriodIdWorkDayId] DEFAULT (newid()),
[PeriodId] [uniqueidentifier] NOT NULL,
[WorkDayId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syPeriodsWorkDays] ADD CONSTRAINT [PK_syPeriodsWorkDays_PeriodIdWorkDayId] PRIMARY KEY CLUSTERED  ([PeriodIdWorkDayId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syPeriodsWorkDays] ADD CONSTRAINT [FK_syPeriodsWorkDays_plWorkDays_WorkDayId_WorkDaysId] FOREIGN KEY ([WorkDayId]) REFERENCES [dbo].[plWorkDays] ([WorkDaysId])
GO
ALTER TABLE [dbo].[syPeriodsWorkDays] ADD CONSTRAINT [FK_syPeriodsWorkDays_syPeriods_PeriodId_PeriodId] FOREIGN KEY ([PeriodId]) REFERENCES [dbo].[syPeriods] ([PeriodId])
GO
