CREATE TABLE [dbo].[arCollegeDivisions]
(
[CollegeDivId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arCollegeDivisions_CollegeDivId] DEFAULT (newid()),
[CollegeDivCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CollegeDivDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CampGrpId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[arCollegeDivisions_Audit_Delete] ON [dbo].[arCollegeDivisions]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arCollegeDivisions','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CollegeDivId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CollegeDivId
                               ,'CollegeDivCode'
                               ,CONVERT(VARCHAR(8000),Old.CollegeDivCode,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CollegeDivId
                               ,'CollegeDivDescrip'
                               ,CONVERT(VARCHAR(8000),Old.CollegeDivDescrip,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[arCollegeDivisions_Audit_Insert] ON [dbo].[arCollegeDivisions]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arCollegeDivisions','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CollegeDivId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CollegeDivId
                               ,'CollegeDivCode'
                               ,CONVERT(VARCHAR(8000),New.CollegeDivCode,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CollegeDivId
                               ,'CollegeDivDescrip'
                               ,CONVERT(VARCHAR(8000),New.CollegeDivDescrip,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[arCollegeDivisions_Audit_Update] ON [dbo].[arCollegeDivisions]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arCollegeDivisions','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CollegeDivId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CollegeDivId = New.CollegeDivId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(CollegeDivCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CollegeDivId
                                   ,'CollegeDivCode'
                                   ,CONVERT(VARCHAR(8000),Old.CollegeDivCode,121)
                                   ,CONVERT(VARCHAR(8000),New.CollegeDivCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CollegeDivId = New.CollegeDivId
                            WHERE   Old.CollegeDivCode <> New.CollegeDivCode
                                    OR (
                                         Old.CollegeDivCode IS NULL
                                         AND New.CollegeDivCode IS NOT NULL
                                       )
                                    OR (
                                         New.CollegeDivCode IS NULL
                                         AND Old.CollegeDivCode IS NOT NULL
                                       ); 
                IF UPDATE(CollegeDivDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CollegeDivId
                                   ,'CollegeDivDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.CollegeDivDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.CollegeDivDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CollegeDivId = New.CollegeDivId
                            WHERE   Old.CollegeDivDescrip <> New.CollegeDivDescrip
                                    OR (
                                         Old.CollegeDivDescrip IS NULL
                                         AND New.CollegeDivDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.CollegeDivDescrip IS NULL
                                         AND Old.CollegeDivDescrip IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[arCollegeDivisions] ADD CONSTRAINT [PK_arCollegeDivisions_CollegeDivId] PRIMARY KEY CLUSTERED  ([CollegeDivId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_arCollegeDivisions_CollegeDivCode_CollegeDivDescrip] ON [dbo].[arCollegeDivisions] ([CollegeDivCode], [CollegeDivDescrip]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arCollegeDivisions] ADD CONSTRAINT [FK_arCollegeDivisions_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
