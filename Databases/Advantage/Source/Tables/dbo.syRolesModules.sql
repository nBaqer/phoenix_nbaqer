CREATE TABLE [dbo].[syRolesModules]
(
[RoleModuleId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syRolesModules_RoleModuleId] DEFAULT (newid()),
[RoleId] [uniqueidentifier] NULL,
[ModuleId] [smallint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syRolesModules] ADD CONSTRAINT [PK_syRolesModules_RoleModuleId] PRIMARY KEY CLUSTERED  ([RoleModuleId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syRolesModules] ADD CONSTRAINT [FK_syRolesModules_syResources_ModuleId_ResourceID] FOREIGN KEY ([ModuleId]) REFERENCES [dbo].[syResources] ([ResourceID])
GO
