CREATE TABLE [dbo].[rptAdmissionsRep]
(
[AdmissionsRepID] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_rptAdmissionsRep_AdmissionsRepID] DEFAULT (newid()),
[AdmissionsRepDescrip] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusId] [uniqueidentifier] NULL,
[CampusId] [uniqueidentifier] NULL,
[rptAdmissionsRepId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_rptAdmissionsRep_rptAdmissionsRepId] DEFAULT (newid())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[rptAdmissionsRep] ADD CONSTRAINT [PK_rptAdmissionsRep_rptAdmissionsRepId] PRIMARY KEY CLUSTERED  ([rptAdmissionsRepId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[rptAdmissionsRep] ADD CONSTRAINT [FK_rptAdmissionsRep_syUsers_AdmissionsRepID_UserId] FOREIGN KEY ([AdmissionsRepID]) REFERENCES [dbo].[syUsers] ([UserId])
GO
ALTER TABLE [dbo].[rptAdmissionsRep] ADD CONSTRAINT [FK_rptAdmissionsRep_syCampuses_CampusId_CampusId] FOREIGN KEY ([CampusId]) REFERENCES [dbo].[syCampuses] ([CampusId])
GO
ALTER TABLE [dbo].[rptAdmissionsRep] ADD CONSTRAINT [FK_rptAdmissionsRep_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
