CREATE TABLE [dbo].[ParamSection]
(
[SectionId] [bigint] NOT NULL IDENTITY(1, 1),
[SectionName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SectionCaption] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SetId] [bigint] NULL,
[SectionSeq] [int] NULL,
[SectionDescription] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SectionType] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ParamSection] ADD CONSTRAINT [PK_ParamSection_SectionId] PRIMARY KEY CLUSTERED  ([SectionId]) ON [PRIMARY]
GO
