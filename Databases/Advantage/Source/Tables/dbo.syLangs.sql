CREATE TABLE [dbo].[syLangs]
(
[LangId] [tinyint] NOT NULL,
[LangName] [varchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syLangs] ADD CONSTRAINT [PK_syLangs_LangId] PRIMARY KEY CLUSTERED  ([LangId]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[syLangs] TO [AdvantageRole]
GO
