CREATE TABLE [dbo].[syLeadStatusChanges]
(
[LeadStatusChangeId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syLeadStatusChanges_LeadStatusChangeId] DEFAULT (newid()),
[OrigStatusId] [uniqueidentifier] NOT NULL,
[NewStatusId] [uniqueidentifier] NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsReversal] [bit] NOT NULL CONSTRAINT [DF_syLeadStatusChanges_IsReversal] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[syLeadStatusChanges_Audit_Delete] ON [dbo].[syLeadStatusChanges]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syLeadStatusChanges','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadStatusChangeId
                               ,'OrigStatusId'
                               ,CONVERT(VARCHAR(8000),Old.OrigStatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadStatusChangeId
                               ,'NewStatusId'
                               ,CONVERT(VARCHAR(8000),Old.NewStatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadStatusChangeId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[syLeadStatusChanges_Audit_Insert] ON [dbo].[syLeadStatusChanges]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syLeadStatusChanges','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadStatusChangeId
                               ,'OrigStatusId'
                               ,CONVERT(VARCHAR(8000),New.OrigStatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadStatusChangeId
                               ,'NewStatusId'
                               ,CONVERT(VARCHAR(8000),New.NewStatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadStatusChangeId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[syLeadStatusChanges_Audit_Update] ON [dbo].[syLeadStatusChanges]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syLeadStatusChanges','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(OrigStatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.LeadStatusChangeId
                                   ,'OrigStatusId'
                                   ,CONVERT(VARCHAR(8000),Old.OrigStatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.OrigStatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.LeadStatusChangeId = New.LeadStatusChangeId
                            WHERE   Old.OrigStatusId <> New.OrigStatusId
                                    OR (
                                         Old.OrigStatusId IS NULL
                                         AND New.OrigStatusId IS NOT NULL
                                       )
                                    OR (
                                         New.OrigStatusId IS NULL
                                         AND Old.OrigStatusId IS NOT NULL
                                       ); 
                IF UPDATE(NewStatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.LeadStatusChangeId
                                   ,'NewStatusId'
                                   ,CONVERT(VARCHAR(8000),Old.NewStatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.NewStatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.LeadStatusChangeId = New.LeadStatusChangeId
                            WHERE   Old.NewStatusId <> New.NewStatusId
                                    OR (
                                         Old.NewStatusId IS NULL
                                         AND New.NewStatusId IS NOT NULL
                                       )
                                    OR (
                                         New.NewStatusId IS NULL
                                         AND Old.NewStatusId IS NOT NULL
                                       ); 
                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.LeadStatusChangeId
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.LeadStatusChangeId = New.LeadStatusChangeId
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO
ALTER TABLE [dbo].[syLeadStatusChanges] ADD CONSTRAINT [PK_syLeadStatusChanges_LeadStatusChangeId] PRIMARY KEY CLUSTERED  ([LeadStatusChangeId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_syLeadStatusChanges_CampGrpId_OrigStatusId_NewStatusId] ON [dbo].[syLeadStatusChanges] ([CampGrpId], [OrigStatusId], [NewStatusId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syLeadStatusChanges] ADD CONSTRAINT [FK_syLeadStatusChanges_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[syLeadStatusChanges] ADD CONSTRAINT [FK_syLeadStatusChanges_syStatusCodes_NewStatusId_StatusCodeId] FOREIGN KEY ([NewStatusId]) REFERENCES [dbo].[syStatusCodes] ([StatusCodeId])
GO
ALTER TABLE [dbo].[syLeadStatusChanges] ADD CONSTRAINT [FK_syLeadStatusChanges_syStatusCodes_OrigStatusId_StatusCodeId] FOREIGN KEY ([OrigStatusId]) REFERENCES [dbo].[syStatusCodes] ([StatusCodeId])
GO
