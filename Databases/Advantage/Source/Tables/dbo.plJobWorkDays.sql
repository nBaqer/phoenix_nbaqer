CREATE TABLE [dbo].[plJobWorkDays]
(
[JobWorkDaysId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_plJobWorkDays_JobWorkDaysId] DEFAULT (newid()),
[EmployerJobId] [uniqueidentifier] NULL,
[WorkDayId] [uniqueidentifier] NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[plJobWorkDays] ADD CONSTRAINT [PK_plJobWorkDays_JobWorkDaysId] PRIMARY KEY CLUSTERED  ([JobWorkDaysId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[plJobWorkDays] ADD CONSTRAINT [FK_plJobWorkDays_plEmployerJobs_EmployerJobId_EmployerJobId] FOREIGN KEY ([EmployerJobId]) REFERENCES [dbo].[plEmployerJobs] ([EmployerJobId])
GO
