CREATE TABLE [doc].[LeadImage]
(
[ImageId] [int] NOT NULL IDENTITY(1, 1),
[LeadId] [uniqueidentifier] NOT NULL,
[Image] [varbinary] (max) NOT NULL,
[ImgFile] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ImgSize] [int] NOT NULL,
[MediaType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TypeId] [int] NOT NULL,
[ModDate] [datetime] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [doc].[LeadImage] ADD CONSTRAINT [PK_LeadImage] PRIMARY KEY CLUSTERED  ([ImageId]) ON [PRIMARY]
GO
ALTER TABLE [doc].[LeadImage] ADD CONSTRAINT [FK_LeadImage_adLeads_LeadId_LeadId] FOREIGN KEY ([LeadId]) REFERENCES [dbo].[adLeads] ([LeadId])
GO
