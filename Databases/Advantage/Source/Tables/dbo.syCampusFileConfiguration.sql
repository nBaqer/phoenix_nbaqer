CREATE TABLE [dbo].[syCampusFileConfiguration]
(
[CampusFileConfigurationId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_syCampusFileConfiguration_CampusFileConfigurationId] DEFAULT (newid()),
[CampusGroupId] [uniqueidentifier] NOT NULL,
[FileStorageType] [int] NOT NULL,
[AppliesToAllFeatures] [bit] NOT NULL CONSTRAINT [DF_syCampusFileConfiguration_AppliesToAllFeatures] DEFAULT ((0)),
[UserName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Password] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Path] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CloudKey] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syCampusFileConfiguration] ADD CONSTRAINT [PK_syCampusFileConfiguration_CampusFileConfigurationId] PRIMARY KEY CLUSTERED  ([CampusFileConfigurationId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syCampusFileConfiguration] ADD CONSTRAINT [FK_syCampusFileConfiguration_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampusGroupId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
