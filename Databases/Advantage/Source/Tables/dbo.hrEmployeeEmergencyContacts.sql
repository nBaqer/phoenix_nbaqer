CREATE TABLE [dbo].[hrEmployeeEmergencyContacts]
(
[EmployeeEmergencyContactId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_hrEmployeeEmergencyContacts_EmployeeEmergencyContactId] DEFAULT (newid()),
[EmployeeEmergencyContactName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[EmpId] [uniqueidentifier] NOT NULL,
[WorkPhone] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HomePhone] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CellPhone] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Beeper] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WorkEmail] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HomeEmail] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[ForeignHomePhone] [bit] NOT NULL CONSTRAINT [DF_hrEmployeeEmergencyContacts_ForeignHomePhone] DEFAULT ((0)),
[ForeignWorkPhone] [bit] NOT NULL CONSTRAINT [DF_hrEmployeeEmergencyContacts_ForeignWorkPhone] DEFAULT ((0)),
[ForeignCellPhone] [bit] NOT NULL CONSTRAINT [DF_hrEmployeeEmergencyContacts_ForeignCellPhone] DEFAULT ((0)),
[RelationId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE TRIGGER [dbo].[hrEmployeeEmergencyContacts_Audit_Delete] ON [dbo].[hrEmployeeEmergencyContacts]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'hrEmployeeEmergencyContacts','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployeeEmergencyContactId
                               ,'RelationId'
                               ,CONVERT(VARCHAR(8000),Old.RelationId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployeeEmergencyContactId
                               ,'WorkPhone'
                               ,CONVERT(VARCHAR(8000),Old.WorkPhone,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployeeEmergencyContactId
                               ,'HomePhone'
                               ,CONVERT(VARCHAR(8000),Old.HomePhone,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployeeEmergencyContactId
                               ,'HomeEmail'
                               ,CONVERT(VARCHAR(8000),Old.HomeEmail,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployeeEmergencyContactId
                               ,'ForeignHomePhone'
                               ,CONVERT(VARCHAR(8000),Old.ForeignHomePhone,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployeeEmergencyContactId
                               ,'ForeignWorkPhone'
                               ,CONVERT(VARCHAR(8000),Old.ForeignWorkPhone,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployeeEmergencyContactId
                               ,'ForeignCellPhone'
                               ,CONVERT(VARCHAR(8000),Old.ForeignCellPhone,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployeeEmergencyContactId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployeeEmergencyContactId
                               ,'WorkEmail'
                               ,CONVERT(VARCHAR(8000),Old.WorkEmail,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployeeEmergencyContactId
                               ,'EmployeeEmergencyContactName'
                               ,CONVERT(VARCHAR(8000),Old.EmployeeEmergencyContactName,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployeeEmergencyContactId
                               ,'Beeper'
                               ,CONVERT(VARCHAR(8000),Old.Beeper,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployeeEmergencyContactId
                               ,'CellPhone'
                               ,CONVERT(VARCHAR(8000),Old.CellPhone,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployeeEmergencyContactId
                               ,'EmpId'
                               ,CONVERT(VARCHAR(8000),Old.EmpId,121)
                        FROM    Deleted Old; 
            END; 
        END;



    SET NOCOUNT OFF; 
GO
GO

GO

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[hrEmployeeEmergencyContacts_Audit_Insert] ON [dbo].[hrEmployeeEmergencyContacts]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'hrEmployeeEmergencyContacts','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployeeEmergencyContactId
                               ,'RelationId'
                               ,CONVERT(VARCHAR(8000),New.RelationId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployeeEmergencyContactId
                               ,'WorkPhone'
                               ,CONVERT(VARCHAR(8000),New.WorkPhone,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployeeEmergencyContactId
                               ,'HomePhone'
                               ,CONVERT(VARCHAR(8000),New.HomePhone,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployeeEmergencyContactId
                               ,'HomeEmail'
                               ,CONVERT(VARCHAR(8000),New.HomeEmail,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployeeEmergencyContactId
                               ,'ForeignHomePhone'
                               ,CONVERT(VARCHAR(8000),New.ForeignHomePhone,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployeeEmergencyContactId
                               ,'ForeignWorkPhone'
                               ,CONVERT(VARCHAR(8000),New.ForeignWorkPhone,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployeeEmergencyContactId
                               ,'ForeignCellPhone'
                               ,CONVERT(VARCHAR(8000),New.ForeignCellPhone,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployeeEmergencyContactId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployeeEmergencyContactId
                               ,'WorkEmail'
                               ,CONVERT(VARCHAR(8000),New.WorkEmail,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployeeEmergencyContactId
                               ,'EmployeeEmergencyContactName'
                               ,CONVERT(VARCHAR(8000),New.EmployeeEmergencyContactName,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployeeEmergencyContactId
                               ,'Beeper'
                               ,CONVERT(VARCHAR(8000),New.Beeper,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployeeEmergencyContactId
                               ,'CellPhone'
                               ,CONVERT(VARCHAR(8000),New.CellPhone,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployeeEmergencyContactId
                               ,'EmpId'
                               ,CONVERT(VARCHAR(8000),New.EmpId,121)
                        FROM    Inserted New; 
            END; 
        END;



    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[hrEmployeeEmergencyContacts_Audit_Update] ON [dbo].[hrEmployeeEmergencyContacts]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'hrEmployeeEmergencyContacts','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(RelationId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployeeEmergencyContactId
                                   ,'RelationId'
                                   ,CONVERT(VARCHAR(8000),Old.RelationId,121)
                                   ,CONVERT(VARCHAR(8000),New.RelationId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployeeEmergencyContactId = New.EmployeeEmergencyContactId
                            WHERE   Old.RelationId <> New.RelationId
                                    OR (
                                         Old.RelationId IS NULL
                                         AND New.RelationId IS NOT NULL
                                       )
                                    OR (
                                         New.RelationId IS NULL
                                         AND Old.RelationId IS NOT NULL
                                       ); 
                IF UPDATE(WorkPhone)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployeeEmergencyContactId
                                   ,'WorkPhone'
                                   ,CONVERT(VARCHAR(8000),Old.WorkPhone,121)
                                   ,CONVERT(VARCHAR(8000),New.WorkPhone,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployeeEmergencyContactId = New.EmployeeEmergencyContactId
                            WHERE   Old.WorkPhone <> New.WorkPhone
                                    OR (
                                         Old.WorkPhone IS NULL
                                         AND New.WorkPhone IS NOT NULL
                                       )
                                    OR (
                                         New.WorkPhone IS NULL
                                         AND Old.WorkPhone IS NOT NULL
                                       ); 
                IF UPDATE(HomePhone)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployeeEmergencyContactId
                                   ,'HomePhone'
                                   ,CONVERT(VARCHAR(8000),Old.HomePhone,121)
                                   ,CONVERT(VARCHAR(8000),New.HomePhone,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployeeEmergencyContactId = New.EmployeeEmergencyContactId
                            WHERE   Old.HomePhone <> New.HomePhone
                                    OR (
                                         Old.HomePhone IS NULL
                                         AND New.HomePhone IS NOT NULL
                                       )
                                    OR (
                                         New.HomePhone IS NULL
                                         AND Old.HomePhone IS NOT NULL
                                       ); 
                IF UPDATE(HomeEmail)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployeeEmergencyContactId
                                   ,'HomeEmail'
                                   ,CONVERT(VARCHAR(8000),Old.HomeEmail,121)
                                   ,CONVERT(VARCHAR(8000),New.HomeEmail,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployeeEmergencyContactId = New.EmployeeEmergencyContactId
                            WHERE   Old.HomeEmail <> New.HomeEmail
                                    OR (
                                         Old.HomeEmail IS NULL
                                         AND New.HomeEmail IS NOT NULL
                                       )
                                    OR (
                                         New.HomeEmail IS NULL
                                         AND Old.HomeEmail IS NOT NULL
                                       ); 
                IF UPDATE(ForeignHomePhone)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployeeEmergencyContactId
                                   ,'ForeignHomePhone'
                                   ,CONVERT(VARCHAR(8000),Old.ForeignHomePhone,121)
                                   ,CONVERT(VARCHAR(8000),New.ForeignHomePhone,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployeeEmergencyContactId = New.EmployeeEmergencyContactId
                            WHERE   Old.ForeignHomePhone <> New.ForeignHomePhone
                                    OR (
                                         Old.ForeignHomePhone IS NULL
                                         AND New.ForeignHomePhone IS NOT NULL
                                       )
                                    OR (
                                         New.ForeignHomePhone IS NULL
                                         AND Old.ForeignHomePhone IS NOT NULL
                                       ); 
                IF UPDATE(ForeignWorkPhone)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployeeEmergencyContactId
                                   ,'ForeignWorkPhone'
                                   ,CONVERT(VARCHAR(8000),Old.ForeignWorkPhone,121)
                                   ,CONVERT(VARCHAR(8000),New.ForeignWorkPhone,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployeeEmergencyContactId = New.EmployeeEmergencyContactId
                            WHERE   Old.ForeignWorkPhone <> New.ForeignWorkPhone
                                    OR (
                                         Old.ForeignWorkPhone IS NULL
                                         AND New.ForeignWorkPhone IS NOT NULL
                                       )
                                    OR (
                                         New.ForeignWorkPhone IS NULL
                                         AND Old.ForeignWorkPhone IS NOT NULL
                                       ); 
                IF UPDATE(ForeignCellPhone)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployeeEmergencyContactId
                                   ,'ForeignCellPhone'
                                   ,CONVERT(VARCHAR(8000),Old.ForeignCellPhone,121)
                                   ,CONVERT(VARCHAR(8000),New.ForeignCellPhone,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployeeEmergencyContactId = New.EmployeeEmergencyContactId
                            WHERE   Old.ForeignCellPhone <> New.ForeignCellPhone
                                    OR (
                                         Old.ForeignCellPhone IS NULL
                                         AND New.ForeignCellPhone IS NOT NULL
                                       )
                                    OR (
                                         New.ForeignCellPhone IS NULL
                                         AND Old.ForeignCellPhone IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployeeEmergencyContactId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployeeEmergencyContactId = New.EmployeeEmergencyContactId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(WorkEmail)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployeeEmergencyContactId
                                   ,'WorkEmail'
                                   ,CONVERT(VARCHAR(8000),Old.WorkEmail,121)
                                   ,CONVERT(VARCHAR(8000),New.WorkEmail,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployeeEmergencyContactId = New.EmployeeEmergencyContactId
                            WHERE   Old.WorkEmail <> New.WorkEmail
                                    OR (
                                         Old.WorkEmail IS NULL
                                         AND New.WorkEmail IS NOT NULL
                                       )
                                    OR (
                                         New.WorkEmail IS NULL
                                         AND Old.WorkEmail IS NOT NULL
                                       ); 
                IF UPDATE(EmployeeEmergencyContactName)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployeeEmergencyContactId
                                   ,'EmployeeEmergencyContactName'
                                   ,CONVERT(VARCHAR(8000),Old.EmployeeEmergencyContactName,121)
                                   ,CONVERT(VARCHAR(8000),New.EmployeeEmergencyContactName,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployeeEmergencyContactId = New.EmployeeEmergencyContactId
                            WHERE   Old.EmployeeEmergencyContactName <> New.EmployeeEmergencyContactName
                                    OR (
                                         Old.EmployeeEmergencyContactName IS NULL
                                         AND New.EmployeeEmergencyContactName IS NOT NULL
                                       )
                                    OR (
                                         New.EmployeeEmergencyContactName IS NULL
                                         AND Old.EmployeeEmergencyContactName IS NOT NULL
                                       ); 
                IF UPDATE(Beeper)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployeeEmergencyContactId
                                   ,'Beeper'
                                   ,CONVERT(VARCHAR(8000),Old.Beeper,121)
                                   ,CONVERT(VARCHAR(8000),New.Beeper,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployeeEmergencyContactId = New.EmployeeEmergencyContactId
                            WHERE   Old.Beeper <> New.Beeper
                                    OR (
                                         Old.Beeper IS NULL
                                         AND New.Beeper IS NOT NULL
                                       )
                                    OR (
                                         New.Beeper IS NULL
                                         AND Old.Beeper IS NOT NULL
                                       ); 
                IF UPDATE(CellPhone)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployeeEmergencyContactId
                                   ,'CellPhone'
                                   ,CONVERT(VARCHAR(8000),Old.CellPhone,121)
                                   ,CONVERT(VARCHAR(8000),New.CellPhone,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployeeEmergencyContactId = New.EmployeeEmergencyContactId
                            WHERE   Old.CellPhone <> New.CellPhone
                                    OR (
                                         Old.CellPhone IS NULL
                                         AND New.CellPhone IS NOT NULL
                                       )
                                    OR (
                                         New.CellPhone IS NULL
                                         AND Old.CellPhone IS NOT NULL
                                       ); 
                IF UPDATE(EmpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployeeEmergencyContactId
                                   ,'EmpId'
                                   ,CONVERT(VARCHAR(8000),Old.EmpId,121)
                                   ,CONVERT(VARCHAR(8000),New.EmpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployeeEmergencyContactId = New.EmployeeEmergencyContactId
                            WHERE   Old.EmpId <> New.EmpId
                                    OR (
                                         Old.EmpId IS NULL
                                         AND New.EmpId IS NOT NULL
                                       )
                                    OR (
                                         New.EmpId IS NULL
                                         AND Old.EmpId IS NOT NULL
                                       ); 
            END; 
        END;



    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[hrEmployeeEmergencyContacts] ADD CONSTRAINT [PK_hrEmployeeEmergencyContacts_EmployeeEmergencyContactId] PRIMARY KEY CLUSTERED  ([EmployeeEmergencyContactId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[hrEmployeeEmergencyContacts] ADD CONSTRAINT [FK_hrEmployeeEmergencyContacts_hrEmployees_EmpId_EmpId] FOREIGN KEY ([EmpId]) REFERENCES [dbo].[hrEmployees] ([EmpId])
GO
ALTER TABLE [dbo].[hrEmployeeEmergencyContacts] ADD CONSTRAINT [FK_hrEmployeeEmergencyContacts_syRelations_RelationId_RelationId] FOREIGN KEY ([RelationId]) REFERENCES [dbo].[syRelations] ([RelationId])
GO
ALTER TABLE [dbo].[hrEmployeeEmergencyContacts] ADD CONSTRAINT [FK_hrEmployeeEmergencyContacts_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
