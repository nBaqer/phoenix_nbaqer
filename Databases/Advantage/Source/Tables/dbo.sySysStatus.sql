CREATE TABLE [dbo].[sySysStatus]
(
[SysStatusId] [int] NOT NULL,
[SysStatusDescrip] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[StatusLevelId] [int] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[InSchool] [int] NULL,
[GEProgramStatus] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PostAcademics] [bit] NOT NULL CONSTRAINT [DF_sySysStatus_PostAcademics] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[sySysStatus] ADD CONSTRAINT [PK_sySysStatus_SysStatusId] PRIMARY KEY CLUSTERED  ([SysStatusId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[sySysStatus] ADD CONSTRAINT [FK_sySysStatus_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
ALTER TABLE [dbo].[sySysStatus] ADD CONSTRAINT [FK_sySysStatus_syStatusLevels_StatusLevelId_StatusLevelId] FOREIGN KEY ([StatusLevelId]) REFERENCES [dbo].[syStatusLevels] ([StatusLevelId])
GO
GRANT SELECT ON  [dbo].[sySysStatus] TO [AdvantageRole]
GO
