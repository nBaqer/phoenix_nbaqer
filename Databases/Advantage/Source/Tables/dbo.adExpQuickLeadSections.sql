CREATE TABLE [dbo].[adExpQuickLeadSections]
(
[SectionId] [int] NULL,
[FldId] [int] NULL,
[ExpId] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adExpQuickLeadSections] ADD CONSTRAINT [PK_adExpQuickLeadSections_ExpId] PRIMARY KEY CLUSTERED  ([ExpId]) ON [PRIMARY]
GO
