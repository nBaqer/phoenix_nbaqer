CREATE TABLE [dbo].[adReqGrpDef]
(
[ReqGrpDefId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_adReqGrpDef_ReqGrpDefId] DEFAULT (newid()),
[ReqGrpId] [uniqueidentifier] NULL,
[adReqId] [uniqueidentifier] NULL,
[Sequence] [int] NULL,
[IsRequired] [bit] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[LeadGrpId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adReqGrpDef] ADD CONSTRAINT [PK_adReqGrpDef_ReqGrpDefId] PRIMARY KEY CLUSTERED  ([ReqGrpDefId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adReqGrpDef] ADD CONSTRAINT [FK_adReqGrpDef_adLeadGroups_LeadGrpId_LeadGrpId] FOREIGN KEY ([LeadGrpId]) REFERENCES [dbo].[adLeadGroups] ([LeadGrpId])
GO
ALTER TABLE [dbo].[adReqGrpDef] ADD CONSTRAINT [FK_adReqGrpDef_adReqGroups_ReqGrpId_ReqGrpId] FOREIGN KEY ([ReqGrpId]) REFERENCES [dbo].[adReqGroups] ([ReqGrpId])
GO
ALTER TABLE [dbo].[adReqGrpDef] ADD CONSTRAINT [FK_adReqGrpDef_adReqs_adReqId_adReqId] FOREIGN KEY ([adReqId]) REFERENCES [dbo].[adReqs] ([adReqId])
GO
