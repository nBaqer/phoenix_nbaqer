CREATE TABLE [dbo].[arPostScoreDictationSpeedTest]
(
[id] [uniqueidentifier] NOT NULL,
[stuenrollid] [uniqueidentifier] NULL,
[termid] [uniqueidentifier] NULL,
[datepassed] [datetime] NULL,
[grdcomponenttypeid] [uniqueidentifier] NULL,
[accuracy] [decimal] (18, 0) NULL,
[speed] [decimal] (18, 0) NULL,
[GrdSysDetailId] [uniqueidentifier] NULL,
[InstructorId] [uniqueidentifier] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[istestmentorproctored] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arPostScoreDictationSpeedTest] ADD CONSTRAINT [PK_arPostScoreDictationSpeedTest_id] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
