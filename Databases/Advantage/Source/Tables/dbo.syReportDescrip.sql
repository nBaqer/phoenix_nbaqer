CREATE TABLE [dbo].[syReportDescrip]
(
[ReportId] [uniqueidentifier] NOT NULL,
[ResourceId] [int] NULL,
[ActualRptName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FriendlyRptName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RptDescrip] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReportDescriptionId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_archive_arGrdBkResults_ReportDescriptionId] DEFAULT (newsequentialid())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syReportDescrip] ADD CONSTRAINT [PK_syReportDescrip_ReportDescriptionId] PRIMARY KEY CLUSTERED  ([ReportDescriptionId]) ON [PRIMARY]
GO
