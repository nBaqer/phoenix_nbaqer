CREATE TABLE [dbo].[syInstitutionPhone]
(
[InstitutionPhoneId] [uniqueidentifier] NOT NULL,
[InstitutionId] [uniqueidentifier] NOT NULL,
[PhoneTypeId] [uniqueidentifier] NULL,
[Phone] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsForeignPhone] [bit] NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[IsDefault] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syInstitutionPhone] ADD CONSTRAINT [PK_syInstitutionPhone_InstitutionPhoneId] PRIMARY KEY CLUSTERED  ([InstitutionPhoneId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syInstitutionPhone] ADD CONSTRAINT [FK_syInstitutionPhone_syInstitutions_InstitutionId_HSID] FOREIGN KEY ([InstitutionId]) REFERENCES [dbo].[syInstitutions] ([HSId])
GO
ALTER TABLE [dbo].[syInstitutionPhone] ADD CONSTRAINT [FK_syInstitutionPhone_syPhoneType_PhoneTypeId_PhoneTypeId] FOREIGN KEY ([PhoneTypeId]) REFERENCES [dbo].[syPhoneType] ([PhoneTypeId])
GO
