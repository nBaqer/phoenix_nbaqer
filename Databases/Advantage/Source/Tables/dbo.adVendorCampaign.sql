CREATE TABLE [dbo].[adVendorCampaign]
(
[CampaignId] [int] NOT NULL IDENTITY(1, 1),
[VendorId] [int] NOT NULL,
[CampaignCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AccountId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsActive] [bit] NOT NULL CONSTRAINT [DF_adVendorCampaign_IsActive] DEFAULT ((1)),
[DateCampaignBegin] [datetime] NOT NULL,
[DateCampaignEnd] [datetime] NOT NULL,
[Cost] [float] NOT NULL CONSTRAINT [DF_adVendorCampaign_Cost] DEFAULT ((0)),
[PayForId] [int] NOT NULL,
[FilterRejectByCounty] [bit] NOT NULL CONSTRAINT [DF_adVendorCampaign_FilterRejectByCounty] DEFAULT ((0)),
[FilterRejectDuplicates] [bit] NOT NULL CONSTRAINT [DF_adVendorCampaign_FilterRejectDuplicates] DEFAULT ((0)),
[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_adVendorCampaign_IsDeleted] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adVendorCampaign] ADD CONSTRAINT [PK_adVendorCampaign_CampaignId] PRIMARY KEY CLUSTERED  ([CampaignId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adVendorCampaign] ADD CONSTRAINT [UIX_adVendorCampaign_CampaignCode_VendorId] UNIQUE NONCLUSTERED  ([CampaignCode], [VendorId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adVendorCampaign] ADD CONSTRAINT [UIX_adVendorCampaign_CampaignId] UNIQUE NONCLUSTERED  ([CampaignId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adVendorCampaign] ADD CONSTRAINT [FK_adVendorCampaign_adVendorPayFor_PayForId_IdPayFor] FOREIGN KEY ([PayForId]) REFERENCES [dbo].[adVendorPayFor] ([IdPayFor])
GO
ALTER TABLE [dbo].[adVendorCampaign] ADD CONSTRAINT [FK_adVendorCampaign_adVendors_VendorId_VendorId] FOREIGN KEY ([VendorId]) REFERENCES [dbo].[adVendors] ([VendorId])
GO
