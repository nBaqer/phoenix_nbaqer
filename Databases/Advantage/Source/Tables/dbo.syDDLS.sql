CREATE TABLE [dbo].[syDDLS]
(
[DDLId] [int] NOT NULL,
[DDLName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TblId] [int] NOT NULL,
[DispFldId] [int] NOT NULL,
[ValFldId] [int] NOT NULL,
[ResourceId] [int] NULL,
[CulDependent] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syDDLS] ADD CONSTRAINT [PK_syDDLS_DDLId] PRIMARY KEY CLUSTERED  ([DDLId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syDDLS] ADD CONSTRAINT [FK_syDDLS_syTables_TblId_TblId] FOREIGN KEY ([TblId]) REFERENCES [dbo].[syTables] ([TblId])
GO
GRANT SELECT ON  [dbo].[syDDLS] TO [AdvantageRole]
GO
