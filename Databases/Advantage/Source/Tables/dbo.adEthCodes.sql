CREATE TABLE [dbo].[adEthCodes]
(
[EthCodeId] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[EthCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[EthCodeDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[IPEDSSequence] [int] NULL,
[IPEDSValue] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adEthCodes] ADD CONSTRAINT [PK_adEthCodes_EthCodeId] PRIMARY KEY CLUSTERED  ([EthCodeId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_adEthCodes_EthCodeId_EthCodeDescrip] ON [dbo].[adEthCodes] ([EthCodeId], [EthCodeDescrip]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adEthCodes] ADD CONSTRAINT [FK_adEthCodes_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[adEthCodes] ADD CONSTRAINT [FK_adEthCodes_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
