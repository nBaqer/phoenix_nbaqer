CREATE TABLE [dbo].[arR2T4TerminationDetails]
(
[TerminationId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_arR2T4TerminationDetails_TerminationId] DEFAULT (newsequentialid()),
[StuEnrollmentId] [uniqueidentifier] NOT NULL,
[StatusCodeId] [uniqueidentifier] NULL,
[DropReasonId] [uniqueidentifier] NULL,
[DateWithdrawalDetermined] [datetime] NULL,
[LastDateAttended] [datetime] NULL,
[IsPerformingR2T4Calculator] [bit] NOT NULL CONSTRAINT [DF_arR2T4TerminationDetails_IsPerformR2T4Calc] DEFAULT ((0)),
[CalculationPeriodTypeId] [uniqueidentifier] NULL,
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_arR2T4TerminationDetails_CreatedDate] DEFAULT (getdate()),
[CreatedById] [uniqueidentifier] NOT NULL,
[UpdatedById] [uniqueidentifier] NULL,
[UpdatedDate] [datetime] NULL,
[IsR2T4ApproveTabEnabled] [bit] NOT NULL CONSTRAINT [DF_arR2T4TerminationDetails_IsR2T4ApproveTabEnabled] DEFAULT ((0)),
[IsTerminationReversed] [bit] NULL CONSTRAINT [DF__arR2T4Ter__IsTer__26EBF4DD] DEFAULT ((1))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arR2T4TerminationDetails] ADD CONSTRAINT [PK_arR2T4TerminationDetails_TerminationId] PRIMARY KEY CLUSTERED  ([TerminationId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arR2T4TerminationDetails] ADD CONSTRAINT [FK_arR2T4TerminationDetails_arDropReasons_DropReasonId_DropReasonId] FOREIGN KEY ([DropReasonId]) REFERENCES [dbo].[arDropReasons] ([DropReasonId])
GO
ALTER TABLE [dbo].[arR2T4TerminationDetails] ADD CONSTRAINT [FK_arR2T4TerminationDetails_arStuEnrollments_StuEnrollmentId_StuEnrollId] FOREIGN KEY ([StuEnrollmentId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
ALTER TABLE [dbo].[arR2T4TerminationDetails] ADD CONSTRAINT [FK_arR2T4TerminationDetails_syR2T4CalculationPeriodTypes_CalculationPeriodType_CalculationPeriodTypeId] FOREIGN KEY ([CalculationPeriodTypeId]) REFERENCES [dbo].[syR2T4CalculationPeriodTypes] ([CalculationPeriodTypeId])
GO
ALTER TABLE [dbo].[arR2T4TerminationDetails] ADD CONSTRAINT [FK_arR2T4TerminationDetails_syStatusCodes_StatusCodeId_StatusCodeId] FOREIGN KEY ([StatusCodeId]) REFERENCES [dbo].[syStatusCodes] ([StatusCodeId])
GO
ALTER TABLE [dbo].[arR2T4TerminationDetails] ADD CONSTRAINT [FK_arR2T4TerminationDetails_syUsers_CreatedBy_UserId] FOREIGN KEY ([CreatedById]) REFERENCES [dbo].[syUsers] ([UserId])
GO
ALTER TABLE [dbo].[arR2T4TerminationDetails] ADD CONSTRAINT [FK_arR2T4TerminationDetails_syUsers_UpdatedBy_UserId] FOREIGN KEY ([UpdatedById]) REFERENCES [dbo].[syUsers] ([UserId])
GO
