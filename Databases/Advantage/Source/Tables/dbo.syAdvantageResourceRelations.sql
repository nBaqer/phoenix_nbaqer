CREATE TABLE [dbo].[syAdvantageResourceRelations]
(
[ResRelId] [int] NOT NULL,
[ResourceId] [smallint] NULL,
[RelatedResourceId] [smallint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syAdvantageResourceRelations] ADD CONSTRAINT [PK_syAdvantageResourceRelations_ResRelId] PRIMARY KEY CLUSTERED  ([ResRelId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syAdvantageResourceRelations] ADD CONSTRAINT [FK_syAdvantageResourceRelations_syResources_RelatedResourceId_ResourceID] FOREIGN KEY ([RelatedResourceId]) REFERENCES [dbo].[syResources] ([ResourceID])
GO
ALTER TABLE [dbo].[syAdvantageResourceRelations] ADD CONSTRAINT [FK_syAdvantageResourceRelations_syResources_ResourceId_ResourceID] FOREIGN KEY ([ResourceId]) REFERENCES [dbo].[syResources] ([ResourceID])
GO
