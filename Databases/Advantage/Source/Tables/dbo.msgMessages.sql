CREATE TABLE [dbo].[msgMessages]
(
[MessageId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_msgMessages_MessageId] DEFAULT (newid()),
[TemplateId] [uniqueidentifier] NULL,
[FromId] [uniqueidentifier] NULL,
[DeliveryType] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RecipientType] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RecipientId] [uniqueidentifier] NULL,
[ReType] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReId] [uniqueidentifier] NULL,
[ChangeId] [uniqueidentifier] NULL,
[MsgContent] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDate] [datetime] NULL,
[DeliveryDate] [datetime] NULL,
[LastDeliveryAttempt] [datetime] NULL,
[LastDeliveryMsg] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateDelivered] [datetime] NULL,
[ModDate] [datetime] NULL,
[ModUser] [uniqueidentifier] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[msgMessages] ADD CONSTRAINT [PK_msgMessages_MessageId] PRIMARY KEY CLUSTERED  ([MessageId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[msgMessages] ADD CONSTRAINT [FK_msgMessages_msgTemplates_TemplateId_TemplateId] FOREIGN KEY ([TemplateId]) REFERENCES [dbo].[msgTemplates] ([TemplateId])
GO
