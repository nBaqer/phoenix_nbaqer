CREATE TABLE [dbo].[arGrdBkConversionResults]
(
[ConversionResultId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arGrdBkConversionResults_ConversionResultId] DEFAULT (newid()),
[ReqId] [uniqueidentifier] NOT NULL,
[GrdComponentTypeId] [uniqueidentifier] NOT NULL,
[Score] [decimal] (18, 2) NULL,
[Comments] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StuEnrollId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL,
[ResNum] [int] NOT NULL,
[PostDate] [smalldatetime] NULL,
[PostUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MinResult] [int] NULL,
[TermId] [uniqueidentifier] NULL,
[Required] [bit] NULL,
[MustPass] [bit] NULL,
[isCourseCredited] [bit] NULL CONSTRAINT [DF_arGrdBkConversionResults_isCourseCredited] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arGrdBkConversionResults] ADD CONSTRAINT [PK_arGrdBkConversionResults_ConversionResultId] PRIMARY KEY CLUSTERED  ([ConversionResultId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_arGrdBkConversionResults_StuEnrollId_ReqId_TermId_GrdComponentTypeId_ConversionResultId_MinResult_Score] ON [dbo].[arGrdBkConversionResults] ([StuEnrollId], [ReqId], [TermId], [GrdComponentTypeId], [ConversionResultId]) INCLUDE ([MinResult], [Score]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arGrdBkConversionResults] ADD CONSTRAINT [FK_arGrdBkConversionResults_arGrdBkConversionResults_ConversionResultId_ConversionResultId] FOREIGN KEY ([ConversionResultId]) REFERENCES [dbo].[arGrdBkConversionResults] ([ConversionResultId])
GO
