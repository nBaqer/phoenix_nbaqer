CREATE TABLE [dbo].[syUserQuestions]
(
[UserQuestionId] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[UserQuestionDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syUserQuestions] ADD CONSTRAINT [PK_syUserQuestions_UserQuestionId] PRIMARY KEY CLUSTERED  ([UserQuestionId]) ON [PRIMARY]
GO
