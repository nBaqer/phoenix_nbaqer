CREATE TABLE [dbo].[syRptSQL]
(
[RptSQLId] [smallint] NOT NULL,
[SQLStmt] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WhereClause] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OrderByClause] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syRptSQL] ADD CONSTRAINT [PK_syRptSQL_RptSQLId] PRIMARY KEY CLUSTERED  ([RptSQLId]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[syRptSQL] TO [AdvantageRole]
GO
