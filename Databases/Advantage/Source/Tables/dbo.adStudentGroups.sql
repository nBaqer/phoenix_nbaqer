CREATE TABLE [dbo].[adStudentGroups]
(
[StuGrpId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_adStuGrpTypes_StuGrpId] DEFAULT (newsequentialid()),
[code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[OwnerId] [uniqueidentifier] NOT NULL,
[TypeId] [uniqueidentifier] NOT NULL,
[ModDate] [datetime] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsPublic] [bit] NOT NULL CONSTRAINT [DF_adStudentGroups_IsPublic] DEFAULT ((0)),
[IsRegHold] [bit] NOT NULL CONSTRAINT [DF_adStudentGroups_IsRegHold] DEFAULT ((0)),
[IsTransHold] [bit] NOT NULL CONSTRAINT [DF_adStudentGroups_IsTransHold] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adStudentGroups] ADD CONSTRAINT [PK_adStudentGroups_StuGrpId] PRIMARY KEY CLUSTERED  ([StuGrpId]) ON [PRIMARY]
GO
