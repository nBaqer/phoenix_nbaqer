CREATE TABLE [dbo].[arGrdTyps]
(
[GrdTypId] [tinyint] NOT NULL,
[GrdTypDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arGrdTyps] ADD CONSTRAINT [PK_arGrdTyps_GrdTypId] PRIMARY KEY CLUSTERED  ([GrdTypId]) ON [PRIMARY]
GO
