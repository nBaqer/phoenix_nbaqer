CREATE TABLE [dbo].[faStudentAwardSchedule]
(
[AwardScheduleId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_faStudentAwardSchedule_AwardScheduleId] DEFAULT (newid()),
[StudentAwardId] [uniqueidentifier] NOT NULL,
[ExpectedDate] [datetime] NULL,
[Amount] [decimal] (19, 4) NULL,
[Reference] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_faStudentAwardSchedule_ModUser] DEFAULT ('sa'),
[ModDate] [datetime] NULL,
[TransactionId] [uniqueidentifier] NULL,
[TermNumber] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[recid] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[isProcessed] [bit] NULL CONSTRAINT [DF_faStudentAwardSchedule_isProcessed] DEFAULT ((0)),
[DisbursementNumber] [tinyint] NULL,
[SequenceNumber] [tinyint] NULL CONSTRAINT [DF_faStudentAwardSchedule_SequenceNumber] DEFAULT ((1)),
[DisbursementStatus] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GrossAmount] [decimal] (19, 4) NULL,
[LoanFees] [decimal] (19, 4) NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE TRIGGER [dbo].[faStudentAwardSchedule_Audit_Delete] ON [dbo].[faStudentAwardSchedule]
    FOR DELETE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 

    DECLARE @stuEnrollId AS UNIQUEIDENTIFIER;

    DECLARE @StudentAwardId AS UNIQUEIDENTIFIER; 

    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 

    SET @StudentAwardId = (
                            SELECT TOP 1
                                    StudentAwardId
                            FROM    Deleted
                          );

    SET @stuEnrollId = (
                         SELECT TOP 1
                                StuEnrollId
                         FROM   faStudentAwards
                         WHERE  StudentAwardId = @StudentAwardId
                       );

    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'faStudentAwardSchedule','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.AwardScheduleId
                               ,'Amount'
                               ,CONVERT(VARCHAR(8000),Old.Amount,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.AwardScheduleId
                               ,'StudentAwardId'
                               ,CONVERT(VARCHAR(8000),Old.StudentAwardId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.AwardScheduleId
                               ,'ExpectedDate'
                               ,CONVERT(VARCHAR(8000),Old.ExpectedDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.AwardScheduleId
                               ,'Reference'
                               ,CONVERT(VARCHAR(8000),Old.Reference,121)
                        FROM    Deleted Old; 
            END; 
--EXEC dbo.USP_AR_StuEnrollPayPeriods @stuEnrollId
        END; 



    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE TRIGGER [dbo].[faStudentAwardSchedule_Audit_Insert] ON [dbo].[faStudentAwardSchedule]
    FOR INSERT
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 

    DECLARE @stuEnrollId AS UNIQUEIDENTIFIER;

    DECLARE @StudentAwardId AS UNIQUEIDENTIFIER; 
 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 

    SET @StudentAwardId = (
                            SELECT TOP 1
                                    StudentAwardId
                            FROM    Inserted
                          );

    SET @stuEnrollId = (
                         SELECT TOP 1
                                StuEnrollId
                         FROM   faStudentAwards
                         WHERE  StudentAwardId = @StudentAwardId
                       );

    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'faStudentAwardSchedule','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.AwardScheduleId
                               ,'Amount'
                               ,CONVERT(VARCHAR(8000),New.Amount,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.AwardScheduleId
                               ,'StudentAwardId'
                               ,CONVERT(VARCHAR(8000),New.StudentAwardId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.AwardScheduleId
                               ,'ExpectedDate'
                               ,CONVERT(VARCHAR(8000),New.ExpectedDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.AwardScheduleId
                               ,'Reference'
                               ,CONVERT(VARCHAR(8000),New.Reference,121)
                        FROM    Inserted New; 
            END; 
--EXEC dbo.USP_AR_StuEnrollPayPeriods @stuEnrollId
        END; 



    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE TRIGGER [dbo].[faStudentAwardSchedule_Audit_Update] ON [dbo].[faStudentAwardSchedule]
    FOR UPDATE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 

    DECLARE @stuEnrollId AS UNIQUEIDENTIFIER;

    DECLARE @StudentAwardId AS UNIQUEIDENTIFIER; 
 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 

    SET @StudentAwardId = (
                            SELECT TOP 1
                                    StudentAwardId
                            FROM    Inserted
                          );

    SET @stuEnrollId = (
                         SELECT TOP 1
                                StuEnrollId
                         FROM   faStudentAwards
                         WHERE  StudentAwardId = @StudentAwardId
                       );
 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'faStudentAwardSchedule','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(Amount)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.AwardScheduleId
                                   ,'Amount'
                                   ,CONVERT(VARCHAR(8000),Old.Amount,121)
                                   ,CONVERT(VARCHAR(8000),New.Amount,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.AwardScheduleId = New.AwardScheduleId
                            WHERE   Old.Amount <> New.Amount
                                    OR (
                                         Old.Amount IS NULL
                                         AND New.Amount IS NOT NULL
                                       )
                                    OR (
                                         New.Amount IS NULL
                                         AND Old.Amount IS NOT NULL
                                       ); 
                IF UPDATE(StudentAwardId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.AwardScheduleId
                                   ,'StudentAwardId'
                                   ,CONVERT(VARCHAR(8000),Old.StudentAwardId,121)
                                   ,CONVERT(VARCHAR(8000),New.StudentAwardId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.AwardScheduleId = New.AwardScheduleId
                            WHERE   Old.StudentAwardId <> New.StudentAwardId
                                    OR (
                                         Old.StudentAwardId IS NULL
                                         AND New.StudentAwardId IS NOT NULL
                                       )
                                    OR (
                                         New.StudentAwardId IS NULL
                                         AND Old.StudentAwardId IS NOT NULL
                                       ); 
                IF UPDATE(ExpectedDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.AwardScheduleId
                                   ,'ExpectedDate'
                                   ,CONVERT(VARCHAR(8000),Old.ExpectedDate,121)
                                   ,CONVERT(VARCHAR(8000),New.ExpectedDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.AwardScheduleId = New.AwardScheduleId
                            WHERE   Old.ExpectedDate <> New.ExpectedDate
                                    OR (
                                         Old.ExpectedDate IS NULL
                                         AND New.ExpectedDate IS NOT NULL
                                       )
                                    OR (
                                         New.ExpectedDate IS NULL
                                         AND Old.ExpectedDate IS NOT NULL
                                       ); 
                IF UPDATE(Reference)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.AwardScheduleId
                                   ,'Reference'
                                   ,CONVERT(VARCHAR(8000),Old.Reference,121)
                                   ,CONVERT(VARCHAR(8000),New.Reference,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.AwardScheduleId = New.AwardScheduleId
                            WHERE   Old.Reference <> New.Reference
                                    OR (
                                         Old.Reference IS NULL
                                         AND New.Reference IS NOT NULL
                                       )
                                    OR (
                                         New.Reference IS NULL
                                         AND Old.Reference IS NOT NULL
                                       ); 
            END; 
--EXEC dbo.USP_AR_StuEnrollPayPeriods @stuEnrollId
        END; 



    SET NOCOUNT OFF;

GO
ALTER TABLE [dbo].[faStudentAwardSchedule] ADD CONSTRAINT [PK_faStudentAwardSchedule_AwardScheduleId] PRIMARY KEY CLUSTERED  ([AwardScheduleId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_faStudentAwardSchedule_AwardScheduleId] ON [dbo].[faStudentAwardSchedule] ([AwardScheduleId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_faStudentAwardSchedule_StudentAwardId] ON [dbo].[faStudentAwardSchedule] ([StudentAwardId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[faStudentAwardSchedule] ADD CONSTRAINT [FK_faStudentAwardSchedule_faStudentAwards_StudentAwardId_StudentAwardId] FOREIGN KEY ([StudentAwardId]) REFERENCES [dbo].[faStudentAwards] ([StudentAwardId])
GO
