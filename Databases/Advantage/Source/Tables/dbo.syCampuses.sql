CREATE TABLE [dbo].[syCampuses]
(
[CampusId] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[CampCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampDescrip] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Address1] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateId] [uniqueidentifier] NULL,
[Zip] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CountryId] [uniqueidentifier] NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone1] [char] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone2] [char] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone3] [char] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fax] [char] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Website] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsCorporate] [bit] NULL CONSTRAINT [DF_syCampuses_IsCorporate] DEFAULT ((0)),
[TranscriptAuthZnTitle] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TranscriptAuthZnName] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TCSourcePath] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TCTargetPath] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsRemoteServer] [bit] NULL,
[PortalContactEmail] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RemoteServerUsrNm] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RemoteServerPwd] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceFolderLoc] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TargetFolderLoc] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UseCampusAddress] [bit] NULL,
[InvAddress1] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvAddress2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvCity] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvStateID] [uniqueidentifier] NULL,
[InvZip] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvCountryID] [uniqueidentifier] NULL,
[InvPhone1] [char] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InvFax] [char] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FLSourcePath] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FLTargetPath] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FLExceptionPath] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsRemoteServerFL] [bit] NOT NULL CONSTRAINT [DF_syCampuses_IsRemoteServerFL] DEFAULT ((0)),
[RemoteServerUsrNmFL] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RemoteServerPwdFL] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceFolderLocFL] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TargetFolderLocFL] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ILSourcePath] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ILArchivePath] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ILExceptionPath] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsRemoteServerIL] [bit] NOT NULL CONSTRAINT [DF_syCampuses_IsRemoteServerIL] DEFAULT ((0)),
[RemoteServerUserNameIL] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RemoteServerPasswordIL] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceCategoryAll] [bit] NULL,
[SourceCategoryId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SourceTypeAll] [bit] NULL,
[SourceTypeId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdmRepAll] [bit] NULL,
[AdmRepId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeadStatusAll] [bit] NULL,
[LeadStatusId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneType1All] [bit] NULL,
[PhoneType1Id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneType2All] [bit] NULL,
[PhoneType2Id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddTypeAll] [bit] NULL,
[AddTypeId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GenderAll] [bit] NULL,
[GenderId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PortalCountryAll] [bit] NULL,
[PortalCountryId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PortalContactEmailAll] [bit] NULL,
[EmailSubjectAll] [bit] NULL,
[EmailSubject] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmailBodyAll] [bit] NULL,
[EmailBody] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OPEID] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SchoolName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Token1098TService] [varchar] (38) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SchoolCodeKissSchoolId] [varchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CmsId] [nvarchar] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FSEOGMatchType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsBranch] [bit] NOT NULL CONSTRAINT [DF_SyCampuses_IsBranch] DEFAULT ((0)),
[ParentCampusId] [uniqueidentifier] NULL,
[AllowGraduateAndStillOweMoney] [bit] NOT NULL CONSTRAINT [DF_syCampuses_AllowGraduateAndStillOweMoney] DEFAULT ((0)),
[AllowGraduateWithoutFullCompletion] [bit] NOT NULL CONSTRAINT [DF_syCampuses_AllowGraduateWithoutFullCompletion] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[syCampuses_Audit_Delete]
ON [dbo].[syCampuses]
FOR DELETE
AS
SET NOCOUNT ON;

DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
DECLARE @EventRows AS INT;
DECLARE @EventDate AS DATETIME;
DECLARE @UserName AS VARCHAR(50);
SET @AuditHistId = NEWID();
SET @EventRows = (
                 SELECT COUNT(*)
                 FROM   Deleted
                 );
SET @EventDate = (
                 SELECT TOP 1 ModDate
                 FROM   Deleted
                 );
SET @UserName = (
                SELECT TOP 1 ModUser
                FROM   Deleted
                );
IF @EventRows > 0
    BEGIN
        EXEC fmAuditHistAdd @AuditHistId
                           ,'syCampuses'
                           ,'D'
                           ,@EventRows
                           ,@EventDate
                           ,@UserName;
        BEGIN
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,'CampCode'
                              ,CONVERT(VARCHAR(8000), Old.CampCode, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,'CampDescrip'
                              ,CONVERT(VARCHAR(8000), Old.CampDescrip, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,'Address1'
                              ,CONVERT(VARCHAR(8000), Old.Address1, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,'Address2'
                              ,CONVERT(VARCHAR(8000), Old.Address2, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,'City'
                              ,CONVERT(VARCHAR(8000), Old.City, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,'StateId'
                              ,CONVERT(VARCHAR(8000), Old.StateId, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,'Zip'
                              ,CONVERT(VARCHAR(8000), Old.Zip, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,'CountryId'
                              ,CONVERT(VARCHAR(8000), Old.CountryId, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,'StatusId'
                              ,CONVERT(VARCHAR(8000), Old.StatusId, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,'Phone1'
                              ,CONVERT(VARCHAR(8000), Old.Phone1, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,'Phone2'
                              ,CONVERT(VARCHAR(8000), Old.Phone2, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,'Phone3'
                              ,CONVERT(VARCHAR(8000), Old.Phone3, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,'Fax'
                              ,CONVERT(VARCHAR(8000), Old.Fax, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,'Email'
                              ,CONVERT(VARCHAR(8000), Old.Email, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,'Website'
                              ,CONVERT(VARCHAR(8000), Old.Website, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,'IsCorporate'
                              ,CONVERT(VARCHAR(8000), Old.IsCorporate, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,'IsCorporate'
                              ,CONVERT(VARCHAR(8000), Old.AllowGraduateAndStillOweMoney, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,'IsCorporate'
                              ,CONVERT(VARCHAR(8000), Old.AllowGraduateWithoutFullCompletion, 121)
                        FROM   Deleted Old;
        END;
    END;



SET NOCOUNT OFF;


GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE TRIGGER [dbo].[syCampuses_Audit_Insert]
ON [dbo].[syCampuses]
FOR INSERT
AS
SET NOCOUNT ON;

DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
DECLARE @EventRows AS INT;
DECLARE @EventDate AS DATETIME;
DECLARE @UserName AS VARCHAR(50);
SET @AuditHistId = NEWID();
SET @EventRows = (
                 SELECT COUNT(*)
                 FROM   Inserted
                 );
SET @EventDate = (
                 SELECT TOP 1 ModDate
                 FROM   Inserted
                 );
SET @UserName = (
                SELECT TOP 1 ModUser
                FROM   Inserted
                );
IF @EventRows > 0
    BEGIN
        EXEC fmAuditHistAdd @AuditHistId
                           ,'syCampuses'
                           ,'I'
                           ,@EventRows
                           ,@EventDate
                           ,@UserName;
        BEGIN
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,'CampCode'
                              ,CONVERT(VARCHAR(8000), New.CampCode, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,'CampDescrip'
                              ,CONVERT(VARCHAR(8000), New.CampDescrip, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,'Address1'
                              ,CONVERT(VARCHAR(8000), New.Address1, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,'Address2'
                              ,CONVERT(VARCHAR(8000), New.Address2, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,'City'
                              ,CONVERT(VARCHAR(8000), New.City, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,'StateId'
                              ,CONVERT(VARCHAR(8000), New.StateId, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,'Zip'
                              ,CONVERT(VARCHAR(8000), New.Zip, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,'CountryId'
                              ,CONVERT(VARCHAR(8000), New.CountryId, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,'StatusId'
                              ,CONVERT(VARCHAR(8000), New.StatusId, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,'Phone1'
                              ,CONVERT(VARCHAR(8000), New.Phone1, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,'Phone2'
                              ,CONVERT(VARCHAR(8000), New.Phone2, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,'Phone3'
                              ,CONVERT(VARCHAR(8000), New.Phone3, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,'Fax'
                              ,CONVERT(VARCHAR(8000), New.Fax, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,'Email'
                              ,CONVERT(VARCHAR(8000), New.Email, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,'Website'
                              ,CONVERT(VARCHAR(8000), New.Website, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,'IsCorporate'
                              ,CONVERT(VARCHAR(8000), New.IsCorporate, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,'IsCorporate'
                              ,CONVERT(VARCHAR(8000), New.AllowGraduateAndStillOweMoney, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,'IsCorporate'
                              ,CONVERT(VARCHAR(8000), New.AllowGraduateWithoutFullCompletion, 121)
                        FROM   Inserted New;
        END;
    END;



SET NOCOUNT OFF;


GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[syCampuses_Audit_Update]
ON [dbo].[syCampuses]
FOR UPDATE
AS
SET NOCOUNT ON;

DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
DECLARE @EventRows AS INT;
DECLARE @EventDate AS DATETIME;
DECLARE @UserName AS VARCHAR(50);
SET @AuditHistId = NEWID();
SET @EventRows = (
                 SELECT COUNT(*)
                 FROM   Inserted
                 );
SET @EventDate = (
                 SELECT TOP 1 ModDate
                 FROM   Inserted
                 );
SET @UserName = (
                SELECT TOP 1 ModUser
                FROM   Inserted
                );
IF @EventRows > 0
    BEGIN
        EXEC fmAuditHistAdd @AuditHistId
                           ,'syCampuses'
                           ,'U'
                           ,@EventRows
                           ,@EventDate
                           ,@UserName;
        BEGIN
            IF UPDATE(CampCode)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,'CampCode'
                                           ,CONVERT(VARCHAR(8000), Old.CampCode, 121)
                                           ,CONVERT(VARCHAR(8000), New.CampCode, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.CampCode <> New.CampCode
                                            OR (
                                               Old.CampCode IS NULL
                                               AND New.CampCode IS NOT NULL
                                               )
                                            OR (
                                               New.CampCode IS NULL
                                               AND Old.CampCode IS NOT NULL
                                               );
            IF UPDATE(CampDescrip)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,'CampDescrip'
                                           ,CONVERT(VARCHAR(8000), Old.CampDescrip, 121)
                                           ,CONVERT(VARCHAR(8000), New.CampDescrip, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.CampDescrip <> New.CampDescrip
                                            OR (
                                               Old.CampDescrip IS NULL
                                               AND New.CampDescrip IS NOT NULL
                                               )
                                            OR (
                                               New.CampDescrip IS NULL
                                               AND Old.CampDescrip IS NOT NULL
                                               );
            IF UPDATE(Address1)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,'Address1'
                                           ,CONVERT(VARCHAR(8000), Old.Address1, 121)
                                           ,CONVERT(VARCHAR(8000), New.Address1, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.Address1 <> New.Address1
                                            OR (
                                               Old.Address1 IS NULL
                                               AND New.Address1 IS NOT NULL
                                               )
                                            OR (
                                               New.Address1 IS NULL
                                               AND Old.Address1 IS NOT NULL
                                               );
            IF UPDATE(Address2)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,'Address2'
                                           ,CONVERT(VARCHAR(8000), Old.Address2, 121)
                                           ,CONVERT(VARCHAR(8000), New.Address2, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.Address2 <> New.Address2
                                            OR (
                                               Old.Address2 IS NULL
                                               AND New.Address2 IS NOT NULL
                                               )
                                            OR (
                                               New.Address2 IS NULL
                                               AND Old.Address2 IS NOT NULL
                                               );
            IF UPDATE(City)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,'City'
                                           ,CONVERT(VARCHAR(8000), Old.City, 121)
                                           ,CONVERT(VARCHAR(8000), New.City, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.City <> New.City
                                            OR (
                                               Old.City IS NULL
                                               AND New.City IS NOT NULL
                                               )
                                            OR (
                                               New.City IS NULL
                                               AND Old.City IS NOT NULL
                                               );
            IF UPDATE(StateId)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,'StateId'
                                           ,CONVERT(VARCHAR(8000), Old.StateId, 121)
                                           ,CONVERT(VARCHAR(8000), New.StateId, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.StateId <> New.StateId
                                            OR (
                                               Old.StateId IS NULL
                                               AND New.StateId IS NOT NULL
                                               )
                                            OR (
                                               New.StateId IS NULL
                                               AND Old.StateId IS NOT NULL
                                               );
            IF UPDATE(Zip)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,'Zip'
                                           ,CONVERT(VARCHAR(8000), Old.Zip, 121)
                                           ,CONVERT(VARCHAR(8000), New.Zip, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.Zip <> New.Zip
                                            OR (
                                               Old.Zip IS NULL
                                               AND New.Zip IS NOT NULL
                                               )
                                            OR (
                                               New.Zip IS NULL
                                               AND Old.Zip IS NOT NULL
                                               );
            IF UPDATE(CountryId)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,'CountryId'
                                           ,CONVERT(VARCHAR(8000), Old.CountryId, 121)
                                           ,CONVERT(VARCHAR(8000), New.CountryId, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.CountryId <> New.CountryId
                                            OR (
                                               Old.CountryId IS NULL
                                               AND New.CountryId IS NOT NULL
                                               )
                                            OR (
                                               New.CountryId IS NULL
                                               AND Old.CountryId IS NOT NULL
                                               );
            IF UPDATE(StatusId)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,'StatusId'
                                           ,CONVERT(VARCHAR(8000), Old.StatusId, 121)
                                           ,CONVERT(VARCHAR(8000), New.StatusId, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.StatusId <> New.StatusId
                                            OR (
                                               Old.StatusId IS NULL
                                               AND New.StatusId IS NOT NULL
                                               )
                                            OR (
                                               New.StatusId IS NULL
                                               AND Old.StatusId IS NOT NULL
                                               );
            IF UPDATE(Phone1)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,'Phone1'
                                           ,CONVERT(VARCHAR(8000), Old.Phone1, 121)
                                           ,CONVERT(VARCHAR(8000), New.Phone1, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.Phone1 <> New.Phone1
                                            OR (
                                               Old.Phone1 IS NULL
                                               AND New.Phone1 IS NOT NULL
                                               )
                                            OR (
                                               New.Phone1 IS NULL
                                               AND Old.Phone1 IS NOT NULL
                                               );
            IF UPDATE(Phone2)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,'Phone2'
                                           ,CONVERT(VARCHAR(8000), Old.Phone2, 121)
                                           ,CONVERT(VARCHAR(8000), New.Phone2, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.Phone2 <> New.Phone2
                                            OR (
                                               Old.Phone2 IS NULL
                                               AND New.Phone2 IS NOT NULL
                                               )
                                            OR (
                                               New.Phone2 IS NULL
                                               AND Old.Phone2 IS NOT NULL
                                               );
            IF UPDATE(Phone3)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,'Phone3'
                                           ,CONVERT(VARCHAR(8000), Old.Phone3, 121)
                                           ,CONVERT(VARCHAR(8000), New.Phone3, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.Phone3 <> New.Phone3
                                            OR (
                                               Old.Phone3 IS NULL
                                               AND New.Phone3 IS NOT NULL
                                               )
                                            OR (
                                               New.Phone3 IS NULL
                                               AND Old.Phone3 IS NOT NULL
                                               );
            IF UPDATE(Fax)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,'Fax'
                                           ,CONVERT(VARCHAR(8000), Old.Fax, 121)
                                           ,CONVERT(VARCHAR(8000), New.Fax, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.Fax <> New.Fax
                                            OR (
                                               Old.Fax IS NULL
                                               AND New.Fax IS NOT NULL
                                               )
                                            OR (
                                               New.Fax IS NULL
                                               AND Old.Fax IS NOT NULL
                                               );
            IF UPDATE(Email)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,'Email'
                                           ,CONVERT(VARCHAR(8000), Old.Email, 121)
                                           ,CONVERT(VARCHAR(8000), New.Email, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.Email <> New.Email
                                            OR (
                                               Old.Email IS NULL
                                               AND New.Email IS NOT NULL
                                               )
                                            OR (
                                               New.Email IS NULL
                                               AND Old.Email IS NOT NULL
                                               );
            IF UPDATE(Website)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,'Website'
                                           ,CONVERT(VARCHAR(8000), Old.Website, 121)
                                           ,CONVERT(VARCHAR(8000), New.Website, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.Website <> New.Website
                                            OR (
                                               Old.Website IS NULL
                                               AND New.Website IS NOT NULL
                                               )
                                            OR (
                                               New.Website IS NULL
                                               AND Old.Website IS NOT NULL
                                               );
            IF UPDATE(IsCorporate)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,'IsCorporate'
                                           ,CONVERT(VARCHAR(8000), Old.IsCorporate, 121)
                                           ,CONVERT(VARCHAR(8000), New.IsCorporate, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.IsCorporate <> New.IsCorporate
                                            OR (
                                               Old.IsCorporate IS NULL
                                               AND New.IsCorporate IS NOT NULL
                                               )
                                            OR (
                                               New.IsCorporate IS NULL
                                               AND Old.IsCorporate IS NOT NULL
                                               );

            IF UPDATE(AllowGraduateWithoutFullCompletion)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,'IsCorporate'
                                           ,CONVERT(VARCHAR(8000), Old.AllowGraduateWithoutFullCompletion, 121)
                                           ,CONVERT(VARCHAR(8000), New.AllowGraduateWithoutFullCompletion, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.AllowGraduateWithoutFullCompletion <> New.AllowGraduateWithoutFullCompletion
                                            OR (
                                               Old.AllowGraduateWithoutFullCompletion IS NULL
                                               AND New.AllowGraduateWithoutFullCompletion IS NOT NULL
                                               )
                                            OR (
                                               New.AllowGraduateWithoutFullCompletion IS NULL
                                               AND Old.AllowGraduateWithoutFullCompletion IS NOT NULL
                                               );

            IF UPDATE(AllowGraduateAndStillOweMoney)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,'IsCorporate'
                                           ,CONVERT(VARCHAR(8000), Old.AllowGraduateAndStillOweMoney, 121)
                                           ,CONVERT(VARCHAR(8000), New.AllowGraduateAndStillOweMoney, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.AllowGraduateAndStillOweMoney <> New.AllowGraduateAndStillOweMoney
                                            OR (
                                               Old.AllowGraduateAndStillOweMoney IS NULL
                                               AND New.AllowGraduateAndStillOweMoney IS NOT NULL
                                               )
                                            OR (
                                               New.AllowGraduateAndStillOweMoney IS NULL
                                               AND Old.AllowGraduateAndStillOweMoney IS NOT NULL
                                               );
        END;
    END;



SET NOCOUNT OFF;


GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--==========================================================================================
-- TRIGGER syCampuses_KlassApps_Delete
-- AFTER DELETE syCampuses, marks syKlassAppConfigurationSetting record as deleted for Klass Apps
--==========================================================================================
CREATE TRIGGER [dbo].[syCampuses_KlassApps_Delete] ON [dbo].[syCampuses]
    AFTER DELETE
	-- KlassApps
	-- locations  is kte   --> Campus is label 
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @EventRows AS INT; 

        UPDATE  SKACS
        SET     SKACS.ItemStatus = 'D'
               ,SKACS.ModDate = GETDATE() -- D.ModDate
               ,SKACS.ModUser = 'Support' -- D.ModUser
        FROM    syKlassAppConfigurationSetting AS SKACS
        INNER JOIN syKlassOperationType AS SKOT ON SKOT.KlassOperationTypeId = SKACS.KlassOperationTypeId
        INNER JOIN Deleted AS D ON D.CampusId = SKACS.AdvantageId
        WHERE   SKOT.Code = 'locations';

        SET NOCOUNT OFF; 
    END;
--==========================================================================================
-- END  --  TRIGGER syCampuses_KlassApsp_Delete
--==========================================================================================
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


--==========================================================================================
-- TRIGGER syCampuses_KlassApps_Insert
-- AFTER INSERT syCampuses, insert syKlassAppConfigurationSetting record for Klass Apps
--==========================================================================================
CREATE TRIGGER [dbo].[syCampuses_KlassApps_Insert] ON [dbo].[syCampuses]
    AFTER INSERT
	-- KlassApps
	-- locations  is kte   --> Campus is label 
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @EventRows AS INT; 
        DECLARE @KlassOperationTypeId AS INTEGER;
        SELECT  @KlassOperationTypeId = SKOT.KlassOperationTypeId
        FROM    syKlassOperationType AS SKOT
        WHERE   SKOT.Code = 'locations';

        INSERT  INTO syKlassAppConfigurationSetting
                (
                 AdvantageId
                ,KlassAppId
                ,KlassEntityId
                ,KlassOperationTypeId
                ,IsCustomField
                ,IsActive
                ,ItemStatus
                ,ItemValue
                ,ItemLabel
                ,ItemValueType
                ,CreationDate
                ,ModDate
                ,ModUser
                ,LastOperationLog
		        )
                SELECT  I.CampusId -- AdvantageId - varchar(38)
                       ,0    -- KlassAppId - int
                       ,1    -- KlassEntityId - int
                       ,@KlassOperationTypeId  -- KlassOperationTypeId - int
                       ,0    -- IsCustomField - bit
                       ,1    -- IsActive - bit
                       ,'I'  -- ItemStatus - char(1)
                       ,I.CampDescrip  -- ItemValue - varchar(200)
                       ,'Campus'  -- ItemLabel - varchar(50)
                       ,'String'  -- ItemValueType - varchar(50)
                       ,I.ModDate -- CreationDate - datetime
                       ,I.ModDate -- ModDate - datetime
                       ,I.ModUser -- ModUser - varchar(50)
                       ,''  -- LastOperationLog - varchar(1000)
                FROM    Inserted AS I;




        SET NOCOUNT OFF; 
    END;
--==========================================================================================
-- END  --  TRIGGER syCampuses_KlassApps_Insert
--==========================================================================================

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--==========================================================================================
-- TRIGGER syCampuses_KlassApps_Update
-- AFTER UPDATE  syCampuses, syKlassAppConfigurationSetting when change fields for Klass Apps
--==========================================================================================
CREATE TRIGGER [dbo].[syCampuses_KlassApps_Update] ON [dbo].[syCampuses]
    AFTER UPDATE
	--   DB syCampuses  --   MAP        --  Klass Apps               locations   
	-- CampusId                                                     AdvantageId
    -- CampDescrip      -- Description  --> name	
	-- Address1			-- Address1		--> address
	-- Phone1			-- Phone1		--> phone
	--                  --              --> location_type
	--					-- Email		--> contact_email
	--                  --              --> contact_person
	-- Phone1			-- Phone1		--> contact_phone
	--                  --              --> contact_position
	--                  --              --> deleted
	--                  --              --> referrals_email_address
	--                  --              --> created_at
	--                  -- ModDate      --> updated_at
	--                  --              --> payments_on
	--                  --              --> 

	-- KlassApps
	-- locations  is kte   --> Campus is label 
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @EventRows AS INT; 

        UPDATE  SKACS
        SET     SKACS.ItemStatus = 'U'
               ,SKACS.ModDate = I.ModDate
               ,SKACS.ModUser = I.ModUser
        FROM    syKlassAppConfigurationSetting AS SKACS
        INNER JOIN syKlassOperationType AS SKOT ON SKOT.KlassOperationTypeId = SKACS.KlassOperationTypeId
        INNER JOIN Inserted AS I ON I.CampusId = SKACS.AdvantageId
        WHERE   SKOT.Code = 'locations'
                AND SKACS.ItemStatus <> 'I'
                AND (
                      UPDATE(Address1)
                      OR UPDATE(Email)
                      OR UPDATE(CampDescrip)
                      OR UPDATE(Phone1)
                    );

        SET NOCOUNT OFF; 
    END;
--==========================================================================================
-- END  --  TRIGGER syCampuses_KlassApps_Update
--==========================================================================================
GO
ALTER TABLE [dbo].[syCampuses] ADD CONSTRAINT [PK_syCampuses_CampusId] PRIMARY KEY CLUSTERED  ([CampusId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_syCampuses_CampCode_CampDescrip_StatusId] ON [dbo].[syCampuses] ([CampCode], [CampDescrip], [StatusId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syCampuses] ADD CONSTRAINT [FK_syCampuses_adCountries_CountryId_CountryId] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[adCountries] ([CountryId])
GO
ALTER TABLE [dbo].[syCampuses] ADD CONSTRAINT [FK_syCampuses_syCampuses_CampusId_ParentCampusId] FOREIGN KEY ([ParentCampusId]) REFERENCES [dbo].[syCampuses] ([CampusId])
GO
ALTER TABLE [dbo].[syCampuses] ADD CONSTRAINT [FK_syCampuses_syStates_StateId_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[syStates] ([StateId])
GO
ALTER TABLE [dbo].[syCampuses] ADD CONSTRAINT [FK_syCampuses_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
