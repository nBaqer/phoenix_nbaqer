CREATE TABLE [dbo].[arTrigOffsetTyps]
(
[TrigOffsetTypId] [tinyint] NOT NULL,
[TrigOffTypDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arTrigOffsetTyps] ADD CONSTRAINT [PK_arTrigOffsetTyps_TrigOffsetTypId] PRIMARY KEY CLUSTERED  ([TrigOffsetTypId]) ON [PRIMARY]
GO
