CREATE TABLE [dbo].[syStateBoardCourses]
(
[StateBoardCourseId] [int] NOT NULL IDENTITY(1, 1),
[Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[StateId] [uniqueidentifier] NOT NULL,
[CreatedById] [uniqueidentifier] NOT NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syStateBoardCourses] ADD CONSTRAINT [PK_syStateBoardCourses_StateBoardCourseId] PRIMARY KEY CLUSTERED  ([StateBoardCourseId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syStateBoardCourses] ADD CONSTRAINT [FK_syStateBoardCourses_syStates_StateId_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[syStates] ([StateId])
GO
ALTER TABLE [dbo].[syStateBoardCourses] ADD CONSTRAINT [FK_syStateBoardCourses_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
ALTER TABLE [dbo].[syStateBoardCourses] ADD CONSTRAINT [FK_syStateBoardCourses_syUsers_CreatedById_UserId] FOREIGN KEY ([CreatedById]) REFERENCES [dbo].[syUsers] ([UserId])
GO
