CREATE TABLE [dbo].[saCourseFees]
(
[CourseFeeId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_saCourseFees_CourseFeeId] DEFAULT (newid()),
[StatusId] [uniqueidentifier] NOT NULL,
[CourseId] [uniqueidentifier] NOT NULL,
[TransCodeId] [uniqueidentifier] NOT NULL,
[TuitionCategoryId] [uniqueidentifier] NULL,
[Amount] [money] NOT NULL CONSTRAINT [DF_saCourseFees_Amount] DEFAULT ((0.00)),
[RateScheduleId] [uniqueidentifier] NULL,
[UnitId] [smallint] NOT NULL,
[StartDate] [datetime] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saCourseFees_Audit_Delete] ON [dbo].[saCourseFees]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saCourseFees','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CourseFeeId
                               ,'UnitId'
                               ,CONVERT(VARCHAR(8000),Old.UnitId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CourseFeeId
                               ,'Amount'
                               ,CONVERT(VARCHAR(8000),Old.Amount,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CourseFeeId
                               ,'CourseId'
                               ,CONVERT(VARCHAR(8000),Old.CourseId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CourseFeeId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CourseFeeId
                               ,'RateScheduleId'
                               ,CONVERT(VARCHAR(8000),Old.RateScheduleId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CourseFeeId
                               ,'TransCodeId'
                               ,CONVERT(VARCHAR(8000),Old.TransCodeId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CourseFeeId
                               ,'TuitionCategoryId'
                               ,CONVERT(VARCHAR(8000),Old.TuitionCategoryId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CourseFeeId
                               ,'StartDate'
                               ,CONVERT(VARCHAR(8000),Old.StartDate,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saCourseFees_Audit_Insert] ON [dbo].[saCourseFees]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saCourseFees','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CourseFeeId
                               ,'UnitId'
                               ,CONVERT(VARCHAR(8000),New.UnitId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CourseFeeId
                               ,'Amount'
                               ,CONVERT(VARCHAR(8000),New.Amount,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CourseFeeId
                               ,'CourseId'
                               ,CONVERT(VARCHAR(8000),New.CourseId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CourseFeeId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CourseFeeId
                               ,'RateScheduleId'
                               ,CONVERT(VARCHAR(8000),New.RateScheduleId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CourseFeeId
                               ,'TransCodeId'
                               ,CONVERT(VARCHAR(8000),New.TransCodeId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CourseFeeId
                               ,'TuitionCategoryId'
                               ,CONVERT(VARCHAR(8000),New.TuitionCategoryId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CourseFeeId
                               ,'StartDate'
                               ,CONVERT(VARCHAR(8000),New.StartDate,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO
GO

GO

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saCourseFees_Audit_Update] ON [dbo].[saCourseFees]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saCourseFees','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(UnitId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CourseFeeId
                                   ,'UnitId'
                                   ,CONVERT(VARCHAR(8000),Old.UnitId,121)
                                   ,CONVERT(VARCHAR(8000),New.UnitId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CourseFeeId = New.CourseFeeId
                            WHERE   Old.UnitId <> New.UnitId
                                    OR (
                                         Old.UnitId IS NULL
                                         AND New.UnitId IS NOT NULL
                                       )
                                    OR (
                                         New.UnitId IS NULL
                                         AND Old.UnitId IS NOT NULL
                                       ); 
                IF UPDATE(Amount)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CourseFeeId
                                   ,'Amount'
                                   ,CONVERT(VARCHAR(8000),Old.Amount,121)
                                   ,CONVERT(VARCHAR(8000),New.Amount,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CourseFeeId = New.CourseFeeId
                            WHERE   Old.Amount <> New.Amount
                                    OR (
                                         Old.Amount IS NULL
                                         AND New.Amount IS NOT NULL
                                       )
                                    OR (
                                         New.Amount IS NULL
                                         AND Old.Amount IS NOT NULL
                                       ); 
                IF UPDATE(CourseId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CourseFeeId
                                   ,'CourseId'
                                   ,CONVERT(VARCHAR(8000),Old.CourseId,121)
                                   ,CONVERT(VARCHAR(8000),New.CourseId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CourseFeeId = New.CourseFeeId
                            WHERE   Old.CourseId <> New.CourseId
                                    OR (
                                         Old.CourseId IS NULL
                                         AND New.CourseId IS NOT NULL
                                       )
                                    OR (
                                         New.CourseId IS NULL
                                         AND Old.CourseId IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CourseFeeId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CourseFeeId = New.CourseFeeId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(RateScheduleId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CourseFeeId
                                   ,'RateScheduleId'
                                   ,CONVERT(VARCHAR(8000),Old.RateScheduleId,121)
                                   ,CONVERT(VARCHAR(8000),New.RateScheduleId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CourseFeeId = New.CourseFeeId
                            WHERE   Old.RateScheduleId <> New.RateScheduleId
                                    OR (
                                         Old.RateScheduleId IS NULL
                                         AND New.RateScheduleId IS NOT NULL
                                       )
                                    OR (
                                         New.RateScheduleId IS NULL
                                         AND Old.RateScheduleId IS NOT NULL
                                       ); 
                IF UPDATE(TransCodeId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CourseFeeId
                                   ,'TransCodeId'
                                   ,CONVERT(VARCHAR(8000),Old.TransCodeId,121)
                                   ,CONVERT(VARCHAR(8000),New.TransCodeId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CourseFeeId = New.CourseFeeId
                            WHERE   Old.TransCodeId <> New.TransCodeId
                                    OR (
                                         Old.TransCodeId IS NULL
                                         AND New.TransCodeId IS NOT NULL
                                       )
                                    OR (
                                         New.TransCodeId IS NULL
                                         AND Old.TransCodeId IS NOT NULL
                                       ); 
                IF UPDATE(TuitionCategoryId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CourseFeeId
                                   ,'TuitionCategoryId'
                                   ,CONVERT(VARCHAR(8000),Old.TuitionCategoryId,121)
                                   ,CONVERT(VARCHAR(8000),New.TuitionCategoryId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CourseFeeId = New.CourseFeeId
                            WHERE   Old.TuitionCategoryId <> New.TuitionCategoryId
                                    OR (
                                         Old.TuitionCategoryId IS NULL
                                         AND New.TuitionCategoryId IS NOT NULL
                                       )
                                    OR (
                                         New.TuitionCategoryId IS NULL
                                         AND Old.TuitionCategoryId IS NOT NULL
                                       ); 
                IF UPDATE(StartDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CourseFeeId
                                   ,'StartDate'
                                   ,CONVERT(VARCHAR(8000),Old.StartDate,121)
                                   ,CONVERT(VARCHAR(8000),New.StartDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CourseFeeId = New.CourseFeeId
                            WHERE   Old.StartDate <> New.StartDate
                                    OR (
                                         Old.StartDate IS NULL
                                         AND New.StartDate IS NOT NULL
                                       )
                                    OR (
                                         New.StartDate IS NULL
                                         AND Old.StartDate IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[saCourseFees] ADD CONSTRAINT [PK_saCourseFees_CourseFeeId] PRIMARY KEY CLUSTERED  ([CourseFeeId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saCourseFees] ADD CONSTRAINT [FK_saCourseFees_arReqs_CourseId_ReqId] FOREIGN KEY ([CourseId]) REFERENCES [dbo].[arReqs] ([ReqId])
GO
ALTER TABLE [dbo].[saCourseFees] ADD CONSTRAINT [FK_saCourseFees_saRateSchedules_RateScheduleId_RateScheduleId] FOREIGN KEY ([RateScheduleId]) REFERENCES [dbo].[saRateSchedules] ([RateScheduleId])
GO
ALTER TABLE [dbo].[saCourseFees] ADD CONSTRAINT [FK_saCourseFees_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
ALTER TABLE [dbo].[saCourseFees] ADD CONSTRAINT [FK_saCourseFees_saTransCodes_TransCodeId_TransCodeId] FOREIGN KEY ([TransCodeId]) REFERENCES [dbo].[saTransCodes] ([TransCodeId])
GO
ALTER TABLE [dbo].[saCourseFees] ADD CONSTRAINT [FK_saCourseFees_saTuitionCategories_TuitionCategoryId_TuitionCategoryId] FOREIGN KEY ([TuitionCategoryId]) REFERENCES [dbo].[saTuitionCategories] ([TuitionCategoryId])
GO
