CREATE TABLE [dbo].[syInstitutionImportTypes]
(
[ImportTypeID] [int] NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syInstitutionImportTypes] ADD CONSTRAINT [PK_syInstitutionImportTypes_ImportTypeID] PRIMARY KEY CLUSTERED  ([ImportTypeID]) ON [PRIMARY]
GO
