CREATE TABLE [dbo].[adLeadNotes]
(
[LeadNoteId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_adLeadNotes_LeadNoteId] DEFAULT (newid()),
[StatusId] [uniqueidentifier] NOT NULL,
[LeadId] [uniqueidentifier] NOT NULL,
[LeadNoteDescrip] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModuleCode] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_adLeadNotes_ModuleCode] DEFAULT ('UK'),
[UserId] [uniqueidentifier] NULL,
[CreatedDate] [datetime] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
--========================================================================================== 
-- TRIGGER adLeadNotes_Audit_Delete 
-- DELETE  add Audit History when delete records in adLeadNotes
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadNotes_Audit_Delete] ON [dbo].[adLeadNotes]
    FOR DELETE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Deleted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Deleted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Deleted
                        );
 
        IF @EventRows > 0
            BEGIN
                EXEC fmAuditHistAdd @AuditHistId,'adLeadNotes','D',@EventRows,@EventDate,@UserName;
                -- StatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadNoteId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(MAX),Old.StatusId)
                        FROM    Deleted AS Old;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadNoteId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),Old.LeadId)
                        FROM    Deleted AS Old;
                -- LeadNoteDescrip 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadNoteId
                               ,'LeadNoteDescrip'
                               ,CONVERT(VARCHAR(MAX),Old.LeadNoteDescrip)
                        FROM    Deleted AS Old;
                -- ModuleCode 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadNoteId
                               ,'ModuleCode'
                               ,CONVERT(VARCHAR(MAX),Old.ModuleCode)
                        FROM    Deleted AS Old;
                -- UserId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadNoteId
                               ,'UserId'
                               ,CONVERT(VARCHAR(MAX),Old.UserId)
                        FROM    Deleted AS Old;
                -- CreatedDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadNoteId
                               ,'CreatedDate'
                               ,CONVERT(VARCHAR(MAX),Old.CreatedDate,121)
                        FROM    Deleted AS Old;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadNoteId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),Old.ModUser)
                        FROM    Deleted AS Old;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadNoteId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                        FROM    Deleted AS Old;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadNotes_Audit_Delete 
--========================================================================================== 
 

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
--========================================================================================== 
-- TRIGGER adLeadNotes_Audit_Insert 
-- INSERT  add Audit History when insert records in adLeadNotes
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadNotes_Audit_Insert] ON [dbo].[adLeadNotes]
    FOR INSERT
AS
    BEGIN 
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadNotes','I',@EventRows,@EventDate,@UserName;
                -- StatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadNoteId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(MAX),New.StatusId)
                        FROM    Inserted AS New;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadNoteId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),New.LeadId)
                        FROM    Inserted AS New;
                -- LeadNoteDescrip 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadNoteId
                               ,'LeadNoteDescrip'
                               ,CONVERT(VARCHAR(MAX),New.LeadNoteDescrip)
                        FROM    Inserted AS New;
                -- ModuleCode 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadNoteId
                               ,'ModuleCode'
                               ,CONVERT(VARCHAR(MAX),New.ModuleCode)
                        FROM    Inserted AS New;
                -- UserId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadNoteId
                               ,'UserId'
                               ,CONVERT(VARCHAR(MAX),New.UserId)
                        FROM    Inserted AS New;
                -- CreatedDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadNoteId
                               ,'CreatedDate'
                               ,CONVERT(VARCHAR(MAX),New.CreatedDate,121)
                        FROM    Inserted AS New;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadNoteId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),New.ModUser)
                        FROM    Inserted AS New;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadNoteId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                        FROM    Inserted AS New;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadNotes_Audit_Insert 
--========================================================================================== 
 

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--========================================================================================== 
-- TRIGGER adLeadNotes_Audit_Update 
-- UPDATE  add Audit History when update fields in adLeadNotes
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadNotes_Audit_Update] ON [dbo].[adLeadNotes]
    FOR UPDATE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadNotes','U',@EventRows,@EventDate,@UserName;
                -- StatusId 
                IF UPDATE(StatusId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadNoteId
                                       ,'StatusId'
                                       ,CONVERT(VARCHAR(MAX),Old.StatusId)
                                       ,CONVERT(VARCHAR(MAX),New.StatusId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadNoteId = New.LeadNoteId
                                WHERE   Old.StatusId <> New.StatusId
                                        OR (
                                             Old.StatusId IS NULL
                                             AND New.StatusId IS NOT NULL
                                           )
                                        OR (
                                             New.StatusId IS NULL
                                             AND Old.StatusId IS NOT NULL
                                           );
                    END;
                -- LeadId 
                IF UPDATE(LeadId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadNoteId
                                       ,'LeadId'
                                       ,CONVERT(VARCHAR(MAX),Old.LeadId)
                                       ,CONVERT(VARCHAR(MAX),New.LeadId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadNoteId = New.LeadNoteId
                                WHERE   Old.LeadId <> New.LeadId
                                        OR (
                                             Old.LeadId IS NULL
                                             AND New.LeadId IS NOT NULL
                                           )
                                        OR (
                                             New.LeadId IS NULL
                                             AND Old.LeadId IS NOT NULL
                                           );
                    END;
                -- LeadNoteDescrip 
                IF UPDATE(LeadNoteDescrip)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadNoteId
                                       ,'LeadNoteDescrip'
                                       ,CONVERT(VARCHAR(MAX),Old.LeadNoteDescrip)
                                       ,CONVERT(VARCHAR(MAX),New.LeadNoteDescrip)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadNoteId = New.LeadNoteId
                                WHERE   Old.LeadNoteDescrip <> New.LeadNoteDescrip
                                        OR (
                                             Old.LeadNoteDescrip IS NULL
                                             AND New.LeadNoteDescrip IS NOT NULL
                                           )
                                        OR (
                                             New.LeadNoteDescrip IS NULL
                                             AND Old.LeadNoteDescrip IS NOT NULL
                                           );
                    END;
                -- ModuleCode 
                IF UPDATE(ModuleCode)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadNoteId
                                       ,'ModuleCode'
                                       ,CONVERT(VARCHAR(MAX),Old.ModuleCode)
                                       ,CONVERT(VARCHAR(MAX),New.ModuleCode)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadNoteId = New.LeadNoteId
                                WHERE   Old.ModuleCode <> New.ModuleCode
                                        OR (
                                             Old.ModuleCode IS NULL
                                             AND New.ModuleCode IS NOT NULL
                                           )
                                        OR (
                                             New.ModuleCode IS NULL
                                             AND Old.ModuleCode IS NOT NULL
                                           );
                    END;
                -- UserId 
                IF UPDATE(UserId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadNoteId
                                       ,'UserId'
                                       ,CONVERT(VARCHAR(MAX),Old.UserId)
                                       ,CONVERT(VARCHAR(MAX),New.UserId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadNoteId = New.LeadNoteId
                                WHERE   Old.UserId <> New.UserId
                                        OR (
                                             Old.UserId IS NULL
                                             AND New.UserId IS NOT NULL
                                           )
                                        OR (
                                             New.UserId IS NULL
                                             AND Old.UserId IS NOT NULL
                                           );
                    END;
                -- CreatedDate 
                IF UPDATE(CreatedDate)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadNoteId
                                       ,'CreatedDate'
                                       ,CONVERT(VARCHAR(MAX),Old.CreatedDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.CreatedDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadNoteId = New.LeadNoteId
                                WHERE   Old.CreatedDate <> New.CreatedDate
                                        OR (
                                             Old.CreatedDate IS NULL
                                             AND New.CreatedDate IS NOT NULL
                                           )
                                        OR (
                                             New.CreatedDate IS NULL
                                             AND Old.CreatedDate IS NOT NULL
                                           );
                    END;
                -- ModUser 
                IF UPDATE(ModUser)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadNoteId
                                       ,'ModUser'
                                       ,CONVERT(VARCHAR(MAX),Old.ModUser)
                                       ,CONVERT(VARCHAR(MAX),New.ModUser)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadNoteId = New.LeadNoteId
                                WHERE   Old.ModUser <> New.ModUser
                                        OR (
                                             Old.ModUser IS NULL
                                             AND New.ModUser IS NOT NULL
                                           )
                                        OR (
                                             New.ModUser IS NULL
                                             AND Old.ModUser IS NOT NULL
                                           );
                    END;
                -- ModDate 
                IF UPDATE(ModDate)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadNoteId
                                       ,'ModDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadNoteId = New.LeadNoteId
                                WHERE   Old.ModDate <> New.ModDate
                                        OR (
                                             Old.ModDate IS NULL
                                             AND New.ModDate IS NOT NULL
                                           )
                                        OR (
                                             New.ModDate IS NULL
                                             AND Old.ModDate IS NOT NULL
                                           );
                    END;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadNotes_Audit_Update 
--========================================================================================== 
 

GO
ALTER TABLE [dbo].[adLeadNotes] ADD CONSTRAINT [PK_adLeadNotes_LeadNoteId] PRIMARY KEY CLUSTERED  ([LeadNoteId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adLeadNotes] ADD CONSTRAINT [FK_adLeadNotes_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
