CREATE TABLE [dbo].[saReversedTransactions]
(
[TransactionId] [uniqueidentifier] NOT NULL,
[ReversedTransactionId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[ReversalReason] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReversedTransactionsID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_saReversedTransactions_ReversedTransactionsID] DEFAULT (newsequentialid())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saReversedTransactions] ADD CONSTRAINT [PK_saReversedTransactions_ReversedTransactionsID] PRIMARY KEY CLUSTERED  ([ReversedTransactionsID]) ON [PRIMARY]
GO
