CREATE TABLE [dbo].[PriorWorkContact]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[StEmploymentId] [uniqueidentifier] NOT NULL,
[JobTitle] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FirstName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MiddleName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PrefixId] [uniqueidentifier] NULL,
[IsPhoneInternational] [bit] NOT NULL CONSTRAINT [DF_PriorWorkContact_IsPhoneInternational] DEFAULT ((0)),
[Phone] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Email] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsActive] [bit] NOT NULL,
[ModUser] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL,
[IsDefault] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PriorWorkContact] ADD CONSTRAINT [PK_PriorWorkContact_Id] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PriorWorkContact] ADD CONSTRAINT [FK_PriorWorkContact_adLeadEmployment_StEmploymentId_StEmploymentId] FOREIGN KEY ([StEmploymentId]) REFERENCES [dbo].[adLeadEmployment] ([StEmploymentId])
GO
ALTER TABLE [dbo].[PriorWorkContact] ADD CONSTRAINT [FK_PriorWorkContact_syPrefixes_PrefixId_PrefixId] FOREIGN KEY ([PrefixId]) REFERENCES [dbo].[syPrefixes] ([PrefixId])
GO
