CREATE TABLE [dbo].[arR2T4OverrideInput]
(
[R2T4OverrideInputId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_arR2T4OverrideInput_R2T4OverrideInputId] DEFAULT (newsequentialid()),
[TerminationId] [uniqueidentifier] NOT NULL,
[ProgramUnitTypeId] [uniqueidentifier] NOT NULL,
[PellGrantDisbursed] [money] NULL,
[PellGrantCouldDisbursed] [money] NULL,
[FSEOGDisbursed] [money] NULL,
[FSEOGCouldDisbursed] [money] NULL,
[TeachGrantDisbursed] [money] NULL,
[TeachGrantCouldDisbursed] [money] NULL,
[IraqAfgGrantDisbursed] [money] NULL,
[IraqAfgGrantCouldDisbursed] [money] NULL,
[UnsubLoanNetAmountDisbursed] [money] NULL,
[UnsubLoanNetAmountCouldDisbursed] [money] NULL,
[SubLoanNetAmountDisbursed] [money] NULL,
[SubLoanNetAmountCouldDisbursed] [money] NULL,
[PerkinsLoanDisbursed] [money] NULL,
[PerkinsLoanCouldDisbursed] [money] NULL,
[DirectGraduatePlusLoanDisbursed] [money] NULL,
[DirectGraduatePlusLoanCouldDisbursed] [money] NULL,
[DirectParentPlusLoanDisbursed] [money] NULL,
[DirectParentPlusLoanCouldDisbursed] [money] NULL,
[IsAttendanceNotRequired] [bit] NOT NULL CONSTRAINT [DF_arR2T4OverrideInput_IsAttendanceNotRequired] DEFAULT ((0)),
[StartDate] [datetime] NULL,
[ScheduledEndDate] [datetime] NULL,
[WithdrawalDate] [datetime] NULL,
[CompletedTime] [decimal] (18, 2) NULL,
[TotalTime] [decimal] (18, 2) NULL,
[TuitionFee] [money] NOT NULL,
[RoomFee] [money] NOT NULL,
[BoardFee] [money] NOT NULL,
[OtherFee] [money] NULL,
[IsTuitionChargedByPaymentPeriod] [bit] NOT NULL CONSTRAINT [DF_arR2T4OverrideInput_IsTuitionChargedByPaymentPeriod] DEFAULT ((0)),
[CreditBalanceRefunded] [money] NOT NULL,
[CreatedById] [uniqueidentifier] NOT NULL,
[CreatedDate] [datetime] NOT NULL CONSTRAINT [DF_arR2T4OverrideInput_CreatedDate] DEFAULT (getdate()),
[UpdatedById] [uniqueidentifier] NULL,
[UpdatedDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arR2T4OverrideInput] ADD CONSTRAINT [PK_arR2T4OverrideInput_R2T4OverrideInputId] PRIMARY KEY CLUSTERED  ([R2T4OverrideInputId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arR2T4OverrideInput] ADD CONSTRAINT [FK_arR2T4OverrideInput_arR2T4TerminationDetails_TerminationId_TerminationId] FOREIGN KEY ([TerminationId]) REFERENCES [dbo].[arR2T4TerminationDetails] ([TerminationId])
GO
ALTER TABLE [dbo].[arR2T4OverrideInput] ADD CONSTRAINT [FK_arR2T4OverrideInput_syUsers_CreatedBy_UserId] FOREIGN KEY ([CreatedById]) REFERENCES [dbo].[syUsers] ([UserId])
GO
ALTER TABLE [dbo].[arR2T4OverrideInput] ADD CONSTRAINT [FK_arR2T4OverrideInputs_syUsers_UpdatedBy_UserId] FOREIGN KEY ([UpdatedById]) REFERENCES [dbo].[syUsers] ([UserId])
GO
