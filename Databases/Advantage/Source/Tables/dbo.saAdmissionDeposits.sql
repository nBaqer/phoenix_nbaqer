CREATE TABLE [dbo].[saAdmissionDeposits]
(
[AdmDepositId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_saAdmissionDeposits_AdmDepositId] DEFAULT (newid()),
[ProspectId] [uniqueidentifier] NULL,
[StuEnrollId] [uniqueidentifier] NOT NULL,
[AdmDepositDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PaidById] [uniqueidentifier] NOT NULL,
[DepositDate] [datetime] NOT NULL,
[Amount] [money] NOT NULL,
[PaymentType] [smallint] NOT NULL CONSTRAINT [DF_saAdmissionDeposits_PaymentType] DEFAULT ((0)),
[CheckNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditCardTypeId] [uniqueidentifier] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saAdmissionDeposits] ADD CONSTRAINT [PK_saAdmissionDeposits_AdmDepositId] PRIMARY KEY CLUSTERED  ([AdmDepositId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saAdmissionDeposits] ADD CONSTRAINT [FK_saAdmissionDeposits_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY ([StuEnrollId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
