CREATE TABLE [dbo].[plHowPlaced]
(
[HowPlacedId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_plHowPlaced_HowPlacedId] DEFAULT (newid()),
[HowPlacedCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusId] [uniqueidentifier] NULL,
[HowPlacedDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CampGrpId] [uniqueidentifier] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[plHowPlaced_Audit_Delete] ON [dbo].[plHowPlaced]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'plHowPlaced','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.HowPlacedId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.HowPlacedId
                               ,'HowPlacedCode'
                               ,CONVERT(VARCHAR(8000),Old.HowPlacedCode,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.HowPlacedId
                               ,'HowPlacedDescrip'
                               ,CONVERT(VARCHAR(8000),Old.HowPlacedDescrip,121)
                        FROM    Deleted Old;
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.HowPlacedId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[plHowPlaced_Audit_Insert] ON [dbo].[plHowPlaced]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'plHowPlaced','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.HowPlacedId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.HowPlacedId
                               ,'HowPlacedCode'
                               ,CONVERT(VARCHAR(8000),New.HowPlacedCode,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.HowPlacedId
                               ,'HowPlacedDescrip'
                               ,CONVERT(VARCHAR(8000),New.HowPlacedDescrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.HowPlacedId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New;
            END; 
        END; 



    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--Create plHowPlaced's triggers


CREATE TRIGGER [dbo].[plHowPlaced_Audit_Update] ON [dbo].[plHowPlaced]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'plHowPlaced','U',@EventRows,@EventDate,@UserName; 

            BEGIN 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.HowPlacedId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.HowPlacedId = New.HowPlacedId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 

                IF UPDATE(HowPlacedCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.HowPlacedId
                                   ,'HowPlacedCode'
                                   ,CONVERT(VARCHAR(8000),Old.HowPlacedCode,121)
                                   ,CONVERT(VARCHAR(8000),New.HowPlacedCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.HowPlacedId = New.HowPlacedId
                            WHERE   Old.HowPlacedCode <> New.HowPlacedCode
                                    OR (
                                         Old.HowPlacedCode IS NULL
                                         AND New.HowPlacedCode IS NOT NULL
                                       )
                                    OR (
                                         New.HowPlacedCode IS NULL
                                         AND Old.HowPlacedCode IS NOT NULL
                                       ); 

                IF UPDATE(HowPlacedDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.HowPlacedId
                                   ,'HowPlacedDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.HowPlacedDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.HowPlacedDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.HowPlacedId = New.HowPlacedId
                            WHERE   Old.HowPlacedDescrip <> New.HowPlacedDescrip
                                    OR (
                                         Old.HowPlacedDescrip IS NULL
                                         AND New.HowPlacedDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.HowPlacedDescrip IS NULL
                                         AND Old.HowPlacedDescrip IS NOT NULL
                                       ); 

                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.HowPlacedId
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.HowPlacedId = New.HowPlacedId
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[plHowPlaced] ADD CONSTRAINT [PK_plHowPlaced_HowPlacedId] PRIMARY KEY CLUSTERED  ([HowPlacedId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[plHowPlaced] ADD CONSTRAINT [FK_plHowPlaced_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[plHowPlaced] ADD CONSTRAINT [FK_plHowPlaced_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
