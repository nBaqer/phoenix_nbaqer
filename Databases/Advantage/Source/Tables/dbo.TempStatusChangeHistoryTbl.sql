CREATE TABLE [dbo].[TempStatusChangeHistoryTbl]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[StudentEnrollmentId] [uniqueidentifier] NULL,
[EffectiveDate] [datetime] NULL,
[StatusDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Reason] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RequestedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ChangedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CaseNo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateOfChange] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TempStatusChangeHistoryTbl] ADD CONSTRAINT [PK_TempStatusChangeHistoryTbl_Id] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TempStatusChangeHistoryTbl] ADD CONSTRAINT [FK_TempStatusChangeHistoryTbl_arStuEnrollments_StudentEnrollmentId_StuEnrollId] FOREIGN KEY ([StudentEnrollmentId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
