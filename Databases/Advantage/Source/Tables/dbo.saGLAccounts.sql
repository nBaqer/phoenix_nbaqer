CREATE TABLE [dbo].[saGLAccounts]
(
[GLAccountId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_saGLAccounts_GLAccountId] DEFAULT (newid()),
[StatusId] [uniqueidentifier] NOT NULL,
[GLAccountCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GLAccountDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saGLAccounts] ADD CONSTRAINT [PK_saGLAccounts_GLAccountId] PRIMARY KEY CLUSTERED  ([GLAccountId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saGLAccounts] ADD CONSTRAINT [FK_saGLAccounts_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[saGLAccounts] ADD CONSTRAINT [FK_saGLAccounts_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
