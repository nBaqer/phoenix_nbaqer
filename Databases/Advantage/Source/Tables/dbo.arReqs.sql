CREATE TABLE [dbo].[arReqs]
(
[ReqId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arReqs_ReqId] DEFAULT (newid()),
[CourseID] [int] NULL,
[Code] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Descrip] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[ReqTypeId] [smallint] NOT NULL,
[Credits] [decimal] (18, 2) NULL,
[FinAidCredits] [decimal] (18, 2) NULL CONSTRAINT [DF_arReqs_FinAidCredits] DEFAULT ((0.00)),
[Hours] [decimal] (18, 2) NULL,
[Cnt] [smallint] NULL,
[GrdLvlId] [uniqueidentifier] NULL,
[UnitTypeId] [uniqueidentifier] NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[CourseCatalog] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CourseComments] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SU] [bit] NULL CONSTRAINT [DF_arReqs_SU] DEFAULT ((0)),
[PF] [bit] NULL CONSTRAINT [DF_arReqs_PF] DEFAULT ((0)),
[CourseCategoryId] [uniqueidentifier] NULL,
[TrackTardies] [bit] NOT NULL CONSTRAINT [DF_arReqs_TrackTardies] DEFAULT ((0)),
[TardiesMakingAbsence] [int] NULL,
[DeptId] [uniqueidentifier] NULL,
[MinDate] [smalldatetime] NULL CONSTRAINT [DF_arReqs_MinDate] DEFAULT (NULL),
[IsComCourse] [bit] NULL CONSTRAINT [DF_arReqs_IsComCourse] DEFAULT ((0)),
[IsOnLine] [bit] NULL CONSTRAINT [DF_arReqs_IsOnLine] DEFAULT ((0)),
[CompletedDate] [datetime] NULL,
[IsExternship] [bit] NULL CONSTRAINT [DF_arReqs_IsExternship] DEFAULT ((0)),
[UseTimeClock] [bit] NULL,
[IsAttendanceOnly] [bit] NOT NULL CONSTRAINT [DF_arReqs_IsAttendanceOnly] DEFAULT ((0)),
[AllowCompletedCourseRetake] [bit] NOT NULL CONSTRAINT [DF_arReqs_AllowCompletedCourseRetake] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[arReqs_Audit_Delete] ON [dbo].[arReqs]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arReqs','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ReqId
                               ,'Code'
                               ,CONVERT(VARCHAR(8000),Old.Code,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ReqId
                               ,'Descrip'
                               ,CONVERT(VARCHAR(8000),Old.Descrip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ReqId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ReqId
                               ,'ReqTypeId'
                               ,CONVERT(VARCHAR(8000),Old.ReqTypeId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ReqId
                               ,'Credits'
                               ,CONVERT(VARCHAR(8000),Old.Credits,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ReqId
                               ,'FinAidCredits'
                               ,CONVERT(VARCHAR(8000),Old.FinAidCredits,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
    AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ReqId
                               ,'Hours'
                               ,CONVERT(VARCHAR(8000),Old.Hours,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ReqId
                               ,'Cnt'
                               ,CONVERT(VARCHAR(8000),Old.Cnt,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ReqId
                               ,'GrdLvlId'
                               ,CONVERT(VARCHAR(8000),Old.GrdLvlId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ReqId
                               ,'UnitTypeId'
                               ,CONVERT(VARCHAR(8000),Old.UnitTypeId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ReqId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ReqId
                               ,'CourseCatalog'
                               ,CONVERT(VARCHAR(8000),Old.CourseCatalog,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ReqId
                               ,'CourseComments'
                               ,CONVERT(VARCHAR(8000),Old.CourseComments,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ReqId
                               ,'SU'
                               ,CONVERT(VARCHAR(8000),Old.SU,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                 ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ReqId
                               ,'PF'
                               ,CONVERT(VARCHAR(8000),Old.PF,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ReqId
                               ,'CourseCategoryId'
                               ,CONVERT(VARCHAR(8000),Old.CourseCategoryId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ReqId
                               ,'TrackTardies'
                               ,CONVERT(VARCHAR(8000),Old.TrackTardies,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ReqId
                               ,'TardiesMakingAbsence'
                               ,CONVERT(VARCHAR(8000),Old.TardiesMakingAbsence,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ReqId
                               ,'IsOnLine'
                               ,CONVERT(VARCHAR(8000),Old.IsOnLine,121)
                        FROM    Deleted Old; 
            END; 
        END;



    SET NOCOUNT OFF;
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[arReqs_Audit_Insert] ON [dbo].[arReqs]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arReqs','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ReqId
                               ,'Code'
                               ,CONVERT(VARCHAR(8000),New.Code,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ReqId
                               ,'Descrip'
                               ,CONVERT(VARCHAR(8000),New.Descrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ReqId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ReqId
                               ,'ReqTypeId'
                               ,CONVERT(VARCHAR(8000),New.ReqTypeId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ReqId
                               ,'Credits'
                               ,CONVERT(VARCHAR(8000),New.Credits,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ReqId
                               ,'FinAidCredits'
                               ,CONVERT(VARCHAR(8000),New.FinAidCredits,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
              AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ReqId
                               ,'Hours'
                               ,CONVERT(VARCHAR(8000),New.Hours,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ReqId
                               ,'Cnt'
                               ,CONVERT(VARCHAR(8000),New.Cnt,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ReqId
                               ,'GrdLvlId'
                               ,CONVERT(VARCHAR(8000),New.GrdLvlId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ReqId
                               ,'UnitTypeId'
                               ,CONVERT(VARCHAR(8000),New.UnitTypeId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ReqId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ReqId
                               ,'CourseCatalog'
                               ,CONVERT(VARCHAR(8000),New.CourseCatalog,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ReqId
                               ,'CourseComments'
                               ,CONVERT(VARCHAR(8000),New.CourseComments,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ReqId
                               ,'SU'
                               ,CONVERT(VARCHAR(8000),New.SU,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ReqId
                               ,'PF'
                               ,CONVERT(VARCHAR(8000),New.PF,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ReqId
                               ,'CourseCategoryId'
                               ,CONVERT(VARCHAR(8000),New.CourseCategoryId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ReqId
                               ,'TrackTardies'
                               ,CONVERT(VARCHAR(8000),New.TrackTardies,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ReqId
                               ,'TardiesMakingAbsence'
                               ,CONVERT(VARCHAR(8000),New.TardiesMakingAbsence,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ReqId
                               ,'IsOnLine'
                               ,CONVERT(VARCHAR(8000),New.IsOnLine,121)
                        FROM    Inserted New; 
            END; 
        END;



    SET NOCOUNT OFF;
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[arReqs_Audit_Update] ON [dbo].[arReqs]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arReqs','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(Code)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ReqId
                                   ,'Code'
                                   ,CONVERT(VARCHAR(8000),Old.Code,121)
                                   ,CONVERT(VARCHAR(8000),New.Code,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ReqId = New.ReqId
                            WHERE   Old.Code <> New.Code
                                    OR (
                                         Old.Code IS NULL
                                         AND New.Code IS NOT NULL
                                       )
                                    OR (
                                         New.Code IS NULL
                                         AND Old.Code IS NOT NULL
                                       ); 
                IF UPDATE(Descrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ReqId
                                   ,'Descrip'
                                   ,CONVERT(VARCHAR(8000),Old.Descrip,121)
                                   ,CONVERT(VARCHAR(8000),New.Descrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ReqId = New.ReqId
                            WHERE   Old.Descrip <> New.Descrip
                                    OR (
                                         Old.Descrip IS NULL
                                         AND New.Descrip IS NOT NULL
                                       )
                                    OR (
                                         New.Descrip IS NULL
                                         AND Old.Descrip IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ReqId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ReqId = New.ReqId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(ReqTypeId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ReqId
                                   ,'ReqTypeId'
                                   ,CONVERT(VARCHAR(8000),Old.ReqTypeId,121)
                                   ,CONVERT(VARCHAR(8000),New.ReqTypeId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ReqId = New.ReqId
                            WHERE   Old.ReqTypeId <> New.ReqTypeId
                                    OR (
                                         Old.ReqTypeId IS NULL
                                         AND New.ReqTypeId IS NOT NULL
                                       )
                                    OR (
                                         New.ReqTypeId IS NULL
                                         AND Old.ReqTypeId IS NOT NULL
                                       ); 
                IF UPDATE(Credits)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ReqId
                                   ,'Credits'
                                   ,CONVERT(VARCHAR(8000),Old.Credits,121)
                                   ,CONVERT(VARCHAR(8000),New.Credits,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ReqId = New.ReqId
                            WHERE   Old.Credits <> New.Credits
                                    OR (
                                         Old.Credits IS NULL
                                         AND New.Credits IS NOT NULL
                                       )
                                    OR (
                                         New.Credits IS NULL
                                         AND Old.Credits IS NOT NULL
                                       ); 
                IF UPDATE(FinAidCredits)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ReqId
                                   ,'FinAidCredits'
                                   ,CONVERT(VARCHAR(8000),Old.FinAidCredits,121)
                                   ,CONVERT(VARCHAR(8000),New.FinAidCredits,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ReqId = New.ReqId
                            WHERE   Old.FinAidCredits <> New.FinAidCredits
                                    OR (
                                         Old.FinAidCredits IS NULL
                                         AND New.FinAidCredits IS NOT NULL
                                       )
                                    OR (
                                         New.FinAidCredits IS NULL
                                         AND Old.FinAidCredits IS NOT NULL
                                       ); 
                IF UPDATE(Hours)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ReqId
                                   ,'Hours'
                                   ,CONVERT(VARCHAR(8000),Old.Hours,121)
                                   ,CONVERT(VARCHAR(8000),New.Hours,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ReqId = New.ReqId
                            WHERE   Old.Hours <> New.Hours
                                    OR (
                                         Old.Hours IS NULL
                                         AND New.Hours IS NOT NULL
                                       )
                                    OR (
                                         New.Hours IS NULL
                                         AND Old.Hours IS NOT NULL
                                       ); 
                IF UPDATE(Cnt)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ReqId
                                   ,'Cnt'
                                   ,CONVERT(VARCHAR(8000),Old.Cnt,121)
                                   ,CONVERT(VARCHAR(8000),New.Cnt,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ReqId = New.ReqId
                            WHERE   Old.Cnt <> New.Cnt
                                    OR (
                                         Old.Cnt IS NULL
                                         AND New.Cnt IS NOT NULL
                                       )
                                    OR (
                                         New.Cnt IS NULL
                                         AND Old.Cnt IS NOT NULL
                                       ); 
                IF UPDATE(GrdLvlId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ReqId
                                   ,'GrdLvlId'
                                   ,CONVERT(VARCHAR(8000),Old.GrdLvlId,121)
                                   ,CONVERT(VARCHAR(8000),New.GrdLvlId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ReqId = New.ReqId
                            WHERE   Old.GrdLvlId <> New.GrdLvlId
                                    OR (
                                     Old.GrdLvlId IS NULL
                                         AND New.GrdLvlId IS NOT NULL
                                       )
                                    OR (
                                         New.GrdLvlId IS NULL
                                         AND Old.GrdLvlId IS NOT NULL
                                       ); 
                IF UPDATE(UnitTypeId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ReqId
                                   ,'UnitTypeId'
                                   ,CONVERT(VARCHAR(8000),Old.UnitTypeId,121)
                                   ,CONVERT(VARCHAR(8000),New.UnitTypeId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ReqId = New.ReqId
                            WHERE   Old.UnitTypeId <> New.UnitTypeId
                                    OR (
                                         Old.UnitTypeId IS NULL
                                         AND New.UnitTypeId IS NOT NULL
                                       )
                                    OR (
                                         New.UnitTypeId IS NULL
                                         AND Old.UnitTypeId IS NOT NULL
                                       ); 
                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ReqId
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ReqId = New.ReqId
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 
                IF UPDATE(CourseCatalog)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ReqId
                                   ,'CourseCatalog'
                                   ,CONVERT(VARCHAR(8000),Old.CourseCatalog,121)
                                   ,CONVERT(VARCHAR(8000),New.CourseCatalog,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ReqId = New.ReqId
                            WHERE   Old.CourseCatalog <> New.CourseCatalog
                                    OR (
                                         Old.CourseCatalog IS NULL
              AND New.CourseCatalog IS NOT NULL
                                       )
                                    OR (
                                         New.CourseCatalog IS NULL
                                         AND Old.CourseCatalog IS NOT NULL
                                       ); 
                IF UPDATE(CourseComments)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ReqId
                                   ,'CourseComments'
                                   ,CONVERT(VARCHAR(8000),Old.CourseComments,121)
                                   ,CONVERT(VARCHAR(8000),New.CourseComments,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ReqId = New.ReqId
                            WHERE   Old.CourseComments <> New.CourseComments
                                    OR (
                                         Old.CourseComments IS NULL
                                         AND New.CourseComments IS NOT NULL
                                       )
                                    OR (
                                         New.CourseComments IS NULL
                                         AND Old.CourseComments IS NOT NULL
                                       ); 
                IF UPDATE(SU)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ReqId
                                   ,'SU'
                                   ,CONVERT(VARCHAR(8000),Old.SU,121)
                                   ,CONVERT(VARCHAR(8000),New.SU,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ReqId = New.ReqId
                            WHERE   Old.SU <> New.SU
                                    OR (
                                         Old.SU IS NULL
                                         AND New.SU IS NOT NULL
                                       )
                                    OR (
                                         New.SU IS NULL
                                         AND Old.SU IS NOT NULL
                                       ); 
                IF UPDATE(PF)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ReqId
                                   ,'PF'
                                   ,CONVERT(VARCHAR(8000),Old.PF,121)
                                   ,CONVERT(VARCHAR(8000),New.PF,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ReqId = New.ReqId
                            WHERE   Old.PF <> New.PF
                                    OR (
                                         Old.PF IS NULL
                                         AND New.PF IS NOT NULL
                                       )
                                    OR (
                                         New.PF IS NULL
                                         AND Old.PF IS NOT NULL
                                       ); 
                IF UPDATE(CourseCategoryId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ReqId
                                   ,'CourseCategoryId'
                                   ,CONVERT(VARCHAR(8000),Old.CourseCategoryId,121)
                                   ,CONVERT(VARCHAR(8000),New.CourseCategoryId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ReqId = New.ReqId
                            WHERE   Old.CourseCategoryId <> New.CourseCategoryId
                                    OR (
                                         Old.CourseCategoryId IS NULL
                                         AND New.CourseCategoryId IS NOT NULL
                                       )
                                    OR (
                                         New.CourseCategoryId IS NULL
                                         AND Old.CourseCategoryId IS NOT NULL
                                       ); 
                IF UPDATE(TrackTardies)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ReqId
                                   ,'TrackTardies'
                                   ,CONVERT(VARCHAR(8000),Old.TrackTardies,121)
                                   ,CONVERT(VARCHAR(8000),New.TrackTardies,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ReqId = New.ReqId
                            WHERE   Old.TrackTardies <> New.TrackTardies
                                    OR (
                                         Old.TrackTardies IS NULL
                                         AND New.TrackTardies IS NOT NULL
                                       )
                                    OR (
                                         New.TrackTardies IS NULL
                                         AND Old.TrackTardies IS NOT NULL
                                       ); 
                IF UPDATE(tardiesmakingabsence)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ReqId
                                   ,'TardiesMakingAbsence'
                                   ,CONVERT(VARCHAR(8000),Old.TardiesMakingAbsence,121)
                                   ,CONVERT(VARCHAR(8000),New.TardiesMakingAbsence,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ReqId = New.ReqId
                            WHERE   Old.TardiesMakingAbsence <> New.TardiesMakingAbsence
                                    OR (
                                         Old.TardiesMakingAbsence IS NULL
                                         AND New.TardiesMakingAbsence IS NOT NULL
                                       )
                                    OR (
                                      New.TardiesMakingAbsence IS NULL
                                         AND Old.TardiesMakingAbsence IS NOT NULL
                                       ); 
                IF UPDATE(IsOnLine)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ReqId
                                   ,'IsOnLine'
                                   ,CONVERT(VARCHAR(8000),Old.IsOnLine,121)
                                   ,CONVERT(VARCHAR(8000),New.IsOnLine,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ReqId = New.ReqId
                            WHERE   Old.IsOnLine <> New.IsOnLine
                                    OR (
                                         Old.IsOnLine IS NULL
                                         AND New.IsOnLine IS NOT NULL
                                       )
                                    OR (
                                         New.IsOnLine IS NULL
                                         AND Old.IsOnLine IS NOT NULL
                                       ); 
            END; 
        END;



    SET NOCOUNT OFF;
GO
ALTER TABLE [dbo].[arReqs] ADD CONSTRAINT [PK_arReqs_ReqId] PRIMARY KEY CLUSTERED  ([ReqId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_arReqs_ReqId_Descrip_Code_Credits_FinAidCredits] ON [dbo].[arReqs] ([ReqId], [Descrip], [Code], [Credits], [FinAidCredits]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arReqs] ADD CONSTRAINT [FK_arReqs_adEdLvls_GrdLvlId_EdLvlId] FOREIGN KEY ([GrdLvlId]) REFERENCES [dbo].[adEdLvls] ([EdLvlId])
GO
ALTER TABLE [dbo].[arReqs] ADD CONSTRAINT [FK_arReqs_arAttUnitType_UnitTypeId_UnitTypeId] FOREIGN KEY ([UnitTypeId]) REFERENCES [dbo].[arAttUnitType] ([UnitTypeId])
GO
ALTER TABLE [dbo].[arReqs] ADD CONSTRAINT [FK_arReqs_arCourseCategories_CourseCategoryId_CourseCategoryId] FOREIGN KEY ([CourseCategoryId]) REFERENCES [dbo].[arCourseCategories] ([CourseCategoryId])
GO
ALTER TABLE [dbo].[arReqs] ADD CONSTRAINT [FK_arReqs_arDepartments_DeptId_DeptId] FOREIGN KEY ([DeptId]) REFERENCES [dbo].[arDepartments] ([DeptId])
GO
ALTER TABLE [dbo].[arReqs] ADD CONSTRAINT [FK_arReqs_arReqTypes_ReqTypeId_ReqTypeId] FOREIGN KEY ([ReqTypeId]) REFERENCES [dbo].[arReqTypes] ([ReqTypeId])
GO
ALTER TABLE [dbo].[arReqs] ADD CONSTRAINT [FK_arReqs_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[arReqs] ADD CONSTRAINT [FK_arReqs_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
