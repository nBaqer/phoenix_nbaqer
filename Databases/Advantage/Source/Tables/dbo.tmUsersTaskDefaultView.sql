CREATE TABLE [dbo].[tmUsersTaskDefaultView]
(
[UserTaskDefViewId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_tmUsersTaskDefaultView_UserTaskDefViewId] DEFAULT (newsequentialid()),
[UserID] [uniqueidentifier] NOT NULL,
[OthersID] [uniqueidentifier] NULL,
[DefaultView] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tmUsersTaskDefaultView] ADD CONSTRAINT [PK_tmUsersTaskDefaultView_UserTaskDefViewId] PRIMARY KEY CLUSTERED  ([UserTaskDefViewId]) ON [PRIMARY]
GO
