CREATE TABLE [dbo].[saPmtDisbRel]
(
[PmtDisbRelId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_saPmtDisbRel_PmtDisbRelId] DEFAULT (newid()),
[TransactionId] [uniqueidentifier] NOT NULL,
[Amount] [decimal] (18, 4) NOT NULL CONSTRAINT [DF_saPmtDisbRel_Amount] DEFAULT ((0)),
[AwardScheduleId] [uniqueidentifier] NULL,
[PayPlanScheduleId] [uniqueidentifier] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[StuAwardId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saPmtDisbRel] ADD CONSTRAINT [PK_saPmtDisbRel_PmtDisbRelId] PRIMARY KEY CLUSTERED  ([PmtDisbRelId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_saPmtDisbRel_Amount] ON [dbo].[saPmtDisbRel] ([Amount]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_saPmtDisbRel_AwardScheduleId] ON [dbo].[saPmtDisbRel] ([AwardScheduleId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_saPmtDisbRel_TransactionId] ON [dbo].[saPmtDisbRel] ([TransactionId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saPmtDisbRel] ADD CONSTRAINT [FK_saPmtDisbRel_faStudentAwardSchedule_AwardScheduleId_AwardScheduleId] FOREIGN KEY ([AwardScheduleId]) REFERENCES [dbo].[faStudentAwardSchedule] ([AwardScheduleId])
GO
ALTER TABLE [dbo].[saPmtDisbRel] ADD CONSTRAINT [FK_saPmtDisbRel_faStuPaymentPlanSchedule_PayPlanScheduleId_PayPlanScheduleId] FOREIGN KEY ([PayPlanScheduleId]) REFERENCES [dbo].[faStuPaymentPlanSchedule] ([PayPlanScheduleId])
GO
