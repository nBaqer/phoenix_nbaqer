CREATE TABLE [dbo].[adImportLeadsFields]
(
[FldName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Caption] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DDLName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ILFieldId] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adImportLeadsFields] ADD CONSTRAINT [PK_adImportLeadsFields_ILFieldId] PRIMARY KEY CLUSTERED  ([ILFieldId]) ON [PRIMARY]
GO
