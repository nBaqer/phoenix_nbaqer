CREATE TABLE [dbo].[tmResultActions]
(
[ResultActionId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_tmResultActions_ResultActionId] DEFAULT (newid()),
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Tablename] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ColumnName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RowGuid] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Values_Query] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tmResultActions] ADD CONSTRAINT [PK_tmResultActions_ResultActionId] PRIMARY KEY CLUSTERED  ([ResultActionId]) ON [PRIMARY]
GO
