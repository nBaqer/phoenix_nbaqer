CREATE TABLE [dbo].[tmResultTasks]
(
[ResultTaskId] [uniqueidentifier] NOT NULL,
[ResultId] [uniqueidentifier] NULL,
[TaskId] [uniqueidentifier] NULL,
[TaskOrder] [smallint] NULL,
[StartGap] [smallint] NULL,
[StartGapUnit] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tmResultTasks] ADD CONSTRAINT [PK_tmResultTasks_ResultTaskId] PRIMARY KEY CLUSTERED  ([ResultTaskId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tmResultTasks] ADD CONSTRAINT [FK_tmResultTasks_tmResults_ResultId_ResultId] FOREIGN KEY ([ResultId]) REFERENCES [dbo].[tmResults] ([ResultId])
GO
ALTER TABLE [dbo].[tmResultTasks] ADD CONSTRAINT [FK_tmResultTasks_tmTasks_TaskId_TaskId] FOREIGN KEY ([TaskId]) REFERENCES [dbo].[tmTasks] ([TaskId])
GO
