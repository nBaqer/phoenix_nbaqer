CREATE TABLE [dbo].[syKlassAppConfigurationSetting]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[AdvantageId] [varchar] (38) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[KlassAppId] [int] NOT NULL CONSTRAINT [DF_syKlassAppConfigurationSetting_KlassAppId] DEFAULT ((0)),
[KlassEntityId] [int] NOT NULL,
[KlassOperationTypeId] [int] NOT NULL,
[IsCustomField] [bit] NOT NULL CONSTRAINT [DF_syKlassAppConfigurationSetting_IsCustomField] DEFAULT ((1)),
[IsActive] [bit] NOT NULL CONSTRAINT [DF_syKlassAppConfigurationSetting_IsActive] DEFAULT ((1)),
[ItemStatus] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_syKlassAppConfigurationSetting_ItemStatus] DEFAULT ('N'),
[ItemValue] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_syKlassAppConfigurationSetting_ItemValue] DEFAULT (''),
[ItemLabel] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_syKlassAppConfigurationSetting_ItemLabel] DEFAULT (''),
[ItemValueType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_syKlassAppConfigurationSetting_ItemValueType] DEFAULT (''),
[CreationDate] [datetime] NOT NULL,
[ModDate] [datetime] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastOperationLog] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_syKlassAppConfigurationSetting_LastOperationLog] DEFAULT ('OK')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syKlassAppConfigurationSetting] ADD CONSTRAINT [PK_syKlassAppConfigurationSetting_Id] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syKlassAppConfigurationSetting] ADD CONSTRAINT [FK_syKlassAppConfigurationSetting_syKlassEntity_KlassEntityId_KlassEntityId] FOREIGN KEY ([KlassEntityId]) REFERENCES [dbo].[syKlassEntity] ([KlassEntityId])
GO
ALTER TABLE [dbo].[syKlassAppConfigurationSetting] ADD CONSTRAINT [FK_syKlassAppConfigurationSetting_syKlassOperationType_KlassOperationTypeId_KlassOperationTypeId] FOREIGN KEY ([KlassOperationTypeId]) REFERENCES [dbo].[syKlassOperationType] ([KlassOperationTypeId])
GO
EXEC sp_addextendedproperty N'MS_Description', 'Item Status  U - update or insert status I - Insert Status D - Delete Status N - Normal', 'SCHEMA', N'dbo', 'TABLE', N'syKlassAppConfigurationSetting', 'COLUMN', N'ItemStatus'
GO
EXEC sp_addextendedproperty N'MS_Description', 'it is the value to be show if the configuration settings is a custom field. it is not used in location.', 'SCHEMA', N'dbo', 'TABLE', N'syKlassAppConfigurationSetting', 'COLUMN', N'ItemValue'
GO
EXEC sp_addextendedproperty N'MS_Description', 'This is our internal reference for value to be send', 'SCHEMA', N'dbo', 'TABLE', N'syKlassAppConfigurationSetting', 'COLUMN', N'KlassEntityId'
GO
EXEC sp_addextendedproperty N'MS_Description', 'Class Type its the class type defined in Class App can be for now:   	- student - status, program version 	- locations - campus', 'SCHEMA', N'dbo', 'TABLE', N'syKlassAppConfigurationSetting', 'COLUMN', N'KlassOperationTypeId'
GO
