CREATE TABLE [dbo].[tmWorkflow]
(
[WorkflowId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_tmWorkflow_WorkflowId] DEFAULT (newid()),
[Name] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Descrip] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CmpGroupid] [uniqueidentifier] NULL,
[ModuleEntityId] [uniqueidentifier] NULL,
[StartTaskId] [uniqueidentifier] NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsActive] [tinyint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tmWorkflow] ADD CONSTRAINT [PK_tmWorkflow_WorkflowId] PRIMARY KEY CLUSTERED  ([WorkflowId]) ON [PRIMARY]
GO
