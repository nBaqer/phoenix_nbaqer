CREATE TABLE [dbo].[arCourseCategories]
(
[CourseCategoryId] [uniqueidentifier] NOT NULL,
[Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arCourseCategories] ADD CONSTRAINT [PK_arCourseCategories_CourseCategoryId] PRIMARY KEY CLUSTERED  ([CourseCategoryId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arCourseCategories] ADD CONSTRAINT [FK_arCourseCategories_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[arCourseCategories] ADD CONSTRAINT [FK_arCourseCategories_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
