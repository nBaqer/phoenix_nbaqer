CREATE TABLE [dbo].[AdVehicles]
(
[VehicleId] [int] NOT NULL IDENTITY(1, 1),
[LeadId] [uniqueidentifier] NOT NULL,
[Position] [int] NOT NULL,
[Permit] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_AdVehicles_Permit] DEFAULT (''),
[Make] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_AdVehicles_Make] DEFAULT (''),
[Model] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_AdVehicles_Model] DEFAULT (''),
[Color] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_AdVehicles_Color] DEFAULT (''),
[Plate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_AdVehicles_Plate] DEFAULT (''),
[ModDate] [datetime] NOT NULL CONSTRAINT [DF_AdVehicles_ModDate] DEFAULT (getdate()),
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AdVehicles] ADD CONSTRAINT [PK_AdVehicles_VehicleId] PRIMARY KEY CLUSTERED  ([VehicleId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AdVehicles] ADD CONSTRAINT [UIX_AdVehicles_LeadId_Position] UNIQUE NONCLUSTERED  ([LeadId], [Position]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AdVehicles] ADD CONSTRAINT [FK_AdVehicles_adLeads_LeadId_LeadId] FOREIGN KEY ([LeadId]) REFERENCES [dbo].[adLeads] ([LeadId])
GO
