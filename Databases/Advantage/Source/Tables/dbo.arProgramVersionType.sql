CREATE TABLE [dbo].[arProgramVersionType]
(
[ProgramVersionTypeId] [int] NOT NULL,
[CodeProgramVersionType] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DescriptionProgramVersionType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arProgramVersionType] ADD CONSTRAINT [PK_arProgramVersionType_ProgramVersionTypeId] PRIMARY KEY CLUSTERED  ([ProgramVersionTypeId]) ON [PRIMARY]
GO
