CREATE TABLE [dbo].[syStatusLevels]
(
[StatusLevelId] [int] NOT NULL,
[StatusLevelDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syStatusLevels] ADD CONSTRAINT [PK_syStatusLevels_StatusLevelId] PRIMARY KEY CLUSTERED  ([StatusLevelId]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[syStatusLevels] TO [AdvantageRole]
GO
