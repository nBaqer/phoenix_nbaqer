CREATE TABLE [dbo].[adColleges]
(
[CollegeId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_adColleges_CollegeId] DEFAULT (newid()),
[CollegeCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[CollegeName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Address1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateId] [uniqueidentifier] NULL,
[Zip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[ForeignZip] [bit] NOT NULL CONSTRAINT [DF_adColleges_ForeignZip] DEFAULT ((0)),
[ForeignPhone] [bit] NOT NULL CONSTRAINT [DF_adColleges_ForeignPhone] DEFAULT ((0)),
[OtherState] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CampGrpId] [uniqueidentifier] NULL,
[Phone] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CountryId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[adColleges_Audit_Delete] ON [dbo].[adColleges]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adColleges','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CollegeId
                               ,'CollegeCode'
                               ,CONVERT(VARCHAR(8000),Old.CollegeCode,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CollegeId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CollegeId
                               ,'Address1'
                               ,CONVERT(VARCHAR(8000),Old.Address1,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CollegeId
                               ,'Address2'
                               ,CONVERT(VARCHAR(8000),Old.Address2,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CollegeId
                               ,'StateId'
                               ,CONVERT(VARCHAR(8000),Old.StateId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CollegeId
                               ,'CollegeName'
                               ,CONVERT(VARCHAR(8000),Old.CollegeName,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CollegeId
                               ,'City'
                               ,CONVERT(VARCHAR(8000),Old.City,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CollegeId
                               ,'Zip'
                               ,CONVERT(VARCHAR(8000),Old.Zip,121)
                        FROM    Deleted Old; 
            END; 
        END;



    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[adColleges_Audit_Insert] ON [dbo].[adColleges]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adColleges','I',@EventRows,@EventDate,@UserName;
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CollegeId
                               ,'CollegeCode'
                               ,CONVERT(VARCHAR(8000),New.CollegeCode,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CollegeId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CollegeId
                               ,'Address1'
                               ,CONVERT(VARCHAR(8000),New.Address1,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CollegeId
                               ,'Address2'
                               ,CONVERT(VARCHAR(8000),New.Address2,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CollegeId
                               ,'StateId'
                               ,CONVERT(VARCHAR(8000),New.StateId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CollegeId
                               ,'CollegeName'
                               ,CONVERT(VARCHAR(8000),New.CollegeName,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CollegeId
                               ,'City'
                               ,CONVERT(VARCHAR(8000),New.City,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CollegeId
                               ,'Zip'
                               ,CONVERT(VARCHAR(8000),New.Zip,121)
                        FROM    Inserted New; 
            END; 
        END;



    SET NOCOUNT OFF; 
GO
GO

GO

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[adColleges_Audit_Update] ON [dbo].[adColleges]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adColleges','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(CollegeCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CollegeId
                                   ,'CollegeCode'
                                   ,CONVERT(VARCHAR(8000),Old.CollegeCode,121)
                                   ,CONVERT(VARCHAR(8000),New.CollegeCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CollegeId = New.CollegeId
                            WHERE   Old.CollegeCode <> New.CollegeCode
                                    OR (
                                         Old.CollegeCode IS NULL
                                         AND New.CollegeCode IS NOT NULL
                                       )
                                    OR (
                                         New.CollegeCode IS NULL
                                         AND Old.CollegeCode IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CollegeId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CollegeId = New.CollegeId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(Address1)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CollegeId
                                   ,'Address1'
                                   ,CONVERT(VARCHAR(8000),Old.Address1,121)
                                   ,CONVERT(VARCHAR(8000),New.Address1,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CollegeId = New.CollegeId
                            WHERE   Old.Address1 <> New.Address1
                                    OR (
                                         Old.Address1 IS NULL
                                         AND New.Address1 IS NOT NULL
                                       )
                                    OR (
                                         New.Address1 IS NULL
                                         AND Old.Address1 IS NOT NULL
                                       ); 
                IF UPDATE(Address2)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CollegeId
                                   ,'Address2'
                                   ,CONVERT(VARCHAR(8000),Old.Address2,121)
                                   ,CONVERT(VARCHAR(8000),New.Address2,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CollegeId = New.CollegeId
                            WHERE   Old.Address2 <> New.Address2
                                    OR (
                                         Old.Address2 IS NULL
                                         AND New.Address2 IS NOT NULL
                                       )
                                    OR (
                                         New.Address2 IS NULL
                                         AND Old.Address2 IS NOT NULL
                                       ); 
                IF UPDATE(StateId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CollegeId
                                   ,'StateId'
                                   ,CONVERT(VARCHAR(8000),Old.StateId,121)
                                   ,CONVERT(VARCHAR(8000),New.StateId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CollegeId = New.CollegeId
                            WHERE   Old.StateId <> New.StateId
                                    OR (
                                         Old.StateId IS NULL
                                         AND New.StateId IS NOT NULL
                                       )
                                    OR (
                                         New.StateId IS NULL
                                         AND Old.StateId IS NOT NULL
                                       ); 
                IF UPDATE(CollegeName)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CollegeId
                                   ,'CollegeName'
                                   ,CONVERT(VARCHAR(8000),Old.CollegeName,121)
                                   ,CONVERT(VARCHAR(8000),New.CollegeName,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CollegeId = New.CollegeId
                            WHERE   Old.CollegeName <> New.CollegeName
                                    OR (
                                         Old.CollegeName IS NULL
                                         AND New.CollegeName IS NOT NULL
                                       )
                                    OR (
                                         New.CollegeName IS NULL
                                         AND Old.CollegeName IS NOT NULL
                                       ); 
                IF UPDATE(City)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CollegeId
                                   ,'City'
                                   ,CONVERT(VARCHAR(8000),Old.City,121)
                                   ,CONVERT(VARCHAR(8000),New.City,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CollegeId = New.CollegeId
                            WHERE   Old.City <> New.City
                                    OR (
                                         Old.City IS NULL
                                         AND New.City IS NOT NULL
                                       )
                                    OR (
                                         New.City IS NULL
                                         AND Old.City IS NOT NULL
                                       ); 
                IF UPDATE(Zip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CollegeId
                                   ,'Zip'
                                   ,CONVERT(VARCHAR(8000),Old.Zip,121)
                                   ,CONVERT(VARCHAR(8000),New.Zip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CollegeId = New.CollegeId
                            WHERE   Old.Zip <> New.Zip
                                    OR (
                                         Old.Zip IS NULL
                                         AND New.Zip IS NOT NULL
                                       )
                                    OR (
                                         New.Zip IS NULL
                                         AND Old.Zip IS NOT NULL
                                       ); 
            END; 
        END;



    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[adColleges] ADD CONSTRAINT [PK_adColleges_CollegeId] PRIMARY KEY CLUSTERED  ([CollegeId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_adColleges_CollegeCode_CollegeName] ON [dbo].[adColleges] ([CollegeCode], [CollegeName]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adColleges] ADD CONSTRAINT [FK_adColleges_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[adColleges] ADD CONSTRAINT [FK_adColleges_adCountries_CountryId_CountryId] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[adCountries] ([CountryId])
GO
ALTER TABLE [dbo].[adColleges] ADD CONSTRAINT [FK_adColleges_syStates_StateId_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[syStates] ([StateId])
GO
ALTER TABLE [dbo].[adColleges] ADD CONSTRAINT [FK_adColleges_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
