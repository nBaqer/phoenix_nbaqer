CREATE TABLE [dbo].[arBuildings]
(
[BldgId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arBuildings_BldgId] DEFAULT (newid()),
[BldgCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_arBuildings_StatusId] DEFAULT (newid()),
[BldgRooms] [tinyint] NOT NULL,
[BldgDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[Address1] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateId] [uniqueidentifier] NULL,
[Phone] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fax] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BldgOpen] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BldgClose] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Sun] [bit] NULL,
[Mon] [bit] NULL,
[Tue] [bit] NULL,
[Wed] [bit] NULL,
[Thu] [bit] NULL,
[Fri] [bit] NULL,
[Sat] [bit] NULL,
[BldgName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BldgTitle] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BldgEmail] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BldgComments] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Zip] [char] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[ForeignPhone] [bit] NULL,
[ForeignFax] [bit] NULL,
[ForeignZip] [bit] NULL,
[OtherState] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[arBuildings_Audit_Delete] ON [dbo].[arBuildings]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arBuildings','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BldgId
                               ,'StateId'
                               ,CONVERT(VARCHAR(8000),Old.StateId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BldgId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BldgId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BldgId
                               ,'Fax'
                               ,CONVERT(VARCHAR(8000),Old.Fax,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BldgId
                               ,'BldgTitle'
                               ,CONVERT(VARCHAR(8000),Old.BldgTitle,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BldgId
                               ,'OtherState'
                               ,CONVERT(VARCHAR(8000),Old.OtherState,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BldgId
                               ,'BldgComments'
                               ,CONVERT(VARCHAR(8000),Old.BldgComments,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BldgId
                               ,'Zip'
                               ,CONVERT(VARCHAR(8000),Old.Zip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BldgId
                               ,'City'
                               ,CONVERT(VARCHAR(8000),Old.City,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BldgId
                               ,'BldgCode'
                               ,CONVERT(VARCHAR(8000),Old.BldgCode,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BldgId
                               ,'Phone'
                               ,CONVERT(VARCHAR(8000),Old.Phone,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BldgId
                               ,'BldgEmail'
                               ,CONVERT(VARCHAR(8000),Old.BldgEmail,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BldgId
                               ,'BldgOpen'
                               ,CONVERT(VARCHAR(8000),Old.BldgOpen,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BldgId
                               ,'BldgClose'
                               ,CONVERT(VARCHAR(8000),Old.BldgClose,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BldgId
                               ,'BldgName'
                               ,CONVERT(VARCHAR(8000),Old.BldgName,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BldgId
                               ,'BldgDescrip'
                               ,CONVERT(VARCHAR(8000),Old.BldgDescrip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BldgId
                               ,'Address1'
                               ,CONVERT(VARCHAR(8000),Old.Address1,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BldgId
                               ,'Address2'
                               ,CONVERT(VARCHAR(8000),Old.Address2,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BldgId
                               ,'BldgRooms'
                               ,CONVERT(VARCHAR(8000),Old.BldgRooms,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BldgId
                               ,'ForeignPhone'
                               ,CONVERT(VARCHAR(8000),Old.ForeignPhone,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BldgId
                               ,'Mon'
                               ,CONVERT(VARCHAR(8000),Old.Mon,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BldgId
                               ,'ForeignFax'
                               ,CONVERT(VARCHAR(8000),Old.ForeignFax,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BldgId
                               ,'ForeignZip'
                               ,CONVERT(VARCHAR(8000),Old.ForeignZip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BldgId
                               ,'Sat'
                               ,CONVERT(VARCHAR(8000),Old.Sat,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BldgId
                               ,'Tue'
                               ,CONVERT(VARCHAR(8000),Old.Tue,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BldgId
                               ,'Wed'
                               ,CONVERT(VARCHAR(8000),Old.Wed,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BldgId
                               ,'Sun'
                               ,CONVERT(VARCHAR(8000),Old.Sun,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BldgId
                               ,'Thu'
                               ,CONVERT(VARCHAR(8000),Old.Thu,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BldgId
                               ,'Fri'
                               ,CONVERT(VARCHAR(8000),Old.Fri,121)
                        FROM    Deleted Old; 
            END; 
        END;



    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[arBuildings_Audit_Insert] ON [dbo].[arBuildings]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arBuildings','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BldgId
                               ,'StateId'
                               ,CONVERT(VARCHAR(8000),New.StateId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BldgId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BldgId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BldgId
                               ,'Fax'
                               ,CONVERT(VARCHAR(8000),New.Fax,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BldgId
                               ,'BldgTitle'
                               ,CONVERT(VARCHAR(8000),New.BldgTitle,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BldgId
                               ,'OtherState'
                               ,CONVERT(VARCHAR(8000),New.OtherState,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BldgId
                               ,'BldgComments'
                               ,CONVERT(VARCHAR(8000),New.BldgComments,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BldgId
                               ,'Zip'
                               ,CONVERT(VARCHAR(8000),New.Zip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BldgId
                               ,'City'
                               ,CONVERT(VARCHAR(8000),New.City,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BldgId
                               ,'BldgCode'
                               ,CONVERT(VARCHAR(8000),New.BldgCode,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BldgId
                               ,'Phone'
                               ,CONVERT(VARCHAR(8000),New.Phone,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BldgId
                               ,'BldgEmail'
                               ,CONVERT(VARCHAR(8000),New.BldgEmail,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BldgId
                               ,'BldgOpen'
                               ,CONVERT(VARCHAR(8000),New.BldgOpen,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BldgId
                               ,'BldgClose'
                               ,CONVERT(VARCHAR(8000),New.BldgClose,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BldgId
                               ,'BldgName'
                               ,CONVERT(VARCHAR(8000),New.BldgName,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BldgId
                               ,'BldgDescrip'
                               ,CONVERT(VARCHAR(8000),New.BldgDescrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BldgId
                               ,'Address1'
                               ,CONVERT(VARCHAR(8000),New.Address1,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BldgId
                               ,'Address2'
                               ,CONVERT(VARCHAR(8000),New.Address2,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BldgId
                               ,'BldgRooms'
                               ,CONVERT(VARCHAR(8000),New.BldgRooms,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BldgId
                               ,'ForeignPhone'
                               ,CONVERT(VARCHAR(8000),New.ForeignPhone,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BldgId
                               ,'Mon'
                               ,CONVERT(VARCHAR(8000),New.Mon,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BldgId
                               ,'ForeignFax'
                               ,CONVERT(VARCHAR(8000),New.ForeignFax,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BldgId
                               ,'ForeignZip'
                               ,CONVERT(VARCHAR(8000),New.ForeignZip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BldgId
                               ,'Sat'
                               ,CONVERT(VARCHAR(8000),New.Sat,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BldgId
                               ,'Tue'
                               ,CONVERT(VARCHAR(8000),New.Tue,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BldgId
                               ,'Wed'
                               ,CONVERT(VARCHAR(8000),New.Wed,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BldgId
                               ,'Sun'
                               ,CONVERT(VARCHAR(8000),New.Sun,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BldgId
                               ,'Thu'
                               ,CONVERT(VARCHAR(8000),New.Thu,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BldgId
                               ,'Fri'
                               ,CONVERT(VARCHAR(8000),New.Fri,121)
                        FROM    Inserted New; 
            END; 
        END;



    SET NOCOUNT OFF; 
GO
GO

GO

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[arBuildings_Audit_Update] ON [dbo].[arBuildings]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arBuildings','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(StateId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BldgId
                                   ,'StateId'
                                   ,CONVERT(VARCHAR(8000),Old.StateId,121)
                                   ,CONVERT(VARCHAR(8000),New.StateId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BldgId = New.BldgId
                            WHERE   Old.StateId <> New.StateId
                                    OR (
                                         Old.StateId IS NULL
                                         AND New.StateId IS NOT NULL
                                       )
                                    OR (
                                         New.StateId IS NULL
                                         AND Old.StateId IS NOT NULL
                                       ); 
                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BldgId
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BldgId = New.BldgId
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BldgId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BldgId = New.BldgId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(Fax)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BldgId
                                   ,'Fax'
                                   ,CONVERT(VARCHAR(8000),Old.Fax,121)
                                   ,CONVERT(VARCHAR(8000),New.Fax,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BldgId = New.BldgId
                            WHERE   Old.Fax <> New.Fax
                                    OR (
                                         Old.Fax IS NULL
                                         AND New.Fax IS NOT NULL
                                       )
                                    OR (
                                         New.Fax IS NULL
                                         AND Old.Fax IS NOT NULL
                                       ); 
                IF UPDATE(BldgTitle)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BldgId
                                   ,'BldgTitle'
                                   ,CONVERT(VARCHAR(8000),Old.BldgTitle,121)
                                   ,CONVERT(VARCHAR(8000),New.BldgTitle,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BldgId = New.BldgId
                            WHERE   Old.BldgTitle <> New.BldgTitle
                                    OR (
                                         Old.BldgTitle IS NULL
                                         AND New.BldgTitle IS NOT NULL
                                       )
                                    OR (
                                         New.BldgTitle IS NULL
                                         AND Old.BldgTitle IS NOT NULL
                                       ); 
                IF UPDATE(OtherState)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BldgId
                                   ,'OtherState'
                                   ,CONVERT(VARCHAR(8000),Old.OtherState,121)
                                   ,CONVERT(VARCHAR(8000),New.OtherState,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BldgId = New.BldgId
                            WHERE   Old.OtherState <> New.OtherState
                                    OR (
                                         Old.OtherState IS NULL
                                         AND New.OtherState IS NOT NULL
                                       )
                                    OR (
                                         New.OtherState IS NULL
                                         AND Old.OtherState IS NOT NULL
                                       ); 
                IF UPDATE(BldgComments)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BldgId
                                   ,'BldgComments'
                                   ,CONVERT(VARCHAR(8000),Old.BldgComments,121)
                                   ,CONVERT(VARCHAR(8000),New.BldgComments,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BldgId = New.BldgId
                            WHERE   Old.BldgComments <> New.BldgComments
                                    OR (
                                         Old.BldgComments IS NULL
                                         AND New.BldgComments IS NOT NULL
                                       )
                                    OR (
                                         New.BldgComments IS NULL
                                         AND Old.BldgComments IS NOT NULL
                                       ); 
                IF UPDATE(Zip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BldgId
                                   ,'Zip'
                                   ,CONVERT(VARCHAR(8000),Old.Zip,121)
                                   ,CONVERT(VARCHAR(8000),New.Zip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BldgId = New.BldgId
                            WHERE   Old.Zip <> New.Zip
                                    OR (
                                         Old.Zip IS NULL
                                         AND New.Zip IS NOT NULL
                                       )
                                    OR (
                                         New.Zip IS NULL
                                         AND Old.Zip IS NOT NULL
                                       ); 
                IF UPDATE(City)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BldgId
                                   ,'City'
                                   ,CONVERT(VARCHAR(8000),Old.City,121)
                                   ,CONVERT(VARCHAR(8000),New.City,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BldgId = New.BldgId
                            WHERE   Old.City <> New.City
                                    OR (
                                         Old.City IS NULL
                                         AND New.City IS NOT NULL
                                       )
                                    OR (
                                         New.City IS NULL
                                         AND Old.City IS NOT NULL
                                       ); 
                IF UPDATE(BldgCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BldgId
                                   ,'BldgCode'
                                   ,CONVERT(VARCHAR(8000),Old.BldgCode,121)
                                   ,CONVERT(VARCHAR(8000),New.BldgCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BldgId = New.BldgId
                            WHERE   Old.BldgCode <> New.BldgCode
                                    OR (
                                         Old.BldgCode IS NULL
                                         AND New.BldgCode IS NOT NULL
                                       )
                                    OR (
                                         New.BldgCode IS NULL
                                         AND Old.BldgCode IS NOT NULL
                                       ); 
                IF UPDATE(Phone)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BldgId
                                   ,'Phone'
                                   ,CONVERT(VARCHAR(8000),Old.Phone,121)
                                   ,CONVERT(VARCHAR(8000),New.Phone,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BldgId = New.BldgId
                            WHERE   Old.Phone <> New.Phone
                                    OR (
                                         Old.Phone IS NULL
                                         AND New.Phone IS NOT NULL
                                       )
                                    OR (
                                         New.Phone IS NULL
                                         AND Old.Phone IS NOT NULL
                                       ); 
                IF UPDATE(BldgEmail)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BldgId
                                   ,'BldgEmail'
                                   ,CONVERT(VARCHAR(8000),Old.BldgEmail,121)
                                   ,CONVERT(VARCHAR(8000),New.BldgEmail,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BldgId = New.BldgId
                            WHERE   Old.BldgEmail <> New.BldgEmail
                                    OR (
                                         Old.BldgEmail IS NULL
                                         AND New.BldgEmail IS NOT NULL
                                       )
                                    OR (
                                         New.BldgEmail IS NULL
                                         AND Old.BldgEmail IS NOT NULL
                                       ); 
                IF UPDATE(BldgOpen)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BldgId
                                   ,'BldgOpen'
                                   ,CONVERT(VARCHAR(8000),Old.BldgOpen,121)
                                   ,CONVERT(VARCHAR(8000),New.BldgOpen,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BldgId = New.BldgId
                            WHERE   Old.BldgOpen <> New.BldgOpen
                                    OR (
                                         Old.BldgOpen IS NULL
                                         AND New.BldgOpen IS NOT NULL
                                       )
                                    OR (
                                         New.BldgOpen IS NULL
                                         AND Old.BldgOpen IS NOT NULL
                                       ); 
                IF UPDATE(BldgClose)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BldgId
                                   ,'BldgClose'
                                   ,CONVERT(VARCHAR(8000),Old.BldgClose,121)
                                   ,CONVERT(VARCHAR(8000),New.BldgClose,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BldgId = New.BldgId
                            WHERE   Old.BldgClose <> New.BldgClose
                                    OR (
                                         Old.BldgClose IS NULL
                                         AND New.BldgClose IS NOT NULL
                                       )
                                    OR (
                                         New.BldgClose IS NULL
                                         AND Old.BldgClose IS NOT NULL
                                       ); 
                IF UPDATE(BldgName)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BldgId
                                   ,'BldgName'
                                   ,CONVERT(VARCHAR(8000),Old.BldgName,121)
                                   ,CONVERT(VARCHAR(8000),New.BldgName,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BldgId = New.BldgId
                            WHERE   Old.BldgName <> New.BldgName
                                    OR (
                                         Old.BldgName IS NULL
                                         AND New.BldgName IS NOT NULL
                                       )
                                    OR (
                                         New.BldgName IS NULL
                                         AND Old.BldgName IS NOT NULL
                                       ); 
                IF UPDATE(BldgDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BldgId
                                   ,'BldgDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.BldgDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.BldgDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BldgId = New.BldgId
                            WHERE   Old.BldgDescrip <> New.BldgDescrip
                                    OR (
                                         Old.BldgDescrip IS NULL
                                         AND New.BldgDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.BldgDescrip IS NULL
                                         AND Old.BldgDescrip IS NOT NULL
                                       ); 
                IF UPDATE(Address1)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BldgId
                                   ,'Address1'
                                   ,CONVERT(VARCHAR(8000),Old.Address1,121)
                                   ,CONVERT(VARCHAR(8000),New.Address1,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BldgId = New.BldgId
                            WHERE   Old.Address1 <> New.Address1
                                    OR (
                                         Old.Address1 IS NULL
                                         AND New.Address1 IS NOT NULL
                                       )
                                    OR (
                                         New.Address1 IS NULL
                                         AND Old.Address1 IS NOT NULL
                                       ); 
                IF UPDATE(Address2)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BldgId
                                   ,'Address2'
                                   ,CONVERT(VARCHAR(8000),Old.Address2,121)
                                   ,CONVERT(VARCHAR(8000),New.Address2,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BldgId = New.BldgId
                            WHERE   Old.Address2 <> New.Address2
                                    OR (
                                         Old.Address2 IS NULL
                                         AND New.Address2 IS NOT NULL
                                       )
                                    OR (
                                         New.Address2 IS NULL
                                         AND Old.Address2 IS NOT NULL
                                       ); 
                IF UPDATE(BldgRooms)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BldgId
                                   ,'BldgRooms'
                                   ,CONVERT(VARCHAR(8000),Old.BldgRooms,121)
                                   ,CONVERT(VARCHAR(8000),New.BldgRooms,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BldgId = New.BldgId
                            WHERE   Old.BldgRooms <> New.BldgRooms
                                    OR (
                                         Old.BldgRooms IS NULL
                                         AND New.BldgRooms IS NOT NULL
                                       )
                                    OR (
                                         New.BldgRooms IS NULL
                                         AND Old.BldgRooms IS NOT NULL
                                       ); 
                IF UPDATE(ForeignPhone)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BldgId
                                   ,'ForeignPhone'
                                   ,CONVERT(VARCHAR(8000),Old.ForeignPhone,121)
                                   ,CONVERT(VARCHAR(8000),New.ForeignPhone,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BldgId = New.BldgId
                            WHERE   Old.ForeignPhone <> New.ForeignPhone
                                    OR (
                                         Old.ForeignPhone IS NULL
                                         AND New.ForeignPhone IS NOT NULL
                                       )
                                    OR (
                                         New.ForeignPhone IS NULL
                                         AND Old.ForeignPhone IS NOT NULL
                                       ); 
                IF UPDATE(Mon)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BldgId
                                   ,'Mon'
                                   ,CONVERT(VARCHAR(8000),Old.Mon,121)
                                   ,CONVERT(VARCHAR(8000),New.Mon,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BldgId = New.BldgId
                            WHERE   Old.Mon <> New.Mon
                                    OR (
                                         Old.Mon IS NULL
                                         AND New.Mon IS NOT NULL
                                       )
                                    OR (
                                         New.Mon IS NULL
                                         AND Old.Mon IS NOT NULL
                                       ); 
                IF UPDATE(ForeignFax)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BldgId
                                   ,'ForeignFax'
                                   ,CONVERT(VARCHAR(8000),Old.ForeignFax,121)
                                   ,CONVERT(VARCHAR(8000),New.ForeignFax,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BldgId = New.BldgId
                            WHERE   Old.ForeignFax <> New.ForeignFax
                                    OR (
                                         Old.ForeignFax IS NULL
                                         AND New.ForeignFax IS NOT NULL
                                       )
                                    OR (
                                         New.ForeignFax IS NULL
                                         AND Old.ForeignFax IS NOT NULL
                                       ); 
                IF UPDATE(ForeignZip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BldgId
                                   ,'ForeignZip'
                                   ,CONVERT(VARCHAR(8000),Old.ForeignZip,121)
                                   ,CONVERT(VARCHAR(8000),New.ForeignZip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BldgId = New.BldgId
                            WHERE   Old.ForeignZip <> New.ForeignZip
                                    OR (
                                         Old.ForeignZip IS NULL
                                         AND New.ForeignZip IS NOT NULL
                                       )
                                    OR (
                                         New.ForeignZip IS NULL
                                         AND Old.ForeignZip IS NOT NULL
                                       ); 
                IF UPDATE(Sat)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BldgId
                                   ,'Sat'
                                   ,CONVERT(VARCHAR(8000),Old.Sat,121)
                                   ,CONVERT(VARCHAR(8000),New.Sat,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BldgId = New.BldgId
                            WHERE   Old.Sat <> New.Sat
                                    OR (
                                         Old.Sat IS NULL
                                         AND New.Sat IS NOT NULL
                                       )
                                    OR (
                                         New.Sat IS NULL
                                         AND Old.Sat IS NOT NULL
                                       ); 
                IF UPDATE(Tue)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BldgId
                                   ,'Tue'
                                   ,CONVERT(VARCHAR(8000),Old.Tue,121)
                                   ,CONVERT(VARCHAR(8000),New.Tue,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BldgId = New.BldgId
                            WHERE   Old.Tue <> New.Tue
                                    OR (
                                         Old.Tue IS NULL
                                         AND New.Tue IS NOT NULL
                                       )
                                    OR (
                                         New.Tue IS NULL
                                         AND Old.Tue IS NOT NULL
                                       ); 
                IF UPDATE(Wed)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BldgId
                                   ,'Wed'
                                   ,CONVERT(VARCHAR(8000),Old.Wed,121)
                                   ,CONVERT(VARCHAR(8000),New.Wed,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BldgId = New.BldgId
                            WHERE   Old.Wed <> New.Wed
                                    OR (
                                         Old.Wed IS NULL
                                         AND New.Wed IS NOT NULL
                                       )
                                    OR (
                                         New.Wed IS NULL
                                         AND Old.Wed IS NOT NULL
                                       ); 
                IF UPDATE(Sun)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BldgId
                                   ,'Sun'
                                   ,CONVERT(VARCHAR(8000),Old.Sun,121)
                                   ,CONVERT(VARCHAR(8000),New.Sun,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BldgId = New.BldgId
                            WHERE   Old.Sun <> New.Sun
                                    OR (
                                         Old.Sun IS NULL
                                         AND New.Sun IS NOT NULL
                                       )
                                    OR (
                                         New.Sun IS NULL
                                         AND Old.Sun IS NOT NULL
                                       ); 
                IF UPDATE(Thu)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BldgId
                                   ,'Thu'
                                   ,CONVERT(VARCHAR(8000),Old.Thu,121)
                                   ,CONVERT(VARCHAR(8000),New.Thu,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BldgId = New.BldgId
                            WHERE   Old.Thu <> New.Thu
                                    OR (
                                         Old.Thu IS NULL
                                         AND New.Thu IS NOT NULL
                                       )
                                    OR (
                                         New.Thu IS NULL
                                         AND Old.Thu IS NOT NULL
                                       ); 
                IF UPDATE(Fri)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BldgId
                                   ,'Fri'
                                   ,CONVERT(VARCHAR(8000),Old.Fri,121)
                                   ,CONVERT(VARCHAR(8000),New.Fri,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BldgId = New.BldgId
                            WHERE   Old.Fri <> New.Fri
                                    OR (
                                         Old.Fri IS NULL
                                         AND New.Fri IS NOT NULL
                                       )
                                    OR (
                                         New.Fri IS NULL
                                         AND Old.Fri IS NOT NULL
                                       ); 
            END; 
        END;



    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[arBuildings] ADD CONSTRAINT [PK_arBuildings_BldgId] PRIMARY KEY CLUSTERED  ([BldgId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_arBuildings_BldgCode_BldgDescrip_CampGrpId] ON [dbo].[arBuildings] ([BldgCode], [BldgDescrip], [CampGrpId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arBuildings] ADD CONSTRAINT [FK_arBuildings_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[arBuildings] ADD CONSTRAINT [FK_arBuildings_syCampuses_CampusId_CampusId] FOREIGN KEY ([CampusId]) REFERENCES [dbo].[syCampuses] ([CampusId])
GO
ALTER TABLE [dbo].[arBuildings] ADD CONSTRAINT [FK_arBuildings_syStates_StateId_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[syStates] ([StateId])
GO
ALTER TABLE [dbo].[arBuildings] ADD CONSTRAINT [FK_arBuildings_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
