CREATE TABLE [dbo].[syEntities]
(
[EntityId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syEntities_EntityId] DEFAULT (newid()),
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syEntities] ADD CONSTRAINT [PK_syEntities_EntityId] PRIMARY KEY CLUSTERED  ([EntityId]) ON [PRIMARY]
GO
