CREATE TABLE [dbo].[adGenders]
(
[GenderId] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[GenderCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[GenderDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[IPEDSSequence] [int] NULL,
[IPEDSValue] [int] NULL,
[AfaMappingId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adGenders] ADD CONSTRAINT [PK_adGenders_GenderId] PRIMARY KEY CLUSTERED  ([GenderId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adGenders] ADD CONSTRAINT [FK_adGenders_AfaCatalogMapping_AfaMappingId_Id] FOREIGN KEY ([AfaMappingId]) REFERENCES [dbo].[AfaCatalogMapping] ([Id])
GO
ALTER TABLE [dbo].[adGenders] ADD CONSTRAINT [FK_adGenders_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[adGenders] ADD CONSTRAINT [FK_adGenders_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
