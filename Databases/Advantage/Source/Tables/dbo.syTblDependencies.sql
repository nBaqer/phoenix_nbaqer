CREATE TABLE [dbo].[syTblDependencies]
(
[table1] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[table2] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[depTable] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TableDependenciesId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_syTblDependencies_TableDependenciesId] DEFAULT (newsequentialid())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syTblDependencies] ADD CONSTRAINT [PK_syTblDependencies_TableDependenciesId] PRIMARY KEY CLUSTERED  ([TableDependenciesId]) ON [PRIMARY]
GO
