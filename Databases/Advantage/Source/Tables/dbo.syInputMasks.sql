CREATE TABLE [dbo].[syInputMasks]
(
[InputMaskId] [tinyint] NOT NULL,
[Item] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Mask] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syInputMasks] ADD CONSTRAINT [PK_syInputMasks_InputMaskId] PRIMARY KEY CLUSTERED  ([InputMaskId]) ON [PRIMARY]
GO
GRANT REFERENCES ON  [dbo].[syInputMasks] TO [AdvantageRole]
GRANT SELECT ON  [dbo].[syInputMasks] TO [AdvantageRole]
GRANT INSERT ON  [dbo].[syInputMasks] TO [AdvantageRole]
GRANT DELETE ON  [dbo].[syInputMasks] TO [AdvantageRole]
GRANT UPDATE ON  [dbo].[syInputMasks] TO [AdvantageRole]
GO
