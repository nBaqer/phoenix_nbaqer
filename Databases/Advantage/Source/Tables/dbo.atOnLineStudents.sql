CREATE TABLE [dbo].[atOnLineStudents]
(
[StuEnrollId] [uniqueidentifier] NOT NULL,
[OnLineStudentId] [int] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[atOnLineStudents] ADD CONSTRAINT [PK_atOnLineStudents_StuEnrollId] PRIMARY KEY CLUSTERED  ([StuEnrollId]) ON [PRIMARY]
GO
