CREATE TABLE [dbo].[arSAPChkResults]
(
[StdRecKey] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arSAPChkResults_StdRecKey] DEFAULT (newid()),
[StuEnrollId] [uniqueidentifier] NOT NULL,
[Period] [int] NOT NULL,
[IsMakingSAP] [bit] NOT NULL,
[HrsAttended] [decimal] (19, 2) NULL,
[HrsEarned] [decimal] (19, 2) NULL,
[CredsAttempted] [decimal] (18, 2) NULL,
[CredsEarned] [decimal] (18, 2) NULL,
[GPA] [decimal] (18, 2) NULL,
[DatePerformed] [datetime] NOT NULL,
[Comments] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[SAPDetailId] [uniqueidentifier] NULL CONSTRAINT [DF_arSAPChkResults_SAPDetailId] DEFAULT (newid()),
[Average] [decimal] (18, 2) NULL,
[Attendance] [decimal] (19, 2) NULL,
[CumFinAidCredits] [decimal] (19, 2) NULL,
[PercentCompleted] [decimal] (19, 2) NULL,
[CheckPointDate] [datetime] NULL,
[TermStartDate] [datetime] NULL,
[PreviewSapCheck] [bit] NOT NULL CONSTRAINT [DF_arSAPChkResults_PreviewSapCheck] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[arSAPChkResults_Audit_Delete] ON [dbo].[arSAPChkResults]
    FOR DELETE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arSAPChkResults','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StdRecKey
                               ,'HrsAttended'
                               ,CONVERT(VARCHAR(8000),Old.HrsAttended,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StdRecKey
                               ,'CredsEarned'
                               ,CONVERT(VARCHAR(8000),Old.CredsEarned,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StdRecKey
                               ,'Period'
                               ,CONVERT(VARCHAR(8000),Old.Period,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StdRecKey
                               ,'HrsEarned'
                               ,CONVERT(VARCHAR(8000),Old.HrsEarned,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StdRecKey
                               ,'CredsAttempted'
                               ,CONVERT(VARCHAR(8000),Old.CredsAttempted,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StdRecKey
                               ,'StuEnrollId'
                               ,CONVERT(VARCHAR(8000),Old.StuEnrollId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StdRecKey
                               ,'DatePerformed'
                               ,CONVERT(VARCHAR(8000),Old.DatePerformed,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StdRecKey
                               ,'Comments'
                               ,CONVERT(VARCHAR(8000),Old.Comments,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StdRecKey
                               ,'IsMakingSAP'
                               ,CONVERT(VARCHAR(8000),Old.IsMakingSAP,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StdRecKey
                               ,'GPA'
                               ,CONVERT(VARCHAR(8000),Old.GPA,121)
                        FROM    Deleted Old; 
            END; 
        END;



    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[arSAPChkResults_Audit_Insert] ON [dbo].[arSAPChkResults]
    FOR INSERT
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arSAPChkResults','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StdRecKey
                               ,'HrsAttended'
                               ,CONVERT(VARCHAR(8000),New.HrsAttended,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StdRecKey
                               ,'CredsEarned'
                               ,CONVERT(VARCHAR(8000),New.CredsEarned,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StdRecKey
                               ,'Period'
                               ,CONVERT(VARCHAR(8000),New.Period,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StdRecKey
                               ,'HrsEarned'
                               ,CONVERT(VARCHAR(8000),New.HrsEarned,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StdRecKey
                               ,'CredsAttempted'
                               ,CONVERT(VARCHAR(8000),New.CredsAttempted,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StdRecKey
                               ,'StuEnrollId'
                               ,CONVERT(VARCHAR(8000),New.StuEnrollId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StdRecKey
                               ,'DatePerformed'
                               ,CONVERT(VARCHAR(8000),New.DatePerformed,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StdRecKey
                               ,'Comments'
                               ,CONVERT(VARCHAR(8000),New.Comments,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StdRecKey
                               ,'IsMakingSAP'
                               ,CONVERT(VARCHAR(8000),New.IsMakingSAP,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StdRecKey
                               ,'GPA'
                               ,CONVERT(VARCHAR(8000),New.GPA,121)
                        FROM    Inserted New; 
            END; 
        END;



    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
CREATE TRIGGER [dbo].[arSAPChkResults_Audit_Update] ON [dbo].[arSAPChkResults] 
    FOR UPDATE 
AS 
    SET NOCOUNT ON;  
 
    DECLARE @AuditHistId AS UNIQUEIDENTIFIER;  
    DECLARE @EventRows AS INT;  
    DECLARE @EventDate AS DATETIME;  
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();  
    SET @EventRows = ( 
                       SELECT   COUNT(*) 
                       FROM     Inserted 
                     );  
    SET @EventDate = ( 
                       SELECT TOP 1 
                                ModDate 
                       FROM     Inserted 
                     );  
    SET @UserName = ( 
                      SELECT TOP 1 
                                ModUser 
                      FROM      Inserted 
                    );  
    IF @EventRows > 0 
        BEGIN  
            EXEC fmAuditHistAdd @AuditHistId,'arSAPChkResults','U',@EventRows,@EventDate,@UserName;  
            BEGIN  
                IF UPDATE(HrsAttended) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.StdRecKey 
                                   ,'HrsAttended' 
                                   ,CONVERT(VARCHAR(8000),Old.HrsAttended,121) 
                                   ,CONVERT(VARCHAR(8000),New.HrsAttended,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.StdRecKey = New.StdRecKey 
                            WHERE   Old.HrsAttended <> New.HrsAttended 
                                    OR ( 
                                         Old.HrsAttended IS NULL 
                                         AND New.HrsAttended IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.HrsAttended IS NULL 
                                         AND Old.HrsAttended IS NOT NULL 
                                       );  
                IF UPDATE(CredsEarned) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.StdRecKey 
                                   ,'CredsEarned' 
                                   ,CONVERT(VARCHAR(8000),Old.CredsEarned,121) 
                                   ,CONVERT(VARCHAR(8000),New.CredsEarned,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.StdRecKey = New.StdRecKey 
                            WHERE   Old.CredsEarned <> New.CredsEarned 
                                    OR ( 
                                         Old.CredsEarned IS NULL 
                                         AND New.CredsEarned IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.CredsEarned IS NULL 
                                         AND Old.CredsEarned IS NOT NULL 
                                       );  
                IF UPDATE(Period) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.StdRecKey 
                                   ,'Period' 
                                   ,CONVERT(VARCHAR(8000),Old.Period,121) 
                                   ,CONVERT(VARCHAR(8000),New.Period,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.StdRecKey = New.StdRecKey 
                            WHERE   Old.Period <> New.Period 
                                    OR ( 
                                         Old.Period IS NULL 
                                         AND New.Period IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.Period IS NULL 
                                         AND Old.Period IS NOT NULL 
                                       );  
                IF UPDATE(HrsEarned) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.StdRecKey 
                                   ,'HrsEarned' 
                                   ,CONVERT(VARCHAR(8000),Old.HrsEarned,121) 
                                   ,CONVERT(VARCHAR(8000),New.HrsEarned,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.StdRecKey = New.StdRecKey 
                            WHERE   Old.HrsEarned <> New.HrsEarned 
                                    OR ( 
                                         Old.HrsEarned IS NULL 
                                         AND New.HrsEarned IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.HrsEarned IS NULL 
                                         AND Old.HrsEarned IS NOT NULL 
                                       );  
                IF UPDATE(CredsAttempted) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.StdRecKey 
                                   ,'CredsAttempted' 
                                   ,CONVERT(VARCHAR(8000),Old.CredsAttempted,121) 
                                   ,CONVERT(VARCHAR(8000),New.CredsAttempted,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.StdRecKey = New.StdRecKey 
                            WHERE   Old.CredsAttempted <> New.CredsAttempted 
                                    OR ( 
                                         Old.CredsAttempted IS NULL 
                                         AND New.CredsAttempted IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.CredsAttempted IS NULL 
                                         AND Old.CredsAttempted IS NOT NULL 
                                       );  
                IF UPDATE(StuEnrollId) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.StdRecKey 
                                   ,'StuEnrollId' 
                                   ,CONVERT(VARCHAR(8000),Old.StuEnrollId,121) 
                                   ,CONVERT(VARCHAR(8000),New.StuEnrollId,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.StdRecKey = New.StdRecKey 
                            WHERE   Old.StuEnrollId <> New.StuEnrollId 
                                    OR ( 
                                         Old.StuEnrollId IS NULL 
                                         AND New.StuEnrollId IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.StuEnrollId IS NULL 
                                         AND Old.StuEnrollId IS NOT NULL 
                                       );  
                IF UPDATE(DatePerformed) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.StdRecKey 
                                   ,'DatePerformed' 
                                   ,CONVERT(VARCHAR(8000),Old.DatePerformed,121) 
                                   ,CONVERT(VARCHAR(8000),New.DatePerformed,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.StdRecKey = New.StdRecKey 
                            WHERE   Old.DatePerformed <> New.DatePerformed 
                                    OR ( 
                                         Old.DatePerformed IS NULL 
                                         AND New.DatePerformed IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.DatePerformed IS NULL 
                                         AND Old.DatePerformed IS NOT NULL 
                                       );  
                IF UPDATE(comments) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.StdRecKey 
                                   ,'Comments' 
                                   ,CONVERT(VARCHAR(8000),Old.Comments,121) 
                                   ,CONVERT(VARCHAR(8000),New.Comments,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.StdRecKey = New.StdRecKey 
                            WHERE   Old.Comments <> New.Comments 
                                    OR ( 
                                         Old.Comments IS NULL 
                                         AND New.Comments IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.Comments IS NULL 
                                         AND Old.Comments IS NOT NULL 
                                       );  
                IF UPDATE(IsMakingSAP) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.StdRecKey 
                                   ,'IsMakingSAP' 
                                   ,CONVERT(VARCHAR(8000),Old.IsMakingSAP,121) 
                                   ,CONVERT(VARCHAR(8000),New.IsMakingSAP,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.StdRecKey = New.StdRecKey 
                            WHERE   Old.IsMakingSAP <> New.IsMakingSAP 
                                    OR ( 
                                         Old.IsMakingSAP IS NULL 
                                         AND New.IsMakingSAP IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.IsMakingSAP IS NULL 
                                         AND Old.IsMakingSAP IS NOT NULL 
                                       );  
                IF UPDATE(GPA) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.StdRecKey 
                                   ,'GPA' 
                                   ,CONVERT(VARCHAR(8000),Old.GPA,121) 
                                   ,CONVERT(VARCHAR(8000),New.GPA,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.StdRecKey = New.StdRecKey 
                            WHERE   Old.GPA <> New.GPA 
                                    OR ( 
                                         Old.GPA IS NULL 
                                         AND New.GPA IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.GPA IS NULL 
                                         AND Old.GPA IS NOT NULL 
                                       );  
            END;  
        END; 
 
 
 
    SET NOCOUNT OFF;  
GO
ALTER TABLE [dbo].[arSAPChkResults] ADD CONSTRAINT [PK_arSAPChkResults_StdRecKey] PRIMARY KEY NONCLUSTERED  ([StdRecKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arSAPChkResults] ADD CONSTRAINT [FK_arSAPChkResults_arSAPDetails_SAPDetailId_SAPDetailId] FOREIGN KEY ([SAPDetailId]) REFERENCES [dbo].[arSAPDetails] ([SAPDetailId])
GO
ALTER TABLE [dbo].[arSAPChkResults] ADD CONSTRAINT [FK_arSAPChkResults_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY ([StuEnrollId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
