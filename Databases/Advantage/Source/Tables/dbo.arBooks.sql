CREATE TABLE [dbo].[arBooks]
(
[BkId] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[BkTitle] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BkAuthor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CategoryId] [uniqueidentifier] NOT NULL,
[ISBN] [varchar] (16) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[arBooks_Audit_Delete] ON [dbo].[arBooks]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arBooks','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BkId
                               ,'BkTitle'
                               ,CONVERT(VARCHAR(8000),Old.BkTitle,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BkId
                               ,'BkAuthor'
                               ,CONVERT(VARCHAR(8000),Old.BkAuthor,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BkId
                               ,'CategoryId'
                               ,CONVERT(VARCHAR(8000),Old.CategoryId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BkId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BkId
                               ,'ISBN'
                               ,CONVERT(VARCHAR(8000),Old.ISBN,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BkId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[arBooks_Audit_Insert] ON [dbo].[arBooks]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arBooks','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BkId
                               ,'BkTitle'
                               ,CONVERT(VARCHAR(8000),New.BkTitle,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BkId
                               ,'BkAuthor'
                               ,CONVERT(VARCHAR(8000),New.BkAuthor,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BkId
                               ,'CategoryId'
                               ,CONVERT(VARCHAR(8000),New.CategoryId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BkId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BkId
                               ,'ISBN'
                               ,CONVERT(VARCHAR(8000),New.ISBN,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BkId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New; 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO
GO

GO

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[arBooks_Audit_Update] ON [dbo].[arBooks]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arBooks','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(BkTitle)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BkId
                                   ,'BkTitle'
                                   ,CONVERT(VARCHAR(8000),Old.BkTitle,121)
                                   ,CONVERT(VARCHAR(8000),New.BkTitle,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BkId = New.BkId
                            WHERE   Old.BkTitle <> New.BkTitle
                                    OR (
                                         Old.BkTitle IS NULL
                                         AND New.BkTitle IS NOT NULL
                                       )
                                    OR (
                                         New.BkTitle IS NULL
                                         AND Old.BkTitle IS NOT NULL
                                       ); 
                IF UPDATE(BkAuthor)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BkId
                                   ,'BkAuthor'
                                   ,CONVERT(VARCHAR(8000),Old.BkAuthor,121)
                                   ,CONVERT(VARCHAR(8000),New.BkAuthor,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BkId = New.BkId
                            WHERE   Old.BkAuthor <> New.BkAuthor
                                    OR (
                                         Old.BkAuthor IS NULL
                                         AND New.BkAuthor IS NOT NULL
                                       )
                                    OR (
                                         New.BkAuthor IS NULL
                                         AND Old.BkAuthor IS NOT NULL
                                       ); 
                IF UPDATE(CategoryId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BkId
                                   ,'CategoryId'
                                   ,CONVERT(VARCHAR(8000),Old.CategoryId,121)
                                   ,CONVERT(VARCHAR(8000),New.CategoryId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BkId = New.BkId
                            WHERE   Old.CategoryId <> New.CategoryId
                                    OR (
                                         Old.CategoryId IS NULL
                                         AND New.CategoryId IS NOT NULL
                                       )
                                    OR (
                                         New.CategoryId IS NULL
                                         AND Old.CategoryId IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BkId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BkId = New.BkId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(ISBN)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BkId
                                   ,'ISBN'
                                   ,CONVERT(VARCHAR(8000),Old.ISBN,121)
                                   ,CONVERT(VARCHAR(8000),New.ISBN,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BkId = New.BkId
                            WHERE   Old.ISBN <> New.ISBN
                                    OR (
                                         Old.ISBN IS NULL
                                         AND New.ISBN IS NOT NULL
                                       )
                                    OR (
                                         New.ISBN IS NULL
                                         AND Old.ISBN IS NOT NULL
                                       ); 
                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BkId
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BkId = New.BkId
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[arBooks] ADD CONSTRAINT [PK_arBooks_BkId] PRIMARY KEY CLUSTERED  ([BkId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arBooks] ADD CONSTRAINT [FK_arBooks_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[arBooks] ADD CONSTRAINT [FK_arBooks_arBkCategories_CategoryId_CategoryId] FOREIGN KEY ([CategoryId]) REFERENCES [dbo].[arBkCategories] ([CategoryId])
GO
ALTER TABLE [dbo].[arBooks] ADD CONSTRAINT [FK_arBooks_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
