CREATE TABLE [dbo].[_archive_arGrdBkResults]
(
[IdArchive] [bigint] NOT NULL IDENTITY(1, 1),
[Archive_Date] [datetime] NULL,
[Archive_User] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GrdBkResultId] [uniqueidentifier] NULL,
[ClsSectionId] [uniqueidentifier] NULL,
[InstrGrdBkWgtDetailId] [uniqueidentifier] NULL,
[Score] [decimal] (6, 2) NULL,
[Comments] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StuEnrollId] [uniqueidentifier] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[ResNum] [int] NULL,
[PostDate] [datetime] NULL,
[IsCompGraded] [bit] NULL,
[IsCourseCredited] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[_archive_arGrdBkResults] ADD CONSTRAINT [PK__archive_arGrdBkResults_IdArchive] PRIMARY KEY CLUSTERED  ([IdArchive]) ON [PRIMARY]
GO
