CREATE TABLE [dbo].[syMessageGroups]
(
[MessageGroupId] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[StatusId] [uniqueidentifier] NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[GroupDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MessageSchemaId] [uniqueidentifier] NOT NULL,
[XmlData] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NOT NULL CONSTRAINT [DF_syMessageGroups_ModDate] DEFAULT (((2005)-(1))-(1)),
[ModUser] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_syMessageGroups_ModUser] DEFAULT ('no user selected')
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[syMessageGroups] ADD CONSTRAINT [PK_syMessageGroups_MessageGroupId] PRIMARY KEY CLUSTERED  ([MessageGroupId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syMessageGroups] ADD CONSTRAINT [FK_syMessageGroups_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[syMessageGroups] ADD CONSTRAINT [FK_syMessageGroups_syMessageSchemas_MessageSchemaId_MessageSchemaId] FOREIGN KEY ([MessageSchemaId]) REFERENCES [dbo].[syMessageSchemas] ([MessageSchemaId])
GO
ALTER TABLE [dbo].[syMessageGroups] ADD CONSTRAINT [FK_syMessageGroups_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
