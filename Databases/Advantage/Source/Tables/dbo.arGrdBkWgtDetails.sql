CREATE TABLE [dbo].[arGrdBkWgtDetails]
(
[InstrGrdBkWgtDetailId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arGrdBkWgtDetails_InstrGrdBkWgtDetailId] DEFAULT (newid()),
[InstrGrdBkWgtId] [uniqueidentifier] NOT NULL,
[Code] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Weight] [decimal] (18, 2) NULL,
[Seq] [int] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[GrdComponentTypeId] [uniqueidentifier] NOT NULL,
[Parameter] [int] NULL,
[Number] [decimal] (18, 2) NULL,
[GrdPolicyId] [int] NULL,
[Required] [bit] NULL CONSTRAINT [DF_arGrdBkWgtDetails_Required] DEFAULT ((0)),
[MustPass] [bit] NULL CONSTRAINT [DF_arGrdBkWgtDetails_MustPass] DEFAULT ((0)),
[CreditsPerService] [decimal] (18, 2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arGrdBkWgtDetails] ADD CONSTRAINT [PK_arGrdBkWgtDetails_InstrGrdBkWgtDetailId] PRIMARY KEY CLUSTERED  ([InstrGrdBkWgtDetailId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_arGrdBkWgtDetails_GrdComponentTypeId] ON [dbo].[arGrdBkWgtDetails] ([GrdComponentTypeId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_arGrdBkWgtDetails_GrdComponentTypeId_Required_MustPass_InstrGrdBkWgtDetailId_Number] ON [dbo].[arGrdBkWgtDetails] ([GrdComponentTypeId], [Required], [MustPass]) INCLUDE ([InstrGrdBkWgtDetailId], [Number]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_arGrdBkWgtDetails_InstrGrdBkWgtId_InstrGrdBkWgtDetailId_GrdComponentTypeId_Number] ON [dbo].[arGrdBkWgtDetails] ([InstrGrdBkWgtId], [InstrGrdBkWgtDetailId], [GrdComponentTypeId]) INCLUDE ([Number]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arGrdBkWgtDetails] ADD CONSTRAINT [FK_arGrdBkWgtDetails_arGrdBkWeights_InstrGrdBkWgtId_InstrGrdBkWgtId] FOREIGN KEY ([InstrGrdBkWgtId]) REFERENCES [dbo].[arGrdBkWeights] ([InstrGrdBkWgtId])
GO
ALTER TABLE [dbo].[arGrdBkWgtDetails] ADD CONSTRAINT [FK_arGrdBkWgtDetails_arGrdComponentTypes_GrdComponentTypeId_GrdComponentTypeId] FOREIGN KEY ([GrdComponentTypeId]) REFERENCES [dbo].[arGrdComponentTypes] ([GrdComponentTypeId])
GO
ALTER TABLE [dbo].[arGrdBkWgtDetails] ADD CONSTRAINT [FK_arGrdBkWgtDetails_syGrdPolicies_GrdPolicyId_GrdPolicyId] FOREIGN KEY ([GrdPolicyId]) REFERENCES [dbo].[syGrdPolicies] ([GrdPolicyId])
GO
