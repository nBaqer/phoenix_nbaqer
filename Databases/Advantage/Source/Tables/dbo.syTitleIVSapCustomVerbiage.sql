CREATE TABLE [dbo].[syTitleIVSapCustomVerbiage]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[SapId] [uniqueidentifier] NOT NULL,
[TitleIVSapStatusId] [int] NOT NULL,
[Message] [nvarchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[CreatedById] [uniqueidentifier] NOT NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syTitleIVSapCustomVerbiage] ADD CONSTRAINT [PK_syTitleIVSapCustomVerbiage_Id] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syTitleIVSapCustomVerbiage] ADD CONSTRAINT [FK_syTitleIVSapCustomVerbiage_arSap_SapId_SAPId] FOREIGN KEY ([SapId]) REFERENCES [dbo].[arSAP] ([SAPId])
GO
ALTER TABLE [dbo].[syTitleIVSapCustomVerbiage] ADD CONSTRAINT [FK_syTitleIVSapCustomVerbiage_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
ALTER TABLE [dbo].[syTitleIVSapCustomVerbiage] ADD CONSTRAINT [FK_syTitleIVSapCustomVerbiage_syTitleIVSapStatus_TitleIVSapStatusId_Id] FOREIGN KEY ([TitleIVSapStatusId]) REFERENCES [dbo].[syTitleIVSapStatus] ([Id])
GO
ALTER TABLE [dbo].[syTitleIVSapCustomVerbiage] ADD CONSTRAINT [FK_syTitleIVSapCustomVerbiage_syUsers_CreatedById_UserId] FOREIGN KEY ([CreatedById]) REFERENCES [dbo].[syUsers] ([UserId])
GO
