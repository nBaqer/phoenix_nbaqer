CREATE TABLE [dbo].[adLeadOtherContactsAddreses]
(
[OtherContactsAddresesId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_adLeadOtherContactsAddreses_OtherContactsAddresesId] DEFAULT (newid()),
[OtherContactId] [uniqueidentifier] NOT NULL,
[LeadId] [uniqueidentifier] NOT NULL,
[AddressTypeId] [uniqueidentifier] NOT NULL,
[Address1] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Address2] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateId] [uniqueidentifier] NULL,
[ZipCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CountryId] [uniqueidentifier] NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[IsMailingAddress] [bit] NOT NULL CONSTRAINT [DF_adLeadOtherContactsAddreses_IsMailingAddress] DEFAULT ((0)),
[ModDate] [datetime] NOT NULL CONSTRAINT [DF_adLeadOtherContactsAddreses_ModDate] DEFAULT (getdate()),
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[State] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsInternational] [bit] NULL CONSTRAINT [DF_adLeadOtherContactsAddreses_IsInternational] DEFAULT ((0)),
[CountyId] [uniqueidentifier] NULL,
[County] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Country] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
--========================================================================================== 
-- TRIGGER adLeadOtherContactsAddreses_Audit_Delete 
-- DELETE  add Audit History when delete records in adLeadOtherContactsAddreses
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadOtherContactsAddreses_Audit_Delete] ON [dbo].[adLeadOtherContactsAddreses]
    FOR DELETE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Deleted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Deleted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Deleted
                        );
 
        IF @EventRows > 0
            BEGIN
                EXEC fmAuditHistAdd @AuditHistId,'adLeadOtherContactsAddreses','D',@EventRows,@EventDate,@UserName;
                -- OtherContactId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'OtherContactId'
                               ,CONVERT(VARCHAR(MAX),Old.OtherContactId)
                        FROM    Deleted AS Old;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),Old.LeadId)
                        FROM    Deleted AS Old;
                -- AddressTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'AddressTypeId'
                               ,CONVERT(VARCHAR(MAX),Old.AddressTypeId)
                        FROM    Deleted AS Old;
                -- Address1 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'Address1'
                               ,CONVERT(VARCHAR(MAX),Old.Address1)
                        FROM    Deleted AS Old;
                -- Address2 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'Address2'
                               ,CONVERT(VARCHAR(MAX),Old.Address2)
                        FROM    Deleted AS Old;
                -- City 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'City'
                               ,CONVERT(VARCHAR(MAX),Old.City)
                        FROM    Deleted AS Old;
                -- StateId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'StateId'
                               ,CONVERT(VARCHAR(MAX),Old.StateId)
                        FROM    Deleted AS Old;
                -- ZipCode 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'ZipCode'
                               ,CONVERT(VARCHAR(MAX),Old.ZipCode)
                        FROM    Deleted AS Old;
                -- CountryId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'CountryId'
                               ,CONVERT(VARCHAR(MAX),Old.CountryId)
                        FROM    Deleted AS Old;
                -- StatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(MAX),Old.StatusId)
                        FROM    Deleted AS Old;
                -- IsMailingAddress 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'IsMailingAddress'
                               ,CONVERT(VARCHAR(MAX),Old.IsMailingAddress)
                        FROM    Deleted AS Old;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                        FROM    Deleted AS Old;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),Old.ModUser)
                        FROM    Deleted AS Old;
                -- State 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'State'
                               ,CONVERT(VARCHAR(MAX),Old.State)
                        FROM    Deleted AS Old;
                -- IsInternational 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'IsInternational'
                               ,CONVERT(VARCHAR(MAX),Old.IsInternational)
                        FROM    Deleted AS Old;
                -- CountyId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'CountyId'
                               ,CONVERT(VARCHAR(MAX),Old.CountyId)
                        FROM    Deleted AS Old;
                -- County 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'County'
                               ,CONVERT(VARCHAR(MAX),Old.County)
                        FROM    Deleted AS Old;
                -- Country 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsAddresesId
                               ,'Country'
                               ,CONVERT(VARCHAR(MAX),Old.Country)
                        FROM    Deleted AS Old;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadOtherContactsAddreses_Audit_Delete 
--========================================================================================== 
 

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
--========================================================================================== 
-- TRIGGER adLeadOtherContactsAddreses_Audit_Insert 
-- INSERT  add Audit History when insert records in adLeadOtherContactsAddreses
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadOtherContactsAddreses_Audit_Insert] ON [dbo].[adLeadOtherContactsAddreses]
    FOR INSERT
AS
    BEGIN 
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadOtherContactsAddreses','I',@EventRows,@EventDate,@UserName;
                -- OtherContactId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'OtherContactId'
                               ,CONVERT(VARCHAR(MAX),New.OtherContactId)
                        FROM    Inserted AS New;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),New.LeadId)
                        FROM    Inserted AS New;
                -- AddressTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'AddressTypeId'
                               ,CONVERT(VARCHAR(MAX),New.AddressTypeId)
                        FROM    Inserted AS New;
                -- Address1 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'Address1'
                               ,CONVERT(VARCHAR(MAX),New.Address1)
                        FROM    Inserted AS New;
                -- Address2 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'Address2'
                               ,CONVERT(VARCHAR(MAX),New.Address2)
                        FROM    Inserted AS New;
                -- City 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'City'
                               ,CONVERT(VARCHAR(MAX),New.City)
                        FROM    Inserted AS New;
                -- StateId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'StateId'
                               ,CONVERT(VARCHAR(MAX),New.StateId)
                        FROM    Inserted AS New;
                -- ZipCode 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'ZipCode'
                               ,CONVERT(VARCHAR(MAX),New.ZipCode)
                        FROM    Inserted AS New;
                -- CountryId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'CountryId'
                               ,CONVERT(VARCHAR(MAX),New.CountryId)
                        FROM    Inserted AS New;
                -- StatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(MAX),New.StatusId)
                        FROM    Inserted AS New;
                -- IsMailingAddress 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'IsMailingAddress'
                               ,CONVERT(VARCHAR(MAX),New.IsMailingAddress)
                        FROM    Inserted AS New;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                        FROM    Inserted AS New;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),New.ModUser)
                        FROM    Inserted AS New;
                -- State 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'State'
                               ,CONVERT(VARCHAR(MAX),New.State)
                        FROM    Inserted AS New;
                -- IsInternational 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'IsInternational'
                               ,CONVERT(VARCHAR(MAX),New.IsInternational)
                        FROM    Inserted AS New;
                -- CountyId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'CountyId'
                               ,CONVERT(VARCHAR(MAX),New.CountyId)
                        FROM    Inserted AS New;
                -- County 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'County'
                               ,CONVERT(VARCHAR(MAX),New.County)
                        FROM    Inserted AS New;
                -- Country 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsAddresesId
                               ,'Country'
                               ,CONVERT(VARCHAR(MAX),New.Country)
                        FROM    Inserted AS New;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadOtherContactsAddreses_Audit_Insert 
--========================================================================================== 
 

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--========================================================================================== 
-- TRIGGER adLeadOtherContactsAddreses_Audit_Update 
-- UPDATE  add Audit History when update fields in adLeadOtherContactsAddreses
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadOtherContactsAddreses_Audit_Update] ON [dbo].[adLeadOtherContactsAddreses]
    FOR UPDATE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadOtherContactsAddreses','U',@EventRows,@EventDate,@UserName;
                -- OtherContactId 
                IF UPDATE(OtherContactId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'OtherContactId'
                                       ,CONVERT(VARCHAR(MAX),Old.OtherContactId)
                                       ,CONVERT(VARCHAR(MAX),New.OtherContactId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.OtherContactId <> New.OtherContactId
                                        OR (
                                             Old.OtherContactId IS NULL
                                             AND New.OtherContactId IS NOT NULL
                                           )
                                        OR (
                                             New.OtherContactId IS NULL
                                             AND Old.OtherContactId IS NOT NULL
                                           );
                    END;
                -- LeadId 
                IF UPDATE(LeadId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'LeadId'
                                       ,CONVERT(VARCHAR(MAX),Old.LeadId)
                                       ,CONVERT(VARCHAR(MAX),New.LeadId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.LeadId <> New.LeadId
                                        OR (
                                             Old.LeadId IS NULL
                                             AND New.LeadId IS NOT NULL
                                           )
                                        OR (
                                             New.LeadId IS NULL
                                             AND Old.LeadId IS NOT NULL
                                           );
                    END;
                -- AddressTypeId 
                IF UPDATE(AddressTypeId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'AddressTypeId'
                                       ,CONVERT(VARCHAR(MAX),Old.AddressTypeId)
                                       ,CONVERT(VARCHAR(MAX),New.AddressTypeId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.AddressTypeId <> New.AddressTypeId
                                        OR (
                                             Old.AddressTypeId IS NULL
                                             AND New.AddressTypeId IS NOT NULL
                                           )
                                        OR (
                                             New.AddressTypeId IS NULL
                                             AND Old.AddressTypeId IS NOT NULL
                                           );
                    END;
                -- Address1 
                IF UPDATE(Address1)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'Address1'
                                       ,CONVERT(VARCHAR(MAX),Old.Address1)
                                       ,CONVERT(VARCHAR(MAX),New.Address1)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.Address1 <> New.Address1
                                        OR (
                                             Old.Address1 IS NULL
                                             AND New.Address1 IS NOT NULL
                                           )
                                        OR (
                                             New.Address1 IS NULL
                                             AND Old.Address1 IS NOT NULL
                                           );
                    END;
                -- Address2 
                IF UPDATE(Address2)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'Address2'
                                       ,CONVERT(VARCHAR(MAX),Old.Address2)
                                       ,CONVERT(VARCHAR(MAX),New.Address2)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.Address2 <> New.Address2
                                        OR (
                                             Old.Address2 IS NULL
                                             AND New.Address2 IS NOT NULL
                                           )
                                        OR (
                                             New.Address2 IS NULL
                                             AND Old.Address2 IS NOT NULL
                                           );
                    END;
                -- City 
                IF UPDATE(City)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'City'
                                       ,CONVERT(VARCHAR(MAX),Old.City)
                                       ,CONVERT(VARCHAR(MAX),New.City)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.City <> New.City
                                        OR (
                                             Old.City IS NULL
                                             AND New.City IS NOT NULL
                                           )
                                        OR (
                                             New.City IS NULL
                                             AND Old.City IS NOT NULL
                                           );
                    END;
                -- StateId 
                IF UPDATE(StateId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'StateId'
                                       ,CONVERT(VARCHAR(MAX),Old.StateId)
                                       ,CONVERT(VARCHAR(MAX),New.StateId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.StateId <> New.StateId
                                        OR (
                                             Old.StateId IS NULL
                                             AND New.StateId IS NOT NULL
                                           )
                                        OR (
                                             New.StateId IS NULL
                                             AND Old.StateId IS NOT NULL
                                           );
                    END;
                -- ZipCode 
                IF UPDATE(ZipCode)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'ZipCode'
                                       ,CONVERT(VARCHAR(MAX),Old.ZipCode)
                                       ,CONVERT(VARCHAR(MAX),New.ZipCode)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.ZipCode <> New.ZipCode
                                        OR (
                                             Old.ZipCode IS NULL
                                             AND New.ZipCode IS NOT NULL
                                           )
                                        OR (
                                             New.ZipCode IS NULL
                                             AND Old.ZipCode IS NOT NULL
                                           );
                    END;
                -- CountryId 
                IF UPDATE(CountryId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'CountryId'
                                       ,CONVERT(VARCHAR(MAX),Old.CountryId)
                                       ,CONVERT(VARCHAR(MAX),New.CountryId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.CountryId <> New.CountryId
                                        OR (
                                             Old.CountryId IS NULL
                                             AND New.CountryId IS NOT NULL
                                           )
                                        OR (
                                             New.CountryId IS NULL
                                             AND Old.CountryId IS NOT NULL
                                           );
                    END;
                -- StatusId 
                IF UPDATE(StatusId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'StatusId'
                                       ,CONVERT(VARCHAR(MAX),Old.StatusId)
                                       ,CONVERT(VARCHAR(MAX),New.StatusId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.StatusId <> New.StatusId
                                        OR (
                                             Old.StatusId IS NULL
                                             AND New.StatusId IS NOT NULL
                                           )
                                        OR (
                                             New.StatusId IS NULL
                                             AND Old.StatusId IS NOT NULL
                                           );
                    END;
                -- IsMailingAddress 
                IF UPDATE(IsMailingAddress)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'IsMailingAddress'
                                       ,CONVERT(VARCHAR(MAX),Old.IsMailingAddress)
                                       ,CONVERT(VARCHAR(MAX),New.IsMailingAddress)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.IsMailingAddress <> New.IsMailingAddress
                                        OR (
                                             Old.IsMailingAddress IS NULL
                                             AND New.IsMailingAddress IS NOT NULL
                                           )
                                        OR (
                                             New.IsMailingAddress IS NULL
                                             AND Old.IsMailingAddress IS NOT NULL
                                           );
                    END;
                -- ModDate 
                IF UPDATE(ModDate)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'ModDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.ModDate <> New.ModDate
                                        OR (
                                             Old.ModDate IS NULL
                                             AND New.ModDate IS NOT NULL
                                           )
                                        OR (
                                             New.ModDate IS NULL
                                             AND Old.ModDate IS NOT NULL
                                           );
                    END;
                -- ModUser 
                IF UPDATE(ModUser)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'ModUser'
                                       ,CONVERT(VARCHAR(MAX),Old.ModUser)
                                       ,CONVERT(VARCHAR(MAX),New.ModUser)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.ModUser <> New.ModUser
                                        OR (
                                             Old.ModUser IS NULL
                                             AND New.ModUser IS NOT NULL
                                           )
                                        OR (
                                             New.ModUser IS NULL
                                             AND Old.ModUser IS NOT NULL
                                           );
                    END;
                -- State 
                IF UPDATE(State)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'State'
                                       ,CONVERT(VARCHAR(MAX),Old.State)
                                       ,CONVERT(VARCHAR(MAX),New.State)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.State <> New.State
                                        OR (
                                             Old.State IS NULL
                                             AND New.State IS NOT NULL
                                           )
                                        OR (
                                             New.State IS NULL
                                             AND Old.State IS NOT NULL
                                           );
                    END;
                -- IsInternational 
                IF UPDATE(IsInternational)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'IsInternational'
                                       ,CONVERT(VARCHAR(MAX),Old.IsInternational)
                                       ,CONVERT(VARCHAR(MAX),New.IsInternational)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.IsInternational <> New.IsInternational
                                        OR (
                                             Old.IsInternational IS NULL
                                             AND New.IsInternational IS NOT NULL
                                           )
                                        OR (
                                             New.IsInternational IS NULL
                                             AND Old.IsInternational IS NOT NULL
                                           );
                    END;
                -- CountyId 
                IF UPDATE(CountyId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'CountyId'
                                       ,CONVERT(VARCHAR(MAX),Old.CountyId)
                                       ,CONVERT(VARCHAR(MAX),New.CountyId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.CountyId <> New.CountyId
                                        OR (
                                             Old.CountyId IS NULL
                                             AND New.CountyId IS NOT NULL
                                           )
                                        OR (
                                             New.CountyId IS NULL
                                             AND Old.CountyId IS NOT NULL
                                           );
                    END;
                -- County 
                IF UPDATE(County)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'County'
                                       ,CONVERT(VARCHAR(MAX),Old.County)
                                       ,CONVERT(VARCHAR(MAX),New.County)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.County <> New.County
                                        OR (
                                             Old.County IS NULL
                                             AND New.County IS NOT NULL
                                           )
                                        OR (
                                             New.County IS NULL
                                             AND Old.County IS NOT NULL
                                           );
                    END;
                -- Country 
                IF UPDATE(Country)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsAddresesId
                                       ,'Country'
                                       ,CONVERT(VARCHAR(MAX),Old.Country)
                                       ,CONVERT(VARCHAR(MAX),New.Country)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsAddresesId = New.OtherContactsAddresesId
                                WHERE   Old.Country <> New.Country
                                        OR (
                                             Old.Country IS NULL
                                             AND New.Country IS NOT NULL
                                           )
                                        OR (
                                             New.Country IS NULL
                                             AND Old.Country IS NOT NULL
                                           );
                    END;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadOtherContactsAddreses_Audit_Update 
--========================================================================================== 
 

GO
ALTER TABLE [dbo].[adLeadOtherContactsAddreses] ADD CONSTRAINT [PK_adLeadOtherContactsAddreses_OtherContactsAddresesId] PRIMARY KEY CLUSTERED  ([OtherContactsAddresesId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adLeadOtherContactsAddreses] ADD CONSTRAINT [FK_adLeadOtherContactsAddreses_adCounties_CountyId_CountyId] FOREIGN KEY ([CountyId]) REFERENCES [dbo].[adCounties] ([CountyId])
GO
ALTER TABLE [dbo].[adLeadOtherContactsAddreses] ADD CONSTRAINT [FK_adLeadOtherContactsAddreses_adCountries_CountryId_CountryId] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[adCountries] ([CountryId])
GO
ALTER TABLE [dbo].[adLeadOtherContactsAddreses] ADD CONSTRAINT [FK_adLeadOtherContactsAddreses_adLeadOtherContacts_OtherContactId_OtherContactId] FOREIGN KEY ([OtherContactId]) REFERENCES [dbo].[adLeadOtherContacts] ([OtherContactId])
GO
ALTER TABLE [dbo].[adLeadOtherContactsAddreses] ADD CONSTRAINT [FK_adLeadOtherContactsAddreses_adLeads_LeadId_LeadId] FOREIGN KEY ([LeadId]) REFERENCES [dbo].[adLeads] ([LeadId])
GO
ALTER TABLE [dbo].[adLeadOtherContactsAddreses] ADD CONSTRAINT [FK_adLeadOtherContactsAddreses_plAddressTypes_AddressTypeId_AddressTypeId] FOREIGN KEY ([AddressTypeId]) REFERENCES [dbo].[plAddressTypes] ([AddressTypeId])
GO
ALTER TABLE [dbo].[adLeadOtherContactsAddreses] ADD CONSTRAINT [FK_adLeadOtherContactsAddreses_syStates_StateId_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[syStates] ([StateId])
GO
ALTER TABLE [dbo].[adLeadOtherContactsAddreses] ADD CONSTRAINT [FK_adLeadOtherContactsAddreses_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
