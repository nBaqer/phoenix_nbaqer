CREATE TABLE [dbo].[sySchedulingMethods]
(
[SchedMethodId] [int] NOT NULL,
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[sySchedulingMethods] ADD CONSTRAINT [PK_sySchedulingMethods_SchedMethodId] PRIMARY KEY CLUSTERED  ([SchedMethodId]) ON [PRIMARY]
GO
