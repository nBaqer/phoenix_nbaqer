CREATE TABLE [dbo].[syResCaptions]
(
[ResourceCapId] [int] NOT NULL,
[ResourceId] [int] NOT NULL,
[LangId] [tinyint] NOT NULL,
[Caption] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syResCaptions] ADD CONSTRAINT [PK_syResCaptions_ResourceCapId] PRIMARY KEY CLUSTERED  ([ResourceCapId]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[syResCaptions] TO [AdvantageRole]
GO
