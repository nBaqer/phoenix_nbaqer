CREATE TABLE [dbo].[tmUserTasks]
(
[UserTaskId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_tmUserTasks_UserTaskId] DEFAULT (newid()),
[TaskId] [uniqueidentifier] NOT NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[Priority] [smallint] NULL,
[Message] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReId] [uniqueidentifier] NULL,
[OwnerId] [uniqueidentifier] NULL,
[AssignedById] [uniqueidentifier] NULL,
[PrevUserTaskId] [uniqueidentifier] NULL,
[ResultId] [uniqueidentifier] NULL,
[Status] [smallint] NULL,
[ModDate] [datetime] NULL,
[ModUser] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tmUserTasks] ADD CONSTRAINT [PK_tmUserTasks_UserTaskId] PRIMARY KEY CLUSTERED  ([UserTaskId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tmUserTasks] ADD CONSTRAINT [FK_tmUserTasks_tmTasks_TaskId_TaskId] FOREIGN KEY ([TaskId]) REFERENCES [dbo].[tmTasks] ([TaskId])
GO
EXEC sp_addextendedproperty N'MS_Description', N'User id of the assigner (link to syUsers)', 'SCHEMA', N'dbo', 'TABLE', N'tmUserTasks', 'COLUMN', N'AssignedById'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Task end date', 'SCHEMA', N'dbo', 'TABLE', N'tmUserTasks', 'COLUMN', N'EndDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Text message associated with task', 'SCHEMA', N'dbo', 'TABLE', N'tmUserTasks', 'COLUMN', N'Message'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Creation date or last mod date', 'SCHEMA', N'dbo', 'TABLE', N'tmUserTasks', 'COLUMN', N'ModDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Owner (link to syUsers)', 'SCHEMA', N'dbo', 'TABLE', N'tmUserTasks', 'COLUMN', N'OwnerId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Numeric priority', 'SCHEMA', N'dbo', 'TABLE', N'tmUserTasks', 'COLUMN', N'Priority'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Id for which this task is regarding.', 'SCHEMA', N'dbo', 'TABLE', N'tmUserTasks', 'COLUMN', N'ReId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'link to tmTaskResults specifying the outcome of the task.', 'SCHEMA', N'dbo', 'TABLE', N'tmUserTasks', 'COLUMN', N'ResultId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Task start date', 'SCHEMA', N'dbo', 'TABLE', N'tmUserTasks', 'COLUMN', N'StartDate'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Specifies if complete, in progress or cancelled', 'SCHEMA', N'dbo', 'TABLE', N'tmUserTasks', 'COLUMN', N'Status'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Link to tmTasks', 'SCHEMA', N'dbo', 'TABLE', N'tmUserTasks', 'COLUMN', N'TaskId'
GO
