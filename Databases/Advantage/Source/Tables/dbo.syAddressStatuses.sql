CREATE TABLE [dbo].[syAddressStatuses]
(
[AddressStatusId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syAddressStatuses_AddressStatusId] DEFAULT (newid()),
[AddressStatusCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddressStatusDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusID] [uniqueidentifier] NULL,
[CampGrpID] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syAddressStatuses] ADD CONSTRAINT [PK_syAddressStatuses_AddressStatusId] PRIMARY KEY CLUSTERED  ([AddressStatusId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syAddressStatuses] ADD CONSTRAINT [FK_syAddressStatuses_syCampGrps_CampGrpID_CampGrpId] FOREIGN KEY ([CampGrpID]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[syAddressStatuses] ADD CONSTRAINT [FK_syAddressStatuses_syStatuses_StatusID_StatusId] FOREIGN KEY ([StatusID]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
