CREATE TABLE [dbo].[sySdfModVis]
(
[SdfModVisId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_sySdfModVis_SdfModVisId] DEFAULT (newid()),
[SDFId] [uniqueidentifier] NOT NULL,
[ModuleId] [tinyint] NOT NULL,
[VisId] [tinyint] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[sySdfModVis] ADD CONSTRAINT [PK_sySdfModVis_SdfModVisId] PRIMARY KEY CLUSTERED  ([SdfModVisId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[sySdfModVis] ADD CONSTRAINT [FK_sySdfModVis_sySDF_SDFId_SDFId] FOREIGN KEY ([SDFId]) REFERENCES [dbo].[sySDF] ([SDFId])
GO
