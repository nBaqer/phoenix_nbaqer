CREATE TABLE [dbo].[ParamDetail]
(
[DetailId] [bigint] NOT NULL IDENTITY(1, 1),
[SectionId] [bigint] NOT NULL,
[ItemId] [bigint] NOT NULL,
[CaptionOverride] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ItemSeq] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ParamDetail] ADD CONSTRAINT [PK_ParamDetail_DetailId] PRIMARY KEY CLUSTERED  ([DetailId]) ON [PRIMARY]
GO
