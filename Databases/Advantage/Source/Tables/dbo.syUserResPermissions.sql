CREATE TABLE [dbo].[syUserResPermissions]
(
[ResPermissionId] [uniqueidentifier] NOT NULL,
[UserResourceId] [int] NULL,
[ResourceId] [int] NULL,
[Permission] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syUserResPermissions] ADD CONSTRAINT [PK_syUserResPermissions_ResPermissionId] PRIMARY KEY CLUSTERED  ([ResPermissionId]) ON [PRIMARY]
GO
