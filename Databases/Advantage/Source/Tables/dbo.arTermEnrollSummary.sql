CREATE TABLE [dbo].[arTermEnrollSummary]
(
[TESummId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arTermEnrollSummary_TESummId] DEFAULT (newid()),
[TermId] [uniqueidentifier] NOT NULL,
[StuEnrollId] [uniqueidentifier] NOT NULL,
[DescripXTranscript] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL CONSTRAINT [DF_arTermEnrollSummary_ModDate] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arTermEnrollSummary] ADD CONSTRAINT [PK_arTermEnrollSummary_TESummId] PRIMARY KEY CLUSTERED  ([TESummId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_arTermEnrollSummary_TermId_StuEnrollId] ON [dbo].[arTermEnrollSummary] ([TermId], [StuEnrollId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arTermEnrollSummary] ADD CONSTRAINT [FK_arTermEnrollSummary_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY ([StuEnrollId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
ALTER TABLE [dbo].[arTermEnrollSummary] ADD CONSTRAINT [FK_arTermEnrollSummary_arTerm_TermId_TermId] FOREIGN KEY ([TermId]) REFERENCES [dbo].[arTerm] ([TermId])
GO
