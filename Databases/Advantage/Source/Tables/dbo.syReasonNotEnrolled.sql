CREATE TABLE [dbo].[syReasonNotEnrolled]
(
[ReasonNotEnrolledId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_syReasonNotEnrolled_ReasonNotEnrolledId] DEFAULT (newid()),
[Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syReasonNotEnrolled] ADD CONSTRAINT [PK_syReasonNotEnrolled_ReasonNotEnrolledId] PRIMARY KEY CLUSTERED  ([ReasonNotEnrolledId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_syReasonNotEnrolled_Code] ON [dbo].[syReasonNotEnrolled] ([Code]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syReasonNotEnrolled] ADD CONSTRAINT [FK_syReasonNotEnrolled_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[syReasonNotEnrolled] ADD CONSTRAINT [FK_syReasonNotEnrolled_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
