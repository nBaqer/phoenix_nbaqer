CREATE TABLE [dbo].[arCourseEquivalent]
(
[ReqEquivalentId] [uniqueidentifier] NOT NULL,
[ReqId] [uniqueidentifier] NULL,
[EquivReqId] [uniqueidentifier] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arCourseEquivalent] ADD CONSTRAINT [PK_arCourseEquivalent_ReqEquivalentId] PRIMARY KEY CLUSTERED  ([ReqEquivalentId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arCourseEquivalent] ADD CONSTRAINT [FK_arCourseEquivalent_arReqs_EquivReqId_ReqId] FOREIGN KEY ([EquivReqId]) REFERENCES [dbo].[arReqs] ([ReqId])
GO
ALTER TABLE [dbo].[arCourseEquivalent] ADD CONSTRAINT [FK_arCourseEquivalent_arReqs_ReqId_ReqId] FOREIGN KEY ([ReqId]) REFERENCES [dbo].[arReqs] ([ReqId])
GO
