CREATE TABLE [dbo].[arPrgMinorCerts]
(
[PrgMinorCertId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arPrgMinorCerts_PrgMinorCertId] DEFAULT (newid()),
[ParentId] [uniqueidentifier] NOT NULL,
[ChildId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arPrgMinorCerts] ADD CONSTRAINT [PK_arPrgMinorCerts_PrgMinorCertId] PRIMARY KEY CLUSTERED  ([PrgMinorCertId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arPrgMinorCerts] ADD CONSTRAINT [FK_arPrgMinorCerts_arPrgVersions_ChildId_PrgVerId] FOREIGN KEY ([ChildId]) REFERENCES [dbo].[arPrgVersions] ([PrgVerId])
GO
ALTER TABLE [dbo].[arPrgMinorCerts] ADD CONSTRAINT [FK_arPrgMinorCerts_arPrgVersions_ParentId_PrgVerId] FOREIGN KEY ([ParentId]) REFERENCES [dbo].[arPrgVersions] ([PrgVerId])
GO
