CREATE TABLE [dbo].[syPositions]
(
[PositionId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syPositions_PositionId] DEFAULT (newid()),
[PositionCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[PositionDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syPositions] ADD CONSTRAINT [PK_syPositions_PositionId] PRIMARY KEY CLUSTERED  ([PositionId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_syPositions_PositionCode_PositionDescrip_CampGrpId] ON [dbo].[syPositions] ([PositionCode], [PositionDescrip], [CampGrpId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syPositions] ADD CONSTRAINT [FK_syPositions_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[syPositions] ADD CONSTRAINT [FK_syPositions_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
