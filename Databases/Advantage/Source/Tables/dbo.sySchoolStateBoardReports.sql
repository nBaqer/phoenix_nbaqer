CREATE TABLE [dbo].[sySchoolStateBoardReports]
(
[SchoolStateBoardReportId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_sySchoolStateBoardReports_SchoolStateBoardReportsId] DEFAULT (newsequentialid()),
[StateId] [uniqueidentifier] NULL,
[ReportId] [uniqueidentifier] NULL,
[CampusId] [uniqueidentifier] NULL,
[ModifiedDate] [datetime] NULL,
[ModifiedUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OwnerName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OwnerLicenseNumber] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OfficerName1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OfficerName2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OfficerName3] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LicensingAgencyId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[sySchoolStateBoardReports] ADD CONSTRAINT [PK__sySchool__SchoolStateBoardReportId] PRIMARY KEY CLUSTERED  ([SchoolStateBoardReportId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[sySchoolStateBoardReports] ADD CONSTRAINT [FK_sySchoolStateBoardReports_syCampuses_CampusId_CampusId] FOREIGN KEY ([CampusId]) REFERENCES [dbo].[syCampuses] ([CampusId])
GO
ALTER TABLE [dbo].[sySchoolStateBoardReports] ADD CONSTRAINT [FK_sySchoolStateBoardReports_syReports_ReportId_ReportId] FOREIGN KEY ([ReportId]) REFERENCES [dbo].[syReports] ([ReportId])
GO
ALTER TABLE [dbo].[sySchoolStateBoardReports] ADD CONSTRAINT [FK_sySchoolStateBoardReports_syStateBoardAgencies_LicensingAgencyId_StateBoardAgencyId] FOREIGN KEY ([LicensingAgencyId]) REFERENCES [dbo].[syStateBoardAgencies] ([StateBoardAgencyId])
GO
ALTER TABLE [dbo].[sySchoolStateBoardReports] ADD CONSTRAINT [FK_sySchoolStateBoardReports_syStates_StateId_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[syStates] ([StateId])
GO
