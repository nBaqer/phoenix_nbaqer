CREATE TABLE [dbo].[syRptAgencyFields]
(
[RptAgencyFldId] [int] NOT NULL,
[RptAgencyId] [int] NULL,
[RptFldId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syRptAgencyFields] ADD CONSTRAINT [PK_syRptAgencyFields_RptAgencyFldId] PRIMARY KEY CLUSTERED  ([RptAgencyFldId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_syRptAgencyFields_RptAgencyId_RptFldId] ON [dbo].[syRptAgencyFields] ([RptAgencyId], [RptFldId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syRptAgencyFields] ADD CONSTRAINT [FK_syRptAgencyFields_syRptAgencies_RptAgencyId_RptAgencyId] FOREIGN KEY ([RptAgencyId]) REFERENCES [dbo].[syRptAgencies] ([RptAgencyId])
GO
ALTER TABLE [dbo].[syRptAgencyFields] ADD CONSTRAINT [FK_syRptAgencyFields_syRptFields_RptFldId_RptFldId] FOREIGN KEY ([RptFldId]) REFERENCES [dbo].[syRptFields] ([RptFldId])
GO
