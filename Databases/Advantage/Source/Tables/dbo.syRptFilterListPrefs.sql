CREATE TABLE [dbo].[syRptFilterListPrefs]
(
[FilterListPrefId] [uniqueidentifier] NOT NULL,
[PrefId] [uniqueidentifier] NOT NULL,
[RptParamId] [smallint] NOT NULL,
[FldValue] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syRptFilterListPrefs] ADD CONSTRAINT [PK_syRptFilterListPrefs_FilterListPrefId] PRIMARY KEY CLUSTERED  ([FilterListPrefId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syRptFilterListPrefs] ADD CONSTRAINT [FK_syRptFilterListPrefs_syRptUserPrefs_PrefId_PrefId] FOREIGN KEY ([PrefId]) REFERENCES [dbo].[syRptUserPrefs] ([PrefId]) ON DELETE CASCADE ON UPDATE CASCADE
GO
GRANT SELECT ON  [dbo].[syRptFilterListPrefs] TO [AdvantageRole]
GO
