CREATE TABLE [dbo].[arPrgVersionFees]
(
[PrgVersionFeeId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arPrgVersionFees_PrgVersionFeeId] DEFAULT (newid()),
[PrgVersionFeeCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[PrgVersionFeeDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NULL,
[PrgVerId] [uniqueidentifier] NULL,
[TransCodeId] [uniqueidentifier] NULL,
[TuitionCategoryId] [uniqueidentifier] NULL,
[Amount] [money] NOT NULL CONSTRAINT [DF_arPrgVersionFees_Amount] DEFAULT ((0)),
[RateScheduleId] [uniqueidentifier] NULL,
[UnitId] [smallint] NOT NULL CONSTRAINT [DF_arPrgVersionFees_UnitId] DEFAULT ((0)),
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arPrgVersionFees] ADD CONSTRAINT [PK_arPrgVersionFees_PrgVersionFeeId] PRIMARY KEY CLUSTERED  ([PrgVersionFeeId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arPrgVersionFees] ADD CONSTRAINT [FK_arPrgVersionFees_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[arPrgVersionFees] ADD CONSTRAINT [FK_arPrgVersionFees_arPrgVersions_PrgVerId_PrgVerId] FOREIGN KEY ([PrgVerId]) REFERENCES [dbo].[arPrgVersions] ([PrgVerId]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[arPrgVersionFees] ADD CONSTRAINT [FK_arPrgVersionFees_saRateSchedules_RateScheduleId_RateScheduleId] FOREIGN KEY ([RateScheduleId]) REFERENCES [dbo].[saRateSchedules] ([RateScheduleId])
GO
ALTER TABLE [dbo].[arPrgVersionFees] ADD CONSTRAINT [FK_arPrgVersionFees_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
ALTER TABLE [dbo].[arPrgVersionFees] ADD CONSTRAINT [FK_arPrgVersionFees_saTransCodes_TransCodeId_TransCodeId] FOREIGN KEY ([TransCodeId]) REFERENCES [dbo].[saTransCodes] ([TransCodeId])
GO
ALTER TABLE [dbo].[arPrgVersionFees] ADD CONSTRAINT [FK_arPrgVersionFees_saTuitionCategories_TuitionCategoryId_TuitionCategoryId] FOREIGN KEY ([TuitionCategoryId]) REFERENCES [dbo].[saTuitionCategories] ([TuitionCategoryId])
GO
