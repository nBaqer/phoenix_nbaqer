CREATE TABLE [dbo].[saPaymentDescriptions]
(
[PmtDescriptionId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_saPaymentDescriptions_PmtDescriptionId] DEFAULT (newid()),
[PmtCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PmtDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[CampGrpId] [uniqueidentifier] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saPaymentDescriptions] ADD CONSTRAINT [PK_saPaymentDescriptions_PmtDescriptionId] PRIMARY KEY CLUSTERED  ([PmtDescriptionId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saPaymentDescriptions] ADD CONSTRAINT [FK_saPaymentDescriptions_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[saPaymentDescriptions] ADD CONSTRAINT [FK_saPaymentDescriptions_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
