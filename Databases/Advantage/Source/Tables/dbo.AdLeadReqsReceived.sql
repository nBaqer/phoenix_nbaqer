CREATE TABLE [dbo].[AdLeadReqsReceived]
(
[LeadReqId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_AdLeadReqsReceived_LeadReqId] DEFAULT (newid()),
[LeadId] [uniqueidentifier] NOT NULL,
[DocumentId] [uniqueidentifier] NOT NULL,
[ReceivedDate] [datetime] NULL,
[IsApproved] [bit] NULL,
[ApprovalDate] [datetime] NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Override] [bit] NOT NULL,
[OverrideReason] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
 
--========================================================================================== 
-- TRIGGER AdLeadReqsReceived_Audit_Delete 
-- INSERT  add Audit History when delete records in AdLeadReqsReceived 
--========================================================================================== 
CREATE TRIGGER [dbo].[AdLeadReqsReceived_Audit_Delete] ON [dbo].[AdLeadReqsReceived]
    FOR DELETE
AS
    BEGIN 
        SET NOCOUNT ON; 
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
        DECLARE @EventRows AS INT; 
        DECLARE @EventDate AS DATETIME; 
        DECLARE @UserName AS VARCHAR(50); 
	
        SET @AuditHistId = NEWID(); 
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Deleted
                         ); 
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Deleted
                         ); 
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Deleted
                        ); 
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'AdLeadReqsReceived','D',@EventRows,@EventDate,@UserName; 
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadReqId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),Old.LeadId)
                        FROM    Deleted AS Old; 
                -- DocumentId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadReqId
                               ,'DocumentId'
                               ,CONVERT(VARCHAR(MAX),Old.DocumentId)
                        FROM    Deleted AS Old; 
                -- ReceivedDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadReqId
                               ,'ReceivedDate'
                               ,CONVERT(VARCHAR(MAX),Old.ReceivedDate,121)
                        FROM    Deleted AS Old; 
                -- IsApproved 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadReqId
                               ,'IsApproved'
                               ,CONVERT(VARCHAR(MAX),Old.IsApproved)
                        FROM    Deleted AS Old; 
                -- ApprovalDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadReqId
                               ,'ApprovalDate'
                               ,CONVERT(VARCHAR(MAX),Old.ApprovalDate,121)
                        FROM    Deleted AS Old; 
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadReqId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                        FROM    Deleted AS Old; 
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadReqId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),Old.ModUser)
                        FROM    Deleted AS Old; 
                -- Override 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadReqId
                               ,'Override'
                               ,CONVERT(VARCHAR(MAX),Old.Override)
                        FROM    Deleted AS Old; 
                -- OverrideReason 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadReqId
                               ,'OverrideReason'
                               ,CONVERT(VARCHAR(MAX),Old.OverrideReason)
                        FROM    Deleted AS Old; 
            END; 
        SET NOCOUNT OFF; 
    END; 
--========================================================================================== 
-- END TRIGGER AdLeadReqsReceived_Audit_Delete 
--========================================================================================== 
 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
 
--========================================================================================== 
-- TRIGGER AdLeadReqsReceived_Audit_Insert 
-- INSERT  add Audit History when insert records in AdLeadReqsReceived 
--========================================================================================== 
CREATE TRIGGER [dbo].[AdLeadReqsReceived_Audit_Insert] ON [dbo].[AdLeadReqsReceived]
    FOR INSERT
AS
    BEGIN 
        SET NOCOUNT ON; 
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
        DECLARE @EventRows AS INT; 
        DECLARE @EventDate AS DATETIME; 
        DECLARE @UserName AS VARCHAR(50); 
	
        SET @AuditHistId = NEWID(); 
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         ); 
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         ); 
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        ); 
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'AdLeadReqsReceived','I',@EventRows,@EventDate,@UserName; 
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadReqId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),New.LeadId)
                        FROM    Inserted AS New; 
                -- DocumentId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadReqId
                               ,'DocumentId'
                               ,CONVERT(VARCHAR(MAX),New.DocumentId)
                        FROM    Inserted AS New; 
                -- ReceivedDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadReqId
                               ,'ReceivedDate'
                               ,CONVERT(VARCHAR(MAX),New.ReceivedDate,121)
                        FROM    Inserted AS New; 
                -- IsApproved 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadReqId
                               ,'IsApproved'
                               ,CONVERT(VARCHAR(MAX),New.IsApproved)
                        FROM    Inserted AS New; 
                -- ApprovalDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadReqId
                               ,'ApprovalDate'
                               ,CONVERT(VARCHAR(MAX),New.ApprovalDate,121)
                        FROM    Inserted AS New; 
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadReqId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                        FROM    Inserted AS New; 
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadReqId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),New.ModUser)
                        FROM    Inserted AS New; 
                -- Override 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadReqId
                               ,'Override'
                               ,CONVERT(VARCHAR(MAX),New.Override)
                        FROM    Inserted AS New; 
                -- OverrideReason 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadReqId
                               ,'OverrideReason'
                               ,CONVERT(VARCHAR(MAX),New.OverrideReason)
                        FROM    Inserted AS New; 
            END; 
        SET NOCOUNT OFF; 
    END; 
--========================================================================================== 
-- END TRIGGER AdLeadReqsReceived_Audit_Insert 
--========================================================================================== 
 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
--========================================================================================== 
-- TRIGGER AdLeadReqsReceived_Audit_Update 
-- UPDATE  add Audit History when update fields in AdLeadReqsReceived 
--========================================================================================== 
CREATE TRIGGER [dbo].[AdLeadReqsReceived_Audit_Update] ON [dbo].[AdLeadReqsReceived]
    FOR UPDATE
AS
    BEGIN 
        SET NOCOUNT ON; 
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
        DECLARE @EventRows AS INT; 
        DECLARE @EventDate AS DATETIME; 
        DECLARE @UserName AS VARCHAR(50); 
	
        SET @AuditHistId = NEWID(); 
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         ); 
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         ); 
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        ); 
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'AdLeadReqsReceived','U',@EventRows,@EventDate,@UserName; 
                -- LeadId 
                IF UPDATE(LeadId)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadReqId
                                       ,'LeadId'
                                       ,CONVERT(VARCHAR(MAX),Old.LeadId)
                                       ,CONVERT(VARCHAR(MAX),New.LeadId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadReqId = New.LeadReqId
                                WHERE   Old.LeadId <> New.LeadId
                                        OR (
                                             Old.LeadId IS NULL
                                             AND New.LeadId IS NOT NULL
                                           )
                                        OR (
                                             New.LeadId IS NULL
                                             AND Old.LeadId IS NOT NULL
                                           ); 
                    END; 
                -- DocumentId 
                IF UPDATE(DocumentId)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadReqId
                                       ,'DocumentId'
                                       ,CONVERT(VARCHAR(MAX),Old.DocumentId)
                                       ,CONVERT(VARCHAR(MAX),New.DocumentId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadReqId = New.LeadReqId
                                WHERE   Old.DocumentId <> New.DocumentId
                                        OR (
                                             Old.DocumentId IS NULL
                                             AND New.DocumentId IS NOT NULL
                                           )
                                        OR (
                                             New.DocumentId IS NULL
                                             AND Old.DocumentId IS NOT NULL
                                           ); 
                    END; 
                -- ReceivedDate 
                IF UPDATE(ReceivedDate)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadReqId
                                       ,'ReceivedDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ReceivedDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ReceivedDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadReqId = New.LeadReqId
                                WHERE   Old.ReceivedDate <> New.ReceivedDate
                                        OR (
                                             Old.ReceivedDate IS NULL
                                             AND New.ReceivedDate IS NOT NULL
                                           )
                                        OR (
                                             New.ReceivedDate IS NULL
                                             AND Old.ReceivedDate IS NOT NULL
                                           ); 
                    END; 
                -- IsApproved 
                IF UPDATE(IsApproved)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadReqId
                                       ,'IsApproved'
                                       ,CONVERT(VARCHAR(MAX),Old.IsApproved)
                                       ,CONVERT(VARCHAR(MAX),New.IsApproved)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadReqId = New.LeadReqId
                                WHERE   Old.IsApproved <> New.IsApproved
                                        OR (
                                             Old.IsApproved IS NULL
                                             AND New.IsApproved IS NOT NULL
                                           )
                                        OR (
                                             New.IsApproved IS NULL
                                             AND Old.IsApproved IS NOT NULL
                                           ); 
                    END; 
                -- ApprovalDate 
                IF UPDATE(ApprovalDate)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadReqId
                                       ,'ApprovalDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ApprovalDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ApprovalDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadReqId = New.LeadReqId
                                WHERE   Old.ApprovalDate <> New.ApprovalDate
                                        OR (
                                             Old.ApprovalDate IS NULL
                                             AND New.ApprovalDate IS NOT NULL
                                           )
                                        OR (
                                             New.ApprovalDate IS NULL
                                             AND Old.ApprovalDate IS NOT NULL
                                           ); 
                    END; 
                -- ModDate 
                IF UPDATE(ModDate)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadReqId
                                       ,'ModDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadReqId = New.LeadReqId
                                WHERE   Old.ModDate <> New.ModDate
                                        OR (
                                             Old.ModDate IS NULL
                                             AND New.ModDate IS NOT NULL
                                           )
                                        OR (
                                             New.ModDate IS NULL
                                             AND Old.ModDate IS NOT NULL
                                           ); 
                    END; 
                -- ModUser 
                IF UPDATE(ModUser)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadReqId
                                       ,'ModUser'
                                       ,CONVERT(VARCHAR(MAX),Old.ModUser)
                                       ,CONVERT(VARCHAR(MAX),New.ModUser)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadReqId = New.LeadReqId
                                WHERE   Old.ModUser <> New.ModUser
                                        OR (
                                             Old.ModUser IS NULL
                                             AND New.ModUser IS NOT NULL
                                           )
                                        OR (
                                             New.ModUser IS NULL
                                             AND Old.ModUser IS NOT NULL
                                           ); 
                    END; 
                -- Override 
                IF UPDATE(Override)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadReqId
                                       ,'Override'
                                       ,CONVERT(VARCHAR(MAX),Old.Override)
                                       ,CONVERT(VARCHAR(MAX),New.Override)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadReqId = New.LeadReqId
                                WHERE   Old.Override <> New.Override
                                        OR (
                                             Old.Override IS NULL
                                             AND New.Override IS NOT NULL
                                           )
                                        OR (
                                             New.Override IS NULL
                                             AND Old.Override IS NOT NULL
                                           ); 
                    END; 
                -- OverrideReason 
                IF UPDATE(OverrideReason)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadReqId
                                       ,'OverrideReason'
                                       ,CONVERT(VARCHAR(MAX),Old.OverrideReason)
                                       ,CONVERT(VARCHAR(MAX),New.OverrideReason)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadReqId = New.LeadReqId
                                WHERE   Old.OverrideReason <> New.OverrideReason
                                        OR (
                                             Old.OverrideReason IS NULL
                                             AND New.OverrideReason IS NOT NULL
                                           )
                                        OR (
                                             New.OverrideReason IS NULL
                                             AND Old.OverrideReason IS NOT NULL
                                           ); 
                    END; 
            END; 
        SET NOCOUNT OFF; 
    END; 
--========================================================================================== 
-- END TRIGGER AdLeadReqsReceived_Audit_Update 
--========================================================================================== 
 
GO
ALTER TABLE [dbo].[AdLeadReqsReceived] ADD CONSTRAINT [PK_AdLeadReqsReceived_LeadReqId] PRIMARY KEY CLUSTERED  ([LeadReqId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AdLeadReqsReceived] ADD CONSTRAINT [FK_AdLeadReqsReceived_adLeads_LeadId_LeadId] FOREIGN KEY ([LeadId]) REFERENCES [dbo].[adLeads] ([LeadId])
GO
ALTER TABLE [dbo].[AdLeadReqsReceived] ADD CONSTRAINT [FK_AdLeadReqsReceived_adReqs_DocumentId_adReqId] FOREIGN KEY ([DocumentId]) REFERENCES [dbo].[adReqs] ([adReqId])
GO
