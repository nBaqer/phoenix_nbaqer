CREATE TABLE [dbo].[arStudentClockAttendance]
(
[StuEnrollId] [uniqueidentifier] NOT NULL,
[ScheduleId] [uniqueidentifier] NOT NULL,
[RecordDate] [smalldatetime] NOT NULL,
[SchedHours] [decimal] (18, 2) NULL,
[ActualHours] [decimal] (18, 2) NULL,
[ModDate] [smalldatetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[isTardy] [bit] NULL CONSTRAINT [DF_arStudentClockAttendance_isTardy] DEFAULT ((0)),
[PostByException] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_arStudentClockAttendance_PostByException] DEFAULT ('no'),
[comments] [varchar] (240) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TardyProcessed] [bit] NULL,
[Converted] [bit] NOT NULL CONSTRAINT [DF_arStudentClockAttendance_Converted] DEFAULT ((0)),
[SchedHoursOnTermination] [decimal] (18, 2) NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		FAME Inc.
-- Create date: 5/17/2019
-- Description:	When inserting record into arStudentClockAttendance, if time clock punches exist and hours not 0 or 999.0, calculate rounding hours
-- =============================================
CREATE   TRIGGER [dbo].[tr_arStudentClockAttendance_Insert]
ON [dbo].[arStudentClockAttendance]
INSTEAD OF INSERT
AS
    BEGIN
        INSERT INTO dbo.arStudentClockAttendance
                    SELECT Inserted.StuEnrollId
                          ,Inserted.ScheduleId
                          ,Inserted.RecordDate
                          ,Inserted.SchedHours
                          ,CASE WHEN (
                                     inserted.ActualHours = 0
                                     OR inserted.ActualHours = 9999.0
                                     OR inserted.ActualHours = 999.0
                                     OR inserted.ActualHours = 99.0
                                     ) THEN Inserted.ActualHours 
                                ELSE dbo.CalculateHoursForStudentOnDate(inserted.StuEnrollId, inserted.RecordDate, inserted.ActualHours)
                           END AS ActualHours
                          ,Inserted.ModDate
                          ,Inserted.ModUser
                          ,Inserted.isTardy
                          ,Inserted.PostByException
                          ,Inserted.comments
                          ,Inserted.TardyProcessed
                          ,Inserted.Converted,
						  Inserted.SchedHoursOnTermination
                    FROM   inserted;


    END;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		FAME Inc.
-- Create date: 5/17/2019
-- Description:	When updating record into arStudentClockAttendance, if time clock punches is updated hours not 0 or 999.0, calculate rounding hours
-- =============================================
CREATE     TRIGGER [dbo].[tr_arStudentClockAttendance_Update]
ON [dbo].[arStudentClockAttendance]
INSTEAD OF UPDATE
AS
    BEGIN
					UPDATE a
					SET a.StuEnrollId = Inserted.StuEnrollId,
					a.ScheduleId = Inserted.ScheduleId,
					a.RecordDate = Inserted.RecordDate,
					a.SchedHours = inserted.SchedHours,
					a.ActualHours =  (CASE WHEN (
                                     inserted.ActualHours = 0
                                     OR inserted.ActualHours = 9999.0
                                     OR inserted.ActualHours = 999.0
                                     OR inserted.ActualHours = 99.0
                                     ) THEN Inserted.ActualHours
                                ELSE dbo.CalculateHoursForStudentOnDate(inserted.StuEnrollId, inserted.RecordDate, inserted.ActualHours)
                           END),
					a.ModDate = Inserted.ModDate,
					a.ModUser = Inserted.ModUser,
					a.isTardy = Inserted.isTardy,
					a.PostByException = Inserted.PostByException,
					a.comments = Inserted.comments,
					a.TardyProcessed = Inserted.TardyProcessed,
					a.Converted =Inserted.Converted,
					a.SchedHoursOnTermination = Inserted.SchedHoursOnTermination
					FROM Inserted JOIN dbo.arStudentClockAttendance a ON a.StuEnrollId = Inserted.StuEnrollId AND a.RecordDate = Inserted.RecordDate;
    END;


GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[TR_InsertAttendanceSummary_ByDay_PA]
ON [dbo].[arStudentClockAttendance]
AFTER INSERT, UPDATE
AS
SET NOCOUNT ON;

DECLARE @MyEnrollments TABLE
    (
        StuEnrollId UNIQUEIDENTIFIER
    );

DECLARE @HourTotals2 TABLE
    (
        StuEnrollId UNIQUEIDENTIFIER
       ,RecordDate DATETIME
       ,ScheduledHours DECIMAL(18, 2)
       ,ActualHours DECIMAL(18, 2)
       ,ActualRunningScheduledDays DECIMAL(18, 2)
       ,ActualRunningPresentHours DECIMAL(18, 2)
       ,ActualRunningAbsentHours DECIMAL(18, 2)
       ,ActualRunningMakeupHours DECIMAL(18, 2)
       ,ActualRunningTardyHours DECIMAL(18, 2)
       ,ActualRunningExcusedDays INT
       ,Tardy DECIMAL(18, 2)
       ,AdjustedRunningPresentHours DECIMAL(18, 2)
       ,AdjustedRunningAbsentHours DECIMAL(18, 2)
       ,tracktardies INT
       ,tardiesMakingAbsence INT
       ,intTardyBreakPoint INT
       ,rownumber INT
    );

INSERT INTO @MyEnrollments
            SELECT     enrollments.StuEnrollId
            FROM       dbo.arStuEnrollments enrollments
            INNER JOIN Inserted i ON i.StuEnrollId = enrollments.StuEnrollId;


INSERT INTO @HourTotals2 (
                         StuEnrollId
                        ,RecordDate
                        ,ScheduledHours
                        ,ActualHours
                        ,Tardy
                        ,ActualRunningScheduledDays
                        ,ActualRunningPresentHours
                        ,ActualRunningAbsentHours
                        ,ActualRunningMakeupHours
                        ,ActualRunningTardyHours
                        ,tracktardies
                        ,tardiesMakingAbsence
                        ,intTardyBreakPoint
                        ,rownumber
                         )
            SELECT   hoursBucketAll.StuEnrollId
                    ,hoursBucketAll.RecordDate
                    ,hoursBucketAll.SchedHours
                    ,hoursBucketAll.ActualHours
                    ,hoursBucketAll.isTardy
                    ,hoursBucketAll.ActualRunningScheduledHours
                    ,hoursBucketAll.ActualRunningPresentHours
                    ,hoursBucketAll.ActualRunningAbsentHours
                    ,hoursBucketAll.ActualRunningMakeupHours
                    ,hoursBucketAll.ActualRunningTardyHours
                    ,hoursBucketAll.TrackTardies
                    ,hoursBucketAll.TardiesMakingAbsence
                    ,hoursBucketAll.intTardyBreakPoint
                    ,hoursBucketAll.RowNumber
            FROM     (
                     SELECT *
                           ,CASE WHEN hoursBucketWithTardy.TrackTardies = 1
                                      AND hoursBucketWithTardy.intTardyBreakPoint % hoursBucketWithTardy.TardiesMakingAbsence = 0 THEN
                           ( -hoursBucketWithTardy.ActualRunningPresentHours )
                                 ELSE hoursBucketWithTardy.ActualRunningPresentHours
                            END AS AdjustedRunningPresentHours
                           ,CASE WHEN hoursBucketWithTardy.TrackTardies = 1
                                      AND hoursBucketWithTardy.intTardyBreakPoint = hoursBucketWithTardy.TardiesMakingAbsence THEN
                           ( -hoursBucketWithTardy.ActualRunningAbsentHours + hoursBucketWithTardy.SchedHours )
                                 ELSE hoursBucketWithTardy.ActualRunningAbsentHours
                            END AS AdjustedRunningAbsentHours
                     FROM   (
                            SELECT hoursBucket.StuEnrollId
                                  ,hoursBucket.RecordDate
                                  ,hoursBucket.SchedHours
                                  ,hoursBucket.ActualHours
                                  ,hoursBucket.isTardy
                                  ,hoursBucket.Absent
                                  ,SUM(   CASE WHEN hoursBucket.ActualHours <> 9999.00
                                                    AND hoursBucket.ActualHours <> 999 THEN hoursBucket.SchedHours
                                               ELSE 0
                                          END
                                      ) OVER ( PARTITION BY hoursBucket.StuEnrollId
                                               ORDER BY hoursBucket.RecordDate
                                             ) AS ActualRunningScheduledHours
                                  ,SUM(   CASE WHEN (
                                                    hoursBucket.ActualHours > 0
                                                    AND hoursBucket.ActualHours > hoursBucket.SchedHours
                                                    AND hoursBucket.ActualHours <> 9999.00
                                                    ) THEN ( hoursBucket.ActualHours - ( hoursBucket.ActualHours - hoursBucket.SchedHours ))
                                               WHEN hoursBucket.ActualHours <> 9999.00 THEN hoursBucket.ActualHours
                                               ELSE 0
                                          END
                                      ) OVER ( PARTITION BY hoursBucket.StuEnrollId
                                               ORDER BY hoursBucket.RecordDate
                                             ) AS ActualRunningPresentHours
                                  ,SUM(   hoursBucket.Absent
                                          + CASE WHEN hoursBucket.ActualHours > 0
                                                      AND hoursBucket.ActualHours < hoursBucket.SchedHours THEN hoursBucket.SchedHours - hoursBucket.ActualHours
                                                 ELSE 0
                                            END
                                      ) OVER ( PARTITION BY hoursBucket.StuEnrollId
                                               ORDER BY hoursBucket.RecordDate
                                             ) AS ActualRunningAbsentHours
                                  ,SUM(   CASE WHEN hoursBucket.ActualHours > 0
                                                    AND hoursBucket.ActualHours > hoursBucket.SchedHours
                                                    AND hoursBucket.ActualHours <> 9999.00 THEN hoursBucket.ActualHours - hoursBucket.SchedHours
                                               ELSE 0
                                          END
                                      ) OVER ( PARTITION BY hoursBucket.StuEnrollId
                                               ORDER BY hoursBucket.RecordDate
                                             ) AS ActualRunningMakeupHours
                                  ,SUM(   CASE WHEN hoursBucket.ActualHours > 0
                                                    AND hoursBucket.ActualHours < hoursBucket.SchedHours
                                                    AND hoursBucket.isTardy = 1 THEN hoursBucket.SchedHours - hoursBucket.ActualHours
                                               ELSE 0
                                          END
                                      ) OVER ( PARTITION BY hoursBucket.StuEnrollId
                                               ORDER BY hoursBucket.RecordDate
                                             ) AS ActualRunningTardyHours
                                  ,SUM(   CASE WHEN hoursBucket.TrackTardies = 1
                                                    AND hoursBucket.isTardy = 1 THEN 1
                                               ELSE 0
                                          END
                                      ) OVER ( PARTITION BY hoursBucket.StuEnrollId
                                               ORDER BY hoursBucket.RecordDate
                                             ) AS intTardyBreakPoint
                                  ,hoursBucket.TrackTardies
                                  ,hoursBucket.TardiesMakingAbsence
                                  ,hoursBucket.RowNumber
                            FROM   (
                                   SELECT *
                                         ,ROW_NUMBER() OVER ( ORDER BY dt.RecordDate ) AS RowNumber
                                   FROM   (
                                          SELECT     t1.StuEnrollId
                                                    ,t1.RecordDate
                                                    ,t1.SchedHours
                                                    ,t1.ActualHours
                                                    ,CASE WHEN (
                                                               (
                                                               (
                                                               (
                                                               AAUT.UnitTypeDescrip = 'Present Absent'
                                                               OR AAUT.UnitTypeDescrip = 'Minutes'
                                                                  AND t1.SchedHours >= 1
                                                               )
                                                               OR (
                                                                  AAUT.UnitTypeDescrip = 'Clock Hours'
                                                                  AND t1.SchedHours > 0
                                                                  )
                                                               )
                                                               AND t1.SchedHours NOT IN ( 999, 9999 )
                                                               )
                                                               AND t1.ActualHours = 0
                                                               ) THEN t1.SchedHours
                                                          ELSE 0
                                                     END AS Absent
                                                    ,t1.isTardy
                                                    ,t3.TrackTardies
                                                    ,t3.TardiesMakingAbsence
                                          FROM       arStudentClockAttendance t1
                                          INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                          INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                          INNER JOIN arAttUnitType AS AAUT ON AAUT.UnitTypeId = t3.UnitTypeId
                                          INNER JOIN @MyEnrollments ON t1.StuEnrollId = [@MyEnrollments].StuEnrollId
                                          WHERE --t3.UnitTypeId IN ( 'ef5535c2-142c-4223-ae3c-25a50a153cc6','B937C92E-FD7A-455E-A731-527A9918C734' ) -- UnitTypeDescrip = ('Present Absent', 'Clock Hours') -
                                                     AAUT.UnitTypeDescrip IN ( 'Present Absent', 'Clock Hours', 'Minutes' )
                                                     AND t1.ActualHours <> 9999.00
                                          ) dt
                                   --ORDER BY StuEnrollId
                                   --        ,MeetDate
                                   ) hoursBucket
                            ) hoursBucketWithTardy
                     ) hoursBucketAll
            ORDER BY hoursBucketAll.StuEnrollId
                    ,hoursBucketAll.RecordDate;


DELETE     dbo.syStudentAttendanceSummary
FROM       dbo.syStudentAttendanceSummary
INNER JOIN @MyEnrollments ON syStudentAttendanceSummary.StuEnrollId = [@MyEnrollments].StuEnrollId;

INSERT INTO syStudentAttendanceSummary (
                                       StuEnrollId
                                      ,ClsSectionId
                                      ,StudentAttendedDate
                                      ,ScheduledDays
                                      ,ActualDays
                                      ,ActualRunningScheduledDays
                                      ,ActualRunningPresentDays
                                      ,ActualRunningAbsentDays
                                      ,ActualRunningMakeupDays
                                      ,ActualRunningTardyDays
                                      ,AdjustedPresentDays
                                      ,AdjustedAbsentDays
                                      ,AttendanceTrackType
                                      ,ModUser
                                      ,ModDate
                                      ,IsExcused
                                      ,ActualRunningExcusedDays
                                      ,IsTardy
                                       )
            SELECT StuEnrollId
                  ,NULL
                  ,RecordDate
                  ,ScheduledHours
                  ,ActualHours
                  ,ActualRunningScheduledDays
                  ,ActualRunningPresentHours
                  ,ActualRunningAbsentHours
                  ,ISNULL(ActualRunningMakeupHours, 0)
                  ,ActualRunningTardyHours
                  ,AdjustedRunningPresentHours
                  ,AdjustedRunningAbsentHours
                  ,'Post Attendance by Class'
                  ,'sa'
                  ,GETDATE()
                  ,0
                  ,NULL
                  ,Tardy
            FROM   @HourTotals2;




SET NOCOUNT OFF;


GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[TR_UpdateLDA_SCA]
ON [dbo].[arStudentClockAttendance]
AFTER INSERT, UPDATE, DELETE
AS

--If update and the record date is changed or fresh insert then refresh the lda
DECLARE @Data TABLE
    (
        StuEnrollId UNIQUEIDENTIFIER NOT NULL
       ,LDA DATETIME NULL
    );

INSERT INTO @Data
            SELECT    enrollments.StuEnrollId
                     ,MAX(a.RecordDate) AS LDA
            FROM      (
                      (SELECT I.StuEnrollId
                       FROM   Inserted I
                       WHERE  (
                              I.ActualHours > 0
                              AND I.ActualHours <> 99.00
                              AND I.ActualHours <> 999.00
                              AND I.ActualHours <> 9999.00
                              ))
                      UNION
                      (SELECT d.StuEnrollId
                       FROM   Deleted d)
                      ) enrollments
            LEFT JOIN dbo.arStudentClockAttendance a ON a.StuEnrollId = enrollments.StuEnrollId
            WHERE     (
                      a.ActualHours > 0
                      AND a.ActualHours <> 99.00
                      AND a.ActualHours <> 999.00
                      AND a.ActualHours <> 9999.00
                      )
            GROUP BY  enrollments.StuEnrollId;


UPDATE e
SET    e.LDA = t.LDA
FROM   dbo.arStuEnrollments e
JOIN   @Data t ON t.StuEnrollId = e.StuEnrollId;




GO
ALTER TABLE [dbo].[arStudentClockAttendance] ADD CONSTRAINT [PK_arStudentClockAttendance_StuEnrollId_ScheduleId_RecordDate] PRIMARY KEY CLUSTERED  ([StuEnrollId], [ScheduleId], [RecordDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_arStudentClockAttendance_StuEnrollId_isTardy_RecordDate] ON [dbo].[arStudentClockAttendance] ([StuEnrollId], [isTardy], [RecordDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_arStudentClockAttendance_StuEnrollId_RecordDate_ActualHours_SchedHours_isTardy] ON [dbo].[arStudentClockAttendance] ([StuEnrollId], [RecordDate], [ActualHours], [SchedHours], [isTardy]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_arStudentClockAttendance_StuEnrollId_RecordDate_SchedHours_ActualHours] ON [dbo].[arStudentClockAttendance] ([StuEnrollId], [RecordDate], [SchedHours], [ActualHours]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arStudentClockAttendance] ADD CONSTRAINT [FK_arStudentClockAttendance_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY ([StuEnrollId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
