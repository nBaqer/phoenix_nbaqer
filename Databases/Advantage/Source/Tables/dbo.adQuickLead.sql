CREATE TABLE [dbo].[adQuickLead]
(
[QkLeadID] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_adQuickLead_QkLeadID] DEFAULT (newid()),
[StudentID] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeadId] [int] NULL,
[LeadValue] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
 
--========================================================================================== 
-- TRIGGER adQuickLead_Audit_Delete 
-- INSERT  add Audit History when delete records in adQuickLead 
--========================================================================================== 
CREATE TRIGGER [dbo].[adQuickLead_Audit_Delete] ON [dbo].[adQuickLead]
    FOR DELETE
AS
    BEGIN 
        SET NOCOUNT ON; 
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
        DECLARE @EventRows AS INT; 
        DECLARE @EventDate AS DATETIME; 
        DECLARE @UserName AS VARCHAR(50); 
	
        SET @AuditHistId = NEWID(); 
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Deleted
                         ); 
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Deleted
                         ); 
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Deleted
                        ); 
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adQuickLead','D',@EventRows,@EventDate,@UserName; 
                -- StudentID 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.QkLeadID
                               ,'StudentID'
                               ,CONVERT(VARCHAR(MAX),Old.StudentID)
                        FROM    Deleted AS Old; 
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.QkLeadID
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),Old.LeadId)
                        FROM    Deleted AS Old; 
                -- LeadValue 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.QkLeadID
                               ,'LeadValue'
                               ,CONVERT(VARCHAR(MAX),Old.LeadValue)
                        FROM    Deleted AS Old; 
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.QkLeadID
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                        FROM    Deleted AS Old; 
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.QkLeadID
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),Old.ModUser)
                        FROM    Deleted AS Old; 
            END; 
        SET NOCOUNT OFF; 
    END; 
--========================================================================================== 
-- END TRIGGER adQuickLead_Audit_Delete 
--========================================================================================== 
 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
 
--========================================================================================== 
-- TRIGGER adQuickLead_Audit_Insert 
-- INSERT  add Audit History when insert records in adQuickLead 
--========================================================================================== 
CREATE TRIGGER [dbo].[adQuickLead_Audit_Insert] ON [dbo].[adQuickLead]
    FOR INSERT
AS
    BEGIN 
        SET NOCOUNT ON; 
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
        DECLARE @EventRows AS INT; 
        DECLARE @EventDate AS DATETIME; 
        DECLARE @UserName AS VARCHAR(50); 
	
        SET @AuditHistId = NEWID(); 
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         ); 
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         ); 
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        ); 
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adQuickLead','I',@EventRows,@EventDate,@UserName; 
                -- StudentID 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.QkLeadID
                               ,'StudentID'
                               ,CONVERT(VARCHAR(MAX),New.StudentID)
                        FROM    Inserted AS New; 
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.QkLeadID
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),New.LeadId)
                        FROM    Inserted AS New; 
                -- LeadValue 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.QkLeadID
                               ,'LeadValue'
                               ,CONVERT(VARCHAR(MAX),New.LeadValue)
                        FROM    Inserted AS New; 
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.QkLeadID
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                        FROM    Inserted AS New; 
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.QkLeadID
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),New.ModUser)
                        FROM    Inserted AS New; 
            END; 
        SET NOCOUNT OFF; 
    END; 
--========================================================================================== 
-- END TRIGGER adQuickLead_Audit_Insert 
--========================================================================================== 
 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
--========================================================================================== 
-- TRIGGER adQuickLead_Audit_Update 
-- UPDATE  add Audit History when update fields in adQuickLead 
--========================================================================================== 
CREATE TRIGGER [dbo].[adQuickLead_Audit_Update] ON [dbo].[adQuickLead]
    FOR UPDATE
AS
    BEGIN 
        SET NOCOUNT ON; 
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
        DECLARE @EventRows AS INT; 
        DECLARE @EventDate AS DATETIME; 
        DECLARE @UserName AS VARCHAR(50); 
	
        SET @AuditHistId = NEWID(); 
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         ); 
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         ); 
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        ); 
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adQuickLead','U',@EventRows,@EventDate,@UserName; 
                -- StudentID 
                IF UPDATE(StudentID)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.QkLeadID
                                       ,'StudentID'
                                       ,CONVERT(VARCHAR(MAX),Old.StudentID)
                                       ,CONVERT(VARCHAR(MAX),New.StudentID)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.QkLeadID = New.QkLeadID
                                WHERE   Old.StudentID <> New.StudentID
                                        OR (
                                             Old.StudentID IS NULL
                                             AND New.StudentID IS NOT NULL
                                           )
                                        OR (
                                             New.StudentID IS NULL
                                             AND Old.StudentID IS NOT NULL
                                           ); 
                    END; 
                -- LeadId 
                IF UPDATE(LeadId)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.QkLeadID
                                       ,'LeadId'
                                       ,CONVERT(VARCHAR(MAX),Old.LeadId)
                                       ,CONVERT(VARCHAR(MAX),New.LeadId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.QkLeadID = New.QkLeadID
                                WHERE   Old.LeadId <> New.LeadId
                                        OR (
                                             Old.LeadId IS NULL
                                             AND New.LeadId IS NOT NULL
                                           )
                                        OR (
                                             New.LeadId IS NULL
                                             AND Old.LeadId IS NOT NULL
                                           ); 
                    END; 
                -- LeadValue 
                IF UPDATE(LeadValue)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.QkLeadID
                                       ,'LeadValue'
                                       ,CONVERT(VARCHAR(MAX),Old.LeadValue)
                                       ,CONVERT(VARCHAR(MAX),New.LeadValue)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.QkLeadID = New.QkLeadID
                                WHERE   Old.LeadValue <> New.LeadValue
                                        OR (
                                             Old.LeadValue IS NULL
                                             AND New.LeadValue IS NOT NULL
                                           )
                                        OR (
                                             New.LeadValue IS NULL
                                             AND Old.LeadValue IS NOT NULL
                                           ); 
                    END; 
                -- ModDate 
                IF UPDATE(ModDate)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.QkLeadID
                                       ,'ModDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.QkLeadID = New.QkLeadID
                                WHERE   Old.ModDate <> New.ModDate
                                        OR (
                                             Old.ModDate IS NULL
                                             AND New.ModDate IS NOT NULL
                                           )
                                        OR (
                                             New.ModDate IS NULL
                                             AND Old.ModDate IS NOT NULL
                                           ); 
                    END; 
                -- ModUser 
                IF UPDATE(ModUser)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.QkLeadID
                                       ,'ModUser'
                                       ,CONVERT(VARCHAR(MAX),Old.ModUser)
                                       ,CONVERT(VARCHAR(MAX),New.ModUser)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.QkLeadID = New.QkLeadID
                                WHERE   Old.ModUser <> New.ModUser
                                        OR (
                                             Old.ModUser IS NULL
                                             AND New.ModUser IS NOT NULL
                                           )
                                        OR (
                                             New.ModUser IS NULL
                                             AND Old.ModUser IS NOT NULL
                                           ); 
                    END; 
            END; 
        SET NOCOUNT OFF; 
    END; 
--========================================================================================== 
-- END TRIGGER adQuickLead_Audit_Update 
--========================================================================================== 
 
GO
ALTER TABLE [dbo].[adQuickLead] ADD CONSTRAINT [PK_adQuickLead_QkLeadID] PRIMARY KEY CLUSTERED  ([QkLeadID]) ON [PRIMARY]
GO
