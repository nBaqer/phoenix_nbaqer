CREATE TABLE [dbo].[syInstitutionTypes]
(
[TypeID] [int] NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syInstitutionTypes] ADD CONSTRAINT [PK_syInstitutionTypes_TypeID] PRIMARY KEY CLUSTERED  ([TypeID]) ON [PRIMARY]
GO
