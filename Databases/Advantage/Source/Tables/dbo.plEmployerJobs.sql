CREATE TABLE [dbo].[plEmployerJobs]
(
[EmployerJobId] [uniqueidentifier] NOT NULL,
[Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[EmployerJobTitle] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JobTitleId] [uniqueidentifier] NOT NULL,
[JobDescription] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JobGroupId] [uniqueidentifier] NOT NULL,
[ContactId] [uniqueidentifier] NULL,
[TypeId] [uniqueidentifier] NULL,
[AreaId] [uniqueidentifier] NULL,
[CampGrpId] [uniqueidentifier] NULL,
[WorkDays] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FeeId] [uniqueidentifier] NULL,
[NumberOpen] [int] NULL,
[NumberFilled] [int] NULL,
[OpenedFrom] [datetime] NULL,
[OpenedTo] [datetime] NULL,
[Start] [datetime] NULL,
[SalaryFrom] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SalaryTo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BenefitsId] [uniqueidentifier] NULL,
[ScheduleId] [uniqueidentifier] NULL,
[HoursFrom] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HoursTo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[EmployerId] [uniqueidentifier] NULL CONSTRAINT [DF_plEmployerJobs_EmployerId] DEFAULT (newid()),
[JobRequirements] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SalaryTypeID] [uniqueidentifier] NULL,
[JobPostedDate] [datetime] NULL,
[ExpertiseId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[plEmployerJobs_Audit_Delete] ON [dbo].[plEmployerJobs]
    FOR DELETE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'plEmployerJobs','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerJobId
                               ,'Code'
                               ,CONVERT(VARCHAR(8000),Old.Code,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerJobId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerJobId
                               ,'JobTitleId'
                               ,CONVERT(VARCHAR(8000),Old.JobTitleId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerJobId
                               ,'JobDescription'
                               ,CONVERT(VARCHAR(8000),Old.JobDescription,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerJobId
                               ,'JobGroupId'
                               ,CONVERT(VARCHAR(8000),Old.JobGroupId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerJobId
                               ,'ContactId'
                               ,CONVERT(VARCHAR(8000),Old.ContactId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerJobId
                               ,'TypeId'
                               ,CONVERT(VARCHAR(8000),Old.TypeId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerJobId
                               ,'AreaId'
                               ,CONVERT(VARCHAR(8000),Old.AreaId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerJobId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerJobId
                               ,'WorkDays'
                               ,CONVERT(VARCHAR(8000),Old.WorkDays,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerJobId
                               ,'FeeId'
                               ,CONVERT(VARCHAR(8000),Old.FeeId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerJobId
                               ,'NumberOpen'
                               ,CONVERT(VARCHAR(8000),Old.NumberOpen,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerJobId
                               ,'NumberFilled'
                               ,CONVERT(VARCHAR(8000),Old.NumberFilled,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerJobId
                               ,'OpenedFrom'
                               ,CONVERT(VARCHAR(8000),Old.OpenedFrom,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerJobId
                               ,'OpenedTo'
                               ,CONVERT(VARCHAR(8000),Old.OpenedTo,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerJobId
                               ,'Start'
                               ,CONVERT(VARCHAR(8000),Old.Start,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerJobId
                               ,'SalaryFrom'
                               ,CONVERT(VARCHAR(8000),Old.SalaryFrom,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerJobId
                               ,'SalaryTo'
                               ,CONVERT(VARCHAR(8000),Old.SalaryTo,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerJobId
                               ,'BenefitsId'
                               ,CONVERT(VARCHAR(8000),Old.BenefitsId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerJobId
                               ,'ScheduleId'
                               ,CONVERT(VARCHAR(8000),Old.ScheduleId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerJobId
                               ,'HoursFrom'
                               ,CONVERT(VARCHAR(8000),Old.HoursFrom,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerJobId
                               ,'HoursTo'
                               ,CONVERT(VARCHAR(8000),Old.HoursTo,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerJobId
                               ,'Notes'
                               ,CONVERT(VARCHAR(8000),Old.Notes,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerJobId
                               ,'EmployerId'
                               ,CONVERT(VARCHAR(8000),Old.EmployerId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerJobId
                               ,'JobRequirements'
                               ,CONVERT(VARCHAR(8000),Old.JobRequirements,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerJobId
                               ,'SalaryTypeId'
                               ,CONVERT(VARCHAR(8000),Old.SalaryTypeID,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerJobId
                               ,'EmployerJobTitle'
                               ,CONVERT(VARCHAR(8000),Old.EmployerJobTitle,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerJobId
                               ,'JobPostedDate'
                               ,CONVERT(VARCHAR(8000),Old.JobPostedDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerJobId
                               ,'ExpertiseId'
                               ,CONVERT(VARCHAR(8000),Old.ExpertiseId,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[plEmployerJobs_Audit_Insert] ON [dbo].[plEmployerJobs]
    FOR INSERT
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'plEmployerJobs','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerJobId
                               ,'Code'
                               ,CONVERT(VARCHAR(8000),New.Code,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerJobId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerJobId
                               ,'JobTitleId'
                               ,CONVERT(VARCHAR(8000),New.JobTitleId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerJobId
                               ,'JobDescription'
                               ,CONVERT(VARCHAR(8000),New.JobDescription,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerJobId
                               ,'JobGroupId'
                               ,CONVERT(VARCHAR(8000),New.JobGroupId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerJobId
                               ,'ContactId'
                               ,CONVERT(VARCHAR(8000),New.ContactId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerJobId
                               ,'TypeId'
                               ,CONVERT(VARCHAR(8000),New.TypeId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerJobId
                               ,'AreaId'
                               ,CONVERT(VARCHAR(8000),New.AreaId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerJobId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerJobId
                               ,'WorkDays'
                               ,CONVERT(VARCHAR(8000),New.WorkDays,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerJobId
                               ,'FeeId'
                               ,CONVERT(VARCHAR(8000),New.FeeId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerJobId
                               ,'NumberOpen'
                               ,CONVERT(VARCHAR(8000),New.NumberOpen,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerJobId
                               ,'NumberFilled'
                               ,CONVERT(VARCHAR(8000),New.NumberFilled,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerJobId
                               ,'OpenedFrom'
                               ,CONVERT(VARCHAR(8000),New.OpenedFrom,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerJobId
                               ,'OpenedTo'
                               ,CONVERT(VARCHAR(8000),New.OpenedTo,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerJobId
                               ,'Start'
                               ,CONVERT(VARCHAR(8000),New.Start,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerJobId
                               ,'SalaryFrom'
                               ,CONVERT(VARCHAR(8000),New.SalaryFrom,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerJobId
                               ,'SalaryTo'
                               ,CONVERT(VARCHAR(8000),New.SalaryTo,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerJobId
                               ,'BenefitsId'
                               ,CONVERT(VARCHAR(8000),New.BenefitsId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerJobId
                               ,'ScheduleId'
                               ,CONVERT(VARCHAR(8000),New.ScheduleId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerJobId
                               ,'HoursFrom'
                               ,CONVERT(VARCHAR(8000),New.HoursFrom,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerJobId
                               ,'HoursTo'
                               ,CONVERT(VARCHAR(8000),New.HoursTo,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerJobId
                               ,'Notes'
                               ,CONVERT(VARCHAR(8000),New.Notes,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerJobId
                               ,'EmployerId'
                               ,CONVERT(VARCHAR(8000),New.EmployerId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerJobId
                               ,'JobRequirements'
                               ,CONVERT(VARCHAR(8000),New.JobRequirements,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerJobId
                               ,'SalaryTypeId'
                               ,CONVERT(VARCHAR(8000),New.SalaryTypeID,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerJobId
                               ,'EmployerJobTitle'
                               ,CONVERT(VARCHAR(8000),New.EmployerJobTitle,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerJobId
                               ,'JobPostedDate'
                               ,CONVERT(VARCHAR(8000),New.JobPostedDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerJobId
                               ,'ExpertiseId'
                               ,CONVERT(VARCHAR(8000),New.ExpertiseId,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
CREATE TRIGGER [dbo].[plEmployerJobs_Audit_Update] ON [dbo].[plEmployerJobs] 
    FOR UPDATE 
AS 
    SET NOCOUNT ON;  
 
    DECLARE @AuditHistId AS UNIQUEIDENTIFIER;  
    DECLARE @EventRows AS INT;  
    DECLARE @EventDate AS DATETIME;  
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();  
    SET @EventRows = ( 
                       SELECT   COUNT(*) 
                       FROM     Inserted 
                     );  
    SET @EventDate = ( 
                       SELECT TOP 1 
                                ModDate 
                       FROM     Inserted 
                     );  
    SET @UserName = ( 
                      SELECT TOP 1 
                                ModUser 
                      FROM      Inserted 
                    );  
    IF @EventRows > 0 
        BEGIN  
            EXEC fmAuditHistAdd @AuditHistId,'plEmployerJobs','U',@EventRows,@EventDate,@UserName;  
            BEGIN  
                IF UPDATE(Code) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.EmployerJobId 
                                   ,'Code' 
                                   ,CONVERT(VARCHAR(8000),Old.Code,121) 
                                   ,CONVERT(VARCHAR(8000),New.Code,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.EmployerJobId = New.EmployerJobId 
                            WHERE   Old.Code <> New.Code 
                                    OR ( 
                                         Old.Code IS NULL 
                                         AND New.Code IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.Code IS NULL 
                                         AND Old.Code IS NOT NULL 
                                       );  
                IF UPDATE(StatusId) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.EmployerJobId 
                                   ,'StatusId' 
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121) 
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.EmployerJobId = New.EmployerJobId 
                            WHERE   Old.StatusId <> New.StatusId 
                                    OR ( 
                                         Old.StatusId IS NULL 
                                         AND New.StatusId IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.StatusId IS NULL 
                                         AND Old.StatusId IS NOT NULL 
                                       );  
                IF UPDATE(JobTitleId) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.EmployerJobId 
                                   ,'JobTitleId' 
                                   ,CONVERT(VARCHAR(8000),Old.JobTitleId,121) 
                                   ,CONVERT(VARCHAR(8000),New.JobTitleId,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.EmployerJobId = New.EmployerJobId 
                            WHERE   Old.JobTitleId <> New.JobTitleId 
                                    OR ( 
                                         Old.JobTitleId IS NULL 
                                         AND New.JobTitleId IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.JobTitleId IS NULL 
                                         AND Old.JobTitleId IS NOT NULL 
                                       );  
                IF UPDATE(JobDescription) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.EmployerJobId 
                                   ,'JobDescription' 
                                   ,CONVERT(VARCHAR(8000),Old.JobDescription,121) 
                                   ,CONVERT(VARCHAR(8000),New.JobDescription,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.EmployerJobId = New.EmployerJobId 
                            WHERE   Old.JobDescription <> New.JobDescription 
                                    OR ( 
                                         Old.JobDescription IS NULL 
                                         AND New.JobDescription IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.JobDescription IS NULL 
                                         AND Old.JobDescription IS NOT NULL 
                                       );  
                IF UPDATE(JobGroupId) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.EmployerJobId 
                                   ,'JobGroupId' 
                                   ,CONVERT(VARCHAR(8000),Old.JobGroupId,121) 
                                   ,CONVERT(VARCHAR(8000),New.JobGroupId,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.EmployerJobId = New.EmployerJobId 
                            WHERE   Old.JobGroupId <> New.JobGroupId 
                                    OR ( 
                                         Old.JobGroupId IS NULL 
                                         AND New.JobGroupId IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.JobGroupId IS NULL 
                                         AND Old.JobGroupId IS NOT NULL 
                                       );  
                IF UPDATE(ContactId) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.EmployerJobId 
                                   ,'ContactId' 
                                   ,CONVERT(VARCHAR(8000),Old.ContactId,121) 
                                   ,CONVERT(VARCHAR(8000),New.ContactId,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.EmployerJobId = New.EmployerJobId 
                            WHERE   Old.ContactId <> New.ContactId 
                                    OR ( 
                                         Old.ContactId IS NULL 
                                         AND New.ContactId IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.ContactId IS NULL 
                                         AND Old.ContactId IS NOT NULL 
                                       );  
                IF UPDATE(TypeId) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.EmployerJobId 
                                   ,'TypeId' 
                                   ,CONVERT(VARCHAR(8000),Old.TypeId,121) 
                                   ,CONVERT(VARCHAR(8000),New.TypeId,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.EmployerJobId = New.EmployerJobId 
                            WHERE   Old.TypeId <> New.TypeId 
                                    OR ( 
                                         Old.TypeId IS NULL 
                                         AND New.TypeId IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.TypeId IS NULL 
                                         AND Old.TypeId IS NOT NULL 
                                       );  
                IF UPDATE(AreaId) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.EmployerJobId 
                                   ,'AreaId' 
                                   ,CONVERT(VARCHAR(8000),Old.AreaId,121) 
                                   ,CONVERT(VARCHAR(8000),New.AreaId,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.EmployerJobId = New.EmployerJobId 
                            WHERE   Old.AreaId <> New.AreaId 
                                    OR ( 
                                         Old.AreaId IS NULL 
                                         AND New.AreaId IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.AreaId IS NULL 
                                         AND Old.AreaId IS NOT NULL 
                                       );  
                IF UPDATE(CampGrpId) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.EmployerJobId 
                                   ,'CampGrpId' 
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121) 
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.EmployerJobId = New.EmployerJobId 
                            WHERE   Old.CampGrpId <> New.CampGrpId 
                                    OR ( 
                                         Old.CampGrpId IS NULL 
                                         AND New.CampGrpId IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.CampGrpId IS NULL 
                                         AND Old.CampGrpId IS NOT NULL 
                                       );  
                IF UPDATE(WorkDays) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.EmployerJobId 
                                   ,'WorkDays' 
                                   ,CONVERT(VARCHAR(8000),Old.WorkDays,121) 
                                   ,CONVERT(VARCHAR(8000),New.WorkDays,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.EmployerJobId = New.EmployerJobId 
                            WHERE   Old.WorkDays <> New.WorkDays 
                                    OR ( 
                                         Old.WorkDays IS NULL 
                                         AND New.WorkDays IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.WorkDays IS NULL 
                                         AND Old.WorkDays IS NOT NULL 
                                       );  
                IF UPDATE(FeeId) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.EmployerJobId 
                                   ,'FeeId' 
                                   ,CONVERT(VARCHAR(8000),Old.FeeId,121) 
                                   ,CONVERT(VARCHAR(8000),New.FeeId,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.EmployerJobId = New.EmployerJobId 
                            WHERE   Old.FeeId <> New.FeeId 
                                    OR ( 
                                         Old.FeeId IS NULL 
                                         AND New.FeeId IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.FeeId IS NULL 
                                         AND Old.FeeId IS NOT NULL 
                                       );  
                IF UPDATE(NumberOpen) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.EmployerJobId 
                                   ,'NumberOpen' 
                                   ,CONVERT(VARCHAR(8000),Old.NumberOpen,121) 
                                   ,CONVERT(VARCHAR(8000),New.NumberOpen,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.EmployerJobId = New.EmployerJobId 
                            WHERE   Old.NumberOpen <> New.NumberOpen 
                                    OR ( 
                                         Old.NumberOpen IS NULL 
                                         AND New.NumberOpen IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.NumberOpen IS NULL 
                                         AND Old.NumberOpen IS NOT NULL 
                                       );  
                IF UPDATE(NumberFilled) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.EmployerJobId 
                                   ,'NumberFilled' 
                                   ,CONVERT(VARCHAR(8000),Old.NumberFilled,121) 
                                   ,CONVERT(VARCHAR(8000),New.NumberFilled,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.EmployerJobId = New.EmployerJobId 
                            WHERE   Old.NumberFilled <> New.NumberFilled 
                                    OR ( 
                                         Old.NumberFilled IS NULL 
                                         AND New.NumberFilled IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.NumberFilled IS NULL 
                                         AND Old.NumberFilled IS NOT NULL 
                                       );  
                IF UPDATE(OpenedFrom) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.EmployerJobId 
                                   ,'OpenedFrom' 
                                   ,CONVERT(VARCHAR(8000),Old.OpenedFrom,121) 
                                   ,CONVERT(VARCHAR(8000),New.OpenedFrom,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.EmployerJobId = New.EmployerJobId 
                            WHERE   Old.OpenedFrom <> New.OpenedFrom 
                                    OR ( 
                                         Old.OpenedFrom IS NULL 
                                         AND New.OpenedFrom IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.OpenedFrom IS NULL 
                                         AND Old.OpenedFrom IS NOT NULL 
                                       );  
                IF UPDATE(OpenedTo) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.EmployerJobId 
                                   ,'OpenedTo' 
                                   ,CONVERT(VARCHAR(8000),Old.OpenedTo,121) 
                                   ,CONVERT(VARCHAR(8000),New.OpenedTo,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.EmployerJobId = New.EmployerJobId 
                            WHERE   Old.OpenedTo <> New.OpenedTo 
                                    OR ( 
                                         Old.OpenedTo IS NULL 
                                         AND New.OpenedTo IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.OpenedTo IS NULL 
                                         AND Old.OpenedTo IS NOT NULL 
                                       );  
                IF UPDATE(Start) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.EmployerJobId 
                                   ,'Start' 
                                   ,CONVERT(VARCHAR(8000),Old.Start,121) 
                                   ,CONVERT(VARCHAR(8000),New.Start,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.EmployerJobId = New.EmployerJobId 
                            WHERE   Old.Start <> New.Start 
                                    OR ( 
                                         Old.Start IS NULL 
                                         AND New.Start IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.Start IS NULL 
                                         AND Old.Start IS NOT NULL 
                                       );  
                IF UPDATE(SalaryFrom) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.EmployerJobId 
                                   ,'SalaryFrom' 
                                   ,CONVERT(VARCHAR(8000),Old.SalaryFrom,121) 
                                   ,CONVERT(VARCHAR(8000),New.SalaryFrom,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.EmployerJobId = New.EmployerJobId 
                            WHERE   Old.SalaryFrom <> New.SalaryFrom 
                                    OR ( 
                                         Old.SalaryFrom IS NULL 
                                         AND New.SalaryFrom IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.SalaryFrom IS NULL 
                                         AND Old.SalaryFrom IS NOT NULL 
                                       );  
                IF UPDATE(SalaryTo) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.EmployerJobId 
                                   ,'SalaryTo' 
                                   ,CONVERT(VARCHAR(8000),Old.SalaryTo,121) 
                                   ,CONVERT(VARCHAR(8000),New.SalaryTo,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.EmployerJobId = New.EmployerJobId 
                            WHERE   Old.SalaryTo <> New.SalaryTo 
                                    OR ( 
                                         Old.SalaryTo IS NULL 
                                         AND New.SalaryTo IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.SalaryTo IS NULL 
                                         AND Old.SalaryTo IS NOT NULL 
                                       );  
                IF UPDATE(BenefitsId) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.EmployerJobId 
                                   ,'BenefitsId' 
                                   ,CONVERT(VARCHAR(8000),Old.BenefitsId,121) 
                                   ,CONVERT(VARCHAR(8000),New.BenefitsId,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.EmployerJobId = New.EmployerJobId 
                            WHERE   Old.BenefitsId <> New.BenefitsId 
                                    OR ( 
                                         Old.BenefitsId IS NULL 
                                         AND New.BenefitsId IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.BenefitsId IS NULL 
                                         AND Old.BenefitsId IS NOT NULL 
                                       );  
                IF UPDATE(ScheduleId) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.EmployerJobId 
                                   ,'ScheduleId' 
                                   ,CONVERT(VARCHAR(8000),Old.ScheduleId,121) 
                                   ,CONVERT(VARCHAR(8000),New.ScheduleId,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.EmployerJobId = New.EmployerJobId 
                            WHERE   Old.ScheduleId <> New.ScheduleId 
                                    OR ( 
                                         Old.ScheduleId IS NULL 
                                         AND New.ScheduleId IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.ScheduleId IS NULL 
                                         AND Old.ScheduleId IS NOT NULL 
                                       );  
                IF UPDATE(HoursFrom) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.EmployerJobId 
                                   ,'HoursFrom' 
                                   ,CONVERT(VARCHAR(8000),Old.HoursFrom,121) 
                                   ,CONVERT(VARCHAR(8000),New.HoursFrom,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.EmployerJobId = New.EmployerJobId 
                            WHERE   Old.HoursFrom <> New.HoursFrom 
                                    OR ( 
                                         Old.HoursFrom IS NULL 
                                         AND New.HoursFrom IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.HoursFrom IS NULL 
                                         AND Old.HoursFrom IS NOT NULL 
                                       );  
                IF UPDATE(HoursTo) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.EmployerJobId 
                                   ,'HoursTo' 
                                   ,CONVERT(VARCHAR(8000),Old.HoursTo,121) 
                                   ,CONVERT(VARCHAR(8000),New.HoursTo,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.EmployerJobId = New.EmployerJobId 
                            WHERE   Old.HoursTo <> New.HoursTo 
                                    OR ( 
                                         Old.HoursTo IS NULL 
                                         AND New.HoursTo IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.HoursTo IS NULL 
                                         AND Old.HoursTo IS NOT NULL 
                                       );  
                IF UPDATE(Notes) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.EmployerJobId 
                                   ,'Notes' 
                                   ,CONVERT(VARCHAR(8000),Old.Notes,121) 
                                   ,CONVERT(VARCHAR(8000),New.Notes,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.EmployerJobId = New.EmployerJobId 
                            WHERE   Old.Notes <> New.Notes 
                                    OR ( 
                                         Old.Notes IS NULL 
                                         AND New.Notes IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.Notes IS NULL 
                                         AND Old.Notes IS NOT NULL 
                                       );  
                IF UPDATE(EmployerId) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.EmployerJobId 
                                   ,'EmployerId' 
                                   ,CONVERT(VARCHAR(8000),Old.EmployerId,121) 
                                   ,CONVERT(VARCHAR(8000),New.EmployerId,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.EmployerJobId = New.EmployerJobId 
                            WHERE   Old.EmployerId <> New.EmployerId 
                                    OR ( 
                                         Old.EmployerId IS NULL 
                                         AND New.EmployerId IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.EmployerId IS NULL 
                                         AND Old.EmployerId IS NOT NULL 
                                       );  
                IF UPDATE(JobRequirements) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.EmployerJobId 
                                   ,'JobRequirements' 
                                   ,CONVERT(VARCHAR(8000),Old.JobRequirements,121) 
                                   ,CONVERT(VARCHAR(8000),New.JobRequirements,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.EmployerJobId = New.EmployerJobId 
                            WHERE   Old.JobRequirements <> New.JobRequirements 
                                    OR ( 
                                         Old.JobRequirements IS NULL 
                                         AND New.JobRequirements IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.JobRequirements IS NULL 
                                         AND Old.JobRequirements IS NOT NULL 
                                       );  
                IF UPDATE(SalaryTypeId) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.EmployerJobId 
                                   ,'SalaryTypeId' 
                                   ,CONVERT(VARCHAR(8000),Old.SalaryTypeID,121) 
                                   ,CONVERT(VARCHAR(8000),New.SalaryTypeID,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.EmployerJobId = New.EmployerJobId 
                            WHERE   Old.SalaryTypeID <> New.SalaryTypeID 
                                    OR ( 
                                         Old.SalaryTypeID IS NULL 
                                         AND New.SalaryTypeID IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.SalaryTypeID IS NULL 
                                         AND Old.SalaryTypeID IS NOT NULL 
                                       );  
                IF UPDATE(EmployerJobTitle) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.EmployerJobId 
                                   ,'EmployerJobTitle' 
                                   ,CONVERT(VARCHAR(8000),Old.EmployerJobTitle,121) 
                                   ,CONVERT(VARCHAR(8000),New.EmployerJobTitle,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.EmployerJobId = New.EmployerJobId 
                            WHERE   Old.EmployerJobTitle <> New.EmployerJobTitle 
                                    OR ( 
                                         Old.EmployerJobTitle IS NULL 
                                         AND New.EmployerJobTitle IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.EmployerJobTitle IS NULL 
                                         AND Old.EmployerJobTitle IS NOT NULL 
                                       );  
                IF UPDATE(JobPostedDate) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.EmployerJobId 
                                   ,'JobPostedDate' 
                                   ,CONVERT(VARCHAR(8000),Old.JobPostedDate,121) 
                                   ,CONVERT(VARCHAR(8000),New.JobPostedDate,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.EmployerJobId = New.EmployerJobId 
                            WHERE   Old.JobPostedDate <> New.JobPostedDate 
                                    OR ( 
                                         Old.JobPostedDate IS NULL 
                                         AND New.JobPostedDate IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.JobPostedDate IS NULL 
                                         AND Old.JobPostedDate IS NOT NULL 
                                       );  
                IF UPDATE(ExpertiseId) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.EmployerJobId 
                                   ,'ExpertiseId' 
                                   ,CONVERT(VARCHAR(8000),Old.ExpertiseId,121) 
                                   ,CONVERT(VARCHAR(8000),New.ExpertiseId,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.EmployerJobId = New.EmployerJobId 
                            WHERE   Old.ExpertiseId <> New.ExpertiseId 
                                    OR ( 
                                         Old.ExpertiseId IS NULL 
                                         AND New.ExpertiseId IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.ExpertiseId IS NULL 
                                         AND Old.ExpertiseId IS NOT NULL 
                                       );  
            END;  
        END;  
 
 
    SET NOCOUNT OFF;  
GO
ALTER TABLE [dbo].[plEmployerJobs] ADD CONSTRAINT [PK_plEmployerJobs_EmployerJobId] PRIMARY KEY CLUSTERED  ([EmployerJobId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_plEmployerJobs_Code_EmployerJobTitle_JobTitleId_EmployerId_CampGrpId] ON [dbo].[plEmployerJobs] ([Code], [EmployerJobTitle], [JobTitleId], [EmployerId], [CampGrpId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[plEmployerJobs] ADD CONSTRAINT [FK_plEmployerJobs_adCounties_AreaId_CountyId] FOREIGN KEY ([AreaId]) REFERENCES [dbo].[adCounties] ([CountyId])
GO
ALTER TABLE [dbo].[plEmployerJobs] ADD CONSTRAINT [FK_plEmployerJobs_adExpertiseLevel_ExpertiseId_ExpertiseId] FOREIGN KEY ([ExpertiseId]) REFERENCES [dbo].[adExpertiseLevel] ([ExpertiseId])
GO
ALTER TABLE [dbo].[plEmployerJobs] ADD CONSTRAINT [FK_plEmployerJobs_adTitles_JobTitleId_TitleId] FOREIGN KEY ([JobTitleId]) REFERENCES [dbo].[adTitles] ([TitleId])
GO
ALTER TABLE [dbo].[plEmployerJobs] ADD CONSTRAINT [FK_plEmployerJobs_plEmployerContact_ContactId_EmployerContactId] FOREIGN KEY ([ContactId]) REFERENCES [dbo].[plEmployerContact] ([EmployerContactId])
GO
ALTER TABLE [dbo].[plEmployerJobs] ADD CONSTRAINT [FK_plEmployerJobs_plEmployers_EmployerId_EmployerId] FOREIGN KEY ([EmployerId]) REFERENCES [dbo].[plEmployers] ([EmployerId])
GO
ALTER TABLE [dbo].[plEmployerJobs] ADD CONSTRAINT [FK_plEmployerJobs_plFee_FeeId_FeeId] FOREIGN KEY ([FeeId]) REFERENCES [dbo].[plFee] ([FeeId])
GO
ALTER TABLE [dbo].[plEmployerJobs] ADD CONSTRAINT [FK_plEmployerJobs_plJobBenefit_BenefitsId_JobBenefitId] FOREIGN KEY ([BenefitsId]) REFERENCES [dbo].[plJobBenefit] ([JobBenefitId])
GO
ALTER TABLE [dbo].[plEmployerJobs] ADD CONSTRAINT [FK_plEmployerJobs_plJobCats_JobGroupId_JobCatId] FOREIGN KEY ([JobGroupId]) REFERENCES [dbo].[plJobCats] ([JobCatId])
GO
ALTER TABLE [dbo].[plEmployerJobs] ADD CONSTRAINT [FK_plEmployerJobs_plJobSchedule_ScheduleId_JobScheduleId] FOREIGN KEY ([ScheduleId]) REFERENCES [dbo].[plJobSchedule] ([JobScheduleId])
GO
ALTER TABLE [dbo].[plEmployerJobs] ADD CONSTRAINT [FK_plEmployerJobs_plJobType_TypeId_JobGroupId] FOREIGN KEY ([TypeId]) REFERENCES [dbo].[plJobType] ([JobGroupId])
GO
ALTER TABLE [dbo].[plEmployerJobs] ADD CONSTRAINT [FK_plEmployerJobs_plSalaryType_SalaryTypeID_SalaryTypeId] FOREIGN KEY ([SalaryTypeID]) REFERENCES [dbo].[plSalaryType] ([SalaryTypeId])
GO
ALTER TABLE [dbo].[plEmployerJobs] ADD CONSTRAINT [FK_plEmployerJobs_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[plEmployerJobs] ADD CONSTRAINT [FK_plEmployerJobs_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
