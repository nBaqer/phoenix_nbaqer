CREATE TABLE [dbo].[adLeadDuplicates]
(
[IdDuplicates] [int] NOT NULL IDENTITY(1, 1),
[NewLeadGuid] [uniqueidentifier] NOT NULL,
[PosibleDuplicateGuid] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adLeadDuplicates] ADD CONSTRAINT [PK_adLeadDuplicates_IdDuplicates] PRIMARY KEY CLUSTERED  ([IdDuplicates]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adLeadDuplicates] ADD CONSTRAINT [FK_adLeadDuplicates_adLeads_NewLeadGuid_LeadId] FOREIGN KEY ([NewLeadGuid]) REFERENCES [dbo].[adLeads] ([LeadId])
GO
ALTER TABLE [dbo].[adLeadDuplicates] ADD CONSTRAINT [FK_adLeadDuplicates_adLeads_PosibleDuplicateGuid_LeadId] FOREIGN KEY ([PosibleDuplicateGuid]) REFERENCES [dbo].[adLeads] ([LeadId])
GO
