CREATE TABLE [dbo].[adLeadGrpReqGroups]
(
[LeadGrpReqGrpId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_adLeadGrpReqGroups_LeadGrpReqGrpId] DEFAULT (newid()),
[ReqGrpId] [uniqueidentifier] NULL,
[LeadGrpId] [uniqueidentifier] NULL,
[NumReqs] [int] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[StatusId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_adLeadGrpReqGroups_StatusId] DEFAULT ([dbo].[UDF_GetSyStatusesValue]('A'))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[adLeadGrpReqGroups_Audit_Delete] ON [dbo].[adLeadGrpReqGroups]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adLeadGrpReqGroups','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadGrpReqGrpId
                               ,'ReqGrpId'
                               ,CONVERT(VARCHAR(8000),Old.ReqGrpId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadGrpReqGrpId
                               ,'NumReqs'
                               ,CONVERT(VARCHAR(8000),Old.NumReqs,121)
                        FROM    Deleted Old; 
            END; 
        END; 




    SET NOCOUNT OFF; 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[adLeadGrpReqGroups_Audit_Insert] ON [dbo].[adLeadGrpReqGroups]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adLeadGrpReqGroups','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadGrpReqGrpId
                               ,'ReqGrpId'
                               ,CONVERT(VARCHAR(8000),New.ReqGrpId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadGrpReqGrpId
                               ,'NumReqs'
                               ,CONVERT(VARCHAR(8000),New.NumReqs,121)
                        FROM    Inserted New; 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[adLeadGrpReqGroups_Audit_Update] ON [dbo].[adLeadGrpReqGroups]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adLeadGrpReqGroups','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(ReqGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.LeadGrpReqGrpId
                                   ,'ReqGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.ReqGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.ReqGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.LeadGrpReqGrpId = New.LeadGrpReqGrpId
                            WHERE   Old.ReqGrpId <> New.ReqGrpId
                                    OR (
                                         Old.ReqGrpId IS NULL
                                         AND New.ReqGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.ReqGrpId IS NULL
                                         AND Old.ReqGrpId IS NOT NULL
                                       ); 
                IF UPDATE(NumReqs)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.LeadGrpReqGrpId
                                   ,'NumReqs'
                                   ,CONVERT(VARCHAR(8000),Old.NumReqs,121)
                                   ,CONVERT(VARCHAR(8000),New.NumReqs,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.LeadGrpReqGrpId = New.LeadGrpReqGrpId
                            WHERE   Old.NumReqs <> New.NumReqs
                                    OR (
                                         Old.NumReqs IS NULL
                                         AND New.NumReqs IS NOT NULL
                                       )
                                    OR (
                                         New.NumReqs IS NULL
                                         AND Old.NumReqs IS NOT NULL
                                       ); 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO
ALTER TABLE [dbo].[adLeadGrpReqGroups] ADD CONSTRAINT [PK_adLeadGrpReqGroups_LeadGrpReqGrpId] PRIMARY KEY CLUSTERED  ([LeadGrpReqGrpId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_adLeadGrpReqGroups_ReqGrpId_LeadGrpId] ON [dbo].[adLeadGrpReqGroups] ([ReqGrpId], [LeadGrpId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adLeadGrpReqGroups] ADD CONSTRAINT [FK_adLeadGrpReqGroups_adLeadGroups_LeadGrpId_LeadGrpId] FOREIGN KEY ([LeadGrpId]) REFERENCES [dbo].[adLeadGroups] ([LeadGrpId])
GO
ALTER TABLE [dbo].[adLeadGrpReqGroups] ADD CONSTRAINT [FK_adLeadGrpReqGroups_adReqGroups_ReqGrpId_ReqGrpId] FOREIGN KEY ([ReqGrpId]) REFERENCES [dbo].[adReqGroups] ([ReqGrpId])
GO
ALTER TABLE [dbo].[adLeadGrpReqGroups] ADD CONSTRAINT [FK_adLeadGrpReqGroups_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
