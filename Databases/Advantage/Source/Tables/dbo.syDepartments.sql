CREATE TABLE [dbo].[syDepartments]
(
[DepartmentId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syDepartments_DepartmentId] DEFAULT (newid()),
[DepartmentCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[DepartmentDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syDepartments] ADD CONSTRAINT [PK_syDepartments_DepartmentId] PRIMARY KEY CLUSTERED  ([DepartmentId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syDepartments] ADD CONSTRAINT [FK_syDepartments_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[syDepartments] ADD CONSTRAINT [FK_syDepartments_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
