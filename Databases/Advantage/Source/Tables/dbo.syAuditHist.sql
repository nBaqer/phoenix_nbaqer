CREATE TABLE [dbo].[syAuditHist]
(
[AuditHistId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syAuditHist_AuditHistId] DEFAULT (newid()),
[TableName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Event] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EventRows] [int] NOT NULL,
[EventDate] [datetime] NULL CONSTRAINT [DF_syAuditHist_EventDate] DEFAULT (getdate()),
[AppName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
CREATE NONCLUSTERED INDEX [IX_syAuditHist_EventDate] ON [dbo].[syAuditHist] ([EventDate]) ON [PRIMARY]

GO
ALTER TABLE [dbo].[syAuditHist] ADD CONSTRAINT [PK_syAuditHist_AuditHistId] PRIMARY KEY CLUSTERED  ([AuditHistId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_syAuditHist_Event] ON [dbo].[syAuditHist] ([Event]) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX_syAuditHist_TableName] ON [dbo].[syAuditHist] ([TableName]) ON [PRIMARY]
GO
