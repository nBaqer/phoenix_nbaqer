CREATE TABLE [dbo].[syResourceTypes]
(
[ResourceTypeID] [tinyint] NOT NULL,
[ResourceType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syResourceTypes] ADD CONSTRAINT [PK_syResourceTypes_ResourceTypeID] PRIMARY KEY CLUSTERED  ([ResourceTypeID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[syResourceTypes] TO [AdvantageRole]
GO
