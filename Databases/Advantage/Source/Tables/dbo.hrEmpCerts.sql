CREATE TABLE [dbo].[hrEmpCerts]
(
[EmpCertId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_hrEmpCerts_EmpCertId] DEFAULT (newid()),
[EmpId] [uniqueidentifier] NOT NULL,
[CertId] [uniqueidentifier] NOT NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[hrEmpCerts] ADD CONSTRAINT [PK_hrEmpCerts_EmpCertId] PRIMARY KEY CLUSTERED  ([EmpCertId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[hrEmpCerts] ADD CONSTRAINT [FK_hrEmpCerts_syCertifications_CertId_CertificationId] FOREIGN KEY ([CertId]) REFERENCES [dbo].[syCertifications] ([CertificationId])
GO
ALTER TABLE [dbo].[hrEmpCerts] ADD CONSTRAINT [FK_hrEmpCerts_hrEmployees_EmpId_EmpId] FOREIGN KEY ([EmpId]) REFERENCES [dbo].[hrEmployees] ([EmpId])
GO
