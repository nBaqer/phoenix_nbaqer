CREATE TABLE [dbo].[syEDExpNotPostPell_ACG_SMART_Teach_DisbursementTable]
(
[DetailId] [uniqueidentifier] NOT NULL,
[ParentId] [uniqueidentifier] NOT NULL,
[AwardScheduleId] [uniqueidentifier] NULL,
[DbIn] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Filter] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccDisbAmount] [decimal] (19, 4) NULL,
[DisbDate] [datetime] NULL,
[DisbNum] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisbRelIndi] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DusbSeqNum] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SimittedDisbAmount] [decimal] (19, 4) NULL,
[ActionStatusDisb] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginalSSN] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[FileName] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syEDExpNotPostPell_ACG_SMART_Teach_DisbursementTable] ADD CONSTRAINT [PK_syEDExpNotPostPell_ACG_SMART_Teach_DisbursementTable_DetailId] PRIMARY KEY CLUSTERED  ([DetailId]) ON [PRIMARY]
GO
