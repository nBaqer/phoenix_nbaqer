CREATE TABLE [dbo].[syNotesPageFields]
(
[PageFieldId] [int] NOT NULL,
[PageName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FieldCaption] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL CONSTRAINT [DF_syNotesPageFields_ModDate] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syNotesPageFields] ADD CONSTRAINT [PK_syNotesPageFields_PageFieldId] PRIMARY KEY CLUSTERED  ([PageFieldId]) ON [PRIMARY]
GO
