CREATE TABLE [dbo].[adCountries]
(
[CountryId] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[CountryCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[CountryDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[IsDefault] [bit] NULL CONSTRAINT [DF_adCountries_IsDefault] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adCountries] ADD CONSTRAINT [PK_adCountries_CountryId] PRIMARY KEY CLUSTERED  ([CountryId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_adCountries_CountryCode_CountryDescrip] ON [dbo].[adCountries] ([CountryCode], [CountryDescrip]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adCountries] ADD CONSTRAINT [FK_adCountries_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[adCountries] ADD CONSTRAINT [FK_adCountries_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
