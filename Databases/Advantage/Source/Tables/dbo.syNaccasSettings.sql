CREATE TABLE [dbo].[syNaccasSettings]
(
[CampusId] [uniqueidentifier] NOT NULL,
[CreatedById] [uniqueidentifier] NULL,
[CreateDate] [datetime2] NOT NULL,
[NaccasSettingId] [int] NOT NULL IDENTITY(1, 1),
[ModUserId] [uniqueidentifier] NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syNaccasSettings] ADD CONSTRAINT [PK_syNaccasSettings_NaccasSettingId] PRIMARY KEY CLUSTERED  ([NaccasSettingId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syNaccasSettings] ADD CONSTRAINT [FK_syNaccasSettings_syCampuses_CampusId_CampusId] FOREIGN KEY ([CampusId]) REFERENCES [dbo].[syCampuses] ([CampusId])
GO
ALTER TABLE [dbo].[syNaccasSettings] ADD CONSTRAINT [FK__syNaccasS__ModUs__0E4F284F] FOREIGN KEY ([ModUserId]) REFERENCES [dbo].[syUsers] ([UserId])
GO
