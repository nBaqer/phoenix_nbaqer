CREATE TABLE [dbo].[atOnLineAttendance]
(
[OnLineStudentId] [int] NOT NULL,
[CourseShortName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AttendedDate] [datetime] NOT NULL,
[Minutes] [int] NOT NULL,
[OnLineTermId] [int] NOT NULL,
[OnlineAttendanceId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_atOnLineAttendance_OnlineAttendanceId] DEFAULT (newsequentialid())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[atOnLineAttendance] ADD CONSTRAINT [PK_atOnLineAttendance_OnlineAttendanceId] PRIMARY KEY CLUSTERED  ([OnlineAttendanceId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_atOnLineAttendance_OnLineTermId_CourseShortName_OnLineStudentId_AttendedDate_Minutes] ON [dbo].[atOnLineAttendance] ([OnLineTermId], [CourseShortName], [OnLineStudentId], [AttendedDate], [Minutes]) ON [PRIMARY]
GO
