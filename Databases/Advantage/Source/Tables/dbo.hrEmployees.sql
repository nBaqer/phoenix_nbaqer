CREATE TABLE [dbo].[hrEmployees]
(
[EmpId] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[StatusId] [uniqueidentifier] NOT NULL,
[LastName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FirstName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MI] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Code] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrefixId] [uniqueidentifier] NULL,
[SuffixId] [uniqueidentifier] NULL,
[SSN] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ID] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BirthDate] [smalldatetime] NULL,
[RaceId] [uniqueidentifier] NULL,
[GenderId] [uniqueidentifier] NULL,
[MaritalStatId] [uniqueidentifier] NULL,
[UserName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Faculty] [bit] NOT NULL CONSTRAINT [DF_hrEmployees_Faculty] DEFAULT ((0)),
[Address1] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateId] [uniqueidentifier] NULL,
[Zip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CountryId] [uniqueidentifier] NULL,
[ForeignZip] [bit] NOT NULL,
[OtherState] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampusId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[hrEmployees] ADD CONSTRAINT [PK_hrEmployees_EmpId] PRIMARY KEY CLUSTERED  ([EmpId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[hrEmployees] ADD CONSTRAINT [FK_hrEmployees_syCampuses_CampusId_CampusId] FOREIGN KEY ([CampusId]) REFERENCES [dbo].[syCampuses] ([CampusId])
GO
ALTER TABLE [dbo].[hrEmployees] ADD CONSTRAINT [FK_hrEmployees_adCountries_CountryId_CountryId] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[adCountries] ([CountryId])
GO
ALTER TABLE [dbo].[hrEmployees] ADD CONSTRAINT [FK_hrEmployees_adGenders_GenderId_GenderId] FOREIGN KEY ([GenderId]) REFERENCES [dbo].[adGenders] ([GenderId])
GO
ALTER TABLE [dbo].[hrEmployees] ADD CONSTRAINT [FK_hrEmployees_adMaritalStatus_MaritalStatId_MaritalStatId] FOREIGN KEY ([MaritalStatId]) REFERENCES [dbo].[adMaritalStatus] ([MaritalStatId])
GO
ALTER TABLE [dbo].[hrEmployees] ADD CONSTRAINT [FK_hrEmployees_syPrefixes_PrefixId_PrefixId] FOREIGN KEY ([PrefixId]) REFERENCES [dbo].[syPrefixes] ([PrefixId])
GO
ALTER TABLE [dbo].[hrEmployees] ADD CONSTRAINT [FK_hrEmployees_adEthCodes_RaceId_EthCodeId] FOREIGN KEY ([RaceId]) REFERENCES [dbo].[adEthCodes] ([EthCodeId])
GO
ALTER TABLE [dbo].[hrEmployees] ADD CONSTRAINT [FK_hrEmployees_syStates_StateId_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[syStates] ([StateId])
GO
ALTER TABLE [dbo].[hrEmployees] ADD CONSTRAINT [FK_hrEmployees_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
ALTER TABLE [dbo].[hrEmployees] ADD CONSTRAINT [FK_hrEmployees_sySuffixes_SuffixId_SuffixId] FOREIGN KEY ([SuffixId]) REFERENCES [dbo].[sySuffixes] ([SuffixId])
GO
