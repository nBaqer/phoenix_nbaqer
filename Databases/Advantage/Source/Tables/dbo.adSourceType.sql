CREATE TABLE [dbo].[adSourceType]
(
[SourceTypeId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_adSourceType_SourceTypeId] DEFAULT (newid()),
[SourceTypeDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SourceCatagoryId] [uniqueidentifier] NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[SourceTypeCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[adSourceType_Audit_Delete] ON [dbo].[adSourceType]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adSourceType','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SourceTypeId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SourceTypeId
                               ,'SourceTypeCode'
                               ,CONVERT(VARCHAR(8000),Old.SourceTypeCode,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SourceTypeId
                               ,'SourceTypeDescrip'
                               ,CONVERT(VARCHAR(8000),Old.SourceTypeDescrip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SourceTypeId
                               ,'SourceCatagoryID'
                               ,CONVERT(VARCHAR(8000),Old.SourceCatagoryId,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[adSourceType_Audit_Insert] ON [dbo].[adSourceType]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adSourceType','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SourceTypeId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SourceTypeId
                               ,'SourceTypeCode'
                               ,CONVERT(VARCHAR(8000),New.SourceTypeCode,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SourceTypeId
                               ,'SourceTypeDescrip'
                               ,CONVERT(VARCHAR(8000),New.SourceTypeDescrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SourceTypeId
                               ,'SourceCatagoryID'
                               ,CONVERT(VARCHAR(8000),New.SourceCatagoryId,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[adSourceType_Audit_Update] ON [dbo].[adSourceType]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adSourceType','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SourceTypeId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SourceTypeId = New.SourceTypeId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(SourceTypeCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SourceTypeId
                                   ,'SourceTypeCode'
                                   ,CONVERT(VARCHAR(8000),Old.SourceTypeCode,121)
                                   ,CONVERT(VARCHAR(8000),New.SourceTypeCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SourceTypeId = New.SourceTypeId
                            WHERE   Old.SourceTypeCode <> New.SourceTypeCode
                                    OR (
                                         Old.SourceTypeCode IS NULL
                                         AND New.SourceTypeCode IS NOT NULL
                                       )
                                    OR (
                                         New.SourceTypeCode IS NULL
                                         AND Old.SourceTypeCode IS NOT NULL
                                       ); 
                IF UPDATE(SourceTypeDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SourceTypeId
                                   ,'SourceTypeDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.SourceTypeDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.SourceTypeDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SourceTypeId = New.SourceTypeId
                            WHERE   Old.SourceTypeDescrip <> New.SourceTypeDescrip
                                    OR (
                                         Old.SourceTypeDescrip IS NULL
                                         AND New.SourceTypeDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.SourceTypeDescrip IS NULL
                                         AND Old.SourceTypeDescrip IS NOT NULL
                                       ); 
                IF UPDATE(SourceCatagoryId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SourceTypeId
                                   ,'SourceCatagoryID'
                                   ,CONVERT(VARCHAR(8000),Old.SourceCatagoryId,121)
                                   ,CONVERT(VARCHAR(8000),New.SourceCatagoryId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SourceTypeId = New.SourceTypeId
                            WHERE   Old.SourceCatagoryId <> New.SourceCatagoryId
                                    OR (
                                         Old.SourceCatagoryId IS NULL
                                         AND New.SourceCatagoryId IS NOT NULL
                                       )
                                    OR (
                                         New.SourceCatagoryId IS NULL
                                         AND Old.SourceCatagoryId IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO
ALTER TABLE [dbo].[adSourceType] ADD CONSTRAINT [PK_adSourceType_SourceTypeId] PRIMARY KEY CLUSTERED  ([SourceTypeId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_adSourceType_SourceTypeCode_SourceTypeDescrip] ON [dbo].[adSourceType] ([SourceTypeCode], [SourceTypeDescrip]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adSourceType] ADD CONSTRAINT [FK_adSourceType_adSourceCatagory_SourceCatagoryId_SourceCatagoryId] FOREIGN KEY ([SourceCatagoryId]) REFERENCES [dbo].[adSourceCatagory] ([SourceCatagoryId])
GO
ALTER TABLE [dbo].[adSourceType] ADD CONSTRAINT [FK_adSourceType_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[adSourceType] ADD CONSTRAINT [FK_adSourceType_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
