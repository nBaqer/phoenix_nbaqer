CREATE TABLE [dbo].[arOverridenConflicts]
(
[OverridenConflictId] [uniqueidentifier] NOT NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeftClsSectionId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_arOverridenConflicts_LeftClsSectionId] DEFAULT (newid()),
[RightClsSectionId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_arOverridenConflicts_RightClsSectionId] DEFAULT (newid())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arOverridenConflicts] ADD CONSTRAINT [PK_arOverridenConflicts_OverridenConflictId] PRIMARY KEY CLUSTERED  ([OverridenConflictId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arOverridenConflicts] ADD CONSTRAINT [FK_arOverridenConflicts_arClassSections_LeftClsSectionId_ClsSectionId] FOREIGN KEY ([LeftClsSectionId]) REFERENCES [dbo].[arClassSections] ([ClsSectionId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[arOverridenConflicts] ADD CONSTRAINT [FK_arOverridenConflicts_arClassSections_RightClsSectionId_ClsSectionId] FOREIGN KEY ([RightClsSectionId]) REFERENCES [dbo].[arClassSections] ([ClsSectionId])
GO
