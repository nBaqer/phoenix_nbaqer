CREATE TABLE [dbo].[plFldStudy]
(
[FldStudyId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_plFldStudy_FldStudyId] DEFAULT (newid()),
[FldStudyDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FldStudyCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[CampGrpId] [uniqueidentifier] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[plFldStudy] ADD CONSTRAINT [PK_plFldStudy_FldStudyId] PRIMARY KEY CLUSTERED  ([FldStudyId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[plFldStudy] ADD CONSTRAINT [FK_plFldStudy_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[plFldStudy] ADD CONSTRAINT [FK_plFldStudy_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
