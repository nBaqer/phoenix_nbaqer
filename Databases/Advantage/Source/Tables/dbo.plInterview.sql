CREATE TABLE [dbo].[plInterview]
(
[InterviewId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_plInterview_InterviewId] DEFAULT (newid()),
[InterviewDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[InterViewCode] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[plInterview] ADD CONSTRAINT [PK_plInterview_InterviewId] PRIMARY KEY CLUSTERED  ([InterviewId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[plInterview] ADD CONSTRAINT [FK_plInterview_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[plInterview] ADD CONSTRAINT [FK_plInterview_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
