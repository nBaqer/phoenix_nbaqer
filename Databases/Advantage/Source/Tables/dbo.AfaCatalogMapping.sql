CREATE TABLE [dbo].[AfaCatalogMapping]
(
[Id] [uniqueidentifier] NOT NULL,
[AfaCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TableId] [int] NOT NULL,
[ModDate] [datetime] NOT NULL,
[Username] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AfaCatalogMapping] ADD CONSTRAINT [PK_AfaCatalogMapping_1] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AfaCatalogMapping] ADD CONSTRAINT [FK_AfaCatalogMapping_syTables_TableId_TblId] FOREIGN KEY ([TableId]) REFERENCES [dbo].[syTables] ([TblId])
GO
