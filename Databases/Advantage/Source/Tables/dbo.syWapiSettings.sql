CREATE TABLE [dbo].[syWapiSettings]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[CodeOperation] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExternalUrl] [varchar] (2048) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IdExtCompany] [int] NOT NULL,
[IdExtOperation] [int] NOT NULL,
[IdAllowedService] [int] NOT NULL,
[IdSecondAllowedService] [int] NULL,
[FirstAllowedServiceQueryString] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SecondAllowedServiceQueryString] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ConsumerKey] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrivateKey] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OperationSecondTimeInterval] [int] NOT NULL CONSTRAINT [DF_syWapiSettings_OperationSecondTimeInterval] DEFAULT ((86400)),
[PollSecondForOnDemandOperation] [int] NOT NULL CONSTRAINT [DF_syWapiSettings_PollSecondForOnDemandOperation] DEFAULT ((60)),
[FlagOnDemandOperation] [bit] NOT NULL CONSTRAINT [DF_syWapiSettings_FlagOnDemandOperation] DEFAULT ((0)),
[FlagRefreshConfiguration] [bit] NOT NULL CONSTRAINT [DF_syWapiSettings_FlagRefreshConfiguration] DEFAULT ((0)),
[IsActive] [bit] NOT NULL CONSTRAINT [DF_syWapiSettings_IsActive] DEFAULT ((1)),
[DateMod] [datetime2] NOT NULL CONSTRAINT [DF_syWapiSettings_DateMod] DEFAULT (getdate()),
[DateLastExecution] [datetime2] NOT NULL CONSTRAINT [DF_syWapiSettings_DateLastExecution] DEFAULT (getdate()),
[UserMod] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_syWapiSettings_UserMod] DEFAULT ('SUPPORT'),
[UserName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syWapiSettings] ADD CONSTRAINT [PK_syWapiSettings_id] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_syWapiSettings_CodeOperation] ON [dbo].[syWapiSettings] ([CodeOperation]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syWapiSettings] ADD CONSTRAINT [FK_syWapiSettings_syWapiAllowedServices_IdAllowedService_Id] FOREIGN KEY ([IdAllowedService]) REFERENCES [dbo].[syWapiAllowedServices] ([Id])
GO
ALTER TABLE [dbo].[syWapiSettings] ADD CONSTRAINT [FK_syWapiSettings_syWapiAllowedServices_IdSecondAllowedService_Id] FOREIGN KEY ([IdSecondAllowedService]) REFERENCES [dbo].[syWapiAllowedServices] ([Id])
GO
ALTER TABLE [dbo].[syWapiSettings] ADD CONSTRAINT [FK_syWapiSettings_syWapiExternalCompanies_IdExtCompany_Id] FOREIGN KEY ([IdExtCompany]) REFERENCES [dbo].[syWapiExternalCompanies] ([Id])
GO
ALTER TABLE [dbo].[syWapiSettings] ADD CONSTRAINT [FK_syWapiSettings_syWapiExternalOperationMode_IdExtOperation_Id] FOREIGN KEY ([IdExtOperation]) REFERENCES [dbo].[syWapiExternalOperationMode] ([Id])
GO
