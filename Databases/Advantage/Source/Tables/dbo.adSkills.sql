CREATE TABLE [dbo].[adSkills]
(
[SkillId] [int] NOT NULL IDENTITY(1, 1),
[LeadId] [uniqueidentifier] NOT NULL,
[LevelId] [int] NOT NULL,
[SkillGrpId] [uniqueidentifier] NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Comment] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_adSkills_Comment] DEFAULT (''),
[ModDate] [datetime] NOT NULL CONSTRAINT [DF_adSkills_ModDate] DEFAULT (getdate()),
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adSkills] ADD CONSTRAINT [PK_adSkills_SkillId] PRIMARY KEY CLUSTERED  ([SkillId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adSkills] ADD CONSTRAINT [FK_adSkills_adLeads_LeadId_LeadId] FOREIGN KEY ([LeadId]) REFERENCES [dbo].[adLeads] ([LeadId])
GO
ALTER TABLE [dbo].[adSkills] ADD CONSTRAINT [FK_adSkills_adLevel_LevelId_LevelId] FOREIGN KEY ([LevelId]) REFERENCES [dbo].[adLevel] ([LevelId])
GO
ALTER TABLE [dbo].[adSkills] ADD CONSTRAINT [FK_adSkills_plSkillGroups_SkillGrpId_SkillGrpId] FOREIGN KEY ([SkillGrpId]) REFERENCES [dbo].[plSkillGroups] ([SkillGrpId])
GO
