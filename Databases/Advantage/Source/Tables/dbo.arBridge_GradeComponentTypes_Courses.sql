CREATE TABLE [dbo].[arBridge_GradeComponentTypes_Courses]
(
[GrdComponentTypeId_ReqId] [uniqueidentifier] NULL,
[GrdComponentTypeId] [uniqueidentifier] NULL,
[ReqId] [uniqueidentifier] NULL,
[BridgeGradeComponentTypesCoursesId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_arBridge_GradeComponentTypes_Courses_BridgeGradeComponentTypesCoursesId] DEFAULT (newsequentialid())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arBridge_GradeComponentTypes_Courses] ADD CONSTRAINT [PK_arBridge_GradeComponentTypes_Courses_BridgeGradeComponentTypesCoursesId] PRIMARY KEY CLUSTERED  ([BridgeGradeComponentTypesCoursesId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arBridge_GradeComponentTypes_Courses] ADD CONSTRAINT [FK_arBridge_GradeComponentTypes_Courses_arGrdComponentTypes_GrdComponentTypeId_GrdComponentTypeId] FOREIGN KEY ([GrdComponentTypeId]) REFERENCES [dbo].[arGrdComponentTypes] ([GrdComponentTypeId]) ON DELETE CASCADE
GO
