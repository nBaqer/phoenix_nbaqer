CREATE TABLE [dbo].[adLeadOtherContactsEmail]
(
[OtherContactsEmailId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_adLeadOtherContactsEmail_OtherContactsEmailId] DEFAULT (newid()),
[OtherContactId] [uniqueidentifier] NOT NULL,
[LeadId] [uniqueidentifier] NOT NULL,
[EMail] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EMailTypeId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL,
[StatusId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
--========================================================================================== 
-- TRIGGER adLeadOtherContactsEmail_Audit_Delete 
-- DELETE  add Audit History when delete records in adLeadOtherContactsEmail
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadOtherContactsEmail_Audit_Delete] ON [dbo].[adLeadOtherContactsEmail]
    FOR DELETE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Deleted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Deleted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Deleted
                        );
 
        IF @EventRows > 0
            BEGIN
                EXEC fmAuditHistAdd @AuditHistId,'adLeadOtherContactsEmail','D',@EventRows,@EventDate,@UserName;
                -- OtherContactId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsEmailId
                               ,'OtherContactId'
                               ,CONVERT(VARCHAR(MAX),Old.OtherContactId)
                        FROM    Deleted AS Old;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsEmailId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),Old.LeadId)
                        FROM    Deleted AS Old;
                -- EMail 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsEmailId
                               ,'EMail'
                               ,CONVERT(VARCHAR(MAX),Old.EMail)
                        FROM    Deleted AS Old;
                -- EMailTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsEmailId
                               ,'EMailTypeId'
                               ,CONVERT(VARCHAR(MAX),Old.EMailTypeId)
                        FROM    Deleted AS Old;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsEmailId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),Old.ModUser)
                        FROM    Deleted AS Old;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsEmailId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                        FROM    Deleted AS Old;
                -- StatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactsEmailId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(MAX),Old.StatusId)
                        FROM    Deleted AS Old;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadOtherContactsEmail_Audit_Delete 
--========================================================================================== 
 

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
--========================================================================================== 
-- TRIGGER adLeadOtherContactsEmail_Audit_Insert 
-- INSERT  add Audit History when insert records in adLeadOtherContactsEmail
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadOtherContactsEmail_Audit_Insert] ON [dbo].[adLeadOtherContactsEmail]
    FOR INSERT
AS
    BEGIN 
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadOtherContactsEmail','I',@EventRows,@EventDate,@UserName;
                -- OtherContactId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsEmailId
                               ,'OtherContactId'
                               ,CONVERT(VARCHAR(MAX),New.OtherContactId)
                        FROM    Inserted AS New;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsEmailId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),New.LeadId)
                        FROM    Inserted AS New;
                -- EMail 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsEmailId
                               ,'EMail'
                               ,CONVERT(VARCHAR(MAX),New.EMail)
                        FROM    Inserted AS New;
                -- EMailTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsEmailId
                               ,'EMailTypeId'
                               ,CONVERT(VARCHAR(MAX),New.EMailTypeId)
                        FROM    Inserted AS New;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsEmailId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),New.ModUser)
                        FROM    Inserted AS New;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsEmailId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                        FROM    Inserted AS New;
                -- StatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactsEmailId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(MAX),New.StatusId)
                        FROM    Inserted AS New;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadOtherContactsEmail_Audit_Insert 
--========================================================================================== 
 

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--========================================================================================== 
-- TRIGGER adLeadOtherContactsEmail_Audit_Update 
-- UPDATE  add Audit History when update fields in adLeadOtherContactsEmail
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadOtherContactsEmail_Audit_Update] ON [dbo].[adLeadOtherContactsEmail]
    FOR UPDATE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadOtherContactsEmail','U',@EventRows,@EventDate,@UserName;
                -- OtherContactId 
                IF UPDATE(OtherContactId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsEmailId
                                       ,'OtherContactId'
                                       ,CONVERT(VARCHAR(MAX),Old.OtherContactId)
                                       ,CONVERT(VARCHAR(MAX),New.OtherContactId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsEmailId = New.OtherContactsEmailId
                                WHERE   Old.OtherContactId <> New.OtherContactId
                                        OR (
                                             Old.OtherContactId IS NULL
                                             AND New.OtherContactId IS NOT NULL
                                           )
                                        OR (
                                             New.OtherContactId IS NULL
                                             AND Old.OtherContactId IS NOT NULL
                                           );
                    END;
                -- LeadId 
                IF UPDATE(LeadId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsEmailId
                                       ,'LeadId'
                                       ,CONVERT(VARCHAR(MAX),Old.LeadId)
                                       ,CONVERT(VARCHAR(MAX),New.LeadId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsEmailId = New.OtherContactsEmailId
                                WHERE   Old.LeadId <> New.LeadId
                                        OR (
                                             Old.LeadId IS NULL
                                             AND New.LeadId IS NOT NULL
                                           )
                                        OR (
                                             New.LeadId IS NULL
                                             AND Old.LeadId IS NOT NULL
                                           );
                    END;
                -- EMail 
                IF UPDATE(EMail)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsEmailId
                                       ,'EMail'
                                       ,CONVERT(VARCHAR(MAX),Old.EMail)
                                       ,CONVERT(VARCHAR(MAX),New.EMail)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsEmailId = New.OtherContactsEmailId
                                WHERE   Old.EMail <> New.EMail
                                        OR (
                                             Old.EMail IS NULL
                                             AND New.EMail IS NOT NULL
                                           )
                                        OR (
                                             New.EMail IS NULL
                                             AND Old.EMail IS NOT NULL
                                           );
                    END;
                -- EMailTypeId 
                IF UPDATE(EMailTypeId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsEmailId
                                       ,'EMailTypeId'
                                       ,CONVERT(VARCHAR(MAX),Old.EMailTypeId)
                                       ,CONVERT(VARCHAR(MAX),New.EMailTypeId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsEmailId = New.OtherContactsEmailId
                                WHERE   Old.EMailTypeId <> New.EMailTypeId
                                        OR (
                                             Old.EMailTypeId IS NULL
                                             AND New.EMailTypeId IS NOT NULL
                                           )
                                        OR (
                                             New.EMailTypeId IS NULL
                                             AND Old.EMailTypeId IS NOT NULL
                                           );
                    END;
                -- ModUser 
                IF UPDATE(ModUser)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsEmailId
                                       ,'ModUser'
                                       ,CONVERT(VARCHAR(MAX),Old.ModUser)
                                       ,CONVERT(VARCHAR(MAX),New.ModUser)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsEmailId = New.OtherContactsEmailId
                                WHERE   Old.ModUser <> New.ModUser
                                        OR (
                                             Old.ModUser IS NULL
                                             AND New.ModUser IS NOT NULL
                                           )
                                        OR (
                                             New.ModUser IS NULL
                                             AND Old.ModUser IS NOT NULL
                                           );
                    END;
                -- ModDate 
                IF UPDATE(ModDate)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsEmailId
                                       ,'ModDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsEmailId = New.OtherContactsEmailId
                                WHERE   Old.ModDate <> New.ModDate
                                        OR (
                                             Old.ModDate IS NULL
                                             AND New.ModDate IS NOT NULL
                                           )
                                        OR (
                                             New.ModDate IS NULL
                                             AND Old.ModDate IS NOT NULL
                                           );
                    END;
                -- StatusId 
                IF UPDATE(StatusId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactsEmailId
                                       ,'StatusId'
                                       ,CONVERT(VARCHAR(MAX),Old.StatusId)
                                       ,CONVERT(VARCHAR(MAX),New.StatusId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactsEmailId = New.OtherContactsEmailId
                                WHERE   Old.StatusId <> New.StatusId
                                        OR (
                                             Old.StatusId IS NULL
                                             AND New.StatusId IS NOT NULL
                                           )
                                        OR (
                                             New.StatusId IS NULL
                                             AND Old.StatusId IS NOT NULL
                                           );
                    END;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadOtherContactsEmail_Audit_Update 
--========================================================================================== 
 

GO
ALTER TABLE [dbo].[adLeadOtherContactsEmail] ADD CONSTRAINT [PK_adLeadOtherContactsEmail_OtherContactsEmailId] PRIMARY KEY CLUSTERED  ([OtherContactsEmailId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adLeadOtherContactsEmail] ADD CONSTRAINT [FK_adLeadOtherContactsEmail_adLeadOtherContacts_OtherContactId_OtherContactId] FOREIGN KEY ([OtherContactId]) REFERENCES [dbo].[adLeadOtherContacts] ([OtherContactId])
GO
ALTER TABLE [dbo].[adLeadOtherContactsEmail] ADD CONSTRAINT [FK_adLeadOtherContactsEmail_adLeads_LeadId_LeadId] FOREIGN KEY ([LeadId]) REFERENCES [dbo].[adLeads] ([LeadId])
GO
ALTER TABLE [dbo].[adLeadOtherContactsEmail] ADD CONSTRAINT [FK_adLeadOtherContactsEmail_syEmailType_EmailTypeId_EmailTypeId] FOREIGN KEY ([EMailTypeId]) REFERENCES [dbo].[syEmailType] ([EMailTypeId])
GO
ALTER TABLE [dbo].[adLeadOtherContactsEmail] ADD CONSTRAINT [FK_adLeadOtherContactsEmail_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
