CREATE TABLE [dbo].[arFERPACategory]
(
[FERPACategoryId] [uniqueidentifier] NOT NULL,
[FERPACategoryCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[FERPACategoryDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arFERPACategory] ADD CONSTRAINT [PK_arFERPACategory_FERPACategoryId] PRIMARY KEY CLUSTERED  ([FERPACategoryId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arFERPACategory] ADD CONSTRAINT [FK_arFERPACategory_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[arFERPACategory] ADD CONSTRAINT [FK_arFERPACategory_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
