CREATE TABLE [dbo].[syUsersRolesCampGrps]
(
[UserRoleCampGrpId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syUsersRolesCampGrps_UserRoleCampGrpId] DEFAULT (newid()),
[UserId] [uniqueidentifier] NOT NULL,
[RoleId] [uniqueidentifier] NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[syUsersRolesCampGrps_Audit_Delete] ON [dbo].[syUsersRolesCampGrps]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syUsersRolesCampGrps','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.UserRoleCampGrpId
                               ,'UserId'
                               ,CONVERT(VARCHAR(8000),Old.UserId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.UserRoleCampGrpId
                               ,'RoleId'
                               ,CONVERT(VARCHAR(8000),Old.RoleId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.UserRoleCampGrpId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
            END; 
        END;



    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[syUsersRolesCampGrps_Audit_Insert] ON [dbo].[syUsersRolesCampGrps]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syUsersRolesCampGrps','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.UserRoleCampGrpId
                               ,'UserId'
                               ,CONVERT(VARCHAR(8000),New.UserId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.UserRoleCampGrpId
                               ,'RoleId'
                               ,CONVERT(VARCHAR(8000),New.RoleId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.UserRoleCampGrpId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New; 
            END; 
        END;



    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[syUsersRolesCampGrps_Audit_Update] ON [dbo].[syUsersRolesCampGrps]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syUsersRolesCampGrps','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(UserId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.UserRoleCampGrpId
                                   ,'UserId'
                                   ,CONVERT(VARCHAR(8000),Old.UserId,121)
                                   ,CONVERT(VARCHAR(8000),New.UserId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.UserRoleCampGrpId = New.UserRoleCampGrpId
                            WHERE   Old.UserId <> New.UserId
                                    OR (
                                         Old.UserId IS NULL
                                         AND New.UserId IS NOT NULL
                                       )
                                    OR (
                                         New.UserId IS NULL
                                         AND Old.UserId IS NOT NULL
                                       ); 
                IF UPDATE(RoleId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.UserRoleCampGrpId
                                   ,'RoleId'
                                   ,CONVERT(VARCHAR(8000),Old.RoleId,121)
                                   ,CONVERT(VARCHAR(8000),New.RoleId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.UserRoleCampGrpId = New.UserRoleCampGrpId
                            WHERE   Old.RoleId <> New.RoleId
                                    OR (
                                         Old.RoleId IS NULL
                                         AND New.RoleId IS NOT NULL
                                       )
                                    OR (
                                         New.RoleId IS NULL
                                         AND Old.RoleId IS NOT NULL
                                       ); 
                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.UserRoleCampGrpId
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.UserRoleCampGrpId = New.UserRoleCampGrpId
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 
            END; 
        END;



    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[syUsersRolesCampGrps] ADD CONSTRAINT [PK_syUsersRolesCampGrps_UserRoleCampGrpId] PRIMARY KEY CLUSTERED  ([UserRoleCampGrpId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syUsersRolesCampGrps] ADD CONSTRAINT [FK_syUsersRolesCampGrps_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[syUsersRolesCampGrps] ADD CONSTRAINT [FK_syUsersRolesCampGrps_syRoles_RoleId_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[syRoles] ([RoleId])
GO
ALTER TABLE [dbo].[syUsersRolesCampGrps] ADD CONSTRAINT [FK_syUsersRolesCampGrps_syUsers_UserId_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[syUsers] ([UserId])
GO
