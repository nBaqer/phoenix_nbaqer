CREATE TABLE [dbo].[AllNotes]
(
[NotesId] [int] NOT NULL IDENTITY(1, 1),
[ModuleCode] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[NoteType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_AllNotes_NoteType] DEFAULT (''),
[UserId] [uniqueidentifier] NOT NULL,
[NoteText] [varchar] (2000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL CONSTRAINT [DF_AllNotes_ModDate] DEFAULT (getdate()),
[PageFieldId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AllNotes] ADD CONSTRAINT [PK_AllNotes_NotesId] PRIMARY KEY CLUSTERED  ([NotesId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AllNotes] ADD CONSTRAINT [FK_AllNotes_syModules_ModuleCode_ModuleID] FOREIGN KEY ([ModuleCode]) REFERENCES [dbo].[syModules] ([ModuleCode])
GO
ALTER TABLE [dbo].[AllNotes] ADD CONSTRAINT [FK_AllNotes_syNotesPageFields_PageFieldId_PageFieldId] FOREIGN KEY ([PageFieldId]) REFERENCES [dbo].[syNotesPageFields] ([PageFieldId])
GO
