CREATE TABLE [dbo].[adSourceAdvertisement]
(
[SourceAdvId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_adSourceAdvertisement_SourceAdvId] DEFAULT (newid()),
[SourceAdvDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[startdate] [datetime] NOT NULL,
[enddate] [datetime] NOT NULL,
[cost] [decimal] (9, 2) NOT NULL,
[SourceTypeId] [uniqueidentifier] NOT NULL,
[AdvIntervalId] [uniqueidentifier] NOT NULL,
[SourceAdvCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CampGrpId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[adSourceAdvertisement_Audit_Delete] ON [dbo].[adSourceAdvertisement]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adSourceAdvertisement','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SourceAdvId
                               ,'sourceadvdescrip'
                               ,CONVERT(VARCHAR(8000),Old.SourceAdvDescrip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SourceAdvId
                               ,'StartDate'
                               ,CONVERT(VARCHAR(8000),Old.startdate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SourceAdvId
                               ,'EndDate'
                               ,CONVERT(VARCHAR(8000),Old.enddate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SourceAdvId
                               ,'AdvIntervalId'
                               ,CONVERT(VARCHAR(8000),Old.AdvIntervalId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SourceAdvId
                               ,'SourceAdvCode'
                               ,CONVERT(VARCHAR(8000),Old.SourceAdvCode,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SourceAdvId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SourceAdvId
                               ,'cost'
                               ,CONVERT(VARCHAR(8000),Old.cost,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SourceAdvId
                               ,'SourceTypeID'
                               ,CONVERT(VARCHAR(8000),Old.SourceTypeId,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[adSourceAdvertisement_Audit_Insert] ON [dbo].[adSourceAdvertisement]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adSourceAdvertisement','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SourceAdvId
                               ,'sourceadvdescrip'
                               ,CONVERT(VARCHAR(8000),New.SourceAdvDescrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SourceAdvId
                               ,'StartDate'
                               ,CONVERT(VARCHAR(8000),New.startdate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SourceAdvId
                               ,'EndDate'
                               ,CONVERT(VARCHAR(8000),New.enddate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SourceAdvId
                               ,'AdvIntervalId'
                               ,CONVERT(VARCHAR(8000),New.AdvIntervalId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SourceAdvId
                               ,'SourceAdvCode'
                               ,CONVERT(VARCHAR(8000),New.SourceAdvCode,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SourceAdvId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SourceAdvId
                               ,'cost'
                               ,CONVERT(VARCHAR(8000),New.cost,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SourceAdvId
                               ,'SourceTypeID'
                               ,CONVERT(VARCHAR(8000),New.SourceTypeId,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO
GO

GO

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[adSourceAdvertisement_Audit_Update] ON [dbo].[adSourceAdvertisement]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adSourceAdvertisement','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(SourceAdvDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SourceAdvId
                                   ,'sourceadvdescrip'
                                   ,CONVERT(VARCHAR(8000),Old.SourceAdvDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.SourceAdvDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SourceAdvId = New.SourceAdvId
                            WHERE   Old.SourceAdvDescrip <> New.SourceAdvDescrip
                                    OR (
                                         Old.SourceAdvDescrip IS NULL
                                         AND New.SourceAdvDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.SourceAdvDescrip IS NULL
                                         AND Old.SourceAdvDescrip IS NOT NULL
                                       ); 
                IF UPDATE(StartDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SourceAdvId
                                   ,'StartDate'
                                   ,CONVERT(VARCHAR(8000),Old.startdate,121)
                                   ,CONVERT(VARCHAR(8000),New.startdate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SourceAdvId = New.SourceAdvId
                            WHERE   Old.startdate <> New.startdate
                                    OR (
                                         Old.startdate IS NULL
                                         AND New.startdate IS NOT NULL
                                       )
                                    OR (
                                         New.startdate IS NULL
                                         AND Old.startdate IS NOT NULL
                                       ); 
                IF UPDATE(EndDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SourceAdvId
                                   ,'EndDate'
                                   ,CONVERT(VARCHAR(8000),Old.enddate,121)
                                   ,CONVERT(VARCHAR(8000),New.enddate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SourceAdvId = New.SourceAdvId
                            WHERE   Old.enddate <> New.enddate
                                    OR (
                                         Old.enddate IS NULL
                                         AND New.enddate IS NOT NULL
                                       )
                                    OR (
                                         New.enddate IS NULL
                                         AND Old.enddate IS NOT NULL
                                       ); 
                IF UPDATE(AdvIntervalId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SourceAdvId
                                   ,'AdvIntervalId'
                                   ,CONVERT(VARCHAR(8000),Old.AdvIntervalId,121)
                                   ,CONVERT(VARCHAR(8000),New.AdvIntervalId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SourceAdvId = New.SourceAdvId
                            WHERE   Old.AdvIntervalId <> New.AdvIntervalId
                                    OR (
                                         Old.AdvIntervalId IS NULL
                                         AND New.AdvIntervalId IS NOT NULL
                                       )
                                    OR (
                                         New.AdvIntervalId IS NULL
                                         AND Old.AdvIntervalId IS NOT NULL
                                       ); 
                IF UPDATE(SourceAdvCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SourceAdvId
                                   ,'SourceAdvCode'
                                   ,CONVERT(VARCHAR(8000),Old.SourceAdvCode,121)
                                   ,CONVERT(VARCHAR(8000),New.SourceAdvCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SourceAdvId = New.SourceAdvId
                            WHERE   Old.SourceAdvCode <> New.SourceAdvCode
                                    OR (
                                         Old.SourceAdvCode IS NULL
                                         AND New.SourceAdvCode IS NOT NULL
                                       )
                                    OR (
                                         New.SourceAdvCode IS NULL
                                         AND Old.SourceAdvCode IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SourceAdvId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SourceAdvId = New.SourceAdvId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(Cost)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SourceAdvId
                                   ,'cost'
                                   ,CONVERT(VARCHAR(8000),Old.cost,121)
                                   ,CONVERT(VARCHAR(8000),New.cost,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SourceAdvId = New.SourceAdvId
                            WHERE   Old.cost <> New.cost
                                    OR (
                                         Old.cost IS NULL
                                         AND New.cost IS NOT NULL
                                       )
                                    OR (
                                         New.cost IS NULL
                                         AND Old.cost IS NOT NULL
                                       ); 
                IF UPDATE(SourceTypeId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SourceAdvId
                                   ,'SourceTypeID'
                                   ,CONVERT(VARCHAR(8000),Old.SourceTypeId,121)
                                   ,CONVERT(VARCHAR(8000),New.SourceTypeId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SourceAdvId = New.SourceAdvId
                            WHERE   Old.SourceTypeId <> New.SourceTypeId
                                    OR (
                                         Old.SourceTypeId IS NULL
                                         AND New.SourceTypeId IS NOT NULL
                                       )
                                    OR (
                                         New.SourceTypeId IS NULL
                                         AND Old.SourceTypeId IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[adSourceAdvertisement] ADD CONSTRAINT [PK_adSourceAdvertisement_SourceAdvId] PRIMARY KEY CLUSTERED  ([SourceAdvId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adSourceAdvertisement] ADD CONSTRAINT [FK_adSourceAdvertisement_adAdvInterval_AdvIntervalId_AdvIntervalId] FOREIGN KEY ([AdvIntervalId]) REFERENCES [dbo].[adAdvInterval] ([AdvIntervalId])
GO
ALTER TABLE [dbo].[adSourceAdvertisement] ADD CONSTRAINT [FK_adSourceAdvertisement_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[adSourceAdvertisement] ADD CONSTRAINT [FK_adSourceAdvertisement_adSourceType_SourceTypeId_SourceTypeId] FOREIGN KEY ([SourceTypeId]) REFERENCES [dbo].[adSourceType] ([SourceTypeId])
GO
ALTER TABLE [dbo].[adSourceAdvertisement] ADD CONSTRAINT [FK_adSourceAdvertisement_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
