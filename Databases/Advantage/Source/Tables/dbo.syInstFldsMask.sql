CREATE TABLE [dbo].[syInstFldsMask]
(
[FldId] [int] NOT NULL,
[Mask] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syInstFldsMask] ADD CONSTRAINT [PK_syInstFldsMask_FldId] PRIMARY KEY CLUSTERED  ([FldId]) ON [PRIMARY]
GO
GRANT REFERENCES ON  [dbo].[syInstFldsMask] TO [AdvantageRole]
GRANT SELECT ON  [dbo].[syInstFldsMask] TO [AdvantageRole]
GRANT INSERT ON  [dbo].[syInstFldsMask] TO [AdvantageRole]
GRANT DELETE ON  [dbo].[syInstFldsMask] TO [AdvantageRole]
GRANT UPDATE ON  [dbo].[syInstFldsMask] TO [AdvantageRole]
GO
