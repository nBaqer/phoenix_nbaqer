CREATE TABLE [dbo].[adImportLeadsAdvFldMappings]
(
[AdvFieldMapping] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_adImportLeadsAdvFldMappings_AdvFieldMapping] DEFAULT (newid()),
[MappingId] [uniqueidentifier] NOT NULL,
[FieldNumber] [smallint] NOT NULL,
[ILFieldId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adImportLeadsAdvFldMappings] ADD CONSTRAINT [PK_adImportLeadsAdvFldMappings_AdvFieldMapping] PRIMARY KEY CLUSTERED  ([AdvFieldMapping]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adImportLeadsAdvFldMappings] ADD CONSTRAINT [FK_adImportLeadsAdvFldMappings_adImportLeadsFields_ILFieldId_ILFieldId] FOREIGN KEY ([ILFieldId]) REFERENCES [dbo].[adImportLeadsFields] ([ILFieldId])
GO
ALTER TABLE [dbo].[adImportLeadsAdvFldMappings] ADD CONSTRAINT [FK_adImportLeadsAdvFldMappings_adImportLeadsMappings_MappingId_MappingId] FOREIGN KEY ([MappingId]) REFERENCES [dbo].[adImportLeadsMappings] ([MappingId]) ON DELETE CASCADE
GO
