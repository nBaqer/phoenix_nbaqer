CREATE TABLE [dbo].[arProgSchedules]
(
[ScheduleId] [uniqueidentifier] NOT NULL,
[Code] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrgVerId] [uniqueidentifier] NULL,
[Active] [bit] NULL,
[UseFlexTime] [bit] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [smalldatetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arProgSchedules] ADD CONSTRAINT [PK_arProgSchedules_ScheduleId] PRIMARY KEY CLUSTERED  ([ScheduleId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_arProgSchedules_PrgVerId] ON [dbo].[arProgSchedules] ([PrgVerId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arProgSchedules] ADD CONSTRAINT [FK_arProgSchedules_arPrgVersions_PrgVerId_PrgVerId] FOREIGN KEY ([PrgVerId]) REFERENCES [dbo].[arPrgVersions] ([PrgVerId])
GO
