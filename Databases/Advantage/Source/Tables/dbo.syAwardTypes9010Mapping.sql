CREATE TABLE [dbo].[syAwardTypes9010Mapping]
(
[AwardTypes9010MappingId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_syAwardTypes9010Mapping_AwardTypes9010MappingId] DEFAULT (newsequentialid()),
[FundSourceId] [uniqueidentifier] NULL,
[CampusId] [uniqueidentifier] NOT NULL,
[AwardType9010Id] [uniqueidentifier] NOT NULL,
[ModDate] [datetime] NOT NULL,
[ModUser] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NULL,
[TransCodeId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syAwardTypes9010Mapping] ADD CONSTRAINT [PK_syAwardTypes9010Mapping_AwardTypes9010MappingId] PRIMARY KEY CLUSTERED  ([AwardTypes9010MappingId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syAwardTypes9010Mapping] ADD CONSTRAINT [FK_syAwardTypes9010Mapping_saFundSources_FundSourceId_FundSourceId] FOREIGN KEY ([FundSourceId]) REFERENCES [dbo].[saFundSources] ([FundSourceId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[syAwardTypes9010Mapping] ADD CONSTRAINT [FK_syAwardTypes9010Mapping_saTransCodes_TransCodeId_TransCodeId] FOREIGN KEY ([TransCodeId]) REFERENCES [dbo].[saTransCodes] ([TransCodeId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[syAwardTypes9010Mapping] ADD CONSTRAINT [FK_syAwardTypes9010Mapping_syCampuses_CampusId_CampusId] FOREIGN KEY ([CampusId]) REFERENCES [dbo].[syCampuses] ([CampusId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[syAwardTypes9010Mapping] ADD CONSTRAINT [FK_syAwardTypes9010Mapping_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
