CREATE TABLE [dbo].[msgEntitiesFieldGroups]
(
[EntityFieldGroupId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_msgEntitiesFieldGroups_EntityFieldGroupId] DEFAULT (newid()),
[EntityId] [smallint] NULL,
[FieldGroupName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[ModUser] [uniqueidentifier] NULL,
[Active] [tinyint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[msgEntitiesFieldGroups] ADD CONSTRAINT [PK_msgEntitiesFieldGroups_EntityFieldGroupId] PRIMARY KEY CLUSTERED  ([EntityFieldGroupId]) ON [PRIMARY]
GO
