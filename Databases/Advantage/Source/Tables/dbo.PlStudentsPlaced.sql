CREATE TABLE [dbo].[PlStudentsPlaced]
(
[PlacementId] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[Supervisor] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[JobStatusId] [uniqueidentifier] NULL,
[SSN] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StartDate] [datetime] NULL,
[Salary] [decimal] (18, 2) NULL,
[TerminationReason] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TerminationDate] [datetime] NULL,
[Notes] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WorkDaysId] [uniqueidentifier] NULL,
[BenefitsId] [uniqueidentifier] NULL,
[JobDescrip] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PlacementRep] [uniqueidentifier] NULL,
[ScheduleId] [uniqueidentifier] NULL,
[Reason] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SalaryTypeId] [uniqueidentifier] NULL,
[PlacedDate] [datetime] NULL,
[InterviewId] [uniqueidentifier] NULL,
[FldStudyId] [uniqueidentifier] NULL,
[StuEnrollId] [uniqueidentifier] NULL,
[Fee] [uniqueidentifier] NULL,
[HowPlacedId] [uniqueidentifier] NULL,
[GraduatedDate] [datetime] NULL,
[EmployerJobId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[PlStudentsPlaced_Audit_Delete] ON [dbo].[PlStudentsPlaced]
    FOR DELETE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'PlStudentsPlaced','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PlacementId
                               ,'Supervisor'
                               ,CONVERT(VARCHAR(8000),Old.Supervisor,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PlacementId
                               ,'JobStatusId'
                               ,CONVERT(VARCHAR(8000),Old.JobStatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PlacementId
                               ,'SSN'
                               ,CONVERT(VARCHAR(8000),Old.SSN,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PlacementId
                               ,'StartDate'
                               ,CONVERT(VARCHAR(8000),Old.StartDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PlacementId
                               ,'Salary'
                               ,CONVERT(VARCHAR(8000),Old.Salary,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PlacementId
                               ,'TerminationReason'
                               ,CONVERT(VARCHAR(8000),Old.TerminationReason,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PlacementId
                               ,'TerminationDate'
                               ,CONVERT(VARCHAR(8000),Old.TerminationDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PlacementId
                               ,'Notes'
                               ,CONVERT(VARCHAR(8000),Old.Notes,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PlacementId
                               ,'WorkDaysId'
                               ,CONVERT(VARCHAR(8000),Old.WorkDaysId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PlacementId
                               ,'BenefitsId'
                               ,CONVERT(VARCHAR(8000),Old.BenefitsId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PlacementId
                               ,'JobDescrip'
                               ,CONVERT(VARCHAR(8000),Old.JobDescrip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PlacementId
                               ,'ScheduleId'
                               ,CONVERT(VARCHAR(8000),Old.ScheduleId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PlacementId
                               ,'PlacementRep'
                               ,CONVERT(VARCHAR(8000),Old.PlacementRep,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PlacementId
                               ,'Reason'
                               ,CONVERT(VARCHAR(8000),Old.Reason,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PlacementId
                               ,'SalaryTypeId'
                               ,CONVERT(VARCHAR(8000),Old.SalaryTypeId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PlacementId
                               ,'PlacedDate'
                               ,CONVERT(VARCHAR(8000),Old.PlacedDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PlacementId
                               ,'InterviewId'
                               ,CONVERT(VARCHAR(8000),Old.InterviewId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PlacementId
                               ,'FldStudyId'
                               ,CONVERT(VARCHAR(8000),Old.FldStudyId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PlacementId
                               ,'StuEnrollId'
                               ,CONVERT(VARCHAR(8000),Old.StuEnrollId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PlacementId
                               ,'Fee'
                               ,CONVERT(VARCHAR(8000),Old.Fee,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PlacementId
                               ,'HowPlacedId'
                               ,CONVERT(VARCHAR(8000),Old.HowPlacedId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PlacementId
                               ,'GraduatedDate'
                               ,CONVERT(VARCHAR(8000),Old.GraduatedDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PlacementId
                               ,'EmployerJobId'
                               ,CONVERT(VARCHAR(8000),Old.EmployerJobId,121)
                        FROM    Deleted Old; 
            END; 
        END;



    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[PlStudentsPlaced_Audit_Insert] ON [dbo].[PlStudentsPlaced]
    FOR INSERT
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'PlStudentsPlaced','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PlacementId
                               ,'Supervisor'
                               ,CONVERT(VARCHAR(8000),New.Supervisor,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PlacementId
                               ,'JobStatusId'
                               ,CONVERT(VARCHAR(8000),New.JobStatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PlacementId
                               ,'SSN'
                               ,CONVERT(VARCHAR(8000),New.SSN,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PlacementId
                               ,'StartDate'
                               ,CONVERT(VARCHAR(8000),New.StartDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PlacementId
                               ,'Salary'
                               ,CONVERT(VARCHAR(8000),New.Salary,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PlacementId
                               ,'TerminationReason'
                               ,CONVERT(VARCHAR(8000),New.TerminationReason,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PlacementId
                               ,'TerminationDate'
                               ,CONVERT(VARCHAR(8000),New.TerminationDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PlacementId
                               ,'Notes'
                               ,CONVERT(VARCHAR(8000),New.Notes,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PlacementId
                               ,'WorkDaysId'
                               ,CONVERT(VARCHAR(8000),New.WorkDaysId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PlacementId
                               ,'BenefitsId'
                               ,CONVERT(VARCHAR(8000),New.BenefitsId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PlacementId
                               ,'JobDescrip'
                               ,CONVERT(VARCHAR(8000),New.JobDescrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PlacementId
                               ,'ScheduleId'
                               ,CONVERT(VARCHAR(8000),New.ScheduleId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PlacementId
                               ,'PlacementRep'
                               ,CONVERT(VARCHAR(8000),New.PlacementRep,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PlacementId
                               ,'Reason'
                               ,CONVERT(VARCHAR(8000),New.Reason,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PlacementId
                               ,'SalaryTypeId'
                               ,CONVERT(VARCHAR(8000),New.SalaryTypeId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PlacementId
                               ,'PlacedDate'
                               ,CONVERT(VARCHAR(8000),New.PlacedDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PlacementId
                               ,'InterviewId'
                               ,CONVERT(VARCHAR(8000),New.InterviewId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PlacementId
                               ,'FldStudyId'
                               ,CONVERT(VARCHAR(8000),New.FldStudyId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PlacementId
                               ,'StuEnrollId'
                               ,CONVERT(VARCHAR(8000),New.StuEnrollId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PlacementId
                               ,'Fee'
                               ,CONVERT(VARCHAR(8000),New.Fee,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PlacementId
                               ,'HowPlacedId'
                               ,CONVERT(VARCHAR(8000),New.HowPlacedId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PlacementId
                               ,'GraduatedDate'
                               ,CONVERT(VARCHAR(8000),New.GraduatedDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PlacementId
                               ,'EmployerJobId'
                               ,CONVERT(VARCHAR(8000),New.EmployerJobId,121)
                        FROM    Inserted New; 
            END; 
        END;



    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[PlStudentsPlaced_Audit_Update] ON [dbo].[PlStudentsPlaced]
    FOR UPDATE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'PlStudentsPlaced','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(Supervisor)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PlacementId
                                   ,'Supervisor'
                                   ,CONVERT(VARCHAR(8000),Old.Supervisor,121)
                                   ,CONVERT(VARCHAR(8000),New.Supervisor,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PlacementId = New.PlacementId
                            WHERE   Old.Supervisor <> New.Supervisor
                                    OR (
                                         Old.Supervisor IS NULL
                                         AND New.Supervisor IS NOT NULL
                                       )
                                    OR (
                                         New.Supervisor IS NULL
                                         AND Old.Supervisor IS NOT NULL
                                       ); 
                IF UPDATE(JobStatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PlacementId
                                   ,'JobStatusId'
                                   ,CONVERT(VARCHAR(8000),Old.JobStatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.JobStatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PlacementId = New.PlacementId
                            WHERE   Old.JobStatusId <> New.JobStatusId
                                    OR (
                                         Old.JobStatusId IS NULL
                                         AND New.JobStatusId IS NOT NULL
                                       )
                                    OR (
                                         New.JobStatusId IS NULL
                                         AND Old.JobStatusId IS NOT NULL
                                       ); 
                IF UPDATE(SSN)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PlacementId
                                   ,'SSN'
                                   ,CONVERT(VARCHAR(8000),Old.SSN,121)
                                   ,CONVERT(VARCHAR(8000),New.SSN,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PlacementId = New.PlacementId
                            WHERE   Old.SSN <> New.SSN
                                    OR (
                                         Old.SSN IS NULL
                                         AND New.SSN IS NOT NULL
                                       )
                                    OR (
                                         New.SSN IS NULL
                                         AND Old.SSN IS NOT NULL
                                       ); 
                IF UPDATE(StartDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PlacementId
                                   ,'StartDate'
                                   ,CONVERT(VARCHAR(8000),Old.StartDate,121)
                                   ,CONVERT(VARCHAR(8000),New.StartDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PlacementId = New.PlacementId
                            WHERE   Old.StartDate <> New.StartDate
                                    OR (
                                         Old.StartDate IS NULL
                                         AND New.StartDate IS NOT NULL
                                       )
                                    OR (
                                         New.StartDate IS NULL
                                         AND Old.StartDate IS NOT NULL
                                       ); 
                IF UPDATE(Salary)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PlacementId
                                   ,'Salary'
                                   ,CONVERT(VARCHAR(8000),Old.Salary,121)
                                   ,CONVERT(VARCHAR(8000),New.Salary,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PlacementId = New.PlacementId
                            WHERE   Old.Salary <> New.Salary
                                    OR (
                                         Old.Salary IS NULL
                                         AND New.Salary IS NOT NULL
                                       )
                                    OR (
                                         New.Salary IS NULL
                                         AND Old.Salary IS NOT NULL
                                       ); 
                IF UPDATE(TerminationReason)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PlacementId
                                   ,'TerminationReason'
                                   ,CONVERT(VARCHAR(8000),Old.TerminationReason,121)
                                   ,CONVERT(VARCHAR(8000),New.TerminationReason,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PlacementId = New.PlacementId
                            WHERE   Old.TerminationReason <> New.TerminationReason
                                    OR (
                                         Old.TerminationReason IS NULL
                                         AND New.TerminationReason IS NOT NULL
                                       )
                                    OR (
                                         New.TerminationReason IS NULL
                                         AND Old.TerminationReason IS NOT NULL
                                       ); 
                IF UPDATE(TerminationDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PlacementId
                                   ,'TerminationDate'
                                   ,CONVERT(VARCHAR(8000),Old.TerminationDate,121)
                                   ,CONVERT(VARCHAR(8000),New.TerminationDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PlacementId = New.PlacementId
                            WHERE   Old.TerminationDate <> New.TerminationDate
                                    OR (
                                         Old.TerminationDate IS NULL
                                         AND New.TerminationDate IS NOT NULL
                                       )
                                    OR (
                                         New.TerminationDate IS NULL
                                         AND Old.TerminationDate IS NOT NULL
                                       ); 
                IF UPDATE(Notes)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PlacementId
                                   ,'Notes'
                                   ,CONVERT(VARCHAR(8000),Old.Notes,121)
                                   ,CONVERT(VARCHAR(8000),New.Notes,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PlacementId = New.PlacementId
                            WHERE   Old.Notes <> New.Notes
                                    OR (
                                         Old.Notes IS NULL
                                         AND New.Notes IS NOT NULL
                                       )
                                    OR (
                                         New.Notes IS NULL
                                         AND Old.Notes IS NOT NULL
                                       ); 
                IF UPDATE(WorkDaysId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PlacementId
                                   ,'WorkDaysId'
                                   ,CONVERT(VARCHAR(8000),Old.WorkDaysId,121)
                                   ,CONVERT(VARCHAR(8000),New.WorkDaysId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PlacementId = New.PlacementId
                            WHERE   Old.WorkDaysId <> New.WorkDaysId
                                    OR (
                                         Old.WorkDaysId IS NULL
                                         AND New.WorkDaysId IS NOT NULL
                                       )
                                    OR (
                                         New.WorkDaysId IS NULL
                                         AND Old.WorkDaysId IS NOT NULL
                                       ); 
                IF UPDATE(BenefitsId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PlacementId
                                   ,'BenefitsId'
                                   ,CONVERT(VARCHAR(8000),Old.BenefitsId,121)
                                   ,CONVERT(VARCHAR(8000),New.BenefitsId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PlacementId = New.PlacementId
                            WHERE   Old.BenefitsId <> New.BenefitsId
                                    OR (
                                         Old.BenefitsId IS NULL
                                         AND New.BenefitsId IS NOT NULL
                                       )
                                    OR (
                                         New.BenefitsId IS NULL
                                         AND Old.BenefitsId IS NOT NULL
                                       ); 
                IF UPDATE(JobDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PlacementId
                                   ,'JobDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.JobDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.JobDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PlacementId = New.PlacementId
                            WHERE   Old.JobDescrip <> New.JobDescrip
                                    OR (
                                         Old.JobDescrip IS NULL
                                         AND New.JobDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.JobDescrip IS NULL
                                         AND Old.JobDescrip IS NOT NULL
                                       ); 
                IF UPDATE(ScheduleId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PlacementId
                                   ,'ScheduleId'
                                   ,CONVERT(VARCHAR(8000),Old.ScheduleId,121)
                                   ,CONVERT(VARCHAR(8000),New.ScheduleId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PlacementId = New.PlacementId
                            WHERE   Old.ScheduleId <> New.ScheduleId
                                    OR (
                                         Old.ScheduleId IS NULL
                                         AND New.ScheduleId IS NOT NULL
                                       )
                                    OR (
                                         New.ScheduleId IS NULL
                                         AND Old.ScheduleId IS NOT NULL
                                       ); 
                IF UPDATE(PlacementRep)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PlacementId
                                   ,'PlacementRep'
                                   ,CONVERT(VARCHAR(8000),Old.PlacementRep,121)
                                   ,CONVERT(VARCHAR(8000),New.PlacementRep,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PlacementId = New.PlacementId
                            WHERE   Old.PlacementRep <> New.PlacementRep
                                    OR (
                                         Old.PlacementRep IS NULL
                                         AND New.PlacementRep IS NOT NULL
                                       )
                                    OR (
                                         New.PlacementRep IS NULL
                                         AND Old.PlacementRep IS NOT NULL
                                       ); 
                IF UPDATE(Reason)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PlacementId
                                   ,'Reason'
                                   ,CONVERT(VARCHAR(8000),Old.Reason,121)
                                   ,CONVERT(VARCHAR(8000),New.Reason,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PlacementId = New.PlacementId
                            WHERE   Old.Reason <> New.Reason
                                    OR (
                                         Old.Reason IS NULL
                                         AND New.Reason IS NOT NULL
                                       )
                                    OR (
                                         New.Reason IS NULL
                                         AND Old.Reason IS NOT NULL
                                       ); 
                IF UPDATE(SalaryTypeId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PlacementId
                                   ,'SalaryTypeId'
                                   ,CONVERT(VARCHAR(8000),Old.SalaryTypeId,121)
                                   ,CONVERT(VARCHAR(8000),New.SalaryTypeId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PlacementId = New.PlacementId
                            WHERE   Old.SalaryTypeId <> New.SalaryTypeId
                                    OR (
                                         Old.SalaryTypeId IS NULL
                                         AND New.SalaryTypeId IS NOT NULL
                                       )
                                    OR (
                                         New.SalaryTypeId IS NULL
                                         AND Old.SalaryTypeId IS NOT NULL
                                       ); 
                IF UPDATE(PlacedDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PlacementId
                                   ,'PlacedDate'
                                   ,CONVERT(VARCHAR(8000),Old.PlacedDate,121)
                                   ,CONVERT(VARCHAR(8000),New.PlacedDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PlacementId = New.PlacementId
                            WHERE   Old.PlacedDate <> New.PlacedDate
                                    OR (
                                         Old.PlacedDate IS NULL
                                         AND New.PlacedDate IS NOT NULL
                                       )
                                    OR (
                                         New.PlacedDate IS NULL
                                         AND Old.PlacedDate IS NOT NULL
                                       ); 
                IF UPDATE(InterviewId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PlacementId
                                   ,'InterviewId'
                                   ,CONVERT(VARCHAR(8000),Old.InterviewId,121)
                                   ,CONVERT(VARCHAR(8000),New.InterviewId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PlacementId = New.PlacementId
                            WHERE   Old.InterviewId <> New.InterviewId
                                    OR (
                                         Old.InterviewId IS NULL
                                         AND New.InterviewId IS NOT NULL
                                       )
                                    OR (
                                         New.InterviewId IS NULL
                                         AND Old.InterviewId IS NOT NULL
                                       ); 
                IF UPDATE(FldStudyId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PlacementId
                                   ,'FldStudyId'
                                   ,CONVERT(VARCHAR(8000),Old.FldStudyId,121)
                                   ,CONVERT(VARCHAR(8000),New.FldStudyId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PlacementId = New.PlacementId
                            WHERE   Old.FldStudyId <> New.FldStudyId
                                    OR (
                                         Old.FldStudyId IS NULL
                                         AND New.FldStudyId IS NOT NULL
                                       )
                                    OR (
                                         New.FldStudyId IS NULL
                                         AND Old.FldStudyId IS NOT NULL
                                       ); 
                IF UPDATE(StuEnrollId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PlacementId
                                   ,'StuEnrollId'
                                   ,CONVERT(VARCHAR(8000),Old.StuEnrollId,121)
                                   ,CONVERT(VARCHAR(8000),New.StuEnrollId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PlacementId = New.PlacementId
                            WHERE   Old.StuEnrollId <> New.StuEnrollId
                                    OR (
                                         Old.StuEnrollId IS NULL
                                         AND New.StuEnrollId IS NOT NULL
                                       )
                                    OR (
                                         New.StuEnrollId IS NULL
                                         AND Old.StuEnrollId IS NOT NULL
                                       ); 
                IF UPDATE(Fee)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PlacementId
                                   ,'Fee'
                                   ,CONVERT(VARCHAR(8000),Old.Fee,121)
                                   ,CONVERT(VARCHAR(8000),New.Fee,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PlacementId = New.PlacementId
                            WHERE   Old.Fee <> New.Fee
                                    OR (
                                         Old.Fee IS NULL
                                         AND New.Fee IS NOT NULL
                                       )
                                    OR (
                                         New.Fee IS NULL
                                         AND Old.Fee IS NOT NULL
                                       ); 
                IF UPDATE(HowPlacedId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PlacementId
                                   ,'HowPlacedId'
                                   ,CONVERT(VARCHAR(8000),Old.HowPlacedId,121)
                                   ,CONVERT(VARCHAR(8000),New.HowPlacedId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PlacementId = New.PlacementId
                            WHERE   Old.HowPlacedId <> New.HowPlacedId
                                    OR (
                                         Old.HowPlacedId IS NULL
                                         AND New.HowPlacedId IS NOT NULL
                                       )
                                    OR (
                                         New.HowPlacedId IS NULL
                                         AND Old.HowPlacedId IS NOT NULL
                                       ); 
                IF UPDATE(GraduatedDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PlacementId
                                   ,'GraduatedDate'
                                   ,CONVERT(VARCHAR(8000),Old.GraduatedDate,121)
                                   ,CONVERT(VARCHAR(8000),New.GraduatedDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PlacementId = New.PlacementId
                            WHERE   Old.GraduatedDate <> New.GraduatedDate
                                    OR (
                                         Old.GraduatedDate IS NULL
                                         AND New.GraduatedDate IS NOT NULL
                                       )
                                    OR (
                                         New.GraduatedDate IS NULL
                                         AND Old.GraduatedDate IS NOT NULL
                                       ); 
                IF UPDATE(EmployerJobId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PlacementId
                                   ,'EmployerJobId'
                                   ,CONVERT(VARCHAR(8000),Old.EmployerJobId,121)
                                   ,CONVERT(VARCHAR(8000),New.EmployerJobId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PlacementId = New.PlacementId
                            WHERE   Old.EmployerJobId <> New.EmployerJobId
                                    OR (
                                         Old.EmployerJobId IS NULL
                                         AND New.EmployerJobId IS NOT NULL
                                       )
                                    OR (
                                         New.EmployerJobId IS NULL
                                         AND Old.EmployerJobId IS NOT NULL
                                       ); 
            END; 
        END;



    SET NOCOUNT OFF;

GO
ALTER TABLE [dbo].[PlStudentsPlaced] ADD CONSTRAINT [PK_PlStudentsPlaced_PlacementId] PRIMARY KEY CLUSTERED  ([PlacementId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PlStudentsPlaced] ADD CONSTRAINT [FK_PlStudentsPlaced_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY ([StuEnrollId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
ALTER TABLE [dbo].[PlStudentsPlaced] ADD CONSTRAINT [FK_PlStudentsPlaced_plEmployerJobs_EmployerJobId_EmployerJobId] FOREIGN KEY ([EmployerJobId]) REFERENCES [dbo].[plEmployerJobs] ([EmployerJobId])
GO
ALTER TABLE [dbo].[PlStudentsPlaced] ADD CONSTRAINT [FK_PlStudentsPlaced_plFee_Fee_FeeId] FOREIGN KEY ([Fee]) REFERENCES [dbo].[plFee] ([FeeId])
GO
ALTER TABLE [dbo].[PlStudentsPlaced] ADD CONSTRAINT [FK_PlStudentsPlaced_plFldStudy_FldStudyId_FldStudyId] FOREIGN KEY ([FldStudyId]) REFERENCES [dbo].[plFldStudy] ([FldStudyId])
GO
ALTER TABLE [dbo].[PlStudentsPlaced] ADD CONSTRAINT [FK_PlStudentsPlaced_plHowPlaced_HowPlacedId_HowPlacedId] FOREIGN KEY ([HowPlacedId]) REFERENCES [dbo].[plHowPlaced] ([HowPlacedId])
GO
ALTER TABLE [dbo].[PlStudentsPlaced] ADD CONSTRAINT [FK_PlStudentsPlaced_plInterview_InterviewId_InterviewId] FOREIGN KEY ([InterviewId]) REFERENCES [dbo].[plInterview] ([InterviewId])
GO
ALTER TABLE [dbo].[PlStudentsPlaced] ADD CONSTRAINT [FK_PlStudentsPlaced_plJobBenefit_BenefitsId_JobBenefitId] FOREIGN KEY ([BenefitsId]) REFERENCES [dbo].[plJobBenefit] ([JobBenefitId])
GO
ALTER TABLE [dbo].[PlStudentsPlaced] ADD CONSTRAINT [FK_PlStudentsPlaced_plJobSchedule_ScheduleId_JobScheduleId] FOREIGN KEY ([ScheduleId]) REFERENCES [dbo].[plJobSchedule] ([JobScheduleId])
GO
ALTER TABLE [dbo].[PlStudentsPlaced] ADD CONSTRAINT [FK_PlStudentsPlaced_plJobStatus_JobStatusId_JobStatusId] FOREIGN KEY ([JobStatusId]) REFERENCES [dbo].[plJobStatus] ([JobStatusId])
GO
ALTER TABLE [dbo].[PlStudentsPlaced] ADD CONSTRAINT [FK_PlStudentsPlaced_plJobWorkDays_WorkDaysId_JobWorkDaysId] FOREIGN KEY ([WorkDaysId]) REFERENCES [dbo].[plJobWorkDays] ([JobWorkDaysId])
GO
ALTER TABLE [dbo].[PlStudentsPlaced] ADD CONSTRAINT [FK_PlStudentsPlaced_plSalaryType_SalaryTypeId_SalaryTypeId] FOREIGN KEY ([SalaryTypeId]) REFERENCES [dbo].[plSalaryType] ([SalaryTypeId])
GO
ALTER TABLE [dbo].[PlStudentsPlaced] ADD CONSTRAINT [FK_PlStudentsPlaced_syUsers_PlacementRep_UserId] FOREIGN KEY ([PlacementRep]) REFERENCES [dbo].[syUsers] ([UserId])
GO
