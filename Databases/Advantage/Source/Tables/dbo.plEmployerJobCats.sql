CREATE TABLE [dbo].[plEmployerJobCats]
(
[EmployerJobId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_plEmployerJobCats_EmployerJobId] DEFAULT (newid()),
[EmployerId] [uniqueidentifier] NULL,
[JobCatId] [uniqueidentifier] NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
 
--========================================================================================== 
-- TRIGGER plEmployerJobCats_Audit_Delete 
-- INSERT  add Audit History when delete records in plEmployerJobCats 
--========================================================================================== 
CREATE TRIGGER [dbo].[plEmployerJobCats_Audit_Delete] ON [dbo].[plEmployerJobCats]
    FOR DELETE
AS
    BEGIN 
        SET NOCOUNT ON; 
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
        DECLARE @EventRows AS INT; 
        DECLARE @EventDate AS DATETIME; 
        DECLARE @UserName AS VARCHAR(50); 
	
        SET @AuditHistId = NEWID(); 
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Deleted
                         ); 
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Deleted
                         ); 
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Deleted
                        ); 
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'plEmployerJobCats','D',@EventRows,@EventDate,@UserName; 
                -- EmployerId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerJobId
                               ,'EmployerId'
                               ,CONVERT(VARCHAR(MAX),Old.EmployerId)
                        FROM    Deleted AS Old; 
                -- JobCatId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerJobId
                               ,'JobCatId'
                               ,CONVERT(VARCHAR(MAX),Old.JobCatId)
                        FROM    Deleted AS Old; 
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerJobId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                        FROM    Deleted AS Old; 
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerJobId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),Old.ModUser)
                        FROM    Deleted AS Old; 
            END; 
        SET NOCOUNT OFF; 
    END; 
--========================================================================================== 
-- END TRIGGER plEmployerJobCats_Audit_Delete 
--========================================================================================== 
 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
 
--========================================================================================== 
-- TRIGGER plEmployerJobCats_Audit_Insert 
-- INSERT  add Audit History when insert records in plEmployerJobCats 
--========================================================================================== 
CREATE TRIGGER [dbo].[plEmployerJobCats_Audit_Insert] ON [dbo].[plEmployerJobCats]
    FOR INSERT
AS
    BEGIN 
        SET NOCOUNT ON; 
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
        DECLARE @EventRows AS INT; 
        DECLARE @EventDate AS DATETIME; 
        DECLARE @UserName AS VARCHAR(50); 
	
        SET @AuditHistId = NEWID(); 
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         ); 
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         ); 
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        ); 
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'plEmployerJobCats','I',@EventRows,@EventDate,@UserName; 
                -- EmployerId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerJobId
                               ,'EmployerId'
                               ,CONVERT(VARCHAR(MAX),New.EmployerId)
                        FROM    Inserted AS New; 
                -- JobCatId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerJobId
                               ,'JobCatId'
                               ,CONVERT(VARCHAR(MAX),New.JobCatId)
                        FROM    Inserted AS New; 
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerJobId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                        FROM    Inserted AS New; 
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerJobId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),New.ModUser)
                        FROM    Inserted AS New; 
            END; 
        SET NOCOUNT OFF; 
    END; 
--========================================================================================== 
-- END TRIGGER plEmployerJobCats_Audit_Insert 
--========================================================================================== 
 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
--========================================================================================== 
-- TRIGGER plEmployerJobCats_Audit_Update 
-- UPDATE  add Audit History when update fields in plEmployerJobCats 
--========================================================================================== 
CREATE TRIGGER [dbo].[plEmployerJobCats_Audit_Update] ON [dbo].[plEmployerJobCats]
    FOR UPDATE
AS
    BEGIN 
        SET NOCOUNT ON; 
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
        DECLARE @EventRows AS INT; 
        DECLARE @EventDate AS DATETIME; 
        DECLARE @UserName AS VARCHAR(50); 
	
        SET @AuditHistId = NEWID(); 
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         ); 
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         ); 
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        ); 
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'plEmployerJobCats','U',@EventRows,@EventDate,@UserName; 
                -- EmployerId 
                IF UPDATE(EmployerId)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.EmployerJobId
                                       ,'EmployerId'
                                       ,CONVERT(VARCHAR(MAX),Old.EmployerId)
                                       ,CONVERT(VARCHAR(MAX),New.EmployerId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.EmployerJobId = New.EmployerJobId
                                WHERE   Old.EmployerId <> New.EmployerId
                                        OR (
                                             Old.EmployerId IS NULL
                                             AND New.EmployerId IS NOT NULL
                                           )
                                        OR (
                                             New.EmployerId IS NULL
                                             AND Old.EmployerId IS NOT NULL
                                           ); 
                    END; 
                -- JobCatId 
                IF UPDATE(JobCatId)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.EmployerJobId
                                       ,'JobCatId'
                                       ,CONVERT(VARCHAR(MAX),Old.JobCatId)
                                       ,CONVERT(VARCHAR(MAX),New.JobCatId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.EmployerJobId = New.EmployerJobId
                                WHERE   Old.JobCatId <> New.JobCatId
                                        OR (
                                             Old.JobCatId IS NULL
                                             AND New.JobCatId IS NOT NULL
                                           )
                                        OR (
                                             New.JobCatId IS NULL
                                             AND Old.JobCatId IS NOT NULL
                                           ); 
                    END; 
                -- ModDate 
                IF UPDATE(ModDate)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.EmployerJobId
                                       ,'ModDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.EmployerJobId = New.EmployerJobId
                                WHERE   Old.ModDate <> New.ModDate
                                        OR (
                                             Old.ModDate IS NULL
                                             AND New.ModDate IS NOT NULL
                                           )
                                        OR (
                                             New.ModDate IS NULL
                                             AND Old.ModDate IS NOT NULL
                                           ); 
                    END; 
                -- ModUser 
                IF UPDATE(ModUser)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.EmployerJobId
                                       ,'ModUser'
                                       ,CONVERT(VARCHAR(MAX),Old.ModUser)
                                       ,CONVERT(VARCHAR(MAX),New.ModUser)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.EmployerJobId = New.EmployerJobId
                                WHERE   Old.ModUser <> New.ModUser
                                        OR (
                                             Old.ModUser IS NULL
                                             AND New.ModUser IS NOT NULL
                                           )
                                        OR (
                                             New.ModUser IS NULL
                                             AND Old.ModUser IS NOT NULL
                                           ); 
                    END; 
            END; 
        SET NOCOUNT OFF; 
    END; 
--========================================================================================== 
-- END TRIGGER plEmployerJobCats_Audit_Update 
--========================================================================================== 
 
GO
ALTER TABLE [dbo].[plEmployerJobCats] ADD CONSTRAINT [PK_plEmployerJobCats_EmployerJobId] PRIMARY KEY CLUSTERED  ([EmployerJobId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[plEmployerJobCats] ADD CONSTRAINT [FK_plEmployerJobCats_plEmployers_EmployerId_EmployerId] FOREIGN KEY ([EmployerId]) REFERENCES [dbo].[plEmployers] ([EmployerId])
GO
ALTER TABLE [dbo].[plEmployerJobCats] ADD CONSTRAINT [FK_plEmployerJobCats_plJobType_JobCatId_JobGroupId] FOREIGN KEY ([JobCatId]) REFERENCES [dbo].[plJobType] ([JobGroupId])
GO
