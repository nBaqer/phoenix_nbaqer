CREATE TABLE [dbo].[arStdSuspensions]
(
[StuSuspensionId] [uniqueidentifier] NOT NULL,
[StuEnrollId] [uniqueidentifier] NOT NULL,
[StartDate] [datetime] NOT NULL,
[EndDate] [datetime] NOT NULL,
[Reason] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StudentStatusChangeId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arStdSuspensions] ADD CONSTRAINT [PK_arStdSuspensions_StuSuspensionId] PRIMARY KEY CLUSTERED  ([StuSuspensionId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arStdSuspensions] ADD CONSTRAINT [FK_arStdSuspensions_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY ([StuEnrollId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
