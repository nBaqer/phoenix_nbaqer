CREATE TABLE [dbo].[arClsSectionTimeClockPolicy]
(
[ClsSectionPolicyId] [uniqueidentifier] NOT NULL,
[ClsSectionId] [uniqueidentifier] NOT NULL,
[AllowEarlyIn] [bit] NULL,
[AllowLateOut] [bit] NULL,
[AllowExtraHours] [bit] NULL,
[CheckTardyIn] [bit] NULL,
[MaxInBeforeTardy] [datetime] NULL,
[AssignTardyInTime] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arClsSectionTimeClockPolicy] ADD CONSTRAINT [PK_arClsSectionTimeClockPolicy_ClsSectionPolicyId] PRIMARY KEY CLUSTERED  ([ClsSectionPolicyId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arClsSectionTimeClockPolicy] ADD CONSTRAINT [FK_arClsSectionTimeClockPolicy_arClassSections_ClsSectionId_ClsSectionId] FOREIGN KEY ([ClsSectionId]) REFERENCES [dbo].[arClassSections] ([ClsSectionId])
GO
