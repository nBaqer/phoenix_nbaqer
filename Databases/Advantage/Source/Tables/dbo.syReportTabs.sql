CREATE TABLE [dbo].[syReportTabs]
(
[ReportId] [uniqueidentifier] NOT NULL,
[PageViewId] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UserControlName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SetId] [bigint] NOT NULL,
[ControllerClass] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DisplayName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParameterSetLookUp] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TabName] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syReportTabs] ADD CONSTRAINT [PK_syReportTabs_ReportId_PageViewId] PRIMARY KEY CLUSTERED  ([ReportId], [PageViewId]) ON [PRIMARY]
GO
