CREATE TABLE [dbo].[syConfigAppSet_Lookup]
(
[LookUpId] [uniqueidentifier] NOT NULL,
[SettingId] [int] NULL,
[ValueOptions] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syConfigAppSet_Lookup] ADD CONSTRAINT [PK_syConfigAppSet_Lookup_LookUpId] PRIMARY KEY CLUSTERED  ([LookUpId]) ON [PRIMARY]
GO
