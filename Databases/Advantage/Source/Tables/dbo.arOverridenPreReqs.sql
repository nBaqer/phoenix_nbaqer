CREATE TABLE [dbo].[arOverridenPreReqs]
(
[OverridenPreReqId] [uniqueidentifier] NOT NULL,
[StuEnrollId] [uniqueidentifier] NOT NULL,
[ReqId] [uniqueidentifier] NOT NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arOverridenPreReqs] ADD CONSTRAINT [PK_arOverridenPreReqs_OverridenPreReqId] PRIMARY KEY CLUSTERED  ([OverridenPreReqId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_arOverridenPreReqs_StuEnrollId_ReqId] ON [dbo].[arOverridenPreReqs] ([StuEnrollId], [ReqId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arOverridenPreReqs] ADD CONSTRAINT [FK_arOverridenPreReqs_arReqs_ReqId_ReqId] FOREIGN KEY ([ReqId]) REFERENCES [dbo].[arReqs] ([ReqId])
GO
ALTER TABLE [dbo].[arOverridenPreReqs] ADD CONSTRAINT [FK_arOverridenPreReqs_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY ([StuEnrollId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
