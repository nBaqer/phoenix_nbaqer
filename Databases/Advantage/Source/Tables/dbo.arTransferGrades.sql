CREATE TABLE [dbo].[arTransferGrades]
(
[TransferId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arTransferGrades_TransferId] DEFAULT (newid()),
[StuEnrollId] [uniqueidentifier] NOT NULL,
[ReqId] [uniqueidentifier] NOT NULL,
[GrdSysDetailId] [uniqueidentifier] NULL,
[Score] [decimal] (18, 2) NULL,
[TermId] [uniqueidentifier] NOT NULL,
[ModDate] [datetime] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsTransferred] [bit] NULL CONSTRAINT [DF_arTransferGrades_IsTransferred] DEFAULT ((0)),
[CompletedDate] [datetime] NULL,
[isClinicsSatisfied] [bit] NULL CONSTRAINT [DF_arTransferGrades_isClinicsSatisfied] DEFAULT ((0)),
[IsCourseCompleted] [bit] NOT NULL CONSTRAINT [DF_arTransferGrades_IsCourseCompleted] DEFAULT ((0)),
[IsGradeOverridden] [bit] NOT NULL CONSTRAINT [DF_arTransferGrades_IsGradeOverridden] DEFAULT ((0)),
[GradeOverriddenBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GradeOverriddenDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--==========================================================================================
-- TRIGGER TR_InsertCreditSummary_TransferGrades
-- AFTER INSERT, UPDATE  
--==========================================================================================
CREATE TRIGGER [dbo].[TR_InsertCreditSummary_TransferGrades]
ON [dbo].[arTransferGrades]
AFTER INSERT, UPDATE
AS
SET NOCOUNT ON;

DECLARE @PrgVerId UNIQUEIDENTIFIER
       ,@rownumber INT
       ,@StuEnrollId UNIQUEIDENTIFIER;
DECLARE @PrevStuEnrollId UNIQUEIDENTIFIER
       ,@PrevReqId UNIQUEIDENTIFIER
       ,@PrevTermId UNIQUEIDENTIFIER
       ,@CreditsAttempted DECIMAL(18, 2)
       ,@CreditsEarned DECIMAL(18, 2)
       ,@TermId UNIQUEIDENTIFIER
       ,@TermDescrip VARCHAR(50);
DECLARE @reqid UNIQUEIDENTIFIER
       ,@CourseCodeDescrip VARCHAR(50)
       ,@FinalGrade UNIQUEIDENTIFIER
       ,@FinalScore DECIMAL(18, 2)
       ,@ClsSectionId UNIQUEIDENTIFIER
       ,@Grade VARCHAR(50)
       ,@IsGradeBookNotSatisified BIT
       ,@TermStartDate DATETIME;
DECLARE @IsPass BIT
       ,@IsCreditsAttempted BIT
       ,@IsCreditsEarned BIT
       ,@Completed BIT
       ,@CurrentScore DECIMAL(18, 2)
       ,@CurrentGrade VARCHAR(10)
       ,@FinalGradeDesc VARCHAR(50)
       ,@FinalGPA DECIMAL(18, 2)
       ,@GrdBkResultId UNIQUEIDENTIFIER;
DECLARE @Product_WeightedAverage_Credits_GPA DECIMAL(18, 2)
       ,@Count_WeightedAverage_Credits DECIMAL(18, 2)
       ,@Product_SimpleAverage_Credits_GPA DECIMAL(18, 2)
       ,@Count_SimpleAverage_Credits DECIMAL(18, 2);
DECLARE @CreditsPerService DECIMAL(18, 2)
       ,@NumberOfServicesAttempted INT
       ,@boolCourseHasLabWorkOrLabHours INT
       ,@sysComponentTypeId INT
       ,@RowCount INT;
DECLARE @decGPALoop DECIMAL(18, 2)
       ,@intCourseCount INT
       ,@decWeightedGPALoop DECIMAL(18, 2)
       ,@IsInGPA BIT
       ,@isGradeEligibleForCreditsEarned BIT
       ,@isGradeEligibleForCreditsAttempted BIT;
DECLARE @ComputedSimpleGPA DECIMAL(18, 2)
       ,@ComputedWeightedGPA DECIMAL(18, 2)
       ,@CourseCredits DECIMAL(18, 2);
DECLARE @FinAidCreditsEarned DECIMAL(18, 2)
       ,@FinAidCredits DECIMAL(18, 2)
       ,@TermAverage DECIMAL(18, 2)
       ,@TermAverageCount INT;
DECLARE @IsWeighted INT;
DECLARE @StuEnrollCampusId UNIQUEIDENTIFIER;
SET @decGPALoop = 0;
SET @intCourseCount = 0;
SET @decWeightedGPALoop = 0;
SET @ComputedSimpleGPA = 0;
SET @ComputedWeightedGPA = 0;
SET @CourseCredits = 0;
DECLARE GetCreditsSummary_Cursor CURSOR FOR
    SELECT   DISTINCT SE.StuEnrollId
            ,T.TermId
            ,T.TermDescrip
            ,T.StartDate
            ,R.ReqId
            ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
            ,RES.Score AS FinalScore
            ,RES.GrdSysDetailId AS FinalGrade
            ,GCT.SysComponentTypeId
            ,R.Credits AS CreditsAttempted
            ,CS.ClsSectionId
            ,GSD.Grade
            ,GSD.IsPass
            ,GSD.IsCreditsAttempted
            ,GSD.IsCreditsEarned
            ,SE.PrgVerId
            ,GSD.IsInGPA
            ,R.FinAidCredits AS FinAidCredits
    FROM     INSERTED I
    INNER JOIN dbo.arStuEnrollments SE ON I.StuEnrollId = SE.StuEnrollId
    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
    INNER JOIN arTransferGrades RES ON RES.StuEnrollId = SE.StuEnrollId
    INNER JOIN arClassSections CS ON RES.ReqId = CS.ReqId
                                     AND RES.TermId = CS.TermId
    INNER JOIN arTerm T ON CS.TermId = T.TermId
    INNER JOIN arReqs R ON CS.ReqId = R.ReqId
    LEFT JOIN arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
    LEFT JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
    LEFT JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
    LEFT JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
    --where SE.StuEnrollId='CF2A8DCC-807F-48CA-8B85-03FA12BA1DFB'  
    ORDER BY T.StartDate
            ,T.TermDescrip
            ,R.ReqId;
OPEN GetCreditsSummary_Cursor;
SET @PrevStuEnrollId = NULL;
SET @PrevTermId = NULL;
SET @PrevReqId = NULL;
SET @RowCount = 0;
FETCH NEXT FROM GetCreditsSummary_Cursor
INTO @StuEnrollId
    ,@TermId
    ,@TermDescrip
    ,@TermStartDate
    ,@reqid
    ,@CourseCodeDescrip
    ,@FinalScore
    ,@FinalGrade
    ,@sysComponentTypeId
    ,@CreditsAttempted
    ,@ClsSectionId
    ,@Grade
    ,@IsPass
    ,@IsCreditsAttempted
    ,@IsCreditsEarned
    ,@PrgVerId
    ,@IsInGPA
    ,@FinAidCredits; --,@GrdBkResultId  
WHILE @@FETCH_STATUS = 0
    BEGIN

        SET @StuEnrollCampusId = (
                                 SELECT CampusId
                                 FROM   dbo.arStuEnrollments
                                 WHERE  StuEnrollId = @StuEnrollId
                                 );
        SET @CourseCredits = @CreditsAttempted;
        SET @RowCount = @RowCount + 1;

        -- Changes made on 12/28/2010 starts here  
        DECLARE @ShowROSSOnlyTabsForStudent_Value BIT
               ,@SetGradeBookAt VARCHAR(50)
               ,@GradesFormat VARCHAR(50);
        --set @ShowROSSOnlyTabsForStudent_Value = (select value from syConfigAppSetValues where settingId=68)  
        SET @ShowROSSOnlyTabsForStudent_Value = (
                                                SELECT dbo.GetAppSettingValue(68, @StuEnrollCampusId)
                                                );
        --SET @SetGradeBookAt = (SELECT value FROM syConfigAppSetValues WHERE SettingId=43)  
        SET @SetGradeBookAt = (
                              SELECT dbo.GetAppSettingValue(43, @StuEnrollCampusId)
                              );
        --set @GradesFormat = (select Value from syConfigAppSetValues where SettingId=47) -- 47 refers to grades format  
        SET @GradesFormat = (
                            SELECT dbo.GetAppSettingValue(47, @StuEnrollCampusId)
                            ); -- 47 refers to grades format  
        SET @FinalGradeDesc = @FinalGrade;
        -- Changes made on 12/28/2010 ends here  

        SET @IsGradeBookNotSatisified = (
                                        SELECT COUNT(*) AS UnsatisfiedWorkUnits
                                        FROM   (
                                               SELECT DISTINCT D.*
                                                     --Case When D.MinimumScore > D.Score Then (D.MinimumScore-D.Score) else 0 end as Remaining,  
                                                     --Case When (D.MinimumScore > D.Score) And (D.MustPass=1) then 0   
                                                     --When (SysComponentTypeId=500 OR SysComponentTypeId=503 OR SysComponentTypeId=544) AND (@ShowROSSOnlyTabsForStudent_Value=0) and (D.Score is not null) and (D.MinimumScore > D.Score) then 0  
                                                     ----When (@sysComponentTypeId=503 or @sysComponentTypeId=500) and (D.MinimumScore > D.Score) then 0  
                                                     ---- When (@ShowROSSOnlyTabsForStudent_Value=0) and (D.Score is not null) and (D.MinimumScore > D.Score) then 0  
                                                     ----When (@ShowROSSOnlyTabsForStudent_Value=1) and (D.Score is not null) and (D.MinimumScore > D.Score) and D.Required=1 then 0  
                                                     --When @ShowROSSOnlyTabsForStudent_Value=1 and D.Score is null and D.Required=1 then 0  
                                                     --When D.Score is Null and D.FinalScore is null And (D.Required=1) then 0   
                                                     --else 1 end as IsWorkUnitSatisfied  
                                                     -- Changes made on 12/28/2010 starts here  
                                                     ,CASE WHEN D.MinimumScore > D.Score THEN ( D.MinimumScore - D.Score )
                                                           ELSE 0
                                                      END AS Remaining
                                                     ,CASE WHEN ( D.MinimumScore > D.Score )
                                                                AND ( D.MustPass = 1 ) THEN 0
                                                           WHEN (
                                                                SysComponentTypeId = 500
                                                                OR SysComponentTypeId = 503
                                                                OR SysComponentTypeId = 544
                                                                )
                                                                AND ( @ShowROSSOnlyTabsForStudent_Value = 0 )
                                                                AND ( D.Score IS NOT NULL )
                                                                AND ( D.MinimumScore > D.Score ) THEN 0
                                                           --When (@ShowROSSOnlyTabsForStudent_Value=0) and (D.Score is not null) and (D.MinimumScore > D.Score) then 0  
                                                           --When (@ShowROSSOnlyTabsForStudent_Value=1) and (D.Score is not null) and (D.MinimumScore > D.Score) and D.Required=1 then 0  
                                                           --Non Ross/ Course Level/ No LabHrLabWorkExternship/Numeric/FinalScore is not posted  
                                                           WHEN @ShowROSSOnlyTabsForStudent_Value = 0
                                                                AND LOWER(LTRIM(RTRIM(@SetGradeBookAt))) = 'courselevel'
                                                                AND LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
                                                                AND (
                                                                    SysComponentTypeId IS NULL
                                                                    OR (
                                                                       SysComponentTypeId <> 500
                                                                       AND SysComponentTypeId <> 503
                                                                       AND SysComponentTypeId <> 544
                                                                       )
                                                                    )
                                                                AND @FinalScore IS NULL THEN 0
                                                           --Non Ross/ Course Level/ No LabHrLabWorkExternship/Letter/Final Grade is not posted  
                                                           WHEN @ShowROSSOnlyTabsForStudent_Value = 0
                                                                AND LOWER(LTRIM(RTRIM(@SetGradeBookAt))) = 'courselevel'
                                                                AND LOWER(LTRIM(RTRIM(@GradesFormat))) = 'letter'
                                                                AND (
                                                                    SysComponentTypeId IS NULL
                                                                    OR (
                                                                       SysComponentTypeId <> 500
                                                                       AND SysComponentTypeId <> 503
                                                                       AND SysComponentTypeId <> 544
                                                                       )
                                                                    )
                                                                AND @FinalGradeDesc IS NULL THEN 0
                                                           WHEN @ShowROSSOnlyTabsForStudent_Value = 1
                                                                AND D.Score IS NULL
                                                                AND D.Required = 1 THEN 0
                                                           WHEN D.Score IS NULL
                                                                AND D.FinalScore IS NULL
                                                                AND ( D.Required = 1 ) THEN 0
                                                           ELSE 1
                                                      END AS IsWorkUnitSatisfied
                                               -- Changes made on 12/28/2010 ends here  
                                               FROM   (
                                                      SELECT DISTINCT T.TermId
                                                            ,T.TermDescrip
                                                            ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                                            ,R.ReqId
                                                            ,RTRIM(GCT.Descrip) AS GradeBookDescription
                                                            ,( CASE WHEN GCT.SysComponentTypeId IN ( 500, 503, 504 ) THEN GBWD.Number
                                                                    ELSE (
                                                                         SELECT MIN(MinVal)
                                                                         FROM   arGradeScaleDetails GSD
                                                                               ,arGradeSystemDetails GSS
                                                                         WHERE  GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                                AND GSS.IsPass = 1
                                                                         )
                                                               END
                                                             ) AS MinimumScore
                                                            --GBR.Score as Score,    
                                                            ,GBWD.Weight AS Weight
                                                            ,RES.Score AS FinalScore
                                                            ,RES.GrdSysDetailId AS FinalGrade
                                                            ,GBWD.Required
                                                            ,GBWD.MustPass
                                                            ,GBWD.GrdPolicyId
                                                            ,( CASE GCT.SysComponentTypeId
                                                                    WHEN 544 THEN (
                                                                                  SELECT SUM(HoursAttended)
                                                                                  FROM   arExternshipAttendance
                                                                                  WHERE  StuEnrollId = SE.StuEnrollId
                                                                                  )
                                                                    WHEN 503 THEN (
                                                                                  SELECT SUM(Score)
                                                                                  FROM   arGrdBkResults
                                                                                  WHERE  StuEnrollId = SE.StuEnrollId
                                                                                         AND ClsSectionId = CS.ClsSectionId
                                                                                         AND InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                                                                  )
                                                                    ELSE GBR.Score
                                                               END
                                                             ) AS Score
                                                            ,GCT.SysComponentTypeId
                                                            ,SE.StuEnrollId
                                                            --GBR.GrdBkResultId,  
                                                            ,R.Credits AS CreditsAttempted
                                                            ,CS.ClsSectionId
                                                            ,GSD.Grade
                                                            ,GSD.IsPass
                                                            ,GSD.IsCreditsAttempted
                                                            ,GSD.IsCreditsEarned
                                                      FROM   arStuEnrollments SE
                                                      INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                                      INNER JOIN arTransferGrades RES ON RES.StuEnrollId = SE.StuEnrollId
                                                      INNER JOIN arClassSections CS ON RES.ReqId = CS.ReqId
                                                                                       AND RES.TermId = CS.TermId
                                                      INNER JOIN arTerm T ON CS.TermId = T.TermId
                                                      INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                                                      LEFT JOIN arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                                                                                      AND GBR.StuEnrollId = SE.StuEnrollId
                                                      LEFT JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                                      LEFT JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                      LEFT JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                                                      WHERE  SE.StuEnrollId = @StuEnrollId
                                                             AND T.TermId = @TermId
                                                             AND R.ReqId = @reqid
                                                      -- and GBR.ResNum>=1  
                                                      ) D
                                               ) E
                                        WHERE  IsWorkUnitSatisfied = 0
                                        );


        --Check if IsCreditsAttempted is set to True  
        IF (
           @IsCreditsAttempted IS NULL
           OR @IsCreditsAttempted = 0
           )
            BEGIN
                SET @CreditsAttempted = 0;
            END;
        IF (
           @IsCreditsEarned IS NULL
           OR @IsCreditsEarned = 0
           )
            BEGIN
                SET @CreditsEarned = 0;
            END;

        IF ( @IsGradeBookNotSatisified >= 1 )
            BEGIN
                SET @CreditsEarned = 0;
                SET @Completed = 0;
            END;
        ELSE
            BEGIN
                SET @GrdBkResultId = (
                                     SELECT TOP 1 GrdBkResultId
                                     FROM   arGrdBkResults
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND ClsSectionId = @ClsSectionId
                                     );
                IF @GrdBkResultId IS NOT NULL
                    BEGIN
                        IF ( @IsCreditsEarned = 1 )
                            BEGIN
                                SET @CreditsEarned = @CreditsAttempted;
                                SET @FinAidCreditsEarned = @FinAidCredits;
                            END;
                        SET @Completed = 1;
                    END;
                IF (
                   @GrdBkResultId IS NULL
                   AND @Grade IS NOT NULL
                   )
                    BEGIN
                        IF ( @IsCreditsEarned = 1 )
                            BEGIN
                                SET @CreditsEarned = @CreditsAttempted;
                                SET @FinAidCreditsEarned = @FinAidCredits;
                            END;
                        SET @Completed = 1;
                    END;
            END;
        IF (
           @FinalScore IS NOT NULL
           AND @Grade IS NOT NULL
           )
            BEGIN
                IF ( @IsCreditsEarned = 1 )
                    BEGIN
                        SET @CreditsEarned = @CreditsAttempted;
                        SET @FinAidCreditsEarned = @FinAidCredits;
                    END;
                SET @Completed = 1;

            END;

        -- If course is not part of the program version definition do not add credits earned and credits attempted  
        -- set the credits earned and attempted to zero  
        DECLARE @coursepartofdefinition INT;
        SET @coursepartofdefinition = 0;

        SET @coursepartofdefinition = (
                                      SELECT COUNT(*) AS RowCountOfProgramDefinition
                                      FROM   (
                                             SELECT ProgVerDefId
                                                   ,PrgVerId
                                                   ,ReqId
                                                   ,ReqSeq
                                                   ,IsRequired
                                                   ,Cnt
                                                   ,Hours
                                                   ,TrkForCompletion
                                                   ,Credits
                                                   ,GrdSysDetailId
                                                   ,ModUser
                                                   ,ModDate
                                                   ,TermNo
                                             FROM   arProgVerDef
                                             WHERE  PrgVerId = @PrgVerId
                                                    AND ReqId = @reqid
                                             UNION
                                             SELECT ProgVerDefId
                                                   ,PrgVerId
                                                   ,ReqId
                                                   ,ReqSeq
                                                   ,IsRequired
                                                   ,Cnt
                                                   ,Hours
                                                   ,TrkForCompletion
                                                   ,Credits
                                                   ,GrdSysDetailId
                                                   ,ModUser
                                                   ,ModDate
                                                   ,TermNo
                                             FROM   arProgVerDef
                                             WHERE  PrgVerId = @PrgVerId
                                                    AND ReqId IN (
                                                                 SELECT GrpId
                                                                 FROM   arReqGrpDef
                                                                 WHERE  ReqId = @reqid
                                                                 )
                                             ) dt
                                      );
        IF ( @coursepartofdefinition = 0 )
            BEGIN
                SET @CreditsEarned = 0;
                SET @CreditsAttempted = 0;
                SET @FinAidCreditsEarned = 0;
            END;


        -- Check the grade scale associated with the class section and figure out of the final score was a passing score  
        DECLARE @coursepassrowcount INT;
        SET @coursepassrowcount = 0;
        IF ( @FinalScore IS NOT NULL )

            -- If the student scores 56 and the score is a passing score then we consider this course as completed  
            BEGIN
                SET @coursepassrowcount = (
                                          SELECT COUNT(t2.MinVal) AS IsCourseCompleted
                                          FROM   arClassSections t1
                                          INNER JOIN arGradeScaleDetails t2 ON t1.GrdScaleId = t2.GrdScaleId
                                          INNER JOIN arGradeSystemDetails t3 ON t2.GrdSysDetailId = t3.GrdSysDetailId
                                          WHERE  t1.ClsSectionId = @ClsSectionId
                                                 AND t3.IsPass = 1
                                                 AND @FinalScore >= t2.MinVal
                                          );
                IF @coursepassrowcount >= 1
                    BEGIN
                        SET @Completed = 1;
                    END;
                ELSE
                    BEGIN
                        SET @Completed = 0;
                    END;
            END;

        -- If Student Scored a Failing Grade (IsPass set to 0 in Grade System)  
        -- then mark this course as Incomplete  
        IF ( @FinalGrade IS NOT NULL )
            BEGIN
                IF ( @IsPass = 0 )
                    BEGIN
                        SET @Completed = 0;
                        IF ( @IsCreditsEarned = 0 )
                            BEGIN
                                SET @CreditsEarned = 0;
                                SET @FinAidCreditsEarned = 0;
                            END;
                        IF ( @IsCreditsAttempted = 0 )
                            BEGIN
                                SET @CreditsAttempted = 0;
                            END;
                    END;
            END;

        SET @CurrentScore = (
                            SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                        ELSE NULL
                                   END AS CurrentScore
                            FROM   (
                                   SELECT   InstrGrdBkWgtDetailId
                                           ,Code
                                           ,Descrip
                                           ,Weight AS GradeBookWeight
                                           ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                 ELSE 0
                                            END AS ActualWeight
                                   FROM     (
                                            SELECT   C.InstrGrdBkWgtDetailId
                                                    ,D.Code
                                                    ,D.Descrip
                                                    ,ISNULL(C.Weight, 0) AS Weight
                                                    ,C.Number AS MinNumber
                                                    ,C.GrdPolicyId
                                                    ,C.Parameter AS Param
                                                    ,X.GrdScaleId
                                                    ,SUM(GR.Score) AS Score
                                                    ,COUNT(D.Descrip) AS NumberOfComponents
                                            FROM     (
                                                     SELECT   DISTINCT TOP 1 A.InstrGrdBkWgtId
                                                             ,A.EffectiveDate
                                                             ,B.GrdScaleId
                                                     FROM     arGrdBkWeights A
                                                             ,arClassSections B
                                                     WHERE    A.ReqId = B.ReqId
                                                              AND A.EffectiveDate <= B.StartDate
                                                              AND B.ClsSectionId = @ClsSectionId
                                                     ORDER BY A.EffectiveDate DESC
                                                     ) X
                                                    ,arGrdBkWgtDetails C
                                                    ,arGrdComponentTypes D
                                                    ,arGrdBkResults GR
                                            WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                     AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                     AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                     AND D.SysComponentTypeId NOT IN ( 500, 503 )
                                                     AND GR.StuEnrollId = @StuEnrollId
                                                     AND GR.ClsSectionId = @ClsSectionId
                                                     AND GR.Score IS NOT NULL
                                            GROUP BY C.InstrGrdBkWgtDetailId
                                                    ,D.Code
                                                    ,D.Descrip
                                                    ,C.Weight
                                                    ,C.Number
                                                    ,C.GrdPolicyId
                                                    ,C.Parameter
                                                    ,X.GrdScaleId
                                            ) S
                                   GROUP BY InstrGrdBkWgtDetailId
                                           ,Code
                                           ,Descrip
                                           ,Weight
                                           ,NumberOfComponents
                                   ) FinalTblToComputeCurrentScore
                            );
        IF ( @CurrentScore IS NULL )
            BEGIN
                -- instructor grade books  
                SET @CurrentScore = (
                                    SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                ELSE NULL
                                           END AS CurrentScore
                                    FROM   (
                                           SELECT   InstrGrdBkWgtDetailId
                                                   ,Code
                                                   ,Descrip
                                                   ,Weight AS GradeBookWeight
                                                   ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                         ELSE 0
                                                    END AS ActualWeight
                                           FROM     (
                                                    SELECT   C.InstrGrdBkWgtDetailId
                                                            ,D.Code
                                                            ,D.Descrip
                                                            ,ISNULL(C.Weight, 0) AS Weight
                                                            ,C.Number AS MinNumber
                                                            ,C.GrdPolicyId
                                                            ,C.Parameter AS Param
                                                            ,X.GrdScaleId
                                                            ,SUM(GR.Score) AS Score
                                                            ,COUNT(D.Descrip) AS NumberOfComponents
                                                    FROM     (
                                                             --SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId          
                                                             --FROM          arGrdBkWeights A,arClassSections B          
                                                             --WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=@ClsSectionId  
                                                             --ORDER BY      A.EffectiveDate DESC  
                                                             SELECT DISTINCT TOP 1 t1.InstrGrdBkWgtId
                                                                   ,t1.GrdScaleId
                                                             FROM   arClassSections t1
                                                                   ,arGrdBkWeights t2
                                                             WHERE  t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId
                                                                    AND t1.ClsSectionId = @ClsSectionId
                                                             ) X
                                                            ,arGrdBkWgtDetails C
                                                            ,arGrdComponentTypes D
                                                            ,arGrdBkResults GR
                                                    WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                             AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                             AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                             AND
                                                        -- D.SysComponentTypeID not in (500,503) and   
                                                        GR.StuEnrollId = @StuEnrollId
                                                             AND GR.ClsSectionId = @ClsSectionId
                                                             AND GR.Score IS NOT NULL
                                                    GROUP BY C.InstrGrdBkWgtDetailId
                                                            ,D.Code
                                                            ,D.Descrip
                                                            ,C.Weight
                                                            ,C.Number
                                                            ,C.GrdPolicyId
                                                            ,C.Parameter
                                                            ,X.GrdScaleId
                                                    ) S
                                           GROUP BY InstrGrdBkWgtDetailId
                                                   ,Code
                                                   ,Descrip
                                                   ,Weight
                                                   ,NumberOfComponents
                                           ) FinalTblToComputeCurrentScore
                                    );

            END;

        IF ( @CurrentScore IS NOT NULL )
            BEGIN
                SET @CurrentGrade = (
                                    SELECT t2.Grade
                                    FROM   arGradeScaleDetails t1
                                          ,arGradeSystemDetails t2
                                    WHERE  t1.GrdSysDetailId = t2.GrdSysDetailId
                                           AND t1.GrdScaleId IN (
                                                                SELECT GrdScaleId
                                                                FROM   arClassSections
                                                                WHERE  ClsSectionId = @ClsSectionId
                                                                )
                                           AND @CurrentScore >= t1.MinVal
                                           AND @CurrentScore <= t1.MaxVal
                                    );

            END;
        ELSE
            BEGIN
                SET @CurrentGrade = NULL;
            END;

        IF (
           @CurrentScore IS NULL
           AND @CurrentGrade IS NULL
           AND @FinalScore IS NULL
           AND @FinalGrade IS NULL
           )
            BEGIN
                SET @Completed = 0;
                SET @CreditsAttempted = 0;
                SET @CreditsEarned = 0;
                SET @FinAidCreditsEarned = 0;
            END;

        IF (
           @FinalScore IS NOT NULL
           OR @FinalGrade IS NOT NULL
           )
            BEGIN

                SET @FinalGradeDesc = (
                                      SELECT Grade
                                      FROM   arGradeSystemDetails
                                      WHERE  GrdSysDetailId = @FinalGrade
                                      );



                IF ( @FinalGradeDesc IS NULL )
                    BEGIN
                        SET @FinalGradeDesc = (
                                              SELECT t2.Grade
                                              FROM   arGradeScaleDetails t1
                                                    ,arGradeSystemDetails t2
                                              WHERE  t1.GrdSysDetailId = t2.GrdSysDetailId
                                                     AND t1.GrdScaleId IN (
                                                                          SELECT GrdScaleId
                                                                          FROM   arClassSections
                                                                          WHERE  ClsSectionId = @ClsSectionId
                                                                          )
                                                     AND @FinalScore >= t1.MinVal
                                                     AND @FinalScore <= t1.MaxVal
                                              );
                    END;
                SET @FinalGPA = (
                                SELECT GPA
                                FROM   arGradeSystemDetails
                                WHERE  GrdSysDetailId = @FinalGrade
                                );
                IF @FinalGPA IS NULL
                    BEGIN
                        SET @FinalGPA = (
                                        SELECT t2.GPA
                                        FROM   arGradeScaleDetails t1
                                              ,arGradeSystemDetails t2
                                        WHERE  t1.GrdSysDetailId = t2.GrdSysDetailId
                                               AND t1.GrdScaleId IN (
                                                                    SELECT GrdScaleId
                                                                    FROM   arClassSections
                                                                    WHERE  ClsSectionId = @ClsSectionId
                                                                    )
                                               AND @FinalScore >= t1.MinVal
                                               AND @FinalScore <= t1.MaxVal
                                        );
                    END;
            END;
        ELSE
            BEGIN
                SET @FinalGradeDesc = NULL;
                SET @FinalGPA = NULL;
            END;

        SET @isGradeEligibleForCreditsEarned = (
                                               SELECT t2.IsCreditsEarned
                                               FROM   arGradeScaleDetails t1
                                                     ,arGradeSystemDetails t2
                                               WHERE  t1.GrdSysDetailId = t2.GrdSysDetailId
                                                      AND t1.GrdScaleId IN (
                                                                           SELECT GrdScaleId
                                                                           FROM   arClassSections
                                                                           WHERE  ClsSectionId = @ClsSectionId
                                                                           )
                                                      AND t2.Grade = @FinalGradeDesc
                                               );

        SET @isGradeEligibleForCreditsAttempted = (
                                                  SELECT t2.IsCreditsAttempted
                                                  FROM   arGradeScaleDetails t1
                                                        ,arGradeSystemDetails t2
                                                  WHERE  t1.GrdSysDetailId = t2.GrdSysDetailId
                                                         AND t1.GrdScaleId IN (
                                                                              SELECT GrdScaleId
                                                                              FROM   arClassSections
                                                                              WHERE  ClsSectionId = @ClsSectionId
                                                                              )
                                                         AND t2.Grade = @FinalGradeDesc
                                                  );

        IF ( @isGradeEligibleForCreditsEarned IS NULL )
            BEGIN
                SET @isGradeEligibleForCreditsEarned = (
                                                       SELECT TOP 1 t2.IsCreditsEarned
                                                       FROM   arGradeSystemDetails t2
                                                       WHERE  t2.Grade = @FinalGradeDesc
                                                       );
            END;

        IF ( @isGradeEligibleForCreditsAttempted IS NULL )
            BEGIN
                SET @isGradeEligibleForCreditsAttempted = (
                                                          SELECT TOP 1 t2.IsCreditsAttempted
                                                          FROM   arGradeSystemDetails t2
                                                          WHERE  t2.Grade = @FinalGradeDesc
                                                          );
            END;

        IF @isGradeEligibleForCreditsEarned = 0
            BEGIN
                SET @CreditsEarned = 0;
                SET @FinAidCreditsEarned = 0;
            END;
        IF @isGradeEligibleForCreditsAttempted = 0
            BEGIN
                SET @CreditsAttempted = 0;
                SET @FinAidCreditsEarned = 0;
            END;

        IF ( @IsPass = 0 )
            BEGIN
                SET @CreditsEarned = 0;
                SET @FinAidCreditsEarned = 0;
            END;

        --For Letter Grade Schools if the score is null but final grade was posted then the   
        --Final grade will be the current grade  
        IF @CurrentGrade IS NULL
           AND @FinalGradeDesc IS NOT NULL
            BEGIN
                SET @CurrentGrade = @FinalGradeDesc;
            END;

        IF (
           @sysComponentTypeId = 503
           OR @sysComponentTypeId = 500
           ) -- Lab work or Lab Hours  
            BEGIN
                -- This course has lab work and lab hours  
                IF ( @Completed = 0 )
                    BEGIN
                        SET @CreditsPerService = (
                                                 SELECT TOP 1 GD.CreditsPerService
                                                 FROM   arGrdBkWeights GBW
                                                       ,arGrdComponentTypes GC
                                                       ,arGrdBkWgtDetails GD
                                                 WHERE  GBW.InstrGrdBkWgtId = GD.InstrGrdBkWgtId
                                                        AND GC.GrdComponentTypeId = GD.GrdComponentTypeId
                                                        AND GBW.ReqId = @reqid
                                                        AND GC.SysComponentTypeId IN ( 500, 503 )
                                                 );
                        SET @NumberOfServicesAttempted = (
                                                         SELECT TOP 1 GBR.Score AS NumberOfServicesAttempted
                                                         FROM   arStuEnrollments SE
                                                         INNER JOIN arGrdBkResults GBR ON SE.StuEnrollId = GBR.StuEnrollId
                                                                                          AND GBR.ClsSectionId = @ClsSectionId
                                                         );

                        SET @CreditsEarned = ISNULL(@CreditsPerService, 0) * ISNULL(@NumberOfServicesAttempted, 0);
                    END;
            END;

        DECLARE @rowAlreadyInserted INT;
        SET @rowAlreadyInserted = 0;

        -- Get the final Gpa only when IsCreditsAttempted is set to 1 and IsInGPA is set to 1  
        IF @IsInGPA = 1
            BEGIN
                IF ( @IsCreditsAttempted = 0 )
                    BEGIN
                        SET @FinalGPA = NULL;
                    END;
            END;
        ELSE
            BEGIN
                SET @FinalGPA = NULL;
            END;

        IF @FinalScore IS NOT NULL
            BEGIN
                SET @CurrentScore = @FinalScore;
            END;

        -- Rally case DE 738 KeyBoarding Courses  
        SET @IsWeighted = (
                          SELECT COUNT(*) AS WeightsCount
                          FROM   (
                                 SELECT T.TermId
                                       ,T.TermDescrip
                                       ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                       ,R.ReqId
                                       ,RTRIM(GCT.Descrip) AS GradeBookDescription
                                       ,( CASE WHEN GCT.SysComponentTypeId IN ( 500, 503, 504, 544 ) THEN GBWD.Number
                                               ELSE (
                                                    SELECT MIN(MinVal)
                                                    FROM   arGradeScaleDetails GSD
                                                          ,arGradeSystemDetails GSS
                                                    WHERE  GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                           AND GSS.IsPass = 1
                                                    )
                                          END
                                        ) AS MinimumScore
                                       --GBR.Score as Score,    
                                       ,GBWD.Weight AS Weight
                                       ,RES.Score AS FinalScore
                                       ,RES.GrdSysDetailId AS FinalGrade
                                       ,GBWD.Required
                                       ,GBWD.MustPass
                                       ,GBWD.GrdPolicyId
                                       ,( CASE GCT.SysComponentTypeId
                                               WHEN 544 THEN (
                                                             SELECT SUM(HoursAttended)
                                                             FROM   arExternshipAttendance
                                                             WHERE  StuEnrollId = SE.StuEnrollId
                                                             )
                                               WHEN 503 THEN (
                                                             SELECT SUM(Score)
                                                             FROM   arGrdBkResults
                                                             WHERE  StuEnrollId = SE.StuEnrollId
                                                                    AND ClsSectionId = CS.ClsSectionId
                                                                    AND InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                                             )
                                               ELSE GBR.Score
                                          END
                                        ) AS GradeBookResult
                                       ,GCT.SysComponentTypeId
                                       ,SE.StuEnrollId
                                       --GBR.GrdBkResultId,  
                                       ,R.Credits AS CreditsAttempted
                                       ,CS.ClsSectionId
                                       ,GSD.Grade
                                       ,GSD.IsPass
                                       ,GSD.IsCreditsAttempted
                                       ,GSD.IsCreditsEarned
                                 FROM   arStuEnrollments SE
                                 INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                 INNER JOIN arTransferGrades RES ON RES.StuEnrollId = SE.StuEnrollId
                                 --inner join Inserted t4 on RES.TransferId = t4.TransferId  
                                 INNER JOIN arClassSections CS ON RES.ReqId = CS.ReqId
                                                                  AND RES.TermId = CS.TermId
                                 INNER JOIN arTerm T ON CS.TermId = T.TermId
                                 INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                                 LEFT JOIN arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                                 LEFT JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                 LEFT JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                 LEFT JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                                 WHERE  SE.StuEnrollId = @StuEnrollId
                                        AND T.TermId = @TermId
                                        AND R.ReqId = @reqid
                                 --and GBR.ResNum>=1  
                                 ) D
                          WHERE  Weight >= 1
                          );

        -- Get the number of components that have scores  
        DECLARE @CountComponentsThatHasScores INT;
        SET @CountComponentsThatHasScores = (
                                            SELECT COUNT(*)
                                            FROM   arGrdBkResults
                                            WHERE  StuEnrollId = @StuEnrollId
                                                   AND ClsSectionId IN (
                                                                       SELECT DISTINCT ClsSectionId
                                                                       FROM   arClassSections
                                                                       WHERE  TermId = @TermId
                                                                              AND ReqId = @reqid
                                                                       )
                                                   AND Score IS NOT NULL
                                            );


        DECLARE @CourseComponentsThatNeedsToBeScored INT;
        DECLARE @clsStartDate DATETIME;
        SET @clsStartDate = (
                            SELECT TOP 1 StartDate
                            FROM   arClassSections
                            WHERE  TermId = @TermId
                                   AND ReqId = @reqid
                            );
        IF LOWER(@SetGradeBookAt) = 'instructorlevel'
            BEGIN
                SET @CourseComponentsThatNeedsToBeScored = (
                                                           SELECT COUNT(*)
                                                           FROM   (
                                                                  SELECT 4 AS Tag
                                                                        ,3 AS Parent
                                                                        ,PV.PrgVerId
                                                                        ,PV.PrgVerDescrip
                                                                        ,NULL AS ProgramCredits
                                                                        ,T.TermId
                                                                        ,T.TermDescrip AS TermDescription
                                                                        ,T.StartDate AS TermStartDate
                                                                        ,T.EndDate AS TermEndDate
                                                                        ,R.ReqId AS CourseId
                                                                        ,R.Descrip AS CourseDescription
                                                                        ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                                                        ,NULL AS CourseCredits
                                                                        ,NULL AS CourseFinAidCredits
                                                                        ,NULL AS CoursePassingGrade
                                                                        ,NULL AS CourseScore
                                                                        ,(
                                                                         SELECT TOP 1 GrdBkResultId
                                                                         FROM   arGrdBkResults
                                                                         WHERE  StuEnrollId = SE.StuEnrollId
                                                                                AND InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                                                                AND ClsSectionId = CS.ClsSectionId
                                                                         ) AS GrdBkResultId
                                                                        ,CASE WHEN LOWER(@SetGradeBookAt) = 'instructorlevel' THEN RTRIM(GBWD.Descrip)
                                                                              ELSE GCT.Descrip
                                                                         END AS GradeBookDescription
                                                                        ,( CASE GCT.SysComponentTypeId
                                                                                WHEN 544 THEN (
                                                                                              SELECT SUM(HoursAttended)
                                                                                              FROM   arExternshipAttendance
                                                                                              WHERE  StuEnrollId = SE.StuEnrollId
                                                                                              )
                                                                                ELSE (
                                                                                     SELECT   TOP 1 Score
                                                                                     FROM     arGrdBkResults
                                                                                     WHERE    StuEnrollId = SE.StuEnrollId
                                                                                              AND InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                                                                              AND ClsSectionId = CS.ClsSectionId
                                                                                     ORDER BY ModDate DESC
                                                                                     )
                                                                           END
                                                                         ) AS GradeBookScore
                                                                        ,NULL AS GradeBookPostDate
                                                                        ,NULL AS GradeBookPassingGrade
                                                                        ,NULL AS GradeBookWeight
                                                                        ,NULL AS GradeBookRequired
                                                                        ,NULL AS GradeBookMustPass
                                                                        ,GCT.SysComponentTypeId AS GradeBookSysComponentTypeId
                                                                        ,NULL AS GradeBookHoursRequired
                                                                        ,NULL AS GradeBookHoursCompleted
                                                                        ,SE.StuEnrollId
                                                                        ,( CASE WHEN GCT.SysComponentTypeId IN ( 500, 503, 504, 544 ) THEN GBWD.Number
                                                                                ELSE (
                                                                                     SELECT MIN(MinVal)
                                                                                     FROM   arGradeScaleDetails GSD
                                                                                           ,arGradeSystemDetails GSS
                                                                                     WHERE  GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                                            AND GSS.IsPass = 1
                                                                                            AND GSD.GrdScaleId = CS.GrdScaleId
                                                                                     )
                                                                           END
                                                                         ) AS MinResult
                                                                        ,SYRES.Resource AS GradeComponentDescription
                                                                        ,NULL AS CreditsAttempted
                                                                        ,NULL AS CreditsEarned
                                                                        ,NULL AS Completed
                                                                        ,NULL AS CurrentScore
                                                                        ,NULL AS CurrentGrade
                                                                        ,GCT.SysComponentTypeId AS FinalScore
                                                                        ,NULL AS FinalGrade
                                                                        ,NULL AS WeightedAverage_GPA
                                                                        ,NULL AS SimpleAverage_GPA
                                                                        ,NULL AS WeightedAverage_CumGPA
                                                                        ,NULL AS SimpleAverage_CumGPA
                                                                        ,C.CampusId
                                                                        ,C.CampDescrip
                                                                        ,ROW_NUMBER() OVER ( PARTITION BY SE.StuEnrollId
                                                                                                         ,R.ReqId
                                                                                             ORDER BY C.CampDescrip
                                                                                                     ,PV.PrgVerDescrip
                                                                                                     ,T.StartDate
                                                                                                     ,T.EndDate
                                                                                                     ,T.TermId
                                                                                                     ,T.TermDescrip
                                                                                                     ,R.ReqId
                                                                                                     ,R.Descrip
                                                                                                     ,GCT.SysComponentTypeId
                                                                                                     ,GCT.Descrip
                                                                                           ) AS rownumber
                                                                        ,S.FirstName AS FirstName
                                                                        ,S.LastName AS LastName
                                                                        ,S.MiddleName
                                                                        ,SYRES.ResourceID
                                                                  FROM -- MOdified by Balaji  
                                                                         arStuEnrollments SE
                                                                  INNER JOIN (
                                                                             SELECT StudentId
                                                                                   ,FirstName
                                                                                   ,LastName
                                                                                   ,MiddleName
                                                                             FROM   arStudent
                                                                             ) S ON S.StudentId = SE.StudentId
                                                                  INNER JOIN arResults RES ON RES.StuEnrollId = SE.StuEnrollId -- RES.TestId = CS.ClsSectionId -- and RES.StuEnrollId = GBR.StuEnrollId  
                                                                  INNER JOIN arClassSections CS ON CS.ClsSectionId = RES.TestId
                                                                  INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                                                  INNER JOIN arTerm T ON CS.TermId = T.TermId
                                                                  INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                                                                  LEFT OUTER JOIN arGrdBkWgtDetails GBWD ON GBWD.InstrGrdBkWgtId = CS.InstrGrdBkWgtId --.InstrGrdBkWgtDetailId = CS.InstrGrdBkWgtDetailId  
                                                                  INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                                  INNER JOIN (
                                                                             SELECT Resource
                                                                                   ,ResourceID
                                                                             FROM   syResources
                                                                             WHERE  ResourceTypeID = 10
                                                                             ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
                                                                  INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                                                                  WHERE  SE.StuEnrollId = @StuEnrollId
                                                                         AND T.TermId = @TermId
                                                                         AND R.ReqId = @reqid
                                                                         AND (
                                                                             @sysComponentTypeId IS NULL
                                                                             OR GCT.SysComponentTypeId IN (
                                                                                                          SELECT Val
                                                                                                          FROM   MultipleValuesForReportParameters(
                                                                                                                                                      @sysComponentTypeId
                                                                                                                                                     ,','
                                                                                                                                                     ,1
                                                                                                                                                  )
                                                                                                          )
                                                                             )
                                                                  UNION
                                                                  SELECT 4 AS Tag
                                                                        ,3
                                                                        ,PV.PrgVerId
                                                                        ,PV.PrgVerDescrip
                                                                        ,NULL
                                                                        ,T.TermId
                                                                        ,T.TermDescrip
                                                                        ,T.StartDate AS termStartdate
                                                                        ,T.EndDate AS TermEndDate
                                                                        ,GBCR.ReqId
                                                                        ,R.Descrip AS CourseDescrip
                                                                        ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                                                        ,NULL
                                                                        ,NULL
                                                                        ,NULL
                                                                        ,NULL
                                                                        ,GBCR.ConversionResultId AS GrdBkResultId
                                                                        ,CASE WHEN LOWER(@SetGradeBookAt) = 'instructorlevel' THEN RTRIM(GBWD.Descrip)
                                                                              ELSE GCT.Descrip
                                                                         END AS GradeBookDescription
                                                                        ,GBCR.Score AS GradeBookResult
                                                                        ,NULL
                                                                        ,NULL
                                                                        ,NULL
                                                                        ,NULL
                                                                        ,NULL
                                                                        ,GCT.SysComponentTypeId
                                                                        ,NULL
                                                                        ,NULL
                                                                        ,SE.StuEnrollId
                                                                        ,GBCR.MinResult
                                                                        ,SYRES.Resource -- Student data    
                                                                        ,NULL AS CreditsAttempted
                                                                        ,NULL AS CreditsEarned
                                                                        ,NULL AS Completed
                                                                        ,NULL AS CurrentScore
                                                                        ,NULL AS CurrentGrade
                                                                        ,NULL AS FinalScore
                                                                        ,NULL AS FinalGrade
                                                                        ,NULL AS WeightedAverage_GPA
                                                                        ,NULL AS SimpleAverage_GPA
                                                                        ,NULL
                                                                        ,NULL
                                                                        ,C.CampusId
                                                                        ,C.CampDescrip
                                                                        ,ROW_NUMBER() OVER ( PARTITION BY SE.StuEnrollId
                                                                                                         ,R.ReqId
                                                                                             ORDER BY C.CampDescrip
                                                                                                     ,PV.PrgVerDescrip
                                                                                                     ,T.StartDate
                                                                                                     ,T.EndDate
                                                                                                     ,T.TermId
                                                                                                     ,T.TermDescrip
                                                                                                     ,R.ReqId
                                                                                                     ,R.Descrip
                                                                                                     ,GCT.SysComponentTypeId
                                                                                                     ,GCT.Descrip
                                                                                           ) AS rownumber
                                                                        ,S.FirstName AS FirstName
                                                                        ,S.LastName AS LastName
                                                                        ,S.MiddleName
                                                                        ,SYRES.ResourceID
                                                                  FROM   arGrdBkConversionResults GBCR
                                                                  INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                                                                  INNER JOIN (
                                                                             SELECT StudentId
                                                                                   ,FirstName
                                                                                   ,LastName
                                                                                   ,MiddleName
                                                                             FROM   arStudent
                                                                             ) S ON S.StudentId = SE.StudentId
                                                                  INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                                                  INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                                                                  INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                                                                  INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                                                  INNER JOIN arGrdBkWgtDetails GBWD ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                                                                       AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                                                  INNER JOIN arGrdBkWeights GBW ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                                                                                                   AND GBCR.ReqId = GBW.ReqId
                                                                  INNER JOIN (
                                                                             SELECT   ReqId
                                                                                     ,MAX(EffectiveDate) AS EffectiveDate
                                                                             FROM     arGrdBkWeights
                                                                             GROUP BY ReqId
                                                                             ) AS MaxEffectiveDatesByCourse ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
                                                                  INNER JOIN (
                                                                             SELECT Resource
                                                                                   ,ResourceID
                                                                             FROM   syResources
                                                                             WHERE  ResourceTypeID = 10
                                                                             ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
                                                                  INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                                                                  WHERE  MaxEffectiveDatesByCourse.EffectiveDate <= T.StartDate
                                                                         AND SE.StuEnrollId = @StuEnrollId
                                                                         AND T.TermId = @TermId
                                                                         AND R.ReqId = @reqid
                                                                         AND (
                                                                             @sysComponentTypeId IS NULL
                                                                             OR GCT.SysComponentTypeId IN (
                                                                                                          SELECT Val
                                                                                                          FROM   MultipleValuesForReportParameters(
                                                                                                                                                      @sysComponentTypeId
                                                                                                                                                     ,','
                                                                                                                                                     ,1
                                                                                                                                                  )
                                                                                                          )
                                                                             )
                                                                  ) dt
                                                           );
            END;
        ELSE
            BEGIN
                SET @CourseComponentsThatNeedsToBeScored = (
                                                           SELECT COUNT(*)
                                                           FROM   (
                                                                  SELECT DISTINCT GradeBookDescription
                                                                        ,GradeBookScore
                                                                        ,MinResult
                                                                        ,GradeBookSysComponentTypeId
                                                                        ,GradeComponentDescription
                                                                        ,CampDescrip
                                                                        ,FirstName
                                                                        ,LastName
                                                                        ,MiddleName
                                                                        ,GrdBkResultId
                                                                        ,TermStartDate
                                                                        ,TermEndDate
                                                                        ,TermDescription
                                                                        ,CourseDescription
                                                                        ,PrgVerDescrip
                                                                        ,StuEnrollId
                                                                        ,CourseId
                                                                        ,ResourceID
                                                                        ,Required
                                                                  FROM   (
                                                                         SELECT DISTINCT 4 AS Tag
                                                                               ,3 AS Parent
                                                                               ,PV.PrgVerId
                                                                               ,PV.PrgVerDescrip
                                                                               ,NULL AS ProgramCredits
                                                                               ,T.TermId
                                                                               ,T.TermDescrip AS TermDescription
                                                                               ,T.StartDate AS TermStartDate
                                                                               ,T.EndDate AS TermEndDate
                                                                               ,R.ReqId AS CourseId
                                                                               ,R.Descrip AS CourseDescription
                                                                               ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                                                               ,NULL AS CourseCredits
                                                                               ,NULL AS CourseFinAidCredits
                                                                               ,NULL AS CoursePassingGrade
                                                                               ,NULL AS CourseScore
                                                                               ,(
                                                                                SELECT TOP 1 GrdBkResultId
                                                                                FROM   arGrdBkResults
                                                                                WHERE  StuEnrollId = SE.StuEnrollId
                                                                                       AND InstrGrdBkWgtDetailId = b.InstrGrdBkWgtDetailId
                                                                                       AND ClsSectionId = CS.ClsSectionId
                                                                                ) AS GrdBkResultId
                                                                               ,RTRIM(GCT.Descrip) + ( CASE WHEN a.ResNum IN ( 0, 1 ) THEN ''
                                                                                                            ELSE CAST(a.ResNum AS CHAR)
                                                                                                       END
                                                                                                     ) AS GradeBookDescription
                                                                               ,( CASE GCT.SysComponentTypeId
                                                                                       WHEN 544 THEN (
                                                                                                     SELECT SUM(HoursAttended)
                                                                                                     FROM   arExternshipAttendance
                                                                                                     WHERE  StuEnrollId = SE.StuEnrollId
                                                                                                     )
                                                                                       ELSE
                                                                                           --        (select Top 1 Score from arGrdBkResults where StuEnrollId=SE.StuEnrollId and   
                                                                                           --        InstrGrdBkWgtDetailId=b.InstrGrdBkWgtDetailId and  
                                                                                           --        ClsSectionId=CS.ClsSectionId order by moddate desc)   
                                                                                           a.Score
                                                                                  END
                                                                                ) AS GradeBookScore
                                                                               ,NULL AS GradeBookPostDate
                                                                               ,NULL AS GradeBookPassingGrade
                                                                               ,NULL AS GradeBookWeight
                                                                               ,NULL AS GradeBookRequired
                                                                               ,NULL AS GradeBookMustPass
                                                                               ,GCT.SysComponentTypeId AS GradeBookSysComponentTypeId
                                                                               ,NULL AS GradeBookHoursRequired
                                                                               ,NULL AS GradeBookHoursCompleted
                                                                               ,SE.StuEnrollId
                                                                               ,( CASE WHEN GCT.SysComponentTypeId IN ( 500, 503, 504, 544 ) THEN b.Number
                                                                                       ELSE (
                                                                                            SELECT MIN(MinVal)
                                                                                            FROM   arGradeScaleDetails GSD
                                                                                                  ,arGradeSystemDetails GSS
                                                                                            WHERE  GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                                                   AND GSS.IsPass = 1
                                                                                                   AND GSD.GrdScaleId = CS.GrdScaleId
                                                                                            )
                                                                                  END
                                                                                ) AS MinResult
                                                                               ,SYRES.Resource AS GradeComponentDescription
                                                                               ,NULL AS CreditsAttempted
                                                                               ,NULL AS CreditsEarned
                                                                               ,NULL AS Completed
                                                                               ,NULL AS CurrentScore
                                                                               ,NULL AS CurrentGrade
                                                                               ,GCT.SysComponentTypeId AS FinalScore
                                                                               ,NULL AS FinalGrade
                                                                               ,NULL AS WeightedAverage_GPA
                                                                               ,NULL AS SimpleAverage_GPA
                                                                               ,NULL AS WeightedAverage_CumGPA
                                                                               ,NULL AS SimpleAverage_CumGPA
                                                                               ,C1.CampusId
                                                                               ,C1.CampDescrip
                                                                               --ROW_NUMBER() OVER (partition by SE.StuEnrollId,R.ReqId Order By C1.CampDescrip,PV.PrgVerDescrip,T.StartDate,T.EndDate,T.TermId,T.TermDescrip,R.ReqId,R.Descrip,GCT.SysComponentTypeId,GCT.Descrip) as rownumber,  
                                                                               ,c.FirstName AS FirstName
                                                                               ,c.LastName AS LastName
                                                                               ,c.MiddleName
                                                                               ,SYRES.ResourceID
                                                                               ,b.Required
                                                                         FROM   arGrdBkResults a
                                                                               ,arGrdBkWgtDetails b
                                                                               ,arStudent c
                                                                               ,arStuEnrollments SE
                                                                               ,arResults e
                                                                               ,arClassSections CS
                                                                               ,arReqs R
                                                                               ,arTerm T
                                                                               ,arGrdComponentTypes GCT
                                                                               , (
                                                                                 SELECT Resource
                                                                                       ,ResourceID
                                                                                 FROM   syResources
                                                                                 WHERE  ResourceTypeID = 10
                                                                                 ) SYRES
                                                                               ,syCampuses C1
                                                                               ,arPrgVersions PV
                                                                         WHERE  a.InstrGrdBkWgtDetailId = b.InstrGrdBkWgtDetailId
                                                                                AND a.StuEnrollId = @StuEnrollId
                                                                                AND a.StuEnrollId = SE.StuEnrollId
                                                                                AND SE.StudentId = c.StudentId
                                                                                AND a.StuEnrollId = e.StuEnrollId
                                                                                AND a.ClsSectionId = e.TestId
                                                                                AND e.TestId = CS.ClsSectionId
                                                                                AND CS.ReqId = R.ReqId
                                                                                AND CS.TermId = T.TermId
                                                                                AND GCT.GrdComponentTypeId = b.GrdComponentTypeId
                                                                                AND SYRES.ResourceID = GCT.SysComponentTypeId
                                                                                AND SE.CampusId = C1.CampusId
                                                                                AND SE.PrgVerId = PV.PrgVerId
                                                                                AND R.ReqId = @reqid
                                                                                AND T.TermId = @TermId
                                                                                --and (@SysComponentTypeId is null or GCT.SysComponentTypeId in (Select Val from [MultipleValuesForReportParameters](@SysComponentTypeId,',',1)))  
                                                                                AND a.ResNum >= 1
                                                                         UNION
                                                                         SELECT DISTINCT 4 AS Tag
                                                                               ,3 AS Parent
                                                                               ,PV.PrgVerId
                                                                               ,PV.PrgVerDescrip
                                                                               ,NULL AS ProgramCredits
                                                                               ,T.TermId
                                                                               ,T.TermDescrip AS TermDescription
                                                                               ,T.StartDate AS TermStartDate
                                                                               ,T.EndDate AS TermEndDate
                                                                               ,R.ReqId AS CourseId
                                                                               ,R.Descrip AS CourseDescription
                                                                               ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                                                               ,NULL AS CourseCredits
                                                                               ,NULL AS CourseFinAidCredits
                                                                               ,NULL AS CoursePassingGrade
                                                                               ,NULL AS CourseScore
                                                                               ,(
                                                                                SELECT TOP 1 GrdBkResultId
                                                                                FROM   arGrdBkResults
                                                                                WHERE  StuEnrollId = SE.StuEnrollId
                                                                                       AND InstrGrdBkWgtDetailId = b.InstrGrdBkWgtDetailId
                                                                                       AND ClsSectionId = CS.ClsSectionId
                                                                                ) AS GrdBkResultId
                                                                               ,RTRIM(GCT.Descrip) + ( CASE WHEN a.ResNum IN ( 0, 1 ) THEN ''
                                                                                                            ELSE CAST(a.ResNum AS CHAR)
                                                                                                       END
                                                                                                     ) AS GradeBookDescription
                                                                               ,( CASE GCT.SysComponentTypeId
                                                                                       WHEN 544 THEN (
                                                                                                     SELECT SUM(HoursAttended)
                                                                                                     FROM   arExternshipAttendance
                                                                                                     WHERE  StuEnrollId = SE.StuEnrollId
                                                                                                     )
                                                                                       ELSE
                                                                                           --        (select Top 1 Score from arGrdBkResults where StuEnrollId=SE.StuEnrollId and   
                                                                                           --        InstrGrdBkWgtDetailId=b.InstrGrdBkWgtDetailId and  
                                                                                           --        ClsSectionId=CS.ClsSectionId order by moddate desc)   
                                                                                           a.Score
                                                                                  END
                                                                                ) AS GradeBookScore
                                                                               ,NULL AS GradeBookPostDate
                                                                               ,NULL AS GradeBookPassingGrade
                                                                               ,NULL AS GradeBookWeight
                                                                               ,NULL AS GradeBookRequired
                                                                               ,NULL AS GradeBookMustPass
                                                                               ,GCT.SysComponentTypeId AS GradeBookSysComponentTypeId
                                                                               ,NULL AS GradeBookHoursRequired
                                                                               ,NULL AS GradeBookHoursCompleted
                                                                               ,SE.StuEnrollId
                                                                               ,( CASE WHEN GCT.SysComponentTypeId IN ( 500, 503, 504, 544 ) THEN b.Number
                                                                                       ELSE (
                                                                                            SELECT MIN(MinVal)
                                                                                            FROM   arGradeScaleDetails GSD
                                                                                                  ,arGradeSystemDetails GSS
                                                                                            WHERE  GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                                                   AND GSS.IsPass = 1
                                                                                                   AND GSD.GrdScaleId = CS.GrdScaleId
                                                                                            )
                                                                                  END
                                                                                ) AS MinResult
                                                                               ,SYRES.Resource AS GradeComponentDescription
                                                                               ,NULL AS CreditsAttempted
                                                                               ,NULL AS CreditsEarned
                                                                               ,NULL AS Completed
                                                                               ,NULL AS CurrentScore
                                                                               ,NULL AS CurrentGrade
                                                                               ,GCT.SysComponentTypeId AS FinalScore
                                                                               ,NULL AS FinalGrade
                                                                               ,NULL AS WeightedAverage_GPA
                                                                               ,NULL AS SimpleAverage_GPA
                                                                               ,NULL AS WeightedAverage_CumGPA
                                                                               ,NULL AS SimpleAverage_CumGPA
                                                                               ,C1.CampusId
                                                                               ,C1.CampDescrip
                                                                               --ROW_NUMBER() OVER (partition by SE.StuEnrollId,R.ReqId Order By C1.CampDescrip,PV.PrgVerDescrip,T.StartDate,T.EndDate,T.TermId,T.TermDescrip,R.ReqId,R.Descrip,GCT.SysComponentTypeId,GCT.Descrip) as rownumber,  
                                                                               ,c.FirstName AS FirstName
                                                                               ,c.LastName AS LastName
                                                                               ,c.MiddleName
                                                                               ,SYRES.ResourceID
                                                                               ,b.Required
                                                                         FROM   arGrdBkResults a
                                                                               ,arGrdBkWgtDetails b
                                                                               ,arStudent c
                                                                               ,arStuEnrollments SE
                                                                               ,arResults e
                                                                               ,arClassSections CS
                                                                               ,arReqs R
                                                                               ,arTerm T
                                                                               ,arGrdComponentTypes GCT
                                                                               , (
                                                                                 SELECT Resource
                                                                                       ,ResourceID
                                                                                 FROM   syResources
                                                                                 WHERE  ResourceTypeID = 10
                                                                                 ) SYRES
                                                                               ,syCampuses C1
                                                                               ,arPrgVersions PV
                                                                         WHERE  a.InstrGrdBkWgtDetailId = b.InstrGrdBkWgtDetailId
                                                                                AND a.StuEnrollId = @StuEnrollId
                                                                                AND a.StuEnrollId = SE.StuEnrollId
                                                                                AND SE.StudentId = c.StudentId
                                                                                AND a.StuEnrollId = e.StuEnrollId
                                                                                AND a.ClsSectionId = e.TestId
                                                                                AND e.TestId = CS.ClsSectionId
                                                                                AND CS.ReqId = R.ReqId
                                                                                AND CS.TermId = T.TermId
                                                                                AND GCT.GrdComponentTypeId = b.GrdComponentTypeId
                                                                                AND SYRES.ResourceID = GCT.SysComponentTypeId
                                                                                AND SE.CampusId = C1.CampusId
                                                                                AND SE.PrgVerId = PV.PrgVerId
                                                                                --and a.ClsSectionId = 'CD64FA81-7E91-4E22-A323-34E7B5695E2E'   
                                                                                AND R.ReqId = @reqid
                                                                                AND T.TermId = @TermId
                                                                                --and (@SysComponentTypeId is null or GCT.SysComponentTypeId in (Select Val from [MultipleValuesForReportParameters](@SysComponentTypeId,',',1)))  
                                                                                AND a.ResNum = 0
                                                                                AND a.GrdBkResultId NOT IN (
                                                                                                           SELECT DISTINCT GrdBkResultId
                                                                                                           FROM   arGrdBkResults a
                                                                                                                 ,arGrdBkWgtDetails b
                                                                                                                 ,arStudent c
                                                                                                                 ,arStuEnrollments SE
                                                                                                                 ,arResults e
                                                                                                                 ,arClassSections CS
                                                                                                                 ,arReqs R
                                                                                                                 ,arTerm T
                                                                                                                 ,arGrdComponentTypes GCT
                                                                                                                 ,(
                                                                                                                  SELECT Resource
                                                                                                                        ,ResourceID
                                                                                                                  FROM   syResources
                                                                                                                  WHERE  ResourceTypeID = 10
                                                                                                                  ) SYRES
                                                                                                                 ,syCampuses C1
                                                                                                                 ,arPrgVersions PV
                                                                                                           WHERE  a.InstrGrdBkWgtDetailId = b.InstrGrdBkWgtDetailId
                                                                                                                  AND a.StuEnrollId = @StuEnrollId
                                                                                                                  AND a.StuEnrollId = SE.StuEnrollId
                                                                                                                  AND SE.StudentId = c.StudentId
                                                                                                                  AND a.StuEnrollId = e.StuEnrollId
                                                                                                                  AND a.ClsSectionId = e.TestId
                                                                                                                  AND e.TestId = CS.ClsSectionId
                                                                                                                  AND CS.ReqId = R.ReqId
                                                                                                                  AND CS.TermId = T.TermId
                                                                                                                  AND GCT.GrdComponentTypeId = b.GrdComponentTypeId
                                                                                                                  AND SYRES.ResourceID = GCT.SysComponentTypeId
                                                                                                                  AND SE.CampusId = C1.CampusId
                                                                                                                  AND SE.PrgVerId = PV.PrgVerId
                                                                                                                  AND R.ReqId = @reqid
                                                                                                                  AND T.TermId = @TermId
                                                                                                                  AND (
                                                                                                                      @sysComponentTypeId IS NULL
                                                                                                                      OR GCT.SysComponentTypeId IN (
                                                                                                                                                   SELECT Val
                                                                                                                                                   FROM   MultipleValuesForReportParameters(
                                                                                                                                                                                               @sysComponentTypeId
                                                                                                                                                                                              ,','
                                                                                                                                                                                              ,1
                                                                                                                                                                                           )
                                                                                                                                                   )
                                                                                                                      )
                                                                                                                  AND a.ResNum >= 1
                                                                                                           )
                                                                         UNION
                                                                         SELECT *
                                                                         FROM   (
                                                                                SELECT DISTINCT 4 AS Tag
                                                                                      ,3 AS Parent
                                                                                      ,PV.PrgVerId
                                                                                      ,PV.PrgVerDescrip
                                                                                      ,NULL AS ProgramCredits
                                                                                      ,T.TermId
                                                                                      ,T.TermDescrip AS TermDescription
                                                                                      ,T.StartDate AS TermStartDate
                                                                                      ,T.EndDate AS TermEndDate
                                                                                      ,R.ReqId AS CourseId
                                                                                      ,R.Descrip AS CourseDescription
                                                                                      ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                                                                      ,NULL AS CourseCredits
                                                                                      ,NULL AS CourseFinAidCredits
                                                                                      ,NULL AS CoursePassingGrade
                                                                                      ,NULL AS CourseScore
                                                                                      ,(
                                                                                       SELECT TOP 1 GrdBkResultId
                                                                                       FROM   arGrdBkResults
                                                                                       WHERE  StuEnrollId = SE.StuEnrollId
                                                                                              AND InstrGrdBkWgtDetailId = b.InstrGrdBkWgtDetailId
                                                                                              AND ClsSectionId = CS.ClsSectionId
                                                                                       ) AS GrdBkResultId
                                                                                      ,RTRIM(GCT.Descrip) AS GradeBookDescription
                                                                                      ,( CASE GCT.SysComponentTypeId
                                                                                              WHEN 544 THEN (
                                                                                                            SELECT SUM(HoursAttended)
                                                                                                            FROM   arExternshipAttendance
                                                                                                            WHERE  StuEnrollId = SE.StuEnrollId
                                                                                                            )
                                                                                              ELSE
                                                                                                  --        (select Top 1 Score from arGrdBkResults where StuEnrollId=SE.StuEnrollId and   
                                                                                                  --        InstrGrdBkWgtDetailId=b.InstrGrdBkWgtDetailId and  
                                                                                                  --        ClsSectionId=CS.ClsSectionId order by moddate desc)   
                                                                                                  NULL
                                                                                         END
                                                                                       ) AS GradeBookScore
                                                                                      ,NULL AS GradeBookPostDate
                                                                                      ,NULL AS GradeBookPassingGrade
                                                                                      ,NULL AS GradeBookWeight
                                                                                      ,NULL AS GradeBookRequired
                                                                                      ,NULL AS GradeBookMustPass
                                                                                      ,GCT.SysComponentTypeId AS GradeBookSysComponentTypeId
                                                                                      ,NULL AS GradeBookHoursRequired
                                                                                      ,NULL AS GradeBookHoursCompleted
                                                                                      ,SE.StuEnrollId
                                                                                      ,( CASE WHEN GCT.SysComponentTypeId IN ( 500, 503, 504, 544 ) THEN b.Number
                                                                                              ELSE (
                                                                                                   SELECT MIN(MinVal)
                                                                                                   FROM   arGradeScaleDetails GSD
                                                                                                         ,arGradeSystemDetails GSS
                                                                                                   WHERE  GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                                                          AND GSS.IsPass = 1
                                                                                                          AND GSD.GrdScaleId = CS.GrdScaleId
                                                                                                   )
                                                                                         END
                                                                                       ) AS MinResult
                                                                                      ,SYRES.Resource AS GradeComponentDescription
                                                                                      ,NULL AS CreditsAttempted
                                                                                      ,NULL AS CreditsEarned
                                                                                      ,NULL AS Completed
                                                                                      ,NULL AS CurrentScore
                                                                                      ,NULL AS CurrentGrade
                                                                                      ,GCT.SysComponentTypeId AS FinalScore
                                                                                      ,NULL AS FinalGrade
                                                                                      ,NULL AS WeightedAverage_GPA
                                                                                      ,NULL AS SimpleAverage_GPA
                                                                                      ,NULL AS WeightedAverage_CumGPA
                                                                                      ,NULL AS SimpleAverage_CumGPA
                                                                                      ,C1.CampusId
                                                                                      ,C1.CampDescrip
                                                                                      --ROW_NUMBER() OVER (partition by SE.StuEnrollId,R.ReqId Order By C1.CampDescrip,PV.PrgVerDescrip,T.StartDate,T.EndDate,T.TermId,T.TermDescrip,R.ReqId,R.Descrip,GCT.SysComponentTypeId,GCT.Descrip) as rownumber,  
                                                                                      ,c.FirstName AS FirstName
                                                                                      ,c.LastName AS LastName
                                                                                      ,c.MiddleName
                                                                                      ,SYRES.ResourceID
                                                                                      ,b.Required
                                                                                FROM   arResults e
                                                                                      ,arStuEnrollments SE
                                                                                      ,arStudent c
                                                                                      ,arClassSections CS
                                                                                      ,arReqs R
                                                                                      ,arTerm T
                                                                                      ,arGrdComponentTypes GCT
                                                                                      , (
                                                                                        SELECT Resource
                                                                                              ,ResourceID
                                                                                        FROM   syResources
                                                                                        WHERE  ResourceTypeID = 10
                                                                                        ) SYRES
                                                                                      ,syCampuses C1
                                                                                      ,arPrgVersions PV
                                                                                      , (
                                                                                        SELECT   DISTINCT TOP 1 A.InstrGrdBkWgtId
                                                                                                ,A.EffectiveDate
                                                                                                ,b.GrdScaleId
                                                                                                ,b.ReqId
                                                                                        FROM     arGrdBkWeights A
                                                                                                ,arClassSections b
                                                                                        WHERE    A.ReqId = b.ReqId
                                                                                                 AND A.EffectiveDate <= b.StartDate
                                                                                                 AND b.TermId = @TermId
                                                                                                 AND b.ReqId = @reqid
                                                                                        ORDER BY EffectiveDate DESC
                                                                                        ) a
                                                                                      ,arGrdBkWgtDetails b
                                                                                WHERE  e.StuEnrollId = SE.StuEnrollId
                                                                                       AND SE.StudentId = c.StudentId
                                                                                       AND e.TestId = CS.ClsSectionId
                                                                                       AND CS.ReqId = R.ReqId
                                                                                       AND CS.TermId = T.TermId
                                                                                       AND a.InstrGrdBkWgtId = b.InstrGrdBkWgtId
                                                                                       AND a.ReqId = R.ReqId
                                                                                       AND GCT.GrdComponentTypeId = b.GrdComponentTypeId
                                                                                       AND SYRES.ResourceID = GCT.SysComponentTypeId
                                                                                       AND SE.CampusId = C1.CampusId
                                                                                       AND SE.PrgVerId = PV.PrgVerId
                                                                                       AND SE.StuEnrollId = @StuEnrollId
                                                                                       AND (
                                                                                           @sysComponentTypeId IS NULL
                                                                                           OR GCT.SysComponentTypeId IN (
                                                                                                                        SELECT Val
                                                                                                                        FROM   MultipleValuesForReportParameters(
                                                                                                                                                                    @sysComponentTypeId
                                                                                                                                                                   ,','
                                                                                                                                                                   ,1
                                                                                                                                                                )
                                                                                                                        )
                                                                                           )
                                                                                ) dt5
                                                                         WHERE  GrdBkResultId IS NULL
                                                                         UNION
                                                                         SELECT DISTINCT 4 AS Tag
                                                                               ,3 AS Parent
                                                                               ,PV.PrgVerId
                                                                               ,PV.PrgVerDescrip
                                                                               ,NULL AS ProgramCredits
                                                                               ,T.TermId
                                                                               ,T.TermDescrip AS TermDescription
                                                                               ,T.StartDate AS TermStartDate
                                                                               ,T.EndDate AS TermEndDate
                                                                               ,R.ReqId AS CourseId
                                                                               ,R.Descrip AS CourseDescription
                                                                               ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                                                               ,NULL AS CourseCredits
                                                                               ,NULL AS CourseFinAidCredits
                                                                               ,NULL AS CoursePassingGrade
                                                                               ,NULL AS CourseScore
                                                                               ,(
                                                                                SELECT TOP 1 GrdBkResultId
                                                                                FROM   arGrdBkResults
                                                                                WHERE  StuEnrollId = SE.StuEnrollId
                                                                                       AND InstrGrdBkWgtDetailId = b.InstrGrdBkWgtDetailId
                                                                                       AND ClsSectionId = CS.ClsSectionId
                                                                                ) AS GrdBkResultId
                                                                               ,RTRIM(GCT.Descrip) AS GradeBookDescription
                                                                               ,( CASE GCT.SysComponentTypeId
                                                                                       WHEN 544 THEN (
                                                                                                     SELECT SUM(HoursAttended)
                                                                                                     FROM   arExternshipAttendance
                                                                                                     WHERE  StuEnrollId = SE.StuEnrollId
                                                                                                     )
                                                                                       ELSE
                                                                                           --        (select Top 1 Score from arGrdBkResults where StuEnrollId=SE.StuEnrollId and   
                                                                                           --        InstrGrdBkWgtDetailId=b.InstrGrdBkWgtDetailId and  
                                                                                           --        ClsSectionId=CS.ClsSectionId order by moddate desc)   
                                                                                           NULL
                                                                                  END
                                                                                ) AS GradeBookScore
                                                                               ,NULL AS GradeBookPostDate
                                                                               ,NULL AS GradeBookPassingGrade
                                                                               ,NULL AS GradeBookWeight
                                                                               ,NULL AS GradeBookRequired
                                                                               ,NULL AS GradeBookMustPass
                                                                               ,GCT.SysComponentTypeId AS GradeBookSysComponentTypeId
                                                                               ,NULL AS GradeBookHoursRequired
                                                                               ,NULL AS GradeBookHoursCompleted
                                                                               ,SE.StuEnrollId
                                                                               ,( CASE WHEN GCT.SysComponentTypeId IN ( 500, 503, 504, 544 ) THEN b.Number
                                                                                       ELSE (
                                                                                            SELECT MIN(MinVal)
                                                                                            FROM   arGradeScaleDetails GSD
                                                                                                  ,arGradeSystemDetails GSS
                                                                                            WHERE  GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                                                   AND GSS.IsPass = 1
                                                                                                   AND GSD.GrdScaleId = CS.GrdScaleId
                                                                                            )
                                                                                  END
                                                                                ) AS MinResult
                                                                               ,SYRES.Resource AS GradeComponentDescription
                                                                               ,NULL AS CreditsAttempted
                                                                               ,NULL AS CreditsEarned
                                                                               ,NULL AS Completed
                                                                               ,NULL AS CurrentScore
                                                                               ,NULL AS CurrentGrade
                                                                               ,GCT.SysComponentTypeId AS FinalScore
                                                                               ,NULL AS FinalGrade
                                                                               ,NULL AS WeightedAverage_GPA
                                                                               ,NULL AS SimpleAverage_GPA
                                                                               ,NULL AS WeightedAverage_CumGPA
                                                                               ,NULL AS SimpleAverage_CumGPA
                                                                               ,C1.CampusId
                                                                               ,C1.CampDescrip
                                                                               --ROW_NUMBER() OVER (partition by SE.StuEnrollId,R.ReqId Order By C1.CampDescrip,PV.PrgVerDescrip,T.StartDate,T.EndDate,T.TermId,T.TermDescrip,R.ReqId,R.Descrip,GCT.SysComponentTypeId,GCT.Descrip) as rownumber,  
                                                                               ,c.FirstName AS FirstName
                                                                               ,c.LastName AS LastName
                                                                               ,c.MiddleName
                                                                               ,SYRES.ResourceID
                                                                               ,b.Required
                                                                         FROM   arResults e
                                                                               ,arStuEnrollments SE
                                                                               ,arStudent c
                                                                               ,arClassSections CS
                                                                               ,arReqs R
                                                                               ,arTerm T
                                                                               ,arGrdComponentTypes GCT
                                                                               , (
                                                                                 SELECT Resource
                                                                                       ,ResourceID
                                                                                 FROM   syResources
                                                                                 WHERE  ResourceTypeID = 10
                                                                                 ) SYRES
                                                                               ,syCampuses C1
                                                                               ,arPrgVersions PV
                                                                               , (
                                                                                 SELECT   DISTINCT TOP 1 A.InstrGrdBkWgtId
                                                                                         ,A.EffectiveDate
                                                                                         ,b.GrdScaleId
                                                                                         ,b.ReqId
                                                                                 FROM     arGrdBkWeights A
                                                                                         ,arClassSections b
                                                                                 WHERE    A.ReqId = b.ReqId
                                                                                          AND A.EffectiveDate <= b.StartDate
                                                                                          AND b.TermId = @TermId
                                                                                          AND b.ReqId = @reqid
                                                                                 ORDER BY A.EffectiveDate DESC
                                                                                 ) a
                                                                               ,arGrdBkWgtDetails b
                                                                         WHERE  e.StuEnrollId = SE.StuEnrollId
                                                                                AND SE.StudentId = c.StudentId
                                                                                AND e.TestId = CS.ClsSectionId
                                                                                AND CS.ReqId = R.ReqId
                                                                                AND CS.TermId = T.TermId
                                                                                AND a.InstrGrdBkWgtId = b.InstrGrdBkWgtId
                                                                                AND a.ReqId = R.ReqId
                                                                                AND GCT.GrdComponentTypeId = b.GrdComponentTypeId
                                                                                AND SYRES.ResourceID = GCT.SysComponentTypeId
                                                                                AND SE.CampusId = C1.CampusId
                                                                                AND SE.PrgVerId = PV.PrgVerId
                                                                                AND SE.StuEnrollId = @StuEnrollId
                                                                                AND R.ReqId = @reqid
                                                                                AND T.TermId = @TermId
                                                                                AND (
                                                                                    @sysComponentTypeId IS NULL
                                                                                    OR GCT.SysComponentTypeId IN (
                                                                                                                 SELECT Val
                                                                                                                 FROM   MultipleValuesForReportParameters(
                                                                                                                                                             @sysComponentTypeId
                                                                                                                                                            ,','
                                                                                                                                                            ,1
                                                                                                                                                         )
                                                                                                                 )
                                                                                    )
                                                                                AND e.TestId NOT IN (
                                                                                                    SELECT ClsSectionId
                                                                                                    FROM   arGrdBkResults
                                                                                                    WHERE  StuEnrollId = e.StuEnrollId
                                                                                                           AND ClsSectionId = e.TestId
                                                                                                    )
                                                                         UNION
                                                                         SELECT 4 AS Tag
                                                                               ,3
                                                                               ,PV.PrgVerId
                                                                               ,PV.PrgVerDescrip
                                                                               ,NULL
                                                                               ,T.TermId
                                                                               ,T.TermDescrip
                                                                               ,T.StartDate AS termStartdate
                                                                               ,T.EndDate AS TermEndDate
                                                                               ,GBCR.ReqId
                                                                               ,R.Descrip AS CourseDescrip
                                                                               ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                                                               ,NULL
                                                                               ,NULL
                                                                               ,NULL
                                                                               ,NULL
                                                                               ,GBCR.ConversionResultId AS GrdBkResultId
                                                                               ,GBCR.Comments AS GradeBookDescription
                                                                               ,GBCR.Score AS GradeBookResult
                                                                               ,NULL
                                                                               ,NULL
                                                                               ,NULL
                                                                               ,NULL
                                                                               ,NULL
                                                                               ,GCT.SysComponentTypeId
                                                                               ,NULL
                                                                               ,NULL
                                                                               ,SE.StuEnrollId
                                                                               ,GBCR.MinResult
                                                                               ,SYRES.Resource -- Student data    
                                                                               ,NULL AS CreditsAttempted
                                                                               ,NULL AS CreditsEarned
                                                                               ,NULL AS Completed
                                                                               ,NULL AS CurrentScore
                                                                               ,NULL AS CurrentGrade
                                                                               ,NULL AS FinalScore
                                                                               ,NULL AS FinalGrade
                                                                               ,NULL AS WeightedAverage_GPA
                                                                               ,NULL AS SimpleAverage_GPA
                                                                               ,NULL
                                                                               ,NULL
                                                                               ,C.CampusId
                                                                               ,C.CampDescrip
                                                                                               --ROW_NUMBER() OVER (partition by SE.StuEnrollId,R.ReqId Order By C.CampDescrip,PV.PrgVerDescrip,T.StartDate,T.EndDate,T.TermId,T.TermDescrip,R.ReqId,R.Descrip,GCT.SysComponentTypeId,GCT.Descrip) as rownumber,  
                                                                               ,S.FirstName AS FirstName
                                                                               ,S.LastName AS LastName
                                                                               ,S.MiddleName
                                                                               ,SYRES.ResourceID
                                                                               ,GBWD.Required
                                                                         FROM   arGrdBkConversionResults GBCR
                                                                         INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                                                                         INNER JOIN (
                                                                                    SELECT StudentId
                                                                                          ,FirstName
                                                                                          ,LastName
                                                                                          ,MiddleName
                                                                                    FROM   arStudent
                                                                                    ) S ON S.StudentId = SE.StudentId
                                                                         INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                                                         INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                                                                         INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                                                                         INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                                                         INNER JOIN arGrdBkWgtDetails GBWD ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                                                                              AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                                                         INNER JOIN arGrdBkWeights GBW ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                                                                                                          AND GBCR.ReqId = GBW.ReqId
                                                                         INNER JOIN (
                                                                                    SELECT Resource
                                                                                          ,ResourceID
                                                                                    FROM   syResources
                                                                                    WHERE  ResourceTypeID = 10
                                                                                    ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
                                                                         INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                                                                         WHERE --MaxEffectiveDatesByCourse.EffectiveDate <= T.StartDate and  
                                                                                SE.StuEnrollId = @StuEnrollId
                                                                                AND T.TermId = @TermId
                                                                                AND R.ReqId = @reqid
                                                                                AND (
                                                                                    @sysComponentTypeId IS NULL
                                                                                    OR GCT.SysComponentTypeId IN (
                                                                                                                 SELECT Val
                                                                                                                 FROM   MultipleValuesForReportParameters(
                                                                                                                                                             @sysComponentTypeId
                                                                                                                                                            ,','
                                                                                                                                                            ,1
                                                                                                                                                         )
                                                                                                                 )
                                                                                    )
                                                                         ) dt
                                                                  ) dt1
                                                           WHERE  Required = 1
                                                           );
            END;


        DECLARE @hasallscoresbeenpostedforrequiredcomponents BIT;
        IF @CountComponentsThatHasScores >= @CourseComponentsThatNeedsToBeScored
            BEGIN
                SET @hasallscoresbeenpostedforrequiredcomponents = 1;
            END;
        ELSE
            BEGIN
                SET @hasallscoresbeenpostedforrequiredcomponents = 0;
            END;



        /************************************************* Changes for Build 2816 *********************/
        -- Rally case DE 738 KeyBoarding Courses  
        --declare @GradesFormat varchar(50)  
        --set @GradesFormat = (select Value from syConfigAppSetValues where SettingId=47) -- 47 refers to grades format  
        SET @GradesFormat = (
                            SELECT dbo.GetAppSettingValue(47, @StuEnrollCampusId)
                            ); -- 47 refers to grades format  
        -- This condition is met only for numeric grade schools  
        IF (
           @IsGradeBookNotSatisified = 0
           AND @IsWeighted = 0
           AND @FinalScore IS NULL
           AND @FinalGradeDesc IS NULL
           AND LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
           )
           AND @CountComponentsThatHasScores >= 1
            BEGIN
                SET @CreditsAttempted = (
                                        SELECT Credits
                                        FROM   arReqs
                                        WHERE  ReqId = @reqid
                                        );
                IF @hasallscoresbeenpostedforrequiredcomponents = 1
                    BEGIN
                        SET @FinAidCredits = (
                                             SELECT FinAidCredits
                                             FROM   arReqs
                                             WHERE  ReqId = @reqid
                                             );
                        SET @CreditsEarned = (
                                             SELECT Credits
                                             FROM   arReqs
                                             WHERE  ReqId = @reqid
                                             );
                        SET @Completed = 1;
                    END;
            END;

        -- DE748 Name: ROSS: Completed field should also check for the Must Pass property of the work unit.   
        IF LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
           AND @IsGradeBookNotSatisified >= 1
            BEGIN
                SET @Completed = 0;
                SET @CreditsEarned = 0;
                SET @FinAidCreditsEarned = 0;
            END;

        --DE738 Name: ROSS: Progress Report not taking care of courses that are not weighted.   

        IF (
           LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
           AND @Completed = 1
           AND @FinalScore IS NULL
           AND @FinalGradeDesc IS NULL
           )
            BEGIN
                SET @CreditsAttempted = @CreditsAttempted;
                SET @CreditsEarned = @CreditsAttempted;
                SET @FinAidCreditsEarned = @FinAidCredits;
            END;

        -- In Ross Example : Externship, the student may not have completed the course but once he attempts a work unit  
        -- we need to take the credits as attempted  
        IF (
           LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
           AND @Completed = 0
           AND @FinalScore IS NULL
           AND @FinalGradeDesc IS NULL
           )
            BEGIN
                DECLARE @rowcount4 INT;
                SET @rowcount4 = (
                                 SELECT COUNT(*)
                                 FROM   arGrdBkResults
                                 WHERE  StuEnrollId = @StuEnrollId
                                        AND ClsSectionId = @ClsSectionId
                                        AND Score IS NOT NULL
                                 );
                IF @rowcount4 >= 1
                    BEGIN
                        SET @CreditsAttempted = (
                                                SELECT Credits
                                                FROM   arReqs
                                                WHERE  ReqId = @reqid
                                                );
                        SET @CreditsEarned = 0;
                        SET @FinAidCreditsEarned = 0;
                    END;
                ELSE
                    BEGIN
                        SET @rowcount4 = (
                                         SELECT COUNT(*)
                                         FROM   arGrdBkConversionResults
                                         WHERE  StuEnrollId = @StuEnrollId
                                                AND ReqId = @reqid
                                                AND TermId = @TermId
                                                AND Score IS NOT NULL
                                         );
                        IF @rowcount4 >= 1
                            BEGIN
                                SET @CreditsAttempted = (
                                                        SELECT Credits
                                                        FROM   arReqs
                                                        WHERE  ReqId = @reqid
                                                        );
                                SET @CreditsEarned = 0;
                                SET @FinAidCreditsEarned = 0;
                            END;
                    END;

                --For Externship Attendance        
                IF @sysComponentTypeId = 544
                    BEGIN
                        SET @rowcount4 = (
                                         SELECT COUNT(*)
                                         FROM   arExternshipAttendance
                                         WHERE  StuEnrollId = @StuEnrollId
                                                AND HoursAttended >= 1
                                         );
                        IF @rowcount4 >= 1
                            BEGIN
                                SET @CreditsAttempted = (
                                                        SELECT Credits
                                                        FROM   arReqs
                                                        WHERE  ReqId = @reqid
                                                        );
                                SET @CreditsEarned = 0;
                                SET @FinAidCreditsEarned = 0;
                            END;

                    END;

            END;
        /************************************************* Changes for Build 2816 *********************/

        -- If the final grade is not null the final grade will over ride current grade   
        IF @FinalGradeDesc IS NOT NULL
            BEGIN
                SET @CurrentGrade = @FinalGradeDesc;

            END;

        /************************* Case added for brownson starts here ******************/
        -- For Letter Grade Schools, if any one of work unit is attempted and if no final grade is posted then   
        -- set credit attempted  
        DECLARE @IsScorePostedForAnyWorkUnit INT; -- If value>=1 then score was posted  
        SET @IsScorePostedForAnyWorkUnit = (
                                           SELECT COUNT(*)
                                           FROM   arGrdBkResults
                                           WHERE  StuEnrollId = @StuEnrollId
                                                  AND ClsSectionId = @ClsSectionId
                                                  AND Score IS NOT NULL
                                           );
        IF (
           LOWER(LTRIM(RTRIM(@GradesFormat))) = 'letter'
           AND @FinalGradeDesc IS NULL
           AND @IsScorePostedForAnyWorkUnit >= 1
           )
            BEGIN
                SET @Completed = 0;
                SET @CreditsAttempted = (
                                        SELECT Credits
                                        FROM   arReqs
                                        WHERE  ReqId = @reqid
                                        );
                SET @CreditsEarned = 0;
                SET @FinAidCreditsEarned = 0;
            END;
        /************************* Case added for brownson ends here ******************/

        IF @IsCreditsEarned = 1
            BEGIN
                IF (
                   @IsGradeBookNotSatisified = 0
                   AND @IsWeighted > 0
                   ) -- If all Grade books are satisfied and all courses are weighted  
                    BEGIN
                        SET @Completed = 1;
                    END;
                ELSE
                    BEGIN
                        SET @Completed = 0;
                    END;
            END;


        -- For Letter grade schools no need to check for grade books satisifed condition  
        IF LOWER(LTRIM(RTRIM(@GradesFormat))) = 'letter'
            BEGIN
                IF @IsCreditsEarned = 1
                    BEGIN
                        SET @Completed = 1;
                    END;
            END;

        -- numeric and non ross schools  
        -- no need to check if student satisfied grade book and weights  
        --declare @ShowROSSOnlyTabsForStudent_Value bit  
        --set @ShowROSSOnlyTabsForStudent_Value = (select value from syConfigAppSetValues where settingId=68)  
        IF LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
           AND @ShowROSSOnlyTabsForStudent_Value = 0
            BEGIN
                IF @IsCreditsEarned = 1
                   OR (
                      @FinalScore IS NOT NULL
                      AND @IsPass = 1
                      )
                    BEGIN
                        SET @Completed = 1;
                    END;
            END;

        -- This condition does not apply for key boarding courses, as no final score or grade is posted and  
        -- isCreditsEarned will always be NULL  
        IF @IsCreditsEarned IS NULL
            BEGIN
                SET @Completed = 0;
                -- Only for Key boarding courses  
                IF (
                   @IsGradeBookNotSatisified = 0
                   AND @IsWeighted = 0
                   AND @FinalScore IS NULL
                   AND @FinalGradeDesc IS NULL
                   AND LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
                   )
                   AND @CountComponentsThatHasScores >= 1
                    BEGIN
                        SET @CreditsAttempted = (
                                                SELECT Credits
                                                FROM   arReqs
                                                WHERE  ReqId = @reqid
                                                );
                        IF @hasallscoresbeenpostedforrequiredcomponents = 1
                            BEGIN
                                SET @FinAidCredits = (
                                                     SELECT FinAidCredits
                                                     FROM   arReqs
                                                     WHERE  ReqId = @reqid
                                                     );
                                SET @CreditsEarned = (
                                                     SELECT Credits
                                                     FROM   arReqs
                                                     WHERE  ReqId = @reqid
                                                     );
                                SET @Completed = 1;
                            END;
                    END;
            END;



        -- DE1148  
        IF @Completed = 1
           AND @IsCreditsEarned = 1
            BEGIN
                SET @CreditsEarned = (
                                     SELECT Credits
                                     FROM   arReqs
                                     WHERE  ReqId = @reqid
                                     );
                SET @FinAidCreditsEarned = @FinAidCredits;
            END;

        DECLARE @varGradeRounding VARCHAR(3);
        DECLARE @roundfinalscore DECIMAL(18, 4);
        --set @varGradeRounding = (select value from syConfigAppSetValues where settingId=45)  
        SET @varGradeRounding = (
                                SELECT dbo.GetAppSettingValue(45, @StuEnrollCampusId)
                                );
        -- If rounding is set to yes, then round the scores to next available score or ignore rounding  
        IF ( LOWER(@varGradeRounding) = 'yes' )
            BEGIN
                IF @FinalScore IS NOT NULL
                    BEGIN
                        SET @FinalScore = ROUND(@FinalScore, 0);
                    END;
                IF @CurrentScore IS NOT NULL
                    BEGIN
                        SET @CurrentScore = ROUND(@CurrentScore, 0);
                    END;
            END;


        IF @CourseComponentsThatNeedsToBeScored >= 1
           AND @CountComponentsThatHasScores = 0
            BEGIN
                SET @CreditsAttempted = 0;
            END;

        IF @CountComponentsThatHasScores >= 1
            BEGIN
                SET @CreditsAttempted = (
                                        SELECT Credits
                                        FROM   arReqs
                                        WHERE  ReqId = @reqid
                                        );
            END;

        IF LOWER(LTRIM(RTRIM(@GradesFormat))) = 'letter'
           AND @FinalGrade IS NOT NULL
           AND @CourseComponentsThatNeedsToBeScored = 0
           AND @IsCreditsAttempted = 1
            BEGIN
                SET @CreditsAttempted = (
                                        SELECT Credits
                                        FROM   arReqs
                                        WHERE  ReqId = @reqid
                                        );
            END;

        IF LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
           AND @FinalScore IS NOT NULL
           AND @CourseComponentsThatNeedsToBeScored = 0
           AND @IsCreditsAttempted = 1
            BEGIN
                SET @CreditsAttempted = (
                                        SELECT Credits
                                        FROM   arReqs
                                        WHERE  ReqId = @reqid
                                        );
            END;

        -- Check if student passed the course  
        IF LOWER(LTRIM(RTRIM(@GradesFormat))) = 'letter'
           AND @FinalGrade IS NOT NULL
           AND @IsPass = 0
            BEGIN
                SET @Completed = 0;
                IF @IsCreditsEarned IS NULL
                   OR @IsCreditsEarned = 0
                    BEGIN
                        SET @CreditsEarned = 0;
                    END;
                IF @IsCreditsAttempted IS NULL
                   OR @IsCreditsAttempted = 0
                    BEGIN
                        SET @CreditsAttempted = 0;
                    END;
            END;

        -- Unitek : If the externship component was not satisfied then set completed to no  
        --IF @sysComponentTypeId = 544   
        --  BEGIN  
        --   IF @IsGradeBookNotSatisified>=1 -- work unit comp not satisfied  
        --    BEGIN  
        --     SET @Completed = 0  
        --    END  
        --   ELSE  
        --    BEGIN  
        --     SET @Completed = 1  
        --    end  
        --  end  

        DECLARE @CountWorkUnitsNotSatisfied_500503544 INT;
        -- Work unit not satisfied    
        IF LOWER(LTRIM(RTRIM(@SetGradeBookAt))) = 'courselevel'
            BEGIN

                CREATE TABLE #Temp1
                    (
                        Id UNIQUEIDENTIFIER
                       ,StuEnrollId UNIQUEIDENTIFIER
                       ,TermId UNIQUEIDENTIFIER
                       ,GradeBookDescription VARCHAR(50)
                       ,Number INT
                       ,GradeBookSysComponentTypeId INT
                       ,GradeBookScore DECIMAL(18, 2)
                       ,MinResult DECIMAL(18, 2)
                       ,GradeComponentDescription VARCHAR(50)
                       ,RowNumber INT
                       ,ClsSectionId UNIQUEIDENTIFIER
                    );
                DECLARE @Id UNIQUEIDENTIFIER
                       ,@Descrip VARCHAR(50)
                       ,@Number INT
                       ,@GrdComponentTypeId INT
                       ,@Counter INT
                       ,@times INT;
                DECLARE @MinResult DECIMAL(18, 2)
                       ,@GrdComponentDescription VARCHAR(50);

                SET @Counter = 0;

                DECLARE @TermStartDate1 DATETIME;
                SET @TermStartDate1 = (
                                      SELECT StartDate
                                      FROM   arTerm
                                      WHERE  TermId = @TermId
                                      );

                CREATE TABLE #temp2
                    (
                        ReqId UNIQUEIDENTIFIER
                       ,EffectiveDate DATETIME
                    );
                INSERT INTO #temp2
                            SELECT   ReqId
                                    ,MAX(EffectiveDate) AS EffectiveDate
                            FROM     arGrdBkWeights
                            WHERE    ReqId = @reqid
                                     AND EffectiveDate <= @TermStartDate1
                            GROUP BY ReqId;

                DECLARE getUsers_Cursor CURSOR FOR
                    SELECT   dt.ID
                            ,dt.Descrip
                            ,dt.Number
                            ,dt.SysComponentTypeId
                            ,dt.MinResult
                            ,dt.GradeComponentDescription
                            ,dt.ClsSectionId
                            ,ROW_NUMBER() OVER ( PARTITION BY @StuEnrollId
                                                             ,@TermId
                                                             ,SysComponentTypeId
                                                 ORDER BY SysComponentTypeId
                                                         ,Descrip
                                               ) AS rownumber
                    FROM     (
                             SELECT DISTINCT ISNULL(GD.InstrGrdBkWgtDetailId, NEWID()) AS ID
                                   ,GC.Descrip
                                   ,GD.Number
                                   ,GC.SysComponentTypeId
                                   ,( CASE WHEN GC.SysComponentTypeId IN ( 500, 503, 504, 544 ) THEN GD.Number
                                           ELSE (
                                                SELECT MIN(MinVal)
                                                FROM   arGradeScaleDetails GSD
                                                      ,arGradeSystemDetails GSS
                                                WHERE  GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                       AND GSS.IsPass = 1
                                                       AND GSD.GrdScaleId = CS.GrdScaleId
                                                )
                                      END
                                    ) AS MinResult
                                   ,S.Resource AS GradeComponentDescription
                                   ,CS.ClsSectionId
                             --,MaxEffectiveDatesByCourse.ReqId   
                             FROM   arGrdComponentTypes GC
                                   ,   (
                                       SELECT InstrGrdBkWgtDetailId
                                             ,InstrGrdBkWgtId
                                             ,Code
                                             ,Descrip
                                             ,Weight
                                             ,Seq
                                             ,ModUser
                                             ,ModDate
                                             ,GrdComponentTypeId
                                             ,Parameter
                                             ,Number
                                             ,GrdPolicyId
                                             ,Required
                                             ,MustPass
                                             ,CreditsPerService
                                       FROM   arGrdBkWgtDetails
                                       WHERE  InstrGrdBkWgtId IN (
                                                                 SELECT t1.InstrGrdBkWgtId
                                                                 FROM   arGrdBkWeights t1
                                                                       ,#temp2 t2
                                                                 WHERE  t1.ReqId = t2.ReqId
                                                                        AND t1.EffectiveDate = t2.EffectiveDate
                                                                 )
                                       ) GD
                                   ,arGrdBkWeights GW
                                   ,arReqs R
                                   ,arClassSections CS
                                   ,syResources S
                                   ,arResults RES
                                   ,arTerm T
                             WHERE  GC.GrdComponentTypeId = GD.GrdComponentTypeId
                                    AND GD.InstrGrdBkWgtId = GW.InstrGrdBkWgtId
                                    AND GW.ReqId = R.ReqId
                                    AND R.ReqId = CS.ReqId
                                    AND CS.TermId = @TermId
                                    AND RES.TestId = CS.ClsSectionId
                                    AND RES.StuEnrollId = @StuEnrollId
                                    AND GD.Number > 0
                                    AND GC.SysComponentTypeId = S.ResourceID
                                    AND CS.TermId = T.TermId
                                    AND R.ReqId = @reqid
                             ) dt
                    ORDER BY SysComponentTypeId
                            ,rownumber;
                OPEN getUsers_Cursor;
                FETCH NEXT FROM getUsers_Cursor
                INTO @Id
                    ,@Descrip
                    ,@Number
                    ,@GrdComponentTypeId
                    ,@MinResult
                    ,@GrdComponentDescription
                    ,@ClsSectionId
                    ,@rownumber;
                SET @Counter = 0;
                DECLARE @Score DECIMAL(18, 2)
                       ,@GrdCompDescrip VARCHAR(50);
                WHILE @@FETCH_STATUS = 0
                    BEGIN
                        --Print @number  
                        SET @times = 1;

                        --if (@GrdComponentTypeId = 500 or @GrdComponentTypeId=503 or @GrdComponentTypeId=544)  
                        -- begin  
                        --  set @GrdCompDescrip = @Descrip  
                        --   set @Score = (select Top 1 Score from arGrdBkResults where StuEnrollId=@StuEnrollId and InstrGrdBkWgtDetailId=@Id and ResNum=@times and ClsSectionId=@ClsSectionId)   
                        --   if @Score is NULL  
                        --    begin  
                        --     set @Score = (select Top 1 Score from arGrdBkResults where StuEnrollId=@StuEnrollId and InstrGrdBkWgtDetailId=@Id and ResNum=(@times-1) and ClsSectionId=@ClsSectionId)    
                        --    end  
                        --   insert into #temp1 values(@Id,@StuEnrollId,@TermId,  
                        --   @GrdCompDescrip,@Number,@GrdComponentTypeId,@Score,@MinResult,@GrdComponentDescription,@rownumber,@ClsSectionId)  
                        -- end  
                        IF (
                           @GrdComponentTypeId = 500
                           OR @GrdComponentTypeId = 503
                           OR @GrdComponentTypeId = 544
                           )
                            BEGIN
                                SET @GrdCompDescrip = @Descrip;
                                IF (
                                   @GrdComponentTypeId = 500
                                   OR @GrdComponentTypeId = 503
                                   )
                                    BEGIN
                                        SET @Score = (
                                                     SELECT SUM(Score)
                                                     FROM   arGrdBkResults
                                                     WHERE  StuEnrollId = @StuEnrollId
                                                            AND InstrGrdBkWgtDetailId = @Id
                                                            AND ClsSectionId = @ClsSectionId
                                                     );
                                    END;
                                IF ( @GrdComponentTypeId = 544 )
                                    BEGIN
                                        SET @Score = (
                                                     SELECT SUM(HoursAttended)
                                                     FROM   arExternshipAttendance
                                                     WHERE  StuEnrollId = @StuEnrollId
                                                     );
                                    END;
                                INSERT INTO #Temp1
                                VALUES ( @Id
                                        ,@StuEnrollId
                                        ,@TermId
                                        ,@GrdCompDescrip
                                        ,@Number
                                        ,@GrdComponentTypeId
                                        ,@Score
                                        ,@MinResult
                                        ,@GrdComponentDescription
                                        ,@rownumber
                                        ,@ClsSectionId
                                       );
                            END;
                        ELSE
                            BEGIN
                                WHILE @times <= @Number
                                    BEGIN
                                        --Print @times  

                                        IF @Number > 1
                                            BEGIN
                                                SET @GrdCompDescrip = @Descrip + CAST(@times AS CHAR);
                                                SET @Score = (
                                                             SELECT Score
                                                             FROM   arGrdBkResults
                                                             WHERE  StuEnrollId = @StuEnrollId
                                                                    AND InstrGrdBkWgtDetailId = @Id
                                                                    AND ResNum = @times
                                                                    AND ClsSectionId = @ClsSectionId
                                                             );


                                                SET @rownumber = @times;
                                            END;
                                        ELSE
                                            BEGIN
                                                SET @GrdCompDescrip = @Descrip;
                                                SET @Score = (
                                                             SELECT TOP 1 Score
                                                             FROM   arGrdBkResults
                                                             WHERE  StuEnrollId = @StuEnrollId
                                                                    AND InstrGrdBkWgtDetailId = @Id
                                                                    AND ResNum = @times
                                                                    AND ClsSectionId = @ClsSectionId
                                                             );
                                                IF @Score IS NULL
                                                    BEGIN
                                                        SET @Score = (
                                                                     SELECT TOP 1 Score
                                                                     FROM   arGrdBkResults
                                                                     WHERE  StuEnrollId = @StuEnrollId
                                                                            AND InstrGrdBkWgtDetailId = @Id
                                                                            AND ResNum = ( @times - 1 )
                                                                            AND ClsSectionId = @ClsSectionId
                                                                     );
                                                    END;
                                            END;
                                        INSERT INTO #Temp1
                                        VALUES ( @Id
                                                ,@StuEnrollId
                                                ,@TermId
                                                ,@GrdCompDescrip
                                                ,@Number
                                                ,@GrdComponentTypeId
                                                ,@Score
                                                ,@MinResult
                                                ,@GrdComponentDescription
                                                ,@rownumber
                                                ,@ClsSectionId
                                               );

                                        SET @times = @times + 1;
                                    END;
                            END;
                        FETCH NEXT FROM getUsers_Cursor
                        INTO @Id
                            ,@Descrip
                            ,@Number
                            ,@GrdComponentTypeId
                            ,@MinResult
                            ,@GrdComponentDescription
                            ,@ClsSectionId
                            ,@rownumber;
                    END;
                CLOSE getUsers_Cursor;
                DEALLOCATE getUsers_Cursor;

                -- Changes made on 12/28/2010 starts here  
                --Create table #Temp3(GetCountOfWorkUnit_500503544_NotSatisfied INT)  
                --INSERT INTO #Temp3   
                --SELECT COUNT(*) AS RowNumber FROM   
                SELECT *
                INTO   #temp3
                FROM   (
                       SELECT Id
                             ,StuEnrollId
                             ,TermId
                             ,GradeBookDescription
                             ,Number
                             ,GradeBookSysComponentTypeId
                             ,GradeBookScore
                             ,MinResult
                             ,GradeComponentDescription
                             ,RowNumber
                             ,ClsSectionId
                       FROM   #Temp1 --where GradeBookSysComponentTypeId=501  
                       --order by   
                       -- GradeBookSysComponentTypeId,GradeBookDescription,RowNumber  
                       UNION
                       SELECT GBWD.InstrGrdBkWgtDetailId
                             ,SE.StuEnrollId
                             ,T.TermId
                             ,GCT.Descrip AS GradeBookDescription
                             ,GBWD.Number
                             ,GCT.SysComponentTypeId
                             ,GBCR.Score
                             ,GBCR.MinResult
                             ,SYRES.Resource
                             ,ROW_NUMBER() OVER ( PARTITION BY SE.StuEnrollId
                                                              ,T.TermId
                                                              ,GCT.SysComponentTypeId
                                                  ORDER BY GCT.SysComponentTypeId
                                                          ,GCT.Descrip
                                                ) AS rownumber
                             ,(
                              SELECT TOP 1 ClsSectionId
                              FROM   arClassSections
                              WHERE  TermId = T.TermId
                                     AND ReqId = R.ReqId
                              ) AS ClsSectionId
                       FROM   arGrdBkConversionResults GBCR
                       INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                       INNER JOIN (
                                  SELECT StudentId
                                        ,FirstName
                                        ,LastName
                                        ,MiddleName
                                  FROM   arStudent
                                  ) S ON S.StudentId = SE.StudentId
                       INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                       INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                       INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                       INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                       INNER JOIN arGrdBkWgtDetails GBWD ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                            AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                       INNER JOIN arGrdBkWeights GBW ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                                                        AND GBCR.ReqId = GBW.ReqId
                       INNER JOIN (
                                  SELECT   ReqId
                                          ,MAX(EffectiveDate) AS EffectiveDate
                                  FROM     arGrdBkWeights
                                  GROUP BY ReqId
                                  ) AS MaxEffectiveDatesByCourse ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
                       INNER JOIN (
                                  SELECT Resource
                                        ,ResourceID
                                  FROM   syResources
                                  WHERE  ResourceTypeID = 10
                                  ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
                       INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                       WHERE  MaxEffectiveDatesByCourse.EffectiveDate <= T.StartDate
                              AND SE.StuEnrollId = @StuEnrollId
                              AND T.TermId = @TermId
                              AND --R.ReqId = @ReqId and   
                           (
                           @sysComponentTypeId IS NULL
                           OR GCT.SysComponentTypeId IN (
                                                        SELECT Val
                                                        FROM   MultipleValuesForReportParameters(@sysComponentTypeId, ',', 1)
                                                        )
                           )
                       --and GCT.SysComponentTypeId=501  
                       ) derT1; --WHERE GradeBookSysComponentTypeId IN (500,503,544) AND (GradeBookScore IS NULL OR MinResult>GradeBookScore)  
                --order by   
                -- GradeBookSysComponentTypeId,GradeBookDescription,RowNumber  
                --SELECT * FROM #temp3  

                DECLARE @DoesCourseHaveClinicalComponents INT
                       ,@DoesCourseHaveNonClinicalComponents INT;
                SET @DoesCourseHaveClinicalComponents = (
                                                        SELECT COUNT(*)
                                                        FROM   #temp3
                                                        WHERE  GradeBookSysComponentTypeId IN ( 500, 503, 544 )
                                                        );
                SET @DoesCourseHaveNonClinicalComponents = (
                                                           SELECT COUNT(*)
                                                           FROM   #temp3
                                                           WHERE  GradeBookSysComponentTypeId NOT IN ( 500, 503, 544 )
                                                           );
                SET @CountWorkUnitsNotSatisfied_500503544 = (
                                                            SELECT COUNT(*)
                                                            FROM   #temp3
                                                            WHERE  GradeBookSysComponentTypeId IN ( 500, 503, 544 )
                                                                   AND (
                                                                       GradeBookScore IS NULL
                                                                       OR MinResult > GradeBookScore
                                                                       )
                                                            );




                IF (
                   @DoesCourseHaveClinicalComponents >= 1
                   AND @DoesCourseHaveNonClinicalComponents >= 1
                   )
                    BEGIN
                        -- There is a combination  
                        -- First check if clinical has been satisfied  
                        IF @CountWorkUnitsNotSatisfied_500503544 >= 1
                            BEGIN
                                SET @Completed = 0; --If Clinical comp not satisfied course is incomplete  
                            END;

                        -- if course has been satisfied  
                        IF @CountWorkUnitsNotSatisfied_500503544 = 0
                            BEGIN
                                -- Check if final score is posted for letter grade schools  
                                IF LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
                                   AND @FinalScore IS NULL
                                    BEGIN
                                        SET @Completed = 0;
                                    END;
                                IF LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
                                   AND @FinalScore IS NOT NULL
                                   AND @IsPass = 1
                                    BEGIN
                                        SET @Completed = 1;
                                    END;
                                -- Check if final grade is posted for numeric grade schools  
                                IF LOWER(LTRIM(RTRIM(@GradesFormat))) = 'letter'
                                   AND @FinalGradeDesc IS NULL
                                    BEGIN
                                        SET @Completed = 0;
                                    END;
                                IF LOWER(LTRIM(RTRIM(@GradesFormat))) = 'letter'
                                   AND @FinalGradeDesc IS NOT NULL
                                   AND @IsPass = 1
                                    BEGIN
                                        SET @Completed = 1;
                                    END;
                                -- For Keyboarding course  no final score is posted but completed should be set to yes  
                                IF LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
                                   AND @FinalScore IS NULL
                                   AND @ShowROSSOnlyTabsForStudent_Value = 1
                                    BEGIN
                                        SET @Completed = 1;
                                    END;
                            END;


                        IF @IsGradeBookNotSatisified >= 1
                            BEGIN
                                SET @Completed = 0;
                            END;

                    END;
                -- If course has only clinic components  
                IF (
                   @DoesCourseHaveClinicalComponents >= 1
                   AND @DoesCourseHaveNonClinicalComponents = 0
                   )
                    BEGIN
                        IF @CountWorkUnitsNotSatisfied_500503544 >= 1
                            BEGIN
                                SET @Completed = 0; --If Clinical comp not satisfied course is incomplete  
                            END;
                        IF @CountWorkUnitsNotSatisfied_500503544 = 0
                            BEGIN
                                SET @Completed = 1; --If Clinical comp not satisfied course is incomplete  
                            END;
                    END;
                --If course has only non clinical courses  
                IF (
                   @DoesCourseHaveClinicalComponents = 0
                   AND @DoesCourseHaveNonClinicalComponents >= 1
                   )
                    BEGIN
                        IF @IsGradeBookNotSatisified >= 1
                            BEGIN
                                SET @Completed = 0;
                            END;
                        IF @IsGradeBookNotSatisified = 0
                           AND @IsPass = 1
                            BEGIN
                                SET @Completed = 1;
                            END;
                    END;
                -- Changes made on 12/28/2010 ends here  


                DROP TABLE #temp3;
                DROP TABLE #temp2;
                DROP TABLE #Temp1;
            END;




        -- Unitek/Ross : If the externship component was not satisfied then set completed to no  
        -- Unitek : If the externship component was not satisfied then set completed to no  
        --if  LOWER(LTRIM(RTRIM(@SetGradeBookAt))) = 'courselevel'  
        --BEGIN   
        -- IF @CountWorkUnitsNotSatisfied_500503544>=1 OR @IsGradeBookNotSatisified>=1   
        --  BEGIN  
        --   SET @Completed = 0  
        --  END  
        -- ELSE  
        --  BEGIN  
        --   SET @Completed = 1  
        --   IF (@FinalScore IS NOT NULL OR @FinalGradeDesc IS NOT NULL) AND @isPass=0  
        --   begin  
        --    SET @Completed = 0  
        --   END  
        --    IF (@FinalScore IS NULL OR @FinalGradeDesc IS NULL) and LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter' AND @CourseComponentsThatNeedsToBeScored>=1 and @CountComponentsThatHasScores=0  
        --    BEGIN  
        --     SET @Completed=0  
        --    END  
        --     IF (@FinalScore IS NULL OR @FinalGradeDesc IS NULL) and LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter' AND @CourseComponentsThatNeedsToBeScored>=1 and @CountComponentsThatHasScores=0  
        --    BEGIN  
        --     SET @Completed=0  
        --    END  
        --    IF (@FinalScore IS NULL OR @FinalGradeDesc IS NULL) and LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter' and (@SysComponentTypeId IS NULL OR @SysComponentTypeId <> 544)  and @CountComponentsThatHasScores=0 --AND @CourseComponentsThatNeedsToBeScored=0   
        --    BEGIN  
        --     SET @Completed=0  
        --    END  
        --    IF (@FinalGradeDesc IS NULL) and LOWER(LTRIM(RTRIM(@GradesFormat))) = 'letter' and @CountComponentsThatHasScores=0 --AND @CourseComponentsThatNeedsToBeScored=0   
        --    BEGIN  
        --     SET @Completed=0  
        --    end  
        --  END  
        --end  


        DELETE FROM syCreditSummary
        WHERE StuEnrollId = @StuEnrollId
              AND TermId = @TermId
              AND ReqId = @reqid
              AND ClsSectionId = @ClsSectionId;

        INSERT INTO syCreditSummary
                    SELECT @StuEnrollId
                          ,@TermId
                          ,@TermDescrip
                          ,@reqid
                          ,@CourseCodeDescrip
                          ,NULL
                          ,ISNULL(@CreditsEarned, 0)
                          ,@CreditsAttempted
                          ,@CurrentScore
                          ,@CurrentGrade
                          ,@FinalScore
                          ,@FinalGradeDesc
                          ,@Completed
                          ,@FinalGPA
                          ,@Product_WeightedAverage_Credits_GPA
                          ,@Count_WeightedAverage_Credits
                          ,@Product_SimpleAverage_Credits_GPA
                          ,@Count_SimpleAverage_Credits
                          ,'sa'
                          ,GETDATE()
                          ,@ComputedSimpleGPA
                          ,@ComputedWeightedGPA
                          ,@CourseCredits
                          ,NULL
                          ,NULL
                          ,@FinAidCreditsEarned
                          ,NULL
                          ,NULL
                          ,@TermStartDate
                    WHERE  NOT EXISTS (
                                      SELECT 1
                                      FROM   dbo.arTransferGrades
                                      WHERE  StuEnrollId = @StuEnrollId
                                             AND TermId = @TermId
                                             AND ReqId = @reqid
                                      );

        DECLARE @wCourseCredits DECIMAL(18, 2)
               ,@wWeighted_GPA_Credits DECIMAL(18, 2)
               ,@sCourseCredits DECIMAL(18, 2)
               ,@sSimple_GPA_Credits DECIMAL(18, 2);
        -- For weighted average  
        SET @ComputedWeightedGPA = 0;
        SET @ComputedSimpleGPA = 0;
        SET @wCourseCredits = (
                              SELECT SUM(coursecredits)
                              FROM   syCreditSummary
                              WHERE  StuEnrollId = @StuEnrollId
                                     AND TermId = @TermId
                                     AND FinalGPA IS NOT NULL
                              );
        SET @wWeighted_GPA_Credits = (
                                     SELECT SUM(coursecredits * FinalGPA)
                                     FROM   syCreditSummary
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND TermId = @TermId
                                            AND FinalGPA IS NOT NULL
                                     );

        IF @wCourseCredits >= 1
            BEGIN
                SET @ComputedWeightedGPA = @wWeighted_GPA_Credits / @wCourseCredits;
            END;


        --For Simple Average  
        SET @sCourseCredits = (
                              SELECT COUNT(*)
                              FROM   syCreditSummary
                              WHERE  StuEnrollId = @StuEnrollId
                                     AND TermId = @TermId
                                     AND FinalGPA IS NOT NULL
                              );
        SET @sSimple_GPA_Credits = (
                                   SELECT SUM(FinalGPA)
                                   FROM   syCreditSummary
                                   WHERE  StuEnrollId = @StuEnrollId
                                          AND TermId = @TermId
                                          AND FinalGPA IS NOT NULL
                                   );
        IF @sCourseCredits >= 1
            BEGIN
                SET @ComputedSimpleGPA = @sSimple_GPA_Credits / @sCourseCredits;
            END;


        --CumulativeGPA  
        DECLARE @cumCourseCredits DECIMAL(18, 2)
               ,@cumWeighted_GPA_Credits DECIMAL(18, 2)
               ,@cumWeightedGPA DECIMAL(18, 2);
        SET @cumWeightedGPA = 0;
        SET @cumCourseCredits = (
                                SELECT SUM(coursecredits)
                                FROM   syCreditSummary
                                WHERE  StuEnrollId = @StuEnrollId
                                       AND FinalGPA IS NOT NULL
                                );
        SET @cumWeighted_GPA_Credits = (
                                       SELECT SUM(coursecredits * FinalGPA)
                                       FROM   syCreditSummary
                                       WHERE  StuEnrollId = @StuEnrollId
                                              AND FinalGPA IS NOT NULL
                                       );

        IF @cumCourseCredits >= 1
            BEGIN
                SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
            END;

        --CumulativeSimpleGPA  
        DECLARE @cumSimpleCourseCredits DECIMAL(18, 2)
               ,@cumSimple_GPA_Credits DECIMAL(18, 2)
               ,@cumSimpleGPA DECIMAL(18, 2);
        SET @cumSimpleGPA = 0;
        SET @cumSimpleCourseCredits = (
                                      SELECT COUNT(coursecredits)
                                      FROM   syCreditSummary
                                      WHERE  StuEnrollId = @StuEnrollId
                                             AND FinalGPA IS NOT NULL
                                      );
        SET @cumSimple_GPA_Credits = (
                                     SELECT SUM(FinalGPA)
                                     FROM   syCreditSummary
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND FinalGPA IS NOT NULL
                                     );

        IF @cumSimpleCourseCredits >= 1
            BEGIN
                SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits;
            END;

        --Average calculation  
        DECLARE @termAverageSum DECIMAL(18, 2)
               ,@CumAverage DECIMAL(18, 2)
               ,@cumAverageSum DECIMAL(18, 2)
               ,@cumAveragecount INT;

        -- Term Average  
        SET @TermAverageCount = (
                                SELECT COUNT(*)
                                FROM   syCreditSummary
                                WHERE  StuEnrollId = @StuEnrollId
                                       --and Completed=1   
                                       AND TermId = @TermId
                                       AND FinalScore IS NOT NULL
                                );
        SET @termAverageSum = (
                              SELECT SUM(FinalScore)
                              FROM   syCreditSummary
                              WHERE  StuEnrollId = @StuEnrollId
                                     --and Completed=1   
                                     AND TermId = @TermId
                                     AND FinalScore IS NOT NULL
                              );
        SET @TermAverage = @termAverageSum / @TermAverageCount;

        -- Cumulative Average  
        SET @cumAveragecount = (
                               SELECT COUNT(*)
                               FROM   syCreditSummary
                               WHERE  StuEnrollId = @StuEnrollId
                                      --and Completed=1   
                                      AND FinalScore IS NOT NULL
                               );
        SET @cumAverageSum = (
                             SELECT SUM(FinalScore)
                             FROM   syCreditSummary
                             WHERE  StuEnrollId = @StuEnrollId
                                    AND
                                 --Completed=1 and   
                                 FinalScore IS NOT NULL
                             );
        SET @CumAverage = @cumAverageSum / @cumAveragecount;


        UPDATE syCreditSummary
        SET    TermGPA_Simple = @ComputedSimpleGPA
              ,TermGPA_Weighted = @ComputedWeightedGPA
              ,Average = @TermAverage
        WHERE  StuEnrollId = @StuEnrollId
               AND TermId = @TermId;

        --Update Cumulative GPA  
        UPDATE syCreditSummary
        SET    CumulativeGPA = @cumWeightedGPA
              ,CumulativeGPA_Simple = @cumSimpleGPA
              ,CumAverage = @CumAverage
        WHERE  StuEnrollId = @StuEnrollId;



        SET @PrevStuEnrollId = @StuEnrollId;
        SET @PrevTermId = @TermId;
        SET @PrevReqId = @reqid;

        FETCH NEXT FROM GetCreditsSummary_Cursor
        INTO @StuEnrollId
            ,@TermId
            ,@TermDescrip
            ,@TermStartDate
            ,@reqid
            ,@CourseCodeDescrip
            ,@FinalScore
            ,@FinalGrade
            ,@sysComponentTypeId
            ,@CreditsAttempted
            ,@ClsSectionId
            ,@Grade
            ,@IsPass
            ,@IsCreditsAttempted
            ,@IsCreditsEarned
            ,@PrgVerId
            ,@IsInGPA
            ,@FinAidCredits;
    END;
CLOSE GetCreditsSummary_Cursor;
DEALLOCATE GetCreditsSummary_Cursor;

--==========================================================================================
-- END  --  TRIGGER TR_InsertCreditSummary_TransferGrades  -- AFTER INSERT, UPDATE  
--========================================================================================== 


SET NOCOUNT OFF;
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[Trigger_SetGrade_ForNumeric]
ON [dbo].[arTransferGrades]
FOR INSERT
AS
SET NOCOUNT ON;
    BEGIN
        DECLARE @TransferId UNIQUEIDENTIFIER;
        DECLARE @score DECIMAL(18, 2);
        DECLARE @graderounding VARCHAR(50);
        DECLARE @IsTransferred BIT;
        SET @graderounding = (
                             SELECT DISTINCT LOWER(graderoundingvalue)
                             FROM   syGradeRounding
                             );
        DECLARE c1 CURSOR
            FOR(
            SELECT DISTINCT TransferId
                  ,Score
                  ,IsTransferred
            FROM   Inserted
            WHERE  GrdSysDetailId IS NULL
                   AND Score IS NOT NULL);

        OPEN c1;
        FETCH NEXT FROM c1
        INTO @TransferId
            ,@score
            ,@IsTransferred;
        WHILE @@FETCH_STATUS = 0
            BEGIN
                IF @graderounding = 'yes'
                    BEGIN
                        SET @score = ROUND(@score, 0);
                    END;
                ELSE
                    BEGIN
                        SET @score = FLOOR(@score);
                    END;

                IF  (
                    @score = 0
                    AND @IsTransferred = 1
                    )
                    BEGIN
                        --When the school uses a 0 and checks IsTransferred in the UI it means that they just want to record a TR grade.
                        --In this case then we need to get the TR grade for the grade system that is tied to the program version of the enrollment
                        UPDATE A
                        SET    A.GrdSysDetailId = B.GrdSysDetailId
                        FROM   arTransferGrades A
                              ,arGradeSystemDetails B
                              ,arStuEnrollments D
                              ,arPrgVersions E
                        WHERE  A.StuEnrollId = D.StuEnrollId
                               AND D.PrgVerId = E.PrgVerId
                               AND E.GrdSystemId = B.GrdSystemId
                               AND B.IsTransferGrade = 1
                               AND A.TransferId = @TransferId;
                    END;
                ELSE IF  (
                         @score >= 0
                         )
                         BEGIN
                             UPDATE A
                             SET    A.GrdSysDetailId = B.GrdSysDetailId
                             FROM   arTransferGrades A
                                   ,arGradeSystemDetails B
                                   ,arGradeScaleDetails C
                                   ,arStuEnrollments D
                                   ,arPrgVersions E
                             WHERE  B.GrdSysDetailId = C.GrdSysDetailId
                                    AND @score >= C.MinVal
                                    AND @score <= C.MaxVal
                                    AND A.StuEnrollId = D.StuEnrollId
                                    AND D.PrgVerId = E.PrgVerId
                                    AND E.GrdSystemId = B.GrdSystemId
                                    AND A.TransferId = @TransferId;
                         END;
                FETCH NEXT FROM c1
                INTO @TransferId
                    ,@score
                    ,@IsTransferred;
            END;
        CLOSE c1;
        DEALLOCATE c1;

    END;

GO
ALTER TABLE [dbo].[arTransferGrades] ADD CONSTRAINT [PK_arTransferGrades_TransferId] PRIMARY KEY CLUSTERED  ([TransferId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_arTransferGrades_StuEnrollId_TermId] ON [dbo].[arTransferGrades] ([StuEnrollId], [TermId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_arTransferGrades_StuEnrollId_TransferId_TermId_ReqId] ON [dbo].[arTransferGrades] ([StuEnrollId], [TransferId], [TermId], [ReqId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arTransferGrades] ADD CONSTRAINT [FK_arTransferGrades_arGradeSystemDetails_GrdSysDetailId_GrdSysDetailId] FOREIGN KEY ([GrdSysDetailId]) REFERENCES [dbo].[arGradeSystemDetails] ([GrdSysDetailId])
GO
ALTER TABLE [dbo].[arTransferGrades] ADD CONSTRAINT [FK_arTransferGrades_arReqs_ReqId_ReqId] FOREIGN KEY ([ReqId]) REFERENCES [dbo].[arReqs] ([ReqId])
GO
ALTER TABLE [dbo].[arTransferGrades] ADD CONSTRAINT [FK_arTransferGrades_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY ([StuEnrollId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
ALTER TABLE [dbo].[arTransferGrades] ADD CONSTRAINT [FK_arTransferGrades_arTerm_TermId_TermId] FOREIGN KEY ([TermId]) REFERENCES [dbo].[arTerm] ([TermId])
GO
