CREATE TABLE [dbo].[syWapiExternalOperationMode]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsActive] [bit] NOT NULL CONSTRAINT [DF_syWapiExternalOperationMode_IsActive] DEFAULT ((1))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syWapiExternalOperationMode] ADD CONSTRAINT [PK_syWapiExternalOperationMode_Id] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_syWapiExternalOperationMode_Code] ON [dbo].[syWapiExternalOperationMode] ([Code]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', 'The type of operation that is possible to do with the external source
in Moment
PUSH
PULL', 'SCHEMA', N'dbo', 'TABLE', N'syWapiExternalOperationMode', NULL, NULL
GO
