CREATE TABLE [dbo].[adReqLeadGroups]
(
[ReqLeadGrpId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_adReqLeadGroups_ReqLeadGrpId] DEFAULT (newid()),
[LeadGrpId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[IsRequired] [bit] NULL,
[adReqEffectiveDateId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adReqLeadGroups] ADD CONSTRAINT [PK_adReqLeadGroups_ReqLeadGrpId] PRIMARY KEY CLUSTERED  ([ReqLeadGrpId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adReqLeadGroups] ADD CONSTRAINT [FK_adReqLeadGroups_adLeadGroups_LeadGrpId_LeadGrpId] FOREIGN KEY ([LeadGrpId]) REFERENCES [dbo].[adLeadGroups] ([LeadGrpId])
GO
