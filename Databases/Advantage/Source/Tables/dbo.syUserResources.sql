CREATE TABLE [dbo].[syUserResources]
(
[ResourceId] [int] NOT NULL IDENTITY(1, 1),
[Resource] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CampGrpId] [uniqueidentifier] NULL,
[ResourceTypeId] [tinyint] NOT NULL,
[EntityId] [smallint] NULL,
[Options] [int] NULL,
[IsPublic] [bit] NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Active] [bit] NULL,
[CreatedBy] [uniqueidentifier] NOT NULL CONSTRAINT [DF_syUserResources_CreatedBy] DEFAULT ('00000000-0000-0000-0000-000000000000'),
[UseLeftJoin] [bit] NULL CONSTRAINT [DF_syUserResources_UseLeftJoin] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syUserResources] ADD CONSTRAINT [PK_syUserResources_ResourceId] PRIMARY KEY CLUSTERED  ([ResourceId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syUserResources] ADD CONSTRAINT [FK_syUserResources_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[syUserResources] ADD CONSTRAINT [FK_syUserResources_syResources_EntityId_ResourceID] FOREIGN KEY ([EntityId]) REFERENCES [dbo].[syResources] ([ResourceID])
GO
ALTER TABLE [dbo].[syUserResources] ADD CONSTRAINT [FK_syUserResources_syResourceTypes_ResourceTypeId_ResourceTypeID] FOREIGN KEY ([ResourceTypeId]) REFERENCES [dbo].[syResourceTypes] ([ResourceTypeID])
GO
