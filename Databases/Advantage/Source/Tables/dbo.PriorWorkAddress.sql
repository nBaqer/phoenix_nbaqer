CREATE TABLE [dbo].[PriorWorkAddress]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[StEmploymentId] [uniqueidentifier] NOT NULL,
[Address1] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Apartment] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Address2] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[City] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsInternational] [bit] NOT NULL,
[StateInternational] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_PriorWorkAddress_StateInternational] DEFAULT (''),
[StateId] [uniqueidentifier] NULL,
[ZipCode] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CountryId] [uniqueidentifier] NULL,
[ModUser] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PriorWorkAddress] ADD CONSTRAINT [PK_PriorWorkAddress_Id] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PriorWorkAddress] ADD CONSTRAINT [FK_PriorWorkAddress_adCountries_CountryId_CountryId] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[adCountries] ([CountryId])
GO
ALTER TABLE [dbo].[PriorWorkAddress] ADD CONSTRAINT [FK_PriorWorkAddress_adLeadEmployment_StEmploymentId_StEmploymentId] FOREIGN KEY ([StEmploymentId]) REFERENCES [dbo].[adLeadEmployment] ([StEmploymentId])
GO
ALTER TABLE [dbo].[PriorWorkAddress] ADD CONSTRAINT [FK_PriorWorkAddress_syStates_StateId_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[syStates] ([StateId])
GO
