CREATE TABLE [dbo].[syStateBoardAgencies]
(
[StateBoardAgencyId] [int] NOT NULL IDENTITY(1, 1),
[Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[StateId] [uniqueidentifier] NOT NULL,
[LicensingAddress1] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LicensingAddress2] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LicensingCountryId] [uniqueidentifier] NOT NULL,
[LicensingStateId] [uniqueidentifier] NOT NULL,
[LicensingCity] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LicensingZipCode] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreatedById] [uniqueidentifier] NOT NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syStateBoardAgencies] ADD CONSTRAINT [PK_syStateBoardAgencies_StateBoardAgencyId] PRIMARY KEY CLUSTERED  ([StateBoardAgencyId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syStateBoardAgencies] ADD CONSTRAINT [FK_sySchoolStateBoardReports_adCountries_LicensingCountryId_CountryId] FOREIGN KEY ([LicensingCountryId]) REFERENCES [dbo].[adCountries] ([CountryId])
GO
ALTER TABLE [dbo].[syStateBoardAgencies] ADD CONSTRAINT [FK_sySchoolStateBoardReports_syStates_LicensingStateId_StateId] FOREIGN KEY ([LicensingStateId]) REFERENCES [dbo].[syStates] ([StateId])
GO
ALTER TABLE [dbo].[syStateBoardAgencies] ADD CONSTRAINT [FK_syStateBoardAgencies_syStates_StateId_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[syStates] ([StateId])
GO
ALTER TABLE [dbo].[syStateBoardAgencies] ADD CONSTRAINT [FK_syStateBoardAgencies_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
ALTER TABLE [dbo].[syStateBoardAgencies] ADD CONSTRAINT [FK_syStateBoardAgencies_syUsers_CreatedById_UserId] FOREIGN KEY ([CreatedById]) REFERENCES [dbo].[syUsers] ([UserId])
GO
