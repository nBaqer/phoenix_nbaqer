CREATE TABLE [dbo].[syContactTypes]
(
[ContactTypeId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_syContactTypes_ContactTypeId] DEFAULT (newid()),
[ContactTypeCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ContactTypeDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--==========================================================================================
-- Update Audit History when delete records in syContactTypes
--==========================================================================================
CREATE TRIGGER [dbo].[syContactTypes_Audit_Delete] ON [dbo].[syContactTypes]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syContactTypes','D',@EventRows,@EventDate,@UserName; 
            --
            INSERT  INTO syAuditHistDetail
                    (
                     AuditHistId
                    ,RowId
                    ,ColumnName
                    ,OldValue
                    )
                    SELECT  @AuditHistId
                           ,Old.ContactTypeId
                           ,'ContactTypeCode'
                           ,CONVERT(VARCHAR(8000),Old.ContactTypeCode)
                    FROM    Deleted Old; 
            --
            INSERT  INTO syAuditHistDetail
                    (
                     AuditHistId
                    ,RowId
                    ,ColumnName
                    ,OldValue
                    )
                    SELECT  @AuditHistId
                           ,Old.ContactTypeId
                           ,'ContactTypeDescrip'
                           ,CONVERT(VARCHAR(8000),Old.ContactTypeDescrip)
                    FROM    Deleted Old; 
            --
            INSERT  INTO syAuditHistDetail
                    (
                     AuditHistId
                    ,RowId
                    ,ColumnName
                    ,OldValue
                    )
                    SELECT  @AuditHistId
                           ,Old.ContactTypeId
                           ,'StatusId'
                           ,CONVERT(VARCHAR(8000),Old.StatusId)
                    FROM    Deleted Old; 
            --
            INSERT  INTO syAuditHistDetail
                    (
                     AuditHistId
                    ,RowId
                    ,ColumnName
                    ,OldValue
                    )
                    SELECT  @AuditHistId
                           ,Old.ContactTypeId
                           ,'CampGrpId'
                           ,CONVERT(VARCHAR(8000),Old.CampGrpId)
                    FROM    Deleted Old; 
        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--==========================================================================================
-- Update Audit History when insert in syContactTypes
--==========================================================================================
CREATE TRIGGER [dbo].[syContactTypes_Audit_Insert] ON [dbo].[syContactTypes]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syContactTypes','I',@EventRows,@EventDate,@UserName; 
            --
            INSERT  INTO syAuditHistDetail
                    (
                     AuditHistId
                    ,RowId
                    ,ColumnName
                    ,NewValue
                    )
                    SELECT  @AuditHistId
                           ,New.ContactTypeId
                           ,'ContactTypeCode'
                           ,CONVERT(VARCHAR(8000),New.ContactTypeCode)
                    FROM    Inserted New; 
            --
            INSERT  INTO syAuditHistDetail
                    (
                     AuditHistId
                    ,RowId
                    ,ColumnName
                    ,NewValue
                    )
                    SELECT  @AuditHistId
                           ,New.ContactTypeId
                           ,'ContactTypeDescrip'
                           ,CONVERT(VARCHAR(8000),New.ContactTypeDescrip)
                    FROM    Inserted New; 
            --
            INSERT  INTO syAuditHistDetail
                    (
                     AuditHistId
                    ,RowId
                    ,ColumnName
                    ,NewValue
                    )
                    SELECT  @AuditHistId
                           ,New.ContactTypeId
                           ,'StatusId'
                           ,CONVERT(VARCHAR(8000),New.StatusId)
                    FROM    Inserted New; 
            --
            INSERT  INTO syAuditHistDetail
                    (
                     AuditHistId
                    ,RowId
                    ,ColumnName
                    ,NewValue
                    )
                    SELECT  @AuditHistId
                           ,New.ContactTypeId
                           ,'CampGrpId'
                           ,CONVERT(VARCHAR(8000),New.CampGrpId)
                    FROM    Inserted New; 
            --                         

        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


--==========================================================================================
-- Update Audit History when update fields in syContactTypes
--==========================================================================================
CREATE TRIGGER [dbo].[syContactTypes_Audit_Update] ON [dbo].[syContactTypes]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syContactTypes','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(ContactTypeCode)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.ContactTypeId
                                       ,'ContactTypeCode'
                                       ,CONVERT(VARCHAR(8000),Old.ContactTypeCode)
                                       ,CONVERT(VARCHAR(8000),New.ContactTypeCode)
                                FROM    Inserted New
                                FULL OUTER JOIN Deleted Old ON Old.ContactTypeId = New.ContactTypeId
                                WHERE   Old.ContactTypeCode <> New.ContactTypeCode
                                        OR (
                                             Old.ContactTypeCode IS NULL
                                             AND New.ContactTypeCode IS NOT NULL
                                           )
                                        OR (
                                             New.ContactTypeCode IS NULL
                                             AND Old.ContactTypeCode IS NOT NULL
                                           );
                    END;            
                --
                IF UPDATE(ContactTypeDescrip)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.ContactTypeId
                                       ,'ContactTypeDescrip'
                                       ,CONVERT(VARCHAR(8000),Old.ContactTypeDescrip)
                                       ,CONVERT(VARCHAR(8000),New.ContactTypeDescrip)
                                FROM    Inserted New
                                FULL OUTER JOIN Deleted Old ON Old.ContactTypeId = New.ContactTypeId
                                WHERE   Old.ContactTypeDescrip <> New.ContactTypeDescrip
                                        OR (
                                             Old.ContactTypeDescrip IS NULL
                                             AND New.ContactTypeDescrip IS NOT NULL
                                           )
                                        OR (
                                             New.ContactTypeDescrip IS NULL
                                             AND Old.ContactTypeDescrip IS NOT NULL
                                           );
                    END;            
                --
                IF UPDATE(StatusId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.ContactTypeId
                                       ,'StatusId'
                                       ,CONVERT(VARCHAR(8000),Old.StatusId)
                                       ,CONVERT(VARCHAR(8000),New.StatusId)
                                FROM    Inserted New
                                FULL OUTER JOIN Deleted Old ON Old.ContactTypeId = New.ContactTypeId
                                WHERE   Old.StatusId <> New.StatusId
                                        OR (
                                             Old.StatusId IS NULL
                                             AND New.StatusId IS NOT NULL
                                           )
                                        OR (
                                             New.StatusId IS NULL
                                             AND Old.StatusId IS NOT NULL
                                           );
                    END;            
                --
                IF UPDATE(CampGrpId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.ContactTypeId
                                       ,'CampGrpId'
                                       ,CONVERT(VARCHAR(8000),Old.CampGrpId)
                                       ,CONVERT(VARCHAR(8000),New.CampGrpId)
                                FROM    Inserted New
                                FULL OUTER JOIN Deleted Old ON Old.ContactTypeId = New.ContactTypeId
                                WHERE   Old.CampGrpId <> New.CampGrpId
                                        OR (
                                             Old.CampGrpId IS NULL
                                             AND New.CampGrpId IS NOT NULL
                                           )
                                        OR (
                                             New.CampGrpId IS NULL
                                             AND Old.CampGrpId IS NOT NULL
                                           );
                    END;            
                --
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[syContactTypes] ADD CONSTRAINT [PK_syContactTypes_ContactTypeId] PRIMARY KEY CLUSTERED  ([ContactTypeId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syContactTypes] ADD CONSTRAINT [FK_syContactTypes_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[syContactTypes] ADD CONSTRAINT [FK_syContactTypes_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
