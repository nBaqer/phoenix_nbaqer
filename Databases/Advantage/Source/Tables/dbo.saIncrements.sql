CREATE TABLE [dbo].[saIncrements]
(
[IncrementId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_saIncrements_IncrementId] DEFAULT (newid()),
[BillingMethodId] [uniqueidentifier] NOT NULL,
[IncrementType] [int] NOT NULL,
[IncrementName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EffectiveDate] [datetime] NOT NULL CONSTRAINT [DF_saIncrements_EffectiveDate] DEFAULT (getdate()),
[ExcAbscenesPercent] [decimal] (6, 4) NOT NULL,
[ModUser] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL CONSTRAINT [DF_saIncrements_ModDate] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saIncrements] ADD CONSTRAINT [PK_saIncrements_IncrementId] PRIMARY KEY CLUSTERED  ([IncrementId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_saIncrements_BillingMethodId] ON [dbo].[saIncrements] ([BillingMethodId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saIncrements] ADD CONSTRAINT [FK_saIncrements_saBillingMethods_BillingMethodId_BillingMethodId] FOREIGN KEY ([BillingMethodId]) REFERENCES [dbo].[saBillingMethods] ([BillingMethodId])
GO
