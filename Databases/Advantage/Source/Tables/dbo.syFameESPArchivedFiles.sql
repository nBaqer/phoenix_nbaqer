CREATE TABLE [dbo].[syFameESPArchivedFiles]
(
[ArchiveId] [uniqueidentifier] NOT NULL,
[FileName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsSuccessfullyProcessed] [bit] NULL CONSTRAINT [DF_syFameESPArchivedFiles_IsSuccessfullyProcessed] DEFAULT ((0)),
[moduser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[moddate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syFameESPArchivedFiles] ADD CONSTRAINT [PK_syFameESPArchivedFiles_ArchiveId] PRIMARY KEY CLUSTERED  ([ArchiveId]) ON [PRIMARY]
GO
