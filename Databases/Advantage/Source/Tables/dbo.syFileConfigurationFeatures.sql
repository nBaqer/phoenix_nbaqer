CREATE TABLE [dbo].[syFileConfigurationFeatures]
(
[FeatureId] [int] NOT NULL,
[FeatureCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FeatureDescription] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syFileConfigurationFeatures] ADD CONSTRAINT [PK_syFileConfigurationFeatures_FeatureId] PRIMARY KEY CLUSTERED  ([FeatureId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syFileConfigurationFeatures] ADD CONSTRAINT [FK_syFileConfigurationFeatures_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
