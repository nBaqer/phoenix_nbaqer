CREATE TABLE [dbo].[hrEmpHRInfo]
(
[EmpHRInfoId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_hrEmpHRInfo_EmpHRInfoId] DEFAULT (newid()),
[EmpId] [uniqueidentifier] NOT NULL,
[HireDate] [smalldatetime] NULL,
[PositionId] [uniqueidentifier] NULL,
[DepartmentId] [uniqueidentifier] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[hrEmpHRInfo_Audit_Delete] ON [dbo].[hrEmpHRInfo]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'hrEmpHRInfo','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmpHRInfoId
                               ,'EmpId'
                               ,CONVERT(VARCHAR(8000),Old.EmpId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmpHRInfoId
                               ,'PositionId'
                               ,CONVERT(VARCHAR(8000),Old.PositionId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmpHRInfoId
                               ,'DepartmentId'
                               ,CONVERT(VARCHAR(8000),Old.DepartmentId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmpHRInfoId
                               ,'HireDate'
                               ,CONVERT(VARCHAR(8000),Old.HireDate,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[hrEmpHRInfo_Audit_Insert] ON [dbo].[hrEmpHRInfo]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'hrEmpHRInfo','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmpHRInfoId
                               ,'EmpId'
                               ,CONVERT(VARCHAR(8000),New.EmpId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmpHRInfoId
                               ,'PositionId'
                               ,CONVERT(VARCHAR(8000),New.PositionId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmpHRInfoId
                               ,'DepartmentId'
                               ,CONVERT(VARCHAR(8000),New.DepartmentId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmpHRInfoId
                               ,'HireDate'
                               ,CONVERT(VARCHAR(8000),New.HireDate,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[hrEmpHRInfo_Audit_Update] ON [dbo].[hrEmpHRInfo]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'hrEmpHRInfo','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(EmpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmpHRInfoId
                                   ,'EmpId'
                                   ,CONVERT(VARCHAR(8000),Old.EmpId,121)
                                   ,CONVERT(VARCHAR(8000),New.EmpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmpHRInfoId = New.EmpHRInfoId
                            WHERE   Old.EmpId <> New.EmpId
                                    OR (
                                         Old.EmpId IS NULL
                                         AND New.EmpId IS NOT NULL
                                       )
                                    OR (
                                         New.EmpId IS NULL
                                         AND Old.EmpId IS NOT NULL
                                       ); 
                IF UPDATE(PositionId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmpHRInfoId
                                   ,'PositionId'
                                   ,CONVERT(VARCHAR(8000),Old.PositionId,121)
                                   ,CONVERT(VARCHAR(8000),New.PositionId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmpHRInfoId = New.EmpHRInfoId
                            WHERE   Old.PositionId <> New.PositionId
                                    OR (
                                         Old.PositionId IS NULL
                                         AND New.PositionId IS NOT NULL
                                       )
                                    OR (
                                         New.PositionId IS NULL
                                         AND Old.PositionId IS NOT NULL
                                       ); 
                IF UPDATE(DepartmentId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmpHRInfoId
                                   ,'DepartmentId'
                                   ,CONVERT(VARCHAR(8000),Old.DepartmentId,121)
                                   ,CONVERT(VARCHAR(8000),New.DepartmentId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmpHRInfoId = New.EmpHRInfoId
                            WHERE   Old.DepartmentId <> New.DepartmentId
                                    OR (
                                         Old.DepartmentId IS NULL
                                         AND New.DepartmentId IS NOT NULL
                                       )
                                    OR (
                                         New.DepartmentId IS NULL
                                         AND Old.DepartmentId IS NOT NULL
                                       ); 
                IF UPDATE(HireDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmpHRInfoId
                                   ,'HireDate'
                                   ,CONVERT(VARCHAR(8000),Old.HireDate,121)
                                   ,CONVERT(VARCHAR(8000),New.HireDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmpHRInfoId = New.EmpHRInfoId
                            WHERE   Old.HireDate <> New.HireDate
                                    OR (
                                         Old.HireDate IS NULL
                                         AND New.HireDate IS NOT NULL
                                       )
                                    OR (
                                         New.HireDate IS NULL
                                         AND Old.HireDate IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[hrEmpHRInfo] ADD CONSTRAINT [PK_hrEmpHRInfo_EmpHRInfoId] PRIMARY KEY CLUSTERED  ([EmpHRInfoId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_hrEmpHRInfo_EmpHRInfoId] ON [dbo].[hrEmpHRInfo] ([EmpHRInfoId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[hrEmpHRInfo] ADD CONSTRAINT [FK_hrEmpHRInfo_syHRDepartments_DepartmentId_HRDepartmentId] FOREIGN KEY ([DepartmentId]) REFERENCES [dbo].[syHRDepartments] ([HRDepartmentId])
GO
ALTER TABLE [dbo].[hrEmpHRInfo] ADD CONSTRAINT [FK_hrEmpHRInfo_hrEmployees_EmpId_EmpId] FOREIGN KEY ([EmpId]) REFERENCES [dbo].[hrEmployees] ([EmpId])
GO
ALTER TABLE [dbo].[hrEmpHRInfo] ADD CONSTRAINT [FK_hrEmpHRInfo_syPositions_PositionId_PositionId] FOREIGN KEY ([PositionId]) REFERENCES [dbo].[syPositions] ([PositionId])
GO
