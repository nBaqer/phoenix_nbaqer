CREATE TABLE [dbo].[sySdfValList]
(
[SdfListId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_sySdfValList_SdfListId] DEFAULT (newid()),
[SDFId] [uniqueidentifier] NOT NULL,
[ValList] [varchar] (5000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[sySdfValList] ADD CONSTRAINT [PK_sySdfValList_SdfListId] PRIMARY KEY CLUSTERED  ([SdfListId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[sySdfValList] ADD CONSTRAINT [FK_sySdfValList_sySDF_SDFId_SDFId] FOREIGN KEY ([SDFId]) REFERENCES [dbo].[sySDF] ([SDFId]) ON DELETE CASCADE
GO
