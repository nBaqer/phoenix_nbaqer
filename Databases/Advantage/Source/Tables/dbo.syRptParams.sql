CREATE TABLE [dbo].[syRptParams]
(
[RptParamId] [smallint] NOT NULL,
[ResourceId] [smallint] NOT NULL,
[TblFldsId] [int] NOT NULL,
[Required] [bit] NOT NULL,
[SortSec] [bit] NOT NULL,
[FilterListSec] [bit] NOT NULL,
[FilterOtherSec] [bit] NOT NULL,
[RptCaption] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syRptParams] ADD CONSTRAINT [PK_syRptParams_RptParamId] PRIMARY KEY CLUSTERED  ([RptParamId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syRptParams] ADD CONSTRAINT [FK_syRptParams_syResources_ResourceId_ResourceID] FOREIGN KEY ([ResourceId]) REFERENCES [dbo].[syResources] ([ResourceID])
GO
ALTER TABLE [dbo].[syRptParams] ADD CONSTRAINT [FK_syRptParams_syTblFlds_TblFldsId_TblFldsId] FOREIGN KEY ([TblFldsId]) REFERENCES [dbo].[syTblFlds] ([TblFldsId])
GO
GRANT SELECT ON  [dbo].[syRptParams] TO [AdvantageRole]
GO
