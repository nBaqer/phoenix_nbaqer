CREATE TABLE [dbo].[syRlsResLvls]
(
[RRLId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syRlsResLvls_RRLId] DEFAULT (newid()),
[RoleId] [uniqueidentifier] NOT NULL,
[ResourceID] [smallint] NOT NULL,
[AccessLevel] [smallint] NOT NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ParentId] [int] NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[syRlsResLvls_Audit_Delete] ON [dbo].[syRlsResLvls]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syRlsResLvls','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.RRLId
                               ,'ResourceID'
                               ,CONVERT(VARCHAR(8000),Old.ResourceID,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.RRLId
                               ,'AccessLevel'
                               ,CONVERT(VARCHAR(8000),Old.AccessLevel,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.RRLId
                               ,'RoleId'
                               ,CONVERT(VARCHAR(8000),Old.RoleId,121)
                        FROM    Deleted Old; 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[syRlsResLvls_Audit_Insert] ON [dbo].[syRlsResLvls]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syRlsResLvls','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.RRLId
                               ,'ResourceID'
                               ,CONVERT(VARCHAR(8000),New.ResourceID,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.RRLId
                               ,'AccessLevel'
                               ,CONVERT(VARCHAR(8000),New.AccessLevel,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.RRLId
                               ,'RoleId'
                               ,CONVERT(VARCHAR(8000),New.RoleId,121)
                        FROM    Inserted New; 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[syRlsResLvls_Audit_Update] ON [dbo].[syRlsResLvls]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syRlsResLvls','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(ResourceId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.RRLId
                                   ,'ResourceID'
                                   ,CONVERT(VARCHAR(8000),Old.ResourceID,121)
                                   ,CONVERT(VARCHAR(8000),New.ResourceID,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.RRLId = New.RRLId
                            WHERE   Old.ResourceID <> New.ResourceID
                                    OR (
                                         Old.ResourceID IS NULL
                                         AND New.ResourceID IS NOT NULL
                                       )
                                    OR (
                                         New.ResourceID IS NULL
                                         AND Old.ResourceID IS NOT NULL
                                       ); 
                IF UPDATE(AccessLevel)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.RRLId
                                   ,'AccessLevel'
                                   ,CONVERT(VARCHAR(8000),Old.AccessLevel,121)
                                   ,CONVERT(VARCHAR(8000),New.AccessLevel,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.RRLId = New.RRLId
                            WHERE   Old.AccessLevel <> New.AccessLevel
                                    OR (
                                         Old.AccessLevel IS NULL
                                         AND New.AccessLevel IS NOT NULL
                                       )
                                    OR (
                                         New.AccessLevel IS NULL
                                         AND Old.AccessLevel IS NOT NULL
                                       ); 
                IF UPDATE(RoleId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.RRLId
                                   ,'RoleId'
                                   ,CONVERT(VARCHAR(8000),Old.RoleId,121)
                                   ,CONVERT(VARCHAR(8000),New.RoleId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.RRLId = New.RRLId
                            WHERE   Old.RoleId <> New.RoleId
                                    OR (
                                         Old.RoleId IS NULL
                                         AND New.RoleId IS NOT NULL
                                       )
                                    OR (
                                         New.RoleId IS NULL
                                         AND Old.RoleId IS NOT NULL
                                       ); 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[syRlsResLvls] ADD CONSTRAINT [PK_syRlsResLvls_RRLId] PRIMARY KEY CLUSTERED  ([RRLId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_syRlsResLvls_RoleId_ResourceID_AccessLevel_ParentId] ON [dbo].[syRlsResLvls] ([RoleId], [ResourceID], [AccessLevel], [ParentId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syRlsResLvls] ADD CONSTRAINT [FK_syRlsResLvls_syResources_ResourceID_ResourceID] FOREIGN KEY ([ResourceID]) REFERENCES [dbo].[syResources] ([ResourceID])
GO
ALTER TABLE [dbo].[syRlsResLvls] ADD CONSTRAINT [FK_syRlsResLvls_syRoles_RoleId_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[syRoles] ([RoleId])
GO
GRANT SELECT ON  [dbo].[syRlsResLvls] TO [AdvantageRole]
GO
