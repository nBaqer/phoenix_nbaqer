CREATE TABLE [dbo].[syGenerateStudentSeq]
(
[Student_SeqId] [int] NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GenerateStudentSeqId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_syGenerateStudentSeq_GenerateStudentSeqId] DEFAULT (newsequentialid())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syGenerateStudentSeq] ADD CONSTRAINT [PK_syGenerateStudentSeq_GenerateStudentSeqId] PRIMARY KEY CLUSTERED  ([GenerateStudentSeqId]) ON [PRIMARY]
GO
