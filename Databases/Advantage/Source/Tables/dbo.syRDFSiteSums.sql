CREATE TABLE [dbo].[syRDFSiteSums]
(
[RDFSiteSumId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syRDFSiteSums_RDFSiteSumId] DEFAULT (newid()),
[ModuleId] [int] NOT NULL,
[RDFSiteSumCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[RDFSiteSumDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[syRDFSiteSums_Audit_Delete] ON [dbo].[syRDFSiteSums]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syRDFSiteSums','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.RDFSiteSumId
                               ,'ModuleId'
                               ,CONVERT(VARCHAR(8000),Old.ModuleId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.RDFSiteSumId
                               ,'RDFSiteSumCode'
                               ,CONVERT(VARCHAR(8000),Old.RDFSiteSumCode,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.RDFSiteSumId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.RDFSiteSumId
                               ,'RDFSiteSumDescrip'
                               ,CONVERT(VARCHAR(8000),Old.RDFSiteSumDescrip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.RDFSiteSumId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[syRDFSiteSums_Audit_Insert] ON [dbo].[syRDFSiteSums]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syRDFSiteSums','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.RDFSiteSumId
                               ,'ModuleId'
                               ,CONVERT(VARCHAR(8000),New.ModuleId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.RDFSiteSumId
                               ,'RDFSiteSumCode'
                               ,CONVERT(VARCHAR(8000),New.RDFSiteSumCode,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.RDFSiteSumId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.RDFSiteSumId
                               ,'RDFSiteSumDescrip'
                               ,CONVERT(VARCHAR(8000),New.RDFSiteSumDescrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.RDFSiteSumId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[syRDFSiteSums_Audit_Update] ON [dbo].[syRDFSiteSums]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syRDFSiteSums','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(ModuleID)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.RDFSiteSumId
                                   ,'ModuleId'
                                   ,CONVERT(VARCHAR(8000),Old.ModuleId,121)
                                   ,CONVERT(VARCHAR(8000),New.ModuleId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.RDFSiteSumId = New.RDFSiteSumId
                            WHERE   Old.ModuleId <> New.ModuleId
                                    OR (
                                         Old.ModuleId IS NULL
                                         AND New.ModuleId IS NOT NULL
                                       )
                                    OR (
                                         New.ModuleId IS NULL
                                         AND Old.ModuleId IS NOT NULL
                                       ); 
                IF UPDATE(RDFSiteSumCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.RDFSiteSumId
                                   ,'RDFSiteSumCode'
                                   ,CONVERT(VARCHAR(8000),Old.RDFSiteSumCode,121)
                                   ,CONVERT(VARCHAR(8000),New.RDFSiteSumCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.RDFSiteSumId = New.RDFSiteSumId
                            WHERE   Old.RDFSiteSumCode <> New.RDFSiteSumCode
                                    OR (
                                         Old.RDFSiteSumCode IS NULL
                                         AND New.RDFSiteSumCode IS NOT NULL
                                       )
                                    OR (
                                         New.RDFSiteSumCode IS NULL
                                         AND Old.RDFSiteSumCode IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.RDFSiteSumId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.RDFSiteSumId = New.RDFSiteSumId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(RDFSiteSumDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.RDFSiteSumId
                                   ,'RDFSiteSumDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.RDFSiteSumDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.RDFSiteSumDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.RDFSiteSumId = New.RDFSiteSumId
                            WHERE   Old.RDFSiteSumDescrip <> New.RDFSiteSumDescrip
                                    OR (
                                         Old.RDFSiteSumDescrip IS NULL
                                         AND New.RDFSiteSumDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.RDFSiteSumDescrip IS NULL
                                         AND Old.RDFSiteSumDescrip IS NOT NULL
                                       ); 
                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.RDFSiteSumId
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.RDFSiteSumId = New.RDFSiteSumId
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[syRDFSiteSums] ADD CONSTRAINT [PK_syRDFSiteSums_RDFSiteSumId] PRIMARY KEY CLUSTERED  ([RDFSiteSumId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syRDFSiteSums] ADD CONSTRAINT [FK_syRDFSiteSums_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[syRDFSiteSums] ADD CONSTRAINT [FK_syRDFSiteSums_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
