CREATE TABLE [dbo].[adExtraCurricular]
(
[IdExtraCurricular] [int] NOT NULL IDENTITY(1, 1),
[LeadId] [uniqueidentifier] NOT NULL,
[LevelId] [int] NOT NULL,
[ExtraCurrGrpId] [uniqueidentifier] NOT NULL,
[ExtraCurrDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExtraCurrComment] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_adExtraCurricular_ExtraCurrComment] DEFAULT (''),
[ModDate] [datetime] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adExtraCurricular] ADD CONSTRAINT [PK_adExtraCurricular_IdExtraCurricular] PRIMARY KEY CLUSTERED  ([IdExtraCurricular]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adExtraCurricular] ADD CONSTRAINT [FK_adExtraCurricular_adExtraCurrGrp_ExtraCurrGrpId_ExtraCurrGrpId] FOREIGN KEY ([ExtraCurrGrpId]) REFERENCES [dbo].[adExtraCurrGrp] ([ExtraCurrGrpId])
GO
ALTER TABLE [dbo].[adExtraCurricular] ADD CONSTRAINT [FK_adExtraCurricular_adLeads_LeadId_LeadId] FOREIGN KEY ([LeadId]) REFERENCES [dbo].[adLeads] ([LeadId])
GO
ALTER TABLE [dbo].[adExtraCurricular] ADD CONSTRAINT [FK_adExtraCurricular_adLevel_LevelId_LevelId] FOREIGN KEY ([LevelId]) REFERENCES [dbo].[adLevel] ([LevelId])
GO
