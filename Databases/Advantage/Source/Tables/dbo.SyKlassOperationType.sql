CREATE TABLE [dbo].[syKlassOperationType]
(
[KlassOperationTypeId] [int] NOT NULL,
[Code] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_syKlassOperationType_Description] DEFAULT ('')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syKlassOperationType] ADD CONSTRAINT [PK_syKlassOperationType_KlassOperationTypeId] PRIMARY KEY CLUSTERED  ([KlassOperationTypeId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syKlassOperationType] ADD CONSTRAINT [UIX_syKlassOperationType_Code] UNIQUE NONCLUSTERED  ([Code]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', 'This table catalog is for internal use. They define the type of operation. The type of operation define the table where the configuration parameter is.', 'SCHEMA', N'dbo', 'TABLE', N'syKlassOperationType', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_Description', 'This is for internal use   	- programVersion 	- StatusStudent 	- Campus', 'SCHEMA', N'dbo', 'TABLE', N'syKlassOperationType', 'COLUMN', N'Code'
GO
