CREATE TABLE [dbo].[syConfigAppSetValues]
(
[ValueId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_syConfigAppSetValues_ValueId] DEFAULT (newsequentialid()),
[SettingId] [int] NULL,
[CampusId] [uniqueidentifier] NULL,
[Value] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[Active] [bit] NOT NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE TRIGGER [dbo].[syConfigAppSetValues_Audit_Delete] ON [dbo].[syConfigAppSetValues]
    FOR DELETE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syConfigAppSetValues','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ValueId
                               ,'ValueId'
                               ,CONVERT(VARCHAR(8000),Old.ValueId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ValueId
                               ,'SettingId'
                               ,CONVERT(VARCHAR(8000),Old.SettingId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ValueId
                               ,'CampusId'
                               ,CONVERT(VARCHAR(8000),Old.CampusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ValueId
                               ,'Value'
                               ,CONVERT(VARCHAR(8000),Old.Value,121)
                        FROM    Deleted Old; 
            END; 
        END;



    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE TRIGGER [dbo].[syConfigAppSetValues_Audit_Insert] ON [dbo].[syConfigAppSetValues]
    FOR INSERT
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
--DECLARE @AppName as varchar(50)
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    );
--SET @AppName = 'Manage Configuration Settings' 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syConfigAppSetValues','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ValueId
                               ,'ValueId'
                               ,CONVERT(VARCHAR(8000),New.ValueId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ValueId
                               ,'SettingId'
                               ,CONVERT(VARCHAR(8000),New.SettingId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ValueId
                               ,'CampusId'
                               ,CONVERT(VARCHAR(8000),New.CampusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ValueId
                               ,'Value'
                               ,CONVERT(VARCHAR(8000),New.Value,121)
                        FROM    Inserted New; 
            END; 
        END;



    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[syConfigAppSetValues_Audit_Update] ON [dbo].[syConfigAppSetValues]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syConfigAppSetValues','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(CampusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ValueId
                                   ,'CampusId'
                                   ,CONVERT(VARCHAR(8000),Old.CampusId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ValueId = New.ValueId
                            WHERE   Old.CampusId <> New.CampusId
                                    OR (
                                         Old.CampusId IS NULL
                                         AND New.CampusId IS NOT NULL
                                       )
                                    OR (
                                         New.CampusId IS NULL
                                         AND Old.CampusId IS NOT NULL
                                       ); 
                IF UPDATE(value)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ValueId
                                   ,'Value'
                                   ,CONVERT(VARCHAR(8000),Old.Value,121)
                                   ,CONVERT(VARCHAR(8000),New.Value,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ValueId = New.ValueId
                            WHERE   Old.Value <> New.Value
                                    OR (
                                         Old.Value IS NULL
                                         AND New.Value IS NOT NULL
                                       )
                                    OR (
                                         New.Value IS NULL
                                         AND Old.Value IS NOT NULL
                                       ); 
            END; 
        END;
    SET NOCOUNT OFF; 

    SET NOCOUNT OFF; 
GO
ALTER TABLE [dbo].[syConfigAppSetValues] ADD CONSTRAINT [PK_syConfigAppSetValues_ValueId] PRIMARY KEY CLUSTERED  ([ValueId]) ON [PRIMARY]
GO
