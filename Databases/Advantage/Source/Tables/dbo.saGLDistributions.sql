CREATE TABLE [dbo].[saGLDistributions]
(
[GLDistributionId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_saGLDistributions_GLDistributionId] DEFAULT (newid()),
[GLDate] [datetime] NOT NULL,
[GLAmount] [decimal] (10, 2) NOT NULL,
[GLAccountId] [uniqueidentifier] NOT NULL,
[GLDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ViewOrder] [int] NOT NULL CONSTRAINT [DF_saGLDistributions_ViewOrder] DEFAULT ((0)),
[ModUser] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[TransactionId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saGLDistributions_Audit_Delete] ON [dbo].[saGLDistributions]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saGLDistributions','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.GLDistributionId
                               ,'ViewOrder'
                               ,CONVERT(VARCHAR(8000),Old.ViewOrder,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.GLDistributionId
                               ,'TransactionId'
                               ,CONVERT(VARCHAR(8000),Old.TransactionId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.GLDistributionId
                               ,'GLAccountId'
                               ,CONVERT(VARCHAR(8000),Old.GLAccountId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.GLDistributionId
                               ,'GLDate'
                               ,CONVERT(VARCHAR(8000),Old.GLDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.GLDistributionId
                               ,'GLDescrip'
                               ,CONVERT(VARCHAR(8000),Old.GLDescrip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.GLDistributionId
                               ,'GLAmount'
                               ,CONVERT(VARCHAR(8000),Old.GLAmount,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saGLDistributions_Audit_Insert] ON [dbo].[saGLDistributions]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saGLDistributions','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.GLDistributionId
                               ,'ViewOrder'
                               ,CONVERT(VARCHAR(8000),New.ViewOrder,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.GLDistributionId
                               ,'TransactionId'
                               ,CONVERT(VARCHAR(8000),New.TransactionId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.GLDistributionId
                               ,'GLAccountId'
                               ,CONVERT(VARCHAR(8000),New.GLAccountId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.GLDistributionId
                               ,'GLDate'
                               ,CONVERT(VARCHAR(8000),New.GLDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.GLDistributionId
                               ,'GLDescrip'
                               ,CONVERT(VARCHAR(8000),New.GLDescrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.GLDistributionId
                               ,'GLAmount'
                               ,CONVERT(VARCHAR(8000),New.GLAmount,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO
GO

GO

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saGLDistributions_Audit_Update] ON [dbo].[saGLDistributions]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saGLDistributions','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(ViewOrder)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.GLDistributionId
                                   ,'ViewOrder'
                                   ,CONVERT(VARCHAR(8000),Old.ViewOrder,121)
                                   ,CONVERT(VARCHAR(8000),New.ViewOrder,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.GLDistributionId = New.GLDistributionId
                            WHERE   Old.ViewOrder <> New.ViewOrder
                                    OR (
                                         Old.ViewOrder IS NULL
                                         AND New.ViewOrder IS NOT NULL
                                       )
                                    OR (
                                         New.ViewOrder IS NULL
                                         AND Old.ViewOrder IS NOT NULL
                                       ); 
                IF UPDATE(TransactionId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.GLDistributionId
                                   ,'TransactionId'
                                   ,CONVERT(VARCHAR(8000),Old.TransactionId,121)
                                   ,CONVERT(VARCHAR(8000),New.TransactionId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.GLDistributionId = New.GLDistributionId
                            WHERE   Old.TransactionId <> New.TransactionId
                                    OR (
                                         Old.TransactionId IS NULL
                                         AND New.TransactionId IS NOT NULL
                                       )
                                    OR (
                                         New.TransactionId IS NULL
                                         AND Old.TransactionId IS NOT NULL
                                       ); 
                IF UPDATE(GLAccountId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.GLDistributionId
                                   ,'GLAccountId'
                                   ,CONVERT(VARCHAR(8000),Old.GLAccountId,121)
                                   ,CONVERT(VARCHAR(8000),New.GLAccountId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.GLDistributionId = New.GLDistributionId
                            WHERE   Old.GLAccountId <> New.GLAccountId
                                    OR (
                                         Old.GLAccountId IS NULL
                                         AND New.GLAccountId IS NOT NULL
                                       )
                                    OR (
                                         New.GLAccountId IS NULL
                                         AND Old.GLAccountId IS NOT NULL
                                       ); 
                IF UPDATE(GLDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.GLDistributionId
                                   ,'GLDate'
                                   ,CONVERT(VARCHAR(8000),Old.GLDate,121)
                                   ,CONVERT(VARCHAR(8000),New.GLDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.GLDistributionId = New.GLDistributionId
                            WHERE   Old.GLDate <> New.GLDate
                                    OR (
                                         Old.GLDate IS NULL
                                         AND New.GLDate IS NOT NULL
                                       )
                                    OR (
                                         New.GLDate IS NULL
                                         AND Old.GLDate IS NOT NULL
                                       ); 
                IF UPDATE(GLDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.GLDistributionId
                                   ,'GLDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.GLDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.GLDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.GLDistributionId = New.GLDistributionId
                            WHERE   Old.GLDescrip <> New.GLDescrip
                                    OR (
                                         Old.GLDescrip IS NULL
                                         AND New.GLDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.GLDescrip IS NULL
                                         AND Old.GLDescrip IS NOT NULL
                                       ); 
                IF UPDATE(GLAmount)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.GLDistributionId
                                   ,'GLAmount'
                                   ,CONVERT(VARCHAR(8000),Old.GLAmount,121)
                                   ,CONVERT(VARCHAR(8000),New.GLAmount,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.GLDistributionId = New.GLDistributionId
                            WHERE   Old.GLAmount <> New.GLAmount
                                    OR (
                                         Old.GLAmount IS NULL
                                         AND New.GLAmount IS NOT NULL
                                       )
                                    OR (
                                         New.GLAmount IS NULL
                                         AND Old.GLAmount IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[saGLDistributions] ADD CONSTRAINT [PK_saGLDistributions_GLDistributionId] PRIMARY KEY CLUSTERED  ([GLDistributionId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saGLDistributions] ADD CONSTRAINT [FK_saGLDistributions_saGLAccounts_GLAccountId_GLAccountId] FOREIGN KEY ([GLAccountId]) REFERENCES [dbo].[saGLAccounts] ([GLAccountId])
GO
ALTER TABLE [dbo].[saGLDistributions] ADD CONSTRAINT [FK_saGLDistributions_saTransactions_TransactionId_TransactionId] FOREIGN KEY ([TransactionId]) REFERENCES [dbo].[saTransactions] ([TransactionId])
GO
