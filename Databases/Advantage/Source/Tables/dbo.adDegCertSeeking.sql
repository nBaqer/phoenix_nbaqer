CREATE TABLE [dbo].[adDegCertSeeking]
(
[DegCertSeekingId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_adDegCertSeeking_DegCertSeekingId] DEFAULT (newid()),
[Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IPEDSSequence] [int] NULL,
[IPEDSValue] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adDegCertSeeking] ADD CONSTRAINT [PK_adDegCertSeeking_DegCertSeekingId] PRIMARY KEY CLUSTERED  ([DegCertSeekingId]) ON [PRIMARY]
GO
