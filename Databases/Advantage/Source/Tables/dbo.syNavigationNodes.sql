CREATE TABLE [dbo].[syNavigationNodes]
(
[HierarchyId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syNavigationNodes_HierarchyId] DEFAULT (newid()),
[HierarchyIndex] [smallint] NOT NULL,
[ResourceId] [smallint] NULL,
[ParentId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL CONSTRAINT [DF_syNavigationNodes_ModDate] DEFAULT (getdate()),
[IsPopupWindow] [bit] NULL CONSTRAINT [DF_syNavigationNodes_IsPopupWindow] DEFAULT ((0)),
[IsShipped] [bit] NULL CONSTRAINT [DF_syNavigationNodes_IsShipped] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syNavigationNodes] ADD CONSTRAINT [PK_syNavigationNodes_HierarchyId] PRIMARY KEY CLUSTERED  ([HierarchyId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_syNavigationNodes_HierarchyId] ON [dbo].[syNavigationNodes] ([HierarchyId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syNavigationNodes] ADD CONSTRAINT [FK_syNavigationNodes_syNavigationNodes_ParentId_HierarchyId] FOREIGN KEY ([ParentId]) REFERENCES [dbo].[syNavigationNodes] ([HierarchyId])
GO
ALTER TABLE [dbo].[syNavigationNodes] ADD CONSTRAINT [FK_syNavigationNodes_syResources_ResourceId_ResourceID] FOREIGN KEY ([ResourceId]) REFERENCES [dbo].[syResources] ([ResourceID])
GO
GRANT SELECT ON  [dbo].[syNavigationNodes] TO [AdvantageRole]
GO
