CREATE TABLE [dbo].[saPayments]
(
[TransactionId] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[PaymentTypeId] [int] NOT NULL CONSTRAINT [DF_saPayments_PaymentTypeId] DEFAULT ((0)),
[CheckNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScheduledPayment] [bit] NOT NULL CONSTRAINT [DF_saPayments_ScheduledPayment] DEFAULT ((0)),
[BankAcctId] [uniqueidentifier] NULL,
[IsDeposited] [bit] NOT NULL CONSTRAINT [DF_saPayments_IsDeposited] DEFAULT ((0)),
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saPayments_Audit_Delete] ON [dbo].[saPayments]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saPayments','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'CheckNumber'
                               ,CONVERT(VARCHAR(8000),Old.CheckNumber,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'ScheduledPayment'
                               ,CONVERT(VARCHAR(8000),Old.ScheduledPayment,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'IsDeposited'
                               ,CONVERT(VARCHAR(8000),Old.IsDeposited,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'BankAcctId'
                               ,CONVERT(VARCHAR(8000),Old.BankAcctId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'PaymentTypeId'
                               ,CONVERT(VARCHAR(8000),Old.PaymentTypeId,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saPayments_Audit_Insert] ON [dbo].[saPayments]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saPayments','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'CheckNumber'
                               ,CONVERT(VARCHAR(8000),New.CheckNumber,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'ScheduledPayment'
                               ,CONVERT(VARCHAR(8000),New.ScheduledPayment,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'IsDeposited'
                               ,CONVERT(VARCHAR(8000),New.IsDeposited,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'BankAcctId'
                               ,CONVERT(VARCHAR(8000),New.BankAcctId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'PaymentTypeId'
                               ,CONVERT(VARCHAR(8000),New.PaymentTypeId,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saPayments_Audit_Update] ON [dbo].[saPayments]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saPayments','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(CheckNumber)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TransactionId
                                   ,'CheckNumber'
                                   ,CONVERT(VARCHAR(8000),Old.CheckNumber,121)
                                   ,CONVERT(VARCHAR(8000),New.CheckNumber,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TransactionId = New.TransactionId
                            WHERE   Old.CheckNumber <> New.CheckNumber
                                    OR (
                                         Old.CheckNumber IS NULL
                                         AND New.CheckNumber IS NOT NULL
                                       )
                                    OR (
                                         New.CheckNumber IS NULL
                                         AND Old.CheckNumber IS NOT NULL
                                       ); 
                IF UPDATE(ScheduledPayment)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TransactionId
                                   ,'ScheduledPayment'
                                   ,CONVERT(VARCHAR(8000),Old.ScheduledPayment,121)
                                   ,CONVERT(VARCHAR(8000),New.ScheduledPayment,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TransactionId = New.TransactionId
                            WHERE   Old.ScheduledPayment <> New.ScheduledPayment
                                    OR (
                                         Old.ScheduledPayment IS NULL
                                         AND New.ScheduledPayment IS NOT NULL
                                       )
                                    OR (
                                         New.ScheduledPayment IS NULL
                                         AND Old.ScheduledPayment IS NOT NULL
                                       ); 
                IF UPDATE(IsDeposited)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TransactionId
                                   ,'IsDeposited'
                                   ,CONVERT(VARCHAR(8000),Old.IsDeposited,121)
                                   ,CONVERT(VARCHAR(8000),New.IsDeposited,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TransactionId = New.TransactionId
                            WHERE   Old.IsDeposited <> New.IsDeposited
                                    OR (
                                         Old.IsDeposited IS NULL
                                         AND New.IsDeposited IS NOT NULL
                                       )
                                    OR (
                                         New.IsDeposited IS NULL
                                         AND Old.IsDeposited IS NOT NULL
                                       ); 
                IF UPDATE(BankAcctId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TransactionId
                                   ,'BankAcctId'
                                   ,CONVERT(VARCHAR(8000),Old.BankAcctId,121)
                                   ,CONVERT(VARCHAR(8000),New.BankAcctId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TransactionId = New.TransactionId
                            WHERE   Old.BankAcctId <> New.BankAcctId
                                    OR (
                                         Old.BankAcctId IS NULL
                                         AND New.BankAcctId IS NOT NULL
                                       )
                                    OR (
                                         New.BankAcctId IS NULL
                                         AND Old.BankAcctId IS NOT NULL
                                       ); 
                IF UPDATE(PaymentTypeId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TransactionId
                                   ,'PaymentTypeId'
                                   ,CONVERT(VARCHAR(8000),Old.PaymentTypeId,121)
                                   ,CONVERT(VARCHAR(8000),New.PaymentTypeId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TransactionId = New.TransactionId
                            WHERE   Old.PaymentTypeId <> New.PaymentTypeId
                                    OR (
                                         Old.PaymentTypeId IS NULL
                                         AND New.PaymentTypeId IS NOT NULL
                                       )
                                    OR (
                                         New.PaymentTypeId IS NULL
                                         AND Old.PaymentTypeId IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO
GO
ALTER TABLE [dbo].[saPayments] ADD CONSTRAINT [PK_saPayments_TransactionId] PRIMARY KEY CLUSTERED  ([TransactionId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_saPayments_TransactionId] ON [dbo].[saPayments] ([TransactionId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saPayments] ADD CONSTRAINT [FK_saPayments_saBankAccounts_BankAcctId_BankAcctId] FOREIGN KEY ([BankAcctId]) REFERENCES [dbo].[saBankAccounts] ([BankAcctId])
GO
ALTER TABLE [dbo].[saPayments] ADD CONSTRAINT [FK_saPayments_saPaymentTypes_PaymentTypeId_PaymentTypeId] FOREIGN KEY ([PaymentTypeId]) REFERENCES [dbo].[saPaymentTypes] ([PaymentTypeId])
GO
ALTER TABLE [dbo].[saPayments] ADD CONSTRAINT [FK_saPayments_saTransactions_TransactionId_TransactionId] FOREIGN KEY ([TransactionId]) REFERENCES [dbo].[saTransactions] ([TransactionId])
GO
