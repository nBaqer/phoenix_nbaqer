CREATE TABLE [doc].[DocumentFile]
(
[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_DocumentFile_Id] DEFAULT (newid()),
[StudentDocId] [uniqueidentifier] NOT NULL,
[FileName] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Content] [varbinary] (max) NOT NULL,
[DateCreated] [smalldatetime] NOT NULL CONSTRAINT [DF_DateCreated] DEFAULT (getdate()),
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [Document] TEXTIMAGE_ON [Document]
GO
ALTER TABLE [doc].[DocumentFile] ADD CONSTRAINT [PK_DocumentFile_Id] PRIMARY KEY CLUSTERED  ([Id]) ON [Document]
GO
ALTER TABLE [doc].[DocumentFile] ADD CONSTRAINT [FK_DocumentFile_plStudentDocs_StudentDocId_StudentDocId] FOREIGN KEY ([StudentDocId]) REFERENCES [dbo].[plStudentDocs] ([StudentDocId])
GO
