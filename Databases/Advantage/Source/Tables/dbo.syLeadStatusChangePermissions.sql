CREATE TABLE [dbo].[syLeadStatusChangePermissions]
(
[LeadStatusChangePermissionID] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syLeadStatusChangePermissions_LeadStatusChangePermissionID] DEFAULT (newid()),
[LeadStatusChangeId] [uniqueidentifier] NOT NULL,
[RoleId] [uniqueidentifier] NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[syLeadStatusChangePermissions_Audit_Delete] ON [dbo].[syLeadStatusChangePermissions]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syLeadStatusChangePermissions','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadStatusChangePermissionID
                               ,'LeadStatusChangeId'
                               ,CONVERT(VARCHAR(8000),Old.LeadStatusChangeId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadStatusChangePermissionID
                               ,'RoleId'
                               ,CONVERT(VARCHAR(8000),Old.RoleId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadStatusChangePermissionID
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[syLeadStatusChangePermissions_Audit_Insert] ON [dbo].[syLeadStatusChangePermissions]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syLeadStatusChangePermissions','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadStatusChangePermissionID
                               ,'LeadStatusChangeId'
                               ,CONVERT(VARCHAR(8000),New.LeadStatusChangeId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadStatusChangePermissionID
                               ,'RoleId'
                               ,CONVERT(VARCHAR(8000),New.RoleId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadStatusChangePermissionID
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[syLeadStatusChangePermissions_Audit_Update] ON [dbo].[syLeadStatusChangePermissions]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syLeadStatusChangePermissions','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(LeadStatusChangeId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.LeadStatusChangePermissionID
                                   ,'LeadStatusChangeId'
                                   ,CONVERT(VARCHAR(8000),Old.LeadStatusChangeId,121)
                                   ,CONVERT(VARCHAR(8000),New.LeadStatusChangeId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.LeadStatusChangePermissionID = New.LeadStatusChangePermissionID
                            WHERE   Old.LeadStatusChangeId <> New.LeadStatusChangeId
                                    OR (
                                         Old.LeadStatusChangeId IS NULL
                                         AND New.LeadStatusChangeId IS NOT NULL
                                       )
                                    OR (
                                         New.LeadStatusChangeId IS NULL
                                         AND Old.LeadStatusChangeId IS NOT NULL
                                       ); 
                IF UPDATE(RoleId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.LeadStatusChangePermissionID
                                   ,'RoleId'
                                   ,CONVERT(VARCHAR(8000),Old.RoleId,121)
                                   ,CONVERT(VARCHAR(8000),New.RoleId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.LeadStatusChangePermissionID = New.LeadStatusChangePermissionID
                            WHERE   Old.RoleId <> New.RoleId
                                    OR (
                                         Old.RoleId IS NULL
                                         AND New.RoleId IS NOT NULL
                                       )
                                    OR (
                                         New.RoleId IS NULL
                                         AND Old.RoleId IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.LeadStatusChangePermissionID
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.LeadStatusChangePermissionID = New.LeadStatusChangePermissionID
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO
ALTER TABLE [dbo].[syLeadStatusChangePermissions] ADD CONSTRAINT [PK_syLeadStatusChangePermissions_LeadStatusChangePermissionID] PRIMARY KEY CLUSTERED  ([LeadStatusChangePermissionID]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_syLeadStatusChangePermissions_LeadStatusChangeId_RoleId] ON [dbo].[syLeadStatusChangePermissions] ([LeadStatusChangeId], [RoleId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syLeadStatusChangePermissions] ADD CONSTRAINT [FK_syLeadStatusChangePermissions_syLeadStatusChanges_LeadStatusChangeId_LeadStatusChangeId] FOREIGN KEY ([LeadStatusChangeId]) REFERENCES [dbo].[syLeadStatusChanges] ([LeadStatusChangeId])
GO
ALTER TABLE [dbo].[syLeadStatusChangePermissions] ADD CONSTRAINT [FK_syLeadStatusChangePermissions_syRoles_RoleId_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[syRoles] ([RoleId])
GO
ALTER TABLE [dbo].[syLeadStatusChangePermissions] ADD CONSTRAINT [FK_syLeadStatusChangePermissions_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
