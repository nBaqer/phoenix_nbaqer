CREATE TABLE [dbo].[rptStuEnrollmentStatus]
(
[StatusCodeId] [uniqueidentifier] NOT NULL,
[StatusCodeDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[rptStuEnrollmentStatus] ADD CONSTRAINT [PK_rptStuEnrollmentStatus_StatusCodeId] PRIMARY KEY CLUSTERED  ([StatusCodeId]) ON [PRIMARY]
GO
