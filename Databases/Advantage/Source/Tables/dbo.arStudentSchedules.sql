CREATE TABLE [dbo].[arStudentSchedules]
(
[StuScheduleId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arStudentSchedules_StuScheduleId] DEFAULT (newid()),
[StuEnrollId] [uniqueidentifier] NOT NULL,
[ScheduleId] [uniqueidentifier] NOT NULL,
[StartDate] [smalldatetime] NULL,
[EndDate] [smalldatetime] NULL,
[Active] [bit] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [smalldatetime] NULL,
[Source] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_arStudentSchedules_Source] DEFAULT ('attendance')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arStudentSchedules] ADD CONSTRAINT [PK_arStudentSchedules_StuScheduleId] PRIMARY KEY CLUSTERED  ([StuScheduleId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_arStudentSchedules_ScheduleId] ON [dbo].[arStudentSchedules] ([ScheduleId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_arStudentSchedules_StuEnrollId] ON [dbo].[arStudentSchedules] ([StuEnrollId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_arStudentSchedules_StuScheduleId_ScheduleId] ON [dbo].[arStudentSchedules] ([StuScheduleId], [ScheduleId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arStudentSchedules] ADD CONSTRAINT [FK_arStudentSchedules_arProgSchedules_ScheduleId_ScheduleId] FOREIGN KEY ([ScheduleId]) REFERENCES [dbo].[arProgSchedules] ([ScheduleId])
GO
ALTER TABLE [dbo].[arStudentSchedules] ADD CONSTRAINT [FK_arStudentSchedules_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY ([StuEnrollId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
