CREATE TABLE [dbo].[adGeographicTypes]
(
[GeographicTypeId] [uniqueidentifier] NOT NULL,
[Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adGeographicTypes] ADD CONSTRAINT [PK_adGeographicTypes_GeographicTypeId] PRIMARY KEY CLUSTERED  ([GeographicTypeId]) ON [PRIMARY]
GO
