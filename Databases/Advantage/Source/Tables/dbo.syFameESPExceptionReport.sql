CREATE TABLE [dbo].[syFameESPExceptionReport]
(
[ExceptionReportId] [uniqueidentifier] NOT NULL,
[SSN] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FAID] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fund] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GrossAmount] [decimal] (19, 2) NULL,
[FileName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Message] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[moduser] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[moddate] [datetime] NULL,
[ExceptionGUID] [uniqueidentifier] NULL,
[Success] [int] NULL,
[Failed] [int] NULL,
[IsInitialImportSuccessful] [bit] NULL CONSTRAINT [DF_syFameESPExceptionReport_IsInitialImportSuccessful] DEFAULT ((0)),
[isEligibleForReprocess] [bit] NULL CONSTRAINT [DF_syFameESPExceptionReport_isEligibleForReprocess] DEFAULT ((0)),
[DisbursementDate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[awardyear] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoanFees] [decimal] (18, 0) NULL,
[loanstartdate] [datetime] NULL,
[loanenddate] [datetime] NULL,
[Hide] [bit] NULL CONSTRAINT [DF_syFameESPExceptionReport_Hide] DEFAULT ((0)),
[RecordPosition] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syFameESPExceptionReport] ADD CONSTRAINT [PK_syFameESPExceptionReport_ExceptionReportId] PRIMARY KEY CLUSTERED  ([ExceptionReportId]) ON [PRIMARY]
GO
