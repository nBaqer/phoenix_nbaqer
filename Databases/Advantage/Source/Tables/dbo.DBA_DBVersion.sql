CREATE TABLE [dbo].[DBA_DBVersion]
(
[DBVersion] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UpgradeDate] [smalldatetime] NOT NULL CONSTRAINT [DF_DBA_DBVersion_UpgradeDate] DEFAULT (getdate()),
[SubVersion] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DBVersionId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_DBA_DBVersion_DBVersionId] DEFAULT (newsequentialid())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DBA_DBVersion] ADD CONSTRAINT [PK_DBA_DBVersion_DBVersionId] PRIMARY KEY CLUSTERED  ([DBVersionId]) ON [PRIMARY]
GO
