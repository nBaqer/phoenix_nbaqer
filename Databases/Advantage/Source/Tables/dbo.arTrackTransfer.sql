CREATE TABLE [dbo].[arTrackTransfer]
(
[TrackTransferId] [uniqueidentifier] NOT NULL,
[StuEnrollId] [uniqueidentifier] NULL,
[SourcePrgVerId] [uniqueidentifier] NULL,
[TargetPrgVerId] [uniqueidentifier] NULL,
[SourceCampusId] [uniqueidentifier] NULL,
[TargetCampusId] [uniqueidentifier] NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arTrackTransfer] ADD CONSTRAINT [PK_arTrackTransfer_TrackTransferId] PRIMARY KEY CLUSTERED  ([TrackTransferId]) ON [PRIMARY]
GO
