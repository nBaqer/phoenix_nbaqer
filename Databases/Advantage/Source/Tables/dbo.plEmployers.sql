CREATE TABLE [dbo].[plEmployers]
(
[EmployerId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_plEmployers_EmployerId] DEFAULT (newid()),
[Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NULL,
[EmployerDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ParentId] [uniqueidentifier] NULL,
[CampGrpId] [uniqueidentifier] NULL,
[Address1] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Address2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateId] [uniqueidentifier] NULL,
[Zip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CountyId] [uniqueidentifier] NULL,
[LocationId] [uniqueidentifier] NULL,
[Phone] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fax] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FeeId] [uniqueidentifier] NULL,
[IndustryId] [uniqueidentifier] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[GroupName] [bit] NULL CONSTRAINT [DF_plEmployers_GroupName] DEFAULT ((0)),
[CountryId] [uniqueidentifier] NULL,
[ForeignPhone] [bit] NULL CONSTRAINT [DF_plEmployers_ForeignPhone] DEFAULT ((0)),
[ForeignFax] [bit] NULL CONSTRAINT [DF_plEmployers_ForeignFax] DEFAULT ((0)),
[ForeignZip] [bit] NULL CONSTRAINT [DF_plEmployers_ForeignZip] DEFAULT ((0)),
[OtherState] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[plEmployers_Audit_Delete] ON [dbo].[plEmployers]
    FOR DELETE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'plEmployers','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerId
                               ,'Code'
                               ,CONVERT(VARCHAR(8000),Old.Code,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerId
                               ,'EmployerDescrip'
                               ,CONVERT(VARCHAR(8000),Old.EmployerDescrip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerId
                               ,'ParentId'
                               ,CONVERT(VARCHAR(8000),Old.ParentId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerId
                               ,'Address1'
                               ,CONVERT(VARCHAR(8000),Old.Address1,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerId
                               ,'Address2'
                               ,CONVERT(VARCHAR(8000),Old.Address2,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerId
                               ,'City'
                               ,CONVERT(VARCHAR(8000),Old.City,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerId
                               ,'StateId'
                               ,CONVERT(VARCHAR(8000),Old.StateId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerId
                               ,'Zip'
                               ,CONVERT(VARCHAR(8000),Old.Zip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerId
                               ,'CountyId'
                               ,CONVERT(VARCHAR(8000),Old.CountyId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerId
                               ,'LocationId'
                               ,CONVERT(VARCHAR(8000),Old.LocationId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerId
                               ,'Phone'
                               ,CONVERT(VARCHAR(8000),Old.Phone,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerId
                               ,'Fax'
                               ,CONVERT(VARCHAR(8000),Old.Fax,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerId
                               ,'Email'
                               ,CONVERT(VARCHAR(8000),Old.Email,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerId
                               ,'FeeId'
                               ,CONVERT(VARCHAR(8000),Old.FeeId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerId
                               ,'IndustryId'
                               ,CONVERT(VARCHAR(8000),Old.IndustryId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerId
                               ,'GroupName'
                               ,CONVERT(VARCHAR(8000),Old.GroupName,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerId
                               ,'CountryId'
                               ,CONVERT(VARCHAR(8000),Old.CountryId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerId
                               ,'ForeignPhone'
                               ,CONVERT(VARCHAR(8000),Old.ForeignPhone,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerId
                               ,'ForeignFax'
                               ,CONVERT(VARCHAR(8000),Old.ForeignFax,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerId
                               ,'ForeignZip'
                               ,CONVERT(VARCHAR(8000),Old.ForeignZip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerId
                               ,'OtherState'
                               ,CONVERT(VARCHAR(8000),Old.OtherState,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[plEmployers_Audit_Insert] ON [dbo].[plEmployers]
    FOR INSERT
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'plEmployers','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerId
                               ,'Code'
                               ,CONVERT(VARCHAR(8000),New.Code,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerId
                               ,'EmployerDescrip'
                               ,CONVERT(VARCHAR(8000),New.EmployerDescrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerId
                               ,'ParentId'
                               ,CONVERT(VARCHAR(8000),New.ParentId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerId
                               ,'Address1'
                               ,CONVERT(VARCHAR(8000),New.Address1,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerId
                               ,'Address2'
                               ,CONVERT(VARCHAR(8000),New.Address2,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerId
                               ,'City'
                               ,CONVERT(VARCHAR(8000),New.City,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerId
                               ,'StateId'
                               ,CONVERT(VARCHAR(8000),New.StateId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerId
                               ,'Zip'
                               ,CONVERT(VARCHAR(8000),New.Zip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerId
                               ,'CountyId'
                               ,CONVERT(VARCHAR(8000),New.CountyId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerId
                               ,'LocationId'
                               ,CONVERT(VARCHAR(8000),New.LocationId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerId
                               ,'Phone'
                               ,CONVERT(VARCHAR(8000),New.Phone,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerId
                               ,'Fax'
                               ,CONVERT(VARCHAR(8000),New.Fax,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerId
                               ,'Email'
                               ,CONVERT(VARCHAR(8000),New.Email,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerId
                               ,'FeeId'
                               ,CONVERT(VARCHAR(8000),New.FeeId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerId
                               ,'IndustryId'
                               ,CONVERT(VARCHAR(8000),New.IndustryId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerId
                               ,'GroupName'
                               ,CONVERT(VARCHAR(8000),New.GroupName,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerId
                               ,'CountryId'
                               ,CONVERT(VARCHAR(8000),New.CountryId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerId
                               ,'ForeignPhone'
                               ,CONVERT(VARCHAR(8000),New.ForeignPhone,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerId
                               ,'ForeignFax'
                               ,CONVERT(VARCHAR(8000),New.ForeignFax,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerId
                               ,'ForeignZip'
                               ,CONVERT(VARCHAR(8000),New.ForeignZip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerId
                               ,'OtherState'
                               ,CONVERT(VARCHAR(8000),New.OtherState,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[plEmployers_Audit_Update] ON [dbo].[plEmployers]
    FOR UPDATE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'plEmployers','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(Code)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerId
                                   ,'Code'
                                   ,CONVERT(VARCHAR(8000),Old.Code,121)
                                   ,CONVERT(VARCHAR(8000),New.Code,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerId = New.EmployerId
                            WHERE   Old.Code <> New.Code
                                    OR (
                                         Old.Code IS NULL
                                         AND New.Code IS NOT NULL
                                       )
                                    OR (
                                         New.Code IS NULL
                                         AND Old.Code IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerId = New.EmployerId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(EmployerDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerId
                                   ,'EmployerDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.EmployerDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.EmployerDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerId = New.EmployerId
                            WHERE   Old.EmployerDescrip <> New.EmployerDescrip
                                    OR (
                                         Old.EmployerDescrip IS NULL
                                         AND New.EmployerDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.EmployerDescrip IS NULL
                                         AND Old.EmployerDescrip IS NOT NULL
                                       ); 
                IF UPDATE(ParentId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerId
                                   ,'ParentId'
                                   ,CONVERT(VARCHAR(8000),Old.ParentId,121)
                                   ,CONVERT(VARCHAR(8000),New.ParentId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerId = New.EmployerId
                            WHERE   Old.ParentId <> New.ParentId
                                    OR (
                                         Old.ParentId IS NULL
                                         AND New.ParentId IS NOT NULL
                                       )
                                    OR (
                                         New.ParentId IS NULL
                                         AND Old.ParentId IS NOT NULL
                                       ); 
                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerId
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerId = New.EmployerId
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 
                IF UPDATE(Address1)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerId
                                   ,'Address1'
                                   ,CONVERT(VARCHAR(8000),Old.Address1,121)
                                   ,CONVERT(VARCHAR(8000),New.Address1,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerId = New.EmployerId
                            WHERE   Old.Address1 <> New.Address1
                                    OR (
                                         Old.Address1 IS NULL
                                         AND New.Address1 IS NOT NULL
                                       )
                                    OR (
                                         New.Address1 IS NULL
                                         AND Old.Address1 IS NOT NULL
                                       ); 
                IF UPDATE(Address2)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerId
                                   ,'Address2'
                                   ,CONVERT(VARCHAR(8000),Old.Address2,121)
                                   ,CONVERT(VARCHAR(8000),New.Address2,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerId = New.EmployerId
                            WHERE   Old.Address2 <> New.Address2
                                    OR (
                                         Old.Address2 IS NULL
                                         AND New.Address2 IS NOT NULL
                                       )
                                    OR (
                                         New.Address2 IS NULL
                                         AND Old.Address2 IS NOT NULL
                                       ); 
                IF UPDATE(City)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerId
                                   ,'City'
                                   ,CONVERT(VARCHAR(8000),Old.City,121)
                                   ,CONVERT(VARCHAR(8000),New.City,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerId = New.EmployerId
                            WHERE   Old.City <> New.City
                                    OR (
                                         Old.City IS NULL
                                         AND New.City IS NOT NULL
                                       )
                                    OR (
                                         New.City IS NULL
                                         AND Old.City IS NOT NULL
                                       ); 
                IF UPDATE(StateId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerId
                                   ,'StateId'
                                   ,CONVERT(VARCHAR(8000),Old.StateId,121)
                                   ,CONVERT(VARCHAR(8000),New.StateId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerId = New.EmployerId
                            WHERE   Old.StateId <> New.StateId
                                    OR (
                                         Old.StateId IS NULL
                                         AND New.StateId IS NOT NULL
                                       )
                                    OR (
                                         New.StateId IS NULL
                                         AND Old.StateId IS NOT NULL
                                       ); 
                IF UPDATE(Zip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerId
                                   ,'Zip'
                                   ,CONVERT(VARCHAR(8000),Old.Zip,121)
                                   ,CONVERT(VARCHAR(8000),New.Zip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerId = New.EmployerId
                            WHERE   Old.Zip <> New.Zip
                                    OR (
                                         Old.Zip IS NULL
                                         AND New.Zip IS NOT NULL
                                       )
                                    OR (
                                         New.Zip IS NULL
                                         AND Old.Zip IS NOT NULL
                                       ); 
                IF UPDATE(CountyId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerId
                                   ,'CountyId'
                                   ,CONVERT(VARCHAR(8000),Old.CountyId,121)
                                   ,CONVERT(VARCHAR(8000),New.CountyId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerId = New.EmployerId
                            WHERE   Old.CountyId <> New.CountyId
                                    OR (
                                         Old.CountyId IS NULL
                                         AND New.CountyId IS NOT NULL
                                       )
                                    OR (
                                         New.CountyId IS NULL
                                         AND Old.CountyId IS NOT NULL
                                       ); 
                IF UPDATE(LocationId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerId
                                   ,'LocationId'
                                   ,CONVERT(VARCHAR(8000),Old.LocationId,121)
                                   ,CONVERT(VARCHAR(8000),New.LocationId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerId = New.EmployerId
                            WHERE   Old.LocationId <> New.LocationId
                                    OR (
                                         Old.LocationId IS NULL
                                         AND New.LocationId IS NOT NULL
                                       )
                                    OR (
                                         New.LocationId IS NULL
                                         AND Old.LocationId IS NOT NULL
                                       ); 
                IF UPDATE(Phone)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerId
                                   ,'Phone'
                                   ,CONVERT(VARCHAR(8000),Old.Phone,121)
                                   ,CONVERT(VARCHAR(8000),New.Phone,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerId = New.EmployerId
                            WHERE   Old.Phone <> New.Phone
                                    OR (
                                         Old.Phone IS NULL
                                         AND New.Phone IS NOT NULL
                                       )
                                    OR (
                                         New.Phone IS NULL
                                         AND Old.Phone IS NOT NULL
                                       ); 
                IF UPDATE(Fax)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerId
                                   ,'Fax'
                                   ,CONVERT(VARCHAR(8000),Old.Fax,121)
                                   ,CONVERT(VARCHAR(8000),New.Fax,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerId = New.EmployerId
                            WHERE   Old.Fax <> New.Fax
                                    OR (
                                         Old.Fax IS NULL
                                         AND New.Fax IS NOT NULL
                                       )
                                    OR (
                                         New.Fax IS NULL
                                         AND Old.Fax IS NOT NULL
                                       ); 
                IF UPDATE(Email)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerId
                                   ,'Email'
                                   ,CONVERT(VARCHAR(8000),Old.Email,121)
                                   ,CONVERT(VARCHAR(8000),New.Email,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerId = New.EmployerId
                            WHERE   Old.Email <> New.Email
                                    OR (
                                         Old.Email IS NULL
                                         AND New.Email IS NOT NULL
                                       )
                                    OR (
                                         New.Email IS NULL
                                         AND Old.Email IS NOT NULL
                                       ); 
                IF UPDATE(FeeId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerId
                                   ,'FeeId'
                                   ,CONVERT(VARCHAR(8000),Old.FeeId,121)
                                   ,CONVERT(VARCHAR(8000),New.FeeId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerId = New.EmployerId
                            WHERE   Old.FeeId <> New.FeeId
                                    OR (
                                         Old.FeeId IS NULL
                                         AND New.FeeId IS NOT NULL
                                       )
                                    OR (
                                         New.FeeId IS NULL
                                         AND Old.FeeId IS NOT NULL
                                       ); 
                IF UPDATE(IndustryId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerId
                                   ,'IndustryId'
                                   ,CONVERT(VARCHAR(8000),Old.IndustryId,121)
                                   ,CONVERT(VARCHAR(8000),New.IndustryId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerId = New.EmployerId
                            WHERE   Old.IndustryId <> New.IndustryId
                                    OR (
                                         Old.IndustryId IS NULL
                                         AND New.IndustryId IS NOT NULL
                                       )
                                    OR (
                                         New.IndustryId IS NULL
                                         AND Old.IndustryId IS NOT NULL
                                       ); 
                IF UPDATE(GroupName)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerId
                                   ,'GroupName'
                                   ,CONVERT(VARCHAR(8000),Old.GroupName,121)
                                   ,CONVERT(VARCHAR(8000),New.GroupName,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerId = New.EmployerId
                            WHERE   Old.GroupName <> New.GroupName
                                    OR (
                                         Old.GroupName IS NULL
                                         AND New.GroupName IS NOT NULL
                                       )
                                    OR (
                                         New.GroupName IS NULL
                                         AND Old.GroupName IS NOT NULL
                                       ); 
                IF UPDATE(CountryId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerId
                                   ,'CountryId'
                                   ,CONVERT(VARCHAR(8000),Old.CountryId,121)
                                   ,CONVERT(VARCHAR(8000),New.CountryId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerId = New.EmployerId
                            WHERE   Old.CountryId <> New.CountryId
                                    OR (
                                         Old.CountryId IS NULL
                                         AND New.CountryId IS NOT NULL
                                       )
                                    OR (
                                         New.CountryId IS NULL
                                         AND Old.CountryId IS NOT NULL
                                       ); 
                IF UPDATE(ForeignPhone)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerId
                                   ,'ForeignPhone'
                                   ,CONVERT(VARCHAR(8000),Old.ForeignPhone,121)
                                   ,CONVERT(VARCHAR(8000),New.ForeignPhone,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerId = New.EmployerId
                            WHERE   Old.ForeignPhone <> New.ForeignPhone
                                    OR (
                                         Old.ForeignPhone IS NULL
                                         AND New.ForeignPhone IS NOT NULL
                                       )
                                    OR (
                                         New.ForeignPhone IS NULL
                                         AND Old.ForeignPhone IS NOT NULL
                                       ); 
                IF UPDATE(ForeignFax)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerId
                                   ,'ForeignFax'
                                   ,CONVERT(VARCHAR(8000),Old.ForeignFax,121)
                                   ,CONVERT(VARCHAR(8000),New.ForeignFax,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerId = New.EmployerId
                            WHERE   Old.ForeignFax <> New.ForeignFax
                                    OR (
                                         Old.ForeignFax IS NULL
                                         AND New.ForeignFax IS NOT NULL
                                       )
                                    OR (
                                         New.ForeignFax IS NULL
                                         AND Old.ForeignFax IS NOT NULL
                                       ); 
                IF UPDATE(ForeignZip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerId
                                   ,'ForeignZip'
                                   ,CONVERT(VARCHAR(8000),Old.ForeignZip,121)
                                   ,CONVERT(VARCHAR(8000),New.ForeignZip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerId = New.EmployerId
                            WHERE   Old.ForeignZip <> New.ForeignZip
                                    OR (
                                         Old.ForeignZip IS NULL
                                         AND New.ForeignZip IS NOT NULL
                                       )
                                    OR (
                                         New.ForeignZip IS NULL
                                         AND Old.ForeignZip IS NOT NULL
                                       ); 
                IF UPDATE(OtherState)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerId
                                   ,'OtherState'
                                   ,CONVERT(VARCHAR(8000),Old.OtherState,121)
                                   ,CONVERT(VARCHAR(8000),New.OtherState,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerId = New.EmployerId
                            WHERE   Old.OtherState <> New.OtherState
                                    OR (
                                         Old.OtherState IS NULL
                                         AND New.OtherState IS NOT NULL
                                       )
                                    OR (
                                         New.OtherState IS NULL
                                         AND Old.OtherState IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF;

GO
ALTER TABLE [dbo].[plEmployers] ADD CONSTRAINT [PK_plEmployers_EmployerId] PRIMARY KEY CLUSTERED  ([EmployerId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_plEmployers_CampGrpId] ON [dbo].[plEmployers] ([CampGrpId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_plEmployers_Code_EmployerDescrip_CampGrpId] ON [dbo].[plEmployers] ([Code], [EmployerDescrip], [CampGrpId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_plEmployers_CountyId] ON [dbo].[plEmployers] ([CountyId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_plEmployers_IndustryId] ON [dbo].[plEmployers] ([IndustryId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_plEmployers_LocationId] ON [dbo].[plEmployers] ([LocationId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_plEmployers_StateId] ON [dbo].[plEmployers] ([StateId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[plEmployers] ADD CONSTRAINT [FK_plEmployers_adCounties_CountyId_CountyId] FOREIGN KEY ([CountyId]) REFERENCES [dbo].[adCounties] ([CountyId])
GO
ALTER TABLE [dbo].[plEmployers] ADD CONSTRAINT [FK_plEmployers_adCountries_CountryId_CountryId] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[adCountries] ([CountryId])
GO
ALTER TABLE [dbo].[plEmployers] ADD CONSTRAINT [FK_plEmployers_plEmployers_ParentId_EmployerId] FOREIGN KEY ([ParentId]) REFERENCES [dbo].[plEmployers] ([EmployerId])
GO
ALTER TABLE [dbo].[plEmployers] ADD CONSTRAINT [FK_plEmployers_plFee_FeeId_FeeId] FOREIGN KEY ([FeeId]) REFERENCES [dbo].[plFee] ([FeeId])
GO
ALTER TABLE [dbo].[plEmployers] ADD CONSTRAINT [FK_plEmployers_plIndustries_IndustryId_IndustryId] FOREIGN KEY ([IndustryId]) REFERENCES [dbo].[plIndustries] ([IndustryId])
GO
ALTER TABLE [dbo].[plEmployers] ADD CONSTRAINT [FK_plEmployers_plLocations_LocationId_LocationId] FOREIGN KEY ([LocationId]) REFERENCES [dbo].[plLocations] ([LocationId])
GO
ALTER TABLE [dbo].[plEmployers] ADD CONSTRAINT [FK_plEmployers_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[plEmployers] ADD CONSTRAINT [FK_plEmployers_syStates_StateId_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[syStates] ([StateId])
GO
ALTER TABLE [dbo].[plEmployers] ADD CONSTRAINT [FK_plEmployers_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
