CREATE TABLE [dbo].[syRptAdhocRelations]
(
[RelationId] [int] NOT NULL IDENTITY(1, 1),
[FkTable] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FkColumn] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PkTable] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PkColumn] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syRptAdhocRelations] ADD CONSTRAINT [PK_syRptAdhocRelations_RelationId] PRIMARY KEY CLUSTERED  ([RelationId]) ON [PRIMARY]
GO
