CREATE TABLE [dbo].[adLeadFields]
(
[LeadID] [int] NOT NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LeadField] [int] NULL,
[LeadFieldId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_adLeadFields_LeadFieldId] DEFAULT (newsequentialid())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adLeadFields] ADD CONSTRAINT [PK_adLeadFields_LeadFieldId] PRIMARY KEY CLUSTERED  ([LeadFieldId]) ON [PRIMARY]
GO
