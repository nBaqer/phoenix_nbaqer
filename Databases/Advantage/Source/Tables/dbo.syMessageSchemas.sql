CREATE TABLE [dbo].[syMessageSchemas]
(
[MessageSchemaId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syMessageSchemas_MessageSchemaId] DEFAULT (newid()),
[StatusId] [uniqueidentifier] NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[MessageSchemaDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SqlStatement] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MessageSchemaURL] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MessageSchema] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NOT NULL CONSTRAINT [DF_syMessageSchemas_ModDate] DEFAULT ('2005-01-01'),
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_syMessageSchemas_ModUser] DEFAULT ('no user')
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[syMessageSchemas] ADD CONSTRAINT [PK_syMessageSchemas_MessageSchemaId] PRIMARY KEY CLUSTERED  ([MessageSchemaId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syMessageSchemas] ADD CONSTRAINT [FK_syMessageSchemas_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[syMessageSchemas] ADD CONSTRAINT [FK_syMessageSchemas_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
