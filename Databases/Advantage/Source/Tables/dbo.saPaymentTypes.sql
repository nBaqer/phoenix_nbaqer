CREATE TABLE [dbo].[saPaymentTypes]
(
[PaymentTypeId] [int] NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saPaymentTypes] ADD CONSTRAINT [PK_saPaymentTypes_PaymentTypeId] PRIMARY KEY CLUSTERED  ([PaymentTypeId]) ON [PRIMARY]
GO
