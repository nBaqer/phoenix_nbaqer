CREATE TABLE [dbo].[syCustomFeatureFileConfiguration]
(
[CustomFeatureConfigurationId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_syCustomFeatureFileConfiguration_CustomFeatureConfigurationId] DEFAULT (newid()),
[FeatureId] [int] NOT NULL,
[CampusConfigurationId] [uniqueidentifier] NOT NULL,
[FileStorageType] [int] NOT NULL,
[UserName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Password] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Path] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CloudKey] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syCustomFeatureFileConfiguration] ADD CONSTRAINT [PK_syCustomFeatureFileConfiguration_CustomFeatureConfigurationId] PRIMARY KEY CLUSTERED  ([CustomFeatureConfigurationId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syCustomFeatureFileConfiguration] ADD CONSTRAINT [FK_syCustomFeatureFileConfiguration_syCampusFileConfiguration_CampusFileConfigurationId_CampusFileConfigurationId] FOREIGN KEY ([CampusConfigurationId]) REFERENCES [dbo].[syCampusFileConfiguration] ([CampusFileConfigurationId]) ON DELETE CASCADE
GO
ALTER TABLE [dbo].[syCustomFeatureFileConfiguration] ADD CONSTRAINT [FK_syCustomFeatureFileConfiguration_syFeatureFileConfiguration_FeatureId_FeatureId] FOREIGN KEY ([FeatureId]) REFERENCES [dbo].[syFileConfigurationFeatures] ([FeatureId])
GO
