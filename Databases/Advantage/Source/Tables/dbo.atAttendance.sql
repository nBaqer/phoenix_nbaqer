CREATE TABLE [dbo].[atAttendance]
(
[AttendanceId] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[EnrollId] [uniqueidentifier] NULL,
[AttendanceType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AttendanceTypeId] [uniqueidentifier] NULL,
[AttendanceDate] [datetime] NULL,
[Actual] [smallint] NULL,
[Tardy] [bit] NULL,
[TardyTime] [int] NULL,
[Excused] [bit] NULL,
[ExcusedTime] [int] NULL,
[Comments] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[atAttendance] ADD CONSTRAINT [PK_atAttendance_AttendanceId] PRIMARY KEY CLUSTERED  ([AttendanceId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[atAttendance] ADD CONSTRAINT [FK_atAttendance_arStuEnrollments_EnrollId_StuEnrollId] FOREIGN KEY ([EnrollId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
