CREATE TABLE [dbo].[arPrgVerInstructionType]
(
[PrgVerInstructionTypeId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_arPrgVerInstructionType_PrgVerInstructionTypeId] DEFAULT (newsequentialid()),
[PrgVerId] [uniqueidentifier] NOT NULL,
[InstructionTypeId] [uniqueidentifier] NOT NULL,
[Hours] [decimal] (6, 0) NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arPrgVerInstructionType] ADD CONSTRAINT [PK_arPrgVerInstructionType_PrgVerInstructionTypeId] PRIMARY KEY CLUSTERED  ([PrgVerInstructionTypeId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arPrgVerInstructionType] ADD CONSTRAINT [FK_arPrgVerInstructionType_arInstructionType_InstructionTypeId_InstructionTypeId] FOREIGN KEY ([InstructionTypeId]) REFERENCES [dbo].[arInstructionType] ([InstructionTypeId])
GO
ALTER TABLE [dbo].[arPrgVerInstructionType] ADD CONSTRAINT [FK_arPrgVerInstructionType_arPrgVersions_PrgVerId_PrgVerId] FOREIGN KEY ([PrgVerId]) REFERENCES [dbo].[arPrgVersions] ([PrgVerId]) ON DELETE CASCADE ON UPDATE CASCADE
GO
