CREATE TABLE [dbo].[syConfigAppSettings]
(
[KeyName] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CampusSpecific] [bit] NOT NULL CONSTRAINT [DF_syConfigAppSettings_CampusSpecific] DEFAULT ((1)),
[ExtraConfirmation] [bit] NOT NULL CONSTRAINT [DF_syConfigAppSettings_ExtraConfirmation] DEFAULT ((0)),
[SettingId] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syConfigAppSettings] ADD CONSTRAINT [PK_syConfigAppSettings_SettingId] PRIMARY KEY CLUSTERED  ([SettingId]) ON [PRIMARY]
GO
