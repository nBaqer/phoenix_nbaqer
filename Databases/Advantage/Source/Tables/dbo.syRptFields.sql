CREATE TABLE [dbo].[syRptFields]
(
[RptFldId] [int] NOT NULL,
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DDL] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FldId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syRptFields] ADD CONSTRAINT [PK_syRptFields_RptFldId] PRIMARY KEY CLUSTERED  ([RptFldId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_syRptFields_Descrip] ON [dbo].[syRptFields] ([Descrip]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syRptFields] ADD CONSTRAINT [FK_syRptFields_syFields_FldId_FldId] FOREIGN KEY ([FldId]) REFERENCES [dbo].[syFields] ([FldId])
GO
