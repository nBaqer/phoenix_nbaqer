CREATE TABLE [dbo].[advProgTyps]
(
[advProgTypId] [tinyint] NOT NULL,
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[advProgTyps] ADD CONSTRAINT [PK_advProgTyps_advProgTypId] PRIMARY KEY CLUSTERED  ([advProgTypId]) ON [PRIMARY]
GO
