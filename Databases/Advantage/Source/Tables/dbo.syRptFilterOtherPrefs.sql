CREATE TABLE [dbo].[syRptFilterOtherPrefs]
(
[FilterOtherPrefId] [uniqueidentifier] NOT NULL,
[PrefId] [uniqueidentifier] NOT NULL,
[RptParamId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OpId] [tinyint] NOT NULL,
[OpValue] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AdHocFieldId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syRptFilterOtherPrefs] ADD CONSTRAINT [PK_syRptFilterOtherPrefs_FilterOtherPrefId] PRIMARY KEY CLUSTERED  ([FilterOtherPrefId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syRptFilterOtherPrefs] ADD CONSTRAINT [FK_syRptFilterOtherPrefs_syRptUserPrefs_PrefId_PrefId] FOREIGN KEY ([PrefId]) REFERENCES [dbo].[syRptUserPrefs] ([PrefId]) ON DELETE CASCADE ON UPDATE CASCADE
GO
GRANT DELETE ON  [dbo].[syRptFilterOtherPrefs] TO [AdvantageRole]
GO
GRANT INSERT ON  [dbo].[syRptFilterOtherPrefs] TO [AdvantageRole]
GO
GRANT REFERENCES ON  [dbo].[syRptFilterOtherPrefs] TO [AdvantageRole]
GO
GRANT SELECT ON  [dbo].[syRptFilterOtherPrefs] TO [AdvantageRole]
GO
GRANT UPDATE ON  [dbo].[syRptFilterOtherPrefs] TO [AdvantageRole]
GO
