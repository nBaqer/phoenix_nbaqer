CREATE TABLE [dbo].[adDependencyTypes]
(
[DependencyTypeId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_adDependencyTypes_DependencyTypeId] DEFAULT (newid()),
[Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adDependencyTypes] ADD CONSTRAINT [PK_adDependencyTypes_DependencyTypeId] PRIMARY KEY CLUSTERED  ([DependencyTypeId]) ON [PRIMARY]
GO
