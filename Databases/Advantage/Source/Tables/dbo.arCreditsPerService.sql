CREATE TABLE [dbo].[arCreditsPerService]
(
[CreditPerServiceId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_arCreditsPerService_CreditPerServiceId] DEFAULT (newid()),
[GrdComponentTypeId] [uniqueidentifier] NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[NumberOfCredits] [decimal] (18, 0) NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arCreditsPerService] ADD CONSTRAINT [PK_arCreditsPerService_CreditPerServiceId] PRIMARY KEY CLUSTERED  ([CreditPerServiceId]) ON [PRIMARY]
GO
