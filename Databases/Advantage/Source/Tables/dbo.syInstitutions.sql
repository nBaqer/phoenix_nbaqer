CREATE TABLE [dbo].[syInstitutions]
(
[HSId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syInstitutions_HSId] DEFAULT (newid()),
[HSCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_syInstitutions_StatusId] DEFAULT ([dbo].[UDF_GetSyStatusesValue]('A')),
[HSName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CampGrpId] [uniqueidentifier] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NOT NULL,
[LevelID] [int] NULL,
[TypeId] [int] NULL,
[ImportTypeId] [int] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[syInstitutions_Audit_Delete] ON [dbo].[syInstitutions]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syInstitutions','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.HSId
                               ,'HSCode'
                               ,CONVERT(VARCHAR(8000),Old.HSCode,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.HSId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.HSId
                               ,'HSName'
                               ,CONVERT(VARCHAR(8000),Old.HSName,121)
                        FROM    Deleted Old; 
                
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.HSId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
            END; 
        END;



    SET NOCOUNT OFF;
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[syInstitutions_Audit_Insert] ON [dbo].[syInstitutions]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syInstitutions','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.HSId
                               ,'HSCode'
                               ,CONVERT(VARCHAR(8000),New.HSCode,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.HSId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.HSId
                               ,'HSName'
                               ,CONVERT(VARCHAR(8000),New.HSName,121)
                        FROM    Inserted New; 
                
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.HSId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New; 
            END; 
        END;



    SET NOCOUNT OFF;
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[syInstitutions_Audit_Update] ON [dbo].[syInstitutions]

    FOR UPDATE

AS

    SET NOCOUNT ON; 



    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 

    DECLARE @EventRows AS INT; 

    DECLARE @EventDate AS DATETIME; 

    DECLARE @UserName AS VARCHAR(50);

    SET @AuditHistId = NEWID(); 

    SET @EventRows = (

                       SELECT   COUNT(*)

                       FROM     Inserted

                     ); 

    SET @EventDate = (

                       SELECT TOP 1

                                ModDate

                       FROM     Inserted

                     ); 

    SET @UserName = (

                      SELECT TOP 1

                                ModUser

                      FROM      Inserted

                    ); 

    IF @EventRows > 0

        BEGIN 

            EXEC fmAuditHistAdd @AuditHistId,'syInstitutions','U',@EventRows,@EventDate,@UserName; 

            BEGIN 

                IF UPDATE(HSCode)

                    INSERT  INTO syAuditHistDetail

                            (

                             AuditHistId

                            ,RowId

                            ,ColumnName

                            ,OldValue

                            ,NewValue

                            )

                            SELECT  @AuditHistId

                                   ,New.HSId

                                   ,'HSCode'

                                   ,CONVERT(VARCHAR(8000),Old.HSCode,121)

                                   ,CONVERT(VARCHAR(8000),New.HSCode,121)

                            FROM    Inserted New

                            FULL OUTER JOIN Deleted Old ON Old.HSId = New.HSId

                            WHERE   Old.HSCode <> New.HSCode

                                    OR (

                                         Old.HSCode IS NULL

                                         AND New.HSCode IS NOT NULL

                                       )

                                    OR (

                                         New.HSCode IS NULL

                                         AND Old.HSCode IS NOT NULL

                                       ); 

                IF UPDATE(StatusId)

                    INSERT  INTO syAuditHistDetail

                            (

                             AuditHistId

                            ,RowId

                            ,ColumnName

                            ,OldValue

                            ,NewValue

                            )

                            SELECT  @AuditHistId

                                   ,New.HSId

                                   ,'StatusId'

                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)

                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)

                            FROM    Inserted New

                            FULL OUTER JOIN Deleted Old ON Old.HSId = New.HSId

                            WHERE   Old.StatusId <> New.StatusId

                                    OR (

                                         Old.StatusId IS NULL

                                         AND New.StatusId IS NOT NULL

                                       )

                                    OR (

                                         New.StatusId IS NULL

                                         AND Old.StatusId IS NOT NULL

                                       ); 

                IF UPDATE(HSName)

                    INSERT  INTO syAuditHistDetail

                            (

                             AuditHistId

                            ,RowId

                            ,ColumnName

                            ,OldValue

                            ,NewValue

)

                            SELECT  @AuditHistId

                                   ,New.HSId

                                   ,'HSName'

       ,CONVERT(VARCHAR(8000),Old.HSName,121)

                                   ,CONVERT(VARCHAR(8000),New.HSName,121)

                            FROM    Inserted New

                            FULL OUTER JOIN Deleted Old ON Old.HSId = New.HSId

                            WHERE   Old.HSName <> New.HSName

                                    OR (

                                         Old.HSName IS NULL

                                         AND New.HSName IS NOT NULL

                                       )

                                    OR (

                                         New.HSName IS NULL

                                         AND Old.HSName IS NOT NULL

                                       ); 

             

                IF UPDATE(CampGrpId)

                    INSERT  INTO syAuditHistDetail

                            (

                             AuditHistId

                            ,RowId

                            ,ColumnName

                            ,OldValue

                            ,NewValue

                            )

                            SELECT  @AuditHistId

                                   ,New.HSId

                                   ,'CampGrpId'

                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)

                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)

                            FROM    Inserted New

                            FULL OUTER JOIN Deleted Old ON Old.HSId = New.HSId

                            WHERE   Old.CampGrpId <> New.CampGrpId

                                    OR (

                                         Old.CampGrpId IS NULL

                                         AND New.CampGrpId IS NOT NULL

                                       )

                                    OR (

                                         New.CampGrpId IS NULL

                                         AND Old.CampGrpId IS NOT NULL

                                       ); 

             

            END; 

        END;







    SET NOCOUNT OFF;
GO
ALTER TABLE [dbo].[syInstitutions] ADD CONSTRAINT [PK_syInstitutions_HSId] PRIMARY KEY CLUSTERED  ([HSId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syInstitutions] ADD CONSTRAINT [FK_syInstitutions_adLevel_LevelId_LevelId] FOREIGN KEY ([LevelID]) REFERENCES [dbo].[adLevel] ([LevelId])
GO
ALTER TABLE [dbo].[syInstitutions] ADD CONSTRAINT [FK_syInstitutions_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[syInstitutions] ADD CONSTRAINT [FK_syInstitutions_syInstitutionImportTypes_ImportTypeId_ImportTypeID] FOREIGN KEY ([ImportTypeId]) REFERENCES [dbo].[syInstitutionImportTypes] ([ImportTypeID])
GO
ALTER TABLE [dbo].[syInstitutions] ADD CONSTRAINT [FK_syInstitutions_syInstitutionTypes_TypeId_TypeId] FOREIGN KEY ([TypeId]) REFERENCES [dbo].[syInstitutionTypes] ([TypeID])
GO
ALTER TABLE [dbo].[syInstitutions] ADD CONSTRAINT [FK_syInstitutions_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
