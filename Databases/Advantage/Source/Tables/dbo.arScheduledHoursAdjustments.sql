CREATE TABLE [dbo].[arScheduledHoursAdjustments]
(
[ScheduledHoursAdjustmentId] [uniqueidentifier] NOT NULL,
[StuEnrollId] [uniqueidentifier] NOT NULL,
[RecordDate] [smalldatetime] NOT NULL,
[AdjustmentAmount] [decimal] (18, 2) NULL,
[ModDate] [smalldatetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arScheduledHoursAdjustments] ADD CONSTRAINT [PK_arScheduledHoursAdjustments_ScheduledHoursAdjustmentId] PRIMARY KEY CLUSTERED  ([ScheduledHoursAdjustmentId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arScheduledHoursAdjustments] ADD CONSTRAINT [FK_arScheduledHoursAdjustments_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY ([StuEnrollId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
