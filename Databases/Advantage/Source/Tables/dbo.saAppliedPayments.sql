CREATE TABLE [dbo].[saAppliedPayments]
(
[AppliedPmtId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_saAppliedPayments_AppliedPmtId] DEFAULT (newid()),
[TransactionId] [uniqueidentifier] NOT NULL,
[ApplyToTransId] [uniqueidentifier] NOT NULL,
[Amount] [decimal] (18, 2) NOT NULL,
[ModDate] [datetime] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saAppliedPayments] ADD CONSTRAINT [PK_saAppliedPayments_AppliedPmtId] PRIMARY KEY CLUSTERED  ([AppliedPmtId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_saAppliedPayments_TransactionId] ON [dbo].[saAppliedPayments] ([TransactionId]) ON [PRIMARY]
GO
