CREATE TABLE [dbo].[syEvents]
(
[EventID] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syEvents_EventID] DEFAULT (newid()),
[StatusId] [uniqueidentifier] NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[ModuleID] [tinyint] NOT NULL,
[EventDate] [datetime] NOT NULL,
[ShowDate] [datetime] NOT NULL,
[Title] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[WhereWhen] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[syEvents_Audit_Delete] ON [dbo].[syEvents]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syEvents','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EventID
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EventID
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EventID
                               ,'ModuleId'
                               ,CONVERT(VARCHAR(8000),Old.ModuleID,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EventID
                               ,'EventDate'
                               ,CONVERT(VARCHAR(8000),Old.EventDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EventID
                               ,'ShowDate'
                               ,CONVERT(VARCHAR(8000),Old.ShowDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EventID
                               ,'Title'
                               ,CONVERT(VARCHAR(8000),Old.Title,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EventID
                               ,'WhereWhen'
                               ,CONVERT(VARCHAR(8000),Old.WhereWhen,121)
                        FROM    Deleted Old; 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[syEvents_Audit_Insert] ON [dbo].[syEvents]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syEvents','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EventID
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EventID
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EventID
                               ,'ModuleId'
                               ,CONVERT(VARCHAR(8000),New.ModuleID,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EventID
                               ,'EventDate'
                               ,CONVERT(VARCHAR(8000),New.EventDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EventID
                               ,'ShowDate'
                               ,CONVERT(VARCHAR(8000),New.ShowDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EventID
                               ,'Title'
                               ,CONVERT(VARCHAR(8000),New.Title,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EventID
                               ,'WhereWhen'
                               ,CONVERT(VARCHAR(8000),New.WhereWhen,121)
                        FROM    Inserted New; 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO
GO

GO

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[syEvents_Audit_Update] ON [dbo].[syEvents]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syEvents','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EventID
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EventID = New.EventID
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EventID
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EventID = New.EventID
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 
                IF UPDATE(ModuleID)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EventID
                                   ,'ModuleId'
                                   ,CONVERT(VARCHAR(8000),Old.ModuleID,121)
                                   ,CONVERT(VARCHAR(8000),New.ModuleID,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EventID = New.EventID
                            WHERE   Old.ModuleID <> New.ModuleID
                                    OR (
                                         Old.ModuleID IS NULL
                                         AND New.ModuleID IS NOT NULL
                                       )
                                    OR (
                                         New.ModuleID IS NULL
                                         AND Old.ModuleID IS NOT NULL
                                       ); 
                IF UPDATE(EventDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EventID
                                   ,'EventDate'
                                   ,CONVERT(VARCHAR(8000),Old.EventDate,121)
                                   ,CONVERT(VARCHAR(8000),New.EventDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EventID = New.EventID
                            WHERE   Old.EventDate <> New.EventDate
                                    OR (
                                         Old.EventDate IS NULL
                                         AND New.EventDate IS NOT NULL
                                       )
                                    OR (
                                         New.EventDate IS NULL
                                         AND Old.EventDate IS NOT NULL
                                       ); 
                IF UPDATE(ShowDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EventID
                                   ,'ShowDate'
                                   ,CONVERT(VARCHAR(8000),Old.ShowDate,121)
                                   ,CONVERT(VARCHAR(8000),New.ShowDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EventID = New.EventID
                            WHERE   Old.ShowDate <> New.ShowDate
                                    OR (
                                         Old.ShowDate IS NULL
                                         AND New.ShowDate IS NOT NULL
                                       )
                                    OR (
                                         New.ShowDate IS NULL
                                         AND Old.ShowDate IS NOT NULL
                                       ); 
                IF UPDATE(Title)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EventID
                                   ,'Title'
                                   ,CONVERT(VARCHAR(8000),Old.Title,121)
                                   ,CONVERT(VARCHAR(8000),New.Title,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EventID = New.EventID
                            WHERE   Old.Title <> New.Title
                                    OR (
                                         Old.Title IS NULL
                                         AND New.Title IS NOT NULL
                                       )
                                    OR (
                                         New.Title IS NULL
                                         AND Old.Title IS NOT NULL
                                       ); 
                IF UPDATE(WhereWhen)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EventID
                                   ,'WhereWhen'
                                   ,CONVERT(VARCHAR(8000),Old.WhereWhen,121)
                                   ,CONVERT(VARCHAR(8000),New.WhereWhen,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EventID = New.EventID
                            WHERE   Old.WhereWhen <> New.WhereWhen
                                    OR (
                                         Old.WhereWhen IS NULL
                                         AND New.WhereWhen IS NOT NULL
                                       )
                                    OR (
                                         New.WhereWhen IS NULL
                                         AND Old.WhereWhen IS NOT NULL
                                       ); 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[syEvents] ADD CONSTRAINT [PK_syEvents_EventID] PRIMARY KEY CLUSTERED  ([EventID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syEvents] ADD CONSTRAINT [FK_syEvents_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[syEvents] ADD CONSTRAINT [FK_syEvents_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
