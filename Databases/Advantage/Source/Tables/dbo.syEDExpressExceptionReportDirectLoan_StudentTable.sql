CREATE TABLE [dbo].[syEDExpressExceptionReportDirectLoan_StudentTable]
(
[ExceptionReportId] [uniqueidentifier] NOT NULL,
[DbIn] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Filter] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddDate] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddTime] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoanAmtApproved] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoanFeePer] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoanId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoanPeriodEndDate] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoanPeriodStartDate] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoanStatus] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoanType] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MPNStatus] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginalSSN] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateDate] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcademicYearId] [uniqueidentifier] NULL,
[ModDate] [datetime] NULL,
[ErrorMsg] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExceptionGUID] [uniqueidentifier] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[syEDExpressExceptionReportDirectLoan_StudentTable] ADD CONSTRAINT [PK_syEDExpressExceptionReportDirectLoan_StudentTable_ExceptionReportId] PRIMARY KEY CLUSTERED  ([ExceptionReportId]) ON [PRIMARY]
GO
