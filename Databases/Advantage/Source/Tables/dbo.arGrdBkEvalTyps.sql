CREATE TABLE [dbo].[arGrdBkEvalTyps]
(
[GrdBkEvalTypId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arGrdBkEvalTyps_GrdBkEvalTypId] DEFAULT (newid()),
[GrdBkEvalTypCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GrdBkEvalTypDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arGrdBkEvalTyps] ADD CONSTRAINT [PK_arGrdBkEvalTyps_GrdBkEvalTypId] PRIMARY KEY CLUSTERED  ([GrdBkEvalTypId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arGrdBkEvalTyps] ADD CONSTRAINT [FK_arGrdBkEvalTyps_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[arGrdBkEvalTyps] ADD CONSTRAINT [FK_arGrdBkEvalTyps_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
