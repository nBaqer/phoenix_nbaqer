CREATE TABLE [dbo].[syWidgetResourceRoles]
(
[WidgetResourceRoleId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_syWidgetResourceRoles_WidgetResourceRoleId] DEFAULT (newsequentialid()),
[WidgetId] [uniqueidentifier] NOT NULL,
[ResourceId] [smallint] NULL,
[SysRoleId] [int] NULL,
[StatusId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syWidgetResourceRoles] ADD CONSTRAINT [PK_syWidgetResourceRoles_WidgetResourceRoleId] PRIMARY KEY CLUSTERED  ([WidgetResourceRoleId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syWidgetResourceRoles] ADD CONSTRAINT [FK_syWidgetResourceRoles_syResources_ResourceId_ResourceId] FOREIGN KEY ([ResourceId]) REFERENCES [dbo].[syResources] ([ResourceID])
GO
ALTER TABLE [dbo].[syWidgetResourceRoles] ADD CONSTRAINT [FK_syWidgetResourceRoles_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
ALTER TABLE [dbo].[syWidgetResourceRoles] ADD CONSTRAINT [FK_syWidgetResourceRoles_sySysRoles_SysRoleId_SysRoleId] FOREIGN KEY ([SysRoleId]) REFERENCES [dbo].[sySysRoles] ([SysRoleId])
GO
ALTER TABLE [dbo].[syWidgetResourceRoles] ADD CONSTRAINT [FK_syWidgetResourceRoles_syWidgets_WidgetId_WidgetId] FOREIGN KEY ([WidgetId]) REFERENCES [dbo].[syWidgets] ([WidgetId])
GO
