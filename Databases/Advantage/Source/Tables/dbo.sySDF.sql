CREATE TABLE [dbo].[sySDF]
(
[SDFId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_sySDF_SDFId] DEFAULT (newid()),
[SDFDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DTypeId] [tinyint] NOT NULL,
[Len] [int] NULL,
[Decimals] [tinyint] NULL,
[HelpText] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModuleId] [tinyint] NULL,
[ValTypeId] [tinyint] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[StatusId] [uniqueidentifier] NULL,
[IsRequired] [bit] NOT NULL CONSTRAINT [DF_sySDF_IsRequired] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[sySDF] ADD CONSTRAINT [PK_sySDF_SDFId] PRIMARY KEY CLUSTERED  ([SDFId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[sySDF] ADD CONSTRAINT [FK_sySDF_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
GRANT REFERENCES ON  [dbo].[sySDF] TO [AdvantageRole]
GRANT SELECT ON  [dbo].[sySDF] TO [AdvantageRole]
GRANT INSERT ON  [dbo].[sySDF] TO [AdvantageRole]
GRANT DELETE ON  [dbo].[sySDF] TO [AdvantageRole]
GRANT UPDATE ON  [dbo].[sySDF] TO [AdvantageRole]
GO
