CREATE TABLE [dbo].[arFERPAPage]
(
[FERPAPageId] [uniqueidentifier] NOT NULL,
[FERPACategoryId] [uniqueidentifier] NOT NULL,
[ResourceId] [smallint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arFERPAPage] ADD CONSTRAINT [PK_arFERPAPage_FERPAPageId] PRIMARY KEY CLUSTERED  ([FERPAPageId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arFERPAPage] ADD CONSTRAINT [FK_arFERPAPage_arFERPACategory_FERPACategoryId_FERPACategoryId] FOREIGN KEY ([FERPACategoryId]) REFERENCES [dbo].[arFERPACategory] ([FERPACategoryId])
GO
ALTER TABLE [dbo].[arFERPAPage] ADD CONSTRAINT [FK_arFERPAPage_syResources_ResourceId_ResourceID] FOREIGN KEY ([ResourceId]) REFERENCES [dbo].[syResources] ([ResourceID])
GO
