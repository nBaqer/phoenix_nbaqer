CREATE TABLE [dbo].[syRelations]
(
[RelationId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syRelations_RelationId] DEFAULT (newid()),
[RelationCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[RelationDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syRelations] ADD CONSTRAINT [PK_syRelations_RelationId] PRIMARY KEY CLUSTERED  ([RelationId]) ON [PRIMARY]
GO
