CREATE TABLE [dbo].[adSourceCatagory]
(
[SourceCatagoryDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusID] [uniqueidentifier] NOT NULL,
[SourceCatagoryCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SourceCatagoryId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_adSourceCatagory_SourceCatagoryId] DEFAULT (newid()),
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CampGrpId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[adSourceCatagory_Audit_Delete] ON [dbo].[adSourceCatagory]
    FOR DELETE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adSourceCatagory','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SourceCatagoryId
                               ,'SourceCatagoryCode'
                               ,CONVERT(VARCHAR(8000),Old.SourceCatagoryCode,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SourceCatagoryId
                               ,'SourceCatagoryDescrip'
                               ,CONVERT(VARCHAR(8000),Old.SourceCatagoryDescrip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SourceCatagoryId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusID,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[adSourceCatagory_Audit_Insert] ON [dbo].[adSourceCatagory]
    FOR INSERT
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adSourceCatagory','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SourceCatagoryId
                               ,'SourceCatagoryCode'
                               ,CONVERT(VARCHAR(8000),New.SourceCatagoryCode,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SourceCatagoryId
                               ,'SourceCatagoryDescrip'
                               ,CONVERT(VARCHAR(8000),New.SourceCatagoryDescrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SourceCatagoryId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusID,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
CREATE TRIGGER [dbo].[adSourceCatagory_Audit_Update] ON [dbo].[adSourceCatagory] 
    FOR UPDATE 
AS 
    SET NOCOUNT ON;  
 
    DECLARE @AuditHistId AS UNIQUEIDENTIFIER;  
    DECLARE @EventRows AS INT;  
    DECLARE @EventDate AS DATETIME;  
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();  
    SET @EventRows = ( 
                       SELECT   COUNT(*) 
                       FROM     Inserted 
                     );  
    SET @EventDate = ( 
                       SELECT TOP 1 
                                ModDate 
                       FROM     Inserted 
                     );  
    SET @UserName = ( 
                      SELECT TOP 1 
                                ModUser 
                      FROM      Inserted 
                    );  
    IF @EventRows > 0 
        BEGIN  
            EXEC fmAuditHistAdd @AuditHistId,'adSourceCatagory','U',@EventRows,@EventDate,@UserName;  
            BEGIN  
                IF UPDATE(SourceCatagoryCode) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.SourceCatagoryId 
                                   ,'SourceCatagoryCode' 
                                   ,CONVERT(VARCHAR(8000),Old.SourceCatagoryCode,121) 
                                   ,CONVERT(VARCHAR(8000),New.SourceCatagoryCode,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.SourceCatagoryId = New.SourceCatagoryId 
                            WHERE   Old.SourceCatagoryCode <> New.SourceCatagoryCode 
                                    OR ( 
                                         Old.SourceCatagoryCode IS NULL 
                                         AND New.SourceCatagoryCode IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.SourceCatagoryCode IS NULL 
                                         AND Old.SourceCatagoryCode IS NOT NULL 
                                       );  
                IF UPDATE(SourceCatagoryDescrip) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.SourceCatagoryId 
                                   ,'SourceCatagoryDescrip' 
                                   ,CONVERT(VARCHAR(8000),Old.SourceCatagoryDescrip,121) 
                                   ,CONVERT(VARCHAR(8000),New.SourceCatagoryDescrip,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.SourceCatagoryId = New.SourceCatagoryId 
                            WHERE   Old.SourceCatagoryDescrip <> New.SourceCatagoryDescrip 
                                    OR ( 
                                         Old.SourceCatagoryDescrip IS NULL 
                                         AND New.SourceCatagoryDescrip IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.SourceCatagoryDescrip IS NULL 
                                         AND Old.SourceCatagoryDescrip IS NOT NULL 
                                       );  
                IF UPDATE(StatusId) 
                    INSERT  INTO syAuditHistDetail 
                            ( 
                             AuditHistId 
                            ,RowId 
                            ,ColumnName 
                            ,OldValue 
                            ,NewValue 
                            ) 
                            SELECT  @AuditHistId 
                                   ,New.SourceCatagoryId 
                                   ,'StatusId' 
                                   ,CONVERT(VARCHAR(8000),Old.StatusID,121) 
                                   ,CONVERT(VARCHAR(8000),New.StatusID,121) 
                            FROM    Inserted New 
                            FULL OUTER JOIN Deleted Old ON Old.SourceCatagoryId = New.SourceCatagoryId 
                            WHERE   Old.StatusID <> New.StatusID 
                                    OR ( 
                                         Old.StatusID IS NULL 
                                         AND New.StatusID IS NOT NULL 
                                       ) 
                                    OR ( 
                                         New.StatusID IS NULL 
                                         AND Old.StatusID IS NOT NULL 
                                       );  
            END;  
        END;  
 
 
    SET NOCOUNT OFF;  
GO
ALTER TABLE [dbo].[adSourceCatagory] ADD CONSTRAINT [PK_adSourceCatagory_SourceCatagoryId] PRIMARY KEY CLUSTERED  ([SourceCatagoryId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_adSourceCatagory_SourceCatagoryCode_SourceCatagoryDescrip_CampGrpId] ON [dbo].[adSourceCatagory] ([SourceCatagoryCode], [SourceCatagoryDescrip], [CampGrpId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adSourceCatagory] ADD CONSTRAINT [FK_adSourceCatagory_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[adSourceCatagory] ADD CONSTRAINT [FK_adSourceCatagory_syStatuses_StatusID_StatusId] FOREIGN KEY ([StatusID]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
