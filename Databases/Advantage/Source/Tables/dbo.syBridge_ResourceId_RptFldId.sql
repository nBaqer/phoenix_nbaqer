CREATE TABLE [dbo].[syBridge_ResourceId_RptFldId]
(
[BridgeId] [int] NOT NULL IDENTITY(1, 1),
[ResourceId] [int] NULL,
[RptFldId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syBridge_ResourceId_RptFldId] ADD CONSTRAINT [PK_syBridge_ResourceId_RptFldId_BridgeId] PRIMARY KEY CLUSTERED  ([BridgeId]) ON [PRIMARY]
GO
