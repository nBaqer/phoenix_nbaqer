CREATE TABLE [dbo].[syPhoneType]
(
[PhoneTypeId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syPhoneType_PhoneTypeId] DEFAULT (newid()),
[PhoneTypeCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneTypeDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NULL,
[CampGrpId] [uniqueidentifier] NULL,
[Sequence] [int] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[syPhoneType_Audit_Delete] ON [dbo].[syPhoneType]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syPhoneType','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PhoneTypeId
                               ,'PhoneTypeDescrip'
                               ,CONVERT(VARCHAR(8000),Old.PhoneTypeDescrip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PhoneTypeId
                               ,'Sequence'
                               ,CONVERT(VARCHAR(8000),Old.Sequence,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PhoneTypeId
                               ,'PhoneTypeCode'
                               ,CONVERT(VARCHAR(8000),Old.PhoneTypeCode,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PhoneTypeId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PhoneTypeId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[syPhoneType_Audit_Insert] ON [dbo].[syPhoneType]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syPhoneType','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PhoneTypeId
                               ,'PhoneTypeDescrip'
                               ,CONVERT(VARCHAR(8000),New.PhoneTypeDescrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PhoneTypeId
                               ,'Sequence'
                               ,CONVERT(VARCHAR(8000),New.Sequence,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PhoneTypeId
                               ,'PhoneTypeCode'
                               ,CONVERT(VARCHAR(8000),New.PhoneTypeCode,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PhoneTypeId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PhoneTypeId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[syPhoneType_Audit_Update] ON [dbo].[syPhoneType]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syPhoneType','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(PhoneTypeDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PhoneTypeId
                                   ,'PhoneTypeDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.PhoneTypeDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.PhoneTypeDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PhoneTypeId = New.PhoneTypeId
                            WHERE   Old.PhoneTypeDescrip <> New.PhoneTypeDescrip
                                    OR (
                                         Old.PhoneTypeDescrip IS NULL
                                         AND New.PhoneTypeDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.PhoneTypeDescrip IS NULL
                                         AND Old.PhoneTypeDescrip IS NOT NULL
                                       ); 
                IF UPDATE(Sequence)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PhoneTypeId
                                   ,'Sequence'
                                   ,CONVERT(VARCHAR(8000),Old.Sequence,121)
                                   ,CONVERT(VARCHAR(8000),New.Sequence,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PhoneTypeId = New.PhoneTypeId
                            WHERE   Old.Sequence <> New.Sequence
                                    OR (
                                         Old.Sequence IS NULL
                                         AND New.Sequence IS NOT NULL
                                       )
                                    OR (
                                         New.Sequence IS NULL
                                         AND Old.Sequence IS NOT NULL
                                       ); 
                IF UPDATE(PhoneTypeCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PhoneTypeId
                                   ,'PhoneTypeCode'
                                   ,CONVERT(VARCHAR(8000),Old.PhoneTypeCode,121)
                                   ,CONVERT(VARCHAR(8000),New.PhoneTypeCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PhoneTypeId = New.PhoneTypeId
                            WHERE   Old.PhoneTypeCode <> New.PhoneTypeCode
                                    OR (
                                         Old.PhoneTypeCode IS NULL
                                         AND New.PhoneTypeCode IS NOT NULL
                                       )
                                    OR (
                                         New.PhoneTypeCode IS NULL
                                         AND Old.PhoneTypeCode IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PhoneTypeId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PhoneTypeId = New.PhoneTypeId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PhoneTypeId
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PhoneTypeId = New.PhoneTypeId
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[syPhoneType] ADD CONSTRAINT [PK_syPhoneType_PhoneTypeId] PRIMARY KEY CLUSTERED  ([PhoneTypeId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_syPhoneType_PhoneTypeCode_PhoneTypeDescrip_CampGrpId] ON [dbo].[syPhoneType] ([PhoneTypeCode], [PhoneTypeDescrip], [CampGrpId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syPhoneType] ADD CONSTRAINT [FK_syPhoneType_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[syPhoneType] ADD CONSTRAINT [FK_syPhoneType_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
