CREATE TABLE [dbo].[arProgScheduleDetails]
(
[ScheduleDetailId] [uniqueidentifier] NOT NULL,
[ScheduleId] [uniqueidentifier] NULL,
[dw] [smallint] NULL,
[total] [decimal] (18, 2) NULL,
[timein] [datetime] NULL,
[lunchin] [datetime] NULL,
[lunchout] [datetime] NULL,
[timeout] [datetime] NULL,
[maxnolunch] [decimal] (18, 2) NULL,
[allow_earlyin] [bit] NULL,
[allow_lateout] [bit] NULL,
[allow_extrahours] [bit] NULL,
[check_tardyin] [bit] NULL,
[max_beforetardy] [datetime] NULL,
[tardy_intime] [datetime] NULL,
[MinimumHoursToBePresent] [decimal] (18, 2) NULL CONSTRAINT [DF_arProgScheduleDetails_MinimumHoursToBePresent] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arProgScheduleDetails] ADD CONSTRAINT [PK_arProgScheduleDetails_ScheduleDetailId] PRIMARY KEY CLUSTERED  ([ScheduleDetailId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_arProgScheduleDetails_ScheduleId] ON [dbo].[arProgScheduleDetails] ([ScheduleId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arProgScheduleDetails] ADD CONSTRAINT [FK_arProgScheduleDetails_arProgSchedules_ScheduleId_ScheduleId] FOREIGN KEY ([ScheduleId]) REFERENCES [dbo].[arProgSchedules] ([ScheduleId])
GO
