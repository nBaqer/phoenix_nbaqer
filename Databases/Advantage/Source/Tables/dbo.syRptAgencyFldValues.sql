CREATE TABLE [dbo].[syRptAgencyFldValues]
(
[RptAgencyFldValId] [int] NOT NULL,
[RptAgencyFldId] [int] NULL,
[AgencyDescrip] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AgencyValue] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syRptAgencyFldValues] ADD CONSTRAINT [PK_syRptAgencyFldValues_RptAgencyFldValId] PRIMARY KEY CLUSTERED  ([RptAgencyFldValId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syRptAgencyFldValues] ADD CONSTRAINT [FK_syRptAgencyFldValues_syRptAgencyFields_RptAgencyFldId_RptAgencyFldId] FOREIGN KEY ([RptAgencyFldId]) REFERENCES [dbo].[syRptAgencyFields] ([RptAgencyFldId])
GO
