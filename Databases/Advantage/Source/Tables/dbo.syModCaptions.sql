CREATE TABLE [dbo].[syModCaptions]
(
[ModCapId] [int] NOT NULL,
[ModuleId] [tinyint] NOT NULL,
[LangId] [tinyint] NOT NULL,
[Caption] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModuleCode] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syModCaptions] ADD CONSTRAINT [PK_syModCaptions_ModCapId] PRIMARY KEY CLUSTERED  ([ModCapId]) ON [PRIMARY]
GO
