CREATE TABLE [dbo].[sySDFVisiblity]
(
[VisId] [tinyint] NOT NULL,
[SDFVisibility] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[sySDFVisiblity] ADD CONSTRAINT [PK_sySDFVisiblity_VisId] PRIMARY KEY CLUSTERED  ([VisId]) ON [PRIMARY]
GO
