CREATE TABLE [dbo].[syRptSQLCount]
(
[RptSQLCountID] [int] NOT NULL,
[RptSQLID] [int] NULL,
[RptCountStmt] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syRptSQLCount] ADD CONSTRAINT [PK_syRptSQLCount_RptSQLCountID] PRIMARY KEY CLUSTERED  ([RptSQLCountID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[syRptSQLCount] TO [AdvantageRole]
GO
