CREATE TABLE [dbo].[arPrograms]
(
[ProgId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arPrograms_ProgId] DEFAULT (newid()),
[ProgCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[ProgDescrip] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[NumPmtPeriods] [tinyint] NULL,
[CampGrpId] [uniqueidentifier] NULL,
[DegreeId] [uniqueidentifier] NULL,
[CalendarTypeId] [int] NULL,
[ACId] [int] NULL,
[shiftid] [uniqueidentifier] NULL,
[CIPCode] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CredentialLevel] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsGEProgram] [bit] NULL,
[CredentialLvlId] [uniqueidentifier] NULL,
[Is1098T] [bit] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[arPrograms_Audit_Delete] ON [dbo].[arPrograms]
    FOR DELETE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arPrograms','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ProgId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ProgId
                               ,'ProgCode'
                               ,CONVERT(VARCHAR(8000),Old.ProgCode,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ProgId
                               ,'ProgDescrip'
                               ,CONVERT(VARCHAR(8000),Old.ProgDescrip,121)
                        FROM    Deleted Old;
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ProgId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
            END; 
        END; 



    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[arPrograms_Audit_Insert] ON [dbo].[arPrograms]
    FOR INSERT
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arPrograms','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ProgId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ProgId
                               ,'ProgCode'
                               ,CONVERT(VARCHAR(8000),New.ProgCode,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ProgId
                               ,'ProgDescrip'
                               ,CONVERT(VARCHAR(8000),New.ProgDescrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ProgId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New;
            END; 
        END; 



    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[arPrograms_Audit_Update] ON [dbo].[arPrograms]
    FOR UPDATE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arPrograms','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ProgId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ProgId = New.ProgId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(ProgCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ProgId
                                   ,'ProgCode'
                                   ,CONVERT(VARCHAR(8000),Old.ProgCode,121)
                                   ,CONVERT(VARCHAR(8000),New.ProgCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ProgId = New.ProgId
                            WHERE   Old.ProgCode <> New.ProgCode
                                    OR (
                                         Old.ProgCode IS NULL
                                         AND New.ProgCode IS NOT NULL
                                       )
                                    OR (
                                         New.ProgCode IS NULL
                                         AND Old.ProgCode IS NOT NULL
                                       ); 
                IF UPDATE(ProgDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ProgId
                                   ,'ProgDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.ProgDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.ProgDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ProgId = New.ProgId
                            WHERE   Old.ProgDescrip <> New.ProgDescrip
                                    OR (
                                         Old.ProgDescrip IS NULL
                                         AND New.ProgDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.ProgDescrip IS NULL
                                         AND Old.ProgDescrip IS NOT NULL
                                       ); 
                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ProgId
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ProgId = New.ProgId
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 
            END; 
        END; 



    SET NOCOUNT OFF;

GO
ALTER TABLE [dbo].[arPrograms] ADD CONSTRAINT [PK_arPrograms_ProgId] PRIMARY KEY CLUSTERED  ([ProgId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_arPrograms_ProgCode_ProgDescrip_CampGrpId] ON [dbo].[arPrograms] ([ProgCode], [ProgDescrip], [CampGrpId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arPrograms] ADD CONSTRAINT [FK_arPrograms_arProgCredential_CredentialLvlId_CredentialId] FOREIGN KEY ([CredentialLvlId]) REFERENCES [dbo].[arProgCredential] ([CredentialId])
GO
ALTER TABLE [dbo].[arPrograms] ADD CONSTRAINT [FK_arPrograms_syAcademicCalendars_ACId_ACId] FOREIGN KEY ([ACId]) REFERENCES [dbo].[syAcademicCalendars] ([ACId])
GO
ALTER TABLE [dbo].[arPrograms] ADD CONSTRAINT [FK_arPrograms_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[arPrograms] ADD CONSTRAINT [FK_arPrograms_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
