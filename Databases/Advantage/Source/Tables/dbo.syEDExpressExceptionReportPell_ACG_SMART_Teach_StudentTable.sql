CREATE TABLE [dbo].[syEDExpressExceptionReportPell_ACG_SMART_Teach_StudentTable]
(
[ExceptionReportId] [uniqueidentifier] NOT NULL,
[DbIn] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Filter] [varchar] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AwardAmount] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AwardId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GrantType] [varchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddDate] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddTime] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginalSSN] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateDate] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateTime] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OriginationStatus] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcademicYearId] [uniqueidentifier] NULL,
[AwardYearStartDate] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AwardYearEndDate] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[ErrorMsg] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExceptionGUID] [uniqueidentifier] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[syEDExpressExceptionReportPell_ACG_SMART_Teach_StudentTable] ADD CONSTRAINT [PK_syEDExpressExceptionReportPell_ACG_SMART_Teach_StudentTable_ExceptionReportId] PRIMARY KEY CLUSTERED  ([ExceptionReportId]) ON [PRIMARY]
GO
