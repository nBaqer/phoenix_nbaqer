CREATE TABLE [dbo].[ParamItemProp]
(
[itempropertyid] [bigint] NOT NULL IDENTITY(1, 1),
[itemid] [bigint] NOT NULL,
[childcontrol] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[propname] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[value] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[valuetype] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[ParamItemProp] ADD CONSTRAINT [PK_ParamItemProp_itempropertyid] PRIMARY KEY CLUSTERED  ([itempropertyid]) ON [PRIMARY]
GO
