CREATE TABLE [dbo].[plEmployerContact]
(
[EmployerContactId] [uniqueidentifier] NOT NULL,
[FirstName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MiddleName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusId] [uniqueidentifier] NULL,
[PrefixId] [uniqueidentifier] NULL,
[SuffixId] [uniqueidentifier] NULL,
[WorkPhone] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HomePhone] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CellPhone] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Beeper] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WorkEmail] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HomeEmail] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[Address1] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [uniqueidentifier] NULL,
[Zip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Country] [uniqueidentifier] NULL,
[WorkExt] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WorkBestTime] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HomeExt] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HomeBestTime] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CellBestTime] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PinNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Notes] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddressTypeID] [uniqueidentifier] NULL,
[EmployerId] [uniqueidentifier] NULL,
[OtherState] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ForeignZip] [bit] NULL,
[ForeignHomePhone] [bit] NULL,
[ForeignCellPhone] [bit] NULL,
[ForeignWorkPhone] [bit] NULL,
[TitleId] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[plEmployerContact_Audit_Delete] ON [dbo].[plEmployerContact]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'plEmployerContact','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerContactId
                               ,'FirstName'
                               ,CONVERT(VARCHAR(8000),Old.FirstName,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerContactId
                               ,'LastName'
                               ,CONVERT(VARCHAR(8000),Old.LastName,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerContactId
                               ,'MiddleName'
                               ,CONVERT(VARCHAR(8000),Old.MiddleName,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerContactId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerContactId
                               ,'PrefixId'
                               ,CONVERT(VARCHAR(8000),Old.PrefixId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerContactId
                               ,'SuffixId'
                               ,CONVERT(VARCHAR(8000),Old.SuffixId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerContactId
                               ,'TitleId'
                               ,CONVERT(VARCHAR(8000),Old.TitleId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerContactId
                               ,'WorkPhone'
                               ,CONVERT(VARCHAR(8000),Old.WorkPhone,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerContactId
                               ,'HomePhone'
                               ,CONVERT(VARCHAR(8000),Old.HomePhone,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerContactId
                               ,'CellPhone'
                               ,CONVERT(VARCHAR(8000),Old.CellPhone,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerContactId
                               ,'Beeper'
                               ,CONVERT(VARCHAR(8000),Old.Beeper,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerContactId
                               ,'WorkEmail'
                               ,CONVERT(VARCHAR(8000),Old.WorkEmail,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerContactId
                               ,'HomeEmail'
                               ,CONVERT(VARCHAR(8000),Old.HomeEmail,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerContactId
                               ,'Address1'
                               ,CONVERT(VARCHAR(8000),Old.Address1,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerContactId
                               ,'Address2'
                               ,CONVERT(VARCHAR(8000),Old.Address2,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerContactId
                               ,'City'
                               ,CONVERT(VARCHAR(8000),Old.City,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerContactId
                               ,'state'
                               ,CONVERT(VARCHAR(8000),Old.State,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerContactId
                               ,'Zip'
                               ,CONVERT(VARCHAR(8000),Old.Zip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerContactId
                               ,'Country'
                               ,CONVERT(VARCHAR(8000),Old.Country,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerContactId
                               ,'WorkExt'
                               ,CONVERT(VARCHAR(8000),Old.WorkExt,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerContactId
                               ,'WorkBestTime'
                               ,CONVERT(VARCHAR(8000),Old.WorkBestTime,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerContactId
                               ,'HomeExt'
                               ,CONVERT(VARCHAR(8000),Old.HomeExt,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerContactId
                               ,'HomeBestTime'
                               ,CONVERT(VARCHAR(8000),Old.HomeBestTime,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerContactId
                               ,'CellBestTime'
                               ,CONVERT(VARCHAR(8000),Old.CellBestTime,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerContactId
                               ,'PinNumber'
                               ,CONVERT(VARCHAR(8000),Old.PinNumber,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerContactId
                               ,'Notes'
                               ,CONVERT(VARCHAR(8000),Old.Notes,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerContactId
                               ,'AddressTypeId'
                               ,CONVERT(VARCHAR(8000),Old.AddressTypeID,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerContactId
                               ,'EmployerId'
                               ,CONVERT(VARCHAR(8000),Old.EmployerId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerContactId
                               ,'ForeignHomePhone'
                               ,CONVERT(VARCHAR(8000),Old.ForeignHomePhone,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerContactId
                               ,'ForeignCellPhone'
                               ,CONVERT(VARCHAR(8000),Old.ForeignCellPhone,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerContactId
                               ,'ForeignWorkPhone'
                               ,CONVERT(VARCHAR(8000),Old.ForeignWorkPhone,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerContactId
                               ,'ForeignZip'
                               ,CONVERT(VARCHAR(8000),Old.ForeignZip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EmployerContactId
                               ,'OtherState'
                               ,CONVERT(VARCHAR(8000),Old.OtherState,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[plEmployerContact_Audit_Insert] ON [dbo].[plEmployerContact]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'plEmployerContact','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerContactId
                               ,'FirstName'
                               ,CONVERT(VARCHAR(8000),New.FirstName,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerContactId
                               ,'LastName'
                               ,CONVERT(VARCHAR(8000),New.LastName,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerContactId
                               ,'MiddleName'
                               ,CONVERT(VARCHAR(8000),New.MiddleName,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerContactId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerContactId
                               ,'PrefixId'
                               ,CONVERT(VARCHAR(8000),New.PrefixId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerContactId
                               ,'SuffixId'
                               ,CONVERT(VARCHAR(8000),New.SuffixId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerContactId
                               ,'TitleId'
                               ,CONVERT(VARCHAR(8000),New.TitleId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerContactId
                               ,'WorkPhone'
                               ,CONVERT(VARCHAR(8000),New.WorkPhone,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerContactId
                               ,'HomePhone'
                               ,CONVERT(VARCHAR(8000),New.HomePhone,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerContactId
                               ,'CellPhone'
                               ,CONVERT(VARCHAR(8000),New.CellPhone,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerContactId
                               ,'Beeper'
                               ,CONVERT(VARCHAR(8000),New.Beeper,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerContactId
                               ,'WorkEmail'
                               ,CONVERT(VARCHAR(8000),New.WorkEmail,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerContactId
                               ,'HomeEmail'
                               ,CONVERT(VARCHAR(8000),New.HomeEmail,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerContactId
                               ,'Address1'
                               ,CONVERT(VARCHAR(8000),New.Address1,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerContactId
                               ,'Address2'
                               ,CONVERT(VARCHAR(8000),New.Address2,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerContactId
                               ,'City'
                               ,CONVERT(VARCHAR(8000),New.City,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerContactId
                               ,'state'
                               ,CONVERT(VARCHAR(8000),New.State,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerContactId
                               ,'Zip'
                               ,CONVERT(VARCHAR(8000),New.Zip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerContactId
                               ,'Country'
                               ,CONVERT(VARCHAR(8000),New.Country,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerContactId
                               ,'WorkExt'
                               ,CONVERT(VARCHAR(8000),New.WorkExt,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerContactId
                               ,'WorkBestTime'
                               ,CONVERT(VARCHAR(8000),New.WorkBestTime,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerContactId
                               ,'HomeExt'
                               ,CONVERT(VARCHAR(8000),New.HomeExt,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerContactId
                               ,'HomeBestTime'
                               ,CONVERT(VARCHAR(8000),New.HomeBestTime,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerContactId
                               ,'CellBestTime'
                               ,CONVERT(VARCHAR(8000),New.CellBestTime,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerContactId
                               ,'PinNumber'
                               ,CONVERT(VARCHAR(8000),New.PinNumber,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerContactId
                               ,'Notes'
                               ,CONVERT(VARCHAR(8000),New.Notes,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerContactId
                               ,'AddressTypeId'
                               ,CONVERT(VARCHAR(8000),New.AddressTypeID,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerContactId
                               ,'EmployerId'
                               ,CONVERT(VARCHAR(8000),New.EmployerId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerContactId
                               ,'ForeignHomePhone'
                               ,CONVERT(VARCHAR(8000),New.ForeignHomePhone,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerContactId
                               ,'ForeignCellPhone'
                               ,CONVERT(VARCHAR(8000),New.ForeignCellPhone,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerContactId
                               ,'ForeignWorkPhone'
                               ,CONVERT(VARCHAR(8000),New.ForeignWorkPhone,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerContactId
                               ,'ForeignZip'
                               ,CONVERT(VARCHAR(8000),New.ForeignZip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EmployerContactId
                               ,'OtherState'
                               ,CONVERT(VARCHAR(8000),New.OtherState,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[plEmployerContact_Audit_Update] ON [dbo].[plEmployerContact]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'plEmployerContact','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(FirstName)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerContactId
                                   ,'FirstName'
                                   ,CONVERT(VARCHAR(8000),Old.FirstName,121)
                                   ,CONVERT(VARCHAR(8000),New.FirstName,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerContactId = New.EmployerContactId
                            WHERE   Old.FirstName <> New.FirstName
                                    OR (
                                         Old.FirstName IS NULL
                                         AND New.FirstName IS NOT NULL
                                       )
                                    OR (
                                         New.FirstName IS NULL
                                         AND Old.FirstName IS NOT NULL
                                       ); 
                IF UPDATE(LastName)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerContactId
                                   ,'LastName'
                                   ,CONVERT(VARCHAR(8000),Old.LastName,121)
                                   ,CONVERT(VARCHAR(8000),New.LastName,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerContactId = New.EmployerContactId
                            WHERE   Old.LastName <> New.LastName
                                    OR (
                                         Old.LastName IS NULL
                                         AND New.LastName IS NOT NULL
                                       )
                                    OR (
                                         New.LastName IS NULL
                                         AND Old.LastName IS NOT NULL
                                       ); 
                IF UPDATE(MiddleName)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerContactId
                                   ,'MiddleName'
                                   ,CONVERT(VARCHAR(8000),Old.MiddleName,121)
                                   ,CONVERT(VARCHAR(8000),New.MiddleName,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerContactId = New.EmployerContactId
                            WHERE   Old.MiddleName <> New.MiddleName
                                    OR (
                                         Old.MiddleName IS NULL
                                         AND New.MiddleName IS NOT NULL
                                       )
                                    OR (
                                         New.MiddleName IS NULL
                                         AND Old.MiddleName IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerContactId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerContactId = New.EmployerContactId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(PrefixId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerContactId
                                   ,'PrefixId'
                                   ,CONVERT(VARCHAR(8000),Old.PrefixId,121)
                                   ,CONVERT(VARCHAR(8000),New.PrefixId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerContactId = New.EmployerContactId
                            WHERE   Old.PrefixId <> New.PrefixId
                                    OR (
                                         Old.PrefixId IS NULL
                                         AND New.PrefixId IS NOT NULL
                                       )
                                    OR (
                                         New.PrefixId IS NULL
                                         AND Old.PrefixId IS NOT NULL
                                       ); 
                IF UPDATE(SuffixId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerContactId
                                   ,'SuffixId'
                                   ,CONVERT(VARCHAR(8000),Old.SuffixId,121)
                                   ,CONVERT(VARCHAR(8000),New.SuffixId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerContactId = New.EmployerContactId
                            WHERE   Old.SuffixId <> New.SuffixId
                                    OR (
                                         Old.SuffixId IS NULL
                                         AND New.SuffixId IS NOT NULL
                                       )
                                    OR (
                                         New.SuffixId IS NULL
                                         AND Old.SuffixId IS NOT NULL
                                       ); 
                IF UPDATE(TitleId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerContactId
                                   ,'TitleId'
                                   ,CONVERT(VARCHAR(8000),Old.TitleId,121)
                                   ,CONVERT(VARCHAR(8000),New.TitleId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerContactId = New.EmployerContactId
                            WHERE   Old.TitleId <> New.TitleId
                                    OR (
                                         Old.TitleId IS NULL
                                         AND New.TitleId IS NOT NULL
                                       )
                                    OR (
                                         New.TitleId IS NULL
                                         AND Old.TitleId IS NOT NULL
                                       ); 
                IF UPDATE(WorkPhone)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerContactId
                                   ,'WorkPhone'
                                   ,CONVERT(VARCHAR(8000),Old.WorkPhone,121)
                                   ,CONVERT(VARCHAR(8000),New.WorkPhone,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerContactId = New.EmployerContactId
                            WHERE   Old.WorkPhone <> New.WorkPhone
                                    OR (
                                         Old.WorkPhone IS NULL
                                         AND New.WorkPhone IS NOT NULL
                                       )
                                    OR (
                                         New.WorkPhone IS NULL
                                         AND Old.WorkPhone IS NOT NULL
                                       ); 
                IF UPDATE(HomePhone)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerContactId
                                   ,'HomePhone'
                                   ,CONVERT(VARCHAR(8000),Old.HomePhone,121)
                                   ,CONVERT(VARCHAR(8000),New.HomePhone,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerContactId = New.EmployerContactId
                            WHERE   Old.HomePhone <> New.HomePhone
                                    OR (
                                         Old.HomePhone IS NULL
                                         AND New.HomePhone IS NOT NULL
                                       )
                                    OR (
                                         New.HomePhone IS NULL
                                         AND Old.HomePhone IS NOT NULL
                                       ); 
                IF UPDATE(CellPhone)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerContactId
                                   ,'CellPhone'
                                   ,CONVERT(VARCHAR(8000),Old.CellPhone,121)
                                   ,CONVERT(VARCHAR(8000),New.CellPhone,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerContactId = New.EmployerContactId
                            WHERE   Old.CellPhone <> New.CellPhone
                                    OR (
                                         Old.CellPhone IS NULL
                                         AND New.CellPhone IS NOT NULL
                                       )
                                    OR (
                                         New.CellPhone IS NULL
                                         AND Old.CellPhone IS NOT NULL
                                       ); 
                IF UPDATE(Beeper)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerContactId
                                   ,'Beeper'
                                   ,CONVERT(VARCHAR(8000),Old.Beeper,121)
                                   ,CONVERT(VARCHAR(8000),New.Beeper,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerContactId = New.EmployerContactId
                            WHERE   Old.Beeper <> New.Beeper
                                    OR (
                                         Old.Beeper IS NULL
                                         AND New.Beeper IS NOT NULL
                                       )
                                    OR (
                                         New.Beeper IS NULL
                                         AND Old.Beeper IS NOT NULL
                                       ); 
                IF UPDATE(WorkEmail)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerContactId
                                   ,'WorkEmail'
                                   ,CONVERT(VARCHAR(8000),Old.WorkEmail,121)
                                   ,CONVERT(VARCHAR(8000),New.WorkEmail,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerContactId = New.EmployerContactId
                            WHERE   Old.WorkEmail <> New.WorkEmail
                                    OR (
                                         Old.WorkEmail IS NULL
                                         AND New.WorkEmail IS NOT NULL
                                       )
                                    OR (
                                         New.WorkEmail IS NULL
                                         AND Old.WorkEmail IS NOT NULL
                                       ); 
                IF UPDATE(HomeEmail)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerContactId
                                   ,'HomeEmail'
                                   ,CONVERT(VARCHAR(8000),Old.HomeEmail,121)
                                   ,CONVERT(VARCHAR(8000),New.HomeEmail,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerContactId = New.EmployerContactId
                            WHERE   Old.HomeEmail <> New.HomeEmail
                                    OR (
                                         Old.HomeEmail IS NULL
                                         AND New.HomeEmail IS NOT NULL
                                       )
                                    OR (
                                         New.HomeEmail IS NULL
                                         AND Old.HomeEmail IS NOT NULL
                                       ); 
                IF UPDATE(Address1)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerContactId
                                   ,'Address1'
                                   ,CONVERT(VARCHAR(8000),Old.Address1,121)
                                   ,CONVERT(VARCHAR(8000),New.Address1,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerContactId = New.EmployerContactId
                            WHERE   Old.Address1 <> New.Address1
                                    OR (
                                         Old.Address1 IS NULL
                                         AND New.Address1 IS NOT NULL
                                       )
                                    OR (
                                         New.Address1 IS NULL
                                         AND Old.Address1 IS NOT NULL
                                       ); 
                IF UPDATE(Address2)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerContactId
                                   ,'Address2'
                                   ,CONVERT(VARCHAR(8000),Old.Address2,121)
                                   ,CONVERT(VARCHAR(8000),New.Address2,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerContactId = New.EmployerContactId
                            WHERE   Old.Address2 <> New.Address2
                                    OR (
                                         Old.Address2 IS NULL
                                         AND New.Address2 IS NOT NULL
                                       )
                                    OR (
                                         New.Address2 IS NULL
                                         AND Old.Address2 IS NOT NULL
                                       ); 
                IF UPDATE(City)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerContactId
                                   ,'City'
                                   ,CONVERT(VARCHAR(8000),Old.City,121)
                                   ,CONVERT(VARCHAR(8000),New.City,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerContactId = New.EmployerContactId
                            WHERE   Old.City <> New.City
                                    OR (
                                         Old.City IS NULL
                                         AND New.City IS NOT NULL
                                       )
                                    OR (
                                         New.City IS NULL
                                         AND Old.City IS NOT NULL
                                       ); 
                IF UPDATE(State)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerContactId
                                   ,'state'
                                   ,CONVERT(VARCHAR(8000),Old.State,121)
                                   ,CONVERT(VARCHAR(8000),New.State,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerContactId = New.EmployerContactId
                            WHERE   Old.State <> New.State
                                    OR (
                                         Old.State IS NULL
                                         AND New.State IS NOT NULL
                                       )
                                    OR (
                                         New.State IS NULL
                                         AND Old.State IS NOT NULL
                                       ); 
                IF UPDATE(Zip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerContactId
                                   ,'Zip'
                                   ,CONVERT(VARCHAR(8000),Old.Zip,121)
                                   ,CONVERT(VARCHAR(8000),New.Zip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerContactId = New.EmployerContactId
                            WHERE   Old.Zip <> New.Zip
                                    OR (
                                         Old.Zip IS NULL
                                         AND New.Zip IS NOT NULL
                                       )
                                    OR (
                                         New.Zip IS NULL
                                         AND Old.Zip IS NOT NULL
                                       ); 
                IF UPDATE(Country)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerContactId
                                   ,'Country'
                                   ,CONVERT(VARCHAR(8000),Old.Country,121)
                                   ,CONVERT(VARCHAR(8000),New.Country,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerContactId = New.EmployerContactId
                            WHERE   Old.Country <> New.Country
                                    OR (
                                         Old.Country IS NULL
                                         AND New.Country IS NOT NULL
                                       )
                                    OR (
                                         New.Country IS NULL
                                         AND Old.Country IS NOT NULL
                                       ); 
                IF UPDATE(WorkExt)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerContactId
                                   ,'WorkExt'
                                   ,CONVERT(VARCHAR(8000),Old.WorkExt,121)
                                   ,CONVERT(VARCHAR(8000),New.WorkExt,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerContactId = New.EmployerContactId
                            WHERE   Old.WorkExt <> New.WorkExt
                                    OR (
                                         Old.WorkExt IS NULL
                                         AND New.WorkExt IS NOT NULL
                                       )
                                    OR (
                                         New.WorkExt IS NULL
                                         AND Old.WorkExt IS NOT NULL
                                       ); 
                IF UPDATE(WorkBestTime)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerContactId
                                   ,'WorkBestTime'
                                   ,CONVERT(VARCHAR(8000),Old.WorkBestTime,121)
                                   ,CONVERT(VARCHAR(8000),New.WorkBestTime,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerContactId = New.EmployerContactId
                            WHERE   Old.WorkBestTime <> New.WorkBestTime
                                    OR (
                                         Old.WorkBestTime IS NULL
                                         AND New.WorkBestTime IS NOT NULL
                                       )
                                    OR (
                                         New.WorkBestTime IS NULL
                                         AND Old.WorkBestTime IS NOT NULL
                                       ); 
                IF UPDATE(HomeExt)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerContactId
                                   ,'HomeExt'
                                   ,CONVERT(VARCHAR(8000),Old.HomeExt,121)
                                   ,CONVERT(VARCHAR(8000),New.HomeExt,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerContactId = New.EmployerContactId
                            WHERE   Old.HomeExt <> New.HomeExt
                                    OR (
                                         Old.HomeExt IS NULL
                                         AND New.HomeExt IS NOT NULL
                                       )
                                    OR (
                                         New.HomeExt IS NULL
                                         AND Old.HomeExt IS NOT NULL
                                       ); 
                IF UPDATE(HomeBestTime)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerContactId
                                   ,'HomeBestTime'
                                   ,CONVERT(VARCHAR(8000),Old.HomeBestTime,121)
                                   ,CONVERT(VARCHAR(8000),New.HomeBestTime,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerContactId = New.EmployerContactId
                            WHERE   Old.HomeBestTime <> New.HomeBestTime
                                    OR (
                                         Old.HomeBestTime IS NULL
                                         AND New.HomeBestTime IS NOT NULL
                                       )
                                    OR (
                                         New.HomeBestTime IS NULL
                                         AND Old.HomeBestTime IS NOT NULL
                                       ); 
                IF UPDATE(CellBestTime)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerContactId
                                   ,'CellBestTime'
                                   ,CONVERT(VARCHAR(8000),Old.CellBestTime,121)
                                   ,CONVERT(VARCHAR(8000),New.CellBestTime,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerContactId = New.EmployerContactId
                            WHERE   Old.CellBestTime <> New.CellBestTime
                                    OR (
                                         Old.CellBestTime IS NULL
                                         AND New.CellBestTime IS NOT NULL
                                       )
                                    OR (
                                         New.CellBestTime IS NULL
                                         AND Old.CellBestTime IS NOT NULL
                                       ); 
                IF UPDATE(PinNumber)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerContactId
                                   ,'PinNumber'
                                   ,CONVERT(VARCHAR(8000),Old.PinNumber,121)
                                   ,CONVERT(VARCHAR(8000),New.PinNumber,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerContactId = New.EmployerContactId
                            WHERE   Old.PinNumber <> New.PinNumber
                                    OR (
                                         Old.PinNumber IS NULL
                                         AND New.PinNumber IS NOT NULL
                                       )
                                    OR (
                                         New.PinNumber IS NULL
                                         AND Old.PinNumber IS NOT NULL
                                       ); 
                IF UPDATE(Notes)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerContactId
                                   ,'Notes'
                                   ,CONVERT(VARCHAR(8000),Old.Notes,121)
                                   ,CONVERT(VARCHAR(8000),New.Notes,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerContactId = New.EmployerContactId
                            WHERE   Old.Notes <> New.Notes
                                    OR (
                                         Old.Notes IS NULL
                                         AND New.Notes IS NOT NULL
                                       )
                                    OR (
                                         New.Notes IS NULL
                                         AND Old.Notes IS NOT NULL
                                       ); 
                IF UPDATE(AddressTypeId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerContactId
                                   ,'AddressTypeId'
                                   ,CONVERT(VARCHAR(8000),Old.AddressTypeID,121)
                                   ,CONVERT(VARCHAR(8000),New.AddressTypeID,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerContactId = New.EmployerContactId
                            WHERE   Old.AddressTypeID <> New.AddressTypeID
                                    OR (
                                         Old.AddressTypeID IS NULL
                                         AND New.AddressTypeID IS NOT NULL
                                       )
                                    OR (
                                         New.AddressTypeID IS NULL
                                         AND Old.AddressTypeID IS NOT NULL
                                       ); 
                IF UPDATE(EmployerId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerContactId
                                   ,'EmployerId'
                                   ,CONVERT(VARCHAR(8000),Old.EmployerId,121)
                                   ,CONVERT(VARCHAR(8000),New.EmployerId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerContactId = New.EmployerContactId
                            WHERE   Old.EmployerId <> New.EmployerId
                                    OR (
                                         Old.EmployerId IS NULL
                                         AND New.EmployerId IS NOT NULL
                                       )
                                    OR (
                                         New.EmployerId IS NULL
                                         AND Old.EmployerId IS NOT NULL
                                       ); 
                IF UPDATE(ForeignHomePhone)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerContactId
                                   ,'ForeignHomePhone'
                                   ,CONVERT(VARCHAR(8000),Old.ForeignHomePhone,121)
                                   ,CONVERT(VARCHAR(8000),New.ForeignHomePhone,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerContactId = New.EmployerContactId
                            WHERE   Old.ForeignHomePhone <> New.ForeignHomePhone
                                    OR (
                                         Old.ForeignHomePhone IS NULL
                                         AND New.ForeignHomePhone IS NOT NULL
                                       )
                                    OR (
                                         New.ForeignHomePhone IS NULL
                                         AND Old.ForeignHomePhone IS NOT NULL
                                       ); 
                IF UPDATE(ForeignCellPhone)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerContactId
                                   ,'ForeignCellPhone'
                                   ,CONVERT(VARCHAR(8000),Old.ForeignCellPhone,121)
                                   ,CONVERT(VARCHAR(8000),New.ForeignCellPhone,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerContactId = New.EmployerContactId
                            WHERE   Old.ForeignCellPhone <> New.ForeignCellPhone
                                    OR (
                                         Old.ForeignCellPhone IS NULL
                                         AND New.ForeignCellPhone IS NOT NULL
                                       )
                                    OR (
                                         New.ForeignCellPhone IS NULL
                                         AND Old.ForeignCellPhone IS NOT NULL
                                       ); 
                IF UPDATE(ForeignWorkPhone)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerContactId
                                   ,'ForeignWorkPhone'
                                   ,CONVERT(VARCHAR(8000),Old.ForeignWorkPhone,121)
                                   ,CONVERT(VARCHAR(8000),New.ForeignWorkPhone,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerContactId = New.EmployerContactId
                            WHERE   Old.ForeignWorkPhone <> New.ForeignWorkPhone
                                    OR (
                                         Old.ForeignWorkPhone IS NULL
                                         AND New.ForeignWorkPhone IS NOT NULL
                                       )
                                    OR (
                                         New.ForeignWorkPhone IS NULL
                                         AND Old.ForeignWorkPhone IS NOT NULL
                                       ); 
                IF UPDATE(ForeignZip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerContactId
                                   ,'ForeignZip'
                                   ,CONVERT(VARCHAR(8000),Old.ForeignZip,121)
                                   ,CONVERT(VARCHAR(8000),New.ForeignZip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerContactId = New.EmployerContactId
                            WHERE   Old.ForeignZip <> New.ForeignZip
                                    OR (
                                         Old.ForeignZip IS NULL
                                         AND New.ForeignZip IS NOT NULL
                                       )
                                    OR (
                                         New.ForeignZip IS NULL
                                         AND Old.ForeignZip IS NOT NULL
                                       ); 
                IF UPDATE(OtherState)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.EmployerContactId
                                   ,'OtherState'
                                   ,CONVERT(VARCHAR(8000),Old.OtherState,121)
                                   ,CONVERT(VARCHAR(8000),New.OtherState,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.EmployerContactId = New.EmployerContactId
                            WHERE   Old.OtherState <> New.OtherState
                                    OR (
                                         Old.OtherState IS NULL
                                         AND New.OtherState IS NOT NULL
                                       )
                                    OR (
                                         New.OtherState IS NULL
                                         AND Old.OtherState IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[plEmployerContact] ADD CONSTRAINT [PK_plEmployerContact_EmployerContactId] PRIMARY KEY CLUSTERED  ([EmployerContactId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[plEmployerContact] ADD CONSTRAINT [FK_plEmployerContact_plAddressTypes_AddressTypeID_AddressTypeId] FOREIGN KEY ([AddressTypeID]) REFERENCES [dbo].[plAddressTypes] ([AddressTypeId])
GO
ALTER TABLE [dbo].[plEmployerContact] ADD CONSTRAINT [FK_plEmployerContact_adCountries_Country_CountryId] FOREIGN KEY ([Country]) REFERENCES [dbo].[adCountries] ([CountryId])
GO
ALTER TABLE [dbo].[plEmployerContact] ADD CONSTRAINT [FK_plEmployerContact_plEmployers_EmployerId_EmployerId] FOREIGN KEY ([EmployerId]) REFERENCES [dbo].[plEmployers] ([EmployerId])
GO
ALTER TABLE [dbo].[plEmployerContact] ADD CONSTRAINT [FK_plEmployerContact_syPrefixes_PrefixId_PrefixId] FOREIGN KEY ([PrefixId]) REFERENCES [dbo].[syPrefixes] ([PrefixId])
GO
ALTER TABLE [dbo].[plEmployerContact] ADD CONSTRAINT [FK_plEmployerContact_syStates_State_StateId] FOREIGN KEY ([State]) REFERENCES [dbo].[syStates] ([StateId])
GO
ALTER TABLE [dbo].[plEmployerContact] ADD CONSTRAINT [FK_plEmployerContact_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
ALTER TABLE [dbo].[plEmployerContact] ADD CONSTRAINT [FK_plEmployerContact_sySuffixes_SuffixId_SuffixId] FOREIGN KEY ([SuffixId]) REFERENCES [dbo].[sySuffixes] ([SuffixId])
GO
