CREATE TABLE [dbo].[adStuGrpTypes]
(
[GrpTypeID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_adStuGrpTypes_GrpTypeID] DEFAULT (newsequentialid()),
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adStuGrpTypes] ADD CONSTRAINT [PK_adStuGrpTypes_GrpTypeID] PRIMARY KEY CLUSTERED  ([GrpTypeID]) ON [PRIMARY]
GO
