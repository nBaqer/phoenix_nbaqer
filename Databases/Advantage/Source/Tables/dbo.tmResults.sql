CREATE TABLE [dbo].[tmResults]
(
[ResultId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_tmResults_ResultId] DEFAULT (newid()),
[Code] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResultActionId] [uniqueidentifier] NULL,
[ResultActionValue] [varchar] (256) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Active] [tinyint] NULL,
[ModDate] [datetime] NULL,
[ModUser] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tmResults] ADD CONSTRAINT [PK_tmResults_ResultId] PRIMARY KEY CLUSTERED  ([ResultId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_tmResults_Code_Descrip] ON [dbo].[tmResults] ([Code], [Descrip]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'boolean specifying if result is active', 'SCHEMA', N'dbo', 'TABLE', N'tmResults', 'COLUMN', N'Active'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Description of the result', 'SCHEMA', N'dbo', 'TABLE', N'tmResults', 'COLUMN', N'Descrip'
GO
