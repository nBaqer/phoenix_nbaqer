CREATE TABLE [dbo].[syRptPrefsIPEDS_SFAPR]
(
[RptPrefId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_syRptPrefsIPEDS_SFAPR_RptPrefId] DEFAULT (newid()),
[PrefId] [uniqueidentifier] NOT NULL,
[FundSourceId] [uniqueidentifier] NOT NULL,
[RptCatId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syRptPrefsIPEDS_SFAPR] ADD CONSTRAINT [PK_syRptPrefsIPEDS_SFAPR_RptPrefId] PRIMARY KEY CLUSTERED  ([RptPrefId]) ON [PRIMARY]
GO
