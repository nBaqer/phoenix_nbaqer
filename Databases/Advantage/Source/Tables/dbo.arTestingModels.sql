CREATE TABLE [dbo].[arTestingModels]
(
[TestingModelId] [tinyint] NOT NULL,
[TestingModelDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arTestingModels] ADD CONSTRAINT [PK_arTestingModels_TestingModelId] PRIMARY KEY CLUSTERED  ([TestingModelId]) ON [PRIMARY]
GO
