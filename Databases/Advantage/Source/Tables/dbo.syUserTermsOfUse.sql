CREATE TABLE [dbo].[syUserTermsOfUse]
(
[UserTermsOfUseId] [int] NOT NULL IDENTITY(1, 1),
[UserId] [uniqueidentifier] NOT NULL,
[Version] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AcceptedDate] [datetime] NOT NULL CONSTRAINT [DE_syUserTermsOfUse_AcceptedDate] DEFAULT (getdate()),
[ModDate] [datetime] NOT NULL CONSTRAINT [DF_syUserTermsOfUse_ModDate] DEFAULT (getdate()),
[ModUser] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syUserTermsOfUse] ADD CONSTRAINT [PK_syUserTermsOfUse_UserTermsOfUseId] PRIMARY KEY CLUSTERED  ([UserTermsOfUseId]) ON [PRIMARY]
GO
