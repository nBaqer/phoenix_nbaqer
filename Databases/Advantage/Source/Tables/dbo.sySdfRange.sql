CREATE TABLE [dbo].[sySdfRange]
(
[SdfRangeId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_sySdfRange_SdfRangeId] DEFAULT (newid()),
[SDFId] [uniqueidentifier] NOT NULL,
[MinVal] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MaxVal] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Moduser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[sySdfRange] ADD CONSTRAINT [PK_sySdfRange_SdfRangeId] PRIMARY KEY CLUSTERED  ([SdfRangeId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[sySdfRange] ADD CONSTRAINT [FK_sySdfRange_sySDF_SDFId_SDFId] FOREIGN KEY ([SDFId]) REFERENCES [dbo].[sySDF] ([SDFId]) ON DELETE CASCADE
GO
