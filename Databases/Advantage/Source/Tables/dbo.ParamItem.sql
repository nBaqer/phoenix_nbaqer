CREATE TABLE [dbo].[ParamItem]
(
[ItemId] [bigint] NOT NULL IDENTITY(1, 1),
[Caption] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ControllerClass] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[valueprop] [int] NOT NULL,
[ReturnValueName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IsItemOverriden] [bit] NULL CONSTRAINT [DF_ParamItem_IsItemOverriden] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ParamItem] ADD CONSTRAINT [PK_ParamItem_ItemId] PRIMARY KEY CLUSTERED  ([ItemId]) ON [PRIMARY]
GO
