CREATE TABLE [dbo].[saTransactions]
(
[TransactionId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_saTransactions_TransactionId] DEFAULT (newid()),
[StuEnrollId] [uniqueidentifier] NOT NULL,
[TermId] [uniqueidentifier] NULL,
[CampusId] [uniqueidentifier] NULL,
[TransDate] [datetime] NOT NULL,
[TransCodeId] [uniqueidentifier] NULL,
[TransReference] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AcademicYearId] [uniqueidentifier] NULL,
[TransDescrip] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TransAmount] [decimal] (19, 4) NOT NULL,
[TransTypeId] [int] NOT NULL,
[IsPosted] [bit] NOT NULL,
[CreateDate] [datetime] NULL,
[BatchPaymentId] [uniqueidentifier] NULL,
[ViewOrder] [int] NULL,
[IsAutomatic] [bit] NOT NULL CONSTRAINT [DF_saTransactions_IsAutomatic] DEFAULT ((0)),
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[Voided] [bit] NOT NULL CONSTRAINT [DF_saTransactions_Voided] DEFAULT ((0)),
[FeeLevelId] [tinyint] NULL,
[FeeId] [uniqueidentifier] NULL,
[PaymentCodeId] [uniqueidentifier] NULL,
[FundSourceId] [uniqueidentifier] NULL,
[StuEnrollPayPeriodId] [uniqueidentifier] NULL,
[DisplaySequence] [int] NULL CONSTRAINT [DF_saTransactions_DisplaySequence] DEFAULT ((99999)),
[SecondDisplaySequence] [int] NULL CONSTRAINT [DF_saTransactions_SecondDisplaySequence] DEFAULT ((99999)),
[PmtPeriodId] [uniqueidentifier] NULL,
[PrgChrPeriodSeqId] [uniqueidentifier] NULL,
[PaymentPeriodNumber] [smallint] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE TRIGGER [dbo].[saTransactions_Audit_Delete]
ON [dbo].[saTransactions]
FOR DELETE
AS
SET NOCOUNT ON;

DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
DECLARE @EventRows AS INT;
DECLARE @EventDate AS DATETIME;
DECLARE @UserName AS VARCHAR(50);
SET @AuditHistId = NEWID();
SET @EventRows = (
                 SELECT COUNT(*)
                 FROM   Deleted
                 );

SET @EventDate = (
                 SELECT TOP 1 ModDate
                 FROM   Deleted
                 );
SET @UserName = (
                SELECT TOP 1 ModUser
                FROM   Deleted
                );
IF @EventRows > 0
    BEGIN
        EXEC fmAuditHistAdd @AuditHistId
                           ,'saTransactions'
                           ,'D'
                           ,@EventRows
                           ,@EventDate
                           ,@UserName;
        BEGIN
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.TransactionId
                              ,'TransTypeId'
                              ,CONVERT(VARCHAR(8000), Old.TransTypeId, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.TransactionId
                              ,'TermId'
                              ,CONVERT(VARCHAR(8000), Old.TermId, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.TransactionId
                              ,'TransCodeId'
                              ,CONVERT(VARCHAR(8000), Old.TransCodeId, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.TransactionId
                              ,'AcademicYearId'
                              ,CONVERT(VARCHAR(8000), Old.AcademicYearId, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.TransactionId
                              ,'TransDate'
                              ,CONVERT(VARCHAR(8000), Old.TransDate, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.TransactionId
                              ,'TransReference'
                              ,CONVERT(VARCHAR(8000), Old.TransReference, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.TransactionId
                              ,'TransDescrip'
                              ,CONVERT(VARCHAR(8000), Old.TransDescrip, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.TransactionId
                              ,'IsPosted'
                              ,CONVERT(VARCHAR(8000), Old.IsPosted, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.TransactionId
                              ,'TransAmount'
                              ,CONVERT(VARCHAR(8000), Old.TransAmount, 121)
                        FROM   Deleted Old;


            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.TransactionId
                              ,'PaymentPeriodNumber'
                              ,CONVERT(VARCHAR(8000), Old.PaymentPeriodNumber, 121)
                        FROM   Deleted Old;
        END;
    END;



SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[saTransactions_Audit_Insert]
ON [dbo].[saTransactions]
FOR INSERT
AS
SET NOCOUNT ON;

DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
DECLARE @EventRows AS INT;
DECLARE @EventDate AS DATETIME;
DECLARE @UserName AS VARCHAR(50);
SET @AuditHistId = NEWID();
SET @EventRows = (
                 SELECT COUNT(*)
                 FROM   Inserted
                 );
SET @EventDate = (
                 SELECT TOP 1 ModDate
                 FROM   Inserted
                 );
SET @UserName = (
                SELECT TOP 1 ModUser
                FROM   Inserted
                );
IF @EventRows > 0
    BEGIN
        EXEC fmAuditHistAdd @AuditHistId
                           ,'saTransactions'
                           ,'I'
                           ,@EventRows
                           ,@EventDate
                           ,@UserName;
        BEGIN
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.TransactionId
                              ,'TransTypeId'
                              ,CONVERT(VARCHAR(8000), New.TransTypeId, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.TransactionId
                              ,'TermId'
                              ,CONVERT(VARCHAR(8000), New.TermId, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.TransactionId
                              ,'TransCodeId'
                              ,CONVERT(VARCHAR(8000), New.TransCodeId, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.TransactionId
                              ,'AcademicYearId'
                              ,CONVERT(VARCHAR(8000), New.AcademicYearId, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.TransactionId
                              ,'TransDate'
                              ,CONVERT(VARCHAR(8000), New.TransDate, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.TransactionId
                              ,'TransReference'
                              ,CONVERT(VARCHAR(8000), New.TransReference, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.TransactionId
                              ,'TransDescrip'
                              ,CONVERT(VARCHAR(8000), New.TransDescrip, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.TransactionId
                              ,'IsPosted'
                              ,CONVERT(VARCHAR(8000), New.IsPosted, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.TransactionId
                              ,'TransAmount'
                              ,CONVERT(VARCHAR(8000), New.TransAmount, 121)
                        FROM   Inserted New;

            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.TransactionId
                              ,'PaymentPeriodNumber'
                              ,CONVERT(VARCHAR(8000), New.PaymentPeriodNumber, 121)
                        FROM   Inserted New;
        END;
    END;



SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[saTransactions_Audit_Update]
ON [dbo].[saTransactions]
FOR UPDATE
AS
SET NOCOUNT ON;

DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
DECLARE @EventRows AS INT;
DECLARE @EventDate AS DATETIME;
DECLARE @UserName AS VARCHAR(50);
SET @AuditHistId = NEWID();
SET @EventRows = (
                 SELECT COUNT(*)
                 FROM   Inserted
                 );
SET @EventDate = (
                 SELECT TOP 1 ModDate
                 FROM   Inserted
                 );
SET @UserName = (
                SELECT TOP 1 ModUser
                FROM   Inserted
                );
IF @EventRows > 0
    BEGIN
        EXEC fmAuditHistAdd @AuditHistId
                           ,'saTransactions'
                           ,'U'
                           ,@EventRows
                           ,@EventDate
                           ,@UserName;
        BEGIN
            IF UPDATE(TransTypeId)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.TransactionId
                                           ,'TransTypeId'
                                           ,CONVERT(VARCHAR(8000), Old.TransTypeId, 121)
                                           ,CONVERT(VARCHAR(8000), New.TransTypeId, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TransactionId = New.TransactionId
                            WHERE           Old.TransTypeId <> New.TransTypeId
                                            OR (
                                               Old.TransTypeId IS NULL
                                               AND New.TransTypeId IS NOT NULL
                                               )
                                            OR (
                                               New.TransTypeId IS NULL
                                               AND Old.TransTypeId IS NOT NULL
                                               );
            IF UPDATE(TermId)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.TransactionId
                                           ,'TermId'
                                           ,CONVERT(VARCHAR(8000), Old.TermId, 121)
                                           ,CONVERT(VARCHAR(8000), New.TermId, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TransactionId = New.TransactionId
                            WHERE           Old.TermId <> New.TermId
                                            OR (
                                               Old.TermId IS NULL
                                               AND New.TermId IS NOT NULL
                                               )
                                            OR (
                                               New.TermId IS NULL
                                               AND Old.TermId IS NOT NULL
                                               );
            IF UPDATE(TransCodeId)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.TransactionId
                                           ,'TransCodeId'
                                           ,CONVERT(VARCHAR(8000), Old.TransCodeId, 121)
                                           ,CONVERT(VARCHAR(8000), New.TransCodeId, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TransactionId = New.TransactionId
                            WHERE           Old.TransCodeId <> New.TransCodeId
                                            OR (
                                               Old.TransCodeId IS NULL
                                               AND New.TransCodeId IS NOT NULL
                                               )
                                            OR (
                                               New.TransCodeId IS NULL
                                               AND Old.TransCodeId IS NOT NULL
                                               );
            IF UPDATE(AcademicYearId)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.TransactionId
                                           ,'AcademicYearId'
                                           ,CONVERT(VARCHAR(8000), Old.AcademicYearId, 121)
                                           ,CONVERT(VARCHAR(8000), New.AcademicYearId, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TransactionId = New.TransactionId
                            WHERE           Old.AcademicYearId <> New.AcademicYearId
                                            OR (
                                               Old.AcademicYearId IS NULL
                                               AND New.AcademicYearId IS NOT NULL
                                               )
                                            OR (
                                               New.AcademicYearId IS NULL
                                               AND Old.AcademicYearId IS NOT NULL
                                               );
            IF UPDATE(TransDate)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.TransactionId
                                           ,'TransDate'
                                           ,CONVERT(VARCHAR(8000), Old.TransDate, 121)
                                           ,CONVERT(VARCHAR(8000), New.TransDate, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TransactionId = New.TransactionId
                            WHERE           Old.TransDate <> New.TransDate
                                            OR (
                                               Old.TransDate IS NULL
                                               AND New.TransDate IS NOT NULL
                                               )
                                            OR (
                                               New.TransDate IS NULL
                                               AND Old.TransDate IS NOT NULL
                                               );
            IF UPDATE(TransReference)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.TransactionId
                                           ,'TransReference'
                                           ,CONVERT(VARCHAR(8000), Old.TransReference, 121)
                                           ,CONVERT(VARCHAR(8000), New.TransReference, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TransactionId = New.TransactionId
                            WHERE           Old.TransReference <> New.TransReference
                                            OR (
                                               Old.TransReference IS NULL
                                               AND New.TransReference IS NOT NULL
                                               )
                                            OR (
                                               New.TransReference IS NULL
                                               AND Old.TransReference IS NOT NULL
                                               );
            IF UPDATE(TransDescrip)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.TransactionId
                                           ,'TransDescrip'
                                           ,CONVERT(VARCHAR(8000), Old.TransDescrip, 121)
                                           ,CONVERT(VARCHAR(8000), New.TransDescrip, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TransactionId = New.TransactionId
                            WHERE           Old.TransDescrip <> New.TransDescrip
                                            OR (
                                               Old.TransDescrip IS NULL
                                               AND New.TransDescrip IS NOT NULL
                                               )
                                            OR (
                                               New.TransDescrip IS NULL
                                               AND Old.TransDescrip IS NOT NULL
                                               );
            IF UPDATE(IsPosted)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.TransactionId
                                           ,'IsPosted'
                                           ,CONVERT(VARCHAR(8000), Old.IsPosted, 121)
                                           ,CONVERT(VARCHAR(8000), New.IsPosted, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TransactionId = New.TransactionId
                            WHERE           Old.IsPosted <> New.IsPosted
                                            OR (
                                               Old.IsPosted IS NULL
                                               AND New.IsPosted IS NOT NULL
                                               )
                                            OR (
                                               New.IsPosted IS NULL
                                               AND Old.IsPosted IS NOT NULL
                                               );
            IF UPDATE(TransAmount)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.TransactionId
                                           ,'TransAmount'
                                           ,CONVERT(VARCHAR(8000), Old.TransAmount, 121)
                                           ,CONVERT(VARCHAR(8000), New.TransAmount, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TransactionId = New.TransactionId
                            WHERE           Old.TransAmount <> New.TransAmount
                                            OR (
                                               Old.TransAmount IS NULL
                                               AND New.TransAmount IS NOT NULL
                                               )
                                            OR (
                                               New.TransAmount IS NULL
                                               AND Old.TransAmount IS NOT NULL
                                               );
            IF UPDATE(PaymentPeriodNumber)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.TransactionId
                                           ,'PaymentPeriodNumber'
                                           ,CONVERT(VARCHAR(8000), Old.PaymentPeriodNumber, 121)
                                           ,CONVERT(VARCHAR(8000), New.PaymentPeriodNumber, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TransactionId = New.TransactionId
                            WHERE           Old.PaymentPeriodNumber <> New.PaymentPeriodNumber
                                            OR (
                                               Old.PaymentPeriodNumber IS NULL
                                               AND New.PaymentPeriodNumber IS NOT NULL
                                               )
                                            OR (
                                               New.PaymentPeriodNumber IS NULL
                                               AND Old.PaymentPeriodNumber IS NOT NULL
                                               );
        END;
    END;



SET NOCOUNT OFF;
GO
ALTER TABLE [dbo].[saTransactions] ADD CONSTRAINT [PK_saTransactions_TransactionId] PRIMARY KEY CLUSTERED  ([TransactionId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_saTransactions_AcademicYearId] ON [dbo].[saTransactions] ([AcademicYearId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_saTransactions_BatchPaymentId] ON [dbo].[saTransactions] ([BatchPaymentId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_saTransactions_StuEnrollId] ON [dbo].[saTransactions] ([StuEnrollId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_saTransactions_TermId] ON [dbo].[saTransactions] ([TermId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_saTransactions_TransCodeId] ON [dbo].[saTransactions] ([TransCodeId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_saTransactions_TransDate] ON [dbo].[saTransactions] ([TransDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_saTransactions_TransTypeId_StuEnrollId_IsPosted_Voided_TransactionId_TransAmount_TransDate] ON [dbo].[saTransactions] ([TransTypeId], [StuEnrollId], [IsPosted], [Voided], [TransactionId], [TransAmount], [TransDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_saTransactions_TransTypeId_StuEnrollId_TransDate_Voided_TransactionId_TransCodeId_TransAmount] ON [dbo].[saTransactions] ([TransTypeId], [StuEnrollId], [TransDate], [Voided], [TransactionId], [TransCodeId]) INCLUDE ([TransAmount]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_saTransactions_TransTypeId_StuEnrollId_Voided_TransDate_TransactionId_TermId_TransCodeId_TransAmount] ON [dbo].[saTransactions] ([TransTypeId], [StuEnrollId], [Voided], [TransDate], [TransactionId], [TermId], [TransCodeId]) INCLUDE ([TransAmount]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saTransactions] ADD CONSTRAINT [FK_saTransactions_arPrgChargePeriodSeq_StuEnrollPayPeriodId_PrgChrPeriodSeqId] FOREIGN KEY ([StuEnrollPayPeriodId]) REFERENCES [dbo].[arPrgChargePeriodSeq] ([PrgChrPeriodSeqId])
GO
ALTER TABLE [dbo].[saTransactions] ADD CONSTRAINT [FK_saTransactions_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY ([StuEnrollId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
ALTER TABLE [dbo].[saTransactions] ADD CONSTRAINT [FK_saTransactions_arTerm_TermId_TermId] FOREIGN KEY ([TermId]) REFERENCES [dbo].[arTerm] ([TermId])
GO
ALTER TABLE [dbo].[saTransactions] ADD CONSTRAINT [FK_saTransactions_saAcademicYears_AcademicYearId_AcademicYearId] FOREIGN KEY ([AcademicYearId]) REFERENCES [dbo].[saAcademicYears] ([AcademicYearId])
GO
ALTER TABLE [dbo].[saTransactions] ADD CONSTRAINT [FK_saTransactions_saBatchPayments_BatchPaymentId_BatchPaymentId] FOREIGN KEY ([BatchPaymentId]) REFERENCES [dbo].[saBatchPayments] ([BatchPaymentId])
GO
ALTER TABLE [dbo].[saTransactions] ADD CONSTRAINT [FK_saTransactions_saFundSources_FundSourceId_FundSourceId] FOREIGN KEY ([FundSourceId]) REFERENCES [dbo].[saFundSources] ([FundSourceId])
GO
ALTER TABLE [dbo].[saTransactions] ADD CONSTRAINT [FK_saTransactions_saPmtPeriods_PmtPeriodId_PmtPeriodId] FOREIGN KEY ([PmtPeriodId]) REFERENCES [dbo].[saPmtPeriods] ([PmtPeriodId])
GO
ALTER TABLE [dbo].[saTransactions] ADD CONSTRAINT [FK_saTransactions_saTransCodes_PaymentCodeId_TransCodeId] FOREIGN KEY ([PaymentCodeId]) REFERENCES [dbo].[saTransCodes] ([TransCodeId])
GO
ALTER TABLE [dbo].[saTransactions] ADD CONSTRAINT [FK_saTransactions_saTransCodes_TransCodeId_TransCodeId] FOREIGN KEY ([TransCodeId]) REFERENCES [dbo].[saTransCodes] ([TransCodeId])
GO
ALTER TABLE [dbo].[saTransactions] ADD CONSTRAINT [FK_saTransactions_saTransTypes_TransTypeId_TransTypeId] FOREIGN KEY ([TransTypeId]) REFERENCES [dbo].[saTransTypes] ([TransTypeId])
GO
