CREATE TABLE [dbo].[syDocumentHistory]
(
[FileId] [uniqueidentifier] NOT NULL,
[FileName] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FileExtension] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[DocumentType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StudentId] [uniqueidentifier] NULL,
[FileNameOnly] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DocumentId] [uniqueidentifier] NULL,
[ModuleId] [int] NULL,
[LeadId] [uniqueidentifier] NULL,
[DisplayName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsArchived] [bit] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


--==========================================================================================  
-- TRIGGER syDocumentHistory_Audit_Delete  
-- INSERT  add Audit History when delete records in syDocumentHistory  
--==========================================================================================  
CREATE TRIGGER [dbo].[syDocumentHistory_Audit_Delete]
ON [dbo].[syDocumentHistory]
FOR DELETE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);

        SET @AuditHistId = NEWID();
        SET @EventRows = (
                         SELECT COUNT(*)
                         FROM   Deleted
                         );
        SET @EventDate = (
                         SELECT TOP 1 ModDate
                         FROM   Deleted
                         );
        SET @UserName = (
                        SELECT TOP 1 ModUser
                        FROM   Deleted
                        );

        IF @EventRows > 0
            BEGIN
                EXEC fmAuditHistAdd @AuditHistId
                                   ,'syDocumentHistory'
                                   ,'D'
                                   ,@EventRows
                                   ,@EventDate
                                   ,@UserName;
                -- FileName  
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.FileId
                                  ,'FileName'
                                  ,CONVERT(VARCHAR(MAX), Old.FileName)
                            FROM   Deleted AS Old;
                -- FileExtension  
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.FileId
                                  ,'FileExtension'
                                  ,CONVERT(VARCHAR(MAX), Old.FileExtension)
                            FROM   Deleted AS Old;
                -- ModUser  
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.FileId
                                  ,'ModUser'
                                  ,CONVERT(VARCHAR(MAX), Old.ModUser)
                            FROM   Deleted AS Old;
                -- ModDate  
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.FileId
                                  ,'ModDate'
                                  ,CONVERT(VARCHAR(MAX), Old.ModDate, 121)
                            FROM   Deleted AS Old;
                -- DocumentType  
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.FileId
                                  ,'DocumentType'
                                  ,CONVERT(VARCHAR(MAX), Old.DocumentType)
                            FROM   Deleted AS Old;
                -- StudentId  
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.FileId
                                  ,'StudentId'
                                  ,CONVERT(VARCHAR(MAX), Old.StudentId)
                            FROM   Deleted AS Old;
                -- FileNameOnly  
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.FileId
                                  ,'FileNameOnly'
                                  ,CONVERT(VARCHAR(MAX), Old.FileNameOnly)
                            FROM   Deleted AS Old;
                -- DocumentId  
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.FileId
                                  ,'DocumentId'
                                  ,CONVERT(VARCHAR(MAX), Old.DocumentId)
                            FROM   Deleted AS Old;
                -- ModuleId  
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.FileId
                                  ,'ModuleId'
                                  ,CONVERT(VARCHAR(MAX), Old.ModuleId)
                            FROM   Deleted AS Old;
                -- LeadId  
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.FileId
                                  ,'LeadId'
                                  ,CONVERT(VARCHAR(MAX), Old.LeadId)
                            FROM   Deleted AS Old;
                -- DisplayName  
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.FileId
                                  ,'DisplayName'
                                  ,CONVERT(VARCHAR(MAX), Old.DisplayName)
                            FROM   Deleted AS Old;
				-- IsArchived  
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                              )
                            SELECT @AuditHistId
                                  ,Old.FileId
                                  ,'IsArchived'
                                  ,CONVERT(VARCHAR(MAX), Old.IsArchived)
                            FROM   Deleted AS Old;
                
            END;
        SET NOCOUNT OFF;
    END;
--==========================================================================================  
-- END TRIGGER syDocumentHistory_Audit_Delete  
--==========================================================================================  

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


--==========================================================================================  
-- TRIGGER syDocumentHistory_Audit_Insert  
-- INSERT  add Audit History when insert records in syDocumentHistory  
--==========================================================================================  
CREATE TRIGGER [dbo].[syDocumentHistory_Audit_Insert]
ON [dbo].[syDocumentHistory]
FOR INSERT
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);

        SET @AuditHistId = NEWID();
        SET @EventRows = (
                         SELECT COUNT(*)
                         FROM   Inserted
                         );
        SET @EventDate = (
                         SELECT TOP 1 ModDate
                         FROM   Inserted
                         );
        SET @UserName = (
                        SELECT TOP 1 ModUser
                        FROM   Inserted
                        );

        IF @EventRows > 0
            BEGIN
                EXEC fmAuditHistAdd @AuditHistId
                                   ,'syDocumentHistory'
                                   ,'I'
                                   ,@EventRows
                                   ,@EventDate
                                   ,@UserName;
                -- FileName  
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.FileId
                                  ,'FileName'
                                  ,CONVERT(VARCHAR(MAX), New.FileName)
                            FROM   Inserted AS New;
                -- FileExtension  
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.FileId
                                  ,'FileExtension'
                                  ,CONVERT(VARCHAR(MAX), New.FileExtension)
                            FROM   Inserted AS New;
                -- ModUser  
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.FileId
                                  ,'ModUser'
                                  ,CONVERT(VARCHAR(MAX), New.ModUser)
                            FROM   Inserted AS New;
                -- ModDate  
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.FileId
                                  ,'ModDate'
                                  ,CONVERT(VARCHAR(MAX), New.ModDate, 121)
                            FROM   Inserted AS New;
                -- DocumentType  
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.FileId
                                  ,'DocumentType'
                                  ,CONVERT(VARCHAR(MAX), New.DocumentType)
                            FROM   Inserted AS New;
                -- StudentId  
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.FileId
                                  ,'StudentId'
                                  ,CONVERT(VARCHAR(MAX), New.StudentId)
                            FROM   Inserted AS New;
                -- FileNameOnly  
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.FileId
                                  ,'FileNameOnly'
                                  ,CONVERT(VARCHAR(MAX), New.FileNameOnly)
                            FROM   Inserted AS New;
                -- DocumentId  
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.FileId
                                  ,'DocumentId'
                                  ,CONVERT(VARCHAR(MAX), New.DocumentId)
                            FROM   Inserted AS New;
                -- ModuleId  
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.FileId
                                  ,'ModuleId'
                                  ,CONVERT(VARCHAR(MAX), New.ModuleId)
                            FROM   Inserted AS New;
                -- LeadId  
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.FileId
                                  ,'LeadId'
                                  ,CONVERT(VARCHAR(MAX), New.LeadId)
                            FROM   Inserted AS New;
                -- DisplayName  
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.FileId
                                  ,'DisplayName'
                                  ,CONVERT(VARCHAR(MAX), New.DisplayName)
                            FROM   Inserted AS New;
				-- IsArchived  
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,NewValue
                                              )
                            SELECT @AuditHistId
                                  ,New.FileId
                                  ,'IsArchived'
                                  ,CONVERT(VARCHAR(MAX), New.IsArchived)
                            FROM   Inserted AS New;
                
            END;
        SET NOCOUNT OFF;
    END;
--==========================================================================================  
-- END TRIGGER syDocumentHistory_Audit_Insert  
--==========================================================================================  

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--==========================================================================================  
-- TRIGGER syDocumentHistory_Audit_Update  
-- UPDATE  add Audit History when update fields in syDocumentHistory  
--==========================================================================================  
CREATE TRIGGER [dbo].[syDocumentHistory_Audit_Update]
ON [dbo].[syDocumentHistory]
FOR UPDATE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);

        SET @AuditHistId = NEWID();
        SET @EventRows = (
                         SELECT COUNT(*)
                         FROM   Inserted
                         );
        SET @EventDate = (
                         SELECT TOP 1 ModDate
                         FROM   Inserted
                         );
        SET @UserName = (
                        SELECT TOP 1 ModUser
                        FROM   Inserted
                        );

        IF @EventRows > 0
            BEGIN
                EXEC fmAuditHistAdd @AuditHistId
                                   ,'syDocumentHistory'
                                   ,'U'
                                   ,@EventRows
                                   ,@EventDate
                                   ,@UserName;
                -- FileName  
                IF UPDATE(FileName)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT          @AuditHistId
                                                   ,New.FileId
                                                   ,'FileName'
                                                   ,CONVERT(VARCHAR(MAX), Old.FileName)
                                                   ,CONVERT(VARCHAR(MAX), New.FileName)
                                    FROM            Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.FileId = New.FileId
                                    WHERE           Old.FileName <> New.FileName
                                                    OR (
                                                       Old.FileName IS NULL
                                                       AND New.FileName IS NOT NULL
                                                       )
                                                    OR (
                                                       New.FileName IS NULL
                                                       AND Old.FileName IS NOT NULL
                                                       );
                    END;
                -- FileExtension  
                IF UPDATE(FileExtension)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT          @AuditHistId
                                                   ,New.FileId
                                                   ,'FileExtension'
                                                   ,CONVERT(VARCHAR(MAX), Old.FileExtension)
                                                   ,CONVERT(VARCHAR(MAX), New.FileExtension)
                                    FROM            Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.FileId = New.FileId
                                    WHERE           Old.FileExtension <> New.FileExtension
                                                    OR (
                                                       Old.FileExtension IS NULL
                                                       AND New.FileExtension IS NOT NULL
                                                       )
                                                    OR (
                                                       New.FileExtension IS NULL
                                                       AND Old.FileExtension IS NOT NULL
                                                       );
                    END;
                -- ModUser  
                IF UPDATE(ModUser)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT          @AuditHistId
                                                   ,New.FileId
                                                   ,'ModUser'
                                                   ,CONVERT(VARCHAR(MAX), Old.ModUser)
                                                   ,CONVERT(VARCHAR(MAX), New.ModUser)
                                    FROM            Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.FileId = New.FileId
                                    WHERE           Old.ModUser <> New.ModUser
                                                    OR (
                                                       Old.ModUser IS NULL
                                                       AND New.ModUser IS NOT NULL
                                                       )
                                                    OR (
                                                       New.ModUser IS NULL
                                                       AND Old.ModUser IS NOT NULL
                                                       );
                    END;
                -- ModDate  
                IF UPDATE(ModDate)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT          @AuditHistId
                                                   ,New.FileId
                                                   ,'ModDate'
                                                   ,CONVERT(VARCHAR(MAX), Old.ModDate, 121)
                                                   ,CONVERT(VARCHAR(MAX), New.ModDate, 121)
                                    FROM            Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.FileId = New.FileId
                                    WHERE           Old.ModDate <> New.ModDate
                                                    OR (
                                                       Old.ModDate IS NULL
                                                       AND New.ModDate IS NOT NULL
                                                       )
                                                    OR (
                                                       New.ModDate IS NULL
                                                       AND Old.ModDate IS NOT NULL
                                                       );
                    END;
                -- DocumentType  
                IF UPDATE(DocumentType)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT          @AuditHistId
                                                   ,New.FileId
                                                   ,'DocumentType'
                                                   ,CONVERT(VARCHAR(MAX), Old.DocumentType)
                                                   ,CONVERT(VARCHAR(MAX), New.DocumentType)
                                    FROM            Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.FileId = New.FileId
                                    WHERE           Old.DocumentType <> New.DocumentType
                                                    OR (
                                                       Old.DocumentType IS NULL
                                                       AND New.DocumentType IS NOT NULL
                                                       )
                                                    OR (
                                                       New.DocumentType IS NULL
                                                       AND Old.DocumentType IS NOT NULL
                                                       );
                    END;
                -- StudentId  
                IF UPDATE(StudentId)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT          @AuditHistId
                                                   ,New.FileId
                                                   ,'StudentId'
                                                   ,CONVERT(VARCHAR(MAX), Old.StudentId)
                                                   ,CONVERT(VARCHAR(MAX), New.StudentId)
                                    FROM            Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.FileId = New.FileId
                                    WHERE           Old.StudentId <> New.StudentId
                                                    OR (
                                                       Old.StudentId IS NULL
                                                       AND New.StudentId IS NOT NULL
                                                       )
                                                    OR (
                                                       New.StudentId IS NULL
                                                       AND Old.StudentId IS NOT NULL
                                                       );
                    END;
                -- FileNameOnly  
                IF UPDATE(FileNameOnly)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT          @AuditHistId
                                                   ,New.FileId
                                                   ,'FileNameOnly'
                                                   ,CONVERT(VARCHAR(MAX), Old.FileNameOnly)
                                                   ,CONVERT(VARCHAR(MAX), New.FileNameOnly)
                                    FROM            Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.FileId = New.FileId
                                    WHERE           Old.FileNameOnly <> New.FileNameOnly
                                                    OR (
                                                       Old.FileNameOnly IS NULL
                                                       AND New.FileNameOnly IS NOT NULL
                                                       )
                                                    OR (
                                                       New.FileNameOnly IS NULL
                                                       AND Old.FileNameOnly IS NOT NULL
                                                       );
                    END;
                -- DocumentId  
                IF UPDATE(DocumentId)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT          @AuditHistId
                                                   ,New.FileId
                                                   ,'DocumentId'
                                                   ,CONVERT(VARCHAR(MAX), Old.DocumentId)
                                                   ,CONVERT(VARCHAR(MAX), New.DocumentId)
                                    FROM            Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.FileId = New.FileId
                                    WHERE           Old.DocumentId <> New.DocumentId
                                                    OR (
                                                       Old.DocumentId IS NULL
                                                       AND New.DocumentId IS NOT NULL
                                                       )
                                                    OR (
                                                       New.DocumentId IS NULL
                                                       AND Old.DocumentId IS NOT NULL
                                                       );
                    END;
                -- ModuleId  
                IF UPDATE(ModuleId)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT          @AuditHistId
                                                   ,New.FileId
                                                   ,'ModuleId'
                                                   ,CONVERT(VARCHAR(MAX), Old.ModuleId)
                                                   ,CONVERT(VARCHAR(MAX), New.ModuleId)
                                    FROM            Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.FileId = New.FileId
                                    WHERE           Old.ModuleId <> New.ModuleId
                                                    OR (
                                                       Old.ModuleId IS NULL
                                                       AND New.ModuleId IS NOT NULL
                                                       )
                                                    OR (
                                                       New.ModuleId IS NULL
                                                       AND Old.ModuleId IS NOT NULL
                                                       );
                    END;
                -- LeadId  
                IF UPDATE(LeadId)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT          @AuditHistId
                                                   ,New.FileId
                                                   ,'LeadId'
                                                   ,CONVERT(VARCHAR(MAX), Old.LeadId)
                                                   ,CONVERT(VARCHAR(MAX), New.LeadId)
                                    FROM            Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.FileId = New.FileId
                                    WHERE           Old.LeadId <> New.LeadId
                                                    OR (
                                                       Old.LeadId IS NULL
                                                       AND New.LeadId IS NOT NULL
                                                       )
                                                    OR (
                                                       New.LeadId IS NULL
                                                       AND Old.LeadId IS NOT NULL
                                                       );
                    END;
                -- DisplayName  
                IF UPDATE(DisplayName)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT          @AuditHistId
                                                   ,New.FileId
                                                   ,'DisplayName'
                                                   ,CONVERT(VARCHAR(MAX), Old.DisplayName)
                                                   ,CONVERT(VARCHAR(MAX), New.DisplayName)
                                    FROM            Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.FileId = New.FileId
                                    WHERE           Old.DisplayName <> New.DisplayName
                                                    OR (
                                                       Old.DisplayName IS NULL
                                                       AND New.DisplayName IS NOT NULL
                                                       )
                                                    OR (
                                                       New.DisplayName IS NULL
                                                       AND Old.DisplayName IS NOT NULL
                                                       );
                    END;

				-- IsArchived  
                IF UPDATE(IsArchived)
                    BEGIN
                        INSERT INTO syAuditHistDetail (
                                                      AuditHistId
                                                     ,RowId
                                                     ,ColumnName
                                                     ,OldValue
                                                     ,NewValue
                                                      )
                                    SELECT          @AuditHistId
                                                   ,New.FileId
                                                   ,'IsArchived'
                                                   ,CONVERT(VARCHAR(MAX), Old.IsArchived)
                                                   ,CONVERT(VARCHAR(MAX), New.IsArchived)
                                    FROM            Inserted AS New
                                    FULL OUTER JOIN Deleted AS Old ON Old.FileId = New.FileId
                                    WHERE           Old.IsArchived <> New.IsArchived
                                                    OR (
                                                       Old.IsArchived IS NULL
                                                       AND New.IsArchived IS NOT NULL
                                                       )
                                                    OR (
                                                       New.IsArchived IS NULL
                                                       AND Old.IsArchived IS NOT NULL
                                                       );
                    END;
               
            END;
        SET NOCOUNT OFF;
    END;
--==========================================================================================  
-- END TRIGGER syDocumentHistory_Audit_Update  
--==========================================================================================  

GO
ALTER TABLE [dbo].[syDocumentHistory] ADD CONSTRAINT [PK_syDocumentHistory_FileId] PRIMARY KEY CLUSTERED  ([FileId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syDocumentHistory] ADD CONSTRAINT [FK_syDocumentHistory_adLeads_LeadId_LeadId] FOREIGN KEY ([LeadId]) REFERENCES [dbo].[adLeads] ([LeadId])
GO
ALTER TABLE [dbo].[syDocumentHistory] ADD CONSTRAINT [FK_syDocumentHistory_adReqs_DocumentId_adReqId] FOREIGN KEY ([DocumentId]) REFERENCES [dbo].[adReqs] ([adReqId])
GO
