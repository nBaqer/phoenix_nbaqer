CREATE TABLE [dbo].[adLeadEmployment]
(
[StEmploymentId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_adLeadEmployment_StEmploymentId] DEFAULT (newid()),
[LeadId] [uniqueidentifier] NOT NULL,
[JobTitleId] [uniqueidentifier] NULL,
[JobStatusId] [uniqueidentifier] NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[Comments] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL,
[JobResponsibilities] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmployerName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EmployerJobTitle] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[adLeadEmployment_Audit_Delete] ON [dbo].[adLeadEmployment]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adLeadEmployment','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StEmploymentId
                               ,'JobStatusId'
                               ,CONVERT(VARCHAR(8000),Old.JobStatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StEmploymentId
                               ,'LeadID'
                               ,CONVERT(VARCHAR(8000),Old.LeadId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StEmploymentId
                               ,'EmployerName'
                               ,CONVERT(VARCHAR(8000),Old.EmployerName,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StEmploymentId
                               ,'EmployerJobTitle'
                               ,CONVERT(VARCHAR(8000),Old.EmployerJobTitle,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StEmploymentId
                               ,'JobTitleId'
                               ,CONVERT(VARCHAR(8000),Old.JobTitleId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StEmploymentId
                               ,'EndDate'
                               ,CONVERT(VARCHAR(8000),Old.EndDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StEmploymentId
                               ,'StartDate'
                               ,CONVERT(VARCHAR(8000),Old.StartDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StEmploymentId
                               ,'JobResponsibilities'
                               ,CONVERT(VARCHAR(8000),Old.JobResponsibilities,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StEmploymentId
                               ,'Comments'
                               ,CONVERT(VARCHAR(8000),Old.Comments,121)
                        FROM    Deleted Old; 
            END; 
        END;



    SET NOCOUNT OFF; 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[adLeadEmployment_Audit_Insert] ON [dbo].[adLeadEmployment]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adLeadEmployment','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StEmploymentId
                               ,'JobStatusId'
                               ,CONVERT(VARCHAR(8000),New.JobStatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StEmploymentId
                               ,'LeadID'
                               ,CONVERT(VARCHAR(8000),New.LeadId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StEmploymentId
                               ,'EmployerName'
                               ,CONVERT(VARCHAR(8000),New.EmployerName,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StEmploymentId
                               ,'EmployerJobTitle'
                               ,CONVERT(VARCHAR(8000),New.EmployerJobTitle,121)
                        FROM    Inserted New;
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StEmploymentId
                               ,'JobTitleId'
                               ,CONVERT(VARCHAR(8000),New.JobTitleId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StEmploymentId
                               ,'EndDate'
                               ,CONVERT(VARCHAR(8000),New.EndDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StEmploymentId
                               ,'StartDate'
                               ,CONVERT(VARCHAR(8000),New.StartDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StEmploymentId
                               ,'JobResponsibilities'
                               ,CONVERT(VARCHAR(8000),New.JobResponsibilities,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StEmploymentId
                               ,'Comments'
                               ,CONVERT(VARCHAR(8000),New.Comments,121)
                        FROM    Inserted New; 
            END; 
        END;



    SET NOCOUNT OFF; 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[adLeadEmployment_Audit_Update] ON [dbo].[adLeadEmployment]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adLeadEmployment','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(JobStatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StEmploymentId
                                   ,'JobStatusId'
                                   ,CONVERT(VARCHAR(8000),Old.JobStatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.JobStatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StEmploymentId = New.StEmploymentId
                            WHERE   Old.JobStatusId <> New.JobStatusId
                                    OR (
                                         Old.JobStatusId IS NULL
                                         AND New.JobStatusId IS NOT NULL
                                       )
                                    OR (
                                         New.JobStatusId IS NULL
                                         AND Old.JobStatusId IS NOT NULL
                                       ); 
                IF UPDATE(LeadId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StEmploymentId
                                   ,'LeadID'
                                   ,CONVERT(VARCHAR(8000),Old.LeadId,121)
                                   ,CONVERT(VARCHAR(8000),New.LeadId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StEmploymentId = New.StEmploymentId
                            WHERE   Old.LeadId <> New.LeadId
                                    OR (
                                         Old.LeadId IS NULL
                                         AND New.LeadId IS NOT NULL
                                       )
                                    OR (
                                         New.LeadId IS NULL
                                         AND Old.LeadId IS NOT NULL
                                       ); 
                IF UPDATE(EmployerName)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StEmploymentId
                                   ,'EmployerName'
                                   ,CONVERT(VARCHAR(8000),Old.EmployerName,121)
                                   ,CONVERT(VARCHAR(8000),New.EmployerName,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StEmploymentId = New.StEmploymentId
                            WHERE   Old.EmployerName <> New.EmployerName
                                    OR (
                                         Old.EmployerName IS NULL
                                         AND New.EmployerName IS NOT NULL
                                       )
                                    OR (
                                         New.EmployerName IS NULL
                                         AND Old.EmployerName IS NOT NULL
                                       ); 
                IF UPDATE(EmployerJobTitle)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StEmploymentId
                                   ,'EmployerJobTitle'
                                   ,CONVERT(VARCHAR(8000),Old.EmployerJobTitle,121)
                                   ,CONVERT(VARCHAR(8000),New.EmployerJobTitle,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StEmploymentId = New.StEmploymentId
                            WHERE   Old.EmployerJobTitle <> New.EmployerJobTitle
                                    OR (
                                         Old.EmployerJobTitle IS NULL
                                         AND New.EmployerJobTitle IS NOT NULL
                                       )
                                    OR (
                                         New.EmployerJobTitle IS NULL
                                         AND Old.EmployerJobTitle IS NOT NULL
                                       ); 
                IF UPDATE(JobTitleId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StEmploymentId
                                   ,'JobTitleId'
                                   ,CONVERT(VARCHAR(8000),Old.JobTitleId,121)
                                   ,CONVERT(VARCHAR(8000),New.JobTitleId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StEmploymentId = New.StEmploymentId
                            WHERE   Old.JobTitleId <> New.JobTitleId
                                    OR (
                                         Old.JobTitleId IS NULL
                                         AND New.JobTitleId IS NOT NULL
                                       )
                                    OR (
                                         New.JobTitleId IS NULL
                                         AND Old.JobTitleId IS NOT NULL
                                       ); 
                IF UPDATE(EndDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StEmploymentId
                                   ,'EndDate'
                                   ,CONVERT(VARCHAR(8000),Old.EndDate,121)
                                   ,CONVERT(VARCHAR(8000),New.EndDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StEmploymentId = New.StEmploymentId
                            WHERE   Old.EndDate <> New.EndDate
                                    OR (
                                         Old.EndDate IS NULL
                                         AND New.EndDate IS NOT NULL
                                       )
                                    OR (
                                         New.EndDate IS NULL
                                         AND Old.EndDate IS NOT NULL
                                       ); 
                IF UPDATE(StartDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StEmploymentId
                                   ,'StartDate'
                                   ,CONVERT(VARCHAR(8000),Old.StartDate,121)
                                   ,CONVERT(VARCHAR(8000),New.StartDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StEmploymentId = New.StEmploymentId
                            WHERE   Old.StartDate <> New.StartDate
                                    OR (
                                         Old.StartDate IS NULL
                                         AND New.StartDate IS NOT NULL
                                       )
                                    OR (
                                         New.StartDate IS NULL
                                         AND Old.StartDate IS NOT NULL
                                       ); 
                IF UPDATE(JobResponsibilities)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StEmploymentId
                                   ,'JobResponsibilities'
                                   ,CONVERT(VARCHAR(8000),Old.JobResponsibilities,121)
                                   ,CONVERT(VARCHAR(8000),New.JobResponsibilities,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StEmploymentId = New.StEmploymentId
                            WHERE   Old.JobResponsibilities <> New.JobResponsibilities
                                    OR (
                                         Old.JobResponsibilities IS NULL
                                         AND New.JobResponsibilities IS NOT NULL
                                       )
                                    OR (
                                         New.JobResponsibilities IS NULL
                                         AND Old.JobResponsibilities IS NOT NULL
                                       ); 
                IF UPDATE(comments)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StEmploymentId
                                   ,'Comments'
                                   ,CONVERT(VARCHAR(8000),Old.Comments,121)
                                   ,CONVERT(VARCHAR(8000),New.Comments,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StEmploymentId = New.StEmploymentId
                            WHERE   Old.Comments <> New.Comments
                                    OR (
                                         Old.Comments IS NULL
                                         AND New.Comments IS NOT NULL
                                       )
                                    OR (
                                         New.Comments IS NULL
                                         AND Old.Comments IS NOT NULL
                                       ); 
            END; 
        END;



    SET NOCOUNT OFF; 
GO
ALTER TABLE [dbo].[adLeadEmployment] ADD CONSTRAINT [PK_adLeadEmployment_StEmploymentId] PRIMARY KEY CLUSTERED  ([StEmploymentId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_adLeadEmployment_LeadId_JobTitleId_EmployerName_EmployerJobTitle] ON [dbo].[adLeadEmployment] ([LeadId], [JobTitleId], [EmployerName], [EmployerJobTitle]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adLeadEmployment] ADD CONSTRAINT [FK_adLeadEmployment_adLeads_LeadId_LeadId] FOREIGN KEY ([LeadId]) REFERENCES [dbo].[adLeads] ([LeadId])
GO
ALTER TABLE [dbo].[adLeadEmployment] ADD CONSTRAINT [FK_adLeadEmployment_adTitles_JobTitleId_TitleId] FOREIGN KEY ([JobTitleId]) REFERENCES [dbo].[adTitles] ([TitleId])
GO
ALTER TABLE [dbo].[adLeadEmployment] ADD CONSTRAINT [FK_adLeadEmployment_plJobStatus_JobStatusId_JobStatusId] FOREIGN KEY ([JobStatusId]) REFERENCES [dbo].[plJobStatus] ([JobStatusId])
GO
