CREATE TABLE [dbo].[adLead_Notes]
(
[id] [int] NOT NULL IDENTITY(1, 1),
[LeadId] [uniqueidentifier] NOT NULL,
[NotesId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adLead_Notes] ADD CONSTRAINT [PK_adLead_Notes_id] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adLead_Notes] ADD CONSTRAINT [FK_adLead_Notes_adLeads_LeadId_LeadId] FOREIGN KEY ([LeadId]) REFERENCES [dbo].[adLeads] ([LeadId])
GO
ALTER TABLE [dbo].[adLead_Notes] ADD CONSTRAINT [FK_adLead_Notes_AllNotes_NotesId_NotesId] FOREIGN KEY ([NotesId]) REFERENCES [dbo].[AllNotes] ([NotesId])
GO
