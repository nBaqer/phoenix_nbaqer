CREATE TABLE [dbo].[arDepartments]
(
[DeptId] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[DeptCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DeptDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CollegeDivId] [uniqueidentifier] NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CampGrpId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[arDepartments_Audit_Delete] ON [dbo].[arDepartments]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arDepartments','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.DeptId
                               ,'CollegeDivId'
                               ,CONVERT(VARCHAR(8000),Old.CollegeDivId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.DeptId
                               ,'DeptCode'
                               ,CONVERT(VARCHAR(8000),Old.DeptCode,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.DeptId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.DeptId
                               ,'DeptDescrip'
                               ,CONVERT(VARCHAR(8000),Old.DeptDescrip,121)
                        FROM    Deleted Old; 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[arDepartments_Audit_Insert] ON [dbo].[arDepartments]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arDepartments','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.DeptId
                               ,'CollegeDivId'
                               ,CONVERT(VARCHAR(8000),New.CollegeDivId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.DeptId
                               ,'DeptCode'
                               ,CONVERT(VARCHAR(8000),New.DeptCode,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.DeptId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.DeptId
                               ,'DeptDescrip'
                               ,CONVERT(VARCHAR(8000),New.DeptDescrip,121)
                        FROM    Inserted New; 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[arDepartments_Audit_Update] ON [dbo].[arDepartments]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arDepartments','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(CollegeDivId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.DeptId
                                   ,'CollegeDivId'
                                   ,CONVERT(VARCHAR(8000),Old.CollegeDivId,121)
                                   ,CONVERT(VARCHAR(8000),New.CollegeDivId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.DeptId = New.DeptId
                            WHERE   Old.CollegeDivId <> New.CollegeDivId
                                    OR (
                                         Old.CollegeDivId IS NULL
                                         AND New.CollegeDivId IS NOT NULL
                                       )
                                    OR (
                                         New.CollegeDivId IS NULL
                                         AND Old.CollegeDivId IS NOT NULL
                                       ); 
                IF UPDATE(DeptCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.DeptId
                                   ,'DeptCode'
                                   ,CONVERT(VARCHAR(8000),Old.DeptCode,121)
                                   ,CONVERT(VARCHAR(8000),New.DeptCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.DeptId = New.DeptId
                            WHERE   Old.DeptCode <> New.DeptCode
                                    OR (
                                         Old.DeptCode IS NULL
                                         AND New.DeptCode IS NOT NULL
                                       )
                                    OR (
                                         New.DeptCode IS NULL
                                         AND Old.DeptCode IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.DeptId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.DeptId = New.DeptId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(DeptDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.DeptId
                                   ,'DeptDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.DeptDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.DeptDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.DeptId = New.DeptId
                            WHERE   Old.DeptDescrip <> New.DeptDescrip
                                    OR (
                                         Old.DeptDescrip IS NULL
                                         AND New.DeptDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.DeptDescrip IS NULL
                                         AND Old.DeptDescrip IS NOT NULL
                                       ); 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[arDepartments] ADD CONSTRAINT [PK_arDepartments_DeptId] PRIMARY KEY CLUSTERED  ([DeptId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_arDepartments_DeptCode_DeptDescrip] ON [dbo].[arDepartments] ([DeptCode], [DeptDescrip]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arDepartments] ADD CONSTRAINT [FK_arDepartments_arCollegeDivisions_CollegeDivId_CollegeDivId] FOREIGN KEY ([CollegeDivId]) REFERENCES [dbo].[arCollegeDivisions] ([CollegeDivId])
GO
ALTER TABLE [dbo].[arDepartments] ADD CONSTRAINT [FK_arDepartments_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
