CREATE TABLE [dbo].[arProbWarningTypes]
(
[ProbWarningTypeId] [int] NOT NULL,
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arProbWarningTypes] ADD CONSTRAINT [PK_arProbWarningTypes_ProbWarningTypeId] PRIMARY KEY CLUSTERED  ([ProbWarningTypeId]) ON [PRIMARY]
GO
