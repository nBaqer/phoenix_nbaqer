CREATE TABLE [dbo].[adReqsEffectiveDates]
(
[adReqEffectiveDateId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_adReqsEffectiveDates_adReqEffectiveDateId] DEFAULT (newid()),
[adReqId] [uniqueidentifier] NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[MinScore] [decimal] (19, 3) NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[MandatoryRequirement] [bit] NULL,
[ValidDays] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adReqsEffectiveDates] ADD CONSTRAINT [PK_adReqsEffectiveDates_adReqEffectiveDateId] PRIMARY KEY CLUSTERED  ([adReqEffectiveDateId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adReqsEffectiveDates] ADD CONSTRAINT [FK_adReqsEffectiveDates_adReqs_adReqId_adReqId] FOREIGN KEY ([adReqId]) REFERENCES [dbo].[adReqs] ([adReqId])
GO
