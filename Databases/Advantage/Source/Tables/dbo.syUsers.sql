CREATE TABLE [dbo].[syUsers]
(
[UserId] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[FullName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Password] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CS_AS NOT NULL,
[ConfirmPassword] [varchar] (40) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Salt] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountActive] [bit] NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CampusId] [uniqueidentifier] NULL,
[ShowDefaultCampus] [bit] NOT NULL CONSTRAINT [DF_syUsers_ShowDefaultCampus] DEFAULT ((0)),
[ModuleId] [smallint] NOT NULL,
[IsAdvantageSuperUser] [bit] NOT NULL CONSTRAINT [DF_syUsers_IsAdvantageSuperUser] DEFAULT ((0)),
[IsDefaultAdminRep] [bit] NULL CONSTRAINT [DF_syUsers_IsDefaultAdminRep] DEFAULT ((0)),
[IsLoggedIn] [bit] NULL CONSTRAINT [DF_syUsers_IsLoggedIn] DEFAULT ((0)),
[UserTypeId] [int] NOT NULL CONSTRAINT [DF_syUsers_UserTypeId] DEFAULT ((1))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syUsers] ADD CONSTRAINT [PK_syUsers_UserId] PRIMARY KEY CLUSTERED  ([UserId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_syUsers_Email_CampusId] ON [dbo].[syUsers] ([Email], [CampusId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_syUsers_UserName_Password_CampusId] ON [dbo].[syUsers] ([UserName], [Password], [CampusId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syUsers] ADD CONSTRAINT [FK_syUsers_syCampuses_CampusId_CampusId] FOREIGN KEY ([CampusId]) REFERENCES [dbo].[syCampuses] ([CampusId])
GO
ALTER TABLE [dbo].[syUsers] ADD CONSTRAINT [FK_syUsers_syUserType_UserTypeId_UserTypeId] FOREIGN KEY ([UserTypeId]) REFERENCES [dbo].[syUserType] ([UserTypeId])
GO
ALTER TABLE [dbo].[syUsers] ADD CONSTRAINT [FK_syUserTermsOfUse_syUsers_UserId_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[syUsers] ([UserId])
GO
