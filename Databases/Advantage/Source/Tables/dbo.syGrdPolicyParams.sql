CREATE TABLE [dbo].[syGrdPolicyParams]
(
[GrdPolParamId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_syGrdPolicyParams_GrdPolParamId] DEFAULT (newid()),
[GrdPolicyId] [int] NOT NULL,
[Param] [tinyint] NOT NULL,
[Seq] [tinyint] NOT NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syGrdPolicyParams] ADD CONSTRAINT [PK_syGrdPolicyParams_GrdPolParamId] PRIMARY KEY CLUSTERED  ([GrdPolParamId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syGrdPolicyParams] ADD CONSTRAINT [FK_syGrdPolicyParams_syGrdPolicies_GrdPolicyId_GrdPolicyId] FOREIGN KEY ([GrdPolicyId]) REFERENCES [dbo].[syGrdPolicies] ([GrdPolicyId])
GO
