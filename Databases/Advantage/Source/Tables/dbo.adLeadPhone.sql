CREATE TABLE [dbo].[adLeadPhone]
(
[LeadPhoneId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_adLeadPhone_LeadPhoneId] DEFAULT (newid()),
[LeadId] [uniqueidentifier] NOT NULL,
[PhoneTypeId] [uniqueidentifier] NOT NULL,
[Phone] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Position] [int] NOT NULL CONSTRAINT [DF_adLeadPhone_Position] DEFAULT ((0)),
[Extension] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_adLeadPhone_extension] DEFAULT (''),
[IsForeignPhone] [bit] NOT NULL CONSTRAINT [DF_adLeadPhone_IsForeignPhone] DEFAULT ((0)),
[IsBest] [bit] NOT NULL CONSTRAINT [DF_adLeadPhone_IsBest] DEFAULT ((0)),
[IsShowOnLeadPage] [bit] NOT NULL CONSTRAINT [DF_adLeadPhone_IsShowOnLeadPage] DEFAULT ((0)),
[StatusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
--========================================================================================== 
-- TRIGGER adLeadPhone_Audit_Delete 
-- DELETE  add Audit History when delete records in adLeadPhone
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadPhone_Audit_Delete] ON [dbo].[adLeadPhone]
    FOR DELETE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Deleted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Deleted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Deleted
                        );
 
        IF @EventRows > 0
            BEGIN
                EXEC fmAuditHistAdd @AuditHistId,'adLeadPhone','D',@EventRows,@EventDate,@UserName;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadPhoneId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),Old.LeadId)
                        FROM    Deleted AS Old;
                -- PhoneTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadPhoneId
                               ,'PhoneTypeId'
                               ,CONVERT(VARCHAR(MAX),Old.PhoneTypeId)
                        FROM    Deleted AS Old;
                -- Phone 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadPhoneId
                               ,'Phone'
                               ,CONVERT(VARCHAR(MAX),Old.Phone)
                        FROM    Deleted AS Old;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadPhoneId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                        FROM    Deleted AS Old;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadPhoneId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),Old.ModUser)
                        FROM    Deleted AS Old;
                -- Position 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadPhoneId
                               ,'Position'
                               ,CONVERT(VARCHAR(MAX),Old.Position)
                        FROM    Deleted AS Old;
                -- Extension 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadPhoneId
                               ,'Extension'
                               ,CONVERT(VARCHAR(MAX),Old.Extension)
                        FROM    Deleted AS Old;
                -- IsForeignPhone 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadPhoneId
                               ,'IsForeignPhone'
                               ,CONVERT(VARCHAR(MAX),Old.IsForeignPhone)
                        FROM    Deleted AS Old;
                -- IsBest 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadPhoneId
                               ,'IsBest'
                               ,CONVERT(VARCHAR(MAX),Old.IsBest)
                        FROM    Deleted AS Old;
                -- IsShowOnLeadPage 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadPhoneId
                               ,'IsShowOnLeadPage'
                               ,CONVERT(VARCHAR(MAX),Old.IsShowOnLeadPage)
                        FROM    Deleted AS Old;
                -- StatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadPhoneId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(MAX),Old.StatusId)
                        FROM    Deleted AS Old;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadPhone_Audit_Delete 
--========================================================================================== 
 

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
--========================================================================================== 
-- TRIGGER adLeadPhone_Audit_Insert 
-- INSERT  add Audit History when insert records in adLeadPhone
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadPhone_Audit_Insert] ON [dbo].[adLeadPhone]
    FOR INSERT
AS
    BEGIN 
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadPhone','I',@EventRows,@EventDate,@UserName;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadPhoneId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),New.LeadId)
                        FROM    Inserted AS New;
                -- PhoneTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadPhoneId
                               ,'PhoneTypeId'
                               ,CONVERT(VARCHAR(MAX),New.PhoneTypeId)
                        FROM    Inserted AS New;
                -- Phone 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadPhoneId
                               ,'Phone'
                               ,CONVERT(VARCHAR(MAX),New.Phone)
                        FROM    Inserted AS New;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadPhoneId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                        FROM    Inserted AS New;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadPhoneId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),New.ModUser)
                        FROM    Inserted AS New;
                -- Position 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadPhoneId
                               ,'Position'
                               ,CONVERT(VARCHAR(MAX),New.Position)
                        FROM    Inserted AS New;
                -- Extension 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadPhoneId
                               ,'Extension'
                               ,CONVERT(VARCHAR(MAX),New.Extension)
                        FROM    Inserted AS New;
                -- IsForeignPhone 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadPhoneId
                               ,'IsForeignPhone'
                               ,CONVERT(VARCHAR(MAX),New.IsForeignPhone)
                        FROM    Inserted AS New;
                -- IsBest 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadPhoneId
                               ,'IsBest'
                               ,CONVERT(VARCHAR(MAX),New.IsBest)
                        FROM    Inserted AS New;
                -- IsShowOnLeadPage 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadPhoneId
                               ,'IsShowOnLeadPage'
                               ,CONVERT(VARCHAR(MAX),New.IsShowOnLeadPage)
                        FROM    Inserted AS New;
                -- StatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadPhoneId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(MAX),New.StatusId)
                        FROM    Inserted AS New;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadPhone_Audit_Insert 
--========================================================================================== 
 

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--========================================================================================== 
-- TRIGGER adLeadPhone_Audit_Update 
-- UPDATE  add Audit History when update fields in adLeadPhone
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadPhone_Audit_Update] ON [dbo].[adLeadPhone]
    FOR UPDATE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadPhone','U',@EventRows,@EventDate,@UserName;
                -- LeadId 
                IF UPDATE(LeadId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadPhoneId
                                       ,'LeadId'
                                       ,CONVERT(VARCHAR(MAX),Old.LeadId)
                                       ,CONVERT(VARCHAR(MAX),New.LeadId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadPhoneId = New.LeadPhoneId
                                WHERE   Old.LeadId <> New.LeadId
                                        OR (
                                             Old.LeadId IS NULL
                                             AND New.LeadId IS NOT NULL
                                           )
                                        OR (
                                             New.LeadId IS NULL
                                             AND Old.LeadId IS NOT NULL
                                           );
                    END;
                -- PhoneTypeId 
                IF UPDATE(PhoneTypeId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadPhoneId
                                       ,'PhoneTypeId'
                                       ,CONVERT(VARCHAR(MAX),Old.PhoneTypeId)
                                       ,CONVERT(VARCHAR(MAX),New.PhoneTypeId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadPhoneId = New.LeadPhoneId
                                WHERE   Old.PhoneTypeId <> New.PhoneTypeId
                                        OR (
                                             Old.PhoneTypeId IS NULL
                                             AND New.PhoneTypeId IS NOT NULL
                                           )
                                        OR (
                                             New.PhoneTypeId IS NULL
                                             AND Old.PhoneTypeId IS NOT NULL
                                           );
                    END;
                -- Phone 
                IF UPDATE(Phone)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadPhoneId
                                       ,'Phone'
                                       ,CONVERT(VARCHAR(MAX),Old.Phone)
                                       ,CONVERT(VARCHAR(MAX),New.Phone)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadPhoneId = New.LeadPhoneId
                                WHERE   Old.Phone <> New.Phone
                                        OR (
                                             Old.Phone IS NULL
                                             AND New.Phone IS NOT NULL
                                           )
                                        OR (
                                             New.Phone IS NULL
                                             AND Old.Phone IS NOT NULL
                                           );
                    END;
                -- ModDate 
                IF UPDATE(ModDate)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadPhoneId
                                       ,'ModDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadPhoneId = New.LeadPhoneId
                                WHERE   Old.ModDate <> New.ModDate
                                        OR (
                                             Old.ModDate IS NULL
                                             AND New.ModDate IS NOT NULL
                                           )
                                        OR (
                                             New.ModDate IS NULL
                                             AND Old.ModDate IS NOT NULL
                                           );
                    END;
                -- ModUser 
                IF UPDATE(ModUser)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadPhoneId
                                       ,'ModUser'
                                       ,CONVERT(VARCHAR(MAX),Old.ModUser)
                                       ,CONVERT(VARCHAR(MAX),New.ModUser)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadPhoneId = New.LeadPhoneId
                                WHERE   Old.ModUser <> New.ModUser
                                        OR (
                                             Old.ModUser IS NULL
                                             AND New.ModUser IS NOT NULL
                                           )
                                        OR (
                                             New.ModUser IS NULL
                                             AND Old.ModUser IS NOT NULL
                                           );
                    END;
                -- Position 
                IF UPDATE(Position)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadPhoneId
                                       ,'Position'
                                       ,CONVERT(VARCHAR(MAX),Old.Position)
                                       ,CONVERT(VARCHAR(MAX),New.Position)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadPhoneId = New.LeadPhoneId
                                WHERE   Old.Position <> New.Position
                                        OR (
                                             Old.Position IS NULL
                                             AND New.Position IS NOT NULL
                                           )
                                        OR (
                                             New.Position IS NULL
                                             AND Old.Position IS NOT NULL
                                           );
                    END;
                -- Extension 
                IF UPDATE(Extension)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadPhoneId
                                       ,'Extension'
                                       ,CONVERT(VARCHAR(MAX),Old.Extension)
                                       ,CONVERT(VARCHAR(MAX),New.Extension)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadPhoneId = New.LeadPhoneId
                                WHERE   Old.Extension <> New.Extension
                                        OR (
                                             Old.Extension IS NULL
                                             AND New.Extension IS NOT NULL
                                           )
                                        OR (
                                             New.Extension IS NULL
                                             AND Old.Extension IS NOT NULL
                                           );
                    END;
                -- IsForeignPhone 
                IF UPDATE(IsForeignPhone)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadPhoneId
                                       ,'IsForeignPhone'
                                       ,CONVERT(VARCHAR(MAX),Old.IsForeignPhone)
                                       ,CONVERT(VARCHAR(MAX),New.IsForeignPhone)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadPhoneId = New.LeadPhoneId
                                WHERE   Old.IsForeignPhone <> New.IsForeignPhone
                                        OR (
                                             Old.IsForeignPhone IS NULL
                                             AND New.IsForeignPhone IS NOT NULL
                                           )
                                        OR (
                                             New.IsForeignPhone IS NULL
                                             AND Old.IsForeignPhone IS NOT NULL
                                           );
                    END;
                -- IsBest 
                IF UPDATE(IsBest)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadPhoneId
                                       ,'IsBest'
                                       ,CONVERT(VARCHAR(MAX),Old.IsBest)
                                       ,CONVERT(VARCHAR(MAX),New.IsBest)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadPhoneId = New.LeadPhoneId
                                WHERE   Old.IsBest <> New.IsBest
                                        OR (
                                             Old.IsBest IS NULL
                                             AND New.IsBest IS NOT NULL
                                           )
                                        OR (
                                             New.IsBest IS NULL
                                             AND Old.IsBest IS NOT NULL
                                           );
                    END;
                -- IsShowOnLeadPage 
                IF UPDATE(IsShowOnLeadPage)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadPhoneId
                                       ,'IsShowOnLeadPage'
                                       ,CONVERT(VARCHAR(MAX),Old.IsShowOnLeadPage)
                                       ,CONVERT(VARCHAR(MAX),New.IsShowOnLeadPage)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadPhoneId = New.LeadPhoneId
                                WHERE   Old.IsShowOnLeadPage <> New.IsShowOnLeadPage
                                        OR (
                                             Old.IsShowOnLeadPage IS NULL
                                             AND New.IsShowOnLeadPage IS NOT NULL
                                           )
                                        OR (
                                             New.IsShowOnLeadPage IS NULL
                                             AND Old.IsShowOnLeadPage IS NOT NULL
                                           );
                    END;
                -- StatusId 
                IF UPDATE(StatusId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadPhoneId
                                       ,'StatusId'
                                       ,CONVERT(VARCHAR(MAX),Old.StatusId)
                                       ,CONVERT(VARCHAR(MAX),New.StatusId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadPhoneId = New.LeadPhoneId
                                WHERE   Old.StatusId <> New.StatusId
                                        OR (
                                             Old.StatusId IS NULL
                                             AND New.StatusId IS NOT NULL
                                           )
                                        OR (
                                             New.StatusId IS NULL
                                             AND Old.StatusId IS NOT NULL
                                           );
                    END;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadPhone_Audit_Update 
--========================================================================================== 
 

GO
ALTER TABLE [dbo].[adLeadPhone] ADD CONSTRAINT [PK_adLeadPhone_LeadPhoneId] PRIMARY KEY CLUSTERED  ([LeadPhoneId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_adLeadPhone_LeadId_Position] ON [dbo].[adLeadPhone] ([LeadId], [Position]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adLeadPhone] ADD CONSTRAINT [FK_adLeadPhone_adLeads_LeadId_LeadId] FOREIGN KEY ([LeadId]) REFERENCES [dbo].[adLeads] ([LeadId])
GO
ALTER TABLE [dbo].[adLeadPhone] ADD CONSTRAINT [FK_adLeadPhone_syPhoneType_PhoneTypeId_PhoneTypeId] FOREIGN KEY ([PhoneTypeId]) REFERENCES [dbo].[syPhoneType] ([PhoneTypeId])
GO
ALTER TABLE [dbo].[adLeadPhone] ADD CONSTRAINT [FK_adLeadPhone_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
