CREATE TABLE [dbo].[msgRules]
(
[RuleId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_msgRules_RuleId] DEFAULT (newid()),
[Type] [smallint] NULL,
[Code] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CampGroupId] [uniqueidentifier] NULL,
[RuleSql] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TemplateId] [uniqueidentifier] NULL,
[RecipientType] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReType] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeliveryType] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastRun] [datetime] NULL,
[LastRunError] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [smalldatetime] NULL,
[ModUser] [uniqueidentifier] NULL,
[Active] [tinyint] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[msgRules] ADD CONSTRAINT [PK_msgRules_RuleId] PRIMARY KEY CLUSTERED  ([RuleId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[msgRules] ADD CONSTRAINT [FK_msgRules_syCampGrps_CampGroupId_CampGrpId] FOREIGN KEY ([CampGroupId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[msgRules] ADD CONSTRAINT [FK_msgRules_msgTemplates_TemplateId_TemplateId] FOREIGN KEY ([TemplateId]) REFERENCES [dbo].[msgTemplates] ([TemplateId])
GO
