CREATE TABLE [dbo].[saTransTypes]
(
[TransTypeId] [int] NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saTransTypes] ADD CONSTRAINT [PK_saTransTypes_TransTypeId] PRIMARY KEY CLUSTERED  ([TransTypeId]) ON [PRIMARY]
GO
