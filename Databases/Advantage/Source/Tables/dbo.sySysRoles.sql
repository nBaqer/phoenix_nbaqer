CREATE TABLE [dbo].[sySysRoles]
(
[SysRoleId] [int] NOT NULL,
[Descrip] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[RoleTypeId] [int] NOT NULL CONSTRAINT [DF_sySysRoles_RoleTypeId] DEFAULT ((1)),
[Permission] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[sySysRoles] ADD CONSTRAINT [PK_sySysRoles_SysRoleId] PRIMARY KEY CLUSTERED  ([SysRoleId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[sySysRoles] ADD CONSTRAINT [FK_sySysRoles_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
ALTER TABLE [dbo].[sySysRoles] ADD CONSTRAINT [FK_sySysRoles_syUserType_RoleTypeId_UserTypeId] FOREIGN KEY ([RoleTypeId]) REFERENCES [dbo].[syUserType] ([UserTypeId])
GO
GRANT DELETE ON  [dbo].[sySysRoles] TO [AdvantageRole]
GO
GRANT INSERT ON  [dbo].[sySysRoles] TO [AdvantageRole]
GO
GRANT REFERENCES ON  [dbo].[sySysRoles] TO [AdvantageRole]
GO
GRANT SELECT ON  [dbo].[sySysRoles] TO [AdvantageRole]
GO
GRANT UPDATE ON  [dbo].[sySysRoles] TO [AdvantageRole]
GO
