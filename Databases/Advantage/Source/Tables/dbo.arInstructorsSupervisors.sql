CREATE TABLE [dbo].[arInstructorsSupervisors]
(
[InstructorId] [uniqueidentifier] NOT NULL,
[SupervisorId] [uniqueidentifier] NOT NULL,
[ModUser] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arInstructorsSupervisors] ADD CONSTRAINT [PK_arInstructorsSupervisors_InstructorId] PRIMARY KEY CLUSTERED  ([InstructorId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arInstructorsSupervisors] ADD CONSTRAINT [FK_arInstructorsSupervisors_syUsers_InstructorId_UserId] FOREIGN KEY ([InstructorId]) REFERENCES [dbo].[syUsers] ([UserId])
GO
ALTER TABLE [dbo].[arInstructorsSupervisors] ADD CONSTRAINT [FK_arInstructorsSupervisors_syUsers_SupervisorId_UserId] FOREIGN KEY ([SupervisorId]) REFERENCES [dbo].[syUsers] ([UserId])
GO
