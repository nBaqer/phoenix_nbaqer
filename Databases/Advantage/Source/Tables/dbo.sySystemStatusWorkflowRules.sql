CREATE TABLE [dbo].[sySystemStatusWorkflowRules]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[UniqueId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_sySystemStatusWorkflowRules_UniqueId] DEFAULT (newid()),
[StatusIdFrom] [int] NULL,
[StatusIdTo] [int] NOT NULL,
[AllowInsert] [bit] NOT NULL CONSTRAINT [DF_sySystemStatusWorkflowRules_AllowInsert] DEFAULT ((0)),
[AllowInsertSameStatusType] [bit] NOT NULL CONSTRAINT [DF_sySystemStatusWorkflowRules_AllowInsertSameStatusType] DEFAULT ((0)),
[AllowEdit] [bit] NOT NULL CONSTRAINT [DF_sySystemStatusWorkflowRules_AllowEdit] DEFAULT ((0)),
[AllowEditSameStatusType] [bit] NOT NULL CONSTRAINT [DF_sySystemStatusWorkflowRules_AllowEditSameStatusType] DEFAULT ((0)),
[AllowDelete] [bit] NOT NULL CONSTRAINT [DF_sySystemStatusWorkflowRules_AllowDelete] DEFAULT ((0)),
[AllowedInBatch] [bit] NOT NULL CONSTRAINT [DF_sySystemStatusWorkflowRules_AllowedInBatch] DEFAULT ((0)),
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL CONSTRAINT [DF_sySystemStatusWorkflowRules_ModDate] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[sySystemStatusWorkflowRules] ADD CONSTRAINT [PK_sySystemStatusWorkflowRules_Id] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[sySystemStatusWorkflowRules] ADD CONSTRAINT [FK_sySystemStatusWorkflowRules_sySysStatus_StatusIdFrom_SysStatusId] FOREIGN KEY ([StatusIdFrom]) REFERENCES [dbo].[sySysStatus] ([SysStatusId])
GO
ALTER TABLE [dbo].[sySystemStatusWorkflowRules] ADD CONSTRAINT [FK_sySystemStatusWorkflowRules_sySysStatus_StatusIdTo_SysStatusId] FOREIGN KEY ([StatusIdTo]) REFERENCES [dbo].[sySysStatus] ([SysStatusId])
GO
