CREATE TABLE [dbo].[sySDFModuleValue]
(
[SDFPKID] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_sySDFModuleValue_SDFPKID] DEFAULT (newid()),
[PgPKID] [uniqueidentifier] NULL CONSTRAINT [DF_sySDFModuleValue_PgPKID] DEFAULT (newid()),
[SDFID] [uniqueidentifier] NULL,
[SDFValue] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[sySDFModuleValue] ADD CONSTRAINT [PK_sySDFModuleValue_SDFPKID] PRIMARY KEY CLUSTERED  ([SDFPKID]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_sySDFModuleValue_PgPKID_SDFID] ON [dbo].[sySDFModuleValue] ([PgPKID], [SDFID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[sySDFModuleValue] ADD CONSTRAINT [FK_sySDFModuleValue_sySDF_SDFID_SDFId] FOREIGN KEY ([SDFID]) REFERENCES [dbo].[sySDF] ([SDFId])
GO
