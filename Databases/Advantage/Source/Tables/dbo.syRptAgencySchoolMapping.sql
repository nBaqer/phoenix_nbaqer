CREATE TABLE [dbo].[syRptAgencySchoolMapping]
(
[MappingId] [uniqueidentifier] NOT NULL,
[RptAgencyFldValId] [int] NULL,
[SchoolDescripId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syRptAgencySchoolMapping] ADD CONSTRAINT [PK_syRptAgencySchoolMapping_MappingId] PRIMARY KEY CLUSTERED  ([MappingId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syRptAgencySchoolMapping] ADD CONSTRAINT [FK_syRptAgencySchoolMapping_syRptAgencyFldValues_RptAgencyFldValId_RptAgencyFldValId] FOREIGN KEY ([RptAgencyFldValId]) REFERENCES [dbo].[syRptAgencyFldValues] ([RptAgencyFldValId])
GO
