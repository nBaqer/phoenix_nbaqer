CREATE TABLE [dbo].[adLeadEducation]
(
[LeadEducationId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_adLeadEducation_LeadEducationId] DEFAULT (newid()),
[LeadId] [uniqueidentifier] NOT NULL,
[EducationInstId] [uniqueidentifier] NOT NULL,
[EducationInstType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[GraduatedDate] [datetime] NULL,
[FinalGrade] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CertificateId] [uniqueidentifier] NULL,
[Comments] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[Major] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_adLeadEducation_StatusId] DEFAULT ([dbo].[UDF_GetSyStatusesValue]('A')),
[Graduate] [bit] NOT NULL CONSTRAINT [DF_adLeadEducation_Graduate] DEFAULT ((0)),
[GPA] [decimal] (3, 2) NULL,
[Rank] [int] NULL,
[Percentile] [int] NULL,
[Certificate] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[adLeadEducation_Audit_Delete] ON [dbo].[adLeadEducation]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adLeadEducation','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEducationId
                               ,'CertificateId'
                               ,CONVERT(VARCHAR(8000),Old.CertificateId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEducationId
                               ,'LeadID'
                               ,CONVERT(VARCHAR(8000),Old.LeadId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEducationId
                               ,'EducationInstId'
                               ,CONVERT(VARCHAR(8000),Old.EducationInstId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEducationId
                               ,'GraduatedDate'
                               ,CONVERT(VARCHAR(8000),Old.GraduatedDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEducationId
                               ,'Comments'
                               ,CONVERT(VARCHAR(8000),Old.Comments,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEducationId
                               ,'Major'
                               ,CONVERT(VARCHAR(8000),Old.Major,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEducationId
                               ,'FinalGrade'
                               ,CONVERT(VARCHAR(8000),Old.FinalGrade,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEducationId
                               ,'EducationInstType'
                               ,CONVERT(VARCHAR(8000),Old.EducationInstType,121)
                        FROM    Deleted Old; 
            END; 
        END;



    SET NOCOUNT OFF; 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[adLeadEducation_Audit_Insert] ON [dbo].[adLeadEducation]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adLeadEducation','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEducationId
                               ,'CertificateId'
                               ,CONVERT(VARCHAR(8000),New.CertificateId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEducationId
                               ,'LeadID'
                               ,CONVERT(VARCHAR(8000),New.LeadId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEducationId
                               ,'EducationInstId'
                               ,CONVERT(VARCHAR(8000),New.EducationInstId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEducationId
                               ,'GraduatedDate'
                               ,CONVERT(VARCHAR(8000),New.GraduatedDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEducationId
                               ,'Comments'
                               ,CONVERT(VARCHAR(8000),New.Comments,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEducationId
                               ,'Major'
                               ,CONVERT(VARCHAR(8000),New.Major,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEducationId
                               ,'FinalGrade'
                               ,CONVERT(VARCHAR(8000),New.FinalGrade,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEducationId
                               ,'EducationInstType'
                               ,CONVERT(VARCHAR(8000),New.EducationInstType,121)
                        FROM    Inserted New; 
            END; 
        END;



    SET NOCOUNT OFF; 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[adLeadEducation_Audit_Update] ON [dbo].[adLeadEducation]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adLeadEducation','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(CertificateId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.LeadEducationId
                                   ,'CertificateId'
                                   ,CONVERT(VARCHAR(8000),Old.CertificateId,121)
                                   ,CONVERT(VARCHAR(8000),New.CertificateId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.LeadEducationId = New.LeadEducationId
                            WHERE   Old.CertificateId <> New.CertificateId
                                    OR (
                                         Old.CertificateId IS NULL
                                         AND New.CertificateId IS NOT NULL
                                       )
                                    OR (
                                         New.CertificateId IS NULL
                                         AND Old.CertificateId IS NOT NULL
                                       ); 
                IF UPDATE(LeadId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.LeadEducationId
                                   ,'LeadID'
                                   ,CONVERT(VARCHAR(8000),Old.LeadId,121)
                                   ,CONVERT(VARCHAR(8000),New.LeadId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.LeadEducationId = New.LeadEducationId
                            WHERE   Old.LeadId <> New.LeadId
                                    OR (
                                         Old.LeadId IS NULL
                                         AND New.LeadId IS NOT NULL
                                       )
                                    OR (
                                         New.LeadId IS NULL
                                         AND Old.LeadId IS NOT NULL
                                       ); 
                IF UPDATE(EducationInstId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.LeadEducationId
                                   ,'EducationInstId'
                                   ,CONVERT(VARCHAR(8000),Old.EducationInstId,121)
                                   ,CONVERT(VARCHAR(8000),New.EducationInstId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.LeadEducationId = New.LeadEducationId
                            WHERE   Old.EducationInstId <> New.EducationInstId
                                    OR (
                                         Old.EducationInstId IS NULL
                                         AND New.EducationInstId IS NOT NULL
                                       )
                                    OR (
                                         New.EducationInstId IS NULL
                                         AND Old.EducationInstId IS NOT NULL
                                       ); 
                IF UPDATE(GraduatedDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.LeadEducationId
                                   ,'GraduatedDate'
                                   ,CONVERT(VARCHAR(8000),Old.GraduatedDate,121)
                                   ,CONVERT(VARCHAR(8000),New.GraduatedDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.LeadEducationId = New.LeadEducationId
                            WHERE   Old.GraduatedDate <> New.GraduatedDate
                                    OR (
                                         Old.GraduatedDate IS NULL
                                         AND New.GraduatedDate IS NOT NULL
                                       )
                                    OR (
                                         New.GraduatedDate IS NULL
                                         AND Old.GraduatedDate IS NOT NULL
                                       ); 
                IF UPDATE(comments)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.LeadEducationId
                                   ,'Comments'
                                   ,CONVERT(VARCHAR(8000),Old.Comments,121)
                                   ,CONVERT(VARCHAR(8000),New.Comments,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.LeadEducationId = New.LeadEducationId
                            WHERE   Old.Comments <> New.Comments
                                    OR (
                                         Old.Comments IS NULL
                                         AND New.Comments IS NOT NULL
                                       )
                                    OR (
                                         New.Comments IS NULL
                                         AND Old.Comments IS NOT NULL
                                       ); 
                IF UPDATE(Major)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.LeadEducationId
                                   ,'Major'
                                   ,CONVERT(VARCHAR(8000),Old.Major,121)
                                   ,CONVERT(VARCHAR(8000),New.Major,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.LeadEducationId = New.LeadEducationId
                            WHERE   Old.Major <> New.Major
                                    OR (
                                         Old.Major IS NULL
                                         AND New.Major IS NOT NULL
                                       )
                                    OR (
                                         New.Major IS NULL
                                         AND Old.Major IS NOT NULL
                                       ); 
                IF UPDATE(FinalGrade)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.LeadEducationId
                                   ,'FinalGrade'
                                   ,CONVERT(VARCHAR(8000),Old.FinalGrade,121)
                                   ,CONVERT(VARCHAR(8000),New.FinalGrade,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.LeadEducationId = New.LeadEducationId
                            WHERE   Old.FinalGrade <> New.FinalGrade
                                    OR (
                                         Old.FinalGrade IS NULL
                                         AND New.FinalGrade IS NOT NULL
                                       )
                                    OR (
                                         New.FinalGrade IS NULL
                                         AND Old.FinalGrade IS NOT NULL
                                       ); 
                IF UPDATE(EducationInstType)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.LeadEducationId
                                   ,'EducationInstType'
                                   ,CONVERT(VARCHAR(8000),Old.EducationInstType,121)
                                   ,CONVERT(VARCHAR(8000),New.EducationInstType,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.LeadEducationId = New.LeadEducationId
                            WHERE   Old.EducationInstType <> New.EducationInstType
                                    OR (
                                         Old.EducationInstType IS NULL
                                         AND New.EducationInstType IS NOT NULL
                                       )
                                    OR (
                                         New.EducationInstType IS NULL
                                         AND Old.EducationInstType IS NOT NULL
                                       ); 
            END; 
        END;



    SET NOCOUNT OFF; 
GO
ALTER TABLE [dbo].[adLeadEducation] ADD CONSTRAINT [PK_adLeadEducation_LeadEducationId] PRIMARY KEY CLUSTERED  ([LeadEducationId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adLeadEducation] ADD CONSTRAINT [FK_adLeadEducation_adLeads_LeadId_LeadId] FOREIGN KEY ([LeadId]) REFERENCES [dbo].[adLeads] ([LeadId])
GO
ALTER TABLE [dbo].[adLeadEducation] ADD CONSTRAINT [FK_adLeadEducation_arDegrees_CertificateId_DegreeId] FOREIGN KEY ([CertificateId]) REFERENCES [dbo].[arDegrees] ([DegreeId])
GO
ALTER TABLE [dbo].[adLeadEducation] ADD CONSTRAINT [FK_adLeadEducation_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
