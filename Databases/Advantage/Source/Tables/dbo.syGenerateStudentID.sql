CREATE TABLE [dbo].[syGenerateStudentID]
(
[Student_SeqID] [int] NOT NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syGenerateStudentID] ADD CONSTRAINT [PK_syGenerateStudentID_Student_SeqID] PRIMARY KEY CLUSTERED  ([Student_SeqID]) ON [PRIMARY]
GO
