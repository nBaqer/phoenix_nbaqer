CREATE TABLE [dbo].[arFERPAEntity]
(
[FERPAEntityId] [uniqueidentifier] NOT NULL,
[FERPAEntityCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[FERPAEntityDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arFERPAEntity] ADD CONSTRAINT [PK_arFERPAEntity_FERPAEntityId] PRIMARY KEY CLUSTERED  ([FERPAEntityId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arFERPAEntity] ADD CONSTRAINT [FK_arFERPAEntity_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[arFERPAEntity] ADD CONSTRAINT [FK_arFERPAEntity_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
