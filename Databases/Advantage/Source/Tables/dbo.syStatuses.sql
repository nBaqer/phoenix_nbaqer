CREATE TABLE [dbo].[syStatuses]
(
[StatusId] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[Status] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusCode] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syStatuses] ADD CONSTRAINT [PK_syStatuses_StatusId] PRIMARY KEY CLUSTERED  ([StatusId]) ON [PRIMARY]
GO
