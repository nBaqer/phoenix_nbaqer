CREATE TABLE [dbo].[arCampusPrgVersions]
(
[CampusPrgVerId] [uniqueidentifier] NOT NULL,
[CampusId] [uniqueidentifier] NULL,
[PrgVerId] [uniqueidentifier] NOT NULL,
[IsTitleIV] [bit] NOT NULL,
[IsFAMEApproved] [bit] NOT NULL,
[IsSelfPaced] [bit] NULL,
[CalculationPeriodTypeId] [uniqueidentifier] NULL,
[AllowExcusAbsPerPayPrd] [decimal] (18, 1) NULL,
[TermSubEqualInLen] [bit] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arCampusPrgVersions] ADD CONSTRAINT [PK_arCampusPrgVersions] PRIMARY KEY CLUSTERED  ([CampusPrgVerId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arCampusPrgVersions] ADD CONSTRAINT [FK_arCampusPrgVersions_arPrgVersions_PrgVerId_PrgVerId] FOREIGN KEY ([PrgVerId]) REFERENCES [dbo].[arPrgVersions] ([PrgVerId])
GO
