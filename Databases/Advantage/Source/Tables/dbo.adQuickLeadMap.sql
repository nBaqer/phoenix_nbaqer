CREATE TABLE [dbo].[adQuickLeadMap]
(
[mapId] [bigint] NOT NULL IDENTITY(1, 1),
[fldName] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ctrlIdName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[propName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[parentCtrlId] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[sequence] [bigint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adQuickLeadMap] ADD CONSTRAINT [PK_adQuickLeadMap_mapId] PRIMARY KEY CLUSTERED  ([mapId]) ON [PRIMARY]
GO
