CREATE TABLE [dbo].[saBatchPayments]
(
[BatchPaymentId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_saBatchPayments_BatchPaymentId] DEFAULT (newid()),
[BatchPaymentNumber] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FundSourceId] [uniqueidentifier] NOT NULL,
[BatchPaymentDate] [datetime] NOT NULL,
[BatchPaymentDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BatchPaymentAmount] [money] NOT NULL CONSTRAINT [DF_saBatchPayments_BatchPaymentAmount] DEFAULT ((0.00)),
[UserId] [uniqueidentifier] NOT NULL,
[IsPosted] [bit] NOT NULL CONSTRAINT [DF_saBatchPayments_IsPosted] DEFAULT ((0)),
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saBatchPayments_Audit_Delete] ON [dbo].[saBatchPayments]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saBatchPayments','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BatchPaymentId
                               ,'BatchPaymentNumber'
                               ,CONVERT(VARCHAR(8000),Old.BatchPaymentNumber,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BatchPaymentId
                               ,'FundSourceId'
                               ,CONVERT(VARCHAR(8000),Old.FundSourceId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BatchPaymentId
                               ,'BatchPaymentDate'
                               ,CONVERT(VARCHAR(8000),Old.BatchPaymentDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BatchPaymentId
                               ,'BatchPaymentDescrip'
                               ,CONVERT(VARCHAR(8000),Old.BatchPaymentDescrip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BatchPaymentId
                               ,'BatchPaymentAmount'
                               ,CONVERT(VARCHAR(8000),Old.BatchPaymentAmount,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BatchPaymentId
                               ,'UserId'
                               ,CONVERT(VARCHAR(8000),Old.UserId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.BatchPaymentId
                               ,'IsPosted'
                               ,CONVERT(VARCHAR(8000),Old.IsPosted,121)
                        FROM    Deleted Old; 
            END; 
        END; 




    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saBatchPayments_Audit_Insert] ON [dbo].[saBatchPayments]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saBatchPayments','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BatchPaymentId
                               ,'BatchPaymentNumber'
                               ,CONVERT(VARCHAR(8000),New.BatchPaymentNumber,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BatchPaymentId
                               ,'FundSourceId'
                               ,CONVERT(VARCHAR(8000),New.FundSourceId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BatchPaymentId
                               ,'BatchPaymentDate'
                               ,CONVERT(VARCHAR(8000),New.BatchPaymentDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BatchPaymentId
                               ,'BatchPaymentDescrip'
                               ,CONVERT(VARCHAR(8000),New.BatchPaymentDescrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BatchPaymentId
                               ,'BatchPaymentAmount'
                               ,CONVERT(VARCHAR(8000),New.BatchPaymentAmount,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BatchPaymentId
                               ,'UserId'
                               ,CONVERT(VARCHAR(8000),New.UserId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.BatchPaymentId
                               ,'IsPosted'
                               ,CONVERT(VARCHAR(8000),New.IsPosted,121)
                        FROM    Inserted New; 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO
GO

GO

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saBatchPayments_Audit_Update] ON [dbo].[saBatchPayments]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saBatchPayments','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(BatchPaymentNumber)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BatchPaymentId
                                   ,'BatchPaymentNumber'
                                   ,CONVERT(VARCHAR(8000),Old.BatchPaymentNumber,121)
                                   ,CONVERT(VARCHAR(8000),New.BatchPaymentNumber,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BatchPaymentId = New.BatchPaymentId
                            WHERE   Old.BatchPaymentNumber <> New.BatchPaymentNumber
                                    OR (
                                         Old.BatchPaymentNumber IS NULL
                                         AND New.BatchPaymentNumber IS NOT NULL
                                       )
                                    OR (
                                         New.BatchPaymentNumber IS NULL
                                         AND Old.BatchPaymentNumber IS NOT NULL
                                       ); 
                IF UPDATE(FundSourceId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BatchPaymentId
                                   ,'FundSourceId'
                                   ,CONVERT(VARCHAR(8000),Old.FundSourceId,121)
                                   ,CONVERT(VARCHAR(8000),New.FundSourceId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BatchPaymentId = New.BatchPaymentId
                            WHERE   Old.FundSourceId <> New.FundSourceId
                                    OR (
                                         Old.FundSourceId IS NULL
                                         AND New.FundSourceId IS NOT NULL
                                       )
                                    OR (
                                         New.FundSourceId IS NULL
                                         AND Old.FundSourceId IS NOT NULL
                                       ); 
                IF UPDATE(BatchPaymentDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BatchPaymentId
                                   ,'BatchPaymentDate'
                                   ,CONVERT(VARCHAR(8000),Old.BatchPaymentDate,121)
                                   ,CONVERT(VARCHAR(8000),New.BatchPaymentDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BatchPaymentId = New.BatchPaymentId
                            WHERE   Old.BatchPaymentDate <> New.BatchPaymentDate
                                    OR (
                                         Old.BatchPaymentDate IS NULL
                                         AND New.BatchPaymentDate IS NOT NULL
                                       )
                                    OR (
                                         New.BatchPaymentDate IS NULL
                                         AND Old.BatchPaymentDate IS NOT NULL
                                       ); 
                IF UPDATE(BatchPaymentDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BatchPaymentId
                                   ,'BatchPaymentDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.BatchPaymentDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.BatchPaymentDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BatchPaymentId = New.BatchPaymentId
                            WHERE   Old.BatchPaymentDescrip <> New.BatchPaymentDescrip
                                    OR (
                                         Old.BatchPaymentDescrip IS NULL
                                         AND New.BatchPaymentDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.BatchPaymentDescrip IS NULL
                                         AND Old.BatchPaymentDescrip IS NOT NULL
                                       ); 
                IF UPDATE(BatchPaymentAmount)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BatchPaymentId
                                   ,'BatchPaymentAmount'
                                   ,CONVERT(VARCHAR(8000),Old.BatchPaymentAmount,121)
                                   ,CONVERT(VARCHAR(8000),New.BatchPaymentAmount,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BatchPaymentId = New.BatchPaymentId
                            WHERE   Old.BatchPaymentAmount <> New.BatchPaymentAmount
                                    OR (
                                         Old.BatchPaymentAmount IS NULL
                                         AND New.BatchPaymentAmount IS NOT NULL
                                       )
                                    OR (
                                         New.BatchPaymentAmount IS NULL
                                         AND Old.BatchPaymentAmount IS NOT NULL
                                       ); 
                IF UPDATE(UserId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BatchPaymentId
                                   ,'UserId'
                                   ,CONVERT(VARCHAR(8000),Old.UserId,121)
                                   ,CONVERT(VARCHAR(8000),New.UserId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BatchPaymentId = New.BatchPaymentId
                            WHERE   Old.UserId <> New.UserId
                                    OR (
                                         Old.UserId IS NULL
                                         AND New.UserId IS NOT NULL
                                       )
                                    OR (
                                         New.UserId IS NULL
                                         AND Old.UserId IS NOT NULL
                                       ); 
                IF UPDATE(IsPosted)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.BatchPaymentId
                                   ,'IsPosted'
                                   ,CONVERT(VARCHAR(8000),Old.IsPosted,121)
                                   ,CONVERT(VARCHAR(8000),New.IsPosted,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.BatchPaymentId = New.BatchPaymentId
                            WHERE   Old.IsPosted <> New.IsPosted
                                    OR (
                                         Old.IsPosted IS NULL
                                         AND New.IsPosted IS NOT NULL
                                       )
                                    OR (
                                         New.IsPosted IS NULL
                                         AND Old.IsPosted IS NOT NULL
                                       ); 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[saBatchPayments] ADD CONSTRAINT [PK_saBatchPayments_BatchPaymentId] PRIMARY KEY CLUSTERED  ([BatchPaymentId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_saBatchPayments_BatchPaymentDate] ON [dbo].[saBatchPayments] ([BatchPaymentDate]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_saBatchPayments_BatchPaymentNumber] ON [dbo].[saBatchPayments] ([BatchPaymentNumber]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saBatchPayments] ADD CONSTRAINT [FK_saBatchPayments_saFundSources_FundSourceId_FundSourceId] FOREIGN KEY ([FundSourceId]) REFERENCES [dbo].[saFundSources] ([FundSourceId])
GO
ALTER TABLE [dbo].[saBatchPayments] ADD CONSTRAINT [FK_saBatchPayments_syUsers_UserId_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[syUsers] ([UserId])
GO
