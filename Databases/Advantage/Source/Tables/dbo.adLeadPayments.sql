CREATE TABLE [dbo].[adLeadPayments]
(
[TransactionId] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[PaymentTypeId] [int] NOT NULL CONSTRAINT [DF_adLeadPayments_PaymentTypeId] DEFAULT ((0)),
[CheckNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[PaymentReference] [int] NOT NULL IDENTITY(1, 1),
[IsDeposited] [bit] NOT NULL CONSTRAINT [DF_adLeadPayments_IsDeposited] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
--========================================================================================== 
-- TRIGGER adLeadPayments_Audit_Delete 
-- DELETE  add Audit History when delete records in adLeadPayments
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadPayments_Audit_Delete] ON [dbo].[adLeadPayments]
    FOR DELETE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Deleted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Deleted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Deleted
                        );
 
        IF @EventRows > 0
            BEGIN
                EXEC fmAuditHistAdd @AuditHistId,'adLeadPayments','D',@EventRows,@EventDate,@UserName;
                -- PaymentTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'PaymentTypeId'
                               ,CONVERT(VARCHAR(MAX),Old.PaymentTypeId)
                        FROM    Deleted AS Old;
                -- CheckNumber 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'CheckNumber'
                               ,CONVERT(VARCHAR(MAX),Old.CheckNumber)
                        FROM    Deleted AS Old;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),Old.ModUser)
                        FROM    Deleted AS Old;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                        FROM    Deleted AS Old;
                -- PaymentReference 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'PaymentReference'
                               ,CONVERT(VARCHAR(MAX),Old.PaymentReference)
                        FROM    Deleted AS Old;
                -- IsDeposited 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TransactionId
                               ,'IsDeposited'
                               ,CONVERT(VARCHAR(MAX),Old.IsDeposited)
                        FROM    Deleted AS Old;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadPayments_Audit_Delete 
--========================================================================================== 
 

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
--========================================================================================== 
-- TRIGGER adLeadPayments_Audit_Insert 
-- INSERT  add Audit History when insert records in adLeadPayments
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadPayments_Audit_Insert] ON [dbo].[adLeadPayments]
    FOR INSERT
AS
    BEGIN 
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadPayments','I',@EventRows,@EventDate,@UserName;
                -- PaymentTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'PaymentTypeId'
                               ,CONVERT(VARCHAR(MAX),New.PaymentTypeId)
                        FROM    Inserted AS New;
                -- CheckNumber 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'CheckNumber'
                               ,CONVERT(VARCHAR(MAX),New.CheckNumber)
                        FROM    Inserted AS New;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),New.ModUser)
                        FROM    Inserted AS New;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                        FROM    Inserted AS New;
                -- PaymentReference 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'PaymentReference'
                               ,CONVERT(VARCHAR(MAX),New.PaymentReference)
                        FROM    Inserted AS New;
                -- IsDeposited 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TransactionId
                               ,'IsDeposited'
                               ,CONVERT(VARCHAR(MAX),New.IsDeposited)
                        FROM    Inserted AS New;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadPayments_Audit_Insert 
--========================================================================================== 
 

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--========================================================================================== 
-- TRIGGER adLeadPayments_Audit_Update 
-- UPDATE  add Audit History when update fields in adLeadPayments
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadPayments_Audit_Update] ON [dbo].[adLeadPayments]
    FOR UPDATE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadPayments','U',@EventRows,@EventDate,@UserName;
                -- PaymentTypeId 
                IF UPDATE(PaymentTypeId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'PaymentTypeId'
                                       ,CONVERT(VARCHAR(MAX),Old.PaymentTypeId)
                                       ,CONVERT(VARCHAR(MAX),New.PaymentTypeId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.PaymentTypeId <> New.PaymentTypeId
                                        OR (
                                             Old.PaymentTypeId IS NULL
                                             AND New.PaymentTypeId IS NOT NULL
                                           )
                                        OR (
                                             New.PaymentTypeId IS NULL
                                             AND Old.PaymentTypeId IS NOT NULL
                                           );
                    END;
                -- CheckNumber 
                IF UPDATE(CheckNumber)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'CheckNumber'
                                       ,CONVERT(VARCHAR(MAX),Old.CheckNumber)
                                       ,CONVERT(VARCHAR(MAX),New.CheckNumber)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.CheckNumber <> New.CheckNumber
                                        OR (
                                             Old.CheckNumber IS NULL
                                             AND New.CheckNumber IS NOT NULL
                                           )
                                        OR (
                                             New.CheckNumber IS NULL
                                             AND Old.CheckNumber IS NOT NULL
                                           );
                    END;
                -- ModUser 
                IF UPDATE(ModUser)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'ModUser'
                                       ,CONVERT(VARCHAR(MAX),Old.ModUser)
                                       ,CONVERT(VARCHAR(MAX),New.ModUser)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.ModUser <> New.ModUser
                                        OR (
                                             Old.ModUser IS NULL
                                             AND New.ModUser IS NOT NULL
                                           )
                                        OR (
                                             New.ModUser IS NULL
                                             AND Old.ModUser IS NOT NULL
                                           );
                    END;
                -- ModDate 
                IF UPDATE(ModDate)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'ModDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.ModDate <> New.ModDate
                                        OR (
                                             Old.ModDate IS NULL
                                             AND New.ModDate IS NOT NULL
                                           )
                                        OR (
                                             New.ModDate IS NULL
                                             AND Old.ModDate IS NOT NULL
                                           );
                    END;
                -- PaymentReference 
                IF UPDATE(PaymentReference)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'PaymentReference'
                                       ,CONVERT(VARCHAR(MAX),Old.PaymentReference)
                                       ,CONVERT(VARCHAR(MAX),New.PaymentReference)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.PaymentReference <> New.PaymentReference
                                        OR (
                                             Old.PaymentReference IS NULL
                                             AND New.PaymentReference IS NOT NULL
                                           )
                                        OR (
                                             New.PaymentReference IS NULL
                                             AND Old.PaymentReference IS NOT NULL
                                           );
                    END;
                -- IsDeposited 
                IF UPDATE(IsDeposited)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.TransactionId
                                       ,'IsDeposited'
                                       ,CONVERT(VARCHAR(MAX),Old.IsDeposited)
                                       ,CONVERT(VARCHAR(MAX),New.IsDeposited)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.TransactionId = New.TransactionId
                                WHERE   Old.IsDeposited <> New.IsDeposited
                                        OR (
                                             Old.IsDeposited IS NULL
                                             AND New.IsDeposited IS NOT NULL
                                           )
                                        OR (
                                             New.IsDeposited IS NULL
                                             AND Old.IsDeposited IS NOT NULL
                                           );
                    END;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadPayments_Audit_Update 
--========================================================================================== 
 

GO
ALTER TABLE [dbo].[adLeadPayments] ADD CONSTRAINT [PK_adLeadPayments_TransactionId] PRIMARY KEY CLUSTERED  ([TransactionId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adLeadPayments] ADD CONSTRAINT [FK_adLeadPayments_adLeadTransactions_TransactionId_TransactionId] FOREIGN KEY ([TransactionId]) REFERENCES [dbo].[adLeadTransactions] ([TransactionId])
GO
