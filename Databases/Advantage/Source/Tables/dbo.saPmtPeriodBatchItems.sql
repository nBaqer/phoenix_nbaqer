CREATE TABLE [dbo].[saPmtPeriodBatchItems]
(
[PmtPeriodBatchItemId] [int] NOT NULL IDENTITY(1, 1),
[PmtPeriodBatchHeaderId] [int] NOT NULL,
[StuEnrollId] [uniqueidentifier] NOT NULL,
[PmtPeriodId] [uniqueidentifier] NOT NULL,
[CreditsHoursValue] [decimal] (18, 0) NULL,
[ChargeAmount] [decimal] (18, 0) NOT NULL,
[TransactionReference] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TransactionDescription] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[IncludedInPost] [bit] NOT NULL CONSTRAINT [DF_saPmtPeriodBatchItems_IncludedInPost] DEFAULT ((1)),
[TransactionId] [uniqueidentifier] NULL,
[TermId] [uniqueidentifier] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime2] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saPmtPeriodBatchItems] ADD CONSTRAINT [PK_saPmtPeriodBatchItems_PmtPeriodBatchItemId] PRIMARY KEY CLUSTERED  ([PmtPeriodBatchItemId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saPmtPeriodBatchItems] ADD CONSTRAINT [FK_saPmtPeriodBatchItems_saPmtPeriodBatchHeaders_PmtPeriodBatchHeaderId_PmtPeriodBatchHeaderId] FOREIGN KEY ([PmtPeriodBatchHeaderId]) REFERENCES [dbo].[saPmtPeriodBatchHeaders] ([PmtPeriodBatchHeaderId])
GO
