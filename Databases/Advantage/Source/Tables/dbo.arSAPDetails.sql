CREATE TABLE [dbo].[arSAPDetails]
(
[SAPDetailId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arSAPDetails_SAPDetailId] DEFAULT (newid()),
[QualMinValue] [decimal] (19, 2) NULL,
[QuantMinValue] [decimal] (18, 2) NULL,
[QualMinTypId] [tinyint] NULL,
[QuantMinUnitTypId] [tinyint] NOT NULL,
[TrigValue] [int] NOT NULL,
[TrigUnitTypId] [tinyint] NOT NULL,
[TrigOffsetTypId] [tinyint] NOT NULL,
[TrigOffsetSeq] [tinyint] NULL,
[ConsequenceTypId] [tinyint] NOT NULL,
[MinCredsCompltd] [decimal] (18, 2) NOT NULL,
[SAPId] [uniqueidentifier] NULL,
[MinAttendanceValue] [decimal] (19, 2) NOT NULL CONSTRAINT [DF_arSAPDetails_MinAttendanceValue] DEFAULT ((0.0)),
[accuracy] [decimal] (18, 2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arSAPDetails] ADD CONSTRAINT [PK_arSAPDetails_SAPDetailId] PRIMARY KEY CLUSTERED  ([SAPDetailId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arSAPDetails] ADD CONSTRAINT [FK_arSAPDetails_arSAP_SAPId_SAPId] FOREIGN KEY ([SAPId]) REFERENCES [dbo].[arSAP] ([SAPId])
GO
ALTER TABLE [dbo].[arSAPDetails] ADD CONSTRAINT [FK_arSAPDetails_arTrigOffsetTyps_TrigOffsetTypId_TrigOffsetTypId] FOREIGN KEY ([TrigOffsetTypId]) REFERENCES [dbo].[arTrigOffsetTyps] ([TrigOffsetTypId])
GO
ALTER TABLE [dbo].[arSAPDetails] ADD CONSTRAINT [FK_arSAPDetails_arTrigUnitTyps_TrigUnitTypId_TrigUnitTypId] FOREIGN KEY ([TrigUnitTypId]) REFERENCES [dbo].[arTrigUnitTyps] ([TrigUnitTypId])
GO
