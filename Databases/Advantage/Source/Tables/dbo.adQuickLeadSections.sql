CREATE TABLE [dbo].[adQuickLeadSections]
(
[SectionId] [int] NOT NULL,
[SectionName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adQuickLeadSections] ADD CONSTRAINT [PK_adQuickLeadSections_SectionId] PRIMARY KEY CLUSTERED  ([SectionId]) ON [PRIMARY]
GO
