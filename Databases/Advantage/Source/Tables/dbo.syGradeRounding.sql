CREATE TABLE [dbo].[syGradeRounding]
(
[graderoundingvalue] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_syGradeRounding_graderoundingvalue] DEFAULT ('no'),
[GradeRoundingId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_syGradeRounding_GradeRoundingId] DEFAULT (newsequentialid())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syGradeRounding] ADD CONSTRAINT [PK_syGradeRounding_GradeRoundingId] PRIMARY KEY CLUSTERED  ([GradeRoundingId]) ON [PRIMARY]
GO
