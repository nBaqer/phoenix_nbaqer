CREATE TABLE [dbo].[hrEmpDegrees]
(
[EmpDegreeId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_hrEmpDegrees_EmpDegreeId] DEFAULT (newid()),
[EmpId] [uniqueidentifier] NOT NULL,
[DegreeId] [uniqueidentifier] NOT NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[hrEmpDegrees] ADD CONSTRAINT [PK_hrEmpDegrees_EmpDegreeId] PRIMARY KEY CLUSTERED  ([EmpDegreeId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[hrEmpDegrees] ADD CONSTRAINT [FK_hrEmpDegrees_arDegrees_DegreeId_DegreeId] FOREIGN KEY ([DegreeId]) REFERENCES [dbo].[arDegrees] ([DegreeId])
GO
ALTER TABLE [dbo].[hrEmpDegrees] ADD CONSTRAINT [FK_hrEmpDegrees_hrEmployees_EmpId_EmpId] FOREIGN KEY ([EmpId]) REFERENCES [dbo].[hrEmployees] ([EmpId])
GO
