CREATE TABLE [dbo].[adExtraCurr]
(
[ExtraCurrId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_adExtraCurr_ExtraCurrId] DEFAULT (newid()),
[ExtraCurrCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[ExtraCurrDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[ExtraCurricularGrpID] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[adExtraCurr_Audit_Delete] ON [dbo].[adExtraCurr]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adExtraCurr','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ExtraCurrId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ExtraCurrId
                               ,'ExtraCurricularGrpID'
                               ,CONVERT(VARCHAR(8000),Old.ExtraCurricularGrpID,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ExtraCurrId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ExtraCurrId
                               ,'ExtraCurrDescrip'
                               ,CONVERT(VARCHAR(8000),Old.ExtraCurrDescrip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ExtraCurrId
                               ,'ExtraCurrCode'
                               ,CONVERT(VARCHAR(8000),Old.ExtraCurrCode,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[adExtraCurr_Audit_Insert] ON [dbo].[adExtraCurr]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adExtraCurr','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ExtraCurrId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ExtraCurrId
                               ,'ExtraCurricularGrpID'
                               ,CONVERT(VARCHAR(8000),New.ExtraCurricularGrpID,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ExtraCurrId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ExtraCurrId
                               ,'ExtraCurrDescrip'
                               ,CONVERT(VARCHAR(8000),New.ExtraCurrDescrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ExtraCurrId
                               ,'ExtraCurrCode'
                               ,CONVERT(VARCHAR(8000),New.ExtraCurrCode,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[adExtraCurr_Audit_Update] ON [dbo].[adExtraCurr]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adExtraCurr','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ExtraCurrId
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ExtraCurrId = New.ExtraCurrId
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 
                IF UPDATE(ExtraCurricularGrpID)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ExtraCurrId
                                   ,'ExtraCurricularGrpID'
                                   ,CONVERT(VARCHAR(8000),Old.ExtraCurricularGrpID,121)
                                   ,CONVERT(VARCHAR(8000),New.ExtraCurricularGrpID,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ExtraCurrId = New.ExtraCurrId
                            WHERE   Old.ExtraCurricularGrpID <> New.ExtraCurricularGrpID
                                    OR (
                                         Old.ExtraCurricularGrpID IS NULL
                                         AND New.ExtraCurricularGrpID IS NOT NULL
                                       )
                                    OR (
                                         New.ExtraCurricularGrpID IS NULL
                                         AND Old.ExtraCurricularGrpID IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ExtraCurrId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ExtraCurrId = New.ExtraCurrId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(ExtraCurrDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ExtraCurrId
                                   ,'ExtraCurrDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.ExtraCurrDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.ExtraCurrDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ExtraCurrId = New.ExtraCurrId
                            WHERE   Old.ExtraCurrDescrip <> New.ExtraCurrDescrip
                                    OR (
                                         Old.ExtraCurrDescrip IS NULL
                                         AND New.ExtraCurrDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.ExtraCurrDescrip IS NULL
                                         AND Old.ExtraCurrDescrip IS NOT NULL
                                       ); 
                IF UPDATE(ExtraCurrCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ExtraCurrId
                                   ,'ExtraCurrCode'
                                   ,CONVERT(VARCHAR(8000),Old.ExtraCurrCode,121)
                                   ,CONVERT(VARCHAR(8000),New.ExtraCurrCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ExtraCurrId = New.ExtraCurrId
                            WHERE   Old.ExtraCurrCode <> New.ExtraCurrCode
                                    OR (
                                         Old.ExtraCurrCode IS NULL
                                         AND New.ExtraCurrCode IS NOT NULL
                                       )
                                    OR (
                                         New.ExtraCurrCode IS NULL
                                         AND Old.ExtraCurrCode IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO
GO
ALTER TABLE [dbo].[adExtraCurr] ADD CONSTRAINT [PK_adExtraCurr_ExtraCurrId] PRIMARY KEY CLUSTERED  ([ExtraCurrId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_adExtraCurr_ExtraCurrCode_ExtraCurrDescrip_ExtraCurricularGrpID] ON [dbo].[adExtraCurr] ([ExtraCurrCode], [ExtraCurrDescrip], [ExtraCurricularGrpID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adExtraCurr] ADD CONSTRAINT [FK_adExtraCurr_adExtraCurrGrp_ExtraCurricularGrpID_ExtraCurrGrpId] FOREIGN KEY ([ExtraCurricularGrpID]) REFERENCES [dbo].[adExtraCurrGrp] ([ExtraCurrGrpId])
GO
ALTER TABLE [dbo].[adExtraCurr] ADD CONSTRAINT [FK_adExtraCurr_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[adExtraCurr] ADD CONSTRAINT [FK_adExtraCurr_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
