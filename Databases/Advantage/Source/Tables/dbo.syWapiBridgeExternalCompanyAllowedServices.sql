CREATE TABLE [dbo].[syWapiBridgeExternalCompanyAllowedServices]
(
[IdWapiBridgeAllowedService] [int] NOT NULL IDENTITY(1, 1),
[IdExternalCompanies] [int] NOT NULL,
[IdAllowedServices] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syWapiBridgeExternalCompanyAllowedServices] ADD CONSTRAINT [PK_syWapiBridgeExternalCompanyAllowedServices_IdWapiBridgeAllowedService] PRIMARY KEY CLUSTERED  ([IdWapiBridgeAllowedService]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syWapiBridgeExternalCompanyAllowedServices] ADD CONSTRAINT [FK_syWapiBridgeExternalCompanyAllowedServices_syWapiAllowedServices_IdAllowedServices_Id] FOREIGN KEY ([IdAllowedServices]) REFERENCES [dbo].[syWapiAllowedServices] ([Id])
GO
ALTER TABLE [dbo].[syWapiBridgeExternalCompanyAllowedServices] ADD CONSTRAINT [FK_syWapiBridgeExternalCompanyAllowedServices_syWapiExternalCompanies_IdExternalCompanies_Id] FOREIGN KEY ([IdExternalCompanies]) REFERENCES [dbo].[syWapiExternalCompanies] ([Id])
GO
EXEC sp_addextendedproperty N'MS_Description', 'This table brake the many to many relationship between external companies and Allowed services. 
The table related the companies with they authorized services.', 'SCHEMA', N'dbo', 'TABLE', N'syWapiBridgeExternalCompanyAllowedServices', NULL, NULL
GO
