CREATE TABLE [dbo].[syCreditSummary]
(
[StuEnrollId] [uniqueidentifier] NULL,
[TermId] [uniqueidentifier] NULL,
[TermDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReqId] [uniqueidentifier] NULL,
[ReqDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ClsSectionId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreditsEarned] [decimal] (18, 2) NULL,
[CreditsAttempted] [decimal] (18, 2) NULL,
[CurrentScore] [decimal] (18, 2) NULL,
[CurrentGrade] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FinalScore] [decimal] (18, 2) NULL,
[FinalGrade] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Completed] [bit] NULL,
[FinalGPA] [decimal] (18, 2) NULL,
[Product_WeightedAverage_Credits_GPA] [decimal] (18, 2) NULL,
[Count_WeightedAverage_Credits] [decimal] (18, 2) NULL,
[Product_SimpleAverage_Credits_GPA] [decimal] (18, 2) NULL,
[Count_SimpleAverage_Credits] [decimal] (18, 2) NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[TermGPA_Simple] [decimal] (18, 2) NULL,
[TermGPA_Weighted] [decimal] (18, 2) NULL,
[coursecredits] [decimal] (18, 2) NULL,
[CumulativeGPA] [decimal] (18, 2) NULL,
[CumulativeGPA_Simple] [decimal] (18, 2) NULL,
[FACreditsEarned] [decimal] (18, 2) NULL,
[Average] [decimal] (18, 2) NULL,
[CumAverage] [decimal] (18, 2) NULL,
[TermStartDate] [datetime] NULL,
[Id] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syCreditSummary] ADD CONSTRAINT [PK_syCreditSummary] PRIMARY KEY NONCLUSTERED  ([Id]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_syCreditSummary_ReqId_StuEnrollId_TermIdC8C7C13C9C10C11C12__INC_C15C16C17C18] ON [dbo].[syCreditSummary] ([ReqId], [StuEnrollId], [TermId], [CreditsAttempted], [CreditsEarned], [Completed], [CurrentScore], [CurrentGrade], [FinalScore], [FinalGrade]) INCLUDE ([Count_SimpleAverage_Credits], [Count_WeightedAverage_Credits], [Product_SimpleAverage_Credits_GPA], [Product_WeightedAverage_Credits_GPA]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_syCreditSummary_StuEnrollID_INC_C2C17C18] ON [dbo].[syCreditSummary] ([StuEnrollId]) INCLUDE ([Count_SimpleAverage_Credits], [Product_SimpleAverage_Credits_GPA], [TermId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_syCreditSummary_StuEnrollId_TermId_INC_C15C16] ON [dbo].[syCreditSummary] ([StuEnrollId], [TermId]) INCLUDE ([Count_WeightedAverage_Credits], [Product_WeightedAverage_Credits_GPA]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_syCreditSummary_StuEnrollId_TermId_ReqId_INC_C15C16] ON [dbo].[syCreditSummary] ([StuEnrollId], [TermId], [ReqId]) INCLUDE ([Count_WeightedAverage_Credits], [Product_WeightedAverage_Credits_GPA]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_syCreditSummary_C1_C2_C4_C6_C7_C8_C9_C10_C11_C12_C13] ON [dbo].[syCreditSummary] ([StuEnrollId], [TermId], [ReqId], [ClsSectionId], [CreditsAttempted], [CreditsEarned], [Completed], [CurrentScore], [CurrentGrade], [FinalScore], [FinalGrade]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_syCreditSummary_StuEnrollId_TermId_ReqId_C8C7C13C9C10C11C12] ON [dbo].[syCreditSummary] ([StuEnrollId], [TermId], [ReqId], [CreditsAttempted], [CreditsEarned], [Completed], [CurrentScore], [CurrentGrade], [FinalScore], [FinalGrade]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_syCreditSummary_C1_C2_C7_C8_C15_C16_C17_C18_C21_C22_C26_C27] ON [dbo].[syCreditSummary] ([StuEnrollId], [TermId], [TermGPA_Simple], [TermGPA_Weighted]) INCLUDE ([Average], [Count_SimpleAverage_Credits], [Count_WeightedAverage_Credits], [CreditsAttempted], [CreditsEarned], [FACreditsEarned], [Product_SimpleAverage_Credits_GPA], [Product_WeightedAverage_Credits_GPA]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_syCreditSummary_StuEnrollId_TermStartDate_TermId_Completed_coursecredits_CreditsEarned_FACreditsEarned_FinalGPA_FinalScore] ON [dbo].[syCreditSummary] ([StuEnrollId], [TermStartDate], [TermId]) INCLUDE ([Completed], [coursecredits], [CreditsEarned], [FACreditsEarned], [FinalGPA], [FinalScore]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_syCreditSummary_TermID_StuEnrollId_ReqId_C8C7C13C9C10C11C12_INC_C15C16C17C18] ON [dbo].[syCreditSummary] ([TermId], [StuEnrollId], [ReqId], [CreditsAttempted], [CreditsEarned], [Completed], [CurrentScore], [CurrentGrade], [FinalScore], [FinalGrade]) INCLUDE ([Count_SimpleAverage_Credits], [Count_WeightedAverage_Credits], [Product_SimpleAverage_Credits_GPA], [Product_WeightedAverage_Credits_GPA]) ON [PRIMARY]
GO
CREATE CLUSTERED INDEX [IX_syCreditSummary_TermStartDate] ON [dbo].[syCreditSummary] ([TermStartDate]) ON [PRIMARY]
GO
