CREATE TABLE [dbo].[syAdvBuild]
(
[BuildNumber] [varchar] (120) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[AdvantageBuildId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_syAdvBuild_AdvantageBuildId] DEFAULT (newsequentialid())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syAdvBuild] ADD CONSTRAINT [PK_syAdvBuild_AdvantageBuildId] PRIMARY KEY CLUSTERED  ([AdvantageBuildId]) ON [PRIMARY]
GO
