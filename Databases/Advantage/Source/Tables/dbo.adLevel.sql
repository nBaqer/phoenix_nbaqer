CREATE TABLE [dbo].[adLevel]
(
[LevelId] [int] NOT NULL,
[Code] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_adLevel_StatusId] DEFAULT ('F23DE1E2-D90A-4720-B4C7-0F6FB09C9965')
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adLevel] ADD CONSTRAINT [PK_adLevel_LevelId] PRIMARY KEY CLUSTERED  ([LevelId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adLevel] ADD CONSTRAINT [FK_adLevel_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
