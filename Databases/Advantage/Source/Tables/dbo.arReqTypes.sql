CREATE TABLE [dbo].[arReqTypes]
(
[ReqTypeId] [smallint] NOT NULL,
[Code] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Descrip] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[IsGroup] [bit] NULL,
[ChildType] [tinyint] NULL,
[TrkGrade] [bit] NULL,
[TrkHours] [bit] NULL,
[TrkCount] [bit] NULL,
[CampGrpId] [uniqueidentifier] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arReqTypes] ADD CONSTRAINT [PK_arReqTypes_ReqTypeId] PRIMARY KEY CLUSTERED  ([ReqTypeId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arReqTypes] ADD CONSTRAINT [FK_arReqTypes_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[arReqTypes] ADD CONSTRAINT [FK_arReqTypes_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
