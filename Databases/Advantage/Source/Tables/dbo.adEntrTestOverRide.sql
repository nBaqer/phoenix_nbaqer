CREATE TABLE [dbo].[adEntrTestOverRide]
(
[EntrTestOverRideId] [uniqueidentifier] NOT NULL,
[LeadId] [uniqueidentifier] NULL,
[EntrTestId] [uniqueidentifier] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[OverRide] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StudentId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
 
--========================================================================================== 
-- TRIGGER adEntrTestOverRide_Audit_Delete 
-- INSERT  add Audit History when delete records in adEntrTestOverRide 
--========================================================================================== 
CREATE TRIGGER [dbo].[adEntrTestOverRide_Audit_Delete] ON [dbo].[adEntrTestOverRide]
    FOR DELETE
AS
    BEGIN 
        SET NOCOUNT ON; 
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
        DECLARE @EventRows AS INT; 
        DECLARE @EventDate AS DATETIME; 
        DECLARE @UserName AS VARCHAR(50); 
	
        SET @AuditHistId = NEWID(); 
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Deleted
                         ); 
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Deleted
                         ); 
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Deleted
                        ); 
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adEntrTestOverRide','D',@EventRows,@EventDate,@UserName; 
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EntrTestOverRideId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),Old.LeadId)
                        FROM    Deleted AS Old; 
                -- EntrTestId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EntrTestOverRideId
                               ,'EntrTestId'
                               ,CONVERT(VARCHAR(MAX),Old.EntrTestId)
                        FROM    Deleted AS Old; 
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EntrTestOverRideId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),Old.ModUser)
                        FROM    Deleted AS Old; 
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EntrTestOverRideId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                        FROM    Deleted AS Old; 
                -- OverRide 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EntrTestOverRideId
                               ,'OverRide'
                               ,CONVERT(VARCHAR(MAX),Old.OverRide)
                        FROM    Deleted AS Old; 
                -- StudentId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.EntrTestOverRideId
                               ,'StudentId'
                               ,CONVERT(VARCHAR(MAX),Old.StudentId)
                        FROM    Deleted AS Old; 
            END; 
        SET NOCOUNT OFF; 
    END; 
--========================================================================================== 
-- END TRIGGER adEntrTestOverRide_Audit_Delete 
--========================================================================================== 
 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
 
--========================================================================================== 
-- TRIGGER adEntrTestOverRide_Audit_Insert 
-- INSERT  add Audit History when insert records in adEntrTestOverRide 
--========================================================================================== 
CREATE TRIGGER [dbo].[adEntrTestOverRide_Audit_Insert] ON [dbo].[adEntrTestOverRide]
    FOR INSERT
AS
    BEGIN 
        SET NOCOUNT ON; 
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
        DECLARE @EventRows AS INT; 
        DECLARE @EventDate AS DATETIME; 
        DECLARE @UserName AS VARCHAR(50); 
	
        SET @AuditHistId = NEWID(); 
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         ); 
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         ); 
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        ); 
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adEntrTestOverRide','I',@EventRows,@EventDate,@UserName; 
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EntrTestOverRideId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),New.LeadId)
                        FROM    Inserted AS New; 
                -- EntrTestId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EntrTestOverRideId
                               ,'EntrTestId'
                               ,CONVERT(VARCHAR(MAX),New.EntrTestId)
                        FROM    Inserted AS New; 
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EntrTestOverRideId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),New.ModUser)
                        FROM    Inserted AS New; 
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EntrTestOverRideId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                        FROM    Inserted AS New; 
                -- OverRide 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EntrTestOverRideId
                               ,'OverRide'
                               ,CONVERT(VARCHAR(MAX),New.OverRide)
                        FROM    Inserted AS New; 
                -- StudentId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.EntrTestOverRideId
                               ,'StudentId'
                               ,CONVERT(VARCHAR(MAX),New.StudentId)
                        FROM    Inserted AS New; 
            END; 
        SET NOCOUNT OFF; 
    END; 
--========================================================================================== 
-- END TRIGGER adEntrTestOverRide_Audit_Insert 
--========================================================================================== 
 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
--========================================================================================== 
-- TRIGGER adEntrTestOverRide_Audit_Update 
-- UPDATE  add Audit History when update fields in adEntrTestOverRide 
--========================================================================================== 
CREATE TRIGGER [dbo].[adEntrTestOverRide_Audit_Update] ON [dbo].[adEntrTestOverRide]
    FOR UPDATE
AS
    BEGIN 
        SET NOCOUNT ON; 
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
        DECLARE @EventRows AS INT; 
        DECLARE @EventDate AS DATETIME; 
        DECLARE @UserName AS VARCHAR(50); 
	
        SET @AuditHistId = NEWID(); 
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         ); 
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         ); 
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        ); 
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adEntrTestOverRide','U',@EventRows,@EventDate,@UserName; 
                -- LeadId 
                IF UPDATE(LeadId)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.EntrTestOverRideId
                                       ,'LeadId'
                                       ,CONVERT(VARCHAR(MAX),Old.LeadId)
                                       ,CONVERT(VARCHAR(MAX),New.LeadId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.EntrTestOverRideId = New.EntrTestOverRideId
                                WHERE   Old.LeadId <> New.LeadId
                                        OR (
                                             Old.LeadId IS NULL
                                             AND New.LeadId IS NOT NULL
                                           )
                                        OR (
                                             New.LeadId IS NULL
                                             AND Old.LeadId IS NOT NULL
                                           ); 
                    END; 
                -- EntrTestId 
                IF UPDATE(EntrTestId)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.EntrTestOverRideId
                                       ,'EntrTestId'
                                       ,CONVERT(VARCHAR(MAX),Old.EntrTestId)
                                       ,CONVERT(VARCHAR(MAX),New.EntrTestId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.EntrTestOverRideId = New.EntrTestOverRideId
                                WHERE   Old.EntrTestId <> New.EntrTestId
                                        OR (
                                             Old.EntrTestId IS NULL
                                             AND New.EntrTestId IS NOT NULL
                                           )
                                        OR (
                                             New.EntrTestId IS NULL
                                             AND Old.EntrTestId IS NOT NULL
                                           ); 
                    END; 
                -- ModUser 
                IF UPDATE(ModUser)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.EntrTestOverRideId
                                       ,'ModUser'
                                       ,CONVERT(VARCHAR(MAX),Old.ModUser)
                                       ,CONVERT(VARCHAR(MAX),New.ModUser)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.EntrTestOverRideId = New.EntrTestOverRideId
                                WHERE   Old.ModUser <> New.ModUser
                                        OR (
                                             Old.ModUser IS NULL
                                             AND New.ModUser IS NOT NULL
                                           )
                                        OR (
                                             New.ModUser IS NULL
                                             AND Old.ModUser IS NOT NULL
                                           ); 
                    END; 
                -- ModDate 
                IF UPDATE(ModDate)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.EntrTestOverRideId
                                       ,'ModDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.EntrTestOverRideId = New.EntrTestOverRideId
                                WHERE   Old.ModDate <> New.ModDate
                                        OR (
                                             Old.ModDate IS NULL
                                             AND New.ModDate IS NOT NULL
                                           )
                                        OR (
                                             New.ModDate IS NULL
                                             AND Old.ModDate IS NOT NULL
                                           ); 
                    END; 
                -- OverRide 
                IF UPDATE(OverRide)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.EntrTestOverRideId
                                       ,'OverRide'
                                       ,CONVERT(VARCHAR(MAX),Old.OverRide)
                                       ,CONVERT(VARCHAR(MAX),New.OverRide)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.EntrTestOverRideId = New.EntrTestOverRideId
                                WHERE   Old.OverRide <> New.OverRide
                                        OR (
                                             Old.OverRide IS NULL
                                             AND New.OverRide IS NOT NULL
                                           )
                                        OR (
                                             New.OverRide IS NULL
                                             AND Old.OverRide IS NOT NULL
                                           ); 
                    END; 
                -- StudentId 
                IF UPDATE(StudentId)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.EntrTestOverRideId
                                       ,'StudentId'
                                       ,CONVERT(VARCHAR(MAX),Old.StudentId)
                                       ,CONVERT(VARCHAR(MAX),New.StudentId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.EntrTestOverRideId = New.EntrTestOverRideId
                                WHERE   Old.StudentId <> New.StudentId
                                        OR (
                                             Old.StudentId IS NULL
                                             AND New.StudentId IS NOT NULL
                                           )
                                        OR (
                                             New.StudentId IS NULL
                                             AND Old.StudentId IS NOT NULL
                                           ); 
                    END; 
            END; 
        SET NOCOUNT OFF; 
    END; 
--========================================================================================== 
-- END TRIGGER adEntrTestOverRide_Audit_Update 
--========================================================================================== 
 
GO
ALTER TABLE [dbo].[adEntrTestOverRide] ADD CONSTRAINT [PK_adEntrTestOverRide_EntrTestOverRideId] PRIMARY KEY CLUSTERED  ([EntrTestOverRideId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_adEntrTestOverRide_EntrTestId_StudentId_OverRide] ON [dbo].[adEntrTestOverRide] ([EntrTestId], [StudentId], [OverRide]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_adEntrTestOverRide_LeadId] ON [dbo].[adEntrTestOverRide] ([LeadId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adEntrTestOverRide] ADD CONSTRAINT [FK_adEntrTestOverRide_adLeads_LeadId_LeadId] FOREIGN KEY ([LeadId]) REFERENCES [dbo].[adLeads] ([LeadId])
GO
ALTER TABLE [dbo].[adEntrTestOverRide] ADD CONSTRAINT [FK_adEntrTestOverRide_adReqs_EntrTestId_adReqId] FOREIGN KEY ([EntrTestId]) REFERENCES [dbo].[adReqs] ([adReqId])
GO
