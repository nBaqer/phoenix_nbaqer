CREATE TABLE [dbo].[arTerm]
(
[TermId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arTerm_TermId] DEFAULT (newid()),
[TermCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[TermDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[ShiftId] [uniqueidentifier] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[IsModule] [bit] NULL CONSTRAINT [DF_arTerm_IsModule] DEFAULT ((0)),
[TermTypeId] [int] NULL,
[ProgId] [uniqueidentifier] NULL,
[MidPtDate] [datetime] NULL,
[MaxGradDate] [datetime] NULL,
[ProgramVersionId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[arTerm_Audit_Delete] ON [dbo].[arTerm]
    FOR DELETE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arTerm','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TermId
                               ,'TermCode'
                               ,CONVERT(VARCHAR(8000),Old.TermCode,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TermId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TermId
                               ,'TermDescrip'
                               ,CONVERT(VARCHAR(8000),Old.TermDescrip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TermId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TermId
                               ,'EndDate'
                               ,CONVERT(VARCHAR(8000),Old.EndDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TermId
                               ,'ShiftId'
                               ,CONVERT(VARCHAR(8000),Old.ShiftId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TermId
                               ,'StartDate'
                               ,CONVERT(VARCHAR(8000),Old.StartDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TermId
                               ,'TermTypeId'
                               ,CONVERT(VARCHAR(8000),Old.TermTypeId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TermId
                               ,'ProgId'
                               ,CONVERT(VARCHAR(8000),Old.ProgId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TermId
                               ,'MidPtDate'
                               ,CONVERT(VARCHAR(8000),Old.MidPtDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.TermId
                               ,'MaxGradDate'
                               ,CONVERT(VARCHAR(8000),Old.MaxGradDate,121)
                        FROM    Deleted Old; 
            END; 
        END; 




    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[arTerm_Audit_Insert] ON [dbo].[arTerm]
    FOR INSERT
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arTerm','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TermId
                               ,'TermCode'
                               ,CONVERT(VARCHAR(8000),New.TermCode,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TermId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TermId
                               ,'TermDescrip'
                               ,CONVERT(VARCHAR(8000),New.TermDescrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TermId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TermId
                               ,'EndDate'
                               ,CONVERT(VARCHAR(8000),New.EndDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TermId
                               ,'ShiftId'
                               ,CONVERT(VARCHAR(8000),New.ShiftId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TermId
                               ,'StartDate'
                               ,CONVERT(VARCHAR(8000),New.StartDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TermId
                               ,'TermTypeId'
                               ,CONVERT(VARCHAR(8000),New.TermTypeId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TermId
                               ,'ProgId'
                               ,CONVERT(VARCHAR(8000),New.ProgId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TermId
                               ,'MidPtDate'
                               ,CONVERT(VARCHAR(8000),New.MidPtDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.TermId
                               ,'MaxGradDate'
                               ,CONVERT(VARCHAR(8000),New.MaxGradDate,121)
                        FROM    Inserted New; 
            END; 
        END; 



    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[arTerm_Audit_Update] ON [dbo].[arTerm]
    FOR UPDATE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arTerm','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(TermCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TermId
                                   ,'TermCode'
                                   ,CONVERT(VARCHAR(8000),Old.TermCode,121)
                                   ,CONVERT(VARCHAR(8000),New.TermCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TermId = New.TermId
                            WHERE   Old.TermCode <> New.TermCode
                                    OR (
                                         Old.TermCode IS NULL
                                         AND New.TermCode IS NOT NULL
                                       )
                                    OR (
                                         New.TermCode IS NULL
                                         AND Old.TermCode IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TermId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TermId = New.TermId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(TermDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TermId
                                   ,'TermDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.TermDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.TermDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TermId = New.TermId
                            WHERE   Old.TermDescrip <> New.TermDescrip
                                    OR (
                                         Old.TermDescrip IS NULL
                                         AND New.TermDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.TermDescrip IS NULL
                                         AND Old.TermDescrip IS NOT NULL
                                       ); 
                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TermId
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TermId = New.TermId
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 
                IF UPDATE(EndDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TermId
                                   ,'EndDate'
                                   ,CONVERT(VARCHAR(8000),Old.EndDate,121)
                                   ,CONVERT(VARCHAR(8000),New.EndDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TermId = New.TermId
                            WHERE   Old.EndDate <> New.EndDate
                                    OR (
                                         Old.EndDate IS NULL
                                         AND New.EndDate IS NOT NULL
                                       )
                                    OR (
                                         New.EndDate IS NULL
                                         AND Old.EndDate IS NOT NULL
                                       ); 
                IF UPDATE(ShiftId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TermId
                                   ,'ShiftId'
                                   ,CONVERT(VARCHAR(8000),Old.ShiftId,121)
                                   ,CONVERT(VARCHAR(8000),New.ShiftId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TermId = New.TermId
                            WHERE   Old.ShiftId <> New.ShiftId
                                    OR (
                                         Old.ShiftId IS NULL
                                         AND New.ShiftId IS NOT NULL
                                       )
                                    OR (
                                         New.ShiftId IS NULL
                                         AND Old.ShiftId IS NOT NULL
                                       ); 
                IF UPDATE(StartDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TermId
                                   ,'StartDate'
                                   ,CONVERT(VARCHAR(8000),Old.StartDate,121)
                                   ,CONVERT(VARCHAR(8000),New.StartDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TermId = New.TermId
                            WHERE   Old.StartDate <> New.StartDate
                                    OR (
                                         Old.StartDate IS NULL
                                         AND New.StartDate IS NOT NULL
                                       )
                                    OR (
                                         New.StartDate IS NULL
                                         AND Old.StartDate IS NOT NULL
                                       ); 
                IF UPDATE(TermTypeId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TermId
                                   ,'TermTypeId'
                                   ,CONVERT(VARCHAR(8000),Old.TermTypeId,121)
                                   ,CONVERT(VARCHAR(8000),New.TermTypeId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TermId = New.TermId
                            WHERE   Old.TermTypeId <> New.TermTypeId
                                    OR (
                                         Old.TermTypeId IS NULL
                                         AND New.TermTypeId IS NOT NULL
                                       )
                                    OR (
                                         New.TermTypeId IS NULL
                                         AND Old.TermTypeId IS NOT NULL
                                       ); 
                IF UPDATE(ProgId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TermId
                                   ,'ProgId'
                                   ,CONVERT(VARCHAR(8000),Old.ProgId,121)
                                   ,CONVERT(VARCHAR(8000),New.ProgId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TermId = New.TermId
                            WHERE   Old.ProgId <> New.ProgId
                                    OR (
                                         Old.ProgId IS NULL
                                         AND New.ProgId IS NOT NULL
                                       )
                                    OR (
                                         New.ProgId IS NULL
                                         AND Old.ProgId IS NOT NULL
                                       ); 
                IF UPDATE(MidPtDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TermId
                                   ,'MidPtDate'
                                   ,CONVERT(VARCHAR(8000),Old.MidPtDate,121)
                                   ,CONVERT(VARCHAR(8000),New.MidPtDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TermId = New.TermId
                            WHERE   Old.MidPtDate <> New.MidPtDate
                                    OR (
                                         Old.MidPtDate IS NULL
                                         AND New.MidPtDate IS NOT NULL
                                       )
                                    OR (
                                         New.MidPtDate IS NULL
                                         AND Old.MidPtDate IS NOT NULL
                                       ); 
                IF UPDATE(MaxGradDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.TermId
                                   ,'MaxGradDate'
                                   ,CONVERT(VARCHAR(8000),Old.MaxGradDate,121)
                                   ,CONVERT(VARCHAR(8000),New.MaxGradDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.TermId = New.TermId
                            WHERE   Old.MaxGradDate <> New.MaxGradDate
                                    OR (
                                         Old.MaxGradDate IS NULL
                                         AND New.MaxGradDate IS NOT NULL
                                       )
                                    OR (
                                         New.MaxGradDate IS NULL
                                         AND Old.MaxGradDate IS NOT NULL
                                       ); 
            END; 
        END; 



    SET NOCOUNT OFF;

GO
ALTER TABLE [dbo].[arTerm] ADD CONSTRAINT [PK_arTerm_TermId] PRIMARY KEY CLUSTERED  ([TermId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_arTerm_TermCode_TermDescrip_CampGrpId] ON [dbo].[arTerm] ([TermCode], [TermDescrip], [CampGrpId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_arTerm_TermId_StartDate_TermDescrip_EndDate] ON [dbo].[arTerm] ([TermId], [StartDate], [TermDescrip], [EndDate]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arTerm] ADD CONSTRAINT [FK_arTerm_arPrgVersions_ProgramVersionId_PrgVerId] FOREIGN KEY ([ProgramVersionId]) REFERENCES [dbo].[arPrgVersions] ([PrgVerId])
GO
ALTER TABLE [dbo].[arTerm] ADD CONSTRAINT [FK_arTerm_arPrograms_ProgId_ProgId] FOREIGN KEY ([ProgId]) REFERENCES [dbo].[arPrograms] ([ProgId])
GO
ALTER TABLE [dbo].[arTerm] ADD CONSTRAINT [FK_arTerm_arShifts_ShiftId_ShiftId] FOREIGN KEY ([ShiftId]) REFERENCES [dbo].[arShifts] ([ShiftId])
GO
ALTER TABLE [dbo].[arTerm] ADD CONSTRAINT [FK_arTerm_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[arTerm] ADD CONSTRAINT [FK_arTerm_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
ALTER TABLE [dbo].[arTerm] ADD CONSTRAINT [FK_arTerm_syTermTypes_TermTypeId_TermTypeId] FOREIGN KEY ([TermTypeId]) REFERENCES [dbo].[syTermTypes] ([TermTypeId])
GO
