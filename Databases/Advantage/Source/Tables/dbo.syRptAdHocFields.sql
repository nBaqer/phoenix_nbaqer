CREATE TABLE [dbo].[syRptAdHocFields]
(
[AdHocFieldId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syRptAdHocFields_AdHocFieldId] DEFAULT (newid()),
[ResourceId] [smallint] NULL,
[TblFldsId] [int] NULL,
[ColOrder] [smallint] NULL,
[Header] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Visible] [bit] NULL,
[FormatString] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Width] [int] NULL,
[ShowInGroupBy] [bit] NULL,
[ShowInSummary] [bit] NULL,
[SummaryType] [int] NULL,
[GroupBy] [bit] NULL,
[SortBy] [bit] NULL,
[IsParam] [bit] NULL,
[IsRequired] [bit] NULL,
[SDFId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syRptAdHocFields] ADD CONSTRAINT [PK_syRptAdHocFields_AdHocFieldId] PRIMARY KEY CLUSTERED  ([AdHocFieldId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syRptAdHocFields] ADD CONSTRAINT [FK_syRptAdHocFields_syRptAdHocFields_AdHocFieldId_AdHocFieldId] FOREIGN KEY ([AdHocFieldId]) REFERENCES [dbo].[syRptAdHocFields] ([AdHocFieldId])
GO
ALTER TABLE [dbo].[syRptAdHocFields] ADD CONSTRAINT [FK_syRptAdHocFields_syResources_ResourceId_ResourceID] FOREIGN KEY ([ResourceId]) REFERENCES [dbo].[syResources] ([ResourceID])
GO
ALTER TABLE [dbo].[syRptAdHocFields] ADD CONSTRAINT [FK_syRptAdHocFields_sySDF_SDFId_SDFId] FOREIGN KEY ([SDFId]) REFERENCES [dbo].[sySDF] ([SDFId])
GO
ALTER TABLE [dbo].[syRptAdHocFields] ADD CONSTRAINT [FK_syRptAdHocFields_syTblFlds_TblFldsId_TblFldsId] FOREIGN KEY ([TblFldsId]) REFERENCES [dbo].[syTblFlds] ([TblFldsId])
GO
