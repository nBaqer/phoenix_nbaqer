CREATE TABLE [dbo].[syTables]
(
[TblId] [int] NOT NULL,
[TblName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TblDescrip] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TblPK] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syTables] ADD CONSTRAINT [PK_syTables_TblId] PRIMARY KEY CLUSTERED  ([TblId]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[syTables] TO [AdvantageRole]
GO
