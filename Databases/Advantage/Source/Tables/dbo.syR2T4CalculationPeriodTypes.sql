CREATE TABLE [dbo].[syR2T4CalculationPeriodTypes]
(
[CalculationPeriodTypeId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_syR2T4CalculationPeriodTypes_CalculationPeriodTypeId] DEFAULT (newsequentialid()),
[Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL,
[ModifiedByUserId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syR2T4CalculationPeriodTypes] ADD CONSTRAINT [PK_syR2T4CalculationPeriodTypes_CalculationPeriodTypeId] PRIMARY KEY CLUSTERED  ([CalculationPeriodTypeId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syR2T4CalculationPeriodTypes] ADD CONSTRAINT [FK_syR2T4CalculationPeriodTypes_syUsers_ModifiedByUserId_UserId] FOREIGN KEY ([ModifiedByUserId]) REFERENCES [dbo].[syUsers] ([UserId])
GO
