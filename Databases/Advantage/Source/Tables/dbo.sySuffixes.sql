CREATE TABLE [dbo].[sySuffixes]
(
[SuffixId] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[SuffixCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[SuffixDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[sySuffixes] ADD CONSTRAINT [PK_sySuffixes_SuffixId] PRIMARY KEY CLUSTERED  ([SuffixId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[sySuffixes] ADD CONSTRAINT [FK_sySuffixes_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[sySuffixes] ADD CONSTRAINT [FK_sySuffixes_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
