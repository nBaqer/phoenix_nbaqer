CREATE TABLE [dbo].[syInstFldsDDReq]
(
[DDFldReqId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syInstFldsDDReq_DDFldReqId] DEFAULT (newid()),
[FldId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syInstFldsDDReq] ADD CONSTRAINT [PK_syInstFldsDDReq_DDFldReqId] PRIMARY KEY CLUSTERED  ([DDFldReqId]) ON [PRIMARY]
GO
GRANT REFERENCES ON  [dbo].[syInstFldsDDReq] TO [AdvantageRole]
GRANT SELECT ON  [dbo].[syInstFldsDDReq] TO [AdvantageRole]
GRANT INSERT ON  [dbo].[syInstFldsDDReq] TO [AdvantageRole]
GRANT DELETE ON  [dbo].[syInstFldsDDReq] TO [AdvantageRole]
GRANT UPDATE ON  [dbo].[syInstFldsDDReq] TO [AdvantageRole]
GO
