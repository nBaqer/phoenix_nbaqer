CREATE TABLE [dbo].[syModuleEntities]
(
[ModuleEntityId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syModuleEntities_ModuleEntityId] DEFAULT (newid()),
[ModuleId] [tinyint] NULL,
[EntityId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syModuleEntities] ADD CONSTRAINT [PK_syModuleEntities_ModuleEntityId] PRIMARY KEY CLUSTERED  ([ModuleEntityId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syModuleEntities] ADD CONSTRAINT [FK_syModuleEntities_syEntities_EntityId_EntityId] FOREIGN KEY ([EntityId]) REFERENCES [dbo].[syEntities] ([EntityId])
GO
