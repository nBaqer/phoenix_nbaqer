CREATE TABLE [dbo].[sySysDocStatuses]
(
[SysDocStatusId] [tinyint] NOT NULL,
[DocStatusDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[sySysDocStatuses] ADD CONSTRAINT [PK_sySysDocStatuses_SysDocStatusId] PRIMARY KEY CLUSTERED  ([SysDocStatusId]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[sySysDocStatuses] TO [AdvantageRole]
GO
