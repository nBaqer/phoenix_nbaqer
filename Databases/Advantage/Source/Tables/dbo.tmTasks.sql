CREATE TABLE [dbo].[tmTasks]
(
[TaskId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_tmTasks_TaskId] DEFAULT (newid()),
[Code] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CampGroupId] [uniqueidentifier] NULL,
[CategoryId] [uniqueidentifier] NULL,
[ModuleEntityId] [int] NULL,
[ModDate] [datetime] NULL,
[ModUser] [uniqueidentifier] NULL,
[Active] [tinyint] NULL,
[StatusCodeId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tmTasks] ADD CONSTRAINT [PK_tmTasks_TaskId] PRIMARY KEY CLUSTERED  ([TaskId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_tmTasks_Code_Descrip_CampGroupId] ON [dbo].[tmTasks] ([Code], [Descrip], [CampGroupId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tmTasks] ADD CONSTRAINT [FK_tmTasks_tmCategories_CategoryId_CategoryId] FOREIGN KEY ([CategoryId]) REFERENCES [dbo].[tmCategories] ([CategoryId])
GO
ALTER TABLE [dbo].[tmTasks] ADD CONSTRAINT [FK_tmTasks_syStatusCodes_StatusCodeId_StatusCodeId] FOREIGN KEY ([StatusCodeId]) REFERENCES [dbo].[syStatusCodes] ([StatusCodeId])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Link to campus group', 'SCHEMA', N'dbo', 'TABLE', N'tmTasks', 'COLUMN', N'CampGroupId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Link to tmCategories', 'SCHEMA', N'dbo', 'TABLE', N'tmTasks', 'COLUMN', N'CategoryId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Text description of the task', 'SCHEMA', N'dbo', 'TABLE', N'tmTasks', 'COLUMN', N'Descrip'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Creation of last mod date', 'SCHEMA', N'dbo', 'TABLE', N'tmTasks', 'COLUMN', N'ModDate'
GO
