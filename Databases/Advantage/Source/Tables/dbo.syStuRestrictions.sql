CREATE TABLE [dbo].[syStuRestrictions]
(
[StuRestrictionId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syStuRestrictions_StuRestrictionId] DEFAULT (newsequentialid()),
[SGroupId] [uniqueidentifier] NOT NULL,
[RestrictionTypeId] [uniqueidentifier] NOT NULL,
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Reason] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DepartmentId] [uniqueidentifier] NOT NULL,
[UserId] [uniqueidentifier] NOT NULL,
[CreateDate] [datetime] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syStuRestrictions] ADD CONSTRAINT [PK_syStuRestrictions_StuRestrictionId] PRIMARY KEY CLUSTERED  ([StuRestrictionId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syStuRestrictions] ADD CONSTRAINT [FK_syStuRestrictions_syDepartments_DepartmentId_DepartmentId] FOREIGN KEY ([DepartmentId]) REFERENCES [dbo].[syDepartments] ([DepartmentId])
GO
ALTER TABLE [dbo].[syStuRestrictions] ADD CONSTRAINT [FK_syStuRestrictions_syStuRestrictionTypes_RestrictionTypeId_RestrictionTypeId] FOREIGN KEY ([RestrictionTypeId]) REFERENCES [dbo].[syStuRestrictionTypes] ([RestrictionTypeId])
GO
ALTER TABLE [dbo].[syStuRestrictions] ADD CONSTRAINT [FK_syStuRestrictions_syUsers_UserId_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[syUsers] ([UserId])
GO
