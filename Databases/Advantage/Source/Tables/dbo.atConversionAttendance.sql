CREATE TABLE [dbo].[atConversionAttendance]
(
[AttendanceId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_atConversionAttendance_AttendanceId] DEFAULT (newid()),
[StuEnrollId] [uniqueidentifier] NOT NULL,
[MeetDate] [datetime] NOT NULL,
[Actual] [decimal] (18, 2) NOT NULL,
[Tardy] [bit] NOT NULL CONSTRAINT [DF_atConversionAttendance_Tardy] DEFAULT ((0)),
[Excused] [bit] NOT NULL CONSTRAINT [DF_atConversionAttendance_Excused] DEFAULT ((0)),
[Schedule] [decimal] (18, 2) NULL,
[PostByException] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_atConversionAttendance_PostByException] DEFAULT ('no'),
[comments] [varchar] (240) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NOT NULL CONSTRAINT [DF_atConversionAttendance_ModDate] DEFAULT (getdate()),
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_atConversionAttendance_ModUser] DEFAULT (''),
[ConversionAttendanceId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_atConversionAttendance_ConversionAttendanceId] DEFAULT (newsequentialid())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[atConversionAttendance] ADD CONSTRAINT [PK_atConversionAttendance_ConversionAttendanceId] PRIMARY KEY CLUSTERED  ([ConversionAttendanceId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_atConversionAttendance_MeetDate_StuEnrollId] ON [dbo].[atConversionAttendance] ([MeetDate], [StuEnrollId]) ON [PRIMARY]
GO
