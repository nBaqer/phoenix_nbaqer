CREATE TABLE [dbo].[arGradeSystemDetails]
(
[GrdSysDetailId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arGradeSystemDetails_GrdSysDetailId] DEFAULT (newid()),
[GrdSystemId] [uniqueidentifier] NOT NULL,
[Grade] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsPass] [bit] NOT NULL,
[GPA] [decimal] (18, 2) NULL,
[IsCreditsEarned] [bit] NOT NULL,
[IsCreditsAttempted] [bit] NOT NULL,
[IsInGPA] [bit] NOT NULL,
[IsInSAP] [bit] NOT NULL,
[IsTransferGrade] [bit] NOT NULL,
[IsIncomplete] [bit] NOT NULL CONSTRAINT [DF_arGradeSystemDetails_IsIncomplete] DEFAULT ((0)),
[IsDefault] [bit] NOT NULL CONSTRAINT [DF_arGradeSystemDetails_IsDefault] DEFAULT ((0)),
[IsDrop] [bit] NOT NULL CONSTRAINT [DF_arGradeSystemDetails_IsDrop] DEFAULT ((0)),
[ViewOrder] [int] NOT NULL CONSTRAINT [DF_arGradeSystemDetails_ViewOrder] DEFAULT ((0)),
[ShowInstructor] [bit] NULL,
[IsSatisfactory] [bit] NULL,
[IsUnSatisfactory] [bit] NULL,
[IsInPass] [bit] NULL,
[IsInFail] [bit] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[GradeDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[quality] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsCreditsAwarded] [bit] NOT NULL CONSTRAINT [DF_arGradeSystemDetails_IsCreditsAwarded] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arGradeSystemDetails] ADD CONSTRAINT [PK_arGradeSystemDetails_GrdSysDetailId] PRIMARY KEY CLUSTERED  ([GrdSysDetailId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arGradeSystemDetails] ADD CONSTRAINT [FK_arGradeSystemDetails_arGradeSystems_GrdSystemId_GrdSystemId] FOREIGN KEY ([GrdSystemId]) REFERENCES [dbo].[arGradeSystems] ([GrdSystemId])
GO
