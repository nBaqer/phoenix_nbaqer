CREATE TABLE [dbo].[syInstitutionAddresses]
(
[InstitutionAddressId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_syInstitutionAddresses_InstitutionAddressId] DEFAULT (newid()),
[InstitutionId] [uniqueidentifier] NOT NULL,
[AddressTypeId] [uniqueidentifier] NULL,
[Address1] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address2] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StateId] [uniqueidentifier] NULL,
[ZipCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CountryId] [uniqueidentifier] NULL,
[StatusId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_syInstitutionAddresses_StatusId] DEFAULT ([dbo].[UDF_GetSyStatusesValue]('A')),
[IsMailingAddress] [bit] NOT NULL CONSTRAINT [DF_syInstitutionAddresses_IsMailingAddress] DEFAULT ((0)),
[ModDate] [datetime] NOT NULL CONSTRAINT [DF_syInstitutionAddresses_ModDate] DEFAULT (getdate()),
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[State] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsInternational] [bit] NULL CONSTRAINT [DF_syInstitutionAddresses_IsInternational] DEFAULT ((0)),
[CountyId] [uniqueidentifier] NULL,
[ForeignCountyStr] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AddressApto] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_syInstitutionAddresses_AddressApto] DEFAULT (''),
[OtherState] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsDefault] [bit] NULL,
[ForeignCountry] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syInstitutionAddresses] ADD CONSTRAINT [PK_syInstitutionAddresses_InstitutionAddressId] PRIMARY KEY CLUSTERED  ([InstitutionAddressId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syInstitutionAddresses] ADD CONSTRAINT [FK_syInstitutionAddresses_adCounties_CountyId_CountyId] FOREIGN KEY ([CountyId]) REFERENCES [dbo].[adCounties] ([CountyId])
GO
ALTER TABLE [dbo].[syInstitutionAddresses] ADD CONSTRAINT [FK_syInstitutionAddresses_adCountries_CountryId_CountryId] FOREIGN KEY ([CountryId]) REFERENCES [dbo].[adCountries] ([CountryId])
GO
ALTER TABLE [dbo].[syInstitutionAddresses] ADD CONSTRAINT [FK_syInstitutionAddresses_plAddressTypes_AddressTypeId_AddressTypeId] FOREIGN KEY ([AddressTypeId]) REFERENCES [dbo].[plAddressTypes] ([AddressTypeId])
GO
ALTER TABLE [dbo].[syInstitutionAddresses] ADD CONSTRAINT [FK_syInstitutionAddresses_syInstitutions_InstitutionId_HSID] FOREIGN KEY ([InstitutionId]) REFERENCES [dbo].[syInstitutions] ([HSId])
GO
ALTER TABLE [dbo].[syInstitutionAddresses] ADD CONSTRAINT [FK_syInstitutionAddresses_syStates_StateId_StateId] FOREIGN KEY ([StateId]) REFERENCES [dbo].[syStates] ([StateId])
GO
ALTER TABLE [dbo].[syInstitutionAddresses] ADD CONSTRAINT [FK_syInstitutionAddresses_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
