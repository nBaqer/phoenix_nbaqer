CREATE TABLE [dbo].[arPrgChargePeriodSeq]
(
[PrgChrPeriodSeqId] [uniqueidentifier] NOT NULL,
[PrgVerId] [uniqueidentifier] NOT NULL,
[FeeLevelID] [tinyint] NOT NULL,
[Sequence] [smallint] NOT NULL,
[StartValue] [decimal] (18, 0) NOT NULL,
[Endvalue] [decimal] (18, 0) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arPrgChargePeriodSeq] ADD CONSTRAINT [PK_arPrgChargePeriodSeq_PrgChrPeriodSeqId] PRIMARY KEY CLUSTERED  ([PrgChrPeriodSeqId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arPrgChargePeriodSeq] ADD CONSTRAINT [FK_arPrgChargePeriodSeq_arPrgVersions_PrgVerId_PrgVerId] FOREIGN KEY ([PrgVerId]) REFERENCES [dbo].[arPrgVersions] ([PrgVerId]) ON DELETE CASCADE ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[arPrgChargePeriodSeq] ADD CONSTRAINT [FK_arPrgChargePeriodSeq_saFeeLevels_FeeLevelID_FeeLevelId] FOREIGN KEY ([FeeLevelID]) REFERENCES [dbo].[saFeeLevels] ([FeeLevelId])
GO
