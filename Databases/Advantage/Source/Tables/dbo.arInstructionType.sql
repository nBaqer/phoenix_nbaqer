CREATE TABLE [dbo].[arInstructionType]
(
[InstructionTypeId] [uniqueidentifier] NOT NULL,
[InstructionTypeCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[InstructionTypeDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[IsDefault] [bit] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arInstructionType] ADD CONSTRAINT [PK_arInstructionType_InstructionTypeId] PRIMARY KEY CLUSTERED  ([InstructionTypeId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_arInstructionType_InstructionTypeCode] ON [dbo].[arInstructionType] ([InstructionTypeCode]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arInstructionType] ADD CONSTRAINT [FK_arInstructionType_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
