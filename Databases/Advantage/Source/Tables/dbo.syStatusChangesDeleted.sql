CREATE TABLE [dbo].[syStatusChangesDeleted]
(
[Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_syStatusChangesDeleted_Id] DEFAULT (newid()),
[StudentStatusChangeId] [uniqueidentifier] NOT NULL,
[StuEnrollId] [uniqueidentifier] NOT NULL,
[OrigStatusId] [uniqueidentifier] NULL,
[NewStatusId] [uniqueidentifier] NULL,
[CampusId] [uniqueidentifier] NOT NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsReversal] [bit] NOT NULL CONSTRAINT [DF_syStatusChangesDeleted_IsReversal] DEFAULT ((0)),
[DropReasonId] [uniqueidentifier] NULL,
[DateOfChange] [datetime] NULL,
[Lda] [datetime2] NULL,
[CaseNumber] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_syStatusChangesDeleted_CaseNumber] DEFAULT (NULL),
[RequestedBy] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF_syStatusChangesDeleted_RequestedBy] DEFAULT (NULL),
[HaveBackup] [bit] NULL CONSTRAINT [DF_syStatusChangesDeleted_HaveBackup] DEFAULT ((0)),
[HaveClientConfirmation] [bit] NULL CONSTRAINT [DF_syStatusChangesDeleted_HaveClientConfirmation] DEFAULT ((0)),
[DeleteDate] [datetime] NOT NULL,
[UserDeleted] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DeleteReasonsId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syStatusChangesDeleted] ADD CONSTRAINT [PK_syStatusChangesDeleted_Id] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syStatusChangesDeleted] ADD CONSTRAINT [FK_syStatusChangesDeleted_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY ([StuEnrollId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
ALTER TABLE [dbo].[syStatusChangesDeleted] ADD CONSTRAINT [FK_syStatusChangesDeleted_syCampuses_CampusId_CampusId] FOREIGN KEY ([CampusId]) REFERENCES [dbo].[syCampuses] ([CampusId])
GO
ALTER TABLE [dbo].[syStatusChangesDeleted] ADD CONSTRAINT [FK_syStatusChangesDeleted_syStatusChangeDeleteReasons_DeleteReasonsId_Id] FOREIGN KEY ([DeleteReasonsId]) REFERENCES [dbo].[syStatusChangeDeleteReasons] ([Id])
GO
ALTER TABLE [dbo].[syStatusChangesDeleted] ADD CONSTRAINT [FK_syStatusChangesDeleted_syStatusCodes_NewStatusId_StatusCodeId] FOREIGN KEY ([NewStatusId]) REFERENCES [dbo].[syStatusCodes] ([StatusCodeId])
GO
ALTER TABLE [dbo].[syStatusChangesDeleted] ADD CONSTRAINT [FK_syStatusChangesDeleted_syStatusCodes_OrigStatusId_StatusCodeId] FOREIGN KEY ([OrigStatusId]) REFERENCES [dbo].[syStatusCodes] ([StatusCodeId])
GO
