CREATE TABLE [dbo].[sySdfDTypes]
(
[DTypeId] [tinyint] NOT NULL,
[DType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[sySdfDTypes] ADD CONSTRAINT [PK_sySdfDTypes_DTypeId] PRIMARY KEY CLUSTERED  ([DTypeId]) ON [PRIMARY]
GO
