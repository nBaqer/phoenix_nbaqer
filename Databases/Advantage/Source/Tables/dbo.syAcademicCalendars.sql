CREATE TABLE [dbo].[syAcademicCalendars]
(
[ACId] [int] NOT NULL,
[ACDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syAcademicCalendars] ADD CONSTRAINT [PK_syAcademicCalendars_ACId] PRIMARY KEY CLUSTERED  ([ACId]) ON [PRIMARY]
GO
