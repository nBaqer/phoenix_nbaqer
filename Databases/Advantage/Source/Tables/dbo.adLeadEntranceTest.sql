CREATE TABLE [dbo].[adLeadEntranceTest]
(
[LeadEntrTestId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_adLeadEntranceTest_LeadEntrTestId] DEFAULT (newid()),
[LeadId] [uniqueidentifier] NULL,
[EntrTestId] [uniqueidentifier] NULL,
[TestTaken] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActualScore] [decimal] (19, 3) NULL,
[Comments] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ViewOrder] [int] NULL,
[Required] [bit] NULL,
[Pass] [bit] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[OverRide] [bit] NULL,
[StudentId] [uniqueidentifier] NULL,
[OverrideReason] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CompletedDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
--========================================================================================== 
-- TRIGGER adLeadEntranceTest_Audit_Delete 
-- DELETE  add Audit History when delete records in adLeadEntranceTest
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadEntranceTest_Audit_Delete] ON [dbo].[adLeadEntranceTest]
    FOR DELETE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Deleted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Deleted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Deleted
                        );
 
        IF @EventRows > 0
            BEGIN
                EXEC fmAuditHistAdd @AuditHistId,'adLeadEntranceTest','D',@EventRows,@EventDate,@UserName;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEntrTestId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),Old.LeadId)
                        FROM    Deleted AS Old;
                -- EntrTestId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEntrTestId
                               ,'EntrTestId'
                               ,CONVERT(VARCHAR(MAX),Old.EntrTestId)
                        FROM    Deleted AS Old;
                -- TestTaken 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEntrTestId
                               ,'TestTaken'
                               ,CONVERT(VARCHAR(MAX),Old.TestTaken)
                        FROM    Deleted AS Old;
                -- ActualScore 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEntrTestId
                               ,'ActualScore'
                               ,CONVERT(VARCHAR(MAX),Old.ActualScore)
                        FROM    Deleted AS Old;
                -- Comments 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEntrTestId
                               ,'Comments'
                               ,CONVERT(VARCHAR(MAX),Old.Comments)
                        FROM    Deleted AS Old;
                -- ViewOrder 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEntrTestId
                               ,'ViewOrder'
                               ,CONVERT(VARCHAR(MAX),Old.ViewOrder)
                        FROM    Deleted AS Old;
                -- Required 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEntrTestId
                               ,'Required'
                               ,CONVERT(VARCHAR(MAX),Old.Required)
                        FROM    Deleted AS Old;
                -- Pass 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEntrTestId
                               ,'Pass'
                               ,CONVERT(VARCHAR(MAX),Old.Pass)
                        FROM    Deleted AS Old;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEntrTestId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),Old.ModUser)
                        FROM    Deleted AS Old;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEntrTestId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                        FROM    Deleted AS Old;
                -- OverRide 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEntrTestId
                               ,'OverRide'
                               ,CONVERT(VARCHAR(MAX),Old.OverRide)
                        FROM    Deleted AS Old;
                -- StudentId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEntrTestId
                               ,'StudentId'
                               ,CONVERT(VARCHAR(MAX),Old.StudentId)
                        FROM    Deleted AS Old;
                -- OverrideReason 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEntrTestId
                               ,'OverrideReason'
                               ,CONVERT(VARCHAR(MAX),Old.OverrideReason)
                        FROM    Deleted AS Old;
                -- CompletedDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadEntrTestId
                               ,'CompletedDate'
                               ,CONVERT(VARCHAR(MAX),Old.CompletedDate,121)
                        FROM    Deleted AS Old;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadEntranceTest_Audit_Delete 
--========================================================================================== 
 


GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
--========================================================================================== 
-- TRIGGER adLeadEntranceTest_Audit_Insert 
-- INSERT  add Audit History when insert records in adLeadEntranceTest
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadEntranceTest_Audit_Insert] ON [dbo].[adLeadEntranceTest]
    FOR INSERT
AS
    BEGIN 
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadEntranceTest','I',@EventRows,@EventDate,@UserName;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEntrTestId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),New.LeadId)
                        FROM    Inserted AS New;
                -- EntrTestId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEntrTestId
                               ,'EntrTestId'
                               ,CONVERT(VARCHAR(MAX),New.EntrTestId)
                        FROM    Inserted AS New;
                -- TestTaken 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEntrTestId
                               ,'TestTaken'
                               ,CONVERT(VARCHAR(MAX),New.TestTaken)
                        FROM    Inserted AS New;
                -- ActualScore 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEntrTestId
                               ,'ActualScore'
                               ,CONVERT(VARCHAR(MAX),New.ActualScore)
                        FROM    Inserted AS New;
                -- Comments 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEntrTestId
                               ,'Comments'
                               ,CONVERT(VARCHAR(MAX),New.Comments)
                        FROM    Inserted AS New;
                -- ViewOrder 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEntrTestId
                               ,'ViewOrder'
                               ,CONVERT(VARCHAR(MAX),New.ViewOrder)
                        FROM    Inserted AS New;
                -- Required 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEntrTestId
                               ,'Required'
                               ,CONVERT(VARCHAR(MAX),New.Required)
                        FROM    Inserted AS New;
                -- Pass 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEntrTestId
                               ,'Pass'
                               ,CONVERT(VARCHAR(MAX),New.Pass)
                        FROM    Inserted AS New;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEntrTestId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),New.ModUser)
                        FROM    Inserted AS New;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEntrTestId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                        FROM    Inserted AS New;
                -- OverRide 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEntrTestId
                               ,'OverRide'
                               ,CONVERT(VARCHAR(MAX),New.OverRide)
                        FROM    Inserted AS New;
                -- StudentId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEntrTestId
                               ,'StudentId'
                               ,CONVERT(VARCHAR(MAX),New.StudentId)
                        FROM    Inserted AS New;
                -- OverrideReason 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEntrTestId
                               ,'OverrideReason'
                               ,CONVERT(VARCHAR(MAX),New.OverrideReason)
                        FROM    Inserted AS New;
                -- CompletedDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadEntrTestId
                               ,'CompletedDate'
                               ,CONVERT(VARCHAR(MAX),New.CompletedDate,121)
                        FROM    Inserted AS New;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadEntranceTest_Audit_Insert 
--========================================================================================== 
 


GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--========================================================================================== 
-- TRIGGER adLeadEntranceTest_Audit_Update 
-- UPDATE  add Audit History when update fields in adLeadEntranceTest
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadEntranceTest_Audit_Update] ON [dbo].[adLeadEntranceTest]
    FOR UPDATE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadEntranceTest','U',@EventRows,@EventDate,@UserName;
                -- LeadId 
                IF UPDATE(LeadId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEntrTestId
                                       ,'LeadId'
                                       ,CONVERT(VARCHAR(MAX),Old.LeadId)
                                       ,CONVERT(VARCHAR(MAX),New.LeadId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEntrTestId = New.LeadEntrTestId
                                WHERE   Old.LeadId <> New.LeadId
                                        OR (
                                             Old.LeadId IS NULL
                                             AND New.LeadId IS NOT NULL
                                           )
                                        OR (
                                             New.LeadId IS NULL
                                             AND Old.LeadId IS NOT NULL
                                           );
                    END;
                -- EntrTestId 
                IF UPDATE(EntrTestId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEntrTestId
                                       ,'EntrTestId'
                                       ,CONVERT(VARCHAR(MAX),Old.EntrTestId)
                                       ,CONVERT(VARCHAR(MAX),New.EntrTestId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEntrTestId = New.LeadEntrTestId
                                WHERE   Old.EntrTestId <> New.EntrTestId
                                        OR (
                                             Old.EntrTestId IS NULL
                                             AND New.EntrTestId IS NOT NULL
                                           )
                                        OR (
                                             New.EntrTestId IS NULL
                                             AND Old.EntrTestId IS NOT NULL
                                           );
                    END;
                -- TestTaken 
                IF UPDATE(TestTaken)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEntrTestId
                                       ,'TestTaken'
                                       ,CONVERT(VARCHAR(MAX),Old.TestTaken)
                                       ,CONVERT(VARCHAR(MAX),New.TestTaken)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEntrTestId = New.LeadEntrTestId
                                WHERE   Old.TestTaken <> New.TestTaken
                                        OR (
                                             Old.TestTaken IS NULL
                                             AND New.TestTaken IS NOT NULL
                                           )
                                        OR (
                                             New.TestTaken IS NULL
                                             AND Old.TestTaken IS NOT NULL
                                           );
                    END;
                -- ActualScore 
                IF UPDATE(ActualScore)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEntrTestId
                                       ,'ActualScore'
                                       ,CONVERT(VARCHAR(MAX),Old.ActualScore)
                                       ,CONVERT(VARCHAR(MAX),New.ActualScore)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEntrTestId = New.LeadEntrTestId
                                WHERE   Old.ActualScore <> New.ActualScore
                                        OR (
                                             Old.ActualScore IS NULL
                                             AND New.ActualScore IS NOT NULL
                                           )
                                        OR (
                                             New.ActualScore IS NULL
                                             AND Old.ActualScore IS NOT NULL
                                           );
                    END;
                -- Comments 
                IF UPDATE(Comments)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEntrTestId
                                       ,'Comments'
                                       ,CONVERT(VARCHAR(MAX),Old.Comments)
                                       ,CONVERT(VARCHAR(MAX),New.Comments)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEntrTestId = New.LeadEntrTestId
                                WHERE   Old.Comments <> New.Comments
                                        OR (
                                             Old.Comments IS NULL
                                             AND New.Comments IS NOT NULL
                                           )
                                        OR (
                                             New.Comments IS NULL
                                             AND Old.Comments IS NOT NULL
                                           );
                    END;
                -- ViewOrder 
                IF UPDATE(ViewOrder)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEntrTestId
                                       ,'ViewOrder'
                                       ,CONVERT(VARCHAR(MAX),Old.ViewOrder)
                                       ,CONVERT(VARCHAR(MAX),New.ViewOrder)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEntrTestId = New.LeadEntrTestId
                                WHERE   Old.ViewOrder <> New.ViewOrder
                                        OR (
                                             Old.ViewOrder IS NULL
                                             AND New.ViewOrder IS NOT NULL
                                           )
                                        OR (
                                             New.ViewOrder IS NULL
                                             AND Old.ViewOrder IS NOT NULL
                                           );
                    END;
                -- Required 
                IF UPDATE(Required)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEntrTestId
                                       ,'Required'
                                       ,CONVERT(VARCHAR(MAX),Old.Required)
                                       ,CONVERT(VARCHAR(MAX),New.Required)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEntrTestId = New.LeadEntrTestId
                                WHERE   Old.Required <> New.Required
                                        OR (
                                             Old.Required IS NULL
                                             AND New.Required IS NOT NULL
                                           )
                                        OR (
                                             New.Required IS NULL
                                             AND Old.Required IS NOT NULL
                                           );
                    END;
                -- Pass 
                IF UPDATE(Pass)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEntrTestId
                                       ,'Pass'
                                       ,CONVERT(VARCHAR(MAX),Old.Pass)
                                       ,CONVERT(VARCHAR(MAX),New.Pass)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEntrTestId = New.LeadEntrTestId
                                WHERE   Old.Pass <> New.Pass
                                        OR (
                                             Old.Pass IS NULL
                                             AND New.Pass IS NOT NULL
                                           )
                                        OR (
                                             New.Pass IS NULL
                                             AND Old.Pass IS NOT NULL
                                           );
                    END;
                -- ModUser 
                IF UPDATE(ModUser)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEntrTestId
                                       ,'ModUser'
                                       ,CONVERT(VARCHAR(MAX),Old.ModUser)
                                       ,CONVERT(VARCHAR(MAX),New.ModUser)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEntrTestId = New.LeadEntrTestId
                                WHERE   Old.ModUser <> New.ModUser
                                        OR (
                                             Old.ModUser IS NULL
                                             AND New.ModUser IS NOT NULL
                                           )
                                        OR (
                                             New.ModUser IS NULL
                                             AND Old.ModUser IS NOT NULL
                                           );
                    END;
                -- ModDate 
                IF UPDATE(ModDate)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEntrTestId
                                       ,'ModDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEntrTestId = New.LeadEntrTestId
                                WHERE   Old.ModDate <> New.ModDate
                                        OR (
                                             Old.ModDate IS NULL
                                             AND New.ModDate IS NOT NULL
                                           )
                                        OR (
                                             New.ModDate IS NULL
                                             AND Old.ModDate IS NOT NULL
                                           );
                    END;
                -- OverRide 
                IF UPDATE(OverRide)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEntrTestId
                                       ,'OverRide'
                                       ,CONVERT(VARCHAR(MAX),Old.OverRide)
                                       ,CONVERT(VARCHAR(MAX),New.OverRide)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEntrTestId = New.LeadEntrTestId
                                WHERE   Old.OverRide <> New.OverRide
                                        OR (
                                             Old.OverRide IS NULL
                                             AND New.OverRide IS NOT NULL
                                           )
                                        OR (
                                             New.OverRide IS NULL
                                             AND Old.OverRide IS NOT NULL
                                           );
                    END;
                -- StudentId 
                IF UPDATE(StudentId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEntrTestId
                                       ,'StudentId'
                                       ,CONVERT(VARCHAR(MAX),Old.StudentId)
                                       ,CONVERT(VARCHAR(MAX),New.StudentId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEntrTestId = New.LeadEntrTestId
                                WHERE   Old.StudentId <> New.StudentId
                                        OR (
                                             Old.StudentId IS NULL
                                             AND New.StudentId IS NOT NULL
                                           )
                                        OR (
                                             New.StudentId IS NULL
                                             AND Old.StudentId IS NOT NULL
                                           );
                    END;
                -- OverrideReason 
                IF UPDATE(OverrideReason)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEntrTestId
                                       ,'OverrideReason'
                                       ,CONVERT(VARCHAR(MAX),Old.OverrideReason)
                                       ,CONVERT(VARCHAR(MAX),New.OverrideReason)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEntrTestId = New.LeadEntrTestId
                                WHERE   Old.OverrideReason <> New.OverrideReason
                                        OR (
                                             Old.OverrideReason IS NULL
                                             AND New.OverrideReason IS NOT NULL
                                           )
                                        OR (
                                             New.OverrideReason IS NULL
                                             AND Old.OverrideReason IS NOT NULL
                                           );
                    END;
                -- CompletedDate 
                IF UPDATE(CompletedDate)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.LeadEntrTestId
                                       ,'CompletedDate'
                                       ,CONVERT(VARCHAR(MAX),Old.CompletedDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.CompletedDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.LeadEntrTestId = New.LeadEntrTestId
                                WHERE   Old.CompletedDate <> New.CompletedDate
                                        OR (
                                             Old.CompletedDate IS NULL
                                             AND New.CompletedDate IS NOT NULL
                                           )
                                        OR (
                                             New.CompletedDate IS NULL
                                             AND Old.CompletedDate IS NOT NULL
                                           );
                    END;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadEntranceTest_Audit_Update 
--========================================================================================== 
 


GO
ALTER TABLE [dbo].[adLeadEntranceTest] ADD CONSTRAINT [PK_adLeadEntranceTest_LeadEntrTestId] PRIMARY KEY CLUSTERED  ([LeadEntrTestId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_adLeadEntranceTest_LeadId_EntrTestId] ON [dbo].[adLeadEntranceTest] ([LeadId], [EntrTestId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adLeadEntranceTest] ADD CONSTRAINT [FK_adLeadEntranceTest_adLeads_LeadId_LeadId] FOREIGN KEY ([LeadId]) REFERENCES [dbo].[adLeads] ([LeadId])
GO
ALTER TABLE [dbo].[adLeadEntranceTest] ADD CONSTRAINT [FK_adLeadEntranceTest_adReqs_EntrTestId_adReqId] FOREIGN KEY ([EntrTestId]) REFERENCES [dbo].[adReqs] ([adReqId])
GO
