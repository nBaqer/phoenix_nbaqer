CREATE TABLE [dbo].[syTimeClockImportLog]
(
[TimeClockImportLogId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syTimeClockImportLog_TimeClockImportLogId] DEFAULT (newid()),
[CampusId] [uniqueidentifier] NOT NULL,
[FileName] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Message] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Status] [int] NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syTimeClockImportLog] ADD CONSTRAINT [PK_syTimeClockImportLog_TimeClockImportLogId] PRIMARY KEY CLUSTERED  ([TimeClockImportLogId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syTimeClockImportLog] ADD CONSTRAINT [FK_syTimeClockImportLog_syCampuses_CampusId_CampusId] FOREIGN KEY ([CampusId]) REFERENCES [dbo].[syCampuses] ([CampusId])
GO
