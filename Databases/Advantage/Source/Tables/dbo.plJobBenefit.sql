CREATE TABLE [dbo].[plJobBenefit]
(
[JobBenefitId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_plJobBenefit_JobBenefitId] DEFAULT (newid()),
[JobBenefitCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[JobBenefitDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CamGrpId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[plJobBenefit] ADD CONSTRAINT [PK_plJobBenefit_JobBenefitId] PRIMARY KEY CLUSTERED  ([JobBenefitId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[plJobBenefit] ADD CONSTRAINT [FK_plJobBenefit_syCampGrps_CamGrpId_CampGrpId] FOREIGN KEY ([CamGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[plJobBenefit] ADD CONSTRAINT [FK_plJobBenefit_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
