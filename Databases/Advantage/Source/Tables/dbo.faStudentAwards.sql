CREATE TABLE [dbo].[faStudentAwards]
(
[StudentAwardId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_faStudentAwards_StudentAwardId] DEFAULT (newid()),
[AwardId] [int] NULL,
[StuEnrollId] [uniqueidentifier] NOT NULL,
[AwardTypeId] [uniqueidentifier] NOT NULL,
[AcademicYearId] [uniqueidentifier] NULL,
[LenderId] [uniqueidentifier] NULL,
[ServicerId] [uniqueidentifier] NULL,
[GuarantorId] [uniqueidentifier] NULL,
[GrossAmount] [decimal] (19, 4) NOT NULL,
[LoanFees] [decimal] (19, 4) NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[AwardStartDate] [datetime] NOT NULL,
[AwardEndDate] [datetime] NOT NULL,
[Disbursements] [int] NOT NULL,
[LoanId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EMASFundCode] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FA_Id] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AwardCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AwardSubCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AwardStatus] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[faStudentAwards_Audit_Delete] ON [dbo].[faStudentAwards]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 

    DECLARE @stuEnrollId AS UNIQUEIDENTIFIER;

    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    );

    SET @stuEnrollId = (
                         SELECT TOP 1
                                StuEnrollId
                         FROM   Deleted
                       ); 
 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'faStudentAwards','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentAwardId
                               ,'Disbursements'
                               ,CONVERT(VARCHAR(8000),Old.Disbursements,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentAwardId
                               ,'LenderId'
                               ,CONVERT(VARCHAR(8000),Old.LenderId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentAwardId
                               ,'GuarantorId'
                               ,CONVERT(VARCHAR(8000),Old.GuarantorId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentAwardId
                               ,'ServicerId'
                               ,CONVERT(VARCHAR(8000),Old.ServicerId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentAwardId
                               ,'StuEnrollId'
                               ,CONVERT(VARCHAR(8000),Old.StuEnrollId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentAwardId
                               ,'AwardTypeId'
                               ,CONVERT(VARCHAR(8000),Old.AwardTypeId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentAwardId
                               ,'AcademicYearId'
                               ,CONVERT(VARCHAR(8000),Old.AcademicYearId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentAwardId
                               ,'AwardStartDate'
                               ,CONVERT(VARCHAR(8000),Old.AwardStartDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentAwardId
                               ,'AwardEndDate'
                               ,CONVERT(VARCHAR(8000),Old.AwardEndDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentAwardId
                               ,'LoanId'
                               ,CONVERT(VARCHAR(8000),Old.LoanId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentAwardId
                               ,'LoanFees'
                               ,CONVERT(VARCHAR(8000),Old.LoanFees,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentAwardId
                               ,'GrossAmount'
                               ,CONVERT(VARCHAR(8000),Old.GrossAmount,121)
                        FROM    Deleted Old; 
            END; 
--EXEC dbo.USP_AR_StuEnrollPayPeriods @stuEnrollId
        END;



    SET NOCOUNT OFF; 
GO
GO

GO

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[faStudentAwards_Audit_Insert] ON [dbo].[faStudentAwards]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 

    DECLARE @stuEnrollId AS UNIQUEIDENTIFIER;

    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 

    SET @stuEnrollId = (
                         SELECT TOP 1
                                StuEnrollId
                         FROM   Inserted
                       ); 

    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'faStudentAwards','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StudentAwardId
                               ,'Disbursements'
                               ,CONVERT(VARCHAR(8000),New.Disbursements,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StudentAwardId
                               ,'LenderId'
                               ,CONVERT(VARCHAR(8000),New.LenderId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StudentAwardId
                               ,'GuarantorId'
                               ,CONVERT(VARCHAR(8000),New.GuarantorId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StudentAwardId
                               ,'ServicerId'
                               ,CONVERT(VARCHAR(8000),New.ServicerId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StudentAwardId
                               ,'StuEnrollId'
                               ,CONVERT(VARCHAR(8000),New.StuEnrollId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StudentAwardId
                               ,'AwardTypeId'
                               ,CONVERT(VARCHAR(8000),New.AwardTypeId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StudentAwardId
                               ,'AcademicYearId'
                               ,CONVERT(VARCHAR(8000),New.AcademicYearId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StudentAwardId
                               ,'AwardStartDate'
                               ,CONVERT(VARCHAR(8000),New.AwardStartDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StudentAwardId
                               ,'AwardEndDate'
                               ,CONVERT(VARCHAR(8000),New.AwardEndDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StudentAwardId
                               ,'LoanId'
                               ,CONVERT(VARCHAR(8000),New.LoanId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StudentAwardId
                               ,'LoanFees'
                               ,CONVERT(VARCHAR(8000),New.LoanFees,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StudentAwardId
                               ,'GrossAmount'
                               ,CONVERT(VARCHAR(8000),New.GrossAmount,121)
                        FROM    Inserted New; 
            END; 
--EXEC dbo.USP_AR_StuEnrollPayPeriods @stuEnrollId
        END;



    SET NOCOUNT OFF; 
GO
GO

GO

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[faStudentAwards_Audit_Update] ON [dbo].[faStudentAwards]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);

    DECLARE @stuEnrollId AS UNIQUEIDENTIFIER;

    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'faStudentAwards','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(Disbursements)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StudentAwardId
                                   ,'Disbursements'
                                   ,CONVERT(VARCHAR(8000),Old.Disbursements,121)
                                   ,CONVERT(VARCHAR(8000),New.Disbursements,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StudentAwardId = New.StudentAwardId
                            WHERE   Old.Disbursements <> New.Disbursements
                                    OR (
                                         Old.Disbursements IS NULL
                                         AND New.Disbursements IS NOT NULL
                                       )
                                    OR (
                                         New.Disbursements IS NULL
                                         AND Old.Disbursements IS NOT NULL
                                       ); 
                IF UPDATE(LenderId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StudentAwardId
                                   ,'LenderId'
                                   ,CONVERT(VARCHAR(8000),Old.LenderId,121)
                                   ,CONVERT(VARCHAR(8000),New.LenderId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StudentAwardId = New.StudentAwardId
                            WHERE   Old.LenderId <> New.LenderId
                                    OR (
                                         Old.LenderId IS NULL
                                         AND New.LenderId IS NOT NULL
                                       )
                                    OR (
                                         New.LenderId IS NULL
                                         AND Old.LenderId IS NOT NULL
                                       ); 
                IF UPDATE(GuarantorId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StudentAwardId
                                   ,'GuarantorId'
                                   ,CONVERT(VARCHAR(8000),Old.GuarantorId,121)
                                   ,CONVERT(VARCHAR(8000),New.GuarantorId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StudentAwardId = New.StudentAwardId
                            WHERE   Old.GuarantorId <> New.GuarantorId
                                    OR (
                                         Old.GuarantorId IS NULL
                                         AND New.GuarantorId IS NOT NULL
                                       )
                                    OR (
                                         New.GuarantorId IS NULL
                                         AND Old.GuarantorId IS NOT NULL
                                       ); 
                IF UPDATE(ServicerId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StudentAwardId
                                   ,'ServicerId'
                                   ,CONVERT(VARCHAR(8000),Old.ServicerId,121)
                                   ,CONVERT(VARCHAR(8000),New.ServicerId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StudentAwardId = New.StudentAwardId
                            WHERE   Old.ServicerId <> New.ServicerId
                                    OR (
                                         Old.ServicerId IS NULL
                                         AND New.ServicerId IS NOT NULL
                                       )
                                    OR (
                                         New.ServicerId IS NULL
                                         AND Old.ServicerId IS NOT NULL
                                       ); 
                IF UPDATE(StuEnrollId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StudentAwardId
                                   ,'StuEnrollId'
                                   ,CONVERT(VARCHAR(8000),Old.StuEnrollId,121)
                                   ,CONVERT(VARCHAR(8000),New.StuEnrollId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StudentAwardId = New.StudentAwardId
                            WHERE   Old.StuEnrollId <> New.StuEnrollId
                                    OR (
                                         Old.StuEnrollId IS NULL
                                         AND New.StuEnrollId IS NOT NULL
                                       )
                                    OR (
                                         New.StuEnrollId IS NULL
                                         AND Old.StuEnrollId IS NOT NULL
                                       ); 
                IF UPDATE(AwardTypeId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StudentAwardId
                                   ,'AwardTypeId'
                                   ,CONVERT(VARCHAR(8000),Old.AwardTypeId,121)
                                   ,CONVERT(VARCHAR(8000),New.AwardTypeId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StudentAwardId = New.StudentAwardId
                            WHERE   Old.AwardTypeId <> New.AwardTypeId
                                    OR (
                                         Old.AwardTypeId IS NULL
                                         AND New.AwardTypeId IS NOT NULL
                                       )
                                    OR (
                                         New.AwardTypeId IS NULL
                                         AND Old.AwardTypeId IS NOT NULL
                                       ); 
                IF UPDATE(AcademicYearId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StudentAwardId
                                   ,'AcademicYearId'
                                   ,CONVERT(VARCHAR(8000),Old.AcademicYearId,121)
                                   ,CONVERT(VARCHAR(8000),New.AcademicYearId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StudentAwardId = New.StudentAwardId
                            WHERE   Old.AcademicYearId <> New.AcademicYearId
                                    OR (
                                         Old.AcademicYearId IS NULL
                                         AND New.AcademicYearId IS NOT NULL
                                       )
                                    OR (
                                         New.AcademicYearId IS NULL
                                         AND Old.AcademicYearId IS NOT NULL
                                       ); 
                IF UPDATE(AwardStartDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StudentAwardId
                                   ,'AwardStartDate'
                                   ,CONVERT(VARCHAR(8000),Old.AwardStartDate,121)
                                   ,CONVERT(VARCHAR(8000),New.AwardStartDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StudentAwardId = New.StudentAwardId
                            WHERE   Old.AwardStartDate <> New.AwardStartDate
                                    OR (
                                         Old.AwardStartDate IS NULL
                                         AND New.AwardStartDate IS NOT NULL
                                       )
                                    OR (
                                         New.AwardStartDate IS NULL
                                         AND Old.AwardStartDate IS NOT NULL
                                       ); 
                IF UPDATE(AwardEndDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StudentAwardId
                                   ,'AwardEndDate'
                                   ,CONVERT(VARCHAR(8000),Old.AwardEndDate,121)
                                   ,CONVERT(VARCHAR(8000),New.AwardEndDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StudentAwardId = New.StudentAwardId
                            WHERE   Old.AwardEndDate <> New.AwardEndDate
                                    OR (
                                         Old.AwardEndDate IS NULL
                                         AND New.AwardEndDate IS NOT NULL
                                       )
                                    OR (
                                         New.AwardEndDate IS NULL
                                         AND Old.AwardEndDate IS NOT NULL
                                       ); 
                IF UPDATE(LoanId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StudentAwardId
                                   ,'LoanId'
                                   ,CONVERT(VARCHAR(8000),Old.LoanId,121)
                                   ,CONVERT(VARCHAR(8000),New.LoanId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StudentAwardId = New.StudentAwardId
                            WHERE   Old.LoanId <> New.LoanId
                                    OR (
                                         Old.LoanId IS NULL
                                         AND New.LoanId IS NOT NULL
                                       )
                                    OR (
                                         New.LoanId IS NULL
                                         AND Old.LoanId IS NOT NULL
                                       ); 
                IF UPDATE(LoanFees)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StudentAwardId
                                   ,'LoanFees'
                                   ,CONVERT(VARCHAR(8000),Old.LoanFees,121)
                                   ,CONVERT(VARCHAR(8000),New.LoanFees,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StudentAwardId = New.StudentAwardId
                            WHERE   Old.LoanFees <> New.LoanFees
                                    OR (
                                         Old.LoanFees IS NULL
                                         AND New.LoanFees IS NOT NULL
                                       )
                                    OR (
                                         New.LoanFees IS NULL
                                         AND Old.LoanFees IS NOT NULL
                                       ); 
                IF UPDATE(GrossAmount)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StudentAwardId
                                   ,'GrossAmount'
                                   ,CONVERT(VARCHAR(8000),Old.GrossAmount,121)
                                   ,CONVERT(VARCHAR(8000),New.GrossAmount,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StudentAwardId = New.StudentAwardId
                            WHERE   Old.GrossAmount <> New.GrossAmount
                                    OR (
                                         Old.GrossAmount IS NULL
                                         AND New.GrossAmount IS NOT NULL
                                       )
                                    OR (
                                         New.GrossAmount IS NULL
                                         AND Old.GrossAmount IS NOT NULL
                                       ); 
            END; 
--EXEC dbo.USP_AR_StuEnrollPayPeriods @stuEnrollId
        END;



    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[faStudentAwards] ADD CONSTRAINT [PK_faStudentAwards_StudentAwardId] PRIMARY KEY CLUSTERED  ([StudentAwardId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_faStudentAwards_AwardTypeId] ON [dbo].[faStudentAwards] ([AwardTypeId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_faStudentAwards_StudentAwardId] ON [dbo].[faStudentAwards] ([StudentAwardId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_faStudentAwards_StuEnrollId] ON [dbo].[faStudentAwards] ([StuEnrollId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[faStudentAwards] ADD CONSTRAINT [FK_faStudentAwards_saAcademicYears_AcademicYearId_AcademicYearId] FOREIGN KEY ([AcademicYearId]) REFERENCES [dbo].[saAcademicYears] ([AcademicYearId])
GO
ALTER TABLE [dbo].[faStudentAwards] ADD CONSTRAINT [FK_faStudentAwards_saFundSources_AwardTypeId_FundSourceId] FOREIGN KEY ([AwardTypeId]) REFERENCES [dbo].[saFundSources] ([FundSourceId])
GO
ALTER TABLE [dbo].[faStudentAwards] ADD CONSTRAINT [FK_faStudentAwards_faLenders_GuarantorId_LenderId] FOREIGN KEY ([GuarantorId]) REFERENCES [dbo].[faLenders] ([LenderId])
GO
ALTER TABLE [dbo].[faStudentAwards] ADD CONSTRAINT [FK_faStudentAwards_faLenders_LenderId_LenderId] FOREIGN KEY ([LenderId]) REFERENCES [dbo].[faLenders] ([LenderId])
GO
ALTER TABLE [dbo].[faStudentAwards] ADD CONSTRAINT [FK_faStudentAwards_faLenders_ServicerId_LenderId] FOREIGN KEY ([ServicerId]) REFERENCES [dbo].[faLenders] ([LenderId])
GO
ALTER TABLE [dbo].[faStudentAwards] ADD CONSTRAINT [FK_faStudentAwards_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY ([StuEnrollId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
