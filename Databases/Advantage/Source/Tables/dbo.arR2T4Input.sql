CREATE TABLE [dbo].[arR2T4Input]
(
[R2T4InputId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_arR2T4Input_R2T4InputId] DEFAULT (newsequentialid()),
[TerminationId] [uniqueidentifier] NOT NULL,
[ProgramUnitTypeId] [int] NOT NULL,
[PellGrantDisbursed] [money] NULL,
[PellGrantCouldDisbursed] [money] NULL,
[FSEOGDisbursed] [money] NULL,
[FSEOGCouldDisbursed] [money] NULL,
[TeachGrantDisbursed] [money] NULL,
[TeachGrantCouldDisbursed] [money] NULL,
[IraqAfgGrantDisbursed] [money] NULL,
[IraqAfgGrantCouldDisbursed] [money] NULL,
[UnsubLoanNetAmountDisbursed] [money] NULL,
[UnsubLoanNetAmountCouldDisbursed] [money] NULL,
[SubLoanNetAmountDisbursed] [money] NULL,
[SubLoanNetAmountCouldDisbursed] [money] NULL,
[PerkinsLoanDisbursed] [money] NULL,
[PerkinsLoanCouldDisbursed] [money] NULL,
[DirectGraduatePlusLoanDisbursed] [money] NULL,
[DirectGraduatePlusLoanCouldDisbursed] [money] NULL,
[DirectParentPlusLoanDisbursed] [money] NULL,
[DirectParentPlusLoanCouldDisbursed] [money] NULL,
[IsAttendanceNotRequired] [bit] NOT NULL CONSTRAINT [DF_arR2T4Input_IsAttendanceNotRequired] DEFAULT ((0)),
[StartDate] [datetime] NULL,
[ScheduledEndDate] [datetime] NULL,
[WithdrawalDate] [datetime] NULL,
[CompletedTime] [decimal] (18, 2) NULL,
[TotalTime] [decimal] (18, 2) NULL,
[TuitionFee] [money] NULL,
[RoomFee] [money] NULL,
[BoardFee] [money] NULL,
[OtherFee] [money] NULL,
[IsTuitionChargedByPaymentPeriod] [bit] NULL CONSTRAINT [DF_arR2T4Input_IsTuitionChargedByPaymentPeriod] DEFAULT ((0)),
[CreditBalanceRefunded] [money] NULL,
[CreatedById] [uniqueidentifier] NOT NULL,
[CreatedDate] [datetime] NULL CONSTRAINT [DF_arR2T4Input_CreatedDate] DEFAULT (getdate()),
[UpdatedById] [uniqueidentifier] NULL,
[UpdatedDate] [datetime] NULL,
[IsR2T4InputCompleted] [bit] NOT NULL CONSTRAINT [DF_arR2T4Input_IsNextClicked] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arR2T4Input] ADD CONSTRAINT [PK_arR2T4Input_R2T4InputId] PRIMARY KEY CLUSTERED  ([R2T4InputId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arR2T4Input] ADD CONSTRAINT [FK_arR2T4Input_arR2T4TerminationDetails_TerminationId_TerminationId] FOREIGN KEY ([TerminationId]) REFERENCES [dbo].[arR2T4TerminationDetails] ([TerminationId])
GO
ALTER TABLE [dbo].[arR2T4Input] ADD CONSTRAINT [FK_arR2T4Input_syUsers_CreatedBy_UserId] FOREIGN KEY ([CreatedById]) REFERENCES [dbo].[syUsers] ([UserId])
GO
ALTER TABLE [dbo].[arR2T4Input] ADD CONSTRAINT [FK_arR2T4Input_syUsers_UpdatedBy_UserId] FOREIGN KEY ([UpdatedById]) REFERENCES [dbo].[syUsers] ([UserId])
GO
