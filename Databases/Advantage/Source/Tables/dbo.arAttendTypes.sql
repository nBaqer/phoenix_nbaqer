CREATE TABLE [dbo].[arAttendTypes]
(
[AttendTypeId] [uniqueidentifier] NOT NULL,
[Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IPEDSSequence] [int] NULL,
[IPEDSValue] [int] NULL,
[GESequence] [int] NULL,
[GERptAgencyFldValId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arAttendTypes] ADD CONSTRAINT [PK_arAttendTypes_AttendTypeId] PRIMARY KEY CLUSTERED  ([AttendTypeId]) ON [PRIMARY]
GO
