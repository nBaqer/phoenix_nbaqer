CREATE TABLE [dbo].[syUserImpersonationLog]
(
[UserImpersonationLogId] [int] NOT NULL IDENTITY(1, 1),
[ImpersonatedUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LogStart] [datetime] NULL,
[LogEnd] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syUserImpersonationLog] ADD CONSTRAINT [PK_syUserImpersonationLog_UserImpersonationLogId] PRIMARY KEY CLUSTERED  ([UserImpersonationLogId]) ON [PRIMARY]
GO
