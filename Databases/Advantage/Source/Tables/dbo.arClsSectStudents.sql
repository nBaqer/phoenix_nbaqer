CREATE TABLE [dbo].[arClsSectStudents]
(
[ClsSectStudentId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arClsSectStudents_ClsSectStudentId] DEFAULT (newid()),
[ClsSectionId] [uniqueidentifier] NOT NULL,
[StudentId] [uniqueidentifier] NOT NULL,
[Grade] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arClsSectStudents] ADD CONSTRAINT [PK_arClsSectStudents_ClsSectStudentId] PRIMARY KEY CLUSTERED  ([ClsSectStudentId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arClsSectStudents] ADD CONSTRAINT [FK_arClsSectStudents_arClassSections_ClsSectionId_ClsSectionId] FOREIGN KEY ([ClsSectionId]) REFERENCES [dbo].[arClassSections] ([ClsSectionId])
GO
