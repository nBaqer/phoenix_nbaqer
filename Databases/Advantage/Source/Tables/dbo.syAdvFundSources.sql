CREATE TABLE [dbo].[syAdvFundSources]
(
[AdvFundSourceId] [tinyint] NOT NULL,
[Descrip] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syAdvFundSources] ADD CONSTRAINT [PK_syAdvFundSources_AdvFundSourceId] PRIMARY KEY CLUSTERED  ([AdvFundSourceId]) ON [PRIMARY]
GO
