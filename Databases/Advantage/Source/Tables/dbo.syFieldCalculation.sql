CREATE TABLE [dbo].[syFieldCalculation]
(
[syFieldCalculationId] [bigint] NOT NULL IDENTITY(1, 1),
[FldId] [int] NOT NULL,
[CalculationSql] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syFieldCalculation] ADD CONSTRAINT [PK_syFieldCalculation_syFieldCalculationId] PRIMARY KEY CLUSTERED  ([syFieldCalculationId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syFieldCalculation] ADD CONSTRAINT [FK_syFieldCalculation_syFields_FldId_FldId] FOREIGN KEY ([FldId]) REFERENCES [dbo].[syFields] ([FldId])
GO
