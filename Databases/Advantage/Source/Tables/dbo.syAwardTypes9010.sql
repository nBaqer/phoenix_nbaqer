CREATE TABLE [dbo].[syAwardTypes9010]
(
[AwardType9010Id] [uniqueidentifier] NOT NULL CONSTRAINT [DF_syAwardTypes9010_AwardType9010Id] DEFAULT (newsequentialid()),
[Name] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DisplayOrder] [int] NULL,
[StatusId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syAwardTypes9010] ADD CONSTRAINT [PK_syAwardTypes9010_AwardType9010Id] PRIMARY KEY CLUSTERED  ([AwardType9010Id]) ON [PRIMARY]
GO
