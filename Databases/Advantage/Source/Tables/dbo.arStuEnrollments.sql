CREATE TABLE [dbo].[arStuEnrollments]
(
[StuEnrollId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arStuEnrollments_StuEnrollId] DEFAULT (newid()),
[StudentId] [uniqueidentifier] NOT NULL,
[EnrollDate] [datetime] NOT NULL,
[PrgVerId] [uniqueidentifier] NOT NULL,
[StartDate] [datetime] NULL,
[ExpStartDate] [datetime] NULL,
[MidPtDate] [datetime] NULL,
[ExpGradDate] [datetime] NULL,
[TransferDate] [datetime] NULL,
[ShiftId] [uniqueidentifier] NULL,
[BillingMethodId] [uniqueidentifier] NULL,
[CampusId] [uniqueidentifier] NOT NULL,
[StatusCodeId] [uniqueidentifier] NULL,
[LDA] [datetime] NULL,
[ModDate] [datetime] NOT NULL CONSTRAINT [DF_arStuEnrollments_ModDate] DEFAULT (getdate()),
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnrollmentId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AdmissionsRep] [uniqueidentifier] NULL,
[AcademicAdvisor] [uniqueidentifier] NULL,
[LeadId] [uniqueidentifier] NULL,
[TuitionCategoryId] [uniqueidentifier] NULL,
[DropReasonId] [uniqueidentifier] NULL,
[DateDetermined] [datetime] NULL,
[EdLvlId] [uniqueidentifier] NULL,
[FAAdvisorId] [uniqueidentifier] NULL,
[attendtypeid] [uniqueidentifier] NULL,
[degcertseekingid] [uniqueidentifier] NULL,
[ReEnrollmentDate] [datetime] NULL,
[BadgeNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CohortStartDate] [datetime] NULL,
[SAPId] [uniqueidentifier] NULL CONSTRAINT [DF_arStuEnrollments_SAPId] DEFAULT (NULL),
[ContractedGradDate] [datetime] NULL,
[TransferHours] [decimal] (18, 2) NULL,
[graduatedorreceiveddate] [datetime] NULL,
[DistanceEdStatus] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrgVersionTypeId] [int] NOT NULL CONSTRAINT [DF_arStuEnrollments_PrgVersionTypeId] DEFAULT ((0)),
[IsDisabled] [bit] NULL,
[IsFirstTimeInSchool] [bit] NOT NULL CONSTRAINT [DF_arStuEnrollments_IsFirstTimeInSchool] DEFAULT ((0)),
[IsFirstTimePostSecSchool] [bit] NOT NULL CONSTRAINT [DF_arStuEnrollments_IsFirstTimePostSecSchool] DEFAULT ((0)),
[EntranceInterviewDate] [datetime] NULL,
[DisableAutoCharge] [bit] NOT NULL CONSTRAINT [DF_arStuEnrollments_DisableAutoCharge] DEFAULT ((0)),
[ThirdPartyContract] [bit] NOT NULL CONSTRAINT [DF_arStuEnrollments_ThirdPartyContract] DEFAULT ((0)),
[TransferHoursFromThisSchoolEnrollmentId] [uniqueidentifier] NULL,
[TotalTransferHoursFromThisSchool] [decimal] (18, 2) NOT NULL CONSTRAINT [DF_arStuEnrollments_TotalTransferHoursFromThisSchool] DEFAULT ((0)),
[LicensureLastPartWrittenOn] [datetime] NULL,
[LicensureWrittenAllParts] [bit] NOT NULL CONSTRAINT [DF_arStuEnrollments_LicensureWrittenAllParts] DEFAULT ((0)),
[LicensurePassedAllParts] [bit] NOT NULL CONSTRAINT [DF_arStuEnrollments_LicensurePassedAllParts] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

 
 
--========================================================================================== 
-- TRIGGER arStuEnrollments_Audit_Delete 
-- INSERT  add Audit History when delete records in arStuEnrollments 
--========================================================================================== 
CREATE TRIGGER [dbo].[arStuEnrollments_Audit_Delete] ON [dbo].[arStuEnrollments]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    BEGIN 
        SET NOCOUNT ON; 
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
        DECLARE @EventRows AS INT; 
        DECLARE @EventDate AS DATETIME; 
        DECLARE @UserName AS VARCHAR(50); 
	
        SET @AuditHistId = NEWID(); 
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Deleted
                         ); 
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Deleted
                         ); 
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Deleted
                        ); 
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'arStuEnrollments','D',@EventRows,@EventDate,@UserName; 
                -- StudentId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'StudentId'
                               ,CONVERT(VARCHAR(MAX),Old.StudentId)
                        FROM    Deleted AS Old; 
                -- EnrollDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'EnrollDate'
                               ,CONVERT(VARCHAR(MAX),Old.EnrollDate,121)
                        FROM    Deleted AS Old; 
                -- PrgVerId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'PrgVerId'
                               ,CONVERT(VARCHAR(MAX),Old.PrgVerId)
                        FROM    Deleted AS Old; 
                -- StartDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'StartDate'
                               ,CONVERT(VARCHAR(MAX),Old.StartDate,121)
                        FROM    Deleted AS Old; 
                -- ExpStartDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'ExpStartDate'
                               ,CONVERT(VARCHAR(MAX),Old.ExpStartDate,121)
                        FROM    Deleted AS Old; 
                -- MidPtDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'MidPtDate'
                               ,CONVERT(VARCHAR(MAX),Old.MidPtDate,121)
                        FROM    Deleted AS Old; 
                -- ExpGradDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'ExpGradDate'
                               ,CONVERT(VARCHAR(MAX),Old.ExpGradDate,121)
                        FROM    Deleted AS Old; 
                -- TransferDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'TransferDate'
                               ,CONVERT(VARCHAR(MAX),Old.TransferDate,121)
                        FROM    Deleted AS Old; 
                -- ShiftId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'ShiftId'
                               ,CONVERT(VARCHAR(MAX),Old.ShiftId)
                        FROM    Deleted AS Old; 
                -- BillingMethodId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'BillingMethodId'
                               ,CONVERT(VARCHAR(MAX),Old.BillingMethodId)
                        FROM    Deleted AS Old; 
                -- CampusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'CampusId'
                               ,CONVERT(VARCHAR(MAX),Old.CampusId)
                        FROM    Deleted AS Old; 
                -- StatusCodeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'StatusCodeId'
                               ,CONVERT(VARCHAR(MAX),Old.StatusCodeId)
                        FROM    Deleted AS Old; 
                -- LDA 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'LDA'
                               ,CONVERT(VARCHAR(MAX),Old.LDA,121)
                        FROM    Deleted AS Old; 
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                        FROM    Deleted AS Old; 
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),Old.ModUser)
                        FROM    Deleted AS Old; 
                -- EnrollmentId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'EnrollmentId'
                               ,CONVERT(VARCHAR(MAX),Old.EnrollmentId)
                        FROM    Deleted AS Old; 
                -- AdmissionsRep 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'AdmissionsRep'
                               ,CONVERT(VARCHAR(MAX),Old.AdmissionsRep)
                        FROM    Deleted AS Old; 
                -- AcademicAdvisor 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'AcademicAdvisor'
                               ,CONVERT(VARCHAR(MAX),Old.AcademicAdvisor)
                        FROM    Deleted AS Old; 
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),Old.LeadId)
                        FROM    Deleted AS Old; 
                -- TuitionCategoryId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'TuitionCategoryId'
                               ,CONVERT(VARCHAR(MAX),Old.TuitionCategoryId)
                        FROM    Deleted AS Old; 
                -- DropReasonId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'DropReasonId'
                               ,CONVERT(VARCHAR(MAX),Old.DropReasonId)
                        FROM    Deleted AS Old; 
                -- DateDetermined 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'DateDetermined'
                               ,CONVERT(VARCHAR(MAX),Old.DateDetermined,121)
                        FROM    Deleted AS Old; 
                -- EdLvlId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'EdLvlId'
                               ,CONVERT(VARCHAR(MAX),Old.EdLvlId)
                        FROM    Deleted AS Old; 
                -- FAAdvisorId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'FAAdvisorId'
                               ,CONVERT(VARCHAR(MAX),Old.FAAdvisorId)
                        FROM    Deleted AS Old; 
                -- attendtypeid 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'attendtypeid'
                               ,CONVERT(VARCHAR(MAX),Old.attendtypeid)
                        FROM    Deleted AS Old; 
                -- degcertseekingid 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'degcertseekingid'
                               ,CONVERT(VARCHAR(MAX),Old.degcertseekingid)
                        FROM    Deleted AS Old; 
                -- ReEnrollmentDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'ReEnrollmentDate'
                               ,CONVERT(VARCHAR(MAX),Old.ReEnrollmentDate,121)
                        FROM    Deleted AS Old; 
                -- BadgeNumber 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'BadgeNumber'
                               ,CONVERT(VARCHAR(MAX),Old.BadgeNumber)
                        FROM    Deleted AS Old; 
                -- CohortStartDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'CohortStartDate'
                               ,CONVERT(VARCHAR(MAX),Old.CohortStartDate,121)
                        FROM    Deleted AS Old; 
                -- SAPId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'SAPId'
                               ,CONVERT(VARCHAR(MAX),Old.SAPId)
                        FROM    Deleted AS Old; 
                -- ContractedGradDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'ContractedGradDate'
                               ,CONVERT(VARCHAR(MAX),Old.ContractedGradDate,121)
                        FROM    Deleted AS Old; 
                -- TransferHours 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'TransferHours'
                               ,CONVERT(VARCHAR(MAX),Old.TransferHours)
                        FROM    Deleted AS Old; 
                -- graduatedorreceiveddate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'graduatedorreceiveddate'
                               ,CONVERT(VARCHAR(MAX),Old.graduatedorreceiveddate,121)
                        FROM    Deleted AS Old; 
                -- DistanceEdStatus 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'DistanceEdStatus'
                               ,CONVERT(VARCHAR(MAX),Old.DistanceEdStatus)
                        FROM    Deleted AS Old; 
                -- PrgVersionTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'PrgVersionTypeId'
                               ,CONVERT(VARCHAR(MAX),Old.PrgVersionTypeId)
                        FROM    Deleted AS Old; 
                -- IsDisabled 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'IsDisabled'
                               ,CONVERT(VARCHAR(MAX),Old.IsDisabled)
                        FROM    Deleted AS Old; 
                -- IsFirstTimeInSchool 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'IsFirstTimeInSchool'
                               ,CONVERT(VARCHAR(MAX),Old.IsFirstTimeInSchool)
                        FROM    Deleted AS Old; 
                -- IsFirstTimePostSecSchool 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'IsFirstTimePostSecSchool'
                               ,CONVERT(VARCHAR(MAX),Old.IsFirstTimePostSecSchool)
                        FROM    Deleted AS Old; 
                -- EntranceInterviewDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StuEnrollId
                               ,'EntranceInterviewDate'
                               ,CONVERT(VARCHAR(MAX),Old.EntranceInterviewDate,121)
                        FROM    Deleted AS Old; 
            END; 
        SET NOCOUNT OFF; 
    END; 
--========================================================================================== 
-- END TRIGGER arStuEnrollments_Audit_Delete 
--========================================================================================== 
 

    SET NOCOUNT OFF; 

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

 
 
--========================================================================================== 
-- TRIGGER arStuEnrollments_Audit_Insert 
-- INSERT  add Audit History when insert records in arStuEnrollments 
--========================================================================================== 
CREATE TRIGGER [dbo].[arStuEnrollments_Audit_Insert] ON [dbo].[arStuEnrollments]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    BEGIN 
        SET NOCOUNT ON; 
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
        DECLARE @EventRows AS INT; 
        DECLARE @EventDate AS DATETIME; 
        DECLARE @UserName AS VARCHAR(50); 
	
        SET @AuditHistId = NEWID(); 
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         ); 
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         ); 
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        ); 
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'arStuEnrollments','I',@EventRows,@EventDate,@UserName; 
                -- StudentId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'StudentId'
                               ,CONVERT(VARCHAR(MAX),New.StudentId)
                        FROM    Inserted AS New; 
                -- EnrollDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'EnrollDate'
                               ,CONVERT(VARCHAR(MAX),New.EnrollDate,121)
                        FROM    Inserted AS New; 
                -- PrgVerId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'PrgVerId'
                               ,CONVERT(VARCHAR(MAX),New.PrgVerId)
                        FROM    Inserted AS New; 
                -- StartDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'StartDate'
                               ,CONVERT(VARCHAR(MAX),New.StartDate,121)
                        FROM    Inserted AS New; 
                -- ExpStartDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'ExpStartDate'
                               ,CONVERT(VARCHAR(MAX),New.ExpStartDate,121)
                        FROM    Inserted AS New; 
                -- MidPtDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'MidPtDate'
                               ,CONVERT(VARCHAR(MAX),New.MidPtDate,121)
                        FROM    Inserted AS New; 
                -- ExpGradDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'ExpGradDate'
                               ,CONVERT(VARCHAR(MAX),New.ExpGradDate,121)
                        FROM    Inserted AS New; 
                -- TransferDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'TransferDate'
                               ,CONVERT(VARCHAR(MAX),New.TransferDate,121)
                        FROM    Inserted AS New; 
                -- ShiftId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'ShiftId'
                               ,CONVERT(VARCHAR(MAX),New.ShiftId)
                        FROM    Inserted AS New; 
                -- BillingMethodId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'BillingMethodId'
                               ,CONVERT(VARCHAR(MAX),New.BillingMethodId)
                        FROM    Inserted AS New; 
                -- CampusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'CampusId'
                               ,CONVERT(VARCHAR(MAX),New.CampusId)
                        FROM    Inserted AS New; 
                -- StatusCodeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'StatusCodeId'
                               ,CONVERT(VARCHAR(MAX),New.StatusCodeId)
                        FROM    Inserted AS New; 
                -- LDA 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'LDA'
                               ,CONVERT(VARCHAR(MAX),New.LDA,121)
                        FROM    Inserted AS New; 
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                        FROM    Inserted AS New; 
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),New.ModUser)
                        FROM    Inserted AS New; 
                -- EnrollmentId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'EnrollmentId'
                               ,CONVERT(VARCHAR(MAX),New.EnrollmentId)
                        FROM    Inserted AS New; 
                -- AdmissionsRep 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'AdmissionsRep'
                               ,CONVERT(VARCHAR(MAX),New.AdmissionsRep)
                        FROM    Inserted AS New; 
                -- AcademicAdvisor 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'AcademicAdvisor'
                               ,CONVERT(VARCHAR(MAX),New.AcademicAdvisor)
                        FROM    Inserted AS New; 
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),New.LeadId)
                        FROM    Inserted AS New; 
                -- TuitionCategoryId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'TuitionCategoryId'
                               ,CONVERT(VARCHAR(MAX),New.TuitionCategoryId)
                        FROM    Inserted AS New; 
                -- DropReasonId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'DropReasonId'
                               ,CONVERT(VARCHAR(MAX),New.DropReasonId)
                        FROM    Inserted AS New; 
                -- DateDetermined 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'DateDetermined'
                               ,CONVERT(VARCHAR(MAX),New.DateDetermined,121)
                        FROM    Inserted AS New; 
                -- EdLvlId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'EdLvlId'
                               ,CONVERT(VARCHAR(MAX),New.EdLvlId)
                        FROM    Inserted AS New; 
                -- FAAdvisorId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'FAAdvisorId'
                               ,CONVERT(VARCHAR(MAX),New.FAAdvisorId)
                        FROM    Inserted AS New; 
                -- attendtypeid 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'attendtypeid'
                               ,CONVERT(VARCHAR(MAX),New.attendtypeid)
                        FROM    Inserted AS New; 
                -- degcertseekingid 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'degcertseekingid'
                               ,CONVERT(VARCHAR(MAX),New.degcertseekingid)
                        FROM    Inserted AS New; 
                -- ReEnrollmentDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'ReEnrollmentDate'
                               ,CONVERT(VARCHAR(MAX),New.ReEnrollmentDate,121)
                        FROM    Inserted AS New; 
                -- BadgeNumber 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'BadgeNumber'
                               ,CONVERT(VARCHAR(MAX),New.BadgeNumber)
                        FROM    Inserted AS New; 
                -- CohortStartDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'CohortStartDate'
                               ,CONVERT(VARCHAR(MAX),New.CohortStartDate,121)
                        FROM    Inserted AS New; 
                -- SAPId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'SAPId'
                               ,CONVERT(VARCHAR(MAX),New.SAPId)
                        FROM    Inserted AS New; 
                -- ContractedGradDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'ContractedGradDate'
                               ,CONVERT(VARCHAR(MAX),New.ContractedGradDate,121)
                        FROM    Inserted AS New; 
                -- TransferHours 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'TransferHours'
                               ,CONVERT(VARCHAR(MAX),New.TransferHours)
                        FROM    Inserted AS New; 
                -- graduatedorreceiveddate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'graduatedorreceiveddate'
                               ,CONVERT(VARCHAR(MAX),New.graduatedorreceiveddate,121)
                        FROM    Inserted AS New; 
                -- DistanceEdStatus 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'DistanceEdStatus'
                               ,CONVERT(VARCHAR(MAX),New.DistanceEdStatus)
                        FROM    Inserted AS New; 
                -- PrgVersionTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'PrgVersionTypeId'
                               ,CONVERT(VARCHAR(MAX),New.PrgVersionTypeId)
                        FROM    Inserted AS New; 
                -- IsDisabled 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'IsDisabled'
                               ,CONVERT(VARCHAR(MAX),New.IsDisabled)
                        FROM    Inserted AS New; 
                -- IsFirstTimeInSchool 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'IsFirstTimeInSchool'
                               ,CONVERT(VARCHAR(MAX),New.IsFirstTimeInSchool)
                        FROM    Inserted AS New; 
                -- IsFirstTimePostSecSchool 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'IsFirstTimePostSecSchool'
                               ,CONVERT(VARCHAR(MAX),New.IsFirstTimePostSecSchool)
                        FROM    Inserted AS New; 
                -- EntranceInterviewDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StuEnrollId
                               ,'EntranceInterviewDate'
                               ,CONVERT(VARCHAR(MAX),New.EntranceInterviewDate,121)
                        FROM    Inserted AS New; 
            END; 
        SET NOCOUNT OFF; 
    END; 
--========================================================================================== 
-- END TRIGGER arStuEnrollments_Audit_Insert 
--========================================================================================== 
 

    SET NOCOUNT OFF; 

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

 
--========================================================================================== 
-- TRIGGER arStuEnrollments_Audit_Update 
-- UPDATE  add Audit History when update fields in arStuEnrollments 
--========================================================================================== 
CREATE TRIGGER [dbo].[arStuEnrollments_Audit_Update] ON [dbo].[arStuEnrollments]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    BEGIN 
        SET NOCOUNT ON; 
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
        DECLARE @EventRows AS INT; 
        DECLARE @EventDate AS DATETIME; 
        DECLARE @UserName AS VARCHAR(50); 
	
        SET @AuditHistId = NEWID(); 
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         ); 
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         ); 
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        ); 
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'arStuEnrollments','U',@EventRows,@EventDate,@UserName; 
                -- StudentId 
                IF UPDATE(StudentId)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'StudentId'
                                       ,CONVERT(VARCHAR(MAX),Old.StudentId)
                                       ,CONVERT(VARCHAR(MAX),New.StudentId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.StudentId <> New.StudentId
                                        OR (
                                             Old.StudentId IS NULL
                                             AND New.StudentId IS NOT NULL
                                           )
                                        OR (
                                             New.StudentId IS NULL
                                             AND Old.StudentId IS NOT NULL
                                           ); 
                    END; 
                -- EnrollDate 
                IF UPDATE(EnrollDate)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'EnrollDate'
                                       ,CONVERT(VARCHAR(MAX),Old.EnrollDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.EnrollDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.EnrollDate <> New.EnrollDate
                                        OR (
                                             Old.EnrollDate IS NULL
                                             AND New.EnrollDate IS NOT NULL
                                           )
                                        OR (
                                             New.EnrollDate IS NULL
                                             AND Old.EnrollDate IS NOT NULL
                                           ); 
                    END; 
                -- PrgVerId 
                IF UPDATE(PrgVerId)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'PrgVerId'
                                       ,CONVERT(VARCHAR(MAX),Old.PrgVerId)
                                       ,CONVERT(VARCHAR(MAX),New.PrgVerId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.PrgVerId <> New.PrgVerId
                                        OR (
                                             Old.PrgVerId IS NULL
                                             AND New.PrgVerId IS NOT NULL
                                           )
                                        OR (
                                             New.PrgVerId IS NULL
                                             AND Old.PrgVerId IS NOT NULL
                                           ); 
                    END; 
                -- StartDate 
                IF UPDATE(StartDate)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'StartDate'
                                       ,CONVERT(VARCHAR(MAX),Old.StartDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.StartDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.StartDate <> New.StartDate
                                        OR (
                                             Old.StartDate IS NULL
                                             AND New.StartDate IS NOT NULL
                                           )
                                        OR (
                                             New.StartDate IS NULL
                                             AND Old.StartDate IS NOT NULL
                                           ); 
                    END; 
                -- ExpStartDate 
                IF UPDATE(ExpStartDate)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'ExpStartDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ExpStartDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ExpStartDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.ExpStartDate <> New.ExpStartDate
                                        OR (
                                             Old.ExpStartDate IS NULL
                                             AND New.ExpStartDate IS NOT NULL
                                           )
                                        OR (
                                             New.ExpStartDate IS NULL
                                             AND Old.ExpStartDate IS NOT NULL
                                           ); 
                    END; 
                -- MidPtDate 
                IF UPDATE(MidPtDate)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'MidPtDate'
                                       ,CONVERT(VARCHAR(MAX),Old.MidPtDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.MidPtDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.MidPtDate <> New.MidPtDate
                                        OR (
                                             Old.MidPtDate IS NULL
                                             AND New.MidPtDate IS NOT NULL
                                           )
                                        OR (
                                             New.MidPtDate IS NULL
                                             AND Old.MidPtDate IS NOT NULL
                                           ); 
                    END; 
                -- ExpGradDate 
                IF UPDATE(ExpGradDate)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'ExpGradDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ExpGradDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ExpGradDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.ExpGradDate <> New.ExpGradDate
                                        OR (
                                             Old.ExpGradDate IS NULL
                                             AND New.ExpGradDate IS NOT NULL
                                           )
                                        OR (
                                             New.ExpGradDate IS NULL
                                             AND Old.ExpGradDate IS NOT NULL
                                           ); 
                    END; 
                -- TransferDate 
                IF UPDATE(TransferDate)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'TransferDate'
                                       ,CONVERT(VARCHAR(MAX),Old.TransferDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.TransferDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.TransferDate <> New.TransferDate
                                        OR (
                                             Old.TransferDate IS NULL
                                             AND New.TransferDate IS NOT NULL
                                           )
                                        OR (
                                             New.TransferDate IS NULL
                                             AND Old.TransferDate IS NOT NULL
                                           ); 
                    END; 
                -- ShiftId 
                IF UPDATE(ShiftId)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'ShiftId'
                                       ,CONVERT(VARCHAR(MAX),Old.ShiftId)
                                       ,CONVERT(VARCHAR(MAX),New.ShiftId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.ShiftId <> New.ShiftId
                                        OR (
                                             Old.ShiftId IS NULL
                                             AND New.ShiftId IS NOT NULL
                                           )
                                        OR (
                                             New.ShiftId IS NULL
                                             AND Old.ShiftId IS NOT NULL
                                           ); 
                    END; 
                -- BillingMethodId 
                IF UPDATE(BillingMethodId)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'BillingMethodId'
                                       ,CONVERT(VARCHAR(MAX),Old.BillingMethodId)
                                       ,CONVERT(VARCHAR(MAX),New.BillingMethodId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.BillingMethodId <> New.BillingMethodId
                                        OR (
                                             Old.BillingMethodId IS NULL
                                             AND New.BillingMethodId IS NOT NULL
                                           )
                                        OR (
                                             New.BillingMethodId IS NULL
                                             AND Old.BillingMethodId IS NOT NULL
                                           ); 
                    END; 
                -- CampusId 
                IF UPDATE(CampusId)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'CampusId'
                                       ,CONVERT(VARCHAR(MAX),Old.CampusId)
                                       ,CONVERT(VARCHAR(MAX),New.CampusId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.CampusId <> New.CampusId
                                        OR (
                                             Old.CampusId IS NULL
                                             AND New.CampusId IS NOT NULL
                                           )
                                        OR (
                                             New.CampusId IS NULL
                                             AND Old.CampusId IS NOT NULL
                                           ); 
                    END; 
                -- StatusCodeId 
                IF UPDATE(StatusCodeId)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'StatusCodeId'
                                       ,CONVERT(VARCHAR(MAX),Old.StatusCodeId)
                                       ,CONVERT(VARCHAR(MAX),New.StatusCodeId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.StatusCodeId <> New.StatusCodeId
                                        OR (
                                             Old.StatusCodeId IS NULL
                                             AND New.StatusCodeId IS NOT NULL
                                           )
                                        OR (
                                             New.StatusCodeId IS NULL
                                             AND Old.StatusCodeId IS NOT NULL
                                           ); 
                    END; 
                -- LDA 
                IF UPDATE(Lda)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'LDA'
                                       ,CONVERT(VARCHAR(MAX),Old.LDA,121)
                                       ,CONVERT(VARCHAR(MAX),New.LDA,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.LDA <> New.LDA
                                        OR (
                                             Old.LDA IS NULL
                                             AND New.LDA IS NOT NULL
                                           )
                                        OR (
                                             New.LDA IS NULL
                                             AND Old.LDA IS NOT NULL
                                           ); 
                    END; 
                -- ModDate 
                IF UPDATE(ModDate)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'ModDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.ModDate <> New.ModDate
                                        OR (
                                             Old.ModDate IS NULL
                                             AND New.ModDate IS NOT NULL
                                           )
                                        OR (
                                             New.ModDate IS NULL
                                             AND Old.ModDate IS NOT NULL
                                           ); 
                    END; 
                -- ModUser 
                IF UPDATE(ModUser)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'ModUser'
                                       ,CONVERT(VARCHAR(MAX),Old.ModUser)
                                       ,CONVERT(VARCHAR(MAX),New.ModUser)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.ModUser <> New.ModUser
                                        OR (
                                             Old.ModUser IS NULL
                                             AND New.ModUser IS NOT NULL
                                           )
                                        OR (
                                             New.ModUser IS NULL
                                             AND Old.ModUser IS NOT NULL
                                           ); 
                    END; 
                -- EnrollmentId 
                IF UPDATE(EnrollmentId)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'EnrollmentId'
                                       ,CONVERT(VARCHAR(MAX),Old.EnrollmentId)
                                       ,CONVERT(VARCHAR(MAX),New.EnrollmentId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.EnrollmentId <> New.EnrollmentId
                                        OR (
                                             Old.EnrollmentId IS NULL
                                             AND New.EnrollmentId IS NOT NULL
                                           )
                                        OR (
                                             New.EnrollmentId IS NULL
                                             AND Old.EnrollmentId IS NOT NULL
                                           ); 
                    END; 
                -- AdmissionsRep 
                IF UPDATE(AdmissionsRep)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'AdmissionsRep'
                                       ,CONVERT(VARCHAR(MAX),Old.AdmissionsRep)
                                       ,CONVERT(VARCHAR(MAX),New.AdmissionsRep)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.AdmissionsRep <> New.AdmissionsRep
                                        OR (
                                             Old.AdmissionsRep IS NULL
                                             AND New.AdmissionsRep IS NOT NULL
                                           )
                                        OR (
                                             New.AdmissionsRep IS NULL
                                             AND Old.AdmissionsRep IS NOT NULL
                                           ); 
                    END; 
                -- AcademicAdvisor 
                IF UPDATE(AcademicAdvisor)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'AcademicAdvisor'
                                       ,CONVERT(VARCHAR(MAX),Old.AcademicAdvisor)
                                       ,CONVERT(VARCHAR(MAX),New.AcademicAdvisor)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.AcademicAdvisor <> New.AcademicAdvisor
                                        OR (
                                             Old.AcademicAdvisor IS NULL
                                             AND New.AcademicAdvisor IS NOT NULL
                                           )
                                        OR (
                                             New.AcademicAdvisor IS NULL
                                             AND Old.AcademicAdvisor IS NOT NULL
                                           ); 
                    END; 
                -- LeadId 
                IF UPDATE(LeadId)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'LeadId'
                                       ,CONVERT(VARCHAR(MAX),Old.LeadId)
                                       ,CONVERT(VARCHAR(MAX),New.LeadId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.LeadId <> New.LeadId
                                        OR (
                                             Old.LeadId IS NULL
                                             AND New.LeadId IS NOT NULL
                                           )
                                        OR (
                                             New.LeadId IS NULL
                                             AND Old.LeadId IS NOT NULL
                                           ); 
                    END; 
                -- TuitionCategoryId 
                IF UPDATE(TuitionCategoryId)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'TuitionCategoryId'
                                       ,CONVERT(VARCHAR(MAX),Old.TuitionCategoryId)
                                       ,CONVERT(VARCHAR(MAX),New.TuitionCategoryId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.TuitionCategoryId <> New.TuitionCategoryId
                                        OR (
                                             Old.TuitionCategoryId IS NULL
                                             AND New.TuitionCategoryId IS NOT NULL
                                           )
                                        OR (
                                             New.TuitionCategoryId IS NULL
                                             AND Old.TuitionCategoryId IS NOT NULL
                                           ); 
                    END; 
                -- DropReasonId 
                IF UPDATE(DropReasonId)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'DropReasonId'
                                       ,CONVERT(VARCHAR(MAX),Old.DropReasonId)
                                       ,CONVERT(VARCHAR(MAX),New.DropReasonId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.DropReasonId <> New.DropReasonId
                                        OR (
                                             Old.DropReasonId IS NULL
                                             AND New.DropReasonId IS NOT NULL
                                           )
                                        OR (
                                             New.DropReasonId IS NULL
                                             AND Old.DropReasonId IS NOT NULL
                                           ); 
                    END; 
                -- DateDetermined 
                IF UPDATE(DateDetermined)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'DateDetermined'
                                       ,CONVERT(VARCHAR(MAX),Old.DateDetermined,121)
                                       ,CONVERT(VARCHAR(MAX),New.DateDetermined,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.DateDetermined <> New.DateDetermined
                                        OR (
                                             Old.DateDetermined IS NULL
                                             AND New.DateDetermined IS NOT NULL
                                           )
                                        OR (
                                             New.DateDetermined IS NULL
                                             AND Old.DateDetermined IS NOT NULL
                                           ); 
                    END; 
                -- EdLvlId 
                IF UPDATE(EdLvlId)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'EdLvlId'
                                       ,CONVERT(VARCHAR(MAX),Old.EdLvlId)
                                       ,CONVERT(VARCHAR(MAX),New.EdLvlId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.EdLvlId <> New.EdLvlId
                                        OR (
                                             Old.EdLvlId IS NULL
                                             AND New.EdLvlId IS NOT NULL
                                           )
                                        OR (
                                             New.EdLvlId IS NULL
                                             AND Old.EdLvlId IS NOT NULL
                                           ); 
                    END; 
                -- FAAdvisorId 
                IF UPDATE(FAAdvisorId)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'FAAdvisorId'
                                       ,CONVERT(VARCHAR(MAX),Old.FAAdvisorId)
                                       ,CONVERT(VARCHAR(MAX),New.FAAdvisorId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.FAAdvisorId <> New.FAAdvisorId
                                        OR (
                                             Old.FAAdvisorId IS NULL
                                             AND New.FAAdvisorId IS NOT NULL
                                           )
                                        OR (
                                             New.FAAdvisorId IS NULL
                                             AND Old.FAAdvisorId IS NOT NULL
                                           ); 
                    END; 
                -- attendtypeid 
                IF UPDATE(AttendTypeId)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'attendtypeid'
                                       ,CONVERT(VARCHAR(MAX),Old.attendtypeid)
                                       ,CONVERT(VARCHAR(MAX),New.attendtypeid)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.attendtypeid <> New.attendtypeid
                                        OR (
                                             Old.attendtypeid IS NULL
                                             AND New.attendtypeid IS NOT NULL
                                           )
                                        OR (
                                             New.attendtypeid IS NULL
                                             AND Old.attendtypeid IS NOT NULL
                                           ); 
                    END; 
                -- degcertseekingid 
                IF UPDATE(DegCertSeekingId)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'degcertseekingid'
                                       ,CONVERT(VARCHAR(MAX),Old.degcertseekingid)
                                       ,CONVERT(VARCHAR(MAX),New.degcertseekingid)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.degcertseekingid <> New.degcertseekingid
                                        OR (
                                             Old.degcertseekingid IS NULL
                                             AND New.degcertseekingid IS NOT NULL
                                           )
                                        OR (
                                             New.degcertseekingid IS NULL
                                             AND Old.degcertseekingid IS NOT NULL
                                           ); 
                    END; 
                -- ReEnrollmentDate 
                IF UPDATE(ReEnrollmentDate)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'ReEnrollmentDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ReEnrollmentDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ReEnrollmentDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.ReEnrollmentDate <> New.ReEnrollmentDate
                                        OR (
                                             Old.ReEnrollmentDate IS NULL
                                             AND New.ReEnrollmentDate IS NOT NULL
                                           )
                                        OR (
                                             New.ReEnrollmentDate IS NULL
                                             AND Old.ReEnrollmentDate IS NOT NULL
                                           ); 
                    END; 
                -- BadgeNumber 
                IF UPDATE(BadgeNumber)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'BadgeNumber'
                                       ,CONVERT(VARCHAR(MAX),Old.BadgeNumber)
                                       ,CONVERT(VARCHAR(MAX),New.BadgeNumber)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.BadgeNumber <> New.BadgeNumber
                                        OR (
                                             Old.BadgeNumber IS NULL
                                             AND New.BadgeNumber IS NOT NULL
                                           )
                                        OR (
                                             New.BadgeNumber IS NULL
                                             AND Old.BadgeNumber IS NOT NULL
                                           ); 
                    END; 
                -- CohortStartDate 
                IF UPDATE(CohortStartDate)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'CohortStartDate'
                                       ,CONVERT(VARCHAR(MAX),Old.CohortStartDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.CohortStartDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.CohortStartDate <> New.CohortStartDate
                                        OR (
                                             Old.CohortStartDate IS NULL
                                             AND New.CohortStartDate IS NOT NULL
                                           )
                                        OR (
                                             New.CohortStartDate IS NULL
                                             AND Old.CohortStartDate IS NOT NULL
                                           ); 
                    END; 
                -- SAPId 
                IF UPDATE(SAPId)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'SAPId'
                                       ,CONVERT(VARCHAR(MAX),Old.SAPId)
                                       ,CONVERT(VARCHAR(MAX),New.SAPId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.SAPId <> New.SAPId
                                        OR (
                                             Old.SAPId IS NULL
                                             AND New.SAPId IS NOT NULL
                                           )
                                        OR (
                                             New.SAPId IS NULL
                                             AND Old.SAPId IS NOT NULL
                                           ); 
                    END; 
                -- ContractedGradDate 
                IF UPDATE(ContractedGradDate)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'ContractedGradDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ContractedGradDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ContractedGradDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.ContractedGradDate <> New.ContractedGradDate
                                        OR (
                                             Old.ContractedGradDate IS NULL
                                             AND New.ContractedGradDate IS NOT NULL
                                           )
                                        OR (
                                             New.ContractedGradDate IS NULL
                                             AND Old.ContractedGradDate IS NOT NULL
                                           ); 
                    END; 
                -- TransferHours 
                IF UPDATE(TransferHours)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'TransferHours'
                                       ,CONVERT(VARCHAR(MAX),Old.TransferHours)
                                       ,CONVERT(VARCHAR(MAX),New.TransferHours)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.TransferHours <> New.TransferHours
                                        OR (
                                             Old.TransferHours IS NULL
                                             AND New.TransferHours IS NOT NULL
                                           )
                                        OR (
                                             New.TransferHours IS NULL
                                             AND Old.TransferHours IS NOT NULL
                                           ); 
                    END; 
                -- graduatedorreceiveddate 
                IF UPDATE(graduatedorreceiveddate)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'graduatedorreceiveddate'
                                       ,CONVERT(VARCHAR(MAX),Old.graduatedorreceiveddate,121)
                                       ,CONVERT(VARCHAR(MAX),New.graduatedorreceiveddate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.graduatedorreceiveddate <> New.graduatedorreceiveddate
                                        OR (
                                             Old.graduatedorreceiveddate IS NULL
                                             AND New.graduatedorreceiveddate IS NOT NULL
                                           )
                                        OR (
                                             New.graduatedorreceiveddate IS NULL
                                             AND Old.graduatedorreceiveddate IS NOT NULL
                                           ); 
                    END; 
                -- DistanceEdStatus 
                IF UPDATE(DistanceEdStatus)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'DistanceEdStatus'
                                       ,CONVERT(VARCHAR(MAX),Old.DistanceEdStatus)
                                       ,CONVERT(VARCHAR(MAX),New.DistanceEdStatus)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.DistanceEdStatus <> New.DistanceEdStatus
                                        OR (
                                             Old.DistanceEdStatus IS NULL
                                             AND New.DistanceEdStatus IS NOT NULL
                                           )
                                        OR (
                                             New.DistanceEdStatus IS NULL
                                             AND Old.DistanceEdStatus IS NOT NULL
                                           ); 
                    END; 
                -- PrgVersionTypeId 
                IF UPDATE(PrgVersionTypeId)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'PrgVersionTypeId'
                                       ,CONVERT(VARCHAR(MAX),Old.PrgVersionTypeId)
                                       ,CONVERT(VARCHAR(MAX),New.PrgVersionTypeId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.PrgVersionTypeId <> New.PrgVersionTypeId
                                        OR (
                                             Old.PrgVersionTypeId IS NULL
                                             AND New.PrgVersionTypeId IS NOT NULL
                                           )
                                        OR (
                                             New.PrgVersionTypeId IS NULL
                                             AND Old.PrgVersionTypeId IS NOT NULL
                                           ); 
                    END; 
                -- IsDisabled 
                IF UPDATE(IsDisabled)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'IsDisabled'
                                       ,CONVERT(VARCHAR(MAX),Old.IsDisabled)
                                       ,CONVERT(VARCHAR(MAX),New.IsDisabled)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.IsDisabled <> New.IsDisabled
                                        OR (
                                             Old.IsDisabled IS NULL
                                             AND New.IsDisabled IS NOT NULL
                                           )
                                        OR (
                                             New.IsDisabled IS NULL
                                             AND Old.IsDisabled IS NOT NULL
                                           ); 
                    END; 
                -- IsFirstTimeInSchool 
                IF UPDATE(IsFirstTimeInSchool)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'IsFirstTimeInSchool'
                                       ,CONVERT(VARCHAR(MAX),Old.IsFirstTimeInSchool)
                                       ,CONVERT(VARCHAR(MAX),New.IsFirstTimeInSchool)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.IsFirstTimeInSchool <> New.IsFirstTimeInSchool
                                        OR (
                                             Old.IsFirstTimeInSchool IS NULL
                                             AND New.IsFirstTimeInSchool IS NOT NULL
                                           )
                                        OR (
                                             New.IsFirstTimeInSchool IS NULL
                                             AND Old.IsFirstTimeInSchool IS NOT NULL
                                           ); 
                    END; 
                -- IsFirstTimePostSecSchool 
                IF UPDATE(IsFirstTimePostSecSchool)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'IsFirstTimePostSecSchool'
                                       ,CONVERT(VARCHAR(MAX),Old.IsFirstTimePostSecSchool)
                                       ,CONVERT(VARCHAR(MAX),New.IsFirstTimePostSecSchool)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.IsFirstTimePostSecSchool <> New.IsFirstTimePostSecSchool
                                        OR (
                                             Old.IsFirstTimePostSecSchool IS NULL
                                             AND New.IsFirstTimePostSecSchool IS NOT NULL
                                           )
                                        OR (
                                             New.IsFirstTimePostSecSchool IS NULL
                                             AND Old.IsFirstTimePostSecSchool IS NOT NULL
                                           ); 
                    END; 
                -- EntranceInterviewDate 
                IF UPDATE(EntranceinterviewDate)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.StuEnrollId
                                       ,'EntranceInterviewDate'
                                       ,CONVERT(VARCHAR(MAX),Old.EntranceInterviewDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.EntranceInterviewDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StuEnrollId = New.StuEnrollId
                                WHERE   Old.EntranceInterviewDate <> New.EntranceInterviewDate
                                        OR (
                                             Old.EntranceInterviewDate IS NULL
                                             AND New.EntranceInterviewDate IS NOT NULL
                                           )
                                        OR (
                                             New.EntranceInterviewDate IS NULL
                                             AND Old.EntranceInterviewDate IS NOT NULL
                                           ); 
                    END; 
            END; 
        SET NOCOUNT OFF; 
    END; 
--========================================================================================== 
-- END TRIGGER arStuEnrollments_Audit_Update 
--========================================================================================== 
 

    SET NOCOUNT OFF; 

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--BEGIN	
--CREATE TABLE dbo.IntegrationEnrollmentTracking
--(
-- EnrollmentTrackingId UNIQUEIDENTIFIER CONSTRAINT [PK_IntegrationEnrollmentTracking_EnrollmentTrackingId] PRIMARY KEY CLUSTERED 
-- CONSTRAINT [DF_IntegrationEnrollmentTracking_EnrollmentTrackingId] DEFAULT NEWID(),
-- StuEnrollId UNIQUEIDENTIFIER CONSTRAINT[FK_IntegrationEnrollmentTracking_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY REFERENCES dbo.arStuEnrollments NOT NULL,
--  EnrollmentStatus VARCHAR(100),
-- ModDate DATETIME NOT NULL ,
-- ModUser NVARCHAR(100),
-- Processed BIT CONSTRAINT [DF_IntegrationEnrollmentTracking_Processed] DEFAULT 0 NOT NULL 	

--)
--END
--DROP TABLE dbo.IntegrationEnrollmentTracking
CREATE TRIGGER [dbo].[arStuEnrollments_Integration_Tracking]
ON [dbo].[arStuEnrollments]
AFTER UPDATE
AS
DECLARE @INS INT
       ,@DEL INT;

SELECT @INS = COUNT(*)
FROM   INSERTED;
SELECT @DEL = COUNT(*)
FROM   DELETED;

IF @INS > 0
   AND @DEL > 0
    BEGIN
        SET NOCOUNT ON;

        IF UPDATE(StatusCodeId)
           OR UPDATE(LDA)
            BEGIN
                IF EXISTS (
                          SELECT     1
                          FROM       INSERTED i
                          INNER JOIN DELETED d ON d.StuEnrollId = i.StuEnrollId
                          WHERE      ( i.StatusCodeId <> d.StatusCodeId )
                                     OR ( i.LDA <> d.LDA )
                          )
                    BEGIN
                        IF EXISTS (
                                  SELECT     1
                                  FROM       dbo.IntegrationEnrollmentTracking IET
                                  INNER JOIN inserted i ON i.StuEnrollId = IET.StuEnrollId
                                  )
                            BEGIN


                                UPDATE     IET
                                SET        EnrollmentStatus = ss.SysStatusDescrip
                                          ,ModDate = i.ModDate
                                          ,ModUser = i.ModUser
                                          ,Processed = 0
                                          ,RetryCount = 0
                                FROM       dbo.IntegrationEnrollmentTracking IET
                                INNER JOIN INSERTED i ON i.StuEnrollId = IET.StuEnrollId
                                INNER JOIN dbo.arStuEnrollments se ON i.StuEnrollId = se.StuEnrollId
                                INNER JOIN dbo.syStatusCodes sc ON sc.StatusCodeId = se.StatusCodeId
                                INNER JOIN dbo.sySysStatus ss ON ss.SysStatusId = sc.SysStatusId
                                INNER JOIN dbo.adLeads l ON l.LeadId = se.LeadId
                                WHERE      IET.StuEnrollId = i.StuEnrollId
                                           AND l.AfaStudentId IS NOT NULL;
                            END;


                        INSERT INTO dbo.IntegrationEnrollmentTracking (
                                                                      EnrollmentTrackingId
                                                                     ,StuEnrollId
                                                                     ,EnrollmentStatus
                                                                     ,ModDate
                                                                     ,ModUser
                                                                     ,Processed
                                                                      )
                                    SELECT     NEWID()
                                              ,i.StuEnrollId
                                              ,ss.SysStatusDescrip
                                              ,i.ModDate
                                              ,i.ModUser
                                              ,0
                                    FROM       INSERTED i
                                    INNER JOIN dbo.arStuEnrollments se ON i.StuEnrollId = se.StuEnrollId
                                    INNER JOIN dbo.syStatusCodes sc ON sc.StatusCodeId = se.StatusCodeId
                                    INNER JOIN dbo.sySysStatus ss ON ss.SysStatusId = sc.SysStatusId
                                    INNER JOIN dbo.adLeads l ON l.LeadId = se.LeadId
                                    LEFT JOIN  dbo.IntegrationEnrollmentTracking iet ON iet.StuEnrollId = se.StuEnrollId
                                    WHERE      l.AfaStudentId IS NOT NULL
                                               AND iet.EnrollmentTrackingId IS NULL;



                    END;

            END;

        SET NOCOUNT OFF;

    END;
GO
ALTER TABLE [dbo].[arStuEnrollments] ADD CONSTRAINT [PK_arStuEnrollments_StuEnrollId] PRIMARY KEY CLUSTERED  ([StuEnrollId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_arStuEnrollments_CampusId_ModDate_StudentId] ON [dbo].[arStuEnrollments] ([CampusId]) INCLUDE ([ModDate], [StudentId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_arStuEnrollments_PrgVerId_ShiftId_StatusCodeId_StudentId_StartDate] ON [dbo].[arStuEnrollments] ([PrgVerId], [ShiftId], [StatusCodeId], [StudentId]) INCLUDE ([StartDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_arStuEnrollments_ShiftId_StatusCodeId_StudentId_PrgVerId] ON [dbo].[arStuEnrollments] ([ShiftId], [StatusCodeId], [StudentId], [PrgVerId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_arStuEnrollments_StatusCodeId_StartDate_CampusId_EdLvlId_degcertseekingid_StudentId_PrgVerId_StuEnrollId_ExpGradDate] ON [dbo].[arStuEnrollments] ([StatusCodeId], [StartDate], [CampusId], [EdLvlId], [degcertseekingid], [StudentId], [PrgVerId], [StuEnrollId]) INCLUDE ([ExpGradDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_arStuEnrollments_StudentId_StatusCodeId_ShiftId_StuEnrollId_PrgVerId_StartDate] ON [dbo].[arStuEnrollments] ([StudentId], [StatusCodeId], [ShiftId], [StuEnrollId], [PrgVerId]) INCLUDE ([StartDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_arStuEnrollments_StuEnrollId_CampusId_PrgVerId_StudentId] ON [dbo].[arStuEnrollments] ([StuEnrollId]) INCLUDE ([CampusId], [PrgVerId], [StudentId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arStuEnrollments] ADD CONSTRAINT [FK_arStuEnrollments_adDegCertSeeking_degcertseekingid_DegCertSeekingId] FOREIGN KEY ([degcertseekingid]) REFERENCES [dbo].[adDegCertSeeking] ([DegCertSeekingId])
GO
ALTER TABLE [dbo].[arStuEnrollments] ADD CONSTRAINT [FK_arStuEnrollments_adEdLvls_EdLvlId_EdLvlId] FOREIGN KEY ([EdLvlId]) REFERENCES [dbo].[adEdLvls] ([EdLvlId])
GO
ALTER TABLE [dbo].[arStuEnrollments] ADD CONSTRAINT [FK_arStuEnrollments_adLeads_LeadId_LeadId] FOREIGN KEY ([LeadId]) REFERENCES [dbo].[adLeads] ([LeadId])
GO
ALTER TABLE [dbo].[arStuEnrollments] ADD CONSTRAINT [FK_arStuEnrollments_arAttendTypes_attendtypeid_AttendTypeId] FOREIGN KEY ([attendtypeid]) REFERENCES [dbo].[arAttendTypes] ([AttendTypeId])
GO
ALTER TABLE [dbo].[arStuEnrollments] ADD CONSTRAINT [FK_arStuEnrollments_arDropReasons_DropReasonId_DropReasonId] FOREIGN KEY ([DropReasonId]) REFERENCES [dbo].[arDropReasons] ([DropReasonId])
GO
ALTER TABLE [dbo].[arStuEnrollments] ADD CONSTRAINT [FK_arStuEnrollments_arPrgVersions_PrgVerId_PrgVerId] FOREIGN KEY ([PrgVerId]) REFERENCES [dbo].[arPrgVersions] ([PrgVerId])
GO
ALTER TABLE [dbo].[arStuEnrollments] ADD CONSTRAINT [FK_arStuEnrollments_arPrograms_TransferHoursFromThisSchoolEnrollmentId_stuEnrollmentId] FOREIGN KEY ([TransferHoursFromThisSchoolEnrollmentId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
ALTER TABLE [dbo].[arStuEnrollments] ADD CONSTRAINT [FK_arStuEnrollments_arProgramVersionType_PrgVersionTypeId_ProgramVersionTypeId] FOREIGN KEY ([PrgVersionTypeId]) REFERENCES [dbo].[arProgramVersionType] ([ProgramVersionTypeId])
GO
ALTER TABLE [dbo].[arStuEnrollments] ADD CONSTRAINT [FK_arStuEnrollments_arSAP_SAPId_SAPId] FOREIGN KEY ([SAPId]) REFERENCES [dbo].[arSAP] ([SAPId])
GO
ALTER TABLE [dbo].[arStuEnrollments] ADD CONSTRAINT [FK_arStuEnrollments_arShifts_ShiftId_ShiftId] FOREIGN KEY ([ShiftId]) REFERENCES [dbo].[arShifts] ([ShiftId])
GO
ALTER TABLE [dbo].[arStuEnrollments] ADD CONSTRAINT [FK_arStuEnrollments_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY ([StuEnrollId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
ALTER TABLE [dbo].[arStuEnrollments] ADD CONSTRAINT [FK_arStuEnrollments_saBillingMethods_BillingMethodId_BillingMethodId] FOREIGN KEY ([BillingMethodId]) REFERENCES [dbo].[saBillingMethods] ([BillingMethodId])
GO
ALTER TABLE [dbo].[arStuEnrollments] ADD CONSTRAINT [FK_arStuEnrollments_saTuitionCategories_TuitionCategoryId_TuitionCategoryId] FOREIGN KEY ([TuitionCategoryId]) REFERENCES [dbo].[saTuitionCategories] ([TuitionCategoryId])
GO
ALTER TABLE [dbo].[arStuEnrollments] ADD CONSTRAINT [FK_arStuEnrollments_syCampuses_CampusId_CampusId] FOREIGN KEY ([CampusId]) REFERENCES [dbo].[syCampuses] ([CampusId])
GO
ALTER TABLE [dbo].[arStuEnrollments] ADD CONSTRAINT [FK_arStuEnrollments_syStatusCodes_StatusCodeId_StatusCodeId] FOREIGN KEY ([StatusCodeId]) REFERENCES [dbo].[syStatusCodes] ([StatusCodeId])
GO
ALTER TABLE [dbo].[arStuEnrollments] ADD CONSTRAINT [FK_arStuEnrollments_syUsers_AcademicAdvisor_UserId] FOREIGN KEY ([AcademicAdvisor]) REFERENCES [dbo].[syUsers] ([UserId])
GO
ALTER TABLE [dbo].[arStuEnrollments] ADD CONSTRAINT [FK_arStuEnrollments_syUsers_AdmissionsRep_UserId] FOREIGN KEY ([AdmissionsRep]) REFERENCES [dbo].[syUsers] ([UserId])
GO
ALTER TABLE [dbo].[arStuEnrollments] ADD CONSTRAINT [FK_arStuEnrollments_syUsers_FAAdvisorId_UserId] FOREIGN KEY ([FAAdvisorId]) REFERENCES [dbo].[syUsers] ([UserId])
GO
