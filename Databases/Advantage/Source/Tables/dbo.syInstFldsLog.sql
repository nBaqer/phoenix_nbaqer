CREATE TABLE [dbo].[syInstFldsLog]
(
[InstId] [int] NOT NULL,
[FldId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syInstFldsLog] ADD CONSTRAINT [PK_syInstFldsLog_InstId_FldId] PRIMARY KEY CLUSTERED  ([InstId], [FldId]) ON [PRIMARY]
GO
GRANT REFERENCES ON  [dbo].[syInstFldsLog] TO [AdvantageRole]
GRANT SELECT ON  [dbo].[syInstFldsLog] TO [AdvantageRole]
GRANT INSERT ON  [dbo].[syInstFldsLog] TO [AdvantageRole]
GRANT DELETE ON  [dbo].[syInstFldsLog] TO [AdvantageRole]
GRANT UPDATE ON  [dbo].[syInstFldsLog] TO [AdvantageRole]
GO
