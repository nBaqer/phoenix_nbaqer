CREATE TABLE [dbo].[adImportLeadsMappings]
(
[MappingId] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[MapName] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adImportLeadsMappings] ADD CONSTRAINT [PK_adImportLeadsMappings_MappingId] PRIMARY KEY CLUSTERED  ([MappingId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adImportLeadsMappings] ADD CONSTRAINT [FK_adImportLeadsMappings_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[adImportLeadsMappings] ADD CONSTRAINT [FK_adImportLeadsMappings_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
