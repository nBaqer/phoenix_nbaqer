CREATE TABLE [dbo].[syFldCaptions]
(
[FldCapId] [int] NOT NULL,
[FldId] [int] NOT NULL,
[LangId] [tinyint] NOT NULL,
[Caption] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FldDescrip] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syFldCaptions] ADD CONSTRAINT [PK_syFldCaptions_FldCapId] PRIMARY KEY CLUSTERED  ([FldCapId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syFldCaptions] ADD CONSTRAINT [FK_syFldCaptions_syFields_FldId_FldId] FOREIGN KEY ([FldId]) REFERENCES [dbo].[syFields] ([FldId])
GO
ALTER TABLE [dbo].[syFldCaptions] ADD CONSTRAINT [FK_syFldCaptions_syLangs_LangId_LangId] FOREIGN KEY ([LangId]) REFERENCES [dbo].[syLangs] ([LangId]) ON DELETE CASCADE ON UPDATE CASCADE
GO
GRANT SELECT ON  [dbo].[syFldCaptions] TO [AdvantageRole]
GO
