CREATE TABLE [dbo].[syModuleDef]
(
[ModDefId] [smallint] NOT NULL,
[ModuleId] [tinyint] NOT NULL,
[ChildId] [smallint] NOT NULL,
[ChildType] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Sequence] [smallint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syModuleDef] ADD CONSTRAINT [PK_syModuleDef_ModDefId] PRIMARY KEY CLUSTERED  ([ModDefId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syModuleDef] ADD CONSTRAINT [FK_syModuleDef_syModules_ModuleId_ModuleID] FOREIGN KEY ([ModuleId]) REFERENCES [dbo].[syModules] ([ModuleID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
GRANT SELECT ON  [dbo].[syModuleDef] TO [AdvantageRole]
GO
