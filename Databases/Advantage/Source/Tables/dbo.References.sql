CREATE TABLE [dbo].[References]
(
[StudentContactId] [uniqueidentifier] NULL ROWGUIDCOL CONSTRAINT [DF_References_StudentContactId] DEFAULT (newid()),
[StudentId] [uniqueidentifier] NULL,
[SSN] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProspectID] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Address] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[City] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[State] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Zip] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Country] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneNo] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Relationship] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ForEmergency] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ReferenceId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_References_ReferenceId] DEFAULT (newsequentialid())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[References] ADD CONSTRAINT [PK_References_ReferenceId] PRIMARY KEY CLUSTERED  ([ReferenceId]) ON [PRIMARY]
GO
