CREATE TABLE [dbo].[syHolidays]
(
[HolidayId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syHolidays_HolidayId] DEFAULT (newid()),
[HolidayCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[HolidayDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[HolidayStartDate] [datetime] NOT NULL,
[AllDay] [bit] NOT NULL,
[StartTimeId] [uniqueidentifier] NULL,
[EndTimeId] [uniqueidentifier] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[HolidayEndDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[syHolidays_Audit_Delete] ON [dbo].[syHolidays]
    FOR DELETE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syHolidays','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.HolidayId
                               ,'HolidayCode'
                               ,CONVERT(VARCHAR(8000),Old.HolidayCode,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.HolidayId
                               ,'HolidayDescrip'
                               ,CONVERT(VARCHAR(8000),Old.HolidayDescrip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.HolidayId
                               ,'HolidayStartDate'
                               ,CONVERT(VARCHAR(8000),Old.HolidayStartDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.HolidayId
                               ,'HolidayEndDate'
                               ,CONVERT(VARCHAR(8000),Old.HolidayEndDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.HolidayId
                               ,'AllDay'
                               ,CONVERT(VARCHAR(8000),Old.AllDay,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.HolidayId
                               ,'StartTimeId'
                               ,CONVERT(VARCHAR(8000),Old.StartTimeId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.HolidayId
                               ,'EndTimeId'
                               ,CONVERT(VARCHAR(8000),Old.EndTimeId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.HolidayId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.HolidayId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
            END; 
        END;



    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE TRIGGER [dbo].[syHolidays_Audit_Insert] ON [dbo].[syHolidays]
    FOR INSERT
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syHolidays','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.HolidayId
                               ,'HolidayCode'
                               ,CONVERT(VARCHAR(8000),New.HolidayCode,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.HolidayId
                               ,'HolidayDescrip'
                               ,CONVERT(VARCHAR(8000),New.HolidayDescrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.HolidayId
                               ,'HolidayStartDate'
                               ,CONVERT(VARCHAR(8000),New.HolidayStartDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.HolidayId
                               ,'HolidayEndDate'
                               ,CONVERT(VARCHAR(8000),New.HolidayEndDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.HolidayId
                               ,'AllDay'
                               ,CONVERT(VARCHAR(8000),New.AllDay,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.HolidayId
                               ,'StartTimeId'
                               ,CONVERT(VARCHAR(8000),New.StartTimeId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.HolidayId
                               ,'EndTimeId'
                               ,CONVERT(VARCHAR(8000),New.EndTimeId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.HolidayId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.HolidayId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New; 
            END; 
        END;



    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE TRIGGER [dbo].[syHolidays_Audit_Update] ON [dbo].[syHolidays]
    FOR UPDATE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syHolidays','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(HolidayCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.HolidayId
                                   ,'HolidayCode'
                                   ,CONVERT(VARCHAR(8000),Old.HolidayCode,121)
                                   ,CONVERT(VARCHAR(8000),New.HolidayCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.HolidayId = New.HolidayId
                            WHERE   Old.HolidayCode <> New.HolidayCode
                                    OR (
                                         Old.HolidayCode IS NULL
                                         AND New.HolidayCode IS NOT NULL
                                       )
                                    OR (
                                         New.HolidayCode IS NULL
                                         AND Old.HolidayCode IS NOT NULL
                                       ); 
                IF UPDATE(HolidayDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.HolidayId
                                   ,'HolidayDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.HolidayDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.HolidayDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.HolidayId = New.HolidayId
                            WHERE   Old.HolidayDescrip <> New.HolidayDescrip
                                    OR (
                                         Old.HolidayDescrip IS NULL
                                         AND New.HolidayDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.HolidayDescrip IS NULL
                                         AND Old.HolidayDescrip IS NOT NULL
                                       ); 
                IF UPDATE(HolidayStartDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.HolidayId
                                   ,'HolidayStartDate'
                                   ,CONVERT(VARCHAR(8000),Old.HolidayStartDate,121)
                                   ,CONVERT(VARCHAR(8000),New.HolidayStartDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.HolidayId = New.HolidayId
                            WHERE   Old.HolidayStartDate <> New.HolidayStartDate
                                    OR (
                                         Old.HolidayStartDate IS NULL
                                         AND New.HolidayStartDate IS NOT NULL
                                       )
                                    OR (
                                         New.HolidayStartDate IS NULL
                                         AND Old.HolidayStartDate IS NOT NULL
                                       ); 
                IF UPDATE(HolidayEndDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.HolidayId
                                   ,'HolidayEndDate'
                                   ,CONVERT(VARCHAR(8000),Old.HolidayEndDate,121)
                                   ,CONVERT(VARCHAR(8000),New.HolidayEndDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.HolidayId = New.HolidayId
                            WHERE   Old.HolidayEndDate <> New.HolidayEndDate
                                    OR (
                                         Old.HolidayEndDate IS NULL
                                         AND New.HolidayEndDate IS NOT NULL
                                       )
                                    OR (
                                         New.HolidayEndDate IS NULL
                                         AND Old.HolidayEndDate IS NOT NULL
                                       ); 
                IF UPDATE(AllDay)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.HolidayId
                                   ,'AllDay'
                                   ,CONVERT(VARCHAR(8000),Old.AllDay,121)
                                   ,CONVERT(VARCHAR(8000),New.AllDay,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.HolidayId = New.HolidayId
                            WHERE   Old.AllDay <> New.AllDay
                                    OR (
                                         Old.AllDay IS NULL
                                         AND New.AllDay IS NOT NULL
                                       )
                                    OR (
                                         New.AllDay IS NULL
                                         AND Old.AllDay IS NOT NULL
                                       ); 
                IF UPDATE(StartTimeId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.HolidayId
                                   ,'StartTimeId'
                                   ,CONVERT(VARCHAR(8000),Old.StartTimeId,121)
                                   ,CONVERT(VARCHAR(8000),New.StartTimeId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.HolidayId = New.HolidayId
                            WHERE   Old.StartTimeId <> New.StartTimeId
                                    OR (
                                         Old.StartTimeId IS NULL
                                         AND New.StartTimeId IS NOT NULL
                                       )
                                    OR (
                                         New.StartTimeId IS NULL
                                         AND Old.StartTimeId IS NOT NULL
                                       ); 
                IF UPDATE(EndTimeId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.HolidayId
                                   ,'EndTimeId'
                                   ,CONVERT(VARCHAR(8000),Old.EndTimeId,121)
                                   ,CONVERT(VARCHAR(8000),New.EndTimeId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.HolidayId = New.HolidayId
                            WHERE   Old.EndTimeId <> New.EndTimeId
                                    OR (
                                         Old.EndTimeId IS NULL
                                         AND New.EndTimeId IS NOT NULL
                                       )
                                    OR (
                                         New.EndTimeId IS NULL
                                         AND Old.EndTimeId IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.HolidayId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.HolidayId = New.HolidayId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.HolidayId
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.HolidayId = New.HolidayId
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 
            END; 
        END;



    SET NOCOUNT OFF;

GO
ALTER TABLE [dbo].[syHolidays] ADD CONSTRAINT [PK_syHolidays_HolidayId] PRIMARY KEY CLUSTERED  ([HolidayId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_syHolidays_StatusId_HolidayStartDate_CampGrpId] ON [dbo].[syHolidays] ([StatusId], [HolidayStartDate], [CampGrpId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syHolidays] ADD CONSTRAINT [FK_syHolidays_cmTimeInterval_EndTimeId_TimeIntervalId] FOREIGN KEY ([EndTimeId]) REFERENCES [dbo].[cmTimeInterval] ([TimeIntervalId])
GO
ALTER TABLE [dbo].[syHolidays] ADD CONSTRAINT [FK_syHolidays_cmTimeInterval_StartTimeId_TimeIntervalId] FOREIGN KEY ([StartTimeId]) REFERENCES [dbo].[cmTimeInterval] ([TimeIntervalId])
GO
ALTER TABLE [dbo].[syHolidays] ADD CONSTRAINT [FK_syHolidays_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[syHolidays] ADD CONSTRAINT [FK_syHolidays_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
