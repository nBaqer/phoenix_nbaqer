CREATE TABLE [dbo].[saDeferredRevenues]
(
[DefRevenueId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_saDeferredRevenues_DefRevenueId] DEFAULT (newid()),
[DefRevenueDate] [datetime] NOT NULL,
[TransactionId] [uniqueidentifier] NOT NULL,
[Type] [bit] NOT NULL CONSTRAINT [DF_saDeferredRevenues_Type] DEFAULT ((0)),
[Source] [tinyint] NOT NULL CONSTRAINT [DF_saDeferredRevenues_Source] DEFAULT ((0)),
[Amount] [money] NOT NULL CONSTRAINT [DF_saDeferredRevenues_Amount] DEFAULT ((0.00)),
[IsPosted] [bit] NOT NULL CONSTRAINT [DF_saDeferredRevenues_IsPosted] DEFAULT ((0)),
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saDeferredRevenues_Audit_Delete] ON [dbo].[saDeferredRevenues]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saDeferredRevenues','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.DefRevenueId
                               ,'Amount'
                               ,CONVERT(VARCHAR(8000),Old.Amount,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.DefRevenueId
                               ,'TransactionId'
                               ,CONVERT(VARCHAR(8000),Old.TransactionId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.DefRevenueId
                               ,'DefRevenueDate'
                               ,CONVERT(VARCHAR(8000),Old.DefRevenueDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.DefRevenueId
                               ,'Source'
                               ,CONVERT(VARCHAR(8000),Old.Source,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.DefRevenueId
                               ,'Type'
                               ,CONVERT(VARCHAR(8000),Old.Type,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.DefRevenueId
                               ,'IsPosted'
                               ,CONVERT(VARCHAR(8000),Old.IsPosted,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saDeferredRevenues_Audit_Insert] ON [dbo].[saDeferredRevenues]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saDeferredRevenues','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.DefRevenueId
                               ,'Amount'
                               ,CONVERT(VARCHAR(8000),New.Amount,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.DefRevenueId
                               ,'TransactionId'
                               ,CONVERT(VARCHAR(8000),New.TransactionId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.DefRevenueId
                               ,'DefRevenueDate'
                               ,CONVERT(VARCHAR(8000),New.DefRevenueDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.DefRevenueId
                               ,'Source'
                               ,CONVERT(VARCHAR(8000),New.Source,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.DefRevenueId
                               ,'Type'
                               ,CONVERT(VARCHAR(8000),New.Type,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.DefRevenueId
                               ,'IsPosted'
                               ,CONVERT(VARCHAR(8000),New.IsPosted,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO
GO

GO

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saDeferredRevenues_Audit_Update] ON [dbo].[saDeferredRevenues]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saDeferredRevenues','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(Amount)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.DefRevenueId
                                   ,'Amount'
                                   ,CONVERT(VARCHAR(8000),Old.Amount,121)
                                   ,CONVERT(VARCHAR(8000),New.Amount,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.DefRevenueId = New.DefRevenueId
                            WHERE   Old.Amount <> New.Amount
                                    OR (
                                         Old.Amount IS NULL
                                         AND New.Amount IS NOT NULL
                                       )
                                    OR (
                                         New.Amount IS NULL
                                         AND Old.Amount IS NOT NULL
                                       ); 
                IF UPDATE(TransactionId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.DefRevenueId
                                   ,'TransactionId'
                                   ,CONVERT(VARCHAR(8000),Old.TransactionId,121)
                                   ,CONVERT(VARCHAR(8000),New.TransactionId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.DefRevenueId = New.DefRevenueId
                            WHERE   Old.TransactionId <> New.TransactionId
                                    OR (
                                         Old.TransactionId IS NULL
                                         AND New.TransactionId IS NOT NULL
                                       )
                                    OR (
                                         New.TransactionId IS NULL
                                         AND Old.TransactionId IS NOT NULL
                                       ); 
                IF UPDATE(DefRevenueDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.DefRevenueId
                                   ,'DefRevenueDate'
                                   ,CONVERT(VARCHAR(8000),Old.DefRevenueDate,121)
                                   ,CONVERT(VARCHAR(8000),New.DefRevenueDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.DefRevenueId = New.DefRevenueId
                            WHERE   Old.DefRevenueDate <> New.DefRevenueDate
                                    OR (
                                         Old.DefRevenueDate IS NULL
                                         AND New.DefRevenueDate IS NOT NULL
                                       )
                                    OR (
                                         New.DefRevenueDate IS NULL
                                         AND Old.DefRevenueDate IS NOT NULL
                                       ); 
                IF UPDATE(Source)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.DefRevenueId
                                   ,'Source'
                                   ,CONVERT(VARCHAR(8000),Old.Source,121)
                                   ,CONVERT(VARCHAR(8000),New.Source,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.DefRevenueId = New.DefRevenueId
                            WHERE   Old.Source <> New.Source
                                    OR (
                                         Old.Source IS NULL
                                         AND New.Source IS NOT NULL
                                       )
                                    OR (
                                         New.Source IS NULL
                                         AND Old.Source IS NOT NULL
                                       ); 
                IF UPDATE(Type)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.DefRevenueId
                                   ,'Type'
                                   ,CONVERT(VARCHAR(8000),Old.Type,121)
                                   ,CONVERT(VARCHAR(8000),New.Type,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.DefRevenueId = New.DefRevenueId
                            WHERE   Old.Type <> New.Type
                                    OR (
                                         Old.Type IS NULL
                                         AND New.Type IS NOT NULL
                                       )
                                    OR (
                                         New.Type IS NULL
                                         AND Old.Type IS NOT NULL
                                       ); 
                IF UPDATE(IsPosted)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.DefRevenueId
                                   ,'IsPosted'
                                   ,CONVERT(VARCHAR(8000),Old.IsPosted,121)
                                   ,CONVERT(VARCHAR(8000),New.IsPosted,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.DefRevenueId = New.DefRevenueId
                            WHERE   Old.IsPosted <> New.IsPosted
                                    OR (
                                         Old.IsPosted IS NULL
                                         AND New.IsPosted IS NOT NULL
                                       )
                                    OR (
                                         New.IsPosted IS NULL
                                         AND Old.IsPosted IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[saDeferredRevenues] ADD CONSTRAINT [PK_saDeferredRevenues_DefRevenueId] PRIMARY KEY CLUSTERED  ([DefRevenueId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_saDeferredRevenues_TransactionId] ON [dbo].[saDeferredRevenues] ([TransactionId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saDeferredRevenues] ADD CONSTRAINT [FK_saDeferredRevenues_saTransactions_TransactionId_TransactionId] FOREIGN KEY ([TransactionId]) REFERENCES [dbo].[saTransactions] ([TransactionId]) ON DELETE CASCADE
GO
