CREATE TABLE [dbo].[syCompOps]
(
[CompOpId] [tinyint] NOT NULL,
[CompOPText] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AllowMultiple] [bit] NULL CONSTRAINT [DF_syCompOps_AllowMultiple] DEFAULT ((0)),
[AllowDateTime] [bit] NOT NULL,
[AllowString] [bit] NOT NULL,
[AllowNumber] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syCompOps] ADD CONSTRAINT [PK_syCompOps_CompOpId] PRIMARY KEY CLUSTERED  ([CompOpId]) ON [PRIMARY]
GO
