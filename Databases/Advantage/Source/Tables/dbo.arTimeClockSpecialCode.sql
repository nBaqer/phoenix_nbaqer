CREATE TABLE [dbo].[arTimeClockSpecialCode]
(
[TCSId] [uniqueidentifier] NOT NULL,
[TCSpecialCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TCSPunchType] [smallint] NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[CampusId] [uniqueidentifier] NOT NULL,
[InstructionTypeId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arTimeClockSpecialCode] ADD CONSTRAINT [PK_arTimeClockSpecialCode_TCSId] PRIMARY KEY CLUSTERED  ([TCSId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arTimeClockSpecialCode] ADD CONSTRAINT [FK_arTimeClockSpecialCode_syCampuses_CampusId_CampusId] FOREIGN KEY ([CampusId]) REFERENCES [dbo].[syCampuses] ([CampusId])
GO
ALTER TABLE [dbo].[arTimeClockSpecialCode] ADD CONSTRAINT [FK_arTimeClockSpecialCode_arInstructionType_InstructionTypeId_InstructionTypeId] FOREIGN KEY ([InstructionTypeId]) REFERENCES [dbo].[arInstructionType] ([InstructionTypeId])
GO
