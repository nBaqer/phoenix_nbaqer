CREATE TABLE [dbo].[tmCategories]
(
[CategoryId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_tmCategories_CategoryId] DEFAULT (newid()),
[Code] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CampGroupId] [uniqueidentifier] NULL,
[ModDate] [datetime] NULL,
[ModUser] [uniqueidentifier] NULL,
[Active] [tinyint] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tmCategories] ADD CONSTRAINT [PK_tmCategories_CategoryId] PRIMARY KEY CLUSTERED  ([CategoryId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_tmCategories_Code_Descrip_CampGroupId] ON [dbo].[tmCategories] ([Code], [Descrip], [CampGroupId]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'Link to the campus group', 'SCHEMA', N'dbo', 'TABLE', N'tmCategories', 'COLUMN', N'CampGroupId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Text category', 'SCHEMA', N'dbo', 'TABLE', N'tmCategories', 'COLUMN', N'Descrip'
GO
