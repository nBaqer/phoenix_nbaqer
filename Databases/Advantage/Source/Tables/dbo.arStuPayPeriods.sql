CREATE TABLE [dbo].[arStuPayPeriods]
(
[StuEnrollPayPeriodID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_arStuPayPeriods_StuEnrollPayPeriodID] DEFAULT (newid()),
[StudentEnrollmentID] [uniqueidentifier] NOT NULL,
[PaymentPeriod] [int] NOT NULL,
[PaymentPeriodRange] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcademicCalendarType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PeriodStartDate] [datetime] NOT NULL,
[PeriodEndDate] [datetime] NOT NULL,
[AcaYrLen] [int] NOT NULL,
[ProgLength] [decimal] (18, 2) NOT NULL,
[PeriodLength] [decimal] (18, 2) NULL,
[PayPeriodsPerAcYr] [int] NOT NULL,
[Comments] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NOT NULL CONSTRAINT [DF_arStuPayPeriods_ModDate] DEFAULT (getdate()),
[AcademicYear] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arStuPayPeriods] ADD CONSTRAINT [PK_arStuPayPeriods_StudentEnrollmentID_PaymentPeriod_PeriodStartDate_PeriodEndDate_ProgLength] PRIMARY KEY CLUSTERED  ([StudentEnrollmentID], [PaymentPeriod], [PeriodStartDate], [PeriodEndDate], [ProgLength]) ON [PRIMARY]
GO
