CREATE TABLE [dbo].[arMentor_GradeComponentTypes_Courses]
(
[MentorProctoredId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_arMentor_GradeComponentTypes_Courses_MentorProctoredId] DEFAULT (newid()),
[ReqId] [uniqueidentifier] NULL,
[EffectiveDate] [datetime] NULL,
[MentorRequirement] [varchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MentorOperator] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GrdComponentTypeId] [uniqueidentifier] NULL,
[Speed] [int] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[OperatorSequence] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arMentor_GradeComponentTypes_Courses] ADD CONSTRAINT [PK_arMentor_GradeComponentTypes_Courses_MentorProctoredId] PRIMARY KEY CLUSTERED  ([MentorProctoredId]) ON [PRIMARY]
GO
