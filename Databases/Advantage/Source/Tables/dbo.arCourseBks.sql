CREATE TABLE [dbo].[arCourseBks]
(
[CourseBkId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arCourseBks_CourseBkId] DEFAULT (newid()),
[BkId] [uniqueidentifier] NOT NULL,
[IsReq] [bit] NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[CourseId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arCourseBks] ADD CONSTRAINT [PK_arCourseBks_CourseBkId] PRIMARY KEY CLUSTERED  ([CourseBkId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arCourseBks] ADD CONSTRAINT [FK_arCourseBks_arBooks_BkId_BkId] FOREIGN KEY ([BkId]) REFERENCES [dbo].[arBooks] ([BkId])
GO
ALTER TABLE [dbo].[arCourseBks] ADD CONSTRAINT [FK_arCourseBks_arReqs_CourseId_ReqId] FOREIGN KEY ([CourseId]) REFERENCES [dbo].[arReqs] ([ReqId])
GO
