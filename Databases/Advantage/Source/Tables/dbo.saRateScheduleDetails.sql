CREATE TABLE [dbo].[saRateScheduleDetails]
(
[RateScheduleDetailId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_saRateScheduleDetails_RateScheduleDetailId] DEFAULT (newid()),
[RateScheduleId] [uniqueidentifier] NOT NULL,
[MinUnits] [smallint] NOT NULL CONSTRAINT [DF_saRateScheduleDetails_MinUnits] DEFAULT ((0)),
[MaxUnits] [smallint] NOT NULL CONSTRAINT [DF_saRateScheduleDetails_MaxUnits] DEFAULT ((32767)),
[TuitionCategoryId] [uniqueidentifier] NULL,
[FlatAmount] [money] NULL,
[Rate] [money] NULL,
[UnitId] [smallint] NULL,
[ViewOrder] [int] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saRateScheduleDetails] ADD CONSTRAINT [PK_saRateScheduleDetails_RateScheduleDetailId] PRIMARY KEY CLUSTERED  ([RateScheduleDetailId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_saRateScheduleDetails_RateScheduleId_MaxUnits_TuitionCategoryId] ON [dbo].[saRateScheduleDetails] ([RateScheduleId], [MaxUnits], [TuitionCategoryId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_saRateScheduleDetails_RateScheduleId_MinUnits_TuitionCategoryId] ON [dbo].[saRateScheduleDetails] ([RateScheduleId], [MinUnits], [TuitionCategoryId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saRateScheduleDetails] ADD CONSTRAINT [FK_saRateScheduleDetails_saRateSchedules_RateScheduleId_RateScheduleId] FOREIGN KEY ([RateScheduleId]) REFERENCES [dbo].[saRateSchedules] ([RateScheduleId])
GO
ALTER TABLE [dbo].[saRateScheduleDetails] ADD CONSTRAINT [FK_saRateScheduleDetails_saTuitionCategories_TuitionCategoryId_TuitionCategoryId] FOREIGN KEY ([TuitionCategoryId]) REFERENCES [dbo].[saTuitionCategories] ([TuitionCategoryId])
GO
