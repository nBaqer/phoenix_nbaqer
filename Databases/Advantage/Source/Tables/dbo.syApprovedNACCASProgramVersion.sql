CREATE TABLE [dbo].[syApprovedNACCASProgramVersion]
(
[IsApproved] [bit] NOT NULL,
[ProgramVersionId] [uniqueidentifier] NULL,
[CreatedById] [uniqueidentifier] NULL,
[CreateDate] [datetime2] NULL,
[NaccasSettingId] [int] NOT NULL,
[ApprovedNACCASProgramVersionId] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syApprovedNACCASProgramVersion] ADD CONSTRAINT [PK_syApprovedNACCASProgramVersion_ApprovedNACCASProgramVersionId] PRIMARY KEY CLUSTERED  ([ApprovedNACCASProgramVersionId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syApprovedNACCASProgramVersion] ADD CONSTRAINT [FK_syApprovedNACCASProgramVersion_arPrgVersions_PrgVerId_PrgVerId] FOREIGN KEY ([ProgramVersionId]) REFERENCES [dbo].[arPrgVersions] ([PrgVerId])
GO
ALTER TABLE [dbo].[syApprovedNACCASProgramVersion] ADD CONSTRAINT [FK_syApprovedNACCASProgramVersion_syNaccasSettings_NaccasSettingId_NaccasSettingId] FOREIGN KEY ([NaccasSettingId]) REFERENCES [dbo].[syNaccasSettings] ([NaccasSettingId])
GO
ALTER TABLE [dbo].[syApprovedNACCASProgramVersion] ADD CONSTRAINT [FK_syApprovedNACCASProgramVersion_syUsers_CreatedById_UserId] FOREIGN KEY ([CreatedById]) REFERENCES [dbo].[syUsers] ([UserId])
GO
