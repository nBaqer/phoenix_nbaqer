CREATE TABLE [dbo].[arSDateSetup]
(
[SDateSetupId] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[SDateSetupCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[ShiftId] [uniqueidentifier] NOT NULL,
[SDate] [datetime] NOT NULL,
[MidPtDate] [datetime] NULL,
[ExpGradDate] [datetime] NOT NULL,
[MaxGradDate] [datetime] NOT NULL,
[SDateSetupDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[BudStarts] [int] NOT NULL,
[MaxStarts] [int] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CampGrpId] [uniqueidentifier] NULL,
[ProgId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[arSDateSetup_Audit_Delete] ON [dbo].[arSDateSetup]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arSDateSetup','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SDateSetupId
                               ,'SDateSetupCode'
                               ,CONVERT(VARCHAR(8000),Old.SDateSetupCode,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SDateSetupId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SDateSetupId
                               ,'ShiftId'
                               ,CONVERT(VARCHAR(8000),Old.ShiftId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SDateSetupId
                               ,'SDate'
                               ,CONVERT(VARCHAR(8000),Old.SDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SDateSetupId
                               ,'MidPtDate'
                               ,CONVERT(VARCHAR(8000),Old.MidPtDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SDateSetupId
                               ,'ExpGradDate'
                               ,CONVERT(VARCHAR(8000),Old.ExpGradDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SDateSetupId
                               ,'MaxGradDate'
                               ,CONVERT(VARCHAR(8000),Old.MaxGradDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SDateSetupId
                               ,'ProgId'
                               ,CONVERT(VARCHAR(8000),Old.ProgId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SDateSetupId
                               ,'SDateSetupDescrip'
                               ,CONVERT(VARCHAR(8000),Old.SDateSetupDescrip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SDateSetupId
                               ,'BudStarts'
                               ,CONVERT(VARCHAR(8000),Old.BudStarts,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SDateSetupId
                               ,'MaxStarts'
                               ,CONVERT(VARCHAR(8000),Old.MaxStarts,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.SDateSetupId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--Add new triggers
CREATE TRIGGER [dbo].[arSDateSetup_Audit_Insert] ON [dbo].[arSDateSetup]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arSDateSetup','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SDateSetupId
                               ,'SDateSetupCode'
                               ,CONVERT(VARCHAR(8000),New.SDateSetupCode,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SDateSetupId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SDateSetupId
                               ,'ShiftId'
                               ,CONVERT(VARCHAR(8000),New.ShiftId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SDateSetupId
                               ,'SDate'
                               ,CONVERT(VARCHAR(8000),New.SDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SDateSetupId
                               ,'MidPtDate'
                               ,CONVERT(VARCHAR(8000),New.MidPtDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SDateSetupId
                               ,'ExpGradDate'
                               ,CONVERT(VARCHAR(8000),New.ExpGradDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SDateSetupId
                               ,'MaxGradDate'
                               ,CONVERT(VARCHAR(8000),New.MaxGradDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SDateSetupId
                               ,'ProgId'
                               ,CONVERT(VARCHAR(8000),New.ProgId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SDateSetupId
                               ,'SDateSetupDescrip'
                               ,CONVERT(VARCHAR(8000),New.SDateSetupDescrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SDateSetupId
                               ,'BudStarts'
                               ,CONVERT(VARCHAR(8000),New.BudStarts,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SDateSetupId
                               ,'MaxStarts'
                               ,CONVERT(VARCHAR(8000),New.MaxStarts,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.SDateSetupId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New; 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO
GO

GO

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[arSDateSetup_Audit_Update] ON [dbo].[arSDateSetup]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arSDateSetup','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(SDateSetupCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SDateSetupId
                                   ,'SDateSetupCode'
                                   ,CONVERT(VARCHAR(8000),Old.SDateSetupCode,121)
                                   ,CONVERT(VARCHAR(8000),New.SDateSetupCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SDateSetupId = New.SDateSetupId
                            WHERE   Old.SDateSetupCode <> New.SDateSetupCode
                                    OR (
                                         Old.SDateSetupCode IS NULL
                                         AND New.SDateSetupCode IS NOT NULL
                                       )
                                    OR (
                                         New.SDateSetupCode IS NULL
                                         AND Old.SDateSetupCode IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SDateSetupId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SDateSetupId = New.SDateSetupId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(ShiftId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SDateSetupId
                                   ,'ShiftId'
                                   ,CONVERT(VARCHAR(8000),Old.ShiftId,121)
                                   ,CONVERT(VARCHAR(8000),New.ShiftId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SDateSetupId = New.SDateSetupId
                            WHERE   Old.ShiftId <> New.ShiftId
                                    OR (
                                         Old.ShiftId IS NULL
                                         AND New.ShiftId IS NOT NULL
                                       )
                                    OR (
                                         New.ShiftId IS NULL
                                         AND Old.ShiftId IS NOT NULL
                                       ); 
                IF UPDATE(SDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SDateSetupId
                                   ,'SDate'
                                   ,CONVERT(VARCHAR(8000),Old.SDate,121)
                                   ,CONVERT(VARCHAR(8000),New.SDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SDateSetupId = New.SDateSetupId
                            WHERE   Old.SDate <> New.SDate
                                    OR (
                                         Old.SDate IS NULL
                                         AND New.SDate IS NOT NULL
                                       )
                                    OR (
                                         New.SDate IS NULL
                                         AND Old.SDate IS NOT NULL
                                       ); 
                IF UPDATE(MidPtDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SDateSetupId
                                   ,'MidPtDate'
                                   ,CONVERT(VARCHAR(8000),Old.MidPtDate,121)
                                   ,CONVERT(VARCHAR(8000),New.MidPtDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SDateSetupId = New.SDateSetupId
                            WHERE   Old.MidPtDate <> New.MidPtDate
                                    OR (
                                         Old.MidPtDate IS NULL
                                         AND New.MidPtDate IS NOT NULL
                                       )
                                    OR (
                                         New.MidPtDate IS NULL
                                         AND Old.MidPtDate IS NOT NULL
                                       ); 
                IF UPDATE(ExpGradDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SDateSetupId
                                   ,'ExpGradDate'
                                   ,CONVERT(VARCHAR(8000),Old.ExpGradDate,121)
                                   ,CONVERT(VARCHAR(8000),New.ExpGradDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SDateSetupId = New.SDateSetupId
                            WHERE   Old.ExpGradDate <> New.ExpGradDate
                                    OR (
                                         Old.ExpGradDate IS NULL
                                         AND New.ExpGradDate IS NOT NULL
                                       )
                                    OR (
                                         New.ExpGradDate IS NULL
                                         AND Old.ExpGradDate IS NOT NULL
                                       ); 
                IF UPDATE(MaxGradDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SDateSetupId
                                   ,'MaxGradDate'
                                   ,CONVERT(VARCHAR(8000),Old.MaxGradDate,121)
                                   ,CONVERT(VARCHAR(8000),New.MaxGradDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SDateSetupId = New.SDateSetupId
                            WHERE   Old.MaxGradDate <> New.MaxGradDate
                                    OR (
                                         Old.MaxGradDate IS NULL
                                         AND New.MaxGradDate IS NOT NULL
                                       )
                                    OR (
                                         New.MaxGradDate IS NULL
                                         AND Old.MaxGradDate IS NOT NULL
                                       ); 
                IF UPDATE(ProgId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SDateSetupId
                                   ,'ProgId'
                                   ,CONVERT(VARCHAR(8000),Old.ProgId,121)
                                   ,CONVERT(VARCHAR(8000),New.ProgId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SDateSetupId = New.SDateSetupId
                            WHERE   Old.ProgId <> New.ProgId
                                    OR (
                                         Old.ProgId IS NULL
                                         AND New.ProgId IS NOT NULL
                                       )
                                    OR (
                                         New.ProgId IS NULL
                                         AND Old.ProgId IS NOT NULL
                                       ); 
                IF UPDATE(SDateSetupDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SDateSetupId
                                   ,'SDateSetupDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.SDateSetupDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.SDateSetupDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SDateSetupId = New.SDateSetupId
                            WHERE   Old.SDateSetupDescrip <> New.SDateSetupDescrip
                                    OR (
                                         Old.SDateSetupDescrip IS NULL
                                         AND New.SDateSetupDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.SDateSetupDescrip IS NULL
                                         AND Old.SDateSetupDescrip IS NOT NULL
                                       ); 
                IF UPDATE(BudStarts)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SDateSetupId
                                   ,'BudStarts'
                                   ,CONVERT(VARCHAR(8000),Old.BudStarts,121)
                                   ,CONVERT(VARCHAR(8000),New.BudStarts,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SDateSetupId = New.SDateSetupId
                            WHERE   Old.BudStarts <> New.BudStarts
                                    OR (
                                         Old.BudStarts IS NULL
                                         AND New.BudStarts IS NOT NULL
                                       )
                                    OR (
                                         New.BudStarts IS NULL
                                         AND Old.BudStarts IS NOT NULL
                                       ); 
                IF UPDATE(MaxStarts)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SDateSetupId
                                   ,'MaxStarts'
                                   ,CONVERT(VARCHAR(8000),Old.MaxStarts,121)
                                   ,CONVERT(VARCHAR(8000),New.MaxStarts,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SDateSetupId = New.SDateSetupId
                            WHERE   Old.MaxStarts <> New.MaxStarts
                                    OR (
                                         Old.MaxStarts IS NULL
                                         AND New.MaxStarts IS NOT NULL
                                       )
                                    OR (
                                         New.MaxStarts IS NULL
                                         AND Old.MaxStarts IS NOT NULL
                                       ); 
                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.SDateSetupId
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.SDateSetupId = New.SDateSetupId
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO
GO
ALTER TABLE [dbo].[arSDateSetup] ADD CONSTRAINT [PK_arSDateSetup_SDateSetupId] PRIMARY KEY CLUSTERED  ([SDateSetupId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arSDateSetup] ADD CONSTRAINT [FK_arSDateSetup_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[arSDateSetup] ADD CONSTRAINT [FK_arSDateSetup_arPrograms_ProgId_ProgId] FOREIGN KEY ([ProgId]) REFERENCES [dbo].[arPrograms] ([ProgId])
GO
ALTER TABLE [dbo].[arSDateSetup] ADD CONSTRAINT [FK_arSDateSetup_arShifts_ShiftId_ShiftId] FOREIGN KEY ([ShiftId]) REFERENCES [dbo].[arShifts] ([ShiftId])
GO
ALTER TABLE [dbo].[arSDateSetup] ADD CONSTRAINT [FK_arSDateSetup_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
