CREATE TABLE [dbo].[saFeeLevels]
(
[FeeLevelId] [tinyint] NOT NULL,
[Description] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saFeeLevels] ADD CONSTRAINT [PK_saFeeLevels_FeeLevelId] PRIMARY KEY CLUSTERED  ([FeeLevelId]) ON [PRIMARY]
GO
