CREATE TABLE [dbo].[sySchoolLogo]
(
[ImgId] [int] NOT NULL,
[Image] [image] NULL,
[ImgLen] [bigint] NULL,
[ImgFile] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ContentType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OfficialUse] [bit] NOT NULL CONSTRAINT [DF_sySchoolLogo_OfficialUse] DEFAULT ((0)),
[ImageCode] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[sySchoolLogo] ADD CONSTRAINT [PK_sySchoolLogo_ImgId] PRIMARY KEY CLUSTERED  ([ImgId]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[sySchoolLogo] TO [AdvantageRole]
GO
