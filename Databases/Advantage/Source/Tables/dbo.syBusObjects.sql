CREATE TABLE [dbo].[syBusObjects]
(
[BusObjectId] [smallint] NOT NULL,
[Descrip] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syBusObjects] ADD CONSTRAINT [PK_syBusObjects_BusObjectId] PRIMARY KEY CLUSTERED  ([BusObjectId]) ON [PRIMARY]
GO
GRANT REFERENCES ON  [dbo].[syBusObjects] TO [AdvantageRole]
GRANT SELECT ON  [dbo].[syBusObjects] TO [AdvantageRole]
GRANT INSERT ON  [dbo].[syBusObjects] TO [AdvantageRole]
GRANT DELETE ON  [dbo].[syBusObjects] TO [AdvantageRole]
GRANT UPDATE ON  [dbo].[syBusObjects] TO [AdvantageRole]
GO
