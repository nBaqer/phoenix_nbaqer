CREATE TABLE [dbo].[atClsSectAttendance]
(
[ClsSectAttId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_atClsSectAttendance_ClsSectAttId] DEFAULT (newid()),
[StudentId] [uniqueidentifier] NULL,
[ClsSectionId] [uniqueidentifier] NOT NULL,
[ClsSectMeetingId] [uniqueidentifier] NULL,
[MeetDate] [datetime] NOT NULL,
[Actual] [decimal] (18, 0) NOT NULL,
[Tardy] [bit] NOT NULL CONSTRAINT [DF_atClsSectAttendance_Tardy] DEFAULT ((0)),
[Excused] [bit] NOT NULL CONSTRAINT [DF_atClsSectAttendance_Excused] DEFAULT ((0)),
[Comments] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StuEnrollId] [uniqueidentifier] NULL,
[Scheduled] [decimal] (18, 0) NULL,
[ModDate] [datetime] NOT NULL CONSTRAINT [DF_atClsSectAttendance_ModDate] DEFAULT (getdate()),
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_atClsSectAttendance_ModUser] DEFAULT ('')
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--==========================================================================================
-- TRIGGER TR_InsertAttendanceSummary_ByClass_Minutes
-- AFTER INSERT, UPDATE
--==========================================================================================
-----------------------------------------------------------------------------
-- Alter trigger [TR_InsertAttendanceSummary_ByClass_Minutes] ON [dbo].[atClsSectAttendance]
-- Add SET NOCOUNT ON and SET NOCOUNT OFF to compatibility with Hibernate.
-- JAGG Begin -- 3/4/2014
-----------------------------------------------------------------------------
/****** Object:  Trigger [dbo].[TR_InsertAttendanceSummary_ByClass_Minutes]    Script Date: 3/14/2014 2:02:43 PM ******/
--------------------------------------------------------------------------------------
--End - Progress Report Fix for AMC, MAU, Chattanooga and NWH
--modified by Balaji G on 10/24/12
---------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
--Start - Progress Report Fix for NWH and MTI Clock Hour
--modified by Balaji G on 10/25/12
---------------------------------------------------------------------------------------
CREATE TRIGGER [dbo].[TR_InsertAttendanceSummary_ByClass_Minutes] ON [dbo].[atClsSectAttendance]
    AFTER INSERT,UPDATE
AS
    BEGIN 
        SET NOCOUNT ON; 

        DECLARE @StuEnrollId UNIQUEIDENTIFIER
           ,@MeetDate DATETIME
           ,@WeekDay VARCHAR(15)
           ,@StartDate DATETIME
           ,@EndDate DATETIME;
        DECLARE @PeriodDescrip VARCHAR(50)
           ,@Actual DECIMAL(18,2)
           ,@Excused DECIMAL(18,2)
           ,@ClsSectionId UNIQUEIDENTIFIER;
        DECLARE @Absent DECIMAL(18,2)
           ,@ScheduledMinutes DECIMAL(18,2)
           ,@TardyMinutes DECIMAL(18,2);
        DECLARE @tardy DECIMAL(18,2)
           ,@tracktardies INT
           ,@tardiesMakingAbsence INT
           ,@PrgVerId UNIQUEIDENTIFIER
           ,@rownumber INT;
        DECLARE @ActualRunningPresentHours DECIMAL(18,2)
           ,@ActualRunningAbsentHours DECIMAL(18,2)
           ,@ActualRunningTardyHours DECIMAL(18,2)
           ,@ActualRunningMakeupHours DECIMAL(18,2);
        DECLARE @PrevStuEnrollId UNIQUEIDENTIFIER
           ,@intTardyBreakPoint INT
           ,@AdjustedRunningPresentHours DECIMAL(18,2)
           ,@AdjustedRunningAbsentHours DECIMAL(18,2)
           ,@ActualRunningScheduledDays DECIMAL(18,2);
        DECLARE @AdjustedPresentDaysComputed DECIMAL(18,2)
           ,@MakeupHours DECIMAL(18,2);
        DECLARE @boolReset BIT;
        DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD
        FOR
            SELECT  *
                   ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
            FROM    (
                      SELECT DISTINCT
                                t1.StuEnrollId
                               ,t1.ClsSectionId
                               ,t1.MeetDate
                               ,DATENAME(dw,t1.MeetDate) AS WeekDay
                               ,t4.StartDate
                               ,t4.EndDate
                               ,t5.PeriodDescrip
                               ,t1.Actual
                               ,t1.Excused
                               ,CASE WHEN (
                                            t1.Actual = 0
                                            AND t1.Excused = 0
                                          ) THEN t1.Scheduled
                                     ELSE CASE WHEN (
                                                      t1.Actual <> 9999.00
                                                      AND t1.Actual < t1.Scheduled
                                                    ) THEN ( t1.Scheduled - t1.Actual )
                                               ELSE 0
                                          END
                                END AS Absent
                               ,t1.Scheduled AS ScheduledMinutes
                               ,CASE WHEN (
                                            t1.Actual > 0
                                            AND t1.Actual < t1.Scheduled
                                          ) THEN ( t1.Scheduled - t1.Actual )
                                     ELSE 0
                                END AS TardyMinutes
                               ,t1.Tardy AS Tardy
                               ,t3.TrackTardies
                               ,t3.TardiesMakingAbsence
                               ,t3.PrgVerId
                      FROM      atClsSectAttendance t1
                      INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                      INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                      INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                         AND (
                                                               CONVERT(DATE,t1.MeetDate,111) >= CONVERT(DATE,t4.StartDate,111)
                                                               AND CONVERT(DATE,t1.MeetDate,111) <= CONVERT(DATE,t4.EndDate,111)
                                                             )
                                                         AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                      INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                      INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                      INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                      INNER JOIN arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                      INNER JOIN arReqs t10 ON t10.ReqId = t9.ReqId
                      WHERE     t2.StuEnrollId IN ( SELECT DISTINCT
                                                            StuEnrollId
                                                    FROM    inserted )
                                AND (
                                      t10.UnitTypeId IN ( 'A1389C74-0BB9-4BBF-A47F-68428BE7FA4D','B937C92E-FD7A-455E-A731-527A9918C734' )
                                      OR t3.UnitTypeId IN ( 'A1389C74-0BB9-4BBF-A47F-68428BE7FA4D','B937C92E-FD7A-455E-A731-527A9918C734' )
                                    ) -- Minutes
                                AND t1.Actual <> 9999
                    ) dt
            ORDER BY StuEnrollId
                   ,MeetDate;
        OPEN GetAttendance_Cursor;
        FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,@PeriodDescrip,@Actual,@Excused,@Absent,
            @ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,@tardiesMakingAbsence,@PrgVerId,@rownumber;
        SET @ActualRunningPresentHours = 0;
        SET @ActualRunningPresentHours = 0;
        SET @ActualRunningAbsentHours = 0;
        SET @ActualRunningTardyHours = 0;
        SET @ActualRunningMakeupHours = 0;
        SET @intTardyBreakPoint = 0;
        SET @AdjustedRunningPresentHours = 0;
        SET @AdjustedRunningAbsentHours = 0;
        SET @ActualRunningScheduledDays = 0;
        SET @boolReset = 0;
        SET @MakeupHours = 0;
        WHILE @@FETCH_STATUS = 0
            BEGIN

                IF @PrevStuEnrollId <> @StuEnrollId
                    BEGIN
                        SET @ActualRunningPresentHours = 0;
                        SET @ActualRunningAbsentHours = 0;
                        SET @intTardyBreakPoint = 0;
                        SET @ActualRunningTardyHours = 0;
                        SET @AdjustedRunningPresentHours = 0;
                        SET @AdjustedRunningAbsentHours = 0;
                        SET @ActualRunningScheduledDays = 0;
                        SET @MakeupHours = 0;
                        SET @boolReset = 1;
                    END;

	                  
            -- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
                IF @Actual <> 9999.00
                    BEGIN
                        SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;
                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual;
                    
                    -- If there are make up hrs deduct that otherwise it will be added again in progress report
                        IF (
                             @Actual > 0
                             AND @Actual > @ScheduledMinutes
                             AND @Actual <> 9999.00
                           )
                            BEGIN
                                SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                            END;
                      
                        SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;                      
                    END;
           
			-- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
                SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;	
      
			-- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
                IF (
                     @Actual > 0
                     AND @Actual < @ScheduledMinutes
                     AND @tardy = 1
                   )
                    BEGIN
                        SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                    END;
					
			-- Track how many days student has been tardy only when 
			-- program version requires to track tardy
                IF (
                     @tracktardies = 1
                     AND @tardy = 1
                   )
                    BEGIN
                        SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                    END;	    
           
            
            -- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
            -- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
            -- when student is tardy the second time, that second occurance will be considered as
            -- absence
            -- Variable @intTardyBreakpoint tracks how many times the student was tardy
            -- Variable @tardiesMakingAbsence tracks the tardy rule
                IF (
                     @tracktardies = 1
                     AND @intTardyBreakPoint = @tardiesMakingAbsence
                   )
                    BEGIN
                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                        SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes
                        SET @intTardyBreakPoint = 0;
                    END;
           
           -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                IF (
                     @Actual > 0
                     AND @Actual > @ScheduledMinutes
                     AND @Actual <> 9999.00
                   )
                    BEGIN
                        SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                    END;
	  
                IF ( @tracktardies = 1 )
                    BEGIN
                        SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
                    END;
                ELSE
                    BEGIN
                        SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                    END;
		
                DELETE  FROM syStudentAttendanceSummary
                WHERE   StuEnrollId = @StuEnrollId
                        AND ClsSectionId = @ClsSectionId
                        AND StudentAttendedDate = @MeetDate;
                INSERT  INTO syStudentAttendanceSummary
                        (
                         StuEnrollId
                        ,ClsSectionId
                        ,StudentAttendedDate
                        ,ScheduledDays
                        ,ActualDays
                        ,ActualRunningScheduledDays
                        ,ActualRunningPresentDays
                        ,ActualRunningAbsentDays
                        ,ActualRunningMakeupDays
                        ,ActualRunningTardyDays
                        ,AdjustedPresentDays
                        ,AdjustedAbsentDays
                        ,AttendanceTrackType
                        ,ModUser
                        ,ModDate
                        ,tardiesmakingabsence
                        )
                VALUES  (
                         @StuEnrollId
                        ,@ClsSectionId
                        ,@MeetDate
                        ,@ScheduledMinutes
                        ,@Actual
                        ,@ActualRunningScheduledDays
                        ,@ActualRunningPresentHours
                        ,@ActualRunningAbsentHours
                        ,ISNULL(@MakeupHours,0)
                        ,@ActualRunningTardyHours
                        ,@AdjustedPresentDaysComputed
                        ,@AdjustedRunningAbsentHours
                        ,'Post Attendance by Class Min'
                        ,'sa'
                        ,GETDATE()
                        ,@tardiesMakingAbsence
                        );

		--update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
                SET @PrevStuEnrollId = @StuEnrollId; 

                FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,@PeriodDescrip,@Actual,@Excused,
                    @Absent,@ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,@tardiesMakingAbsence,@PrgVerId,@rownumber;
            END;
        CLOSE GetAttendance_Cursor;
        DEALLOCATE GetAttendance_Cursor; 
        SET NOCOUNT OFF; 
    END;
--==========================================================================================
-- END  -- TRIGGER TR_InsertAttendanceSummary_ByClass_Minutes
--==========================================================================================
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--==========================================================================================
-- TRIGGER TR_InsertAttendanceSummary_ByClass_PA
-- AFTER INSERT, UPDATE
--==========================================================================================
-----------------------------------------------------------------------------
-- Alter trigger [TR_InsertAttendanceSummary_ByClass_PA] ON [dbo].[atClsSectAttendance]
-- Add SET NOCOUNT ON and SET NOCOUNT OFF to compatibility with Hibernate.
-- JAGG Begin -- 3/4/2014
-----------------------------------------------------------------------------
CREATE TRIGGER [dbo].[TR_InsertAttendanceSummary_ByClass_PA] ON [dbo].[atClsSectAttendance]
    AFTER INSERT,UPDATE
AS
    BEGIN
        SET NOCOUNT ON; 
    
        DECLARE @StuEnrollId UNIQUEIDENTIFIER
           ,@MeetDate DATETIME
           ,@WeekDay VARCHAR(15)
           ,@StartDate DATETIME
           ,@EndDate DATETIME;
        DECLARE @PeriodDescrip VARCHAR(50)
           ,@Actual DECIMAL(18,2)
           ,@Excused DECIMAL(18,2)
           ,@ClsSectionId UNIQUEIDENTIFIER;
        DECLARE @Absent DECIMAL(18,2)
           ,@ScheduledMinutes DECIMAL(18,2)
           ,@TardyMinutes DECIMAL(18,2);
        DECLARE @tardy DECIMAL(18,2)
           ,@tracktardies INT
           ,@tardiesMakingAbsence INT
           ,@PrgVerId UNIQUEIDENTIFIER
           ,@rownumber INT;
        DECLARE @ActualRunningPresentHours DECIMAL(18,2)
           ,@ActualRunningAbsentHours DECIMAL(18,2)
           ,@ActualRunningTardyHours DECIMAL(18,2)
           ,@ActualRunningMakeupHours DECIMAL(18,2);
        DECLARE @PrevStuEnrollId UNIQUEIDENTIFIER
           ,@intTardyBreakPoint INT
           ,@AdjustedRunningPresentHours DECIMAL(18,2)
           ,@AdjustedRunningAbsentHours DECIMAL(18,2)
           ,@ActualRunningScheduledDays DECIMAL(18,2);
        DECLARE @AdjustedPresentDaysComputed DECIMAL(18,2)
           ,@MakeupHours DECIMAL(18,2);
        DECLARE @boolReset BIT;
		
        DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD
        FOR
            SELECT  *
                   ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
            FROM    (
                      SELECT DISTINCT
                                t1.StuEnrollId
                               ,t1.ClsSectionId
                               ,t1.MeetDate
                               ,DATENAME(dw,t1.MeetDate) AS WeekDay
                               ,t4.StartDate
                               ,t4.EndDate
                               ,t5.PeriodDescrip
                               ,t1.Actual
                               ,t1.Excused
                               ,CASE WHEN (
                                            t1.Actual = 0
                                            AND t1.Excused = 0
                                          ) THEN t1.Scheduled
                                     ELSE CASE WHEN (
                                                      t1.Actual <> 9999.00
                                                      AND t1.Actual < t1.Scheduled
                                                      AND t1.Excused <> 1
                                                    ) THEN ( t1.Scheduled - t1.Actual )
                                               ELSE 0
                                          END
                                END AS Absent
                               ,t1.Scheduled AS ScheduledMinutes
                               ,CASE WHEN (
                                            t1.Actual > 0
                                            AND t1.Actual < t1.Scheduled
                                          ) THEN ( t1.Scheduled - t1.Actual )
                                     ELSE 0
                                END AS TardyMinutes
                               ,t1.Tardy AS Tardy
                               ,t3.TrackTardies
                               ,t3.TardiesMakingAbsence
                               ,t3.PrgVerId
                      FROM      atClsSectAttendance t1
                      INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                      INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                      INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                         AND (
                                                               CONVERT(DATE,t1.MeetDate,111) >= CONVERT(DATE,t4.StartDate,111)
                                                               AND CONVERT(DATE,t1.MeetDate,111) <= CONVERT(DATE,t4.EndDate,111)
                                                             )
                                                         AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                      INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                      INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                      INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                      INNER JOIN arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                      INNER JOIN arReqs t10 ON t10.ReqId = t9.ReqId
                      INNER JOIN Inserted t8 ON t8.StuEnrollId = t1.StuEnrollId
                                                AND t8.ClsSectionId = t1.ClsSectionId
                                                AND t8.MeetDate = t1.MeetDate
                      WHERE     t2.StuEnrollId IN ( SELECT DISTINCT
                                                            StuEnrollId
                                                    FROM    inserted )
                                AND (
                                      t10.UnitTypeId IN ( '2600592A-9739-4A13-BDCE-7A25FE4A7478','EF5535C2-142C-4223-AE3C-25A50A153CC6' )
                                      OR t3.UnitTypeId IN ( '2600592A-9739-4A13-BDCE-7A25FE4A7478','EF5535C2-142C-4223-AE3C-25A50A153CC6' )
                                    ) -- Minutes
                                AND t1.Actual <> 9999
                    ) dt
            ORDER BY StuEnrollId
                   ,MeetDate;
        OPEN GetAttendance_Cursor;
        FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,@PeriodDescrip,@Actual,@Excused,@Absent,
            @ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,@tardiesMakingAbsence,@PrgVerId,@rownumber;
        SET @ActualRunningPresentHours = 0;
        SET @ActualRunningPresentHours = 0;
        SET @ActualRunningAbsentHours = 0;
        SET @ActualRunningTardyHours = 0;
        SET @ActualRunningMakeupHours = 0;
        SET @intTardyBreakPoint = 0;
        SET @AdjustedRunningPresentHours = 0;
        SET @AdjustedRunningAbsentHours = 0;
        SET @ActualRunningScheduledDays = 0;
        SET @boolReset = 0;
        SET @MakeupHours = 0;
        WHILE @@FETCH_STATUS = 0
            BEGIN

                IF @PrevStuEnrollId <> @StuEnrollId
                    BEGIN
                        SET @ActualRunningPresentHours = 0;
                        SET @ActualRunningAbsentHours = 0;
                        SET @intTardyBreakPoint = 0;
                        SET @ActualRunningTardyHours = 0;
                        SET @AdjustedRunningPresentHours = 0;
                        SET @AdjustedRunningAbsentHours = 0;
                        SET @ActualRunningScheduledDays = 0;
                        SET @MakeupHours = 0;
                        SET @boolReset = 1;
                    END;

	                  
            -- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
                IF @Actual <> 9999.00
                    BEGIN
                        SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual + @Excused;
                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual + @Excused;
                    
                    -- If there are make up hrs deduct that otherwise it will be added again in progress report
                        IF (
                             @Actual > 0
                             AND @Actual > @ScheduledMinutes
                             AND @Actual <> 9999.00
                           )
                            BEGIN
                                SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                            END;
        
                        SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;                      
                    END;
           
			-- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
                SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;	
      
			-- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
                IF (
                     @Actual > 0
                     AND @Actual < @ScheduledMinutes
                     AND @tardy = 1
                   )
                    BEGIN
                        SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                    END;
					
			-- Track how many days student has been tardy only when 
			-- program version requires to track tardy
                IF (
                     @tracktardies = 1
                     AND @tardy = 1
                   )
                    BEGIN
                        SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                    END;	    
           
            
            -- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
            -- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
            -- when student is tardy the second time, that second occurance will be considered as
            -- absence
            -- Variable @intTardyBreakpoint tracks how many times the student was tardy
            -- Variable @tardiesMakingAbsence tracks the tardy rule
                IF (
                     @tracktardies = 1
                     AND @intTardyBreakPoint = @tardiesMakingAbsence
                   )
                    BEGIN
                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual - @Excused;
                        SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes
                        SET @intTardyBreakPoint = 0;
                    END;
           
           -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                IF (
                     @Actual > 0
                     AND @Actual > @ScheduledMinutes
                     AND @Actual <> 9999.00
                   )
                    BEGIN
                        SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                    END;
	  
                IF ( @tracktardies = 1 )
                    BEGIN
                        SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
                    END;
                ELSE
                    BEGIN
                        SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                    END;
		
                DELETE  FROM syStudentAttendanceSummary
                WHERE   StuEnrollId = @StuEnrollId
                        AND ClsSectionId = @ClsSectionId
                        AND StudentAttendedDate = @MeetDate;
                INSERT  INTO syStudentAttendanceSummary
                        (
                         StuEnrollId
                        ,ClsSectionId
                        ,StudentAttendedDate
                        ,ScheduledDays
                        ,ActualDays
                        ,ActualRunningScheduledDays
                        ,ActualRunningPresentDays
                        ,ActualRunningAbsentDays
                        ,ActualRunningMakeupDays
                        ,ActualRunningTardyDays
                        ,AdjustedPresentDays
                        ,AdjustedAbsentDays
                        ,AttendanceTrackType
                        ,ModUser
                        ,ModDate
                        ,tardiesmakingabsence
                        )
                VALUES  (
                         @StuEnrollId
                        ,@ClsSectionId
                        ,@MeetDate
                        ,@ScheduledMinutes
                        ,@Actual
                        ,@ActualRunningScheduledDays
                        ,@ActualRunningPresentHours
                        ,@ActualRunningAbsentHours
                        ,ISNULL(@MakeupHours,0)
                        ,@ActualRunningTardyHours
                        ,@AdjustedPresentDaysComputed
                        ,@AdjustedRunningAbsentHours
                        ,'Post Attendance by Class Min'
                        ,'sa'
                        ,GETDATE()
                        ,@tardiesMakingAbsence
                        );

		--update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
                SET @PrevStuEnrollId = @StuEnrollId; 

                FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,@PeriodDescrip,@Actual,@Excused,
                    @Absent,@ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,@tardiesMakingAbsence,@PrgVerId,@rownumber;
            END;
        CLOSE GetAttendance_Cursor;
        DEALLOCATE GetAttendance_Cursor;

        SET NOCOUNT OFF; 
    END;
--==========================================================================================
-- END  --  TRIGGER TR_InsertAttendanceSummary_ByClass_PA
--==========================================================================================
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--==========================================================================================
-- TRIGGER TR_InsertClsSectionAttendance_Class_ClockHour
-- AFTER INSERT, UPDATE
--==========================================================================================
CREATE TRIGGER [dbo].[TR_InsertClsSectionAttendance_Class_ClockHour] ON [dbo].[atClsSectAttendance]
    AFTER INSERT,UPDATE
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @StuEnrollId UNIQUEIDENTIFIER
           ,@MeetDate DATETIME
           ,@WeekDay VARCHAR(15)
           ,@StartDate DATETIME
           ,@EndDate DATETIME;
        DECLARE @PeriodDescrip VARCHAR(50)
           ,@Actual DECIMAL(18,2)
           ,@Excused DECIMAL(18,2)
           ,@ClsSectionId UNIQUEIDENTIFIER;
        DECLARE @Absent DECIMAL(18,2)
           ,@ScheduledMinutes DECIMAL(18,2)
           ,@TardyMinutes DECIMAL(18,2);
        DECLARE @tardy DECIMAL(18,2)
           ,@tracktardies INT
           ,@tardiesMakingAbsence INT
           ,@PrgVerId UNIQUEIDENTIFIER
           ,@rownumber INT;
        DECLARE @ActualRunningPresentHours DECIMAL(18,2)
           ,@ActualRunningAbsentHours DECIMAL(18,2);
        DECLARE @ActualRunningTardyHours DECIMAL(18,2)
           ,@ActualRunningMakeupHours DECIMAL(18,2);
        DECLARE @PrevStuEnrollId UNIQUEIDENTIFIER
           ,@intTardyBreakPoint INT
           ,@AdjustedRunningPresentHours DECIMAL(18,2)
           ,@AdjustedRunningAbsentHours DECIMAL(18,2)
           ,@ActualRunningScheduledDays DECIMAL(18,2);
        DECLARE @AdjustedPresentDaysComputed DECIMAL(18,2)
           ,@PeriodDescrip1 VARCHAR(500)
           ,@MakeupHours DECIMAL(18,2);
        DECLARE GetAttendance_Cursor CURSOR
        FOR
            SELECT  *
                   ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
            FROM    (
                      SELECT DISTINCT
                                t1.StuEnrollId
                               ,t1.ClsSectionId
                               ,(
                                  SELECT    Descrip
                                  FROM      arReqs A1
                                           ,arClassSections A2
                                  WHERE     A1.ReqId = A2.ReqId
                                            AND A2.ClsSectionId = t1.ClsSectionId
                                ) + '  ' + t5.PeriodDescrip AS PeriodDescrip1
                               ,t1.MeetDate
                               ,DATENAME(dw,t1.MeetDate) AS WeekDay
                               ,t4.StartDate
                               ,t4.EndDate
                               ,t5.PeriodDescrip
                               ,CASE WHEN (
                                            t1.Actual >= 0
                                            AND t1.Actual <> 9999
                                          ) THEN t1.Actual / CAST(60 AS FLOAT)
                                     ELSE t1.Actual
                                END AS Actual
                               ,t1.Excused
                               ,CASE WHEN (
                                            t1.Actual >= 0
                                            AND t1.Actual <> 9999
                                            AND ( t1.Actual / CAST(60 AS FLOAT) ) < DATEDIFF(hh,t6.TimeIntervalDescrip,t7.TimeIntervalDescrip)
                                          ) THEN ( ( t1.Scheduled / CAST(60 AS FLOAT) ) - ( t1.Actual / CAST(60 AS FLOAT) ) )
                                     ELSE 0
                                END AS Absent
                               ,CASE WHEN t1.Scheduled > 0 THEN ( t1.Scheduled / CAST(60 AS FLOAT) )
                                     ELSE 0
                                END AS ScheduledMinutes
                               ,NULL AS TardyMinutes
                               ,CASE WHEN t1.Tardy = 1 THEN 1
                                     ELSE 0
                                END AS Tardy
                               ,t3.TrackTardies
                               ,t3.TardiesMakingAbsence
                               ,t3.PrgVerId
                      FROM      atClsSectAttendance t1
                      INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                      INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                      INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                         AND (
                                                               CONVERT(DATE,t1.MeetDate,111) >= CONVERT(DATE,t4.StartDate,111)
                                                               AND CONVERT(DATE,t1.MeetDate,111) <= CONVERT(DATE,t4.EndDate,111)
                                                             )
                                                         AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8522 line added
                      INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                      INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                      INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                      INNER JOIN syPeriodsWorkDays PWD ON t5.PeriodId = PWD.PeriodId
                      INNER JOIN plWorkDays WD ON WD.WorkDaysId = PWD.WorkDayId
                      INNER JOIN arClassSections t8 ON t8.ClsSectionId = t1.ClsSectionId
                      INNER JOIN arReqs t9 ON t9.ReqId = t8.ReqId
                      WHERE     t2.StuEnrollId IN ( SELECT  StuEnrollId
                                                    FROM    inserted )
                                AND (
                                      t9.UnitTypeId IN ( 'B937C92E-FD7A-455E-A731-527A9918C734' )
                                      OR t3.UnitTypeId IN ( 'B937C92E-FD7A-455E-A731-527A9918C734' )
                                    ) -- Clock Hours
                                AND t1.Actual <> 9999.00
                                AND SUBSTRING(DATENAME(dw,t1.MeetDate),1,3) = SUBSTRING(WD.WorkDaysDescrip,1,3)
			
		-- Some times Makeup days don't fall inside the schedule
		-- ex: there may be a schedule for T-Fri and school may mark attendance for saturday
		-- the following query will bring whatever was left out in above query
                      UNION
                      SELECT DISTINCT
                                t1.StuEnrollId
                               ,t1.ClsSectionId
                               ,(
                                  SELECT    Descrip
                                  FROM      arReqs A1
                                           ,arClassSections A2
                                  WHERE     A1.ReqId = A2.ReqId
                                            AND A2.ClsSectionId = t1.ClsSectionId
                                ) + '  ' + t5.PeriodDescrip AS PeriodDescrip1
                               ,t1.MeetDate
                               ,DATENAME(dw,t1.MeetDate) AS WeekDay
                               ,t4.StartDate
                               ,t4.EndDate
                               ,t5.PeriodDescrip
                               ,CASE WHEN (
                                            t1.Actual >= 0
                                            AND t1.Actual <> 9999
                                          ) THEN t1.Actual / CAST(60 AS FLOAT)
                                     ELSE t1.Actual
                                END AS Actual
                               ,t1.Excused
                               ,CASE WHEN (
                                            t1.Actual >= 0
                                            AND t1.Actual <> 9999
                                            AND ( t1.Actual / CAST(60 AS FLOAT) ) < DATEDIFF(hh,t6.TimeIntervalDescrip,t7.TimeIntervalDescrip)
                                          ) THEN ( ( t1.Scheduled / CAST(60 AS FLOAT) ) - ( t1.Actual / CAST(60 AS FLOAT) ) )
                                     ELSE 0
                                END AS Absent
                               ,CASE WHEN t1.Scheduled > 0 THEN ( t1.Scheduled / CAST(60 AS FLOAT) )
                                     ELSE 0
                                END AS ScheduledMinutes
                               ,NULL AS TardyMinutes
                               ,CASE WHEN t1.Tardy = 1 THEN 1
                                     ELSE 0
                                END AS Tardy
                               ,t3.TrackTardies
                               ,t3.TardiesMakingAbsence
                               ,t3.PrgVerId
                      FROM      atClsSectAttendance t1
                      INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                      INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                      INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                         AND (
                                                               CONVERT(DATE,t1.MeetDate,111) >= CONVERT(DATE,t4.StartDate,111)
                                                               AND CONVERT(DATE,t1.MeetDate,111) <= CONVERT(DATE,t4.EndDate,111)
                                                             )
                                                         AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8522 line added
                      INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                      INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                      INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                      INNER JOIN syPeriodsWorkDays PWD ON t5.PeriodId = PWD.PeriodId
                      INNER JOIN plWorkDays WD ON WD.WorkDaysId = PWD.WorkDayId
                            --INNER JOIN Inserted t20 ON t2.StuEnrollId = t20.StuEnrollId
                      INNER JOIN arClassSections t8 ON t8.ClsSectionId = t1.ClsSectionId
                      INNER JOIN arReqs t9 ON t9.ReqId = t8.ReqId
                      WHERE     t2.StuEnrollId IN ( SELECT  StuEnrollId
                                                    FROM    inserted )
                                AND (
                                      t9.UnitTypeId IN ( 'B937C92E-FD7A-455E-A731-527A9918C734' )
                                      OR t3.UnitTypeId IN ( 'B937C92E-FD7A-455E-A731-527A9918C734' )
                                    ) -- Clock Hours
                                AND t1.Actual <> 9999.00
                                AND t1.MeetDate NOT IN (
                                SELECT  t1.MeetDate
                                FROM    atClsSectAttendance t1
                                INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                   AND (
                                                                         CONVERT(DATE,t1.MeetDate,111) >= CONVERT(DATE,t4.StartDate,111)
                                                                         AND CONVERT(DATE,t1.MeetDate,111) <= CONVERT(DATE,t4.EndDate,111)
                                                                       )
                                INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                                INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                                INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                                INNER JOIN syPeriodsWorkDays PWD ON t5.PeriodId = PWD.PeriodId
                                INNER JOIN plWorkDays WD ON WD.WorkDaysId = PWD.WorkDayId
                                    --INNER JOIN Inserted t20 ON t2.StuEnrollId = t20.StuEnrollId
                                WHERE   t2.StuEnrollId IN ( SELECT  StuEnrollId
                                                            FROM    inserted )
                                        AND t3.UnitTypeId IN ( 'B937C92E-FD7A-455E-A731-527A9918C734' ) -- Clock Hours
                                        AND t1.Actual <> 9999.00
                                        AND SUBSTRING(DATENAME(dw,t1.MeetDate),1,3) = SUBSTRING(WD.WorkDaysDescrip,1,3) )
                    ) dt
            ORDER BY StuEnrollId
                   ,MeetDate;
	
	
        OPEN GetAttendance_Cursor;
        FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@PeriodDescrip1,@MeetDate,@WeekDay,@StartDate,@EndDate,@PeriodDescrip,@Actual,
            @Excused,@Absent,@ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,@tardiesMakingAbsence,@PrgVerId,@rownumber;
        SET @ActualRunningPresentHours = 0;
        SET @ActualRunningPresentHours = 0;
        SET @ActualRunningAbsentHours = 0;
        SET @ActualRunningTardyHours = 0;
        SET @ActualRunningMakeupHours = 0;
        SET @intTardyBreakPoint = 0;
        SET @AdjustedRunningPresentHours = 0;
        SET @AdjustedRunningAbsentHours = 0;
        SET @ActualRunningScheduledDays = 0;
        SET @MakeupHours = 0;
        WHILE @@FETCH_STATUS = 0
            BEGIN

                IF @PrevStuEnrollId <> @StuEnrollId
                    BEGIN
                        SET @ActualRunningPresentHours = 0;
                        SET @ActualRunningAbsentHours = 0;
                        SET @intTardyBreakPoint = 0;
                        SET @ActualRunningTardyHours = 0;
                        SET @AdjustedRunningPresentHours = 0;
                        SET @AdjustedRunningAbsentHours = 0;
                        SET @ActualRunningScheduledDays = 0;
                        SET @MakeupHours = 0;
                    END;

	  	    -- Calculate : Actual Running Scheduled Days
                SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;
            
            -- Calculate: Actual and Adjusted Running Present Hours
                IF @Actual <> 9999
                    BEGIN
                        IF (
                             @Actual > 0
                             AND @Actual > @ScheduledMinutes
                           ) -- Makeup Hours
                            BEGIN
                                SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual - ( @Actual - @ScheduledMinutes ); -- Subtract Makeup Hours from Actual
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual - ( @Actual - @ScheduledMinutes ); -- Subtract Makeup Hours from Actual
                            END; 
                        ELSE
                            BEGIN
                                SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual; 
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual;
                            END;
                    END; 
            
            -- Calculate: Adjusted Running Absent Hours
                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;	
            
                IF (
                     @Actual > 0
                     AND @Actual < @ScheduledMinutes
                     AND @tardy = 1
                   )
                    BEGIN
                        SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                    END;
	
                IF (
                     @Actual > 0
                     AND @Actual > @ScheduledMinutes
                   )
                    BEGIN
                        SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                    END;
                IF (
                     @tracktardies = 1
                     AND @TardyMinutes > 0
                     AND @tardy = 1
                   )
                    BEGIN
                        SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                    END;	    
                IF (
                     @tracktardies = 1
                     AND @intTardyBreakPoint = @tardiesMakingAbsence
                   )
                    BEGIN
                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                        SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes
                        SET @ActualRunningTardyHours = @ActualRunningTardyHours - ( @ScheduledMinutes - @Actual );
                        SET @intTardyBreakPoint = 0;
                    END;
	  
                IF ( @tracktardies = 1 )
                    BEGIN
                        SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
                    END;
                ELSE
                    BEGIN
                        SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                    END;
		
					
                DELETE  FROM syStudentAttendanceSummary
                WHERE   StuEnrollId = @StuEnrollId
                        AND ClsSectionId = @ClsSectionId
                        AND StudentAttendedDate = @MeetDate;
		
                INSERT  INTO syStudentAttendanceSummary
                        (
                         StuEnrollId
                        ,ClsSectionId
                        ,StudentAttendedDate
                        ,ScheduledDays
                        ,ActualDays
                        ,ActualRunningScheduledDays
                        ,ActualRunningPresentDays
                        ,ActualRunningAbsentDays
                        ,ActualRunningMakeupDays
                        ,ActualRunningTardyDays
                        ,AdjustedPresentDays
                        ,AdjustedAbsentDays
                        ,AttendanceTrackType
                        ,ModUser
                        ,ModDate
                        )
                VALUES  (
                         @StuEnrollId
                        ,@ClsSectionId
                        ,@MeetDate
                        ,@ScheduledMinutes
                        ,@Actual
                        ,@ActualRunningScheduledDays
                        ,@ActualRunningPresentHours
                        ,@ActualRunningAbsentHours
                        ,ISNULL(@MakeupHours,0)
                        ,@ActualRunningTardyHours
                        ,@AdjustedPresentDaysComputed
                        ,@AdjustedRunningAbsentHours
                        ,'Post Attendance by Class Clock'
                        ,'sa'
                        ,GETDATE()
                        );

                UPDATE  syStudentAttendanceSummary
                SET     tardiesmakingabsence = @tardiesMakingAbsence
                WHERE   StuEnrollId = @StuEnrollId;
            
                SET @PrevStuEnrollId = @StuEnrollId; 

                FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@PeriodDescrip1,@MeetDate,@WeekDay,@StartDate,@EndDate,@PeriodDescrip,
                    @Actual,@Excused,@Absent,@ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,@tardiesMakingAbsence,@PrgVerId,@rownumber;
            END;
        CLOSE GetAttendance_Cursor;
        DEALLOCATE GetAttendance_Cursor;
-----------------------------------------------------------------------------
-- JAGG END
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- US Story - 5319 - JG 03/17/2014 - Modify menu items for exclusions.
-----------------------------------------------------------------------------

        SET NOCOUNT OFF; 
    END;
--==========================================================================================
-- TRIGGER TR_InsertClsSectionAttendance_Class_ClockHour
-- AFTER INSERT, UPDATE
--==========================================================================================
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE TRIGGER [dbo].[TR_UpdateLDA_CSA]
ON [dbo].[atClsSectAttendance]
AFTER INSERT, UPDATE, DELETE
AS

--If update and the record date is changed or fresh insert then refresh the lda

DECLARE @Data TABLE
    (
        StuEnrollId UNIQUEIDENTIFIER NOT NULL
       ,LDA DATETIME NULL
    );

INSERT INTO @Data
            SELECT    enrollments.StuEnrollId
                     ,MAX(csa.MeetDate) AS LDA
            FROM      (
                      (SELECT I.StuEnrollId
                       FROM   Inserted I
                       WHERE  (
                              I.Actual > 0
                              AND I.Actual <> 99.00
                              AND I.Actual <> 999.00
                              AND I.Actual <> 9999.00
                              ))
                      UNION
                      (SELECT d.StuEnrollId
                       FROM   Deleted d)
                      ) enrollments
            LEFT JOIN dbo.atClsSectAttendance csa ON csa.StuEnrollId = enrollments.StuEnrollId
            WHERE     (
                      csa.Actual > 0
                      AND csa.Actual <> 99.00
                      AND csa.Actual <> 999.00
                      AND csa.Actual <> 9999.00
                      )
            GROUP BY  enrollments.StuEnrollId;


UPDATE e
SET    e.LDA = t.LDA
FROM   dbo.arStuEnrollments e
JOIN   @Data t ON t.StuEnrollId = e.StuEnrollId;
GO
ALTER TABLE [dbo].[atClsSectAttendance] ADD CONSTRAINT [PK_atClsSectAttendance_ClsSectAttId] PRIMARY KEY CLUSTERED  ([ClsSectAttId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_atClsSectAttendance_ClsSectionId_MeetDate_StuEnrollId_ClsSectMeetingId] ON [dbo].[atClsSectAttendance] ([ClsSectionId], [MeetDate], [StuEnrollId], [ClsSectMeetingId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_atClsSectAttendance_StuEnrollId_Actual_MeetDate] ON [dbo].[atClsSectAttendance] ([StuEnrollId]) INCLUDE ([Actual], [MeetDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_atClsSectAttendance_StuEnrollId_Actual_ClsSectionId_MeetDate_ClsSectAttId] ON [dbo].[atClsSectAttendance] ([StuEnrollId], [Actual], [ClsSectionId], [MeetDate]) INCLUDE ([ClsSectAttId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_atClsSectAttendance_StuEnrollId_Actual_Excused_MeetDate] ON [dbo].[atClsSectAttendance] ([StuEnrollId], [Actual], [Excused], [MeetDate]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_atClsSectAttendance_StuEnrollId_MeetDate_Actual_Excused_Tardy] ON [dbo].[atClsSectAttendance] ([StuEnrollId], [MeetDate], [Actual], [Excused], [Tardy]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_atClsSectAttendance_StuEnrollId_MeetDate_ClsSectAttId_ClsSectionId_Actual_Tardy_Excused] ON [dbo].[atClsSectAttendance] ([StuEnrollId], [MeetDate], [ClsSectAttId], [ClsSectionId], [Actual], [Tardy], [Excused]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_atClsSectAttendance_Tardy_Actual_StuEnrollId_MeetDate] ON [dbo].[atClsSectAttendance] ([Tardy], [Actual], [StuEnrollId], [MeetDate]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[atClsSectAttendance] ADD CONSTRAINT [FK_atClsSectAttendance_arClassSections_ClsSectionId_ClsSectionId] FOREIGN KEY ([ClsSectionId]) REFERENCES [dbo].[arClassSections] ([ClsSectionId])
GO
ALTER TABLE [dbo].[atClsSectAttendance] ADD CONSTRAINT [FK_atClsSectAttendance_arClsSectMeetings_ClsSectMeetingId_ClsSectMeetingId] FOREIGN KEY ([ClsSectMeetingId]) REFERENCES [dbo].[arClsSectMeetings] ([ClsSectMeetingId])
GO
ALTER TABLE [dbo].[atClsSectAttendance] ADD CONSTRAINT [FK_atClsSectAttendance_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY ([StuEnrollId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
