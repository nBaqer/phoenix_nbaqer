CREATE TABLE [dbo].[arSAP_ShortHandSkillRequirement]
(
[ShortHandSkillRequirementId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_arSAP_ShortHandSkillRequirement_ShortHandSkillRequirementId] DEFAULT (newid()),
[SAPDetailId] [uniqueidentifier] NULL,
[GrdComponentTypeId] [uniqueidentifier] NULL,
[Speed] [int] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[Operator] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[OperatorSequence] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arSAP_ShortHandSkillRequirement] ADD CONSTRAINT [PK_arSAP_ShortHandSkillRequirement_ShortHandSkillRequirementId] PRIMARY KEY CLUSTERED  ([ShortHandSkillRequirementId]) ON [PRIMARY]
GO
