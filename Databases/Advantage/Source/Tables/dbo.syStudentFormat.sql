CREATE TABLE [dbo].[syStudentFormat]
(
[FormatType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[YearNumber] [int] NULL,
[MonthNumber] [int] NULL,
[DateNumber] [int] NULL,
[LNameNumber] [int] NULL,
[FNameNumber] [int] NULL,
[SeqNumber] [int] NULL,
[SeqStartingNumber] [int] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[StudentFormatId] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syStudentFormat] ADD CONSTRAINT [PK_syStudentFormat_StudentFormatId] PRIMARY KEY CLUSTERED  ([StudentFormatId]) ON [PRIMARY]
GO
