CREATE TABLE [dbo].[sySummLists]
(
[SummListId] [smallint] NOT NULL,
[SummListName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TblId] [smallint] NOT NULL,
[DispFldId] [smallint] NOT NULL,
[ValFldId] [smallint] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[sySummLists] ADD CONSTRAINT [PK_sySummLists_SummListId] PRIMARY KEY CLUSTERED  ([SummListId]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[sySummLists] TO [AdvantageRole]
GO
