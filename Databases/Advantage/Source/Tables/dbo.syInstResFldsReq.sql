CREATE TABLE [dbo].[syInstResFldsReq]
(
[ResReqFldId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syInstResFldsReq_ResReqFldId] DEFAULT (newid()),
[ResourceId] [int] NOT NULL,
[TblFldsId] [int] NOT NULL,
[ModDate] [datetime] NOT NULL CONSTRAINT [DF_syInstResFldsReq_ModDate] DEFAULT (getdate()),
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syInstResFldsReq] ADD CONSTRAINT [PK_syInstResFldsReq_ResReqFldId] PRIMARY KEY CLUSTERED  ([ResReqFldId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_syInstResFldsReq_ResourceId_TblFldsId] ON [dbo].[syInstResFldsReq] ([ResourceId], [TblFldsId]) ON [PRIMARY]
GO
GRANT REFERENCES ON  [dbo].[syInstResFldsReq] TO [AdvantageRole]
GRANT SELECT ON  [dbo].[syInstResFldsReq] TO [AdvantageRole]
GRANT INSERT ON  [dbo].[syInstResFldsReq] TO [AdvantageRole]
GRANT DELETE ON  [dbo].[syInstResFldsReq] TO [AdvantageRole]
GRANT UPDATE ON  [dbo].[syInstResFldsReq] TO [AdvantageRole]
GO
