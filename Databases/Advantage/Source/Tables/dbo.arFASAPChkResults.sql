CREATE TABLE [dbo].[arFASAPChkResults]
(
[StdRecKey] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arFASAPChkResults_StdRecKey] DEFAULT (newid()),
[StuEnrollId] [uniqueidentifier] NOT NULL,
[Period] [int] NOT NULL,
[IsMakingSAP] [bit] NOT NULL,
[HrsAttended] [decimal] (19, 2) NULL,
[HrsEarned] [decimal] (19, 2) NULL,
[CredsAttempted] [decimal] (18, 2) NULL,
[CredsEarned] [decimal] (18, 2) NULL,
[GPA] [decimal] (18, 2) NULL,
[DatePerformed] [datetime] NOT NULL,
[Comments] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[SAPDetailId] [uniqueidentifier] NULL CONSTRAINT [DF_arFASAPChkResults_SAPDetailId] DEFAULT (newid()),
[Average] [decimal] (18, 2) NULL,
[Attendance] [decimal] (19, 2) NULL,
[CumFinAidCredits] [decimal] (19, 2) NULL,
[PercentCompleted] [decimal] (19, 2) NULL,
[CheckPointDate] [datetime] NULL,
[TermStartDate] [datetime] NULL,
[PreviewSapCheck] [bit] NOT NULL,
[TitleIVStatusId] [int] NOT NULL CONSTRAINT [DF_arFASAPChkResults_TitleIVStatusId] DEFAULT ((1)),
[StatusId] [uniqueidentifier] NOT NULL CONSTRAINT [DF__arFASAPCh__Statu__39BF9D86] DEFAULT ('F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'),
[Printed] [bit] NOT NULL CONSTRAINT [DF_arFASAPChkResults_Printed] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arFASAPChkResults] ADD CONSTRAINT [PK_arFASAPChkResults_StdRecKey] PRIMARY KEY NONCLUSTERED  ([StdRecKey]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arFASAPChkResults] ADD CONSTRAINT [FK_arFASAPChkResults_arSAPDetails_SAPDetailId_SAPDetailId] FOREIGN KEY ([SAPDetailId]) REFERENCES [dbo].[arSAPDetails] ([SAPDetailId])
GO
ALTER TABLE [dbo].[arFASAPChkResults] ADD CONSTRAINT [FK_arFASAPChkResults_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY ([StuEnrollId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
ALTER TABLE [dbo].[arFASAPChkResults] ADD CONSTRAINT [FK_arFASAPChkResults_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
ALTER TABLE [dbo].[arFASAPChkResults] ADD CONSTRAINT [FK_arFASAPChkResults_syTitleIVSapStatus_TitleIVStatusId_Id] FOREIGN KEY ([TitleIVStatusId]) REFERENCES [dbo].[syTitleIVSapStatus] ([Id])
GO
