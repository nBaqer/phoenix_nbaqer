CREATE TABLE [dbo].[adAdminCriteria]
(
[admincriteriaid] [uniqueidentifier] NOT NULL CONSTRAINT [DF_adAdminCriteria_admincriteriaid] DEFAULT (newid()),
[Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adAdminCriteria] ADD CONSTRAINT [PK_adAdminCriteria_admincriteriaid] PRIMARY KEY CLUSTERED  ([admincriteriaid]) ON [PRIMARY]
GO
