CREATE TABLE [dbo].[syGenerateStudentFormatID]
(
[Student_SeqId] [int] NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GenerateStudentFormatId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_syGenerateStudentFormatID_GenerateStudentFormatId] DEFAULT (newsequentialid())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syGenerateStudentFormatID] ADD CONSTRAINT [PK_syGenerateStudentFormatID_GenerateStudentFormatId] PRIMARY KEY CLUSTERED  ([GenerateStudentFormatId]) ON [PRIMARY]
GO
