CREATE TABLE [dbo].[adAdvInterval]
(
[AdvIntervalId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_adAdvInterval_AdvIntervalId] DEFAULT (newid()),
[AdvIntervalDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[AdvIntervalCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CampGrpId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[adAdvInterval_Audit_Delete] ON [dbo].[adAdvInterval]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adAdvInterval','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.AdvIntervalId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.AdvIntervalId
                               ,'AdvIntervalCode'
                               ,CONVERT(VARCHAR(8000),Old.AdvIntervalCode,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.AdvIntervalId
                               ,'AdvIntervalDescrip'
                               ,CONVERT(VARCHAR(8000),Old.AdvIntervalDescrip,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[adAdvInterval_Audit_Insert] ON [dbo].[adAdvInterval]
    FOR INSERT
AS
    SET NOCOUNT ON; 
 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adAdvInterval','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.AdvIntervalId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.AdvIntervalId
                               ,'AdvIntervalCode'
                               ,CONVERT(VARCHAR(8000),New.AdvIntervalCode,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.AdvIntervalId
                               ,'AdvIntervalDescrip'
                               ,CONVERT(VARCHAR(8000),New.AdvIntervalDescrip,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[adAdvInterval_Audit_Update] ON [dbo].[adAdvInterval]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adAdvInterval','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.AdvIntervalId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.AdvIntervalId = New.AdvIntervalId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(AdvIntervalCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.AdvIntervalId
                                   ,'AdvIntervalCode'
                                   ,CONVERT(VARCHAR(8000),Old.AdvIntervalCode,121)
                                   ,CONVERT(VARCHAR(8000),New.AdvIntervalCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.AdvIntervalId = New.AdvIntervalId
                            WHERE   Old.AdvIntervalCode <> New.AdvIntervalCode
                                    OR (
                                         Old.AdvIntervalCode IS NULL
                                         AND New.AdvIntervalCode IS NOT NULL
                                       )
                                    OR (
                                         New.AdvIntervalCode IS NULL
                                         AND Old.AdvIntervalCode IS NOT NULL
                                       ); 
                IF UPDATE(AdvIntervalDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.AdvIntervalId
                                   ,'AdvIntervalDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.AdvIntervalDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.AdvIntervalDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.AdvIntervalId = New.AdvIntervalId
                            WHERE   Old.AdvIntervalDescrip <> New.AdvIntervalDescrip
                                    OR (
                                         Old.AdvIntervalDescrip IS NULL
                                         AND New.AdvIntervalDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.AdvIntervalDescrip IS NULL
                                         AND Old.AdvIntervalDescrip IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO
ALTER TABLE [dbo].[adAdvInterval] ADD CONSTRAINT [PK_adAdvInterval_AdvIntervalId] PRIMARY KEY CLUSTERED  ([AdvIntervalId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_adAdvInterval_AdvIntervalCode_AdvIntervalDescrip] ON [dbo].[adAdvInterval] ([AdvIntervalCode], [AdvIntervalDescrip]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adAdvInterval] ADD CONSTRAINT [FK_adAdvInterval_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[adAdvInterval] ADD CONSTRAINT [FK_adAdvInterval_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
