CREATE TABLE [dbo].[adVendorPayFor]
(
[IdPayFor] [int] NOT NULL IDENTITY(1, 1),
[PayForCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adVendorPayFor] ADD CONSTRAINT [PK_adVendorPayFor_IdPayFor] PRIMARY KEY CLUSTERED  ([IdPayFor]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adVendorPayFor] ADD CONSTRAINT [UIX_adVendorPayFor_IdPayFor] UNIQUE NONCLUSTERED  ([IdPayFor]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adVendorPayFor] ADD CONSTRAINT [UIX_adVendorPayFor_PayForCode] UNIQUE NONCLUSTERED  ([PayForCode]) ON [PRIMARY]
GO
