CREATE TABLE [dbo].[syVersionHistory]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Major] [int] NULL,
[Minor] [int] NULL,
[Build] [int] NULL,
[Revision] [int] NULL,
[VersionBegin] [datetime] NULL,
[VersionEnd] [datetime] NULL,
[Description] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syVersionHistory] ADD CONSTRAINT [PK_syVersionHistory_Id] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
