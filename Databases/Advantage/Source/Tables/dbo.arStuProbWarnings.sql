CREATE TABLE [dbo].[arStuProbWarnings]
(
[StuProbWarningId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arStuProbWarnings_StuProbWarningId] DEFAULT (newid()),
[StuEnrollId] [uniqueidentifier] NOT NULL,
[StartDate] [datetime] NOT NULL,
[EndDate] [datetime] NULL,
[ProbWarningTypeId] [int] NULL,
[Reason] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StudentStatusChangeId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arStuProbWarnings] ADD CONSTRAINT [PK_arStuProbWarnings_StuProbWarningId] PRIMARY KEY CLUSTERED  ([StuProbWarningId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arStuProbWarnings] ADD CONSTRAINT [FK_arStuProbWarnings_arProbWarningTypes_ProbWarningTypeId_ProbWarningTypeId] FOREIGN KEY ([ProbWarningTypeId]) REFERENCES [dbo].[arProbWarningTypes] ([ProbWarningTypeId])
GO
ALTER TABLE [dbo].[arStuProbWarnings] ADD CONSTRAINT [FK_arStuProbWarnings_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY ([StuEnrollId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
