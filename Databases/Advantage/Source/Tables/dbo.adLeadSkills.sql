CREATE TABLE [dbo].[adLeadSkills]
(
[LeadSkillId] [uniqueidentifier] NOT NULL,
[SkillId] [uniqueidentifier] NULL,
[LeadId] [uniqueidentifier] NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[adLeadSkills_Audit_Delete] ON [dbo].[adLeadSkills]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adLeadSkills','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadSkillId
                               ,'SkillId'
                               ,CONVERT(VARCHAR(8000),Old.SkillId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadSkillId
                               ,'LeadID'
                               ,CONVERT(VARCHAR(8000),Old.LeadId,121)
                        FROM    Deleted Old; 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- Remove SkillGrpId from adLeadSkills Trigger
CREATE TRIGGER [dbo].[adLeadSkills_Audit_Insert] ON [dbo].[adLeadSkills]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adLeadSkills','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadSkillId
                               ,'SkillId'
                               ,CONVERT(VARCHAR(8000),New.SkillId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.LeadSkillId
                               ,'LeadID'
                               ,CONVERT(VARCHAR(8000),New.LeadId,121)
                        FROM    Inserted New; 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[adLeadSkills_Audit_Update] ON [dbo].[adLeadSkills]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adLeadSkills','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(SkillId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.LeadSkillId
                                   ,'SkillId'
                                   ,CONVERT(VARCHAR(8000),Old.SkillId,121)
                                   ,CONVERT(VARCHAR(8000),New.SkillId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.LeadSkillId = New.LeadSkillId
                            WHERE   Old.SkillId <> New.SkillId
                                    OR (
                                         Old.SkillId IS NULL
                                         AND New.SkillId IS NOT NULL
                                       )
                                    OR (
                                         New.SkillId IS NULL
                                         AND Old.SkillId IS NOT NULL
                                       ); 
                IF UPDATE(LeadId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.LeadSkillId
                                   ,'LeadID'
                                   ,CONVERT(VARCHAR(8000),Old.LeadId,121)
                                   ,CONVERT(VARCHAR(8000),New.LeadId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.LeadSkillId = New.LeadSkillId
                            WHERE   Old.LeadId <> New.LeadId
                                    OR (
                                         Old.LeadId IS NULL
                                         AND New.LeadId IS NOT NULL
                                       )
                                    OR (
                                         New.LeadId IS NULL
                                         AND Old.LeadId IS NOT NULL
                                       ); 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[adLeadSkills] ADD CONSTRAINT [PK_adLeadSkills_LeadSkillId] PRIMARY KEY CLUSTERED  ([LeadSkillId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adLeadSkills] ADD CONSTRAINT [FK_adLeadSkills_adLeads_LeadId_LeadId] FOREIGN KEY ([LeadId]) REFERENCES [dbo].[adLeads] ([LeadId])
GO
ALTER TABLE [dbo].[adLeadSkills] ADD CONSTRAINT [FK_adLeadSkills_plSkills_SkillId_SkillId] FOREIGN KEY ([SkillId]) REFERENCES [dbo].[plSkills] ([SkillId])
GO
