CREATE TABLE [dbo].[syStudentAttendanceSummary]
(
[StuEnrollId] [uniqueidentifier] NULL,
[ClsSectionId] [uniqueidentifier] NULL,
[StudentAttendedDate] [datetime] NULL,
[ScheduledDays] [decimal] (18, 2) NULL,
[ActualDays] [decimal] (18, 2) NULL,
[ActualRunningScheduledDays] [decimal] (18, 2) NULL,
[ActualRunningPresentDays] [decimal] (18, 2) NULL,
[ActualRunningAbsentDays] [decimal] (18, 2) NULL,
[ActualRunningMakeupDays] [decimal] (18, 2) NULL,
[ActualRunningTardyDays] [decimal] (18, 2) NULL,
[AdjustedPresentDays] [decimal] (18, 2) NULL,
[AdjustedAbsentDays] [decimal] (18, 2) NULL,
[AttendanceTrackType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[TermId] [uniqueidentifier] NULL,
[ActualPresentDays_ConvertTo_Hours_Calc] [decimal] (18, 2) NULL,
[ScheduledHours] [decimal] (18, 2) NULL,
[ActualAbsentDays_ConvertTo_Hours_Calc] [decimal] (18, 2) NULL,
[TermStartDate] [datetime] NULL,
[ActualPresentDays_ConvertTo_Hours] [decimal] (18, 2) NULL,
[ActualAbsentDays_ConvertTo_Hours] [decimal] (18, 2) NULL,
[ActualTardyDays_ConvertTo_Hours] [decimal] (18, 2) NULL,
[TermDescrip] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[tardiesmakingabsence] [int] NULL,
[IsExcused] [bit] NULL CONSTRAINT [DF_syStudentAttendanceSummary_IsExcused] DEFAULT ((0)),
[ActualRunningExcusedDays] [int] NULL CONSTRAINT [DF_syStudentAttendanceSummary_ActualRunningExcusedDays] DEFAULT ((0)),
[IsTardy] [int] NULL CONSTRAINT [DF_syStudentAttendanceSummary_IsTardy] DEFAULT ((0)),
[StudentAttendanceSummaryId] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syStudentAttendanceSummary] ADD CONSTRAINT [PK_syStudentAttendanceSummary_StudentAttendanceSummaryId] PRIMARY KEY CLUSTERED  ([StudentAttendanceSummaryId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_syStudentAttendanceSummary_C1_C6_C9_C11_C12] ON [dbo].[syStudentAttendanceSummary] ([StuEnrollId]) INCLUDE ([ActualRunningMakeupDays], [ActualRunningScheduledDays], [AdjustedAbsentDays], [AdjustedPresentDays]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_syStudentAttendanceSummary_C1_C3_C6_C9_C11_C12] ON [dbo].[syStudentAttendanceSummary] ([StuEnrollId], [StudentAttendedDate]) INCLUDE ([ActualRunningMakeupDays], [ActualRunningScheduledDays], [AdjustedAbsentDays], [AdjustedPresentDays]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_syStudentAttendanceSummary_StuEnrollId_StudentAttendedDate_TermId_ActualAbsentDays_ConvertTo_Hours_Calc] ON [dbo].[syStudentAttendanceSummary] ([StuEnrollId], [StudentAttendedDate], [TermId]) INCLUDE ([ActualAbsentDays_ConvertTo_Hours_Calc]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_syStudentAttendanceSummary_StuEnrollId_StudentAttendedDate_TermId_ActualPresentDays_ConvertTo_Hours_Calc] ON [dbo].[syStudentAttendanceSummary] ([StuEnrollId], [StudentAttendedDate], [TermId]) INCLUDE ([ActualPresentDays_ConvertTo_Hours_Calc]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_syStudentAttendanceSummary_StuEnrollId_StudentAttendedDate_TermId_ScheduledHours] ON [dbo].[syStudentAttendanceSummary] ([StuEnrollId], [StudentAttendedDate], [TermId]) INCLUDE ([ScheduledHours]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_syStudentAttendanceSummary_StuEnrollId_TermStartDate_TermId_ActualAbsentDays_ConvertTo_Hours] ON [dbo].[syStudentAttendanceSummary] ([StuEnrollId], [TermStartDate], [TermId]) INCLUDE ([ActualAbsentDays_ConvertTo_Hours]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_syStudentAttendanceSummary_StuEnrollId_TermStartDate_TermId_ActualPresentDays_ConvertTo_Hours] ON [dbo].[syStudentAttendanceSummary] ([StuEnrollId], [TermStartDate], [TermId]) INCLUDE ([ActualPresentDays_ConvertTo_Hours]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_syStudentAttendanceSummary_StuEnrollId_TermStartDate_TermId_ActualTardyDays_ConvertTo_Hours] ON [dbo].[syStudentAttendanceSummary] ([StuEnrollId], [TermStartDate], [TermId]) INCLUDE ([ActualTardyDays_ConvertTo_Hours]) ON [PRIMARY]
GO
