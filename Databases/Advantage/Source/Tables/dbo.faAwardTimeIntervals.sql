CREATE TABLE [dbo].[faAwardTimeIntervals]
(
[AwdTimeIntervalId] [int] NOT NULL,
[AwdTimeIntervalDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[faAwardTimeIntervals] ADD CONSTRAINT [PK_faAwardTimeIntervals_AwdTimeIntervalId] PRIMARY KEY CLUSTERED  ([AwdTimeIntervalId]) ON [PRIMARY]
GO
