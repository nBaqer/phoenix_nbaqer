CREATE TABLE [dbo].[arClassSectionTerms]
(
[ClsSectTermId] [uniqueidentifier] NOT NULL,
[ClsSectionId] [uniqueidentifier] NOT NULL,
[TermId] [uniqueidentifier] NOT NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[arClassSectionTerms_Audit_Delete] ON [dbo].[arClassSectionTerms]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arClassSectionTerms','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ClsSectTermId
                               ,'ClsSectionId'
                               ,CONVERT(VARCHAR(8000),Old.ClsSectionId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ClsSectTermId
                               ,'TermId'
                               ,CONVERT(VARCHAR(8000),Old.TermId,121)
                        FROM    Deleted Old; 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[arClassSectionTerms_Audit_Insert] ON [dbo].[arClassSectionTerms]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arClassSectionTerms','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ClsSectTermId
                               ,'ClsSectionId'
                               ,CONVERT(VARCHAR(8000),New.ClsSectionId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ClsSectTermId
                               ,'TermId'
                               ,CONVERT(VARCHAR(8000),New.TermId,121)
                        FROM    Inserted New; 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[arClassSectionTerms_Audit_Update] ON [dbo].[arClassSectionTerms]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arClassSectionTerms','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(ClsSectionId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ClsSectTermId
                                   ,'ClsSectionId'
                                   ,CONVERT(VARCHAR(8000),Old.ClsSectionId,121)
                                   ,CONVERT(VARCHAR(8000),New.ClsSectionId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ClsSectTermId = New.ClsSectTermId
                            WHERE   Old.ClsSectionId <> New.ClsSectionId
                                    OR (
                                         Old.ClsSectionId IS NULL
                                         AND New.ClsSectionId IS NOT NULL
                                       )
                                    OR (
                                         New.ClsSectionId IS NULL
                                         AND Old.ClsSectionId IS NOT NULL
                                       ); 
                IF UPDATE(TermId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ClsSectTermId
                                   ,'TermId'
                                   ,CONVERT(VARCHAR(8000),Old.TermId,121)
                                   ,CONVERT(VARCHAR(8000),New.TermId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ClsSectTermId = New.ClsSectTermId
                            WHERE   Old.TermId <> New.TermId
                                    OR (
                                         Old.TermId IS NULL
                                         AND New.TermId IS NOT NULL
                                       )
                                    OR (
                                         New.TermId IS NULL
                                         AND Old.TermId IS NOT NULL
                                       ); 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[arClassSectionTerms] ADD CONSTRAINT [PK_arClassSectionTerms_ClsSectTermId] PRIMARY KEY CLUSTERED  ([ClsSectTermId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arClassSectionTerms] ADD CONSTRAINT [FK_arClassSectionTerms_arClassSections_ClsSectionId_ClsSectionId] FOREIGN KEY ([ClsSectionId]) REFERENCES [dbo].[arClassSections] ([ClsSectionId])
GO
ALTER TABLE [dbo].[arClassSectionTerms] ADD CONSTRAINT [FK_arClassSectionTerms_arTerm_TermId_TermId] FOREIGN KEY ([TermId]) REFERENCES [dbo].[arTerm] ([TermId])
GO
