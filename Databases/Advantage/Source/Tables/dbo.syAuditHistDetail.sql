CREATE TABLE [dbo].[syAuditHistDetail]
(
[AuditHistDetailId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syAuditHistDetail_AuditHistDetailId] DEFAULT (newid()),
[AuditHistId] [uniqueidentifier] NOT NULL,
[RowId] [uniqueidentifier] NOT NULL,
[ColumnName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OldValue] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NewValue] [varchar] (8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syAuditHistDetail] ADD CONSTRAINT [PK_syAuditHistDetail_AuditHistDetailId] PRIMARY KEY CLUSTERED  ([AuditHistDetailId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_syAuditHistDetail_ColumnName] ON [dbo].[syAuditHistDetail] ([ColumnName]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_syAuditHistDetail_NewValue] ON [dbo].[syAuditHistDetail] ([NewValue]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_syAuditHistDetail_OldValue] ON [dbo].[syAuditHistDetail] ([OldValue]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_syAuditHistDetail_AuditHistId_RowId] ON [dbo].[syAuditHistDetail] ([RowId], [AuditHistId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_syAuditHistDetail_RowId_AuditHistId] ON [dbo].[syAuditHistDetail] ([RowId], [AuditHistId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syAuditHistDetail] ADD CONSTRAINT [FK_syAuditHistDetail_syAuditHist_AuditHistId_AuditHistId] FOREIGN KEY ([AuditHistId]) REFERENCES [dbo].[syAuditHist] ([AuditHistId])
GO
