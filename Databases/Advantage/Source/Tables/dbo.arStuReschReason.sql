CREATE TABLE [dbo].[arStuReschReason]
(
[ReschReasonTypeId] [uniqueidentifier] NOT NULL,
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StuEnrollId] [uniqueidentifier] NOT NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CampusId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arStuReschReason] ADD CONSTRAINT [PK_arStuReschReason_ReschReasonTypeId] PRIMARY KEY CLUSTERED  ([ReschReasonTypeId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arStuReschReason] ADD CONSTRAINT [FK_arStuReschReason_syCampuses_CampusId_CampusId] FOREIGN KEY ([CampusId]) REFERENCES [dbo].[syCampuses] ([CampusId])
GO
ALTER TABLE [dbo].[arStuReschReason] ADD CONSTRAINT [FK_arStuReschReason_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY ([StuEnrollId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
