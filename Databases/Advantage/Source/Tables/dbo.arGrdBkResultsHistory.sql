CREATE TABLE [dbo].[arGrdBkResultsHistory]
(
[GrdBkResultHistoryId] [uniqueidentifier] NOT NULL,
[ModUserHistory] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDateHistory] [datetime] NOT NULL,
[GrdBkResultId] [uniqueidentifier] NOT NULL,
[Score] [decimal] (6, 2) NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL,
[ResNum] [int] NOT NULL,
[PostDate] [smalldatetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arGrdBkResultsHistory] ADD CONSTRAINT [PK__arGrdBkResultsHistory__GrdBkResultHistoryId] PRIMARY KEY CLUSTERED  ([GrdBkResultHistoryId]) ON [PRIMARY]
GO
