CREATE TABLE [dbo].[adFullPartTime]
(
[FullTimeId] [uniqueidentifier] NOT NULL,
[FullPartTimeCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FullPartTimeDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusId] [uniqueidentifier] NULL,
[CampGrpId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adFullPartTime] ADD CONSTRAINT [PK_adFullPartTime_FullTimeId] PRIMARY KEY CLUSTERED  ([FullTimeId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adFullPartTime] ADD CONSTRAINT [FK_adFullPartTime_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[adFullPartTime] ADD CONSTRAINT [FK_adFullPartTime_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
