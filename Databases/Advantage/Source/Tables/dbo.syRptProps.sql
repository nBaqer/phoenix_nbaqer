CREATE TABLE [dbo].[syRptProps]
(
[RptPropsId] [smallint] NOT NULL,
[ResourceId] [smallint] NOT NULL,
[AllowParams] [bit] NOT NULL,
[RptSQLId] [smallint] NULL,
[RptObjId] [smallint] NULL,
[AllowDescrip] [bit] NULL,
[AllowInstruct] [bit] NULL,
[AllowNotes] [bit] NULL,
[AllowFilters] [bit] NOT NULL CONSTRAINT [DF_syRptProps_AllowFilters] DEFAULT ((1)),
[AllowSortOrder] [bit] NOT NULL CONSTRAINT [DF_syRptProps_AllowSortOrder] DEFAULT ((1)),
[SelectFilters] [bit] NOT NULL CONSTRAINT [DF_syRptProps_SelectFilters] DEFAULT ((1)),
[SelectSortOrder] [bit] NOT NULL CONSTRAINT [DF_syRptProps_SelectSortOrder] DEFAULT ((0)),
[AllowDateIssue] [bit] NOT NULL CONSTRAINT [DF_syRptProps_AllowDateIssue] DEFAULT ((0)),
[AllowStudentGroup] [bit] NOT NULL CONSTRAINT [DF_syRptProps_AllowStudentGroup] DEFAULT ((0)),
[AllowProgramGroup] [bit] NOT NULL CONSTRAINT [DF_syRptProps_AllowProgramGroup] DEFAULT ((0)),
[ShowClassDates] [bit] NOT NULL CONSTRAINT [DF_syRptProps_ShowClassDates] DEFAULT ((0)),
[ShowProjExceedsBal] [bit] NOT NULL CONSTRAINT [DF_syRptProps_ShowProjExceedsBal] DEFAULT ((0)),
[ShowCosts] [bit] NOT NULL CONSTRAINT [DF_syRptProps_ShowCosts] DEFAULT ((0)),
[ShowExpectedFunding] [bit] NOT NULL CONSTRAINT [DF_syRptProps_ShowExpectedFunding] DEFAULT ((0)),
[ShowCategoryBreakdown] [bit] NOT NULL CONSTRAINT [DF_syRptProps_ShowCategoryBreakdown] DEFAULT ((0)),
[ShowEnrollmentGroup] [bit] NOT NULL CONSTRAINT [DF_syRptProps_ShowEnrollmentGroup] DEFAULT ((0)),
[ShowDisbNotBeenPaid] [bit] NOT NULL CONSTRAINT [DF_syRptProps_ShowDisbNotBeenPaid] DEFAULT ((0)),
[ShowLegalDisclaimer] [bit] NOT NULL CONSTRAINT [DF_syRptProps_ShowLegalDisclaimer] DEFAULT ((0)),
[BaseCAOnRegClasses] [bit] NULL CONSTRAINT [DF_syRptProps_BaseCAOnRegClasses] DEFAULT ((0)),
[ShowTransferCampus] [bit] NULL CONSTRAINT [DF_syRptProps_ShowTransferCampus] DEFAULT ((0)),
[ShowTransferProgram] [bit] NULL CONSTRAINT [DF_syRptProps_ShowTransferProgram] DEFAULT ((0)),
[ShowLDA] [bit] NULL CONSTRAINT [DF_syRptProps_ShowLDA] DEFAULT ((0)),
[ShowUseStuCurrStatus] [bit] NULL CONSTRAINT [DF_syRptProps_ShowUseStuCurrStatus] DEFAULT ((0)),
[ShowUseSignLineForAttnd] [bit] NULL,
[ShowTermProgressDescription] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syRptProps] ADD CONSTRAINT [PK_syRptProps_RptPropsId] PRIMARY KEY CLUSTERED  ([RptPropsId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syRptProps] ADD CONSTRAINT [FK_syRptProps_syBusObjects_RptObjId_BusObjectId] FOREIGN KEY ([RptObjId]) REFERENCES [dbo].[syBusObjects] ([BusObjectId])
GO
ALTER TABLE [dbo].[syRptProps] ADD CONSTRAINT [FK_syRptProps_syResources_ResourceId_ResourceID] FOREIGN KEY ([ResourceId]) REFERENCES [dbo].[syResources] ([ResourceID])
GO
GRANT SELECT ON  [dbo].[syRptProps] TO [AdvantageRole]
GO
