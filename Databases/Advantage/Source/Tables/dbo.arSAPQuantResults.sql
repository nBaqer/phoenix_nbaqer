CREATE TABLE [dbo].[arSAPQuantResults]
(
[arSAPQuantResultID] [uniqueidentifier] NOT NULL CONSTRAINT [DF_arSAPQuantResults_arSAPQuantResultID] DEFAULT (newid()),
[StuEnrollId] [uniqueidentifier] NOT NULL,
[InstructionTypeId] [uniqueidentifier] NOT NULL,
[SAPDetailId] [uniqueidentifier] NOT NULL,
[PercentCompleted] [decimal] (19, 2) NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arSAPQuantResults] ADD CONSTRAINT [PK_arSAPQuantResults_arSAPQuantResultID] PRIMARY KEY CLUSTERED  ([arSAPQuantResultID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arSAPQuantResults] ADD CONSTRAINT [FK_arSAPQuantResults_arSAPQuantResults_arSAPQuantResultID_arSAPQuantResultID] FOREIGN KEY ([arSAPQuantResultID]) REFERENCES [dbo].[arSAPQuantResults] ([arSAPQuantResultID])
GO
ALTER TABLE [dbo].[arSAPQuantResults] ADD CONSTRAINT [FK_arSAPQuantResults_arInstructionType_InstructionTypeId_InstructionTypeId] FOREIGN KEY ([InstructionTypeId]) REFERENCES [dbo].[arInstructionType] ([InstructionTypeId])
GO
ALTER TABLE [dbo].[arSAPQuantResults] ADD CONSTRAINT [FK_arSAPQuantResults_arSAPDetails_SAPDetailId_SAPDetailId] FOREIGN KEY ([SAPDetailId]) REFERENCES [dbo].[arSAPDetails] ([SAPDetailId])
GO
ALTER TABLE [dbo].[arSAPQuantResults] ADD CONSTRAINT [FK_arSAPQuantResults_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY ([StuEnrollId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
