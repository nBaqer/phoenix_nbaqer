CREATE TABLE [dbo].[ParamSet]
(
[SetId] [bigint] NOT NULL IDENTITY(1, 1),
[SetName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SetDisplayName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SetDescription] [nvarchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SetType] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ParamSet] ADD CONSTRAINT [PK_ParamSet_SetId] PRIMARY KEY CLUSTERED  ([SetId]) ON [PRIMARY]
GO
