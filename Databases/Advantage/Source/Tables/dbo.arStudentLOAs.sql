CREATE TABLE [dbo].[arStudentLOAs]
(
[StudentLOAId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arStudentLOAs_StudentLOAId] DEFAULT (newid()),
[StuEnrollId] [uniqueidentifier] NOT NULL,
[StartDate] [datetime] NOT NULL,
[EndDate] [datetime] NOT NULL,
[LOAReasonId] [uniqueidentifier] NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LOAReturnDate] [datetime] NULL,
[StudentStatusChangeId] [uniqueidentifier] NULL,
[StatusCodeId] [uniqueidentifier] NULL,
[LOARequestDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arStudentLOAs] ADD CONSTRAINT [PK_arStudentLOAs_StudentLOAId] PRIMARY KEY CLUSTERED  ([StudentLOAId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arStudentLOAs] ADD CONSTRAINT [FK_arStudentLOAs_arLOAReasons_LOAReasonId_LOAReasonId] FOREIGN KEY ([LOAReasonId]) REFERENCES [dbo].[arLOAReasons] ([LOAReasonId])
GO
ALTER TABLE [dbo].[arStudentLOAs] ADD CONSTRAINT [FK_arStudentLOAs_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY ([StuEnrollId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
