CREATE TABLE [dbo].[plWorkDays]
(
[WorkDaysId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_plWorkDays_WorkDaysId] DEFAULT (newid()),
[WorkDaysDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ViewOrder] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[plWorkDays] ADD CONSTRAINT [PK_plWorkDays_WorkDaysId] PRIMARY KEY CLUSTERED  ([WorkDaysId]) ON [PRIMARY]
GO
