CREATE TABLE [dbo].[syWapiOperationLogger]
(
[Id] [bigint] NOT NULL IDENTITY(1, 1),
[ServiceCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CompanyCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DateExecution] [datetime2] NOT NULL,
[NextPlanningDate] [datetime2] NULL,
[IsError] [bit] NOT NULL,
[Comment] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syWapiOperationLogger] ADD CONSTRAINT [PK_syWapiOperationLogger_Id] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
