CREATE TABLE [dbo].[saRateSchedules]
(
[RateScheduleId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_saRateSchedules_RateScheduleId] DEFAULT (newid()),
[RateScheduleCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[RateScheduleDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saRateSchedules_Audit_Delete] ON [dbo].[saRateSchedules]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saRateSchedules','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.RateScheduleId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.RateScheduleId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.RateScheduleId
                               ,'RateScheduleDescrip'
                               ,CONVERT(VARCHAR(8000),Old.RateScheduleDescrip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.RateScheduleId
                               ,'RateScheduleCode'
                               ,CONVERT(VARCHAR(8000),Old.RateScheduleCode,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF; 

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saRateSchedules_Audit_Insert] ON [dbo].[saRateSchedules]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saRateSchedules','I',@EventRows,@EventDate,@UserName;; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.RateScheduleId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.RateScheduleId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.RateScheduleId
                               ,'RateScheduleDescrip'
                               ,CONVERT(VARCHAR(8000),New.RateScheduleDescrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.RateScheduleId
                               ,'RateScheduleCode'
                               ,CONVERT(VARCHAR(8000),New.RateScheduleCode,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF; 

GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saRateSchedules_Audit_Update] ON [dbo].[saRateSchedules]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saRateSchedules','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.RateScheduleId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.RateScheduleId = New.RateScheduleId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.RateScheduleId
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.RateScheduleId = New.RateScheduleId
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 
                IF UPDATE(RateScheduleDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.RateScheduleId
                                   ,'RateScheduleDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.RateScheduleDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.RateScheduleDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.RateScheduleId = New.RateScheduleId
                            WHERE   Old.RateScheduleDescrip <> New.RateScheduleDescrip
                                    OR (
                                         Old.RateScheduleDescrip IS NULL
                                         AND New.RateScheduleDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.RateScheduleDescrip IS NULL
                                         AND Old.RateScheduleDescrip IS NOT NULL
                                       ); 
                IF UPDATE(RateScheduleCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.RateScheduleId
                                   ,'RateScheduleCode'
                                   ,CONVERT(VARCHAR(8000),Old.RateScheduleCode,121)
                                   ,CONVERT(VARCHAR(8000),New.RateScheduleCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.RateScheduleId = New.RateScheduleId
                            WHERE   Old.RateScheduleCode <> New.RateScheduleCode
                                    OR (
                                         Old.RateScheduleCode IS NULL
                                         AND New.RateScheduleCode IS NOT NULL
                                       )
                                    OR (
                                         New.RateScheduleCode IS NULL
                                         AND Old.RateScheduleCode IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[saRateSchedules] ADD CONSTRAINT [PK_saRateSchedules_RateScheduleId] PRIMARY KEY CLUSTERED  ([RateScheduleId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_saRateSchedules_RateScheduleCode_RateScheduleDescrip_CampGrpId] ON [dbo].[saRateSchedules] ([RateScheduleCode], [RateScheduleDescrip], [CampGrpId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saRateSchedules] ADD CONSTRAINT [FK_saRateSchedules_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[saRateSchedules] ADD CONSTRAINT [FK_saRateSchedules_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
