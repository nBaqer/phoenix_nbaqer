CREATE TABLE [dbo].[syResources]
(
[ResourceID] [smallint] NOT NULL,
[Resource] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResourceTypeID] [tinyint] NOT NULL,
[ResourceURL] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SummListId] [smallint] NULL,
[ChildTypeId] [tinyint] NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AllowSchlReqFlds] [bit] NULL CONSTRAINT [DF_syResources_AllowSchlReqFlds] DEFAULT ((0)),
[MRUTypeId] [smallint] NULL,
[UsedIn] [int] NULL,
[TblFldsId] [int] NULL,
[DisplayName] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syResources] ADD CONSTRAINT [PK_syResources_ResourceID] PRIMARY KEY CLUSTERED  ([ResourceID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_syResources_ResourceTypeID_ResourceID_Resource] ON [dbo].[syResources] ([ResourceTypeID], [ResourceID], [Resource]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syResources] ADD CONSTRAINT [FK_syResources_syResourceTypes_ResourceTypeID_ResourceTypeID] FOREIGN KEY ([ResourceTypeID]) REFERENCES [dbo].[syResourceTypes] ([ResourceTypeID])
GO
GRANT SELECT ON  [dbo].[syResources] TO [AdvantageRole]
GO
