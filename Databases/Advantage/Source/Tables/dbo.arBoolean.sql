CREATE TABLE [dbo].[arBoolean]
(
[boolValue] [bit] NOT NULL,
[BoolDescrip] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arBoolean] ADD CONSTRAINT [PK_arBoolean_boolValue] PRIMARY KEY CLUSTERED  ([boolValue]) ON [PRIMARY]
GO
