CREATE TABLE [dbo].[arPrgGrp]
(
[PrgGrpId] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[PrgGrpCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NULL,
[PrgGrpDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[CampGrpId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[arPrgGrp_Audit_Delete] ON [dbo].[arPrgGrp]
    FOR DELETE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arPrgGrp','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgGrpId
                               ,'PrgGrpDescrip'
                               ,CONVERT(VARCHAR(8000),Old.PrgGrpDescrip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgGrpId
                               ,'PrgGrpCode'
                               ,CONVERT(VARCHAR(8000),Old.PrgGrpCode,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgGrpId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
            END; 
        END; 



    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[arPrgGrp_Audit_Insert] ON [dbo].[arPrgGrp]
    FOR INSERT
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arPrgGrp','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgGrpId
                               ,'PrgGrpDescrip'
                               ,CONVERT(VARCHAR(8000),New.PrgGrpDescrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgGrpId
                               ,'PrgGrpCode'
                               ,CONVERT(VARCHAR(8000),New.PrgGrpCode,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgGrpId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
            END; 
        END; 



    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[arPrgGrp_Audit_Update] ON [dbo].[arPrgGrp]
    FOR UPDATE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arPrgGrp','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(PrgGrpDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgGrpId
                                   ,'PrgGrpDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.PrgGrpDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.PrgGrpDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgGrpId = New.PrgGrpId
                            WHERE   Old.PrgGrpDescrip <> New.PrgGrpDescrip
                                    OR (
                                         Old.PrgGrpDescrip IS NULL
                                         AND New.PrgGrpDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.PrgGrpDescrip IS NULL
                                         AND Old.PrgGrpDescrip IS NOT NULL
                                       ); 
                IF UPDATE(PrgGrpCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgGrpId
                                   ,'PrgGrpCode'
                                   ,CONVERT(VARCHAR(8000),Old.PrgGrpCode,121)
                                   ,CONVERT(VARCHAR(8000),New.PrgGrpCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgGrpId = New.PrgGrpId
                            WHERE   Old.PrgGrpCode <> New.PrgGrpCode
                                    OR (
                                         Old.PrgGrpCode IS NULL
                                         AND New.PrgGrpCode IS NOT NULL
                                       )
                                    OR (
                                         New.PrgGrpCode IS NULL
                                         AND Old.PrgGrpCode IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgGrpId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgGrpId = New.PrgGrpId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
            END; 
        END; 



    SET NOCOUNT OFF;

GO
ALTER TABLE [dbo].[arPrgGrp] ADD CONSTRAINT [PK_arPrgGrp_PrgGrpId] PRIMARY KEY CLUSTERED  ([PrgGrpId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_arPrgGrp_PrgGrpCode_PrgGrpDescrip_CampGrpId] ON [dbo].[arPrgGrp] ([PrgGrpCode], [PrgGrpDescrip], [CampGrpId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arPrgGrp] ADD CONSTRAINT [FK_arPrgGrp_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[arPrgGrp] ADD CONSTRAINT [FK_arPrgGrp_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
