CREATE TABLE [dbo].[rptLeadStatus]
(
[StatusCodeId] [uniqueidentifier] NOT NULL,
[StatusCodeDescrip] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[CampusId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[rptLeadStatus] ADD CONSTRAINT [PK_rptLeadStatus_StatusCodeId] PRIMARY KEY CLUSTERED  ([StatusCodeId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[rptLeadStatus] ADD CONSTRAINT [FK_rptLeadStatus_syCampuses_CampusId_CampusId] FOREIGN KEY ([CampusId]) REFERENCES [dbo].[syCampuses] ([CampusId])
GO
ALTER TABLE [dbo].[rptLeadStatus] ADD CONSTRAINT [FK_rptLeadStatus_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
