CREATE TABLE [dbo].[syResourceSdf]
(
[PkId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syResourceSdf_PkId] DEFAULT (newid()),
[ResourceId] [int] NULL,
[sdfID] [uniqueidentifier] NULL,
[SDFVisibilty] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[EntityId] [int] NULL,
[Position] [int] NOT NULL CONSTRAINT [DF_syResourceSdf_Position] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[syResourceSdf_Audit_Delete] ON [dbo].[syResourceSdf]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syResourceSdf','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PkId
                               ,'ResourceID'
                               ,CONVERT(VARCHAR(8000),Old.ResourceId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PkId
                               ,'SDFId'
                               ,CONVERT(VARCHAR(8000),Old.sdfID,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PkId
                               ,'SDFVisibilty'
                               ,CONVERT(VARCHAR(8000),Old.SDFVisibilty,121)
                        FROM    Deleted Old; 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[syResourceSdf_Audit_Insert] ON [dbo].[syResourceSdf]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syResourceSdf','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PkId
                               ,'ResourceID'
                               ,CONVERT(VARCHAR(8000),New.ResourceId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PkId
                               ,'SDFId'
                               ,CONVERT(VARCHAR(8000),New.sdfID,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PkId
                               ,'SDFVisibilty'
                               ,CONVERT(VARCHAR(8000),New.SDFVisibilty,121)
                        FROM    Inserted New; 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[syResourceSdf_Audit_Update] ON [dbo].[syResourceSdf]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syResourceSdf','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(ResourceId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PkId
                                   ,'ResourceID'
                                   ,CONVERT(VARCHAR(8000),Old.ResourceId,121)
                                   ,CONVERT(VARCHAR(8000),New.ResourceId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PkId = New.PkId
                            WHERE   Old.ResourceId <> New.ResourceId
                                    OR (
                                         Old.ResourceId IS NULL
                                         AND New.ResourceId IS NOT NULL
                                       )
                                    OR (
                                         New.ResourceId IS NULL
                                         AND Old.ResourceId IS NOT NULL
                                       ); 
            
                IF UPDATE(SDFID)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PkId
                                   ,'SDFId'
                                   ,CONVERT(VARCHAR(8000),Old.sdfID,121)
                                   ,CONVERT(VARCHAR(8000),New.sdfID,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PkId = New.PkId
                            WHERE   Old.sdfID <> New.sdfID
                                    OR (
                                         Old.sdfID IS NULL
                                         AND New.sdfID IS NOT NULL
                                       )
                                    OR (
                                         New.sdfID IS NULL
                                         AND Old.sdfID IS NOT NULL
                                       ); 
                IF UPDATE(SDFVisibilty)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PkId
                                   ,'SDFVisibilty'
                                   ,CONVERT(VARCHAR(8000),Old.SDFVisibilty,121)
                                   ,CONVERT(VARCHAR(8000),New.SDFVisibilty,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PkId = New.PkId
                            WHERE   Old.SDFVisibilty <> New.SDFVisibilty
                                    OR (
                                         Old.SDFVisibilty IS NULL
                                         AND New.SDFVisibilty IS NOT NULL
                                       )
                                    OR (
                                         New.SDFVisibilty IS NULL
                                         AND Old.SDFVisibilty IS NOT NULL
                                       ); 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO
ALTER TABLE [dbo].[syResourceSdf] ADD CONSTRAINT [PK_syResourceSdf_PkId] PRIMARY KEY CLUSTERED  ([PkId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syResourceSdf] ADD CONSTRAINT [UIX_syResourceSdf_ResourceId_sdfID_EntityId] UNIQUE NONCLUSTERED  ([ResourceId], [sdfID], [EntityId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syResourceSdf] ADD CONSTRAINT [FK_syResourceSdf_sySDF_sdfID_SDFId] FOREIGN KEY ([sdfID]) REFERENCES [dbo].[sySDF] ([SDFId])
GO
GRANT SELECT ON  [dbo].[syResourceSdf] TO [AdvantageRole]
GO
