CREATE TABLE [dbo].[adStuGrpStudents]
(
[StuGrpStuId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_adStuGrpTypes_StuGrpStuId] DEFAULT (newsequentialid()),
[StuGrpId] [uniqueidentifier] NOT NULL,
[StudentId] [uniqueidentifier] NOT NULL,
[DateAdded] [datetime] NOT NULL,
[DateRemoved] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL,
[IsDeleted] [bit] NULL CONSTRAINT [DF_adStuGrpStudents_IsDeleted] DEFAULT ((0)),
[StuEnrollId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adStuGrpStudents] ADD CONSTRAINT [PK_adStuGrpStudents_StuGrpStuId] PRIMARY KEY CLUSTERED  ([StuGrpStuId]) ON [PRIMARY]
GO
