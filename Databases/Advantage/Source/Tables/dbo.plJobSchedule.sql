CREATE TABLE [dbo].[plJobSchedule]
(
[JobScheduleId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_plJobSchedule_JobScheduleId] DEFAULT (newid()),
[JobScheduleCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[JobScheduleDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CamGrpId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[plJobSchedule] ADD CONSTRAINT [PK_plJobSchedule_JobScheduleId] PRIMARY KEY CLUSTERED  ([JobScheduleId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[plJobSchedule] ADD CONSTRAINT [FK_plJobSchedule_syCampGrps_CamGrpId_CampGrpId] FOREIGN KEY ([CamGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[plJobSchedule] ADD CONSTRAINT [FK_plJobSchedule_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
