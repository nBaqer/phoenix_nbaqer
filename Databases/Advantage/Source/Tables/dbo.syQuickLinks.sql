CREATE TABLE [dbo].[syQuickLinks]
(
[QuickLinkID] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syQuickLinks_QuickLinkId] DEFAULT (newid()),
[ModuleID] [int] NOT NULL,
[StatusID] [uniqueidentifier] NOT NULL,
[Description] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[URL] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[syQuickLinks_Audit_Delete] ON [dbo].[syQuickLinks]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syQuickLinks','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.QuickLinkID
                               ,'ModuleId'
                               ,CONVERT(VARCHAR(8000),Old.ModuleID,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.QuickLinkID
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusID,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.QuickLinkID
                               ,'Description'
                               ,CONVERT(VARCHAR(8000),Old.Description,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.QuickLinkID
                               ,'URL'
                               ,CONVERT(VARCHAR(8000),Old.URL,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[syQuickLinks_Audit_Insert] ON [dbo].[syQuickLinks]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syQuickLinks','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.QuickLinkID
                               ,'ModuleId'
                               ,CONVERT(VARCHAR(8000),New.ModuleID,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.QuickLinkID
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusID,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.QuickLinkID
                               ,'Description'
                               ,CONVERT(VARCHAR(8000),New.Description,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.QuickLinkID
                               ,'URL'
                               ,CONVERT(VARCHAR(8000),New.URL,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[syQuickLinks_Audit_Update] ON [dbo].[syQuickLinks]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syQuickLinks','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(ModuleID)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.QuickLinkID
                                   ,'ModuleId'
                                   ,CONVERT(VARCHAR(8000),Old.ModuleID,121)
                                   ,CONVERT(VARCHAR(8000),New.ModuleID,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.QuickLinkID = New.QuickLinkID
                            WHERE   Old.ModuleID <> New.ModuleID
                                    OR (
                                         Old.ModuleID IS NULL
                                         AND New.ModuleID IS NOT NULL
                                       )
                                    OR (
                                         New.ModuleID IS NULL
                                         AND Old.ModuleID IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.QuickLinkID
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusID,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusID,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.QuickLinkID = New.QuickLinkID
                            WHERE   Old.StatusID <> New.StatusID
                                    OR (
                                         Old.StatusID IS NULL
                                         AND New.StatusID IS NOT NULL
                                       )
                                    OR (
                                         New.StatusID IS NULL
                                         AND Old.StatusID IS NOT NULL
                                       ); 
                IF UPDATE(Description)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.QuickLinkID
                                   ,'Description'
                                   ,CONVERT(VARCHAR(8000),Old.Description,121)
                                   ,CONVERT(VARCHAR(8000),New.Description,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.QuickLinkID = New.QuickLinkID
                            WHERE   Old.Description <> New.Description
                                    OR (
                                         Old.Description IS NULL
                                         AND New.Description IS NOT NULL
                                       )
                                    OR (
                                         New.Description IS NULL
                                         AND Old.Description IS NOT NULL
                                       ); 
                IF UPDATE(Url)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.QuickLinkID
                                   ,'URL'
                                   ,CONVERT(VARCHAR(8000),Old.URL,121)
                                   ,CONVERT(VARCHAR(8000),New.URL,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.QuickLinkID = New.QuickLinkID
                            WHERE   Old.URL <> New.URL
                                    OR (
                                         Old.URL IS NULL
                                         AND New.URL IS NOT NULL
                                       )
                                    OR (
                                         New.URL IS NULL
                                         AND Old.URL IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[syQuickLinks] ADD CONSTRAINT [PK_syQuickLinks_QuickLinkID] PRIMARY KEY CLUSTERED  ([QuickLinkID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syQuickLinks] ADD CONSTRAINT [FK_syQuickLinks_syStatuses_StatusID_StatusId] FOREIGN KEY ([StatusID]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
