CREATE TABLE [dbo].[syIneligibilityReasons]
(
[InelReasonId] [uniqueidentifier] NOT NULL,
[InelReasonName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InelReasonCode] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syIneligibilityReasons] ADD CONSTRAINT [PK__syInelig__0CB92C135B492739] PRIMARY KEY CLUSTERED  ([InelReasonId]) ON [PRIMARY]
GO
