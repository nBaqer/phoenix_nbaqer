CREATE TABLE [dbo].[syPeriods]
(
[PeriodId] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[PeriodCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[PeriodDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[StartTimeId] [uniqueidentifier] NOT NULL,
[EndTimeId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[StartTimeAndEndTimeAreFixed] [bit] NOT NULL CONSTRAINT [DF_syPeriods_StartTimeAndEndTimeAreFixed] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[syPeriods_Audit_Delete] ON [dbo].[syPeriods]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syPeriods','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PeriodId
                               ,'PeriodCode'
                               ,CONVERT(VARCHAR(8000),Old.PeriodCode,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PeriodId
                               ,'PeriodDescrip'
                               ,CONVERT(VARCHAR(8000),Old.PeriodDescrip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PeriodId
                               ,'StartTimeId'
                               ,CONVERT(VARCHAR(8000),Old.StartTimeId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PeriodId
                               ,'EndTimeId'
                               ,CONVERT(VARCHAR(8000),Old.EndTimeId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PeriodId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PeriodId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[syPeriods_Audit_Insert] ON [dbo].[syPeriods]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syPeriods','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PeriodId
                               ,'PeriodCode'
                               ,CONVERT(VARCHAR(8000),New.PeriodCode,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PeriodId
                               ,'PeriodDescrip'
                               ,CONVERT(VARCHAR(8000),New.PeriodDescrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PeriodId
                               ,'StartTimeId'
                               ,CONVERT(VARCHAR(8000),New.StartTimeId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PeriodId
                               ,'EndTimeId'
                               ,CONVERT(VARCHAR(8000),New.EndTimeId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PeriodId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PeriodId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New; 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[syPeriods_Audit_Update] ON [dbo].[syPeriods]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syPeriods','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(PeriodCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PeriodId
                                   ,'PeriodCode'
                                   ,CONVERT(VARCHAR(8000),Old.PeriodCode,121)
                                   ,CONVERT(VARCHAR(8000),New.PeriodCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PeriodId = New.PeriodId
                            WHERE   Old.PeriodCode <> New.PeriodCode
                                    OR (
                                         Old.PeriodCode IS NULL
                                         AND New.PeriodCode IS NOT NULL
                                       )
                                    OR (
                                         New.PeriodCode IS NULL
                                         AND Old.PeriodCode IS NOT NULL
                                       ); 
                IF UPDATE(PeriodDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PeriodId
                                   ,'PeriodDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.PeriodDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.PeriodDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PeriodId = New.PeriodId
                            WHERE   Old.PeriodDescrip <> New.PeriodDescrip
                                    OR (
                                         Old.PeriodDescrip IS NULL
                                         AND New.PeriodDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.PeriodDescrip IS NULL
                                         AND Old.PeriodDescrip IS NOT NULL
                                       ); 
                IF UPDATE(StartTimeId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PeriodId
                                   ,'StartTimeId'
                                   ,CONVERT(VARCHAR(8000),Old.StartTimeId,121)
                                   ,CONVERT(VARCHAR(8000),New.StartTimeId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PeriodId = New.PeriodId
                            WHERE   Old.StartTimeId <> New.StartTimeId
                                    OR (
                                         Old.StartTimeId IS NULL
                                         AND New.StartTimeId IS NOT NULL
                                       )
                                    OR (
                                         New.StartTimeId IS NULL
                                         AND Old.StartTimeId IS NOT NULL
                                       ); 
                IF UPDATE(EndTimeId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PeriodId
                                   ,'EndTimeId'
                                   ,CONVERT(VARCHAR(8000),Old.EndTimeId,121)
                                   ,CONVERT(VARCHAR(8000),New.EndTimeId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PeriodId = New.PeriodId
                            WHERE   Old.EndTimeId <> New.EndTimeId
                                    OR (
                                         Old.EndTimeId IS NULL
                                         AND New.EndTimeId IS NOT NULL
                                       )
                                    OR (
                                         New.EndTimeId IS NULL
                                         AND Old.EndTimeId IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PeriodId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PeriodId = New.PeriodId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PeriodId
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PeriodId = New.PeriodId
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO
ALTER TABLE [dbo].[syPeriods] ADD CONSTRAINT [PK_syPeriods_PeriodId] PRIMARY KEY CLUSTERED  ([PeriodId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_syPeriods_PeriodId_StartTimeId_EndTimeId_PeriodDescrip] ON [dbo].[syPeriods] ([PeriodId], [StartTimeId], [EndTimeId]) INCLUDE ([PeriodDescrip]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syPeriods] ADD CONSTRAINT [FK_syPeriods_cmTimeInterval_EndTimeId_TimeIntervalId] FOREIGN KEY ([EndTimeId]) REFERENCES [dbo].[cmTimeInterval] ([TimeIntervalId])
GO
ALTER TABLE [dbo].[syPeriods] ADD CONSTRAINT [FK_syPeriods_cmTimeInterval_StartTimeId_TimeIntervalId] FOREIGN KEY ([StartTimeId]) REFERENCES [dbo].[cmTimeInterval] ([TimeIntervalId])
GO
ALTER TABLE [dbo].[syPeriods] ADD CONSTRAINT [FK_syPeriods_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[syPeriods] ADD CONSTRAINT [FK_syPeriods_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
