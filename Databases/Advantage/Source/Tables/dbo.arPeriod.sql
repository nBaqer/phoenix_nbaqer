CREATE TABLE [dbo].[arPeriod]
(
[PrdId] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[PrdCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PrdDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[PrdLen] [int] NULL,
[Mon] [bit] NULL,
[Tue] [bit] NULL,
[Wed] [bit] NULL,
[Thur] [bit] NULL,
[Fri] [bit] NULL,
[Sat] [bit] NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[TimeIntervalId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[arPeriod_Audit_Delete] ON [dbo].[arPeriod]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arPeriod','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrdId
                               ,'PrdLen'
                               ,CONVERT(VARCHAR(8000),Old.PrdLen,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrdId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrdId
                               ,'TimeIntervalId'
                               ,CONVERT(VARCHAR(8000),Old.TimeIntervalId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrdId
                               ,'PrdCode'
                               ,CONVERT(VARCHAR(8000),Old.PrdCode,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrdId
                               ,'PrdDescrip'
                               ,CONVERT(VARCHAR(8000),Old.PrdDescrip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrdId
                               ,'Fri'
                               ,CONVERT(VARCHAR(8000),Old.Fri,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrdId
                               ,'Sat'
                               ,CONVERT(VARCHAR(8000),Old.Sat,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrdId
                               ,'Thur'
                               ,CONVERT(VARCHAR(8000),Old.Thur,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrdId
                               ,'Mon'
                               ,CONVERT(VARCHAR(8000),Old.Mon,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrdId
                               ,'Tue'
                               ,CONVERT(VARCHAR(8000),Old.Tue,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrdId
                               ,'Wed'
                               ,CONVERT(VARCHAR(8000),Old.Wed,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[arPeriod_Audit_Insert] ON [dbo].[arPeriod]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arPeriod','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrdId
                               ,'PrdLen'
                               ,CONVERT(VARCHAR(8000),New.PrdLen,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrdId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrdId
                               ,'TimeIntervalId'
                               ,CONVERT(VARCHAR(8000),New.TimeIntervalId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrdId
                               ,'PrdCode'
                               ,CONVERT(VARCHAR(8000),New.PrdCode,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrdId
                               ,'PrdDescrip'
                               ,CONVERT(VARCHAR(8000),New.PrdDescrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrdId
                               ,'Fri'
                               ,CONVERT(VARCHAR(8000),New.Fri,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrdId
                               ,'Sat'
                               ,CONVERT(VARCHAR(8000),New.Sat,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrdId
                               ,'Thur'
                               ,CONVERT(VARCHAR(8000),New.Thur,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrdId
                               ,'Mon'
                               ,CONVERT(VARCHAR(8000),New.Mon,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrdId
                               ,'Tue'
                               ,CONVERT(VARCHAR(8000),New.Tue,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrdId
                               ,'Wed'
                               ,CONVERT(VARCHAR(8000),New.Wed,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO
GO

GO

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[arPeriod_Audit_Update] ON [dbo].[arPeriod]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arPeriod','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(PrdLen)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrdId
                                   ,'PrdLen'
                                   ,CONVERT(VARCHAR(8000),Old.PrdLen,121)
                                   ,CONVERT(VARCHAR(8000),New.PrdLen,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrdId = New.PrdId
                            WHERE   Old.PrdLen <> New.PrdLen
                                    OR (
                                         Old.PrdLen IS NULL
                                         AND New.PrdLen IS NOT NULL
                                       )
                                    OR (
                                         New.PrdLen IS NULL
                                         AND Old.PrdLen IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrdId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrdId = New.PrdId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(TimeIntervalId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrdId
                                   ,'TimeIntervalId'
                                   ,CONVERT(VARCHAR(8000),Old.TimeIntervalId,121)
                                   ,CONVERT(VARCHAR(8000),New.TimeIntervalId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrdId = New.PrdId
                            WHERE   Old.TimeIntervalId <> New.TimeIntervalId
                                    OR (
                                         Old.TimeIntervalId IS NULL
                                         AND New.TimeIntervalId IS NOT NULL
                                       )
                                    OR (
                                         New.TimeIntervalId IS NULL
                                         AND Old.TimeIntervalId IS NOT NULL
                                       ); 
                IF UPDATE(PrdCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrdId
                                   ,'PrdCode'
                                   ,CONVERT(VARCHAR(8000),Old.PrdCode,121)
                                   ,CONVERT(VARCHAR(8000),New.PrdCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrdId = New.PrdId
                            WHERE   Old.PrdCode <> New.PrdCode
                                    OR (
                                         Old.PrdCode IS NULL
                                         AND New.PrdCode IS NOT NULL
                                       )
                                    OR (
                                         New.PrdCode IS NULL
                                         AND Old.PrdCode IS NOT NULL
                                       ); 
                IF UPDATE(PrdDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrdId
                                   ,'PrdDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.PrdDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.PrdDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrdId = New.PrdId
                            WHERE   Old.PrdDescrip <> New.PrdDescrip
                                    OR (
                                         Old.PrdDescrip IS NULL
                                         AND New.PrdDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.PrdDescrip IS NULL
                                         AND Old.PrdDescrip IS NOT NULL
                                       ); 
                IF UPDATE(Fri)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrdId
                                   ,'Fri'
                                   ,CONVERT(VARCHAR(8000),Old.Fri,121)
                                   ,CONVERT(VARCHAR(8000),New.Fri,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrdId = New.PrdId
                            WHERE   Old.Fri <> New.Fri
                                    OR (
                                         Old.Fri IS NULL
                                         AND New.Fri IS NOT NULL
                                       )
                                    OR (
                                         New.Fri IS NULL
                                         AND Old.Fri IS NOT NULL
                                       ); 
                IF UPDATE(Sat)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrdId
                                   ,'Sat'
                                   ,CONVERT(VARCHAR(8000),Old.Sat,121)
                                   ,CONVERT(VARCHAR(8000),New.Sat,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrdId = New.PrdId
                            WHERE   Old.Sat <> New.Sat
                                    OR (
                                         Old.Sat IS NULL
                                         AND New.Sat IS NOT NULL
                                       )
                                    OR (
                                         New.Sat IS NULL
                                         AND Old.Sat IS NOT NULL
                                       ); 
                IF UPDATE(Thur)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrdId
                                   ,'Thur'
                                   ,CONVERT(VARCHAR(8000),Old.Thur,121)
                                   ,CONVERT(VARCHAR(8000),New.Thur,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrdId = New.PrdId
                            WHERE   Old.Thur <> New.Thur
                                    OR (
                                         Old.Thur IS NULL
                                         AND New.Thur IS NOT NULL
                                       )
                                    OR (
                                         New.Thur IS NULL
                                         AND Old.Thur IS NOT NULL
                                       ); 
                IF UPDATE(Mon)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrdId
                                   ,'Mon'
                                   ,CONVERT(VARCHAR(8000),Old.Mon,121)
                                   ,CONVERT(VARCHAR(8000),New.Mon,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrdId = New.PrdId
                            WHERE   Old.Mon <> New.Mon
                                    OR (
                                         Old.Mon IS NULL
                                         AND New.Mon IS NOT NULL
                                       )
                                    OR (
                                         New.Mon IS NULL
                                         AND Old.Mon IS NOT NULL
                                       ); 
                IF UPDATE(Tue)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrdId
                                   ,'Tue'
                                   ,CONVERT(VARCHAR(8000),Old.Tue,121)
                                   ,CONVERT(VARCHAR(8000),New.Tue,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrdId = New.PrdId
                            WHERE   Old.Tue <> New.Tue
                                    OR (
                                         Old.Tue IS NULL
                                         AND New.Tue IS NOT NULL
                                       )
                                    OR (
                                         New.Tue IS NULL
                                         AND Old.Tue IS NOT NULL
                                       ); 
                IF UPDATE(Wed)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrdId
                                   ,'Wed'
                                   ,CONVERT(VARCHAR(8000),Old.Wed,121)
                                   ,CONVERT(VARCHAR(8000),New.Wed,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrdId = New.PrdId
                            WHERE   Old.Wed <> New.Wed
                                    OR (
                                         Old.Wed IS NULL
                                         AND New.Wed IS NOT NULL
                                       )
                                    OR (
                                         New.Wed IS NULL
                                         AND Old.Wed IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[arPeriod] ADD CONSTRAINT [PK_arPeriod_PrdId] PRIMARY KEY CLUSTERED  ([PrdId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arPeriod] ADD CONSTRAINT [FK_arPeriod_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
ALTER TABLE [dbo].[arPeriod] ADD CONSTRAINT [FK_arPeriod_cmTimeInterval_TimeIntervalId_TimeIntervalId] FOREIGN KEY ([TimeIntervalId]) REFERENCES [dbo].[cmTimeInterval] ([TimeIntervalId])
GO
