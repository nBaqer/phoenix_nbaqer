CREATE TABLE [dbo].[syUserType]
(
[UserTypeId] [int] NOT NULL,
[Code] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NOT NULL CONSTRAINT [DF_syUserType_ModDate] DEFAULT (getdate()),
[ModUser] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syUserType] ADD CONSTRAINT [PK_syUserType_UserTypeId] PRIMARY KEY CLUSTERED  ([UserTypeId]) ON [PRIMARY]
GO
