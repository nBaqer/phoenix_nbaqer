-- removed terminationId
CREATE TABLE [dbo].[plStudentDocs]
(
[StudentDocId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_plStudentDocs_StudentDocId] DEFAULT (newid()),
[StudentId] [uniqueidentifier] NOT NULL,
[DocumentId] [uniqueidentifier] NOT NULL,
[DocStatusId] [uniqueidentifier] NOT NULL,
[RequestDate] [datetime] NULL,
[ReceiveDate] [datetime] NULL,
[img_data] [image] NULL,
[img_contenttype] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScannedId] [int] NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModuleID] [tinyint] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[plStudentDocs_Audit_Delete] ON [dbo].[plStudentDocs]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'plStudentDocs','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentDocId
                               ,'StudentId'
                               ,CONVERT(VARCHAR(8000),Old.StudentId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentDocId
                               ,'DocumentId'
                               ,CONVERT(VARCHAR(8000),Old.DocumentId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentDocId
                               ,'DocStatusId'
                               ,CONVERT(VARCHAR(8000),Old.DocStatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentDocId
                               ,'RequestDate'
                               ,CONVERT(VARCHAR(8000),Old.RequestDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentDocId
                               ,'ReceiveDate'
                               ,CONVERT(VARCHAR(8000),Old.ReceiveDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentDocId
                               ,'ScannedId'
                               ,CONVERT(VARCHAR(8000),Old.ScannedId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentDocId
                               ,'ModuleId'
                               ,CONVERT(VARCHAR(8000),Old.ModuleID,121)
                        FROM    Deleted Old; 

                SET NOCOUNT OFF;


            END; 
        END;



    SET NOCOUNT OFF; 

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[plStudentDocs_Audit_Insert] ON [dbo].[plStudentDocs]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'plStudentDocs','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StudentDocId
                               ,'StudentId'
                               ,CONVERT(VARCHAR(8000),New.StudentId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StudentDocId
                               ,'DocumentId'
                               ,CONVERT(VARCHAR(8000),New.DocumentId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StudentDocId
                               ,'DocStatusId'
                               ,CONVERT(VARCHAR(8000),New.DocStatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StudentDocId
                               ,'RequestDate'
                               ,CONVERT(VARCHAR(8000),New.RequestDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StudentDocId
                               ,'ReceiveDate'
                               ,CONVERT(VARCHAR(8000),New.ReceiveDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StudentDocId
                               ,'ScannedId'
                               ,CONVERT(VARCHAR(8000),New.ScannedId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StudentDocId
                               ,'ModuleId'
                               ,CONVERT(VARCHAR(8000),New.ModuleID,121)
                        FROM    Inserted New; 

                SET NOCOUNT OFF;

            END; 
        END;



    SET NOCOUNT OFF; 

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[plStudentDocs_Audit_Update] ON [dbo].[plStudentDocs]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'plStudentDocs','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(StudentId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StudentDocId
                                   ,'StudentId'
                                   ,CONVERT(VARCHAR(8000),Old.StudentId,121)
                                   ,CONVERT(VARCHAR(8000),New.StudentId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StudentDocId = New.StudentDocId
                            WHERE   Old.StudentId <> New.StudentId
                                    OR (
                                         Old.StudentId IS NULL
                                         AND New.StudentId IS NOT NULL
                                       )
                                    OR (
                                         New.StudentId IS NULL
                                         AND Old.StudentId IS NOT NULL
                                       ); 
                IF UPDATE(DocumentId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StudentDocId
                                   ,'DocumentId'
                                   ,CONVERT(VARCHAR(8000),Old.DocumentId,121)
                                   ,CONVERT(VARCHAR(8000),New.DocumentId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StudentDocId = New.StudentDocId
                            WHERE   Old.DocumentId <> New.DocumentId
                                    OR (
                                         Old.DocumentId IS NULL
                                         AND New.DocumentId IS NOT NULL
                                       )
                                    OR (
                                         New.DocumentId IS NULL
                                         AND Old.DocumentId IS NOT NULL
                                       ); 
                IF UPDATE(DocStatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StudentDocId
                                   ,'DocStatusId'
                                   ,CONVERT(VARCHAR(8000),Old.DocStatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.DocStatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StudentDocId = New.StudentDocId
                            WHERE   Old.DocStatusId <> New.DocStatusId
                                    OR (
                                         Old.DocStatusId IS NULL
                                         AND New.DocStatusId IS NOT NULL
                                       )
                                    OR (
                                         New.DocStatusId IS NULL
                                         AND Old.DocStatusId IS NOT NULL
                                       ); 
                IF UPDATE(RequestDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StudentDocId
                                   ,'RequestDate'
                                   ,CONVERT(VARCHAR(8000),Old.RequestDate,121)
                                   ,CONVERT(VARCHAR(8000),New.RequestDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StudentDocId = New.StudentDocId
                            WHERE   Old.RequestDate <> New.RequestDate
                                    OR (
                                         Old.RequestDate IS NULL
                                         AND New.RequestDate IS NOT NULL
                                       )
                                    OR (
                                         New.RequestDate IS NULL
                                         AND Old.RequestDate IS NOT NULL
                                       ); 
                IF UPDATE(ReceiveDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StudentDocId
                                   ,'ReceiveDate'
                                   ,CONVERT(VARCHAR(8000),Old.ReceiveDate,121)
                                   ,CONVERT(VARCHAR(8000),New.ReceiveDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StudentDocId = New.StudentDocId
                            WHERE   Old.ReceiveDate <> New.ReceiveDate
                                    OR (
                                         Old.ReceiveDate IS NULL
                                         AND New.ReceiveDate IS NOT NULL
                                       )
                                    OR (
                                         New.ReceiveDate IS NULL
                                         AND Old.ReceiveDate IS NOT NULL
                                       ); 
                IF UPDATE(ScannedId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StudentDocId
                                   ,'ScannedId'
                                   ,CONVERT(VARCHAR(8000),Old.ScannedId,121)
                                   ,CONVERT(VARCHAR(8000),New.ScannedId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StudentDocId = New.StudentDocId
                            WHERE   Old.ScannedId <> New.ScannedId
                                    OR (
                                         Old.ScannedId IS NULL
                                         AND New.ScannedId IS NOT NULL
                                       )
                                    OR (
                                         New.ScannedId IS NULL
                                         AND Old.ScannedId IS NOT NULL
                                       ); 
                IF UPDATE(ModuleID)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StudentDocId
                                   ,'ModuleId'
                                   ,CONVERT(VARCHAR(8000),Old.ModuleID,121)
                                   ,CONVERT(VARCHAR(8000),New.ModuleID,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StudentDocId = New.StudentDocId
                            WHERE   Old.ModuleID <> New.ModuleID
                                    OR (
                                         Old.ModuleID IS NULL
                                         AND New.ModuleID IS NOT NULL
                                       )
                                    OR (
                                         New.ModuleID IS NULL
                                         AND Old.ModuleID IS NOT NULL
                                       ); 


            END; 

            SET NOCOUNT OFF;

        END;



    SET NOCOUNT OFF; 

GO
ALTER TABLE [dbo].[plStudentDocs] ADD CONSTRAINT [PK_plStudentDocs_StudentDocId] PRIMARY KEY CLUSTERED  ([StudentDocId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_plStudentDocs_StudentId_DocStatusId_DocumentId] ON [dbo].[plStudentDocs] ([StudentId], [DocStatusId]) INCLUDE ([DocumentId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[plStudentDocs] ADD CONSTRAINT [FK_plStudentDocs_adReqs_DocumentId_adReqId] FOREIGN KEY ([DocumentId]) REFERENCES [dbo].[adReqs] ([adReqId])
GO
ALTER TABLE [dbo].[plStudentDocs] ADD CONSTRAINT [FK_plStudentDocs_syDocStatuses_DocStatusId_DocStatusId] FOREIGN KEY ([DocStatusId]) REFERENCES [dbo].[syDocStatuses] ([DocStatusId])
GO
ALTER TABLE [dbo].[plStudentDocs] ADD CONSTRAINT [FK_plStudentDocs_syModules_ModuleID_ModuleID] FOREIGN KEY ([ModuleID]) REFERENCES [dbo].[syModules] ([ModuleID])
GO
