CREATE TABLE [dbo].[syInstitutionContacts]
(
[InstitutionContactId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_syInstitutionContacts_InstitutionContactId] DEFAULT (newid()),
[InstitutionId] [uniqueidentifier] NOT NULL,
[FirstName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MiddleName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusId] [uniqueidentifier] NULL CONSTRAINT [DF_syInstitutionContacts_StatusId] DEFAULT ([dbo].[UDF_GetSyStatusesValue]('A')),
[PrefixId] [uniqueidentifier] NULL,
[SuffixId] [uniqueidentifier] NULL,
[Title] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Phone] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneExt] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ForeignPhone] [bit] NULL,
[Email] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[IsDefault] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syInstitutionContacts] ADD CONSTRAINT [PK_syInstitutionContacts_InstitutionContactId] PRIMARY KEY CLUSTERED  ([InstitutionContactId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syInstitutionContacts] ADD CONSTRAINT [FK_syInstitutionContacts_syInstitutions_InstitutionId_HSID] FOREIGN KEY ([InstitutionId]) REFERENCES [dbo].[syInstitutions] ([HSId])
GO
ALTER TABLE [dbo].[syInstitutionContacts] ADD CONSTRAINT [FK_syInstitutionContacts_syPrefixes_PrefixId_PrefixId] FOREIGN KEY ([PrefixId]) REFERENCES [dbo].[syPrefixes] ([PrefixId])
GO
ALTER TABLE [dbo].[syInstitutionContacts] ADD CONSTRAINT [FK_syInstitutionContacts_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
ALTER TABLE [dbo].[syInstitutionContacts] ADD CONSTRAINT [FK_syInstitutionContacts_sySuffixes_SuffixId_SuffixId] FOREIGN KEY ([SuffixId]) REFERENCES [dbo].[sySuffixes] ([SuffixId])
GO
