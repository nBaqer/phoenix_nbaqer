CREATE TABLE [dbo].[syNACCASDropReasons]
(
[NACCASDropReasonId] [uniqueidentifier] NOT NULL,
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Code] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syNACCASDropReasons] ADD CONSTRAINT [PK__syNACCAS__57EF0CA511FAE5F4] PRIMARY KEY CLUSTERED  ([NACCASDropReasonId]) ON [PRIMARY]
GO
