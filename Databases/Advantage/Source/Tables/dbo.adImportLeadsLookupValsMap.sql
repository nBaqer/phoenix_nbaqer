CREATE TABLE [dbo].[adImportLeadsLookupValsMap]
(
[LookupValuesMapId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_adImportLeadsLookupValsMap_LookupValuesMapId] DEFAULT (newid()),
[MappingId] [uniqueidentifier] NOT NULL,
[ILFieldId] [int] NOT NULL,
[FileValue] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AdvValue] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adImportLeadsLookupValsMap] ADD CONSTRAINT [PK_adImportLeadsLookupValsMap_LookupValuesMapId] PRIMARY KEY CLUSTERED  ([LookupValuesMapId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adImportLeadsLookupValsMap] ADD CONSTRAINT [FK_adImportLeadsLookupValsMap_adImportLeadsFields_ILFieldId_ILFieldId] FOREIGN KEY ([ILFieldId]) REFERENCES [dbo].[adImportLeadsFields] ([ILFieldId])
GO
ALTER TABLE [dbo].[adImportLeadsLookupValsMap] ADD CONSTRAINT [FK_adImportLeadsLookupValsMap_adImportLeadsMappings_MappingId_MappingId] FOREIGN KEY ([MappingId]) REFERENCES [dbo].[adImportLeadsMappings] ([MappingId]) ON DELETE CASCADE
GO
