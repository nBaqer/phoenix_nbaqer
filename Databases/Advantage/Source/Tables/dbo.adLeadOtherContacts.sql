CREATE TABLE [dbo].[adLeadOtherContacts]
(
[OtherContactId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_adLeadOtherContacts_OtherContactId] DEFAULT (newid()),
[LeadId] [uniqueidentifier] NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[RelationshipId] [uniqueidentifier] NOT NULL,
[ContactTypeId] [uniqueidentifier] NOT NULL,
[PrefixId] [uniqueidentifier] NULL,
[SufixId] [uniqueidentifier] NULL,
[FirstName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MiddleName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Comments] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NOT NULL CONSTRAINT [DF_adLeadOtherContacts_ModDate] DEFAULT (getdate()),
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
--========================================================================================== 
-- TRIGGER adLeadOtherContacts_Audit_Delete 
-- DELETE  add Audit History when delete records in adLeadOtherContacts
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadOtherContacts_Audit_Delete] ON [dbo].[adLeadOtherContacts]
    FOR DELETE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Deleted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Deleted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Deleted
                        );
 
        IF @EventRows > 0
            BEGIN
                EXEC fmAuditHistAdd @AuditHistId,'adLeadOtherContacts','D',@EventRows,@EventDate,@UserName;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),Old.LeadId)
                        FROM    Deleted AS Old;
                -- StatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(MAX),Old.StatusId)
                        FROM    Deleted AS Old;
                -- RelationshipId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactId
                               ,'RelationshipId'
                               ,CONVERT(VARCHAR(MAX),Old.RelationshipId)
                        FROM    Deleted AS Old;
                -- ContactTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactId
                               ,'ContactTypeId'
                               ,CONVERT(VARCHAR(MAX),Old.ContactTypeId)
                        FROM    Deleted AS Old;
                -- PrefixId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactId
                               ,'PrefixId'
                               ,CONVERT(VARCHAR(MAX),Old.PrefixId)
                        FROM    Deleted AS Old;
                -- SufixId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactId
                               ,'SufixId'
                               ,CONVERT(VARCHAR(MAX),Old.SufixId)
                        FROM    Deleted AS Old;
                -- FirstName 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactId
                               ,'FirstName'
                               ,CONVERT(VARCHAR(MAX),Old.FirstName)
                        FROM    Deleted AS Old;
                -- LastName 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactId
                               ,'LastName'
                               ,CONVERT(VARCHAR(MAX),Old.LastName)
                        FROM    Deleted AS Old;
                -- MiddleName 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactId
                               ,'MiddleName'
                               ,CONVERT(VARCHAR(MAX),Old.MiddleName)
                        FROM    Deleted AS Old;
                -- Comments 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactId
                               ,'Comments'
                               ,CONVERT(VARCHAR(MAX),Old.Comments)
                        FROM    Deleted AS Old;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                        FROM    Deleted AS Old;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.OtherContactId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),Old.ModUser)
                        FROM    Deleted AS Old;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadOtherContacts_Audit_Delete 
--========================================================================================== 
 

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
--========================================================================================== 
-- TRIGGER adLeadOtherContacts_Audit_Insert 
-- INSERT  add Audit History when insert records in adLeadOtherContacts
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadOtherContacts_Audit_Insert] ON [dbo].[adLeadOtherContacts]
    FOR INSERT
AS
    BEGIN 
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadOtherContacts','I',@EventRows,@EventDate,@UserName;
                -- LeadId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactId
                               ,'LeadId'
                               ,CONVERT(VARCHAR(MAX),New.LeadId)
                        FROM    Inserted AS New;
                -- StatusId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(MAX),New.StatusId)
                        FROM    Inserted AS New;
                -- RelationshipId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactId
                               ,'RelationshipId'
                               ,CONVERT(VARCHAR(MAX),New.RelationshipId)
                        FROM    Inserted AS New;
                -- ContactTypeId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactId
                               ,'ContactTypeId'
                               ,CONVERT(VARCHAR(MAX),New.ContactTypeId)
                        FROM    Inserted AS New;
                -- PrefixId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactId
                               ,'PrefixId'
                               ,CONVERT(VARCHAR(MAX),New.PrefixId)
                        FROM    Inserted AS New;
                -- SufixId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactId
                               ,'SufixId'
                               ,CONVERT(VARCHAR(MAX),New.SufixId)
                        FROM    Inserted AS New;
                -- FirstName 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactId
                               ,'FirstName'
                               ,CONVERT(VARCHAR(MAX),New.FirstName)
                        FROM    Inserted AS New;
                -- LastName 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactId
                               ,'LastName'
                               ,CONVERT(VARCHAR(MAX),New.LastName)
                        FROM    Inserted AS New;
                -- MiddleName 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactId
                               ,'MiddleName'
                               ,CONVERT(VARCHAR(MAX),New.MiddleName)
                        FROM    Inserted AS New;
                -- Comments 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactId
                               ,'Comments'
                               ,CONVERT(VARCHAR(MAX),New.Comments)
                        FROM    Inserted AS New;
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                        FROM    Inserted AS New;
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.OtherContactId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),New.ModUser)
                        FROM    Inserted AS New;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadOtherContacts_Audit_Insert 
--========================================================================================== 
 

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--========================================================================================== 
-- TRIGGER adLeadOtherContacts_Audit_Update 
-- UPDATE  add Audit History when update fields in adLeadOtherContacts
--========================================================================================== 
CREATE TRIGGER [dbo].[adLeadOtherContacts_Audit_Update] ON [dbo].[adLeadOtherContacts]
    FOR UPDATE
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
        DECLARE @EventRows AS INT;
        DECLARE @EventDate AS DATETIME;
        DECLARE @UserName AS VARCHAR(50);
	
        SET @AuditHistId = NEWID();
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adLeadOtherContacts','U',@EventRows,@EventDate,@UserName;
                -- LeadId 
                IF UPDATE(LeadId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactId
                                       ,'LeadId'
                                       ,CONVERT(VARCHAR(MAX),Old.LeadId)
                                       ,CONVERT(VARCHAR(MAX),New.LeadId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactId = New.OtherContactId
                                WHERE   Old.LeadId <> New.LeadId
                                        OR (
                                             Old.LeadId IS NULL
                                             AND New.LeadId IS NOT NULL
                                           )
                                        OR (
                                             New.LeadId IS NULL
                                             AND Old.LeadId IS NOT NULL
                                           );
                    END;
                -- StatusId 
                IF UPDATE(StatusId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactId
                                       ,'StatusId'
                                       ,CONVERT(VARCHAR(MAX),Old.StatusId)
                                       ,CONVERT(VARCHAR(MAX),New.StatusId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactId = New.OtherContactId
                                WHERE   Old.StatusId <> New.StatusId
                                        OR (
                                             Old.StatusId IS NULL
                                             AND New.StatusId IS NOT NULL
                                           )
                                        OR (
                                             New.StatusId IS NULL
                                             AND Old.StatusId IS NOT NULL
                                           );
                    END;
                -- RelationshipId 
                IF UPDATE(RelationshipId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactId
                                       ,'RelationshipId'
                                       ,CONVERT(VARCHAR(MAX),Old.RelationshipId)
                                       ,CONVERT(VARCHAR(MAX),New.RelationshipId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactId = New.OtherContactId
                                WHERE   Old.RelationshipId <> New.RelationshipId
                                        OR (
                                             Old.RelationshipId IS NULL
                                             AND New.RelationshipId IS NOT NULL
                                           )
                                        OR (
                                             New.RelationshipId IS NULL
                                             AND Old.RelationshipId IS NOT NULL
                                           );
                    END;
                -- ContactTypeId 
                IF UPDATE(ContactTypeId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactId
                                       ,'ContactTypeId'
                                       ,CONVERT(VARCHAR(MAX),Old.ContactTypeId)
                                       ,CONVERT(VARCHAR(MAX),New.ContactTypeId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactId = New.OtherContactId
                                WHERE   Old.ContactTypeId <> New.ContactTypeId
                                        OR (
                                             Old.ContactTypeId IS NULL
                                             AND New.ContactTypeId IS NOT NULL
                                           )
                                        OR (
                                             New.ContactTypeId IS NULL
                                             AND Old.ContactTypeId IS NOT NULL
                                           );
                    END;
                -- PrefixId 
                IF UPDATE(PrefixId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactId
                                       ,'PrefixId'
                                       ,CONVERT(VARCHAR(MAX),Old.PrefixId)
                                       ,CONVERT(VARCHAR(MAX),New.PrefixId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactId = New.OtherContactId
                                WHERE   Old.PrefixId <> New.PrefixId
                                        OR (
                                             Old.PrefixId IS NULL
                                             AND New.PrefixId IS NOT NULL
                                           )
                                        OR (
                                             New.PrefixId IS NULL
                                             AND Old.PrefixId IS NOT NULL
                                           );
                    END;
                -- SufixId 
                IF UPDATE(SufixId)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactId
                                       ,'SufixId'
                                       ,CONVERT(VARCHAR(MAX),Old.SufixId)
                                       ,CONVERT(VARCHAR(MAX),New.SufixId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactId = New.OtherContactId
                                WHERE   Old.SufixId <> New.SufixId
                                        OR (
                                             Old.SufixId IS NULL
                                             AND New.SufixId IS NOT NULL
                                           )
                                        OR (
                                             New.SufixId IS NULL
                                             AND Old.SufixId IS NOT NULL
                                           );
                    END;
                -- FirstName 
                IF UPDATE(FirstName)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactId
                                       ,'FirstName'
                                       ,CONVERT(VARCHAR(MAX),Old.FirstName)
                                       ,CONVERT(VARCHAR(MAX),New.FirstName)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactId = New.OtherContactId
                                WHERE   Old.FirstName <> New.FirstName
                                        OR (
                                             Old.FirstName IS NULL
                                             AND New.FirstName IS NOT NULL
                                           )
                                        OR (
                                             New.FirstName IS NULL
                                             AND Old.FirstName IS NOT NULL
                                           );
                    END;
                -- LastName 
                IF UPDATE(LastName)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactId
                                       ,'LastName'
                                       ,CONVERT(VARCHAR(MAX),Old.LastName)
                                       ,CONVERT(VARCHAR(MAX),New.LastName)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactId = New.OtherContactId
                                WHERE   Old.LastName <> New.LastName
                                        OR (
                                             Old.LastName IS NULL
                                             AND New.LastName IS NOT NULL
                                           )
                                        OR (
                                             New.LastName IS NULL
                                             AND Old.LastName IS NOT NULL
                                           );
                    END;
                -- MiddleName 
                IF UPDATE(MiddleName)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactId
                                       ,'MiddleName'
                                       ,CONVERT(VARCHAR(MAX),Old.MiddleName)
                                       ,CONVERT(VARCHAR(MAX),New.MiddleName)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactId = New.OtherContactId
                                WHERE   Old.MiddleName <> New.MiddleName
                                        OR (
                                             Old.MiddleName IS NULL
                                             AND New.MiddleName IS NOT NULL
                                           )
                                        OR (
                                             New.MiddleName IS NULL
                                             AND Old.MiddleName IS NOT NULL
                                           );
                    END;
                -- Comments 
                IF UPDATE(Comments)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactId
                                       ,'Comments'
                                       ,CONVERT(VARCHAR(MAX),Old.Comments)
                                       ,CONVERT(VARCHAR(MAX),New.Comments)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactId = New.OtherContactId
                                WHERE   Old.Comments <> New.Comments
                                        OR (
                                             Old.Comments IS NULL
                                             AND New.Comments IS NOT NULL
                                           )
                                        OR (
                                             New.Comments IS NULL
                                             AND Old.Comments IS NOT NULL
                                           );
                    END;
                -- ModDate 
                IF UPDATE(ModDate)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactId
                                       ,'ModDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactId = New.OtherContactId
                                WHERE   Old.ModDate <> New.ModDate
                                        OR (
                                             Old.ModDate IS NULL
                                             AND New.ModDate IS NOT NULL
                                           )
                                        OR (
                                             New.ModDate IS NULL
                                             AND Old.ModDate IS NOT NULL
                                           );
                    END;
                -- ModUser 
                IF UPDATE(ModUser)
                    BEGIN
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.OtherContactId
                                       ,'ModUser'
                                       ,CONVERT(VARCHAR(MAX),Old.ModUser)
                                       ,CONVERT(VARCHAR(MAX),New.ModUser)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.OtherContactId = New.OtherContactId
                                WHERE   Old.ModUser <> New.ModUser
                                        OR (
                                             Old.ModUser IS NULL
                                             AND New.ModUser IS NOT NULL
                                           )
                                        OR (
                                             New.ModUser IS NULL
                                             AND Old.ModUser IS NOT NULL
                                           );
                    END;
            END;
        SET NOCOUNT OFF;
    END;
--========================================================================================== 
-- END  --  TRIGGER adLeadOtherContacts_Audit_Update 
--========================================================================================== 
 

GO
ALTER TABLE [dbo].[adLeadOtherContacts] ADD CONSTRAINT [PK_adLeadOtherContacts_OtherContactId] PRIMARY KEY CLUSTERED  ([OtherContactId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adLeadOtherContacts] ADD CONSTRAINT [UIX_adLeadOtherContacts_OtherContactId] UNIQUE NONCLUSTERED  ([OtherContactId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adLeadOtherContacts] ADD CONSTRAINT [FK_adLeadOtherContacts_adLeads_LeadId_LeadId] FOREIGN KEY ([LeadId]) REFERENCES [dbo].[adLeads] ([LeadId])
GO
ALTER TABLE [dbo].[adLeadOtherContacts] ADD CONSTRAINT [FK_adLeadOtherContacts_syContactTypes_ContactTypeId_ContactTypeId] FOREIGN KEY ([ContactTypeId]) REFERENCES [dbo].[syContactTypes] ([ContactTypeId])
GO
ALTER TABLE [dbo].[adLeadOtherContacts] ADD CONSTRAINT [FK_adLeadOtherContacts_syPrefixes_PrefixId_PrefixId] FOREIGN KEY ([PrefixId]) REFERENCES [dbo].[syPrefixes] ([PrefixId])
GO
ALTER TABLE [dbo].[adLeadOtherContacts] ADD CONSTRAINT [FK_adLeadOtherContacts_syRelations_RelationshipId_RelationId] FOREIGN KEY ([RelationshipId]) REFERENCES [dbo].[syRelations] ([RelationId])
GO
ALTER TABLE [dbo].[adLeadOtherContacts] ADD CONSTRAINT [FK_adLeadOtherContacts_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
ALTER TABLE [dbo].[adLeadOtherContacts] ADD CONSTRAINT [FK_adLeadOtherContacts_sySuffixes_SufixId_SuffixId] FOREIGN KEY ([SufixId]) REFERENCES [dbo].[sySuffixes] ([SuffixId])
GO
