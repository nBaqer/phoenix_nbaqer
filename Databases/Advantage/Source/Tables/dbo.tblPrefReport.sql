CREATE TABLE [dbo].[tblPrefReport]
(
[PrefReportId] [uniqueidentifier] NOT NULL,
[PrefId] [uniqueidentifier] NULL,
[CampGrpId] [uniqueidentifier] NULL,
[termId] [uniqueidentifier] NULL,
[PrgVerId] [uniqueidentifier] NULL,
[FirstName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[termstartdate] [datetime] NULL,
[termenddate] [datetime] NULL,
[attendancestartdate] [datetime] NULL,
[attendanceenddate] [datetime] NULL,
[moduser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[moddate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblPrefReport] ADD CONSTRAINT [PK_tblPrefReport_PrefReportId] PRIMARY KEY CLUSTERED  ([PrefReportId]) ON [PRIMARY]
GO
