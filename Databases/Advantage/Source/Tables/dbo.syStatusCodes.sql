CREATE TABLE [dbo].[syStatusCodes]
(
[StatusCodeId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syStatusCodes_StatusCodeId] DEFAULT (newid()),
[StatusCode] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusCodeDescrip] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[SysStatusId] [int] NOT NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AcadProbation] [bit] NULL,
[DiscProbation] [bit] NULL,
[IsDefaultLeadStatus] [bit] NULL CONSTRAINT [DF_syStatusCodes_IsDefaultLeadStatus] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[syStatusCodes_Audit_Delete] ON [dbo].[syStatusCodes]
    FOR DELETE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syStatusCodes','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StatusCodeId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StatusCodeId
                               ,'SysStatusId'
                               ,CONVERT(VARCHAR(8000),Old.SysStatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StatusCodeId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StatusCodeId
                               ,'StatusCode'
                               ,CONVERT(VARCHAR(8000),Old.StatusCode,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.StatusCodeId
                               ,'StatusCodeDescrip'
                               ,CONVERT(VARCHAR(8000),Old.StatusCodeDescrip,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[syStatusCodes_Audit_Insert] ON [dbo].[syStatusCodes]
    FOR INSERT
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syStatusCodes','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StatusCodeId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StatusCodeId
                               ,'SysStatusId'
                               ,CONVERT(VARCHAR(8000),New.SysStatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StatusCodeId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StatusCodeId
                               ,'StatusCode'
                               ,CONVERT(VARCHAR(8000),New.StatusCode,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.StatusCodeId
                               ,'StatusCodeDescrip'
                               ,CONVERT(VARCHAR(8000),New.StatusCodeDescrip,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[syStatusCodes_Audit_Update] ON [dbo].[syStatusCodes]
    FOR UPDATE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'syStatusCodes','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StatusCodeId
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StatusCodeId = New.StatusCodeId
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 
                IF UPDATE(SysStatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StatusCodeId
                                   ,'SysStatusId'
                                   ,CONVERT(VARCHAR(8000),Old.SysStatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.SysStatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StatusCodeId = New.StatusCodeId
                            WHERE   Old.SysStatusId <> New.SysStatusId
                                    OR (
                                         Old.SysStatusId IS NULL
                                         AND New.SysStatusId IS NOT NULL
                                       )
                                    OR (
                                         New.SysStatusId IS NULL
                                         AND Old.SysStatusId IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StatusCodeId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StatusCodeId = New.StatusCodeId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(StatusCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StatusCodeId
                                   ,'StatusCode'
                                   ,CONVERT(VARCHAR(8000),Old.StatusCode,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StatusCodeId = New.StatusCodeId
                            WHERE   Old.StatusCode <> New.StatusCode
                                    OR (
                                         Old.StatusCode IS NULL
                                         AND New.StatusCode IS NOT NULL
                                       )
                                    OR (
                                         New.StatusCode IS NULL
                                         AND Old.StatusCode IS NOT NULL
                                       ); 
                IF UPDATE(StatusCodeDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.StatusCodeId
                                   ,'StatusCodeDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.StatusCodeDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusCodeDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.StatusCodeId = New.StatusCodeId
                            WHERE   Old.StatusCodeDescrip <> New.StatusCodeDescrip
                                    OR (
                                         Old.StatusCodeDescrip IS NULL
                                         AND New.StatusCodeDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.StatusCodeDescrip IS NULL
                                         AND Old.StatusCodeDescrip IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--==========================================================================================
-- TRIGGER syStatusCodes_KlassApps_Delete
-- AFTER DELETE syStatusCodes, marks syKlassAppConfigurationSetting record as deleted for Klass Apps
--==========================================================================================
CREATE TRIGGER [dbo].[syStatusCodes_KlassApps_Delete] ON [dbo].[syStatusCodes]
    AFTER DELETE
	-- Klass Apps
	-- studentStatus  is kte   --> Status is lable
AS
    BEGIN
        SET NOCOUNT ON;

        UPDATE  SKACS
        SET     SKACS.ItemStatus = 'D'
               ,SKACS.ModDate = GETDATE() -- D.ModDate
               ,SKACS.ModUser = 'Support' -- D.ModUser
        FROM    syKlassAppConfigurationSetting AS SKACS
        INNER JOIN syKlassOperationType AS SKOT ON SKOT.KlassOperationTypeId = SKACS.KlassOperationTypeId
        INNER JOIN Deleted AS D ON D.StatusCodeId = SKACS.AdvantageId
        WHERE   SKOT.Code = 'studentStatus';

        SET NOCOUNT OFF; 
    END;
--==========================================================================================
-- END  --  TRIGGER syStatusCodes_KlassApps_Delete
--==========================================================================================
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--==========================================================================================
-- TRIGGER syStatusCodes_KlassApps_Insert
-- AFTER INSERT syStatusCodes, insert syKlassAppConfigurationSetting record for Klass Apps
--==========================================================================================
CREATE TRIGGER [dbo].[syStatusCodes_KlassApps_Insert] ON [dbo].[syStatusCodes]
    AFTER INSERT
	-- Klass Apps
	-- studentStatus  is kte   --> Status is lable
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @KlassOperationTypeId AS INTEGER;
        SELECT  @KlassOperationTypeId = SKOT.KlassOperationTypeId
        FROM    syKlassOperationType AS SKOT
        WHERE   SKOT.Code = 'studentStatus';

        INSERT  INTO syKlassAppConfigurationSetting
                (
                 AdvantageId
                ,KlassAppId
                ,KlassEntityId
                ,KlassOperationTypeId
                ,IsCustomField
                ,IsActive
                ,ItemStatus
                ,ItemValue
                ,ItemLabel
                ,ItemValueType
                ,CreationDate
                ,ModDate
                ,ModUser
                ,LastOperationLog
		        )
                SELECT  I.StatusCodeId -- AdvantageId - varchar(38)
                       ,0    -- KlassAppId - int
                       ,1    -- KlassEntityId - int
                       ,@KlassOperationTypeId -- KlassOperationTypeId - int
                       ,0    -- IsCustomField - bit
                       ,1    -- IsActive - bit
                       ,'I'  -- ItemStatus - char(1)
                       ,I.StatusCodeDescrip  -- ItemValue - varchar(200)
                       ,'Status'   -- ItemLabel - varchar(50)
                       ,'String'   -- ItemValueType - varchar(50)
                       ,I.ModDate  -- CreationDate - datetime
                       ,I.ModDate  -- ModDate - datetime
                       ,I.ModUser  -- ModUser - varchar(50)
                       ,''  -- LastOperationLog - varchar(1000)
                FROM    Inserted AS I;

        SET NOCOUNT OFF; 
    END;
--==========================================================================================
-- END  --  TRIGGER syCampuses_KlassApps_Insert
--==========================================================================================
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--==========================================================================================
-- TRIGGER syStatusCodes_KlassApps_Update
-- AFTER UPDATE  syStatusCodes, syKlassAppConfigurationSetting when change fields for Klass Apps
--==========================================================================================
CREATE TRIGGER [dbo].[syStatusCodes_KlassApps_Update] ON [dbo].[syStatusCodes]
    AFTER UPDATE
	--   DB             --   MAP        --  Klass Apps           studentStatus
	-- StatusCodeId     --              --                       AdvantageId
	-- StatusCodeDescrip-- Description	--> name
	--                  --              --> value
	--                  --              --> type
	--                  --              --> deleted
	--					--				--> created_at	
	-- ModDate			-- ModifiedDate	--> updated_at
	--                  --              --> cauto_disable
	-- Klass Apps
	-- studentStatus  is kte   --> Status is lable
AS
    BEGIN
        SET NOCOUNT ON;

        UPDATE  SKACS
        SET     SKACS.ItemStatus = 'U'
               ,SKACS.ModDate = I.ModDate
               ,SKACS.ModUser = I.ModUser
        FROM    syKlassAppConfigurationSetting AS SKACS
        INNER JOIN syKlassOperationType AS SKOT ON SKOT.KlassOperationTypeId = SKACS.KlassOperationTypeId
        INNER JOIN Inserted AS I ON I.StatusCodeId = SKACS.AdvantageId
        WHERE   SKOT.Code = 'studentStatus'
                AND SKACS.ItemStatus <> 'I'
                AND ( UPDATE(StatusCodeDescrip) );

        SET NOCOUNT OFF; 
    END;
--==========================================================================================
-- END  --  TRIGGER syStatusCodes_KlassApps_Update
--==========================================================================================
GO
ALTER TABLE [dbo].[syStatusCodes] ADD CONSTRAINT [PK_syStatusCodes_StatusCodeId] PRIMARY KEY CLUSTERED  ([StatusCodeId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UIX_syStatusCodes_StatusCode_StatusCodeDescrip_CampGrpId] ON [dbo].[syStatusCodes] ([StatusCode], [StatusCodeDescrip], [CampGrpId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syStatusCodes] ADD CONSTRAINT [FK_syStatusCodes_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[syStatusCodes] ADD CONSTRAINT [FK_syStatusCodes_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
ALTER TABLE [dbo].[syStatusCodes] ADD CONSTRAINT [FK_syStatusCodes_SySysStatus_SysStatusId_SysStatusId] FOREIGN KEY ([SysStatusId]) REFERENCES [dbo].[sySysStatus] ([SysStatusId])
GO
