CREATE TABLE [dbo].[sySubMods]
(
[SubModID] [smallint] NOT NULL,
[SubModName] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ResourceTypeID] [tinyint] NOT NULL,
[ModuleId] [tinyint] NOT NULL,
[ResourceId] [smallint] NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[sySubMods] ADD CONSTRAINT [PK_sySubMods_SubModID] PRIMARY KEY CLUSTERED  ([SubModID]) ON [PRIMARY]
GO
GRANT SELECT ON  [dbo].[sySubMods] TO [AdvantageRole]
GO
