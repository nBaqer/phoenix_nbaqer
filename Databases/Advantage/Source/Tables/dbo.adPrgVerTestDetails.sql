CREATE TABLE [dbo].[adPrgVerTestDetails]
(
[PrgVerTestDetailId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_adPrgVerTestDetails_PrgVerTestDetailId] DEFAULT (newid()),
[MinScore] [decimal] (19, 3) NULL,
[ViewOrder] [int] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[PrgVerId] [uniqueidentifier] NULL,
[ReqGrpId] [uniqueidentifier] NULL,
[adReqId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
 
--========================================================================================== 
-- TRIGGER adPrgVerTestDetails_Audit_Delete 
-- INSERT  add Audit History when delete records in adPrgVerTestDetails 
--========================================================================================== 
CREATE TRIGGER [dbo].[adPrgVerTestDetails_Audit_Delete] ON [dbo].[adPrgVerTestDetails]
    FOR DELETE
AS
    BEGIN 
        SET NOCOUNT ON; 
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
        DECLARE @EventRows AS INT; 
        DECLARE @EventDate AS DATETIME; 
        DECLARE @UserName AS VARCHAR(50); 
	
        SET @AuditHistId = NEWID(); 
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Deleted
                         ); 
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Deleted
                         ); 
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Deleted
                        ); 
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adPrgVerTestDetails','D',@EventRows,@EventDate,@UserName; 
                -- MinScore 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerTestDetailId
                               ,'MinScore'
                               ,CONVERT(VARCHAR(MAX),Old.MinScore)
                        FROM    Deleted AS Old; 
                -- ViewOrder 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerTestDetailId
                               ,'ViewOrder'
                               ,CONVERT(VARCHAR(MAX),Old.ViewOrder)
                        FROM    Deleted AS Old; 
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerTestDetailId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),Old.ModUser)
                        FROM    Deleted AS Old; 
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerTestDetailId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                        FROM    Deleted AS Old; 
                -- PrgVerId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerTestDetailId
                               ,'PrgVerId'
                               ,CONVERT(VARCHAR(MAX),Old.PrgVerId)
                        FROM    Deleted AS Old; 
                -- ReqGrpId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerTestDetailId
                               ,'ReqGrpId'
                               ,CONVERT(VARCHAR(MAX),Old.ReqGrpId)
                        FROM    Deleted AS Old; 
                -- adReqId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PrgVerTestDetailId
                               ,'adReqId'
                               ,CONVERT(VARCHAR(MAX),Old.adReqId)
                        FROM    Deleted AS Old; 
            END; 
        SET NOCOUNT OFF; 
    END; 
--========================================================================================== 
-- END TRIGGER adPrgVerTestDetails_Audit_Delete 
--========================================================================================== 
 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
 
--========================================================================================== 
-- TRIGGER adPrgVerTestDetails_Audit_Insert 
-- INSERT  add Audit History when insert records in adPrgVerTestDetails 
--========================================================================================== 
CREATE TRIGGER [dbo].[adPrgVerTestDetails_Audit_Insert] ON [dbo].[adPrgVerTestDetails]
    FOR INSERT
AS
    BEGIN 
        SET NOCOUNT ON; 
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
        DECLARE @EventRows AS INT; 
        DECLARE @EventDate AS DATETIME; 
        DECLARE @UserName AS VARCHAR(50); 
	
        SET @AuditHistId = NEWID(); 
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         ); 
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         ); 
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        ); 
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adPrgVerTestDetails','I',@EventRows,@EventDate,@UserName; 
                -- MinScore 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerTestDetailId
                               ,'MinScore'
                               ,CONVERT(VARCHAR(MAX),New.MinScore)
                        FROM    Inserted AS New; 
                -- ViewOrder 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerTestDetailId
                               ,'ViewOrder'
                               ,CONVERT(VARCHAR(MAX),New.ViewOrder)
                        FROM    Inserted AS New; 
                -- ModUser 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerTestDetailId
                               ,'ModUser'
                               ,CONVERT(VARCHAR(MAX),New.ModUser)
                        FROM    Inserted AS New; 
                -- ModDate 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerTestDetailId
                               ,'ModDate'
                               ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                        FROM    Inserted AS New; 
                -- PrgVerId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerTestDetailId
                               ,'PrgVerId'
                               ,CONVERT(VARCHAR(MAX),New.PrgVerId)
                        FROM    Inserted AS New; 
                -- ReqGrpId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerTestDetailId
                               ,'ReqGrpId'
                               ,CONVERT(VARCHAR(MAX),New.ReqGrpId)
                        FROM    Inserted AS New; 
                -- adReqId 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PrgVerTestDetailId
                               ,'adReqId'
                               ,CONVERT(VARCHAR(MAX),New.adReqId)
                        FROM    Inserted AS New; 
            END; 
        SET NOCOUNT OFF; 
    END; 
--========================================================================================== 
-- END TRIGGER adPrgVerTestDetails_Audit_Insert 
--========================================================================================== 
 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
--========================================================================================== 
-- TRIGGER adPrgVerTestDetails_Audit_Update 
-- UPDATE  add Audit History when update fields in adPrgVerTestDetails 
--========================================================================================== 
CREATE TRIGGER [dbo].[adPrgVerTestDetails_Audit_Update] ON [dbo].[adPrgVerTestDetails]
    FOR UPDATE
AS
    BEGIN 
        SET NOCOUNT ON; 
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
        DECLARE @EventRows AS INT; 
        DECLARE @EventDate AS DATETIME; 
        DECLARE @UserName AS VARCHAR(50); 
	
        SET @AuditHistId = NEWID(); 
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         ); 
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         ); 
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        ); 
 
        IF @EventRows > 0
            BEGIN 
                EXEC fmAuditHistAdd @AuditHistId,'adPrgVerTestDetails','U',@EventRows,@EventDate,@UserName; 
                -- MinScore 
                IF UPDATE(MinScore)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.PrgVerTestDetailId
                                       ,'MinScore'
                                       ,CONVERT(VARCHAR(MAX),Old.MinScore)
                                       ,CONVERT(VARCHAR(MAX),New.MinScore)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.PrgVerTestDetailId = New.PrgVerTestDetailId
                                WHERE   Old.MinScore <> New.MinScore
                                        OR (
                                             Old.MinScore IS NULL
                                             AND New.MinScore IS NOT NULL
                                           )
                                        OR (
                                             New.MinScore IS NULL
                                             AND Old.MinScore IS NOT NULL
                                           ); 
                    END; 
                -- ViewOrder 
                IF UPDATE(ViewOrder)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.PrgVerTestDetailId
                                       ,'ViewOrder'
                                       ,CONVERT(VARCHAR(MAX),Old.ViewOrder)
                                       ,CONVERT(VARCHAR(MAX),New.ViewOrder)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.PrgVerTestDetailId = New.PrgVerTestDetailId
                                WHERE   Old.ViewOrder <> New.ViewOrder
                                        OR (
                                             Old.ViewOrder IS NULL
                                             AND New.ViewOrder IS NOT NULL
                                           )
                                        OR (
                                             New.ViewOrder IS NULL
                                             AND Old.ViewOrder IS NOT NULL
                                           ); 
                    END; 
                -- ModUser 
                IF UPDATE(ModUser)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.PrgVerTestDetailId
                                       ,'ModUser'
                                       ,CONVERT(VARCHAR(MAX),Old.ModUser)
                                       ,CONVERT(VARCHAR(MAX),New.ModUser)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.PrgVerTestDetailId = New.PrgVerTestDetailId
                                WHERE   Old.ModUser <> New.ModUser
                                        OR (
                                             Old.ModUser IS NULL
                                             AND New.ModUser IS NOT NULL
                                           )
                                        OR (
                                             New.ModUser IS NULL
                                             AND Old.ModUser IS NOT NULL
                                           ); 
                    END; 
                -- ModDate 
                IF UPDATE(ModDate)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.PrgVerTestDetailId
                                       ,'ModDate'
                                       ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.PrgVerTestDetailId = New.PrgVerTestDetailId
                                WHERE   Old.ModDate <> New.ModDate
                                        OR (
                                             Old.ModDate IS NULL
                                             AND New.ModDate IS NOT NULL
                                           )
                                        OR (
                                             New.ModDate IS NULL
                                             AND Old.ModDate IS NOT NULL
                                           ); 
                    END; 
                -- PrgVerId 
                IF UPDATE(PrgVerId)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.PrgVerTestDetailId
                                       ,'PrgVerId'
                                       ,CONVERT(VARCHAR(MAX),Old.PrgVerId)
                                       ,CONVERT(VARCHAR(MAX),New.PrgVerId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.PrgVerTestDetailId = New.PrgVerTestDetailId
                                WHERE   Old.PrgVerId <> New.PrgVerId
                                        OR (
                                             Old.PrgVerId IS NULL
                                             AND New.PrgVerId IS NOT NULL
                                           )
                                        OR (
                                             New.PrgVerId IS NULL
                                             AND Old.PrgVerId IS NOT NULL
                                           ); 
                    END; 
                -- ReqGrpId 
                IF UPDATE(ReqGrpId)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.PrgVerTestDetailId
                                       ,'ReqGrpId'
                                       ,CONVERT(VARCHAR(MAX),Old.ReqGrpId)
                                       ,CONVERT(VARCHAR(MAX),New.ReqGrpId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.PrgVerTestDetailId = New.PrgVerTestDetailId
                                WHERE   Old.ReqGrpId <> New.ReqGrpId
                                        OR (
                                             Old.ReqGrpId IS NULL
                                             AND New.ReqGrpId IS NOT NULL
                                           )
                                        OR (
                                             New.ReqGrpId IS NULL
                                             AND Old.ReqGrpId IS NOT NULL
                                           ); 
                    END; 
                -- adReqId 
                IF UPDATE(adReqId)
                    BEGIN 
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue
                                )
                                SELECT  @AuditHistId
                                       ,New.PrgVerTestDetailId
                                       ,'adReqId'
                                       ,CONVERT(VARCHAR(MAX),Old.adReqId)
                                       ,CONVERT(VARCHAR(MAX),New.adReqId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.PrgVerTestDetailId = New.PrgVerTestDetailId
                                WHERE   Old.adReqId <> New.adReqId
                                        OR (
                                             Old.adReqId IS NULL
                                             AND New.adReqId IS NOT NULL
                                           )
                                        OR (
                                             New.adReqId IS NULL
                                             AND Old.adReqId IS NOT NULL
                                           ); 
                    END; 
            END; 
        SET NOCOUNT OFF; 
    END; 
--========================================================================================== 
-- END TRIGGER adPrgVerTestDetails_Audit_Update 
--========================================================================================== 
 
GO
ALTER TABLE [dbo].[adPrgVerTestDetails] ADD CONSTRAINT [PK_adPrgVerTestDetails_PrgVerTestDetailId] PRIMARY KEY CLUSTERED  ([PrgVerTestDetailId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adPrgVerTestDetails] ADD CONSTRAINT [FK_adPrgVerTestDetails_adReqGroups_ReqGrpId_ReqGrpId] FOREIGN KEY ([ReqGrpId]) REFERENCES [dbo].[adReqGroups] ([ReqGrpId])
GO
ALTER TABLE [dbo].[adPrgVerTestDetails] ADD CONSTRAINT [FK_adPrgVerTestDetails_adReqs_adReqId_adReqId] FOREIGN KEY ([adReqId]) REFERENCES [dbo].[adReqs] ([adReqId])
GO
ALTER TABLE [dbo].[adPrgVerTestDetails] ADD CONSTRAINT [FK_adPrgVerTestDetails_arPrgVersions_PrgVerId_PrgVerId] FOREIGN KEY ([PrgVerId]) REFERENCES [dbo].[arPrgVersions] ([PrgVerId])
GO
