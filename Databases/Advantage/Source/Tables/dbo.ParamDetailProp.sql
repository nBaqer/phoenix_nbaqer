CREATE TABLE [dbo].[ParamDetailProp]
(
[detailpropertyid] [bigint] NOT NULL IDENTITY(1, 1),
[detailid] [bigint] NOT NULL,
[childcontrol] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[propname] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[value] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[valuetype] [nvarchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[ParamDetailProp] ADD CONSTRAINT [PK_ParamDetailProp_detailpropertyid] PRIMARY KEY CLUSTERED  ([detailpropertyid]) ON [PRIMARY]
GO
