CREATE TABLE [dbo].[plExitInterview]
(
[ExtInterviewId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_plExitInterview_ExtInterviewId] DEFAULT (newid()),
[EnrollmentId] [uniqueidentifier] NULL,
[ScStatusId] [uniqueidentifier] NULL,
[WantAssistance] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WaiverSigned] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Eligible] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Reason] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AreaId] [uniqueidentifier] NULL,
[AvailableDate] [datetime] NULL,
[AvailableDays] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AvailableHours] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LowSalary] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HighSalary] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TransportationId] [uniqueidentifier] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[ExpertiseLevelId] [uniqueidentifier] NULL,
[FullTimeId] [uniqueidentifier] NULL,
[InelReasonId] [uniqueidentifier] NULL,
[ExitInterviewDate] [datetime2] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[plExitInterview_Audit_Delete] ON [dbo].[plExitInterview]
    FOR DELETE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'plExitInterview','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ExtInterviewId
                               ,'EnrollmentId'
                               ,CONVERT(VARCHAR(8000),Old.EnrollmentId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ExtInterviewId
                               ,'WantAssistance'
                               ,CONVERT(VARCHAR(8000),Old.WantAssistance,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ExtInterviewId
                               ,'WaiverSigned'
                               ,CONVERT(VARCHAR(8000),Old.WaiverSigned,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ExtInterviewId
                               ,'Reason'
                               ,CONVERT(VARCHAR(8000),Old.Reason,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ExtInterviewId
                               ,'AreaId'
                               ,CONVERT(VARCHAR(8000),Old.AreaId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ExtInterviewId
                               ,'HighSalary'
                               ,CONVERT(VARCHAR(8000),Old.HighSalary,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ExtInterviewId
                               ,'TransportationId'
                               ,CONVERT(VARCHAR(8000),Old.TransportationId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ExtInterviewId
                               ,'ScStatusId'
                               ,CONVERT(VARCHAR(8000),Old.ScStatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ExtInterviewId
                               ,'Eligible'
                               ,CONVERT(VARCHAR(8000),Old.Eligible,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ExtInterviewId
                               ,'AvailableDate'
                               ,CONVERT(VARCHAR(8000),Old.AvailableDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ExtInterviewId
                               ,'AvailableDays'
                               ,CONVERT(VARCHAR(8000),Old.AvailableDays,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ExtInterviewId
                               ,'AvailableHours'
                               ,CONVERT(VARCHAR(8000),Old.AvailableHours,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ExtInterviewId
                               ,'LowSalary'
                               ,CONVERT(VARCHAR(8000),Old.LowSalary,121)
                        FROM    Deleted Old; 
            END; 
        END; 




    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[plExitInterview_Audit_Insert] ON [dbo].[plExitInterview]
    FOR INSERT
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'plExitInterview','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ExtInterviewId
                               ,'EnrollmentId'
                               ,CONVERT(VARCHAR(8000),New.EnrollmentId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ExtInterviewId
                               ,'WantAssistance'
                               ,CONVERT(VARCHAR(8000),New.WantAssistance,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ExtInterviewId
                               ,'WaiverSigned'
                               ,CONVERT(VARCHAR(8000),New.WaiverSigned,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ExtInterviewId
                               ,'Reason'
                               ,CONVERT(VARCHAR(8000),New.Reason,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ExtInterviewId
                               ,'AreaId'
                               ,CONVERT(VARCHAR(8000),New.AreaId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ExtInterviewId
                               ,'HighSalary'
                               ,CONVERT(VARCHAR(8000),New.HighSalary,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ExtInterviewId
                               ,'TransportationId'
                               ,CONVERT(VARCHAR(8000),New.TransportationId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ExtInterviewId
                               ,'ScStatusId'
                               ,CONVERT(VARCHAR(8000),New.ScStatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ExtInterviewId
                               ,'Eligible'
                               ,CONVERT(VARCHAR(8000),New.Eligible,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ExtInterviewId
                               ,'AvailableDate'
                               ,CONVERT(VARCHAR(8000),New.AvailableDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ExtInterviewId
                               ,'AvailableDays'
                               ,CONVERT(VARCHAR(8000),New.AvailableDays,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ExtInterviewId
                               ,'AvailableHours'
                               ,CONVERT(VARCHAR(8000),New.AvailableHours,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ExtInterviewId
                               ,'LowSalary'
                               ,CONVERT(VARCHAR(8000),New.LowSalary,121)
                        FROM    Inserted New; 
            END; 
        END; 




    SET NOCOUNT OFF;

GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[plExitInterview_Audit_Update] ON [dbo].[plExitInterview]
    FOR UPDATE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'plExitInterview','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(EnrollmentId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ExtInterviewId
                                   ,'EnrollmentId'
                                   ,CONVERT(VARCHAR(8000),Old.EnrollmentId,121)
                                   ,CONVERT(VARCHAR(8000),New.EnrollmentId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ExtInterviewId = New.ExtInterviewId
                            WHERE   Old.EnrollmentId <> New.EnrollmentId
                                    OR (
                                         Old.EnrollmentId IS NULL
                                         AND New.EnrollmentId IS NOT NULL
                                       )
                                    OR (
                                         New.EnrollmentId IS NULL
                                         AND Old.EnrollmentId IS NOT NULL
                                       ); 
                IF UPDATE(WantAssistance)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ExtInterviewId
                                   ,'WantAssistance'
                                   ,CONVERT(VARCHAR(8000),Old.WantAssistance,121)
                                   ,CONVERT(VARCHAR(8000),New.WantAssistance,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ExtInterviewId = New.ExtInterviewId
                            WHERE   Old.WantAssistance <> New.WantAssistance
                                    OR (
                                         Old.WantAssistance IS NULL
                                         AND New.WantAssistance IS NOT NULL
                                       )
                                    OR (
                                         New.WantAssistance IS NULL
                                         AND Old.WantAssistance IS NOT NULL
                                       ); 
                IF UPDATE(WaiverSigned)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ExtInterviewId
                                   ,'WaiverSigned'
                                   ,CONVERT(VARCHAR(8000),Old.WaiverSigned,121)
                                   ,CONVERT(VARCHAR(8000),New.WaiverSigned,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ExtInterviewId = New.ExtInterviewId
                            WHERE   Old.WaiverSigned <> New.WaiverSigned
                                    OR (
                                         Old.WaiverSigned IS NULL
                                         AND New.WaiverSigned IS NOT NULL
                                       )
                                    OR (
                                         New.WaiverSigned IS NULL
                                         AND Old.WaiverSigned IS NOT NULL
                                       ); 
                IF UPDATE(Reason)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ExtInterviewId
                                   ,'Reason'
                                   ,CONVERT(VARCHAR(8000),Old.Reason,121)
                                   ,CONVERT(VARCHAR(8000),New.Reason,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ExtInterviewId = New.ExtInterviewId
                            WHERE   Old.Reason <> New.Reason
                                    OR (
                                         Old.Reason IS NULL
                                         AND New.Reason IS NOT NULL
                                       )
                                    OR (
                                         New.Reason IS NULL
                                         AND Old.Reason IS NOT NULL
                                       ); 
                IF UPDATE(AreaId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ExtInterviewId
                                   ,'AreaId'
                                   ,CONVERT(VARCHAR(8000),Old.AreaId,121)
                                   ,CONVERT(VARCHAR(8000),New.AreaId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ExtInterviewId = New.ExtInterviewId
                            WHERE   Old.AreaId <> New.AreaId
                                    OR (
                                         Old.AreaId IS NULL
                                         AND New.AreaId IS NOT NULL
                                       )
                                    OR (
                                         New.AreaId IS NULL
                                         AND Old.AreaId IS NOT NULL
                                       ); 
                IF UPDATE(HighSalary)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ExtInterviewId
                                   ,'HighSalary'
                                   ,CONVERT(VARCHAR(8000),Old.HighSalary,121)
                                   ,CONVERT(VARCHAR(8000),New.HighSalary,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ExtInterviewId = New.ExtInterviewId
                            WHERE   Old.HighSalary <> New.HighSalary
                                    OR (
                                         Old.HighSalary IS NULL
                                         AND New.HighSalary IS NOT NULL
                                       )
                                    OR (
                                         New.HighSalary IS NULL
                                         AND Old.HighSalary IS NOT NULL
                                       ); 
                IF UPDATE(TransportationId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ExtInterviewId
                                   ,'TransportationId'
                                   ,CONVERT(VARCHAR(8000),Old.TransportationId,121)
                                   ,CONVERT(VARCHAR(8000),New.TransportationId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ExtInterviewId = New.ExtInterviewId
                            WHERE   Old.TransportationId <> New.TransportationId
                                    OR (
                                         Old.TransportationId IS NULL
                                         AND New.TransportationId IS NOT NULL
                                       )
                                    OR (
                                         New.TransportationId IS NULL
                                         AND Old.TransportationId IS NOT NULL
                                       ); 
                IF UPDATE(ScStatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ExtInterviewId
                                   ,'ScStatusId'
                                   ,CONVERT(VARCHAR(8000),Old.ScStatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.ScStatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ExtInterviewId = New.ExtInterviewId
                            WHERE   Old.ScStatusId <> New.ScStatusId
                                    OR (
                                         Old.ScStatusId IS NULL
                                         AND New.ScStatusId IS NOT NULL
                                       )
                                    OR (
                                         New.ScStatusId IS NULL
                                         AND Old.ScStatusId IS NOT NULL
                                       ); 
                IF UPDATE(Eligible)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ExtInterviewId
                                   ,'Eligible'
                                   ,CONVERT(VARCHAR(8000),Old.Eligible,121)
                                   ,CONVERT(VARCHAR(8000),New.Eligible,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ExtInterviewId = New.ExtInterviewId
                            WHERE   Old.Eligible <> New.Eligible
                                    OR (
                                         Old.Eligible IS NULL
                                         AND New.Eligible IS NOT NULL
                                       )
                                    OR (
                                         New.Eligible IS NULL
                                         AND Old.Eligible IS NOT NULL
                                       ); 
                IF UPDATE(AvailableDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ExtInterviewId
                                   ,'AvailableDate'
                                   ,CONVERT(VARCHAR(8000),Old.AvailableDate,121)
                                   ,CONVERT(VARCHAR(8000),New.AvailableDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ExtInterviewId = New.ExtInterviewId
                            WHERE   Old.AvailableDate <> New.AvailableDate
                                    OR (
                                         Old.AvailableDate IS NULL
                                         AND New.AvailableDate IS NOT NULL
                                       )
                                    OR (
                                         New.AvailableDate IS NULL
                                         AND Old.AvailableDate IS NOT NULL
                                       ); 
                IF UPDATE(AvailableDays)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ExtInterviewId
                                   ,'AvailableDays'
                                   ,CONVERT(VARCHAR(8000),Old.AvailableDays,121)
                                   ,CONVERT(VARCHAR(8000),New.AvailableDays,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ExtInterviewId = New.ExtInterviewId
                            WHERE   Old.AvailableDays <> New.AvailableDays
                                    OR (
                                         Old.AvailableDays IS NULL
                                         AND New.AvailableDays IS NOT NULL
                                       )
                                    OR (
                                         New.AvailableDays IS NULL
                                         AND Old.AvailableDays IS NOT NULL
                                       ); 
                IF UPDATE(AvailableHours)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ExtInterviewId
                                   ,'AvailableHours'
                                   ,CONVERT(VARCHAR(8000),Old.AvailableHours,121)
                                   ,CONVERT(VARCHAR(8000),New.AvailableHours,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ExtInterviewId = New.ExtInterviewId
                            WHERE   Old.AvailableHours <> New.AvailableHours
                                    OR (
                                         Old.AvailableHours IS NULL
                                         AND New.AvailableHours IS NOT NULL
                                       )
                                    OR (
                                         New.AvailableHours IS NULL
                                         AND Old.AvailableHours IS NOT NULL
                                       ); 
                IF UPDATE(LowSalary)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ExtInterviewId
                                   ,'LowSalary'
                                   ,CONVERT(VARCHAR(8000),Old.LowSalary,121)
                                   ,CONVERT(VARCHAR(8000),New.LowSalary,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ExtInterviewId = New.ExtInterviewId
                            WHERE   Old.LowSalary <> New.LowSalary
                                    OR (
                                         Old.LowSalary IS NULL
                                         AND New.LowSalary IS NOT NULL
                                       )
                                    OR (
                                         New.LowSalary IS NULL
                                         AND Old.LowSalary IS NOT NULL
                                       ); 
            END; 
        END; 




    SET NOCOUNT OFF;

GO
ALTER TABLE [dbo].[plExitInterview] ADD CONSTRAINT [PK_plExitInterview_ExtInterviewId] PRIMARY KEY CLUSTERED  ([ExtInterviewId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[plExitInterview] ADD CONSTRAINT [FK__plExitInt__InelR__7C65823E] FOREIGN KEY ([InelReasonId]) REFERENCES [dbo].[syIneligibilityReasons] ([InelReasonId])
GO
ALTER TABLE [dbo].[plExitInterview] ADD CONSTRAINT [FK_plExitInterview_adCounties_AreaId_CountyId] FOREIGN KEY ([AreaId]) REFERENCES [dbo].[adCounties] ([CountyId])
GO
ALTER TABLE [dbo].[plExitInterview] ADD CONSTRAINT [FK_plExitInterview_adExpertiseLevel_ExpertiseLevelId_ExpertiseId] FOREIGN KEY ([ExpertiseLevelId]) REFERENCES [dbo].[adExpertiseLevel] ([ExpertiseId])
GO
ALTER TABLE [dbo].[plExitInterview] ADD CONSTRAINT [FK_plExitInterview_adFullPartTime_FullTimeId_FullTimeId] FOREIGN KEY ([FullTimeId]) REFERENCES [dbo].[adFullPartTime] ([FullTimeId])
GO
ALTER TABLE [dbo].[plExitInterview] ADD CONSTRAINT [FK_plExitInterview_arStuEnrollments_EnrollmentId_StuEnrollId] FOREIGN KEY ([EnrollmentId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
ALTER TABLE [dbo].[plExitInterview] ADD CONSTRAINT [FK_plExitInterview_plTransportation_TransportationId_TransportationId] FOREIGN KEY ([TransportationId]) REFERENCES [dbo].[plTransportation] ([TransportationId])
GO
ALTER TABLE [dbo].[plExitInterview] ADD CONSTRAINT [FK_plExitInterview_syStatuses_ScStatusId_StatusId] FOREIGN KEY ([ScStatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
