CREATE TABLE [dbo].[syMessageTemplates]
(
[MessageTemplateId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syMessageTemplates_MessageTemplateId] DEFAULT (newid()),
[StatusId] [uniqueidentifier] NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[TemplateDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[MessageSchemaId] [uniqueidentifier] NOT NULL,
[AdvantageEventId] [tinyint] NOT NULL CONSTRAINT [DF_syMessageTemplates_AdvantageEventId] DEFAULT ((0)),
[RecipientTypeId] [tinyint] NOT NULL CONSTRAINT [DF_syMessageTemplates_RecipientTypeId] DEFAULT ((1)),
[SqlStatement] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Xslt_Html] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Xslt_Rtf] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Xsl_Fo] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SpsData] [text] COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MessageTypeId] [tinyint] NOT NULL CONSTRAINT [DF_syMessageTemplates_MessageTypeId] DEFAULT ((1)),
[MessageFormatId] [tinyint] NOT NULL CONSTRAINT [DF_syMessageTemplates_MessageFormatId] DEFAULT ((1)),
[SendImmediately] [bit] NOT NULL CONSTRAINT [DF_syMessageTemplates_SendImmediately] DEFAULT ((0)),
[KeepHistory] [bit] NOT NULL CONSTRAINT [DF_syMessageTemplates_KeepHistory] DEFAULT ((1)),
[DelayTimeSpanUnitId] [tinyint] NOT NULL CONSTRAINT [DF_syMessageTemplates_DelayTimeSpanUnitId] DEFAULT ((1)),
[DelayTimeSpanNumberOfUnits] [tinyint] NOT NULL CONSTRAINT [DF_syMessageTemplates_DelayTimeSpanNumberOfUnits] DEFAULT ((0)),
[ModDate] [datetime] NOT NULL CONSTRAINT [DF_syMessageTemplates_ModDate] DEFAULT (((2005)-(1))-(1)),
[ModUser] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF_syMessageTemplates_ModUser] DEFAULT ('no user selected')
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[syMessageTemplates] ADD CONSTRAINT [PK_syMessageTemplates_MessageTemplateId] PRIMARY KEY CLUSTERED  ([MessageTemplateId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syMessageTemplates] ADD CONSTRAINT [FK_syMessageTemplates_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[syMessageTemplates] ADD CONSTRAINT [FK_syMessageTemplates_syMessageSchemas_MessageSchemaId_MessageSchemaId] FOREIGN KEY ([MessageSchemaId]) REFERENCES [dbo].[syMessageSchemas] ([MessageSchemaId])
GO
ALTER TABLE [dbo].[syMessageTemplates] ADD CONSTRAINT [FK_syMessageTemplates_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
