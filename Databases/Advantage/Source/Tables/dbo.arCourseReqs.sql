CREATE TABLE [dbo].[arCourseReqs]
(
[CourseReqId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arCourseReqs_CourseReqId] DEFAULT (newid()),
[ReqId] [uniqueidentifier] NOT NULL,
[PreCoReqId] [uniqueidentifier] NULL,
[CourseReqTypId] [tinyint] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL,
[PrgVerId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE TRIGGER [dbo].[arCourseReqs_Audit_Delete] ON [dbo].[arCourseReqs]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arCourseReqs','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CourseReqId
                               ,'PreCoReqId'
                               ,CONVERT(VARCHAR(8000),Old.PreCoReqId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CourseReqId
                               ,'ReqId'
                               ,CONVERT(VARCHAR(8000),Old.ReqId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CourseReqId
                               ,'CourseReqTypId'
                               ,CONVERT(VARCHAR(8000),Old.CourseReqTypId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.CourseReqId
                               ,'PrgVerId'
                               ,CONVERT(VARCHAR(8000),Old.PrgVerId,121)
                        FROM    Deleted Old; 
            END; 
        END;




    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE TRIGGER [dbo].[arCourseReqs_Audit_Insert] ON [dbo].[arCourseReqs]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arCourseReqs','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CourseReqId
                               ,'PreCoReqId'
                               ,CONVERT(VARCHAR(8000),New.PreCoReqId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CourseReqId
                               ,'ReqId'
                               ,CONVERT(VARCHAR(8000),New.ReqId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CourseReqId
                               ,'CourseReqTypId'
                               ,CONVERT(VARCHAR(8000),New.CourseReqTypId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.CourseReqId
                               ,'PrgVerId'
                               ,CONVERT(VARCHAR(8000),New.PrgVerId,121)
                        FROM    Inserted New; 
            END; 
        END;




    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE TRIGGER [dbo].[arCourseReqs_Audit_Update] ON [dbo].[arCourseReqs]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arCourseReqs','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(PreCoReqId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CourseReqId
                                   ,'PreCoReqId'
                                   ,CONVERT(VARCHAR(8000),Old.PreCoReqId,121)
                                   ,CONVERT(VARCHAR(8000),New.PreCoReqId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CourseReqId = New.CourseReqId
                            WHERE   Old.PreCoReqId <> New.PreCoReqId
                                    OR (
                                         Old.PreCoReqId IS NULL
                                         AND New.PreCoReqId IS NOT NULL
                                       )
                                    OR (
                                         New.PreCoReqId IS NULL
                                         AND Old.PreCoReqId IS NOT NULL
                                       ); 
                IF UPDATE(ReqId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CourseReqId
                                   ,'ReqId'
                                   ,CONVERT(VARCHAR(8000),Old.ReqId,121)
                                   ,CONVERT(VARCHAR(8000),New.ReqId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CourseReqId = New.CourseReqId
                            WHERE   Old.ReqId <> New.ReqId
                                    OR (
                                         Old.ReqId IS NULL
                                         AND New.ReqId IS NOT NULL
                                       )
                                    OR (
                                         New.ReqId IS NULL
                                         AND Old.ReqId IS NOT NULL
                                       ); 
                IF UPDATE(CourseReqTypId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CourseReqId
                                   ,'CourseReqTypId'
                                   ,CONVERT(VARCHAR(8000),Old.CourseReqTypId,121)
                                   ,CONVERT(VARCHAR(8000),New.CourseReqTypId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CourseReqId = New.CourseReqId
                            WHERE   Old.CourseReqTypId <> New.CourseReqTypId
                                    OR (
                                         Old.CourseReqTypId IS NULL
                                         AND New.CourseReqTypId IS NOT NULL
                                       )
                                    OR (
                                         New.CourseReqTypId IS NULL
                                         AND Old.CourseReqTypId IS NOT NULL
                                       ); 
                IF UPDATE(PrgVerId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.CourseReqId
                                   ,'PrgVerId'
                                   ,CONVERT(VARCHAR(8000),Old.PrgVerId,121)
                                   ,CONVERT(VARCHAR(8000),New.PrgVerId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CourseReqId = New.CourseReqId
                            WHERE   Old.PrgVerId <> New.PrgVerId
                                    OR (
                                         Old.PrgVerId IS NULL
                                         AND New.PrgVerId IS NOT NULL
                                       )
                                    OR (
                                         New.PrgVerId IS NULL
                                         AND Old.PrgVerId IS NOT NULL
                                       ); 

            END; 
        END;




    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[arCourseReqs] ADD CONSTRAINT [PK_arCourseReqs_CourseReqId] PRIMARY KEY CLUSTERED  ([CourseReqId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arCourseReqs] ADD CONSTRAINT [FK_arCourseReqs_arReqs_PreCoReqId_ReqId] FOREIGN KEY ([PreCoReqId]) REFERENCES [dbo].[arReqs] ([ReqId])
GO
ALTER TABLE [dbo].[arCourseReqs] ADD CONSTRAINT [FK_arCourseReqs_arPrgVersions_PrgVerId_PrgVerId] FOREIGN KEY ([PrgVerId]) REFERENCES [dbo].[arPrgVersions] ([PrgVerId])
GO
ALTER TABLE [dbo].[arCourseReqs] ADD CONSTRAINT [FK_arCourseReqs_arReqs_ReqId_ReqId] FOREIGN KEY ([ReqId]) REFERENCES [dbo].[arReqs] ([ReqId])
GO
