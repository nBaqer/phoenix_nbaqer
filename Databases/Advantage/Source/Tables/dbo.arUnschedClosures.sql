CREATE TABLE [dbo].[arUnschedClosures]
(
[UnschedClosureId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_arUnschedClosures_UnschedClosureId] DEFAULT (newid()),
[StartDate] [datetime] NOT NULL,
[EndDate] [datetime] NOT NULL,
[ClsSectionId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL,
[ClsMeetingId] [uniqueidentifier] NULL,
[RescheduledMeetingId] [uniqueidentifier] NULL,
[ClassReSchduledFrom] [datetime] NULL,
[ClassReSchduledTo] [datetime] NULL
) ON [PRIMARY]
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[arUnschedClosures_Audit_Delete] ON [dbo].[arUnschedClosures]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arUnschedClosures','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.UnschedClosureId
                               ,'ClsSectionId'
                               ,CONVERT(VARCHAR(8000),Old.ClsSectionId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.UnschedClosureId
                               ,'StartDate'
                               ,CONVERT(VARCHAR(8000),Old.StartDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.UnschedClosureId
                               ,'EndDate'
                               ,CONVERT(VARCHAR(8000),Old.EndDate,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[arUnschedClosures_Audit_Insert] ON [dbo].[arUnschedClosures]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arUnschedClosures','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.UnschedClosureId
                               ,'ClsSectionId'
                               ,CONVERT(VARCHAR(8000),New.ClsSectionId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.UnschedClosureId
                               ,'StartDate'
                               ,CONVERT(VARCHAR(8000),New.StartDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.UnschedClosureId
                               ,'EndDate'
                               ,CONVERT(VARCHAR(8000),New.EndDate,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[arUnschedClosures_Audit_Update] ON [dbo].[arUnschedClosures]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arUnschedClosures','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(ClsSectionId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.UnschedClosureId
                                   ,'ClsSectionId'
                                   ,CONVERT(VARCHAR(8000),Old.ClsSectionId,121)
                                   ,CONVERT(VARCHAR(8000),New.ClsSectionId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.UnschedClosureId = New.UnschedClosureId
                            WHERE   Old.ClsSectionId <> New.ClsSectionId
                                    OR (
                                         Old.ClsSectionId IS NULL
                                         AND New.ClsSectionId IS NOT NULL
                                       )
                                    OR (
                                         New.ClsSectionId IS NULL
                                         AND Old.ClsSectionId IS NOT NULL
                                       ); 
                IF UPDATE(StartDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.UnschedClosureId
                                   ,'StartDate'
                                   ,CONVERT(VARCHAR(8000),Old.StartDate,121)
                                   ,CONVERT(VARCHAR(8000),New.StartDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.UnschedClosureId = New.UnschedClosureId
                            WHERE   Old.StartDate <> New.StartDate
                                    OR (
                                         Old.StartDate IS NULL
                                         AND New.StartDate IS NOT NULL
                                       )
                                    OR (
                                         New.StartDate IS NULL
                                         AND Old.StartDate IS NOT NULL
                                       ); 
                IF UPDATE(EndDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.UnschedClosureId
                                   ,'EndDate'
                                   ,CONVERT(VARCHAR(8000),Old.EndDate,121)
                                   ,CONVERT(VARCHAR(8000),New.EndDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.UnschedClosureId = New.UnschedClosureId
                            WHERE   Old.EndDate <> New.EndDate
                                    OR (
                                         Old.EndDate IS NULL
                                         AND New.EndDate IS NOT NULL
                                       )
                                    OR (
                                         New.EndDate IS NULL
                                         AND Old.EndDate IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO

ALTER TABLE [dbo].[arUnschedClosures] ADD CONSTRAINT [PK_arUnschedClosures_UnschedClosureId] PRIMARY KEY CLUSTERED  ([UnschedClosureId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arUnschedClosures] ADD CONSTRAINT [FK_arUnschedClosures_arClassSections_ClsSectionId_ClsSectionId] FOREIGN KEY ([ClsSectionId]) REFERENCES [dbo].[arClassSections] ([ClsSectionId]) ON DELETE CASCADE ON UPDATE CASCADE
GO
