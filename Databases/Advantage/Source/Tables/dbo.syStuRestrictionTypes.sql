CREATE TABLE [dbo].[syStuRestrictionTypes]
(
[RestrictionTypeId] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[Code] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Descrip] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[CampGrpId] [uniqueidentifier] NOT NULL,
[Reason] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EnableAlertButton] [bit] NOT NULL,
[BlkGradeBook] [bit] NOT NULL,
[BlkAttendancePosting] [bit] NOT NULL,
[BlkFA] [bit] NOT NULL,
[BlkStuPortalClsSched] [bit] NOT NULL,
[BlkStuPortalGrdBk] [bit] NOT NULL,
[BlkStuPortalResume] [bit] NOT NULL,
[BlkStuPortalEntire] [bit] NOT NULL,
[BlkStuPortalCareer] [bit] NOT NULL,
[RaiseToRestrictionId] [uniqueidentifier] NULL,
[DelayDays] [smallint] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syStuRestrictionTypes] ADD CONSTRAINT [PK_syStuRestrictionTypes_RestrictionTypeId] PRIMARY KEY CLUSTERED  ([RestrictionTypeId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syStuRestrictionTypes] ADD CONSTRAINT [FK_syStuRestrictionTypes_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[syStuRestrictionTypes] ADD CONSTRAINT [FK_syStuRestrictionTypes_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
