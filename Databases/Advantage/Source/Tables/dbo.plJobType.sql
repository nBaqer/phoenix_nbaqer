CREATE TABLE [dbo].[plJobType]
(
[JobGroupId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_plJobType_JobGroupId] DEFAULT (newid()),
[JobGroupCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StatusId] [uniqueidentifier] NOT NULL,
[JobGroupDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CamGrpId] [uniqueidentifier] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[plJobType] ADD CONSTRAINT [PK_plJobType_JobGroupId] PRIMARY KEY CLUSTERED  ([JobGroupId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[plJobType] ADD CONSTRAINT [FK_plJobType_syCampGrps_CamGrpId_CampGrpId] FOREIGN KEY ([CamGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[plJobType] ADD CONSTRAINT [FK_plJobType_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
