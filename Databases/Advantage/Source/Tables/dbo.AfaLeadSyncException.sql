CREATE TABLE [dbo].[AfaLeadSyncException]
(
[ID] [uniqueidentifier] NOT NULL,
[LeadId] [uniqueidentifier] NULL,
[AfaStudentId] [bigint] NOT NULL,
[FirstName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LastName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExceptionReason] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AfaLeadSyncException] ADD CONSTRAINT [PK_AfaLeadSyncException] PRIMARY KEY CLUSTERED  ([ID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AfaLeadSyncException] ADD CONSTRAINT [FK_AfaLeadSyncException_adLeads_LeadId_LeadId] FOREIGN KEY ([LeadId]) REFERENCES [dbo].[adLeads] ([LeadId])
GO
