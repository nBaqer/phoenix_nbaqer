CREATE TABLE [dbo].[saPeriodicFees]
(
[PeriodicFeeId] [uniqueidentifier] NOT NULL ROWGUIDCOL,
[StatusId] [uniqueidentifier] NOT NULL,
[TermId] [uniqueidentifier] NOT NULL,
[TransCodeId] [uniqueidentifier] NOT NULL,
[TuitionCategoryId] [uniqueidentifier] NULL,
[Amount] [money] NOT NULL,
[RateScheduleId] [uniqueidentifier] NULL,
[UnitId] [smallint] NOT NULL,
[ApplyTo] [smallint] NOT NULL CONSTRAINT [DF_saPeriodicFees_ApplyTo] DEFAULT ((0)),
[ProgTypId] [uniqueidentifier] NULL,
[PrgVerId] [uniqueidentifier] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[TermStartDate] [datetime] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saPeriodicFees_Audit_Delete] ON [dbo].[saPeriodicFees]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saPeriodicFees','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PeriodicFeeId
                               ,'UnitId'
                               ,CONVERT(VARCHAR(8000),Old.UnitId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PeriodicFeeId
                               ,'ApplyTo'
                               ,CONVERT(VARCHAR(8000),Old.ApplyTo,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PeriodicFeeId
                               ,'Amount'
                               ,CONVERT(VARCHAR(8000),Old.Amount,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PeriodicFeeId
                               ,'TermId'
                               ,CONVERT(VARCHAR(8000),Old.TermId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PeriodicFeeId
                               ,'ProgTypId'
                               ,CONVERT(VARCHAR(8000),Old.ProgTypId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PeriodicFeeId
                               ,'TransCodeId'
                               ,CONVERT(VARCHAR(8000),Old.TransCodeId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PeriodicFeeId
                               ,'PrgVerId'
                               ,CONVERT(VARCHAR(8000),Old.PrgVerId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PeriodicFeeId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PeriodicFeeId
                               ,'RateScheduleId'
                               ,CONVERT(VARCHAR(8000),Old.RateScheduleId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.PeriodicFeeId
                               ,'TuitionCategoryId'
                               ,CONVERT(VARCHAR(8000),Old.TuitionCategoryId,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saPeriodicFees_Audit_Insert] ON [dbo].[saPeriodicFees]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saPeriodicFees','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PeriodicFeeId
                               ,'UnitId'
                               ,CONVERT(VARCHAR(8000),New.UnitId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PeriodicFeeId
                               ,'ApplyTo'
                               ,CONVERT(VARCHAR(8000),New.ApplyTo,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PeriodicFeeId
                               ,'Amount'
                               ,CONVERT(VARCHAR(8000),New.Amount,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PeriodicFeeId
                               ,'TermId'
                               ,CONVERT(VARCHAR(8000),New.TermId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PeriodicFeeId
                               ,'ProgTypId'
                               ,CONVERT(VARCHAR(8000),New.ProgTypId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PeriodicFeeId
                               ,'TransCodeId'
                               ,CONVERT(VARCHAR(8000),New.TransCodeId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PeriodicFeeId
                               ,'PrgVerId'
                               ,CONVERT(VARCHAR(8000),New.PrgVerId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PeriodicFeeId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PeriodicFeeId
                               ,'RateScheduleId'
                               ,CONVERT(VARCHAR(8000),New.RateScheduleId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.PeriodicFeeId
                               ,'TuitionCategoryId'
                               ,CONVERT(VARCHAR(8000),New.TuitionCategoryId,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[saPeriodicFees_Audit_Update] ON [dbo].[saPeriodicFees]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'saPeriodicFees','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(UnitId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PeriodicFeeId
                                   ,'UnitId'
                                   ,CONVERT(VARCHAR(8000),Old.UnitId,121)
                                   ,CONVERT(VARCHAR(8000),New.UnitId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PeriodicFeeId = New.PeriodicFeeId
                            WHERE   Old.UnitId <> New.UnitId
                                    OR (
                                         Old.UnitId IS NULL
                                         AND New.UnitId IS NOT NULL
                                       )
                                    OR (
                                         New.UnitId IS NULL
                                         AND Old.UnitId IS NOT NULL
                                       ); 
                IF UPDATE(ApplyTo)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PeriodicFeeId
                                   ,'ApplyTo'
                                   ,CONVERT(VARCHAR(8000),Old.ApplyTo,121)
                                   ,CONVERT(VARCHAR(8000),New.ApplyTo,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PeriodicFeeId = New.PeriodicFeeId
                            WHERE   Old.ApplyTo <> New.ApplyTo
                                    OR (
                                         Old.ApplyTo IS NULL
                                         AND New.ApplyTo IS NOT NULL
                                       )
                                    OR (
                                         New.ApplyTo IS NULL
                                         AND Old.ApplyTo IS NOT NULL
                                       ); 
                IF UPDATE(Amount)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PeriodicFeeId
                                   ,'Amount'
                                   ,CONVERT(VARCHAR(8000),Old.Amount,121)
                                   ,CONVERT(VARCHAR(8000),New.Amount,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PeriodicFeeId = New.PeriodicFeeId
                            WHERE   Old.Amount <> New.Amount
                                    OR (
                                         Old.Amount IS NULL
                                         AND New.Amount IS NOT NULL
                                       )
                                    OR (
                                         New.Amount IS NULL
                                         AND Old.Amount IS NOT NULL
                                       ); 
                IF UPDATE(TermId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PeriodicFeeId
                                   ,'TermId'
                                   ,CONVERT(VARCHAR(8000),Old.TermId,121)
                                   ,CONVERT(VARCHAR(8000),New.TermId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PeriodicFeeId = New.PeriodicFeeId
                            WHERE   Old.TermId <> New.TermId
                                    OR (
                                         Old.TermId IS NULL
                                         AND New.TermId IS NOT NULL
                                       )
                                    OR (
                                         New.TermId IS NULL
                                         AND Old.TermId IS NOT NULL
                                       ); 
                IF UPDATE(ProgTypId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PeriodicFeeId
                                   ,'ProgTypId'
                                   ,CONVERT(VARCHAR(8000),Old.ProgTypId,121)
                                   ,CONVERT(VARCHAR(8000),New.ProgTypId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PeriodicFeeId = New.PeriodicFeeId
                            WHERE   Old.ProgTypId <> New.ProgTypId
                                    OR (
                                         Old.ProgTypId IS NULL
                                         AND New.ProgTypId IS NOT NULL
                                       )
                                    OR (
                                         New.ProgTypId IS NULL
                                         AND Old.ProgTypId IS NOT NULL
                                       ); 
                IF UPDATE(TransCodeId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PeriodicFeeId
                                   ,'TransCodeId'
                                   ,CONVERT(VARCHAR(8000),Old.TransCodeId,121)
                                   ,CONVERT(VARCHAR(8000),New.TransCodeId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PeriodicFeeId = New.PeriodicFeeId
                            WHERE   Old.TransCodeId <> New.TransCodeId
                                    OR (
                                         Old.TransCodeId IS NULL
                                         AND New.TransCodeId IS NOT NULL
                                       )
                                    OR (
                                         New.TransCodeId IS NULL
                                         AND Old.TransCodeId IS NOT NULL
                                       ); 
                IF UPDATE(PrgVerId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PeriodicFeeId
                                   ,'PrgVerId'
                                   ,CONVERT(VARCHAR(8000),Old.PrgVerId,121)
                                   ,CONVERT(VARCHAR(8000),New.PrgVerId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PeriodicFeeId = New.PeriodicFeeId
                            WHERE   Old.PrgVerId <> New.PrgVerId
                                    OR (
                                         Old.PrgVerId IS NULL
                                         AND New.PrgVerId IS NOT NULL
                                       )
                                    OR (
                                         New.PrgVerId IS NULL
                                         AND Old.PrgVerId IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PeriodicFeeId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PeriodicFeeId = New.PeriodicFeeId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(RateScheduleId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PeriodicFeeId
                                   ,'RateScheduleId'
                                   ,CONVERT(VARCHAR(8000),Old.RateScheduleId,121)
                                   ,CONVERT(VARCHAR(8000),New.RateScheduleId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PeriodicFeeId = New.PeriodicFeeId
                            WHERE   Old.RateScheduleId <> New.RateScheduleId
                                    OR (
                                         Old.RateScheduleId IS NULL
                                         AND New.RateScheduleId IS NOT NULL
                                       )
                                    OR (
                                         New.RateScheduleId IS NULL
                                         AND Old.RateScheduleId IS NOT NULL
                                       ); 
                IF UPDATE(TuitionCategoryId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PeriodicFeeId
                                   ,'TuitionCategoryId'
                                   ,CONVERT(VARCHAR(8000),Old.TuitionCategoryId,121)
                                   ,CONVERT(VARCHAR(8000),New.TuitionCategoryId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PeriodicFeeId = New.PeriodicFeeId
                            WHERE   Old.TuitionCategoryId <> New.TuitionCategoryId
                                    OR (
                                         Old.TuitionCategoryId IS NULL
                                         AND New.TuitionCategoryId IS NOT NULL
                                       )
                                    OR (
                                         New.TuitionCategoryId IS NULL
                                         AND Old.TuitionCategoryId IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF; 
GO
ALTER TABLE [dbo].[saPeriodicFees] ADD CONSTRAINT [PK_saPeriodicFees_PeriodicFeeId] PRIMARY KEY CLUSTERED  ([PeriodicFeeId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saPeriodicFees] ADD CONSTRAINT [FK_saPeriodicFees_arPrgVersions_PrgVerId_PrgVerId] FOREIGN KEY ([PrgVerId]) REFERENCES [dbo].[arPrgVersions] ([PrgVerId])
GO
ALTER TABLE [dbo].[saPeriodicFees] ADD CONSTRAINT [FK_saPeriodicFees_arProgTypes_ProgTypId_ProgTypId] FOREIGN KEY ([ProgTypId]) REFERENCES [dbo].[arProgTypes] ([ProgTypId])
GO
ALTER TABLE [dbo].[saPeriodicFees] ADD CONSTRAINT [FK_saPeriodicFees_arTerm_TermId_TermId] FOREIGN KEY ([TermId]) REFERENCES [dbo].[arTerm] ([TermId])
GO
ALTER TABLE [dbo].[saPeriodicFees] ADD CONSTRAINT [FK_saPeriodicFees_saRateSchedules_RateScheduleId_RateScheduleId] FOREIGN KEY ([RateScheduleId]) REFERENCES [dbo].[saRateSchedules] ([RateScheduleId])
GO
ALTER TABLE [dbo].[saPeriodicFees] ADD CONSTRAINT [FK_saPeriodicFees_saTransCodes_TransCodeId_TransCodeId] FOREIGN KEY ([TransCodeId]) REFERENCES [dbo].[saTransCodes] ([TransCodeId])
GO
ALTER TABLE [dbo].[saPeriodicFees] ADD CONSTRAINT [FK_saPeriodicFees_saTuitionCategories_TuitionCategoryId_TuitionCategoryId] FOREIGN KEY ([TuitionCategoryId]) REFERENCES [dbo].[saTuitionCategories] ([TuitionCategoryId])
GO
ALTER TABLE [dbo].[saPeriodicFees] ADD CONSTRAINT [FK_saPeriodicFees_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
EXEC sp_addextendedproperty N'MS_Description', N'', 'SCHEMA', N'dbo', 'TABLE', N'saPeriodicFees', 'COLUMN', N'ApplyTo'
GO
