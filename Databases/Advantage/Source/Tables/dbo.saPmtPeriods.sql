CREATE TABLE [dbo].[saPmtPeriods]
(
[PmtPeriodId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_saPmtPeriods_PmtPeriodId] DEFAULT (newid()),
[IncrementId] [uniqueidentifier] NOT NULL,
[PeriodNumber] [int] NOT NULL,
[IncrementValue] [decimal] (10, 2) NOT NULL CONSTRAINT [DF_saPmtPeriods_IncrementValue] DEFAULT ((0.0)),
[CumulativeValue] [decimal] (10, 2) NOT NULL CONSTRAINT [DF_saPmtPeriods_CumulativeValue] DEFAULT ((0.0)),
[ChargeAmount] [money] NOT NULL CONSTRAINT [DF_saPmtPeriods_ChargeAmount] DEFAULT ((0)),
[IsCharged] [bit] NOT NULL CONSTRAINT [DF_saPmtPeriods_IsCharged] DEFAULT ((0)),
[ModUser] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL CONSTRAINT [DF_saPmtPeriods_ModDate] DEFAULT (getdate()),
[TransactionCodeId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		<Author,Sweta>
-- Create date: <Create Date,10/3/2019>
-- Description:	<Description,Updates period number depending on the increment value>
-- =============================================
CREATE TRIGGER [dbo].[saPmtPeriods_PeriodNumber]
ON [dbo].[saPmtPeriods]
AFTER UPDATE, INSERT, DELETE
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;

        DECLARE @incrementId UNIQUEIDENTIFIER;
		IF EXISTS (SELECT * FROM inserted) AND NOT EXISTS(SELECT * FROM deleted)
		BEGIN
		    SET @incrementId = (
                           SELECT TOP 1 Inserted.IncrementId
                           FROM   Inserted
                           );
		END
        ELSE	
		BEGIN
		    SET @incrementId = (
                           SELECT TOP 1 Deleted.IncrementId
                           FROM   Deleted
                           );
		END
        DECLARE @pmtTbl AS TABLE
            (
                NewPeriod INT
               ,PmtPeriodId UNIQUEIDENTIFIER
               ,PeriodNumber INT
               ,IncrementValue DECIMAL(10, 2)
            );
        INSERT INTO @pmtTbl (
                            NewPeriod
                           ,PmtPeriodId
                           ,PeriodNumber
                           ,IncrementValue
                            )
                    SELECT DENSE_RANK() OVER ( ORDER BY IncrementValue ) AS newPeriod
                          ,PmtPeriodId
                          ,PeriodNumber
                          ,IncrementValue
                    FROM   dbo.saPmtPeriods
                    WHERE  IncrementId = @incrementId;

        UPDATE     dbo.saPmtPeriods
        SET        PeriodNumber = [@pmtTbl].NewPeriod
        FROM       dbo.saPmtPeriods
        INNER JOIN @pmtTbl ON [@pmtTbl].PmtPeriodId = saPmtPeriods.PmtPeriodId
        WHERE      IncrementId = @incrementId;

    END;
	SET NOCOUNT OFF;
GO
ALTER TABLE [dbo].[saPmtPeriods] ADD CONSTRAINT [PK_saPmtPeriods_PmtPeriodId] PRIMARY KEY CLUSTERED  ([PmtPeriodId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[saPmtPeriods] ADD CONSTRAINT [FK_saPmtPeriods_saIncrements_IncrementId_IncrementId] FOREIGN KEY ([IncrementId]) REFERENCES [dbo].[saIncrements] ([IncrementId])
GO
ALTER TABLE [dbo].[saPmtPeriods] ADD CONSTRAINT [FK_saPmtPeriods_saTransCodes_TransactionCodeId_TransCodeId] FOREIGN KEY ([TransactionCodeId]) REFERENCES [dbo].[saTransCodes] ([TransCodeId])
GO
