CREATE TABLE [dbo].[arFERPAPolicy]
(
[FERPAPolicyId] [uniqueidentifier] NOT NULL,
[FERPAEntityId] [uniqueidentifier] NOT NULL,
[FERPACategoryId] [uniqueidentifier] NOT NULL,
[StudentId] [uniqueidentifier] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arFERPAPolicy] ADD CONSTRAINT [PK_arFERPAPolicy_FERPAPolicyId] PRIMARY KEY CLUSTERED  ([FERPAPolicyId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[arFERPAPolicy] ADD CONSTRAINT [FK_arFERPAPolicy_arFERPACategory_FERPACategoryId_FERPACategoryId] FOREIGN KEY ([FERPACategoryId]) REFERENCES [dbo].[arFERPACategory] ([FERPACategoryId])
GO
ALTER TABLE [dbo].[arFERPAPolicy] ADD CONSTRAINT [FK_arFERPAPolicy_arFERPAEntity_FERPAEntityId_FERPAEntityId] FOREIGN KEY ([FERPAEntityId]) REFERENCES [dbo].[arFERPAEntity] ([FERPAEntityId])
GO
