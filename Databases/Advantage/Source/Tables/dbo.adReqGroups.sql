CREATE TABLE [dbo].[adReqGroups]
(
[ReqGrpId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_adReqGroups_ReqGrpId] DEFAULT (newid()),
[Code] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Descrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusId] [uniqueidentifier] NULL,
[CampGrpId] [uniqueidentifier] NULL,
[ModDate] [datetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsMandatoryReqGrp] [bit] NULL CONSTRAINT [DF_adReqGroups_IsMandatoryReqGrp] DEFAULT ((0))
) ON [PRIMARY]
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[adReqGroups_Audit_Delete] ON [dbo].[adReqGroups]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adReqGroups','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ReqGrpId
                               ,'Code'
                               ,CONVERT(VARCHAR(8000),Old.Code,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ReqGrpId
                               ,'Descrip'
                               ,CONVERT(VARCHAR(8000),Old.Descrip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ReqGrpId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.ReqGrpId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                        FROM    Deleted Old; 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE TRIGGER [dbo].[adReqGroups_Audit_Insert] ON [dbo].[adReqGroups]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adReqGroups','I',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ReqGrpId
                               ,'Code'
                               ,CONVERT(VARCHAR(8000),New.Code,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ReqGrpId
                               ,'Descrip'
                               ,CONVERT(VARCHAR(8000),New.Descrip,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ReqGrpId
                               ,'StatusId'
                               ,CONVERT(VARCHAR(8000),New.StatusId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.ReqGrpId
                               ,'CampGrpId'
                               ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                        FROM    Inserted New; 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE TRIGGER [dbo].[adReqGroups_Audit_Update] ON [dbo].[adReqGroups]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adReqGroups','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(Code)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ReqGrpId
                                   ,'Code'
                                   ,CONVERT(VARCHAR(8000),Old.Code,121)
                                   ,CONVERT(VARCHAR(8000),New.Code,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ReqGrpId = New.ReqGrpId
                            WHERE   Old.Code <> New.Code
                                    OR (
                                         Old.Code IS NULL
                                         AND New.Code IS NOT NULL
                                       )
                                    OR (
                                         New.Code IS NULL
                                         AND Old.Code IS NOT NULL
                                       ); 
                IF UPDATE(Descrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ReqGrpId
                                   ,'Descrip'
                                   ,CONVERT(VARCHAR(8000),Old.Descrip,121)
                                   ,CONVERT(VARCHAR(8000),New.Descrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ReqGrpId = New.ReqGrpId
                            WHERE   Old.Descrip <> New.Descrip
                                    OR (
                                         Old.Descrip IS NULL
                                         AND New.Descrip IS NOT NULL
                                       )
                                    OR (
                                         New.Descrip IS NULL
                                         AND Old.Descrip IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ReqGrpId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ReqGrpId = New.ReqGrpId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.ReqGrpId
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.ReqGrpId = New.ReqGrpId
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 
            END; 
        END; 



    SET NOCOUNT OFF; 
GO
ALTER TABLE [dbo].[adReqGroups] ADD CONSTRAINT [PK_adReqGroups_ReqGrpId] PRIMARY KEY CLUSTERED  ([ReqGrpId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[adReqGroups] ADD CONSTRAINT [FK_adReqGroups_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[adReqGroups] ADD CONSTRAINT [FK_adReqGroups_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
