CREATE TABLE [dbo].[tmTaskResults]
(
[TaskResultId] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_tmTaskResults_TaskResultId] DEFAULT (newid()),
[TaskId] [uniqueidentifier] NULL,
[ResultId] [uniqueidentifier] NULL,
[ModDate] [datetime] NULL,
[ModUser] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tmTaskResults] ADD CONSTRAINT [PK_tmTaskResults_TaskResultId] PRIMARY KEY CLUSTERED  ([TaskResultId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tmTaskResults] ADD CONSTRAINT [FK_tmTaskResults_tmResults_ResultId_ResultId] FOREIGN KEY ([ResultId]) REFERENCES [dbo].[tmResults] ([ResultId])
GO
ALTER TABLE [dbo].[tmTaskResults] ADD CONSTRAINT [FK_tmTaskResults_tmTasks_TaskId_TaskId] FOREIGN KEY ([TaskId]) REFERENCES [dbo].[tmTasks] ([TaskId])
GO
EXEC sp_addextendedproperty N'MS_Description', N'Link to tmResults (provides the Description).', 'SCHEMA', N'dbo', 'TABLE', N'tmTaskResults', 'COLUMN', N'ResultId'
GO
EXEC sp_addextendedproperty N'MS_Description', N'Link to tmTasks', 'SCHEMA', N'dbo', 'TABLE', N'tmTaskResults', 'COLUMN', N'TaskId'
GO
