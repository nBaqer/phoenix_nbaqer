CREATE TABLE [dbo].[SyLoggerDualEnrollment]
(
[LoggerId] [bigint] NOT NULL IDENTITY(1, 1),
[StudentId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FullName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClassId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ClassSection] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[RequirementCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[StartDate] [datetime] NULL,
[EndDate] [datetime] NULL,
[Classification] [int] NOT NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SyLoggerDualEnrollment] ADD CONSTRAINT [PK_SyLoggerDualEnrollment_LoggerId] PRIMARY KEY CLUSTERED  ([LoggerId]) ON [PRIMARY]
GO
