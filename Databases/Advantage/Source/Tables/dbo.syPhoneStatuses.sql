CREATE TABLE [dbo].[syPhoneStatuses]
(
[PhoneStatusID] [uniqueidentifier] NOT NULL ROWGUIDCOL CONSTRAINT [DF_syPhoneStatuses_PhoneStatusID] DEFAULT (newid()),
[PhoneStatusCode] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PhoneStatusDescrip] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusId] [uniqueidentifier] NULL,
[CampGrpId] [uniqueidentifier] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syPhoneStatuses] ADD CONSTRAINT [PK_syPhoneStatuses_PhoneStatusID] PRIMARY KEY CLUSTERED  ([PhoneStatusID]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[syPhoneStatuses] ADD CONSTRAINT [FK_syPhoneStatuses_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
ALTER TABLE [dbo].[syPhoneStatuses] ADD CONSTRAINT [FK_syPhoneStatuses_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
