/*
This migration script replaces uncommitted changes made to these objects:
syPeriodsWorkDays

Use this script to make necessary schema and data changes for these objects only. Schema changes to any other objects won't be deployed.

Schema changes and migration scripts are deployed in the order they're committed.

Migration scripts must not reference static data. When you deploy migration scripts alongside static data 
changes, the migration scripts will run first. This can cause the deployment to fail. 
Read more at https://documentation.red-gate.com/display/SOC6/Static+data+and+migrations.
*/

SET NUMERIC_ROUNDABORT OFF;
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON;
GO
DELETE FROM dbo.[syPeriodsWorkDays]
WHERE PeriodId NOT IN (
                      SELECT PeriodId
                      FROM   [syPeriods]
                      );

IF NOT EXISTS (
              SELECT OBJECT_NAME(object_id) AS NameofConstraint
                    ,SCHEMA_NAME(schema_id) AS SchemaName
                    ,OBJECT_NAME(parent_object_id) AS TableName
                    ,type_desc AS ConstraintType
              FROM   sys.objects
              WHERE  type_desc LIKE '%CONSTRAINT'
                     AND OBJECT_NAME(object_id) = 'FK_syPeriodsWorkDays_syPeriods_PeriodId_PeriodId'
              )
    BEGIN
        PRINT N'Adding foreign keys to [dbo].[syPeriodsWorkDays]';

        ALTER TABLE [dbo].[syPeriodsWorkDays]
        ADD CONSTRAINT [FK_syPeriodsWorkDays_syPeriods_PeriodId_PeriodId]
            FOREIGN KEY ( [PeriodId] )
            REFERENCES [dbo].[syPeriods] ( [PeriodId] );
    END;
GO


