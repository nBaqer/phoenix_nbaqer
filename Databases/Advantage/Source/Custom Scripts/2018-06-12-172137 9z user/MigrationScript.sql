/****** Script for SelectTopNRows command from SSMS  ******/
DECLARE @TempSettingsTable TABLE
    (
        SettingId INT
       ,KeyName VARCHAR(MAX)
       ,Description VARCHAR(MAX)
       ,ModUser VARCHAR(MAX)
       ,ModDate DATETIME
       ,CampusSpecific BIT
       ,ExtraConfirmation BIT
    );

--Insert Data in current table
INSERT INTO @TempSettingsTable (
                               SettingId
                              ,KeyName
                              ,Description
                              ,ModUser
                              ,ModDate
                              ,CampusSpecific
                              ,ExtraConfirmation
                               )
            SELECT SettingId
                  ,KeyName
                  ,Description
                  ,ModUser
                  ,ModDate
                  ,CampusSpecific
                  ,ExtraConfirmation
            FROM   dbo.syConfigAppSettings;

IF EXISTS (
          SELECT OBJECT_NAME(object_id) AS NameofConstraint
                ,SCHEMA_NAME(schema_id) AS SchemaName
                ,OBJECT_NAME(parent_object_id) AS TableName
                ,type_desc AS ConstraintType
          FROM   sys.objects
          WHERE  type_desc LIKE '%CONSTRAINT'
                 AND OBJECT_NAME(object_id) = 'PK_syConfigAppSettings_SettingId'
          )
    BEGIN
        PRINT N'Dropping constraints from [dbo].[References]';
        ALTER TABLE [dbo].syConfigAppSettings
        DROP CONSTRAINT [PK_syConfigAppSettings_SettingId];
    END;

-- drop old column
ALTER TABLE [dbo].syConfigAppSettings
DROP COLUMN SettingId;

--add new column with identity
DELETE FROM [dbo].syConfigAppSettings;

--add new column with identity
ALTER TABLE [dbo].syConfigAppSettings
ADD SettingId INT IDENTITY(1, 1) NOT NULL CONSTRAINT PK_syConfigAppSettings_SettingId
        PRIMARY KEY CLUSTERED ( SettingId );


SET IDENTITY_INSERT [dbo].syConfigAppSettings ON;
INSERT INTO dbo.syConfigAppSettings (
                                    SettingId
                                   ,KeyName
                                   ,Description
                                   ,ModUser
                                   ,ModDate
                                   ,CampusSpecific
                                   ,ExtraConfirmation
                                    )
            SELECT SettingId
                  ,KeyName
                  ,Description
                  ,ModUser
                  ,ModDate
                  ,CampusSpecific
                  ,ExtraConfirmation
            FROM   @TempSettingsTable;

SET IDENTITY_INSERT [dbo].syConfigAppSettings OFF;





