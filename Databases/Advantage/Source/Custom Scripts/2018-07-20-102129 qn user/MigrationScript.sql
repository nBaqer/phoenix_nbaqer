--=================================================================================================
--START
-- AD-2911
-- Set the arStuEnrollment LeadId column to not null. This will add data integrity validation to the database.
-- Jose A 7-19-18
-- START
--=================================================================================================

UPDATE E
SET E.LeadId = L.LeadId
FROM dbo.arStuEnrollments E
JOIN arstudent S ON S.StudentId = E.StudentId
JOIN adleads L ON L.StudentId = S.StudentId
WHERE E.LeadId <> L.LeadId OR E.LeadId IS NULL


--=================================================================================================
--END
-- AD-2911
-- Set the arStuEnrollment LeadId column to not null. This will add data integrity validation to the database.
-- END
--=================================================================================================

