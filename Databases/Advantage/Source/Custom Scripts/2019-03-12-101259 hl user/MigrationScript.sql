/*
This migration script replaces uncommitted changes made to these objects:
arStudentClockAttendance

Use this script to make necessary schema and data changes for these objects only. Schema changes to any other objects won't be deployed.

Schema changes and migration scripts are deployed in the order they're committed.

Migration scripts must not reference static data. When you deploy migration scripts alongside static data 
changes, the migration scripts will run first. This can cause the deployment to fail. 
Read more at https://documentation.red-gate.com/display/SOC6/Static+data+and+migrations.
*/

SET NUMERIC_ROUNDABORT OFF;
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON;
GO
PRINT N'Adding foreign keys to [dbo].[arStudentClockAttendance]';
GO
IF NOT EXISTS
(
    SELECT 1
    FROM sys.foreign_keys
    WHERE object_id = OBJECT_ID(N'[dbo].[FK_arStudentClockAttendance_arStuEnrollments_StuEnrollId_StuEnrollId]', 'F')
          AND parent_object_id = OBJECT_ID(N'[dbo].[arStudentClockAttendance]', 'U')
)
BEGIN
    DELETE FROM arStudentClockAttendance
    WHERE StuEnrollId IN
          (
              SELECT StuEnrollId
              FROM dbo.arStudentClockAttendance
              WHERE StuEnrollId NOT IN
                    (
                        SELECT enrollments.StuEnrollId FROM dbo.arStuEnrollments enrollments
                    )
              GROUP BY StuEnrollId
          );
    ALTER TABLE [dbo].[arStudentClockAttendance]
    ADD CONSTRAINT [FK_arStudentClockAttendance_arStuEnrollments_StuEnrollId_StuEnrollId]
        FOREIGN KEY ([StuEnrollId])
        REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId]);
END;
GO

