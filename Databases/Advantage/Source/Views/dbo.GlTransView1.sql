SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


 
CREATE VIEW [dbo].[GlTransView1]
AS
    SELECT  T.TransTypeId
           ,T.TransCodeId
           ,TCGLA.TypeId
           ,T.TransAmount
           ,TCGLA.TransCodeGLAccountId
           ,Percentage
           ,T.TransactionId
           ,T.TransDate AS Date
           ,T.TransDescrip + '-' + (
                                     SELECT FirstName + ' ' + LastName
                                     FROM   arStudent
                                     WHERE  StudentId = (
                                                          SELECT    StudentId
                                                          FROM      arStuEnrollments
                                                          WHERE     StuEnrollId = T.StuEnrollId
                                                        )
                                   ) AS TransCodeDescrip
           ,(
              SELECT    TransCodeDescrip
              FROM      saTransCodes
              WHERE     TransCodeId = T.TransCodeId
            ) TransCodeDescripSummary
           ,TCGLA.GLAccount
           ,(
              SELECT    CASE WHEN TypeId = 0
                                  OR TypeId = 2 THEN T.TransAmount * ( Percentage / 100 )
                             WHEN TypeId = 1
                                  OR TypeId = 3 THEN T.TransAmount * ( Percentage / 100 ) * ( -1 )
                        END
            ) AS Amount
           ,T.CampusId
           ,TC.TransCodeDescrip AS TransCode
    FROM    saTransactions T
           ,dbo.saTransCodeGLAccounts TCGLA
           ,saTransCodes TC
    WHERE   TCGLA.TransCodeId = T.TransCodeId
            AND T.Voided = 0
            AND TypeId IN ( 0,1 )
            AND T.TransCodeId = TC.TransCodeId
    UNION ALL
    SELECT  T.TransTypeId
           ,T.TransCodeId
           ,TCGLA.TypeId
           ,T.TransAmount
           ,TCGLA.TransCodeGLAccountId
           ,Percentage
           ,T.TransactionId
           ,D.DefRevenueDate AS Date
           ,T.TransDescrip + '-' + (
                                     SELECT FirstName + ' ' + LastName
                                     FROM   arStudent
                                     WHERE  StudentId = (
                                                          SELECT    StudentId
                                                          FROM      arStuEnrollments
                                                          WHERE     StuEnrollId = T.StuEnrollId
                                                        )
                                   ) AS TransCodeDescrip
           ,(
              SELECT    TransCodeDescrip
              FROM      saTransCodes
              WHERE     TransCodeId = T.TransCodeId
            ) TransCodeDescripSummary
           ,TCGLA.GLAccount
           ,(
              SELECT    CASE WHEN TypeId = 0
                                  OR TypeId = 2 THEN D.Amount
                             WHEN TypeId = 1
                                  OR TypeId = 3 THEN D.Amount * ( -1 )
                        END
            ) AS Amount
           ,T.CampusId
           ,TC.TransCodeDescrip AS TransCode
    FROM    dbo.saDeferredRevenues D
           ,saTransactions T
           ,dbo.saTransCodeGLAccounts TCGLA
           ,saTransCodes TC
    WHERE   T.TransactionId = D.TransactionId
            AND TCGLA.TransCodeId = T.TransCodeId
            AND T.Voided = 0
            AND TypeId IN ( 2,3 )
            AND T.TransCodeId = TC.TransCodeId
            AND TC.TransCodeId = TCGLA.TransCodeId
    UNION ALL
    SELECT  T.TransTypeId
           ,T.PaymentCodeId
           ,TCGLA.TypeId
           ,T.TransAmount
           ,TCGLA.TransCodeGLAccountId
           ,Percentage
           ,T.TransactionId
           ,T.TransDate AS Date
           ,T.TransDescrip + '-' + (
                                     SELECT FirstName + ' ' + LastName
                                     FROM   arStudent
                                     WHERE  StudentId = (
                                                          SELECT    StudentId
                                                          FROM      arStuEnrollments
                                                          WHERE     StuEnrollId = T.StuEnrollId
                                                        )
                                   ) AS TransCodeDescrip
           ,(
              SELECT    TransCodeDescrip
              FROM      saTransCodes
              WHERE     TransCodeId = T.TransCodeId
            ) TransCodeDescripSummary
           ,TCGLA.GLAccount
           ,(
              SELECT    CASE WHEN TypeId = 0
                                  OR TypeId = 2 THEN T.TransAmount * ( Percentage / 100 )
                             WHEN TypeId = 1
                                  OR TypeId = 3 THEN T.TransAmount * ( Percentage / 100 ) * ( -1 )
                        END
            ) AS Amount
           ,T.CampusId
           ,TC.TransCodeDescrip AS TransCode
    FROM    saTransactions T
           ,dbo.saTransCodeGLAccounts TCGLA
           ,saTransCodes TC
    WHERE   TCGLA.TransCodeId = T.PaymentCodeId
            AND T.Voided = 0
            AND TypeId IN ( 0,1 )
            AND T.TransCodeId = TC.TransCodeId; 



GO
