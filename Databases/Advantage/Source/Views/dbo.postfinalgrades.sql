SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[postfinalgrades]
AS
    SELECT  e.Grade
           ,e.GrdSysDetailId
    FROM    dbo.arClassSections a
    INNER JOIN dbo.arGradeScales b ON a.GrdScaleId = b.GrdScaleId
    INNER JOIN dbo.arGradeScaleDetails c ON b.GrdScaleId = c.GrdScaleId
    INNER JOIN dbo.arGradeSystems d ON b.GrdSystemId = d.GrdSystemId
    INNER JOIN dbo.arGradeSystemDetails e ON c.GrdSysDetailId = e.GrdSysDetailId
    WHERE   ( a.ClsSectionId = '1E872B0C-A440-448D-A867-64CBA2326A27' );


GO
