SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[plStudentExtraCurriculars]
AS
    SELECT  AEC.IdExtraCurricular AS StuExtraCurricularID
           ,NULL AS ExtraCurricularID  --adExtraCurr
           ,AL.StudentId AS StudentId
           ,AEC.ExtraCurrGrpId AS ExtraCurricularGrpID   -- adExtraCurrGrp
           ,AEC.ModDate AS ModDate
           ,AEC.ModUser AS ModUser
    FROM    adExtraCurricular AS AEC
    INNER JOIN adLeads AS AL ON AL.LeadId = AEC.LeadId
    WHERE   AL.StudentId <> '00000000-0000-0000-0000-000000000000';
GO
