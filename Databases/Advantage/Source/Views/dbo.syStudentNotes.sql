SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[syStudentNotes]
AS
    SELECT  AN.NotesId AS StudentNoteId
           ,(
              SELECT    SS.StatusId
              FROM      syStatuses AS SS
              WHERE     SS.Status = 'Active'
            ) AS StatusId
           ,AL.StudentId
           ,AN.NoteText AS StudentNoteDescrip
           ,AN.ModuleCode
           ,AN.UserId
           ,AN.ModDate AS CreatedDate
           ,AN.ModUser
           ,AN.ModDate
    FROM    AllNotes AS AN
    JOIN    dbo.adLead_Notes AS ALN ON ALN.NotesId = AN.NotesId
    JOIN    dbo.adLeads AS AL ON AL.LeadId = ALN.LeadId
    WHERE   AL.StudentId <> '00000000-0000-0000-0000-000000000000';
GO
