SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
 
CREATE VIEW [dbo].[VIEW_LeadUserTask]
AS
    SELECT  ut.UserTaskId
           ,ut.ReId AS LeadId
           ,task.Descrip AS Activity
           ,task.Code AS ActivityCode
           ,task.CampGroupId
           ,ut.Status
           ,ut.EndDate
           ,ut.StartDate
    FROM    dbo.tmUserTasks AS ut
    INNER JOIN dbo.adLeads AS l ON l.LeadId = ut.ReId
    INNER JOIN dbo.syStatusCodes AS sta ON sta.StatusCodeId = l.LeadStatus
    INNER JOIN dbo.tmTasks AS task ON task.TaskId = ut.TaskId
    WHERE   (
              sta.SysStatusId = 1
              OR sta.SysStatusId = 2
              OR sta.SysStatusId = 4
              OR sta.SysStatusId = 5
            )
            AND (
                  ut.Status = 0
                  OR ut.Status = 2
                ); 
 
 
GO
