SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[SettingStdScoreLimit]
AS
    SELECT  dbo.arGradeScaleDetails.GrdScaleId
           ,MIN(dbo.arGradeScaleDetails.MinVal) AS Expr1
           ,MAX(dbo.arGradeScaleDetails.MaxVal) AS Expr2
    FROM    dbo.arPrgVersions
    INNER JOIN dbo.arStuEnrollments ON dbo.arPrgVersions.PrgVerId = dbo.arStuEnrollments.PrgVerId
    INNER JOIN dbo.arGradeScaleDetails
    INNER JOIN dbo.arGradeScales ON dbo.arGradeScaleDetails.GrdScaleId = dbo.arGradeScales.GrdScaleId ON dbo.arPrgVersions.ThGrdScaleId = dbo.arGradeScales.GrdScaleId
    WHERE   ( dbo.arStuEnrollments.StuEnrollId = '85DDDF78-F3CA-4783-8E42-35DD949FF36E' )
    GROUP BY dbo.arGradeScaleDetails.GrdScaleId;


GO
