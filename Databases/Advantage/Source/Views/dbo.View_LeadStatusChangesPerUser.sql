SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[View_LeadStatusChangesPerUser]
AS
    SELECT NEWID() AS Id
          ,UserId
          ,CampusId
          ,OrigStatusId
          ,NewStatusId
    FROM   (
           SELECT DISTINCT rolcam.UserId
                 ,cgc.CampusId
                 ,sta1.OrigStatusId
                 ,sta1.NewStatusId
           FROM   dbo.syLeadStatusChangePermissions AS per
           INNER JOIN dbo.syLeadStatusChanges AS sta1 ON sta1.LeadStatusChangeId = per.LeadStatusChangeId
           INNER JOIN dbo.syStatusCodes AS sc1 ON sc1.StatusCodeId = sta1.NewStatusId
           INNER JOIN dbo.syStatusCodes AS sc2 ON sc2.StatusCodeId = sta1.OrigStatusId
           INNER JOIN dbo.syRoles AS r ON r.RoleId = per.RoleId
           INNER JOIN dbo.syUsersRolesCampGrps AS rolcam ON rolcam.RoleId = r.RoleId
           INNER JOIN dbo.syCampGrps AS cg ON cg.CampGrpId = rolcam.CampGrpId
           INNER JOIN dbo.syCmpGrpCmps AS cgc ON cgc.CampGrpId = cg.CampGrpId
           INNER JOIN dbo.syCampuses AS camp ON camp.CampusId = cgc.CampusId
           INNER JOIN dbo.syUsers AS u ON u.UserId = rolcam.UserId
           INNER JOIN dbo.syStatuses AS act ON act.StatusId = per.StatusId
           INNER JOIN dbo.syStatuses AS act1 ON act1.StatusId = cg.StatusId
           INNER JOIN dbo.syStatuses AS act2 ON act2.StatusId = camp.StatusId
           WHERE  ( act.StatusCode = 'A' )
                  AND ( act1.StatusCode = 'A' )
                  AND ( act2.StatusCode = 'A' )
                  AND (
                      (
                      sc2.SysStatusId = 6
                      AND sc1.SysStatusId = 6
                      )
                      OR (
                         sc2.SysStatusId <> 6
                         AND sc1.SysStatusId <> 6
                         )
                      )
           ) AS T;
GO
