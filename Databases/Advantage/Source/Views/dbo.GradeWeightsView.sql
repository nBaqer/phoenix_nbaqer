SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[GradeWeightsView]
AS
    SELECT  dbo.arGrdBkWeights.InstrGrdBkWgtId
           ,dbo.arGrdBkWgtDetails.Code
           ,dbo.arGrdBkWgtDetails.Descrip
           ,dbo.arGrdBkWgtDetails.Weight
           ,dbo.arGrdBkWgtDetails.Seq
           ,dbo.arGrdBkWeights.InstructorId
           ,dbo.arGrdBkWeights.Descrip AS Expr1
    FROM    dbo.arGrdBkWeights
    INNER JOIN dbo.arGrdBkWgtDetails ON dbo.arGrdBkWeights.InstrGrdBkWgtId = dbo.arGrdBkWgtDetails.InstrGrdBkWgtId;


GO
