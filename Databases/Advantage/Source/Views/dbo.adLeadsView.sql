SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--=================================================================================================
-- CREATE VIEW adLeadsView
--=================================================================================================
CREATE VIEW [dbo].[adLeadsView]
AS
    SELECT AL.LeadId
          ,(
           SELECT   TOP 1 ALE.EMail
           FROM     AdLeadEmail AS ALE
           WHERE    ALE.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Status = 'Active'
                    AND ALE.IsPreferred = 1
                    AND ALE.LeadId = AL.LeadId
           ORDER BY ALE.ModDate DESC
           ) AS Email_Best
          ,(
           SELECT   TOP 1 ALE.EMail
           FROM     AdLeadEmail AS ALE
           WHERE    ALE.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Status = 'Active'
                    AND ALE.IsPreferred = 0
                    AND ALE.LeadId = AL.LeadId
           ORDER BY ALE.ModDate DESC
           ) AS Email
          ,(
           SELECT   TOP 1 ALP.IsForeignPhone
           FROM     adLeadPhone AS ALP
           WHERE    ALP.IsBest = 1
                    AND ALP.Position = 1
                    AND ALP.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Status = 'Active'
                    AND ALP.LeadId = AL.LeadId
           ORDER BY ALP.ModDate DESC
           ) AS IsForeignPhone
          ,(
           SELECT   TOP 1 ALP.Phone
           FROM     adLeadPhone AS ALP
           WHERE    ALP.IsBest = 1
                    AND ALP.Position = 1
                    AND ALP.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Status = 'Active'
                    AND ALP.LeadId = AL.LeadId
           ORDER BY ALP.ModDate DESC
           ) AS Phone_Best
          ,(
           SELECT   TOP 1 st.PhoneTypeDescrip
           FROM     adLeadPhone AS ALP
           INNER JOIN syPhoneType AS st ON st.PhoneTypeId = ALP.PhoneTypeId
           WHERE    ALP.IsBest = 1
                    AND ALP.Position = 1
                    AND ALP.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Status = 'Active'
                    AND ALP.LeadId = AL.LeadId
           ORDER BY ALP.ModDate DESC
           ) AS PhoneTypeId
          ,(
           SELECT   TOP 1 ALP.PhoneTypeId
           FROM     adLeadPhone AS ALP
           WHERE    ALP.IsBest = 1
                    AND ALP.Position = 1
                    AND ALP.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Status = 'Active'
                    AND ALP.LeadId = AL.LeadId
           ORDER BY ALP.ModDate DESC
           ) AS PhoneType
          ,(
           SELECT   TOP 1 ALA.AddressTypeId
           FROM     adLeadAddresses AS ALA
           WHERE    ALA.IsShowOnLeadPage = 1
                    AND ALA.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Status = 'Active'
                    AND ALA.LeadId = AL.LeadId
           ORDER BY ALA.ModDate DESC
           ) AS AddressTypeId
          ,(
           SELECT   TOP 1 ALA.Address1
           FROM     adLeadAddresses AS ALA
           WHERE    ALA.IsShowOnLeadPage = 1
                    AND ALA.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Status = 'Active'
                    AND ALA.LeadId = AL.LeadId
           ORDER BY ALA.ModDate DESC
           ) AS Address1
          ,(
           SELECT   TOP 1 ALA.Address2
           FROM     adLeadAddresses AS ALA
           WHERE    ALA.IsShowOnLeadPage = 1
                    AND ALA.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Status = 'Active'
                    AND ALA.LeadId = AL.LeadId
           ORDER BY ALA.ModDate DESC
           ) AS Address2
          ,(
           SELECT   TOP 1 ALA.City
           FROM     adLeadAddresses AS ALA
           WHERE    ALA.IsShowOnLeadPage = 1
                    AND ALA.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Status = 'Active'
                    AND ALA.LeadId = AL.LeadId
           ORDER BY ALA.ModDate DESC
           ) AS City
          ,(
           SELECT   TOP 1 ALA.StateId
           FROM     adLeadAddresses AS ALA
           WHERE    ALA.IsShowOnLeadPage = 1
                    AND ALA.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Status = 'Active'
                    AND ALA.LeadId = AL.LeadId
           ORDER BY ALA.ModDate DESC
           ) AS StateId
          ,(
           SELECT   TOP 1 ALA.ZipCode
           FROM     adLeadAddresses AS ALA
           WHERE    ALA.IsShowOnLeadPage = 1
                    AND ALA.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Status = 'Active'
                    AND ALA.LeadId = AL.LeadId
           ORDER BY ALA.ModDate DESC
           ) AS ZipCode
          ,(
           SELECT   TOP 1 ALA.CountryId
           FROM     adLeadAddresses AS ALA
           WHERE    ALA.IsShowOnLeadPage = 1
                    AND ALA.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Status = 'Active'
                    AND ALA.LeadId = AL.LeadId
           ORDER BY ALA.ModDate DESC
           ) AS CountryId
          ,(
           SELECT   TOP 1 ALA.State
           FROM     adLeadAddresses AS ALA
           WHERE    ALA.IsShowOnLeadPage = 1
                    AND ALA.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Status = 'Active'
                    AND ALA.LeadId = AL.LeadId
           ORDER BY ALA.ModDate DESC
           ) AS State
          ,(
           SELECT   TOP 1 ALA.IsInternational
           FROM     adLeadAddresses AS ALA
           WHERE    ALA.IsShowOnLeadPage = 1
                    AND ALA.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Status = 'Active'
                    AND ALA.LeadId = AL.LeadId
           ORDER BY ALA.ModDate DESC
           ) AS IsInternational
          ,(
           SELECT   TOP 1 ALA.CountyId
           FROM     adLeadAddresses AS ALA
           WHERE    ALA.IsShowOnLeadPage = 1
                    AND ALA.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Status = 'Active'
                    AND ALA.LeadId = AL.LeadId
           ORDER BY ALA.ModDate DESC
           ) AS CountyId
    FROM   adLeads AS AL;
--=================================================================================================
-- END  --  CREATE VIEW adLeadsView
--=================================================================================================
GO
