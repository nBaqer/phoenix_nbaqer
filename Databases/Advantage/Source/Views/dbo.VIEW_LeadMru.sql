SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
 
CREATE VIEW [dbo].[VIEW_LeadMru]
AS
    SELECT  MRUId
           ,ChildId AS LeadId
           ,Counter
           ,MRUTypeId
           ,UserId
           ,CampusId
           ,ModUser
           ,ModDate
    FROM    dbo.syMRUS
    WHERE   ( MRUTypeId = 4 ); 
 
 
GO
