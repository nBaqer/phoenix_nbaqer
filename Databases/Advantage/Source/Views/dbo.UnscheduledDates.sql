SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[UnscheduledDates]
AS
    SELECT  dbo.arUnschedClosures.StartDate
           ,dbo.arUnschedClosures.EndDate
           ,dbo.arUnschedClosures.UnschedClosureId
    FROM    dbo.arUnschedClosures
    INNER JOIN dbo.arClassSections ON dbo.arUnschedClosures.ClsSectionId = dbo.arClassSections.ClsSectionId
    WHERE   ( dbo.arClassSections.TermId = '1693EA60-1114-4F1C-BE15-BA9AF5DE9DEF' );


GO
