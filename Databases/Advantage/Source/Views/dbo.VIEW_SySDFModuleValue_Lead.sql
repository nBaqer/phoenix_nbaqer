SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
CREATE VIEW [dbo].[VIEW_SySDFModuleValue_Lead]
AS
    SELECT  mod.SDFPKID
           ,mod.PgPKID AS LeadId
           ,mod.SDFID
           ,mod.SDFValue
           ,mod.ModUser
           ,mod.ModDate
    FROM    dbo.sySDFModuleValue mod
    JOIN    adLeads lead ON lead.LeadId = mod.PgPKID; 
 
 
 
--SELECT  mod.SDFPKID, mod.PgPKID AS LeadId, mod.SDFID, mod.SDFValue, mod.ModUser, mod.ModDate 
--FROM    dbo.sySDFModuleValue mod 
--JOIN  adLeads lead ON lead.LeadId = mod.PgPKID 
 
GO
