SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
 
CREATE VIEW [dbo].[VIEW_AdmissionRepByCampus]
AS
    SELECT  ROW_NUMBER() OVER ( ORDER BY us.FullName ) AS ID
           ,us.UserId
           ,us.FullName
           ,c.CampusId
    FROM    syUsers us
    JOIN    dbo.syUsersRolesCampGrps rcg ON rcg.UserId = us.UserId
    JOIN    syRoles roles ON roles.RoleId = rcg.RoleId
    JOIN    dbo.syCampGrps cg ON cg.CampGrpId = rcg.CampGrpId
    JOIN    dbo.syCmpGrpCmps cgc ON cgc.CampGrpId = cg.CampGrpId
    JOIN    dbo.syCampuses c ON c.CampusId = cgc.CampusId
    WHERE   roles.SysRoleId = 3
            AND us.FullName <> 'Support'
            AND us.AccountActive = 1
    GROUP BY us.UserId
           ,us.FullName
           ,c.CampusId; 
 
 
 
 
GO
