SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[GradeView]
AS
    SELECT  dbo.arGradeScales.GrdScaleId
           ,dbo.arGradeScales.InstructorId
           ,dbo.arGradeScales.Descrip
           ,dbo.arGradeScaleDetails.MinVal
           ,dbo.arGradeScaleDetails.MaxVal
           ,dbo.arGradeScaleDetails.GrdSysDetailId
           ,dbo.arGradeSystemDetails.Grade
    FROM    dbo.arGradeScaleDetails
    INNER JOIN dbo.arGradeScales ON dbo.arGradeScaleDetails.GrdScaleId = dbo.arGradeScales.GrdScaleId
    INNER JOIN dbo.arGradeSystemDetails ON dbo.arGradeScaleDetails.GrdSysDetailId = dbo.arGradeSystemDetails.GrdSysDetailId;


GO
