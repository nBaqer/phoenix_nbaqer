SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
 
CREATE VIEW [dbo].[View_Messages]
AS
    SELECT  msg.MessageId
           ,msg.TemplateId
           ,msg.DeliveryType
           ,msg.DateDelivered
           ,msg.FromId
           ,msg.RecipientId
    FROM    dbo.msgMessages AS msg
    INNER JOIN dbo.adLeads AS l ON l.LeadId = msg.RecipientId; 
 
 
GO
