SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[syStudentContactAddresses]
AS
    SELECT  ad.OtherContactsAddresesId AS StudentContactAddressId
           ,ad.StatusId
           ,ad.OtherContactId AS StudentContactId
           ,ad.AddressTypeId AS AddrTypeId
           ,ad.Address1
           ,ad.Address2
           ,ad.City
           ,ad.StateId
           ,ad.CountryId
           ,CASE WHEN ad.IsInternational = 1 THEN ''
                 ELSE ad.ZipCode
            END AS Zip
           ,CASE WHEN ad.IsInternational = 1 THEN ad.ZipCode
                 ELSE ''
            END AS ForeignZip
           ,ad.State AS OtherState
           ,ad.ModUser
           ,ad.ModDate
    FROM    dbo.adLeadOtherContactsAddreses ad
    JOIN    dbo.adLeads AS AL ON AL.LeadId = ad.LeadId
    WHERE   AL.StudentId <> '00000000-0000-0000-0000-000000000000';
GO
