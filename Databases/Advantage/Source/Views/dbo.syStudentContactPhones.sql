SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[syStudentContactPhones]
AS
    SELECT  ALOCP.OtherContactsPhoneId AS StudentContactPhoneId
           ,ALOCP.StatusId
           ,ALOCP.OtherContactId AS StudentContactId
           ,ALOCP.PhoneTypeId
           ,CASE WHEN ALOCP.IsForeignPhone = 1 THEN ''
                 ELSE ALOCP.Phone
            END AS Phone
           ,NULL AS BestTime
           ,ALOCP.Extension AS Ext
           ,CASE WHEN ALOCP.IsForeignPhone = 1 THEN ALOCP.Phone
                 ELSE ''
            END AS ForeignPhone
           ,ALOCP.ModUser
           ,ALOCP.ModDate
    FROM    dbo.adLeadOtherContactsPhone AS ALOCP
    JOIN    dbo.adLeads AS AL ON AL.LeadId = ALOCP.LeadId
    WHERE   AL.StudentId <> '00000000-0000-0000-0000-000000000000';
GO
