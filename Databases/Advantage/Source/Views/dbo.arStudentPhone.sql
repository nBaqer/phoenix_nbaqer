SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[arStudentPhone]
AS
    SELECT LeadPhoneId AS StudentPhoneId
          ,AL.StudentId
          ,ALP.PhoneTypeId AS PhoneTypeId
          ,ALP.Phone
          ,NULL AS BestTime
          ,ALP.StatusId
          ,ALP.Extension AS Ext
          ,ALP.ModDate
          ,ALP.ModUser
          ,CASE WHEN ALP.Position = 1 THEN 1
                ELSE 0
           END AS default1
          ,ALP.IsForeignPhone AS ForeignPhone
          ,ALP.Position
          ,ALP.Extension
          ,ALP.IsForeignPhone
          ,ALP.IsBest
          ,ALP.IsShowOnLeadPage
    FROM   adLeadPhone AS ALP
    JOIN   adLeads AS AL ON AL.LeadId = ALP.LeadId
    WHERE  AL.StudentId <> '00000000-0000-0000-0000-000000000000';

GO
