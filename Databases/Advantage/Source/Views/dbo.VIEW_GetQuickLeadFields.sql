SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--================================================================================================= 
-- CREATE VIEW dbo.VIEW_GetQuickLeadFields 
--================================================================================================= 
CREATE VIEW [dbo].[VIEW_GetQuickLeadFields]
AS
    SELECT DISTINCT t6.ResDefId
          ,t7.TblFldsId
          ,t3.FldId
          ,CASE WHEN t3.FldId IN (
                                 SELECT FldId
                                 FROM   dbo.syFields
                                 WHERE  FldName IN ( 'Comments', 'Sponsor', 'AdmissionsRep', 'AssignedDate', 'PreviousEducation', 'admincriteriaid'
                                                    ,'DateApplied', 'HighSchool', 'AttendingHs', 'HighSchoolGradDate'
                                                   )
                                 ) THEN 5562
                ELSE t1.SectionId
           END AS SectionId
          ,t3.FldName
          ,CASE WHEN t3.FldId = (
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'LeadStatus'
                                ) THEN 1
                ELSE t6.Required
           END AS Required
          ,t3.DDLId
          ,t3.FldLen
          ,CASE WHEN t3.FldName = 'AttendingHs' THEN 'Uniqueidentifier'
                ELSE t8.FldType
           END AS FldType
          ,CASE WHEN t4.Caption = 'Created Date' THEN 'Date/Time'
                WHEN t4.Caption = 'Source Category' THEN 'Category'
                WHEN t4.Caption = 'Source Type' THEN 'Type'
                ELSE t4.Caption
           END AS Caption
          ,t6.ControlName
          ,CASE WHEN t3.FldName = 'AttendingHs' THEN 72
                ELSE t8.FldTypeId
           END AS FldTypeId
          ,t9.ctrlIdName
          ,t9.propName
          ,t9.parentCtrlId
          ,t9.sequence
    FROM   adQuickLeadSections t1
    JOIN   adExpQuickLeadSections t2 ON t1.SectionId = t2.SectionId
    JOIN   syFields t3 ON t2.FldId = t3.FldId
    JOIN   syFldCaptions t4 ON t3.FldId = t4.FldId
    --JOIN    adLeadFields t5 ON t3.FldId = t5.LeadID
    --                           AND t5.LeadField = t1.SectionId
    JOIN   syTblFlds t7 ON t3.FldId = t7.FldId
    JOIN   syResTblFlds t6 ON t6.TblFldsId = t7.TblFldsId
    JOIN   syFieldTypes t8 ON t8.FldTypeId = t3.FldTypeId
    LEFT OUTER JOIN adQuickLeadMap t9 ON t9.fldName = t3.FldName
    WHERE  t7.TblId IN (
                       SELECT TblId
                       FROM   dbo.syTables
                       WHERE  TblName IN ( 'syCampuses', 'AdLeads', 'adLeadPhone', 'syEmailType', 'AllNotes', 'adLeadAddresses', 'adleadPhone' )
                       )
           AND ResourceId = 170 --206 
           AND t7.TblFldsId NOT IN (
                                   SELECT TblFldsId
                                   FROM   syTblFlds
                                   WHERE  TblId = 183
                                          AND FldId IN (
                                                       SELECT FldId
                                                       FROM   dbo.syFields
                                                       WHERE  FldName IN ( 'Comments', 'ShiftId', 'Nationality', 'PhoneStatus', 'WorkEmail', 'ForeignPhone'
                                                                          ,'Address2', 'AddressStatus', 'DegCertSeekingId'
                                                                         )
                                                       )
                                   );

--================================================================================================= 
-- END  -- CREATE VIEW dbo.VIEW_GetQuickLeadFields 
--================================================================================================= 

GO
