SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[plStudentEmployment]
AS
    SELECT  ALE.StEmploymentId
           ,AL.StudentId
           ,ALE.JobTitleId
           ,ALE.JobStatusId
           ,ALE.StartDate
           ,ALE.EndDate
           ,ALE.Comments
           ,ALE.ModUser
           ,ALE.ModDate
           ,ALE.JobResponsibilities
           ,ALE.EmployerName
           ,ALE.EmployerJobTitle
    FROM    adLeadEmployment AS ALE
    INNER JOIN adLeads AS AL ON AL.LeadId = ALE.LeadId
    WHERE   AL.StudentId <> '00000000-0000-0000-0000-000000000000';
GO
