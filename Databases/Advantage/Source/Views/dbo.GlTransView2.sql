SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[GlTransView2]
AS
    SELECT
 DISTINCT   TransTypeId
           ,TransCodeId
           ,TypeId
           ,TransAmount
           ,TransCodeGLAccountId
           ,Percentage
           ,TransactionId
           ,Date
           ,TransCodeDescrip
           ,TransCodeDescripSummary
           ,GLAccount
           ,CampusId
           ,Amount
           ,(
              SELECT    CASE WHEN TransTypeId = 0
                                  AND Amount < 0 THEN Amount * ( -1 )
                             WHEN TransTypeId = 2
                                  AND (
                                        TypeId = 1
                                        OR TypeId = 3
                                      ) THEN Amount
                             WHEN TransTypeId = 1
                                  AND (
                                        TypeId = 1
                                        OR TypeId = 3
                                      )
                                  AND Amount < 0 THEN Amount * ( -1 )
                             WHEN TransTypeId = 1
                                  AND (
                                        TypeId = 1
                                        OR TypeId = 3
                                      )
                                  AND Amount > 0 THEN Amount
                        END
            ) AS CreditAmount
           ,(
              SELECT    CASE WHEN TransTypeId = 0
                                  AND Amount > 0 THEN Amount
                             WHEN TransTypeId = 2
                                  AND (
                                        TypeId = 0
                                        OR TypeId = 2
                                      ) THEN Amount * ( -1 )
                             WHEN TransTypeId = 1
                                  AND (
                                        TypeId = 0
                                        OR TypeId = 2
                                      )
                                  AND Amount < 0 THEN Amount * ( -1 )
                             WHEN TransTypeId = 1
                                  AND (
                                        TypeId = 0
                                        OR TypeId = 2
                                      )
                                  AND Amount > 0 THEN Amount
                        END
            ) AS DebitAmount
           ,TransCode
    FROM    GlTransView1;
 


GO
