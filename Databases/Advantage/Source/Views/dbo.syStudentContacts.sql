SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[syStudentContacts]
AS
    SELECT  ALOC.OtherContactId AS StudentContactId
           ,ALOC.StatusId
           ,AL.StudentId
           ,ALOC.FirstName
           ,ALOC.MiddleName AS MI
           ,ALOC.LastName
           ,ALOC.PrefixId
           ,ALOC.SufixId AS suffixId
           ,ALOC.Comments
           ,cmf.EMail
           ,ALOC.ModUser
           ,ALOC.ModDate
           ,ALOC.RelationshipId AS RelationId
    FROM    dbo.adLeadOtherContacts AS ALOC
    JOIN    dbo.adLeads AL ON AL.LeadId = ALOC.LeadId
    LEFT JOIN (
                SELECT TOP 1
                        ALOCE.EMail
                       ,ALOCE.OtherContactId
                FROM    dbo.adLeadOtherContactsEmail AS ALOCE
                JOIN    dbo.adLeadOtherContacts AS ALOC2 ON ALOC2.OtherContactId = ALOCE.OtherContactId
              ) cmf ON cmf.OtherContactId = ALOC.OtherContactId
    WHERE   AL.StudentId <> '00000000-0000-0000-0000-000000000000';
GO
