SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- ========================================================================================================= 
-- Create View StudentEnrollmentSearch 
-- ========================================================================================================= 
CREATE VIEW [dbo].[StudentEnrollmentSearch]
AS
    SELECT  E.StuEnrollId
           ,E.EnrollmentId
           ,E.StudentId
           ,CASE WHEN S.MiddleName IS NULL
                      OR S.MiddleName = ''
                 THEN S.LastName + ', ' + S.FirstName + ' - [' + ISNULL(SSN,'No SSN') + '] - [' + PrgVerDescrip + '] - [' + StatusCodeDescrip + ']'
                 ELSE S.LastName + ', ' + S.FirstName + ' ' + S.MiddleName + ' - [' + ISNULL(SSN,'No SSN') + '] - [' + PrgVerDescrip + '] - ['
                      + StatusCodeDescrip + ']'
            END AS SearchDisplay
           ,CASE WHEN S.MiddleName IS NULL
                      OR S.MiddleName = '' THEN S.LastName + ', ' + S.FirstName
                 ELSE S.LastName + ', ' + S.FirstName + ' ' + S.MiddleName
            END AS FullName
           ,ISNULL(S.FirstName,'') AS FirstName
           ,ISNULL(S.LastName,'') AS LastName
           ,S.SSN
           ,S.StudentNumber
           ,E.PrgVerId
           ,PV.PrgVerDescrip
           ,E.ShiftId
           ,SH.ShiftDescrip
           ,E.StatusCodeId
           ,SC.StatusCodeDescrip
           ,SC.SysStatusId
           ,CGC.CampGrpId
           ,CGC.CampusId
           ,(
              SELECT TOP ( 1 )
                        LLG.LeadGrpId
              FROM      dbo.adLeadByLeadGroups AS LLG
              INNER JOIN dbo.adLeadGroups AS LG ON LLG.LeadGrpId = LG.LeadGrpId
              WHERE     ( E.StuEnrollId = LLG.StuEnrollId )
            ) AS LeadGrpId
           ,ISNULL(TH.IsTransHold,0) AS IsTransHold
    FROM    dbo.arStuEnrollments AS E
    INNER JOIN dbo.arStudent AS S ON E.StudentId = S.StudentId
    LEFT OUTER JOIN dbo.arPrgVersions AS PV ON E.PrgVerId = PV.PrgVerId
    LEFT OUTER JOIN dbo.arShifts AS SH ON E.ShiftId = SH.ShiftId
    INNER JOIN dbo.syStatusCodes AS SC ON E.StatusCodeId = SC.StatusCodeId
    INNER JOIN dbo.syCmpGrpCmps AS CGC ON E.CampusId = CGC.CampusId
                                          AND CGC.CampGrpId = PV.CampGrpId
    LEFT OUTER JOIN (
                      -- list of Enrollments have atleast one IsTransHold = 1 
                                SELECT DISTINCT
                                        ASGS.StuEnrollId
                                       ,ASG.IsTransHold
                                FROM    adStuGrpStudents AS ASGS
                                INNER JOIN adStudentGroups AS ASG ON ASG.StuGrpId = ASGS.StuGrpId
                                WHERE   ASGS.IsDeleted = 0
                                        AND ASG.IsTransHold = 1
                    ) AS TH ON E.StuEnrollId = TH.StuEnrollId; 
-- ========================================================================================================= 
-- Create View StudentEnrollmentSearch 
-- ========================================================================================================= 
 
 
GO
EXEC sp_addextendedproperty N'MS_DiagramPane1', N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "E"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 121
               Right = 219
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "S"
            Begin Extent = 
               Top = 6
               Left = 257
               Bottom = 121
               Right = 432
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "PV"
            Begin Extent = 
               Top = 126
               Left = 38
               Bottom = 241
               Right = 228
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SH"
            Begin Extent = 
               Top = 126
               Left = 266
               Bottom = 241
               Right = 418
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "SC"
            Begin Extent = 
               Top = 246
               Left = 38
               Bottom = 361
               Right = 217
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "CGC"
            Begin Extent = 
               Top = 246
               Left = 255
               Bottom = 361
               Right = 407
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 20
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Widt', 'SCHEMA', N'dbo', 'VIEW', N'StudentEnrollmentSearch', NULL, NULL
GO
EXEC sp_addextendedproperty N'MS_DiagramPane2', N'h = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', 'SCHEMA', N'dbo', 'VIEW', N'StudentEnrollmentSearch', NULL, NULL
GO
DECLARE @xp int
SELECT @xp=2
EXEC sp_addextendedproperty N'MS_DiagramPaneCount', @xp, 'SCHEMA', N'dbo', 'VIEW', N'StudentEnrollmentSearch', NULL, NULL
GO
