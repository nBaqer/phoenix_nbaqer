SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[plStudentEducation]
AS
    SELECT  ALE.LeadEducationId AS StuEducationId
           ,AL.StudentId
           ,ALE.EducationInstId
           ,ALE.EducationInstType
           ,ALE.GraduatedDate
           ,ALE.FinalGrade
           ,ALE.CertificateId
		   ,ALE.Comments
           ,ALE.ModUser
           ,ALE.ModDate
           ,ALE.Major
    FROM    adLeadEducation AS ALE
    INNER JOIN adLeads AS AL ON AL.LeadId = ALE.LeadId
    WHERE   AL.StudentId <> '00000000-0000-0000-0000-000000000000';
GO
