;
GO
;
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[VIEW_GetHistory]
AS
    --
--Status History
    SELECT  hd.AuditHistId HistoryId
           ,'AD' ModuleCode
           ,LeadId
           ,h.EventDate Date
           ,m.Resource Module
           ,'Status' Type
           ,'From: ' + scO.StatusCodeDescrip + '  To: ' + scN.StatusCodeDescrip Description
           ,ISNULL(h.UserName,'Unknown') ModUser
           ,NULL AdditionalContent
    FROM    adLeads l
    JOIN    syAuditHistDetail hd ON l.LeadId = hd.RowId
                                    AND hd.ColumnName = 'LeadStatus'
    JOIN    syAuditHist h ON hd.AuditHistId = h.AuditHistId
                             AND TableName = 'adLeads'
    INNER JOIN syStatusCodes AS scN ON hd.NewValue = scN.StatusCodeId
    INNER JOIN sySysStatus AS ssN ON scN.SysStatusId = ssN.SysStatusId
    INNER JOIN syStatusCodes AS scO ON hd.OldValue = scO.StatusCodeId
    INNER JOIN syAdvantageResourceRelations rr ON ssN.StatusLevelId = rr.ResRelId
    INNER JOIN syResources m ON rr.RelatedResourceId = m.ResourceID
    WHERE   m.ResourceID = 189 
									--See "Entity, Module, Entity-Module tables"  comment below
--
--Interview, Event, Tour History
    UNION ALL
    SELECT  rr.LeadReqId HistoryId
           ,m.ModuleCode ModuleCode
           ,l.LeadId
           ,rr.ModDate Date
           ,m.ModuleName Module
           ,rt.Descrip Type
           ,r.Descrip Description
           ,u.FullName ModUser
           ,NULL AdditionalContent
    FROM    adLeads l
    INNER JOIN AdLeadReqsReceived rr ON l.LeadId = rr.LeadId
    INNER JOIN adReqs r ON rr.DocumentId = r.adReqId
    INNER JOIN adReqTypes rt ON r.adReqTypeId = rt.adReqTypeId
                                AND rt.adReqTypeId BETWEEN 4 AND 6 -- Interview, Event, Tour 
    INNER JOIN syModules m ON r.ModuleId = m.ModuleID
                              AND m.ModuleID = 2
    LEFT JOIN syUsers u ON rr.ModUser = u.UserName
--
--Fee History
    UNION ALL
    SELECT  t.TransactionId HistoryId
           ,m.ModuleCode ModuleCode
           ,l.LeadId
           ,t.ModDate Date
           ,m.ModuleName Module
           ,rt.Descrip Type
           ,r.Descrip + CASE WHEN Voided = 1 THEN ' (Voided)'
                             ELSE ''
                        END Description
           ,u.FullName ModUser
           ,NULL AdditionalContent
    FROM    adLeads l
    INNER  JOIN adLeadTransactions t ON t.LeadId = l.LeadId
    INNER JOIN adReqs r ON t.LeadRequirementId = r.adReqId
    INNER JOIN adReqTypes rt ON r.adReqTypeId = rt.adReqTypeId
                                AND rt.adReqTypeId = 7 --  Fee
    INNER JOIN saTransTypes ty ON t.TransTypeId = ty.TransTypeId
                                  AND ty.Description = 'Payment'
    INNER JOIN syModules m ON r.ModuleId = m.ModuleID
                              AND m.ModuleID = 2
    LEFT JOIN syUsers u ON t.ModUser = u.UserName
--
-- Task History
    UNION ALL
    SELECT  ut.UserTaskId HistoryId
           ,'AD' ModuleCode
           ,l.LeadId
           ,ut.ModDate Date
           ,m.Resource Module
           ,'Task/Appointment' Type
           ,t.Descrip Description
           ,u.FullName ModUser
           ,NULL AdditionalContent
    FROM    adLeads l
    INNER JOIN tmUserTasks ut ON l.LeadId = ut.ReId
    INNER JOIN tmTasks t ON ut.TaskId = t.TaskId
    INNER JOIN syAdvantageResourceRelations rr ON t.ModuleEntityId = rr.ResRelId
    INNER JOIN syResources m ON rr.RelatedResourceId = m.ResourceID
    LEFT JOIN syUsers u ON ut.ModUser = u.UserId
    WHERE   m.ResourceID = 189  
									--See "Entity, Module, Entity-Module tables"  comment below
--
-- Note History
    UNION ALL
    SELECT  n.UserId HistoryId
           ,m.ModuleCode ModuleCode
           ,l.LeadId
           ,n.ModDate Date
           ,m.ModuleName Module
           ,'Note' Type
           ,n.NoteText Description
           ,u.FullName ModUser
           ,NULL AdditionalContent
    FROM    adLeads l
    JOIN    adLead_Notes ln ON l.LeadId = ln.LeadId
    JOIN    AllNotes n ON ln.NotesId = n.NotesId
                          AND NoteType != 'Confidential'
    INNER JOIN syModules m ON n.ModuleCode = m.ModuleCode
    LEFT JOIN syUsers u ON n.ModUser = u.UserName
--
--Email History
    UNION ALL
    SELECT  ms.MessageId HistoryId
           ,'AD' ModuleCode
           ,l.LeadId
           ,ms.ModDate Date
           ,m.Resource Module
           ,'Email' Type
           ,mt.Descrip Description
           ,u.FullName ModUser
           ,ms.MsgContent AdditionalContent
    FROM    adLeads l
    INNER JOIN msgMessages ms ON l.LeadId = ms.RecipientId
    INNER JOIN msgTemplates mt ON ms.TemplateId = mt.TemplateId
    INNER JOIN syAdvantageResourceRelations rr ON mt.ModuleEntityId = rr.ResRelId
    INNER JOIN syResources m ON rr.RelatedResourceId = m.ResourceID
    LEFT JOIN syUsers u ON ms.ModUser = u.UserId
    WHERE   m.ResourceID = 189
            AND DeliveryType = 'Email';

-------Entity, Module, Entity-Module tables comment

		--There are 2 Groups of Entity, Module, Entity-Module tables.
		-- Many tables (Task maybe Audit) are associated with Modules via the Modules represented in syResources where ResourceTypeID = 1
		-- This is seperate and not fully consistant and not joinable(/easily mappable)  with the Modules  represented in syModules
		-- In the "2 groups of Entity, Module, Entity-Module tables" comment (in US8105) are the tables that need to be bridged/mapped 
		-- however for the current assignment (US8105) it suffices to address just the Admissions module

GO
