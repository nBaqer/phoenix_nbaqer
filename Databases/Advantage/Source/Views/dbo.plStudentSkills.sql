SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[plStudentSkills]
AS
    SELECT  PS.SkillId AS StudentSkillId
           ,PS.SkillId AS SkillId
           ,PSG.SkillGrpId AS SkillGrpId
           ,AL.StudentId AS StudentId
           ,ASK.ModDate AS ModDate
           ,ASK.ModUser AS ModUser
    FROM    adSkills AS ASK
    INNER JOIN adLeads AS AL ON AL.LeadId = ASK.LeadId
    INNER JOIN plSkillGroups AS PSG ON PSG.SkillGrpId = ASK.SkillGrpId
    INNER JOIN plSkills AS PS ON PS.SkillGrpId = PSG.SkillGrpId
    WHERE   AL.StudentId <> '00000000-0000-0000-0000-000000000000';
GO
