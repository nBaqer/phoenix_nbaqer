SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[VIEW1]
AS
    SELECT  PSG.SkillGrpId
           ,PS.SkillDescrip
           ,PS.SkillId AS StudentSkillId
    FROM    plSkillGroups AS PSG
    INNER JOIN plSkills AS PS ON PS.SkillGrpId = PSG.SkillGrpId
    INNER JOIN adSkills AS ASK ON ASK.SkillGrpId = PSG.SkillGrpId;
GO
