SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[VIEW2]
AS
    SELECT  dbo.syResTblFlds.ResDefId
           ,dbo.syTblFlds.TblFldsId
           ,dbo.syResTblFlds.ResourceId
           ,dbo.syFields.FldName
           ,dbo.syFields.FldId
           ,dbo.syDDLS.DDLName
           ,dbo.syTables.TblName
           ,dbo.syDDLS.ValFldId
           ,dbo.syDDLS.DispFldId
    FROM    dbo.syTables
    INNER JOIN dbo.syDDLS ON dbo.syTables.TblId = dbo.syDDLS.TblId
    INNER JOIN dbo.syResTblFlds
    INNER JOIN dbo.syTblFlds ON dbo.syResTblFlds.TblFldsId = dbo.syTblFlds.TblFldsId
    INNER JOIN dbo.syFields ON dbo.syTblFlds.FldId = dbo.syFields.FldId ON dbo.syDDLS.DDLId = dbo.syFields.DDLId
    WHERE   ( dbo.syResTblFlds.ResourceId = 88 );


GO
