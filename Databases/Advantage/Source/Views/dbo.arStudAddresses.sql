SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [dbo].[arStudAddresses]
AS
    SELECT ALA.adLeadAddressId AS StdAddressId
          ,AL.StudentId
          ,ALA.Address1
          ,ALA.Address2
          ,ALA.City
          ,ALA.StateId
          ,ALA.ZipCode AS Zip
          ,ALA.CountryId
          ,ALA.AddressTypeId
          ,ALA.ModUser
          ,ALA.ModDate
          ,ALA.StatusId
          ,ALA.IsMailingAddress AS default1
          ,ALA.IsInternational AS ForeignZip
          ,ALA.State AS OtherState
          ,ALA.IsMailingAddress
          ,ALA.IsShowOnLeadPage
          ,ALA.IsInternational
          ,ALA.CountyId
          ,ALA.ForeignCountyStr
          ,ALA.ForeignCountryStr
          ,ALA.AddressApto
    FROM   adLeadAddresses AS ALA
    JOIN   dbo.adLeads AS AL ON AL.LeadId = ALA.LeadId
    WHERE  AL.StudentId <> '00000000-0000-0000-0000-000000000000';


GO
