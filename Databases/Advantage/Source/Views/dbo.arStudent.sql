SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [dbo].[arStudent]
AS
    SELECT  AL.StudentId
           ,AL.FirstName
           ,AL.MiddleName
           ,AL.LastName
           ,AL.SSN
           ,AL.StudentStatusId AS StudentStatus
           ,(
              SELECT TOP 1
                        ALE.EMail
              FROM      AdLeadEmail AS ALE
              INNER JOIN syStatuses AS SS ON SS.StatusId = ALE.StatusId
              WHERE     ALE.LeadId = AL.LeadId
                        AND SS.Status = 'Active'
                        AND ALE.EMailTypeId = (
                                                SELECT  EMailTypeId
                                                FROM    dbo.syEmailType
                                                WHERE   EMailTypeCode = 'Work'
                                              )
              ORDER BY  ALE.IsPreferred DESC
                       ,ALE.IsShowOnLeadPage DESC
                       ,ALE.ModDate DESC
            ) AS WorkEmail
           ,(
              SELECT TOP 1
                        EMail
              FROM      AdLeadEmail AS ALE
              INNER JOIN syStatuses AS SS ON SS.StatusId = ALE.StatusId
              WHERE     ALE.LeadId = AL.LeadId
                        AND SS.Status = 'Active'
                        AND ALE.EMailTypeId = (
                                                SELECT  EMailTypeId
                                                FROM    dbo.syEmailType
                                                WHERE   EMailTypeCode = 'Home'
                                              )
              ORDER BY  ALE.IsPreferred DESC
                       ,ALE.IsShowOnLeadPage DESC
                       ,ALE.ModDate DESC
            ) AS HomeEmail
           ,AL.BirthDate AS DOB
           ,AL.ModUser
           ,AL.ModDate
           ,AL.Prefix
           ,AL.Suffix
           ,AL.Sponsor
           ,AL.AssignedDate
           ,AL.Gender
           ,AL.Race
           ,AL.MaritalStatus
           ,AL.FamilyIncome
           ,CONVERT(INTEGER,AL.Children) AS Children
           ,AL.ExpectedStart
           ,AL.ShiftID AS ShiftId
           ,AL.Nationality
           ,AL.Citizen
           ,AL.DrivLicStateID AS DrivLicStateId
           ,AL.DrivLicNumber
           ,AL.AlienNumber
           ,(
              SELECT TOP 1
                        SUBSTRING(AN.NoteText,1,300)
              FROM      AllNotes AS AN
              JOIN      adLead_Notes AS ALN ON ALN.NotesId = AN.NotesId
              WHERE     AN.ModuleCode = 'AR'
                        AND AN.PageFieldId = 2
                        AND ALN.LeadId = AL.LeadId
            ) AS Comments                                       -- Varchar(300)
           ,(
              SELECT TOP 1
                        ALA.CountyId
              FROM      dbo.adLeadAddresses AS ALA
              INNER JOIN syStatuses AS SS ON SS.StatusId = ALA.StatusId
              INNER JOIN plAddressTypes AS PAT ON PAT.AddressTypeId = ALA.AddressTypeId
              WHERE     ALA.LeadId = AL.LeadId
                        AND SS.Status = 'Active'
              ORDER BY  ALA.IsShowOnLeadPage DESC
                       ,PAT.AddressDescrip ASC
                       ,ALA.ModDate DESC
            ) AS County                                          -- foreign key
           ,AL.SourceDate AS SourceDate                     -- SourceDate
           ,AL.PreviousEducation AS EdLvlId
           ,AL.StudentNumber
           ,'' AS Objective
           ,AL.DependencyTypeId
           ,AL.DegCertSeekingId
           ,AL.GeographicTypeId
           ,AL.HousingId
           ,AL.admincriteriaid
           ,AL.AttendTypeId
           ,NULL AS ApportionedCreditBalanceAY1
           ,NULL AS ApportionedCreditBalanceAY2
		   ,AL.LeadId
    FROM    adLeads AS AL
    WHERE   StudentId <> '00000000-0000-0000-0000-000000000000';


GO
