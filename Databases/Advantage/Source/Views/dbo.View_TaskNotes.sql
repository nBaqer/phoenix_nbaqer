SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
 
CREATE VIEW [dbo].[View_TaskNotes]
AS
    SELECT  ut.UserTaskId
           ,ut.ReId AS LeadId
           ,ut.Message
           ,dbo.tmTasks.Descrip AS Description
           ,ut.ModDate
           ,ut.AssignedById
    FROM    dbo.tmUserTasks AS ut
    INNER JOIN dbo.tmTasks ON dbo.tmTasks.TaskId = ut.TaskId
    INNER JOIN dbo.adLeads AS lead ON lead.LeadId = ut.ReId; 
 
 
GO
