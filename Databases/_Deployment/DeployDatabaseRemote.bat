@ECHO OFF
IF NOT "%Debug%"=="ON" SET Debug=OFF
SETLOCAL
GOTO Start

:Usage
ECHO.
ECHO DeployDatabaseRemote:
ECHO   Executes DeployDatabase.bat on a remote server where RedGate compare 
ECHO   tools are available
ECHO.
ECHO Usage:
ECHO   DeployDatabaseRemote SourceFolder TargetServer TargetDatabase [SyncSchema] 
ECHO     [SyncData] [LogFile] [SqlCmpOptions] [SqlUser] [SqlPass]
ECHO     [SqlCompareExe] [SqlDataCmpExe] [RedGateServer]
ECHO.
ECHO Arguments:
ECHO   SourceFolder   Folder containing SQL Source Control scripted database files
ECHO                  Ex: C:\_tfs\eSolutions\DBSources\Development\Phase3\Master
ECHO   TargetServer   Name of the target SQL Server
ECHO                  Ex: Dev2
ECHO   TargetDatabase Name of database on target server
ECHO                  Ex: Phase3
ECHO   SyncSchema     Optional: Y/N
ECHO                    Y=Sync target schema
ECHO                    N=Just generate change scripts
ECHO                  Default: N
ECHO   SyncData       Optional: Y/N
ECHO                    Y=Sync target schema
ECHO                    N=Just generate change scripts
ECHO                  Default: N
ECHO   LogFile        Optional: Y/N
ECHO                    Y=Send output to log file
ECHO                    N=Send output to console
ECHO                  Default:N
ECHO   SqlCmpOptions  Optional: SQLCompare Options (see SQL Compare command
ECHO                  line help for /Options:)
ECHO                  Default: NoTransactions
ECHO   SqlUser        Optional: SQL user to connect to target database
ECHO                  Default: Current windows user
ECHO   SqlPass        Optional: SQL user password
ECHO                  Default: none
ECHO   SqlCompareExe  Optional: Fully qualified path of sqlcompare.exe
ECHO                  Default: default install path
ECHO   SqlDataCmpExe  Optional: Fully qualified path of sqldatacompare.exe
ECHO                  Default: default install path
ECHO   RedGateServer  Optional: Server where RedGate compare tools are located
ECHO                  Default: groza7.internal.fameinc.com 
ECHO.
ECHO Note:
ECHO   DeployDatabase will create SQL change scripts and optionally a log file
ECHO   in the same folder as the batch file. Hence, do not put this batch file
ECHO   in SourceFolder as the generated SQL change scripts will confuse the sync.
GOTO End

:Start
SET SourceFolder=%~1
SET TargetServer=%~2
SET TargetDatabase=%~3
SET SyncSchema=%~4
SET SyncData=%~5
SET LogFile=%~6
SET SqlCmpOptions=%~7
SET SqlUser=%~8
SET SqlPass=%~9

SET DropFolder=%~dp0
SET ErrFileName=%DropFolder%%~n0.error
SET LogFileName=%DropFolder%%~n0.log

SHIFT
SET SqlCompareExe=%~s9
SHIFT
SET SqlDataCmpExe=%~s9
SHIFT
SET RedGateServer=%~9

IF "%RedGateServer%"=="" SET RedGateServer=Groza7.internal.fameinc.com
IF NOT "%RedGateServer%"=="" IF /i "%RedGateServer%"=="%COMPUTERNAME%" SET RedGateServer=

rem If we are running from TFS Build and RedGate is on a remote server, TFS 
rem won't capture console output from PSExec, so we need to send console to a  
rem file and then TYPE it to the screen
IF NOT "%RedGateServer%"=="" IF NOT "%TF_BUILD%"=="" SET TfsRemote=Y

SET PSExec=psexec
IF NOT "%ProgramFiles(x86)%"=="" IF EXIST "%PubAppsFolder%psexec64.exe" SET PSExec=psexec64
SET PSExecSwitches=-u "Internal\TFSBuild" -p TFS.Build -h -accepteula -w C:\Windows\Temp cmd /c

SET DeployDatabaseBat=%DropFolder%DeployDatabaseCmd.bat
SET DeployDatabaseLog=%LogFile%
IF "%RedGateServer%"=="" (
	SET DeployDatabase="%DeployDatabaseBat%"
	SET RedGateServerDesc=local machine
) ELSE (
	SET DeployDatabase="%DropFolder%psexec" \\%RedGateServer% %PSExecSwitches% "%DeployDatabaseBat%" 2^>^&1
	IF "%TfsRemote%"=="Y" SET DeployDatabaseLog=N
	SET RedGateServerDesc=%RedGateServer%
)
SET DeployDatabaseCmd="%DropFolder%DeployDatabase.bat" "%SourceFolder%" "%TargetServer%" "%TargetDatabase%" "%SyncSchema%" "%SyncData%" "%DeployDatabaseLog%" "%SqlCmpOptions%" "%SqlUser%" "%SqlPass%"

IF /i NOT %Debug%==ON GOTO SkipDebug
ECHO.
ECHO SourceFolder    %SourceFolder%
ECHO TargetServer    %TargetServer%
ECHO TargetDatabase  %TargetDatabase%
ECHO SyncSchema      %SyncSchema%
ECHO SyncData        %SyncData%
ECHO LogFile         %LogFile%
ECHO SqlCmpOptions   %SqlCmpOptions%
ECHO SqlUser         %SqlUser%
ECHO SqlPass         %SqlPass%
ECHO SqlCompareExe   %SqlCompareExe%
ECHO SqlDataCmpExe   %SqlDataCmpExe%
ECHO RedGateServer   %RedGateServer%
ECHO.
ECHO LogFileName     %LogFileName%
ECHO.
ECHO DeployDatabaseCmd:
ECHO %DeployDatabaseCmd%
ECHO.
ECHO DeployDatabase:
ECHO %DeployDatabase%
ECHO.
PAUSE
:SkipDebug

IF EXIST "%ErrFileName%" DEL "%ErrFileName%" /f/q
SET RedirLog=
IF /i "%LogFile%"=="Y" SET RedirLog=^>^> "%LogFileName%" 2^>^&1
SET RedirCmd=
SET RedirCmdLog=%DropFolder%DeployDatabase.log
IF "%TfsRemote%"=="Y" SET RedirCmd=^> "%RedirCmdLog%"

ECHO Executing deployment batch file on %RedGateServer%... %RedirLog%
IF EXIST "%DeployDatabaseBat%" DEL /Q/F "%DeployDatabaseBat%"
IF EXIST "%DeployDatabaseBat%" (SET ExitCode=1 & GOTO Error)

ECHO @%DeployDatabaseCmd% ^%RedirCmd% > "%DeployDatabaseBat%"
IF NOT EXIST "%DeployDatabaseBat%" (SET ExitCode=1 & GOTO Error)
ECHO %DeployDatabase% %RedirLog%

SET MaxAttempts=6
SET AttemptWait=10
SET /a AttemptWaitMS=%AttemptWait% * 1000
SET Attempt=0

:DeployDatabase
SET /a Attempt+=1
SET ExitCode=0
ECHO. %RedirLog%
ECHO Attempt %Attempt% of %MaxAttempts% to launch batch file on %RedGateServerDesc%... %RedirLog%
IF "%TfsRemote%"=="Y" IF EXIST "%RedirCmdLog%" DEL /Q/F "%RedirCmdLog%"
CALL %DeployDatabase% %RedirLog%
SET ExitCode=%ERRORLEVEL%
IF "%TfsRemote%"=="Y" IF EXIST "%RedirCmdLog%" TYPE "%RedirCmdLog%"
IF "%ExitCode%"=="0" GOTO Success
IF %Attempt% LSS %MaxAttempts% IF NOT "%RedGateServer%"=="" (
	ECHO Launch failed with exitcode %ExitCode%.  Attempting to stop PSEXESVC on %RedGateServerDesc%... %RedirLog%
	CALL "%DropFolder%SvcCtl" PSEXESVC Stop %RedGateServer% 3 10 %RedirLog%
	GOTO DeployDatabase
)
IF NOT "%ExitCode%"=="0" GOTO Error

:Success
ECHO Deployment batch file completed successfully %RedirLog%
GOTO End

:Error
ECHO Terminating with ExitCode: %ExitCode% %RedirLog%
ECHO ExitCode: %ExitCode% > "%ErrFileName%"

:End
ECHO. %RedirLog%

IF "%ExitCode%"=="" SET ExitCode=0
EXIT /b %ExitCode%
ENDLOCAL
