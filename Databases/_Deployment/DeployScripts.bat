@ECHO OFF
IF NOT "%Debug%"=="ON" SET Debug=OFF
SETLOCAL
GOTO Start

:Usage
ECHO.
ECHO DeployScripts:
ECHO   Executes a folder of SQL scripts on the specified SQL Server and DB
ECHO.
ECHO Usage:
ECHO   DeployScripts TargetServer TargetDatabase [SourceFolder]
ECHO     [SourceFileSpec] [LogFile] [Timeout] [SqlCmdExe]
ECHO.
ECHO Arguments:
ECHO   TargetServer   Name of the target SQL Server
ECHO                  Ex: Dev2
ECHO   TargetDatabase Name of database on target server
ECHO                  Ex: Phase3
ECHO   SourceFolder   Optional: Folder containing SQL script files to execute on the
ECHO                  target database
ECHO                  Default: Sources folder under location of 
ECHO                  DeployScripts.bat if it exists, otherwise defaults to 
ECHO                  location of DeployScripts.bat
ECHO   SourceFileSpec Optional: File spec of source scripts to deploy
ECHO                  Default: *.sql
ECHO   LogFile        Optional: Y/N
ECHO                    Y=Send output to log file
ECHO                    N=Send output to console
ECHO                  Default: N
ECHO   Timeout        Optional: Timeout for sqlcmd.exe
ECHO                  Default: 300 seconds (5 minutes)
ECHO   SqlCmdExe      Optional: Fully qualified path of sqlcmd.exe
ECHO                  Default: default install path
ECHO.
ECHO Note:
ECHO   DeployScripts will create an sqlcmd output file and optionally a log file
ECHO   in the same folder as the batch file.
GOTO End

:Start
SET TargetServer=%~1
SET TargetDatabase=%~2
SET SourceFolder=%~3
SET SourceFileSpec=%~4
SET LogFile=%~5
SET Timeout=%~6
SET SqlCmdExe=%~s7

IF "%TargetServer%"=="" GOTO Usage
IF "%TargetServer%"=="?" GOTO Usage
IF "%TargetServer%"=="/?" GOTO Usage
IF "%TargetDatabase%"=="" GOTO Usage
IF "%SourceFolder%"=="" IF EXIST "%~dp0Sources" SET SourceFolder=%~dp0Sources
IF "%SourceFolder%"=="" SET SourceFolder=%~dp0
IF NOT "%SourceFolder:~-1%"=="\" SET SourceFolder=%SourceFolder%\
IF "%SourceFileSpec%"=="" SET SourceFileSpec=*.sql
IF "%LogFile%"=="" SET LogFile=N
IF "%Timeout%"=="" SET Timeout=300
IF "%SqlCmdExe%"==""       SET SqlCmdExe=C:\Program Files\Microsoft SQL Server\130\Tools\Binn\sqlcmd.exe
IF NOT EXIST "%SqlCmdExe%" SET SqlCmdExe=C:\Program Files (x86)\Microsoft SQL Server\130\Tools\Binn\sqlcmd.exe
IF NOT EXIST "%SqlCmdExe%" SET SqlCmdExe=C:\Program Files\Microsoft SQL Server\120\Tools\Binn\sqlcmd.exe
IF NOT EXIST "%SqlCmdExe%" SET SqlCmdExe=C:\Program Files (x86)\Microsoft SQL Server\120\Tools\Binn\sqlcmd.exe
IF NOT EXIST "%SqlCmdExe%" SET SqlCmdExe=C:\Program Files\Microsoft SQL Server\110\Tools\Binn\sqlcmd.exe
IF NOT EXIST "%SqlCmdExe%" SET SqlCmdExe=C:\Program Files (x86)\Microsoft SQL Server\110\Tools\Binn\sqlcmd.exe
IF NOT EXIST "%SqlCmdExe%" SET SqlCmdExe=C:\Program Files\Microsoft SQL Server\100\Tools\Binn\sqlcmd.exe
IF NOT EXIST "%SqlCmdExe%" SET SqlCmdExe=C:\Program Files (x86)\Microsoft SQL Server\100\Tools\Binn\sqlcmd.exe
IF NOT EXIST "%SqlCmdExe%" SET SqlCmdExe=C:\Program Files\Microsoft SQL Server\90\Tools\Binn\sqlcmd.exe
IF NOT EXIST "%SqlCmdExe%" SET SqlCmdExe=C:\Program Files (x86)\Microsoft SQL Server\90\Tools\Binn\sqlcmd.exe
IF NOT EXIST "%SqlCmdExe%" SET SqlCmdExe=C:\Program Files\Microsoft SQL Server\80\Tools\Binn\sqlcmd.exe
IF NOT EXIST "%SqlCmdExe%" SET SqlCmdExe=C:\Program Files (x86)\Microsoft SQL Server\80\Tools\Binn\sqlcmd.exe

IF NOT EXIST "%SqlCmdExe%" SET SqlCmdExe=C:\Program Files\Microsoft SQL Server\Client SDK\ODBC\130\Tools\Binn\sqlcmd.exe
IF NOT EXIST "%SqlCmdExe%" SET SqlCmdExe=C:\Program Files (x86)\Microsoft SQL Server\Client SDK\ODBC\130\Tools\Binn\sqlcmd.exe

SET OutputFolder=%~dp0
SET LogFileName=%OutputFolder%%~n0.log
SET ErrFileName=%OutputFolder%%~n0.error

REM Note: the input script file is added in the FOR command below via the -i switch
SET SqlCmdCmd="%SqlCmdExe%" -E -S %TargetServer% -d %TargetDatabase% -b -t %Timeout% -X -m-1 -r1

IF /i NOT %Debug%==ON GOTO SkipDebug
ECHO.
ECHO TargetServer    %TargetServer%
ECHO TargetDatabase  %TargetDatabase%
ECHO SourceFolder    %SourceFolder%
ECHO SourceFileSpec  %SourceFileSpec%
ECHO LogFile         %LogFile%
ECHO Timeout         %Timeout%
ECHO SqlCmdExe       %SqlCmdExe%
ECHO.
ECHO LogFileName     %LogFileName%
ECHO.
ECHO SqlCmdCmd:
ECHO %SqlCmdCmd% -i [ScriptFile]
ECHO.
PAUSE
:SkipDebug

SET RedirLog=
IF /i "%LogFile%"=="Y" SET RedirLog=^>^> "%LogFileName%"

SET Pass=0
SET MaxPass=2

:Loop
SET /a Pass+=1
SET ExitCode=

IF EXIST "%ErrFileName%" DEL "%ErrFileName%" /f/q

ECHO Deploying: %SourceFolder% %RedirLog%
ECHO To: %TargetServer%\%TargetDatabase% %RedirLog%
ECHO Pass %Pass% of %MaxPass% %RedirLog%
ECHO. %RedirLog%
ECHO Executing scripts... %RedirLog%
FOR %%f IN ("%SourceFolder%%SourceFileSpec%") DO (ECHO. %RedirLog% & ECHO %%~nxf %RedirLog% & %SqlCmdCmd% -i "%%f" %RedirLog% 2>&1)
SET ExitCode=%ERRORLEVEL%
IF "%ExitCode%"=="9009" SET ExitCode=0
ECHO. %RedirLog%
IF "%ExitCode%"=="0" GOTO End
ECHO Exit Code: %ExitCode% %RedirLog%
ECHO Exit Code: %ExitCode% > "%ErrFileName%"
GOTO End

:End
rem If Pass not initialized, we never got into the loop, just exit
IF NOT DEFINED Pass GOTO EndLoop
IF "%Pass%"=="" GOTO EndLoop
IF %Pass%==0 GOTO EndLoop

rem If ExitCode is 0, scripts were successful, skip remaining passes and exit
IF "%ExitCode%"=="0" GOTO EndLoop

rem If not at MaxPass yet, loop for another pass
IF %Pass% LSS %MaxPass% GOTO Loop
:EndLoop

rem If MaxPasses exceeded, return exit code 
EXIT /b %ExitCode%
ENDLOCAL
