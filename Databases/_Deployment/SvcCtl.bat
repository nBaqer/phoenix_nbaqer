@ECHO OFF
IF NOT "%Debug%"=="ON" SET Debug=OFF
SETLOCAL
GOTO Start

:Usage
ECHO.
ECHO SvcCtl:
ECHO   Starts or stops a service and verifies if operation was successful, 
ECHO   with optional retry on failure.
ECHO.
ECHO Usage:
ECHO   SvcCtl ServiceName Operation [ServerName] [MaxAttempts] [AttemptWait]
ECHO      [LogFile] [LogFileName]
ECHO.
ECHO Arguments:
ECHO   ServiceName   Service Name
ECHO   Operation     Start, Stop
ECHO   ServerName    Optional: Server name
ECHO                 Default: "" (local server)
ECHO   MaxAttempts   Optional: Number of times to attempt the operation
ECHO                 Default: 3
ECHO   AttemptWait   Optional: Number of seconds to wait between attempts
ECHO                 Default: 10
ECHO   LogFile       Optional: Y/N
ECHO                 Y=Send output to log file
ECHO                 N=Send output to console
ECHO                 Default: N
ECHO   LogFileName   Optional: Fully qualified path and name of the log file
ECHO                 Default: Same as this bach file, but with .log extension
ECHO.
ECHO ExitCode:       0=Success, otherwise failure
ECHO.
GOTO End

:Start
SET ServiceName=%~1
SET Operation=%~2
SET ServerName=%~3
SET MaxAttempts=%~4
SET AttemptWait=%~5
SET LogFile=%~6
SET LogFileName=%~7

SET ExitCode=1

IF "%ServiceName%"=="?" GOTO Usage
IF "%ServiceName%"=="/?" GOTO Usage

IF "%ServiceName%"=="" GOTO Usage
IF "%Operation%"=="" GOTO Usage

SET ExitCode=0

IF /i "%Operation%"=="STOP" SET Operation=STOP
IF /i "%Operation%"=="START" SET Operation=START
IF "%ServerName%"=="" GOTO SkipBS
IF NOT "%ServerName:~0,2%"=="\\" SET ServerName=\\%ServerName%
:SkipBS
IF "%MaxAttempts%"=="" SET MaxAttempts=3
IF "%AttemptWait%"=="" SET AttemptWait=10
SET /a AttemptWaitMS=%AttemptWait% * 1000
IF "%LogFile%"=="" SET LogFile=N
IF "%LogFileName%"=="" SET LogFileName=%~dp0%~n0.log
SET RedirLog=
IF /i "%LogFile%"=="Y" SET RedirLog=^>^> "%LogFileName%"

IF "%Operation%"=="STOP" (
	SET SuccessState=STOPPED
	SET PendingState=STOP_PENDING
)
IF "%Operation%"=="START" (
	SET SuccessState=RUNNING
	SET PendingState=START_PENDING
)

SET SvcCntrlCmd=SC %ServerName% %Operation% "%ServiceName%"

IF /i NOT %Debug%==ON GOTO SkipDebug
ECHO.
ECHO ServiceName    %ServiceName%
ECHO Operation      %Operation%
ECHO ServerName     %ServerName%
ECHO MaxAttempts    %MaxAttempts%
ECHO AttemptWait    %AttemptWait%
ECHO LogFile        %LogFile%
ECHO LogFileName    %LogFileName%
ECHO.
ECHO SuccessState   %SuccessState%
ECHO PendingState   %PendingState%
ECHO.
ECHO SvcCntrlCmd    %SvcCntrlCmd%
ECHO.
PAUSE
:SkipDebug

SET Attempt=0
:SvcCmd
SET /a Attempt+=1
SET ExitCode=0
ECHO. %RedirLog%
ECHO Attempt %Attempt% of %MaxAttempts% to %Operation% service... %RedirLog%

ECHO Checking current service state... %RedirLog%
IF "%Operation%"=="STOP" (
	SC %ServerName% query "%ServiceName%" | FIND "service does not exist" %RedirLog%
	IF errorlevel 0 IF NOT errorlevel 1 GOTO Success
)
SC %ServerName% query "%ServiceName%" | FIND "STATE" | FIND "%SuccessState%" %RedirLog% 
IF errorlevel 0 IF NOT errorlevel 1 GOTO Success

ECHO Requesting new service state... %RedirLog%
%SvcCntrlCmd% 2>&1 %RedirLog% 

ECHO Waiting for pending request to complete... %RedirLog%
:ChkState
SC %ServerName% query "%ServiceName%" | FIND "STATE" | FIND "%PendingState%" %RedirLog% 
IF errorlevel 0 IF NOT errorlevel 1 (
	PING 192.0.2.2 -n 2 -w 1000 > nul
	GOTO ChkState
)

ECHO Checking if service is in the desired state... %RedirLog%
SC %ServerName% query "%ServiceName%" | FIND "STATE" | FIND "%SuccessState%" 2>&1 %RedirLog% 
IF errorlevel 0 IF NOT errorlevel 1 GOTO Success
SET ExitCode=%ERRORLEVEL%
IF %Attempt% LSS %MaxAttempts% (
	ECHO Operation failed.  Retrying in %AttemptWait% seconds
	PING 192.0.2.2 -n 1 -w %AttemptWaitMS% > nul
	GOTO SvcCmd
)
IF NOT "%ExitCode%"=="0" GOTO Error

:Success
ECHO SvcCtl successful
GOTO End

:Error
IF "%ExitCode%"=="" SET ExitCode=1
IF "%ExitCode%"=="0" SET ExitCode=1
ECHO SvcCtl terminating with ExitCode %ExitCode% %RedirLog%
ECHO. %RedirLog%

:End
EXIT /b %ExitCode%
ENDLOCAL
