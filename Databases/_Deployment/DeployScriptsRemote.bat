@ECHO OFF
IF NOT "%Debug%"=="ON" SET Debug=OFF
SETLOCAL
GOTO Start

:Usage
ECHO.
ECHO DeployScriptsRemote:
ECHO   Executes a folder of SQL scripts on the specified SQL Server and DB
ECHO.
ECHO Usage:
ECHO   DeployScriptsRemote TargetServer TargetDatabase [SourceFolder]
ECHO     [SourceFileSpec] [LogFile] [Timeout] [SqlCmdExe]
ECHO.
ECHO Arguments:
ECHO   TargetServer   Name of the target SQL Server
ECHO                  Ex: Dev2
ECHO   TargetDatabase Name of database on target server
ECHO                  Ex: Phase3
ECHO   SourceFolder   Optional: Folder containing SQL script files to execute 
ECHO                  on the target database
ECHO                  Default: Sources folder under location of 
ECHO                  DeployScripts.bat if it exists, otherwise defaults to 
ECHO                  location of DeployScripts.bat
ECHO   SourceFileSpec Optional: File spec of source scripts to deploy
ECHO                  Default: *.sql
ECHO   LogFile        Optional: Y/N
ECHO                    Y=Send output to log file
ECHO                    N=Send output to console
ECHO                  Default: N
ECHO   Timeout        Optional: Timeout for sqlcmd.exe
ECHO                  Default: 300 seconds (5 minutes)
ECHO   SqlCmdExe      Optional: Fully qualified path of sqlcmd.exe
ECHO                  Default: default install path
ECHO.
ECHO Note:
ECHO   DeployScripts will create a sqlcmd output file for each script and 
ECHO   optionally a log file in the same folder as the batch file.
GOTO End

:Start
SET TargetServer=%~1
SET TargetDatabase=%~2
SET SourceFolder=%~3
SET SourceFileSpec=%~4
SET LogFile=%~5
SET Timeout=%~6
SET SqlCmdExe=%~s7

SET DropFolder=%~dp0
SET ErrFileName=%DropFolder%%~n0.error
SET LogFileName=%DropFolder%%~n0.log

SET PSExec=psexec
IF NOT "%ProgramFiles(x86)%"=="" IF EXIST "%DropFolder%psexec64.exe" SET PSExec=psexec64
SET PSExecSwitches=-u "Internal\TFSBuild" -p TFS.Build -h -accepteula -w C:\Windows\Temp cmd /c

REM Parse ServerName from TargetServer instance name ("SERVER\INSTANCE")
FOR /f "tokens=1 delims=\" %%a IN ("%TargetServer%") DO SET ServerName=%%a

rem If we are running from TFS Build and RedGate is on a remote server, TFS 
rem won't capture console output from PSExec, so we need to send console to a  
rem file and then TYPE it to the screen
IF NOT "%TargetServer%"=="" IF NOT "%TF_BUILD%"=="" SET TfsRemote=Y

SET DeployScriptsBat=%DropFolder%DeployScriptsCmd.bat
SET DeployScriptsLog=%LogFile%

IF "%TargetServer%"=="" (
	SET DeployScripts="%DeployScriptsBat%"
	SET TargetServerDesc=local machine
) ELSE (
	SET DeployScripts="%DropFolder%psexec" \\%ServerName% %PSExecSwitches% "%DeployScriptsBat%" 2^>^&1
	IF "%TfsRemote%"=="Y" SET InstallSvcLog=N
	SET TargetServerDesc=%TargetServer%
)

SET DeployScriptsCmd=@"%DropFolder%DeployScripts.bat" "%TargetServer%" "%TargetDatabase%" "%SourceFolder%" "%SourceFileSpec%" "%DeployScriptsLog%" "%Timeout%" "%SqlCmdExe%"

IF /i NOT %Debug%==ON GOTO SkipDebug
ECHO.
ECHO TargetServer    %TargetServer%
ECHO TargetDatabase  %TargetDatabase%
ECHO SourceFolder    %SourceFolder%
ECHO SourceFileSpec  %SourceFileSpec%
ECHO LogFile         %LogFile%
ECHO Timeout         %Timeout%
ECHO SqlCmdExe       %SqlCmdExe%
ECHO.
ECHO LogFileName     %LogFileName%
ECHO ServerName      %ServerName%
ECHO.
ECHO DeployScriptsCmd:
ECHO %DeployScriptsCmd%
ECHO.
ECHO DeployScripts:
ECHO %DeployScripts%
ECHO.
PAUSE
:SkipDebug

IF EXIST "%ErrFileName%" DEL "%ErrFileName%" /f/q
SET RedirLog=
IF /i "%LogFile%"=="Y" SET RedirLog=^>^> "%LogFileName%" 2^>^&1
SET RedirCmd=
SET RedirCmdLog=%DropFolder%DeployDatabase.log
IF "%TfsRemote%"=="Y" SET RedirCmd=^> "%RedirCmdLog%"

ECHO Executing deployment batch file on %ServerName%... %RedirLog%
IF EXIST "%DeployScriptsBat%" DEL /Q/F "%DeployScriptsBat%"
ECHO %DeployScriptsCmd% ^%RedirCmd% > "%DeployScriptsBat%"
ECHO %DeployScripts% %RedirLog%

SET MaxAttempts=6
SET Attempt=0

:DeployScripts
SET /a Attempt+=1
SET ExitCode=0
ECHO. %RedirLog%
ECHO Attempt %Attempt% of %MaxAttempts% to launch batch file on %TargetServerDesc%... %RedirLog%
IF "%TfsRemote%"=="Y" IF EXIST "%RedirCmdLog%" DEL /Q/F "%RedirCmdLog%"
CALL %DeployScripts% %RedirLog%
SET ExitCode=%ERRORLEVEL%
IF "%TfsRemote%"=="Y" IF EXIST "%RedirCmdLog%" TYPE "%RedirCmdLog%"
IF "%ExitCode%"=="0" GOTO Success
IF %Attempt% LSS %MaxAttempts% IF NOT "%TargetServer%"=="" (
	ECHO Launch failed with errorlevel %ExitCode%.  Attempting to stop PSEXESVC on %TargetServerDesc%... %RedirLog%
	CALL "%DropFolder%SvcCtl" PSEXESVC Stop %ServerName% 3 10 %RedirLog%
	GOTO DeployScripts
)
IF NOT "%ExitCode%"=="0" GOTO Error

:Success
ECHO Deployment batch file completed successfully %RedirLog%
GOTO End

:Error
ECHO Terminating with ExitCode: %ExitCode% %RedirLog%
ECHO ExitCode: %ExitCode% > "%ErrFileName%"

:End
ECHO. %RedirLog%

IF "%ExitCode%"=="" SET ExitCode=0
EXIT /b %ExitCode%
ENDLOCAL
