/*
This migration script replaces uncommitted changes made to these objects:
TenantQuestions

Use this script to make necessary schema and data changes for these objects only. Schema changes to any other objects won't be deployed.

Schema changes and migration scripts are deployed in the order they're committed.

Migration scripts must not reference static data. When you deploy migration scripts alongside static data 
changes, the migration scripts will run first. This can cause the deployment to fail. 
Read more at https://documentation.red-gate.com/display/SOC6/Static+data+and+migrations.
*/

--================================================================

-- START --  AD-2688 Add Primary Keys to the Tenant tables missing the PK so that EF Core can scafold correctly

-- Name Initial and the date

--================================================================


IF OBJECT_ID('tempdb..#TenantQuestionsTemp') IS NOT NULL
    DROP TABLE #TenantQuestionsTemp;
GO

--select into temp table values where id is null
SELECT [Description]
INTO   #TenantQuestionsTemp
FROM   dbo.TenantQuestions
WHERE  Id IS NULL;

--delete from tenant questions table rows where id is null
DELETE FROM dbo.TenantQuestions
WHERE Id IS NULL;

SET NUMERIC_ROUNDABORT OFF;
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON;
PRINT N'Altering [dbo].[TenantQuestions]';


--alter table so that id is not null
ALTER TABLE [dbo].[TenantQuestions]
ALTER COLUMN [Id] [INT] NOT NULL;
GO

--if pk does not exist
IF (
   SELECT COUNT(*)
   FROM   sys.objects
   WHERE  type_desc LIKE '%CONSTRAINT'
          AND OBJECT_NAME(object_id) = 'PK_TenantQuestions_TenantQuestionId'
   ) = 0
    BEGIN
        PRINT N'Creating primary key [PK_TenantQuestions_TenantQuestionId] on [dbo].[TenantQuestions]';

        --create PK contraint for Id
        ALTER TABLE [dbo].[TenantQuestions]
        ADD CONSTRAINT [PK_TenantQuestions_TenantQuestionId]
            PRIMARY KEY CLUSTERED ( [Id] ) ON [PRIMARY];

    END;

--insert values into table that were copied into temp table
INSERT INTO dbo.TenantQuestions (
                                Description
                                )
            SELECT [Description]
            FROM   #TenantQuestionsTemp;

--drop temp table
DROP TABLE #TenantQuestionsTemp;

GO

--================================================================

-- END  --  AD-2688 Add Primary Keys to the Tenant tables missing the PK so that EF Core can scafold correctly

--================================================================



