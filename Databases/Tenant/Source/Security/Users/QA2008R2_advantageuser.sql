IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'QA2008R2\advantageuser')
CREATE LOGIN [QA2008R2\advantageuser] FROM WINDOWS
GO
CREATE USER [QA2008R2\advantageuser] FOR LOGIN [QA2008R2\advantageuser]
GO
