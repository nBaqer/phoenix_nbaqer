IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'MtiTstUsr1')
CREATE LOGIN [MtiTstUsr1] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [MtiTstUsr1] FOR LOGIN [MtiTstUsr1] WITH DEFAULT_SCHEMA=[Fame.Fame4321]
GO
