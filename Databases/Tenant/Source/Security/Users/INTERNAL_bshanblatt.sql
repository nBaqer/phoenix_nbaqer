IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'INTERNAL\bshanblatt')
CREATE LOGIN [INTERNAL\bshanblatt] FROM WINDOWS
GO
CREATE USER [INTERNAL\bshanblatt] FOR LOGIN [INTERNAL\bshanblatt]
GO
