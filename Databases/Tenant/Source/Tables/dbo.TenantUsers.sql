CREATE TABLE [dbo].[TenantUsers]
(
[TenantUserId] [int] NOT NULL IDENTITY(1, 1),
[TenantId] [int] NOT NULL,
[UserId] [uniqueidentifier] NOT NULL,
[IsDefaultTenant] [bit] NULL CONSTRAINT [DF__TenantUse__IsDef__151B244E] DEFAULT ((0)),
[IsSupportUser] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TenantUsers] ADD CONSTRAINT [PK_TenantUsers] PRIMARY KEY CLUSTERED  ([TenantUserId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TenantUsers] ADD CONSTRAINT [FK_TenantUsers_aspnet_Users_UserId_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
ALTER TABLE [dbo].[TenantUsers] ADD CONSTRAINT [FK_TenantUsers_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
GO
