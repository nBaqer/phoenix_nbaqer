CREATE TABLE [dbo].[UserMigrationErrors]
(
[MigrationId] [int] NOT NULL IDENTITY(1, 1),
[DatabaseName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ErrorNumber] [int] NULL,
[ErrorSeverity] [int] NULL,
[ErrorState] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ErrorProcedure] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ErrorLine] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ErrorMessage] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserMigrationErrors] ADD CONSTRAINT [PK_UserMigrationErrors_MigrationId] PRIMARY KEY CLUSTERED  ([MigrationId]) ON [PRIMARY]
GO
