CREATE TABLE [dbo].[tblUsers]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[Databasename] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Email] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[AccountActive] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[tblUsers] ADD CONSTRAINT [PK_tblUsers_Id] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
