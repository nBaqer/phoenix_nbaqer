CREATE TABLE [dbo].[RebusQueue]
(
[id] [bigint] NOT NULL IDENTITY(1, 1),
[recipient] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[priority] [int] NOT NULL,
[expiration] [datetime2] NOT NULL,
[visible] [datetime2] NOT NULL,
[headers] [varbinary] (max) NOT NULL,
[body] [varbinary] (max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[RebusQueue] ADD CONSTRAINT [PK_RebusQueue] PRIMARY KEY CLUSTERED  ([recipient], [priority], [id]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_EXPIRATION_RebusQueue] ON [dbo].[RebusQueue] ([expiration]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IDX_RECEIVE_RebusQueue] ON [dbo].[RebusQueue] ([recipient], [priority], [visible], [expiration], [id]) ON [PRIMARY]
GO
