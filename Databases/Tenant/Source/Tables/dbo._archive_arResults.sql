CREATE TABLE [dbo].[_archive_arResults]
(
[IdArchive] [bigint] NOT NULL IDENTITY(1, 1),
[Archive_Date] [datetime] NULL CONSTRAINT [DF_archive_arResults_Archive_Date] DEFAULT (getdate()),
[Archive_User] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ResultId] [uniqueidentifier] NOT NULL,
[TestId] [uniqueidentifier] NULL,
[Score] [decimal] (18, 2) NULL,
[GrdSysDetailId] [uniqueidentifier] NULL,
[Cnt] [int] NULL,
[Hours] [int] NULL,
[StuEnrollId] [uniqueidentifier] NULL,
[IsInComplete] [bit] NULL,
[DroppedInAddDrop] [bit] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[IsTransfered] [bit] NULL,
[IsClinicsSatisfied] [bit] NULL,
[DateDetermined] [datetime] NULL,
[IsCourseCompleted] [bit] NULL,
[IsGradeOverridden] [bit] NULL,
[GradeOverriddenDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[_archive_arResults] ADD CONSTRAINT [PK_archive_arResults] PRIMARY KEY CLUSTERED  ([IdArchive]) ON [PRIMARY]
GO
