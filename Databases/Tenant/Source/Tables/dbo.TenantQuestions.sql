CREATE TABLE [dbo].[TenantQuestions]
(
[Id] [int] NOT NULL,
[Description] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TenantQuestions] ADD CONSTRAINT [PK_TenantQuestions_TenantQuestionId] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
