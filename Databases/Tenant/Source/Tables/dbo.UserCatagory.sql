CREATE TABLE [dbo].[UserCatagory]
(
[UserCategoryId] [int] NOT NULL IDENTITY(1, 1),
[UserId] [uniqueidentifier] NOT NULL,
[IsSupportUser] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UserCatagory] ADD CONSTRAINT [PK_UserCategory_UserCategoryId] PRIMARY KEY CLUSTERED  ([UserCategoryId]) ON [PRIMARY]
GO
