CREATE TABLE [dbo].[Tenant]
(
[TenantId] [int] NOT NULL IDENTITY(1, 1),
[TenantName] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedDate] [datetime] NULL,
[ModifiedDate] [datetime] NULL,
[ModifiedBy] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ServerName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DatabaseName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[UserName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Password] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EnvironmentID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tenant] ADD CONSTRAINT [PK_Tenant] PRIMARY KEY CLUSTERED  ([TenantId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tenant] ADD CONSTRAINT [UC_TenantName] UNIQUE NONCLUSTERED  ([TenantName]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Tenant] ADD CONSTRAINT [FK_TENANT_ENVIRONMENT] FOREIGN KEY ([EnvironmentID]) REFERENCES [dbo].[Environment] ([EnvironmentId])
GO
