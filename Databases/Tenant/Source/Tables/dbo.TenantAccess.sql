CREATE TABLE [dbo].[TenantAccess]
(
[TenantAccessId] [int] NOT NULL IDENTITY(1, 1),
[TenantId] [int] NOT NULL,
[EnvironmentId] [int] NULL,
[ConnectionStringId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TenantAccess] ADD CONSTRAINT [PK__TenantAc__B9DCB96302FC7413] PRIMARY KEY CLUSTERED  ([TenantAccessId]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TenantAccess] ADD CONSTRAINT [FK_TenantAccess_DataConnection] FOREIGN KEY ([ConnectionStringId]) REFERENCES [dbo].[DataConnection] ([ConnectionStringId])
GO
ALTER TABLE [dbo].[TenantAccess] ADD CONSTRAINT [FK_TenantAccess_Environment] FOREIGN KEY ([EnvironmentId]) REFERENCES [dbo].[Environment] ([EnvironmentId])
GO
ALTER TABLE [dbo].[TenantAccess] ADD CONSTRAINT [FK_TenantAccess_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
GO
