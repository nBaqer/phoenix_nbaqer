CREATE TABLE [dbo].[Environment]
(
[EnvironmentId] [int] NOT NULL IDENTITY(1, 1),
[EnvironmentName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VersionNumber] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ApplicationURL] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Environment] ADD CONSTRAINT [PK_Environment] PRIMARY KEY CLUSTERED  ([EnvironmentId]) ON [PRIMARY]
GO
