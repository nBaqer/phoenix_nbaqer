CREATE TABLE [dbo].[ApiAuthenticationKey]
(
[Id] [int] NOT NULL IDENTITY(1000, 1),
[TenantId] [int] NOT NULL,
[Key] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[CreatedDate] [smalldatetime] NOT NULL CONSTRAINT [DF_ApiAuthenticationKey_CreatedDate] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ApiAuthenticationKey] ADD CONSTRAINT [PK_ApiAuthenticationKey] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ApiAuthenticationKey] ADD CONSTRAINT [FK_ApiAuthenticationKey_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
GO
