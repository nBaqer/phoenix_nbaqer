CREATE TABLE [dbo].[WapiLog]
(
[id] [bigint] NOT NULL IDENTITY(1, 1),
[TenantName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DateMod] [datetime] NOT NULL,
[NumberOfRecords] [int] NOT NULL CONSTRAINT [DF__syWapiLog__Numbe__6D6238AF] DEFAULT ((0)),
[Company] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ServiceInvoqued] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[OperationDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsOk] [bit] NOT NULL CONSTRAINT [DF__syWapiLog__IsOk__6E565CE8] DEFAULT ((0)),
[Comment] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WapiLog] ADD CONSTRAINT [PK_syWebApiLogs] PRIMARY KEY CLUSTERED  ([id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WapiLog] ADD CONSTRAINT [UQ_syWebApiLogs_id] UNIQUE NONCLUSTERED  ([id]) ON [PRIMARY]
GO
EXEC sp_addextendedproperty N'MS_Description', N'This table store the data log for Web API Operations
Fields Descriptions:
UserMod:                         Username
DatMod:                           Create data time
NumberOfRecords:        Number of record that were operated (if exists)
CompanyCode:              The code of the company that originate the log
ServiceCode:                   The service that originate the log (SETTING, LOGIN, GSTUDENTS)
OperationCode:              The type of operation involved (READ, PUSH, ETC)
IsOk:                                  If the operation was ERROR(0) or OK(1) 
          ', 'SCHEMA', N'dbo', 'TABLE', N'WapiLog', NULL, NULL
GO
