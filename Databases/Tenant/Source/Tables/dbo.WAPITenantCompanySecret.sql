CREATE TABLE [dbo].[WAPITenantCompanySecret]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[ExternalCompanyCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TenantId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WAPITenantCompanySecret] ADD CONSTRAINT [PK_WAPITenantCompanySecret] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WAPITenantCompanySecret] ADD CONSTRAINT [UQ_WAPITenantCompanySecret_COMPANY_SECRET] UNIQUE NONCLUSTERED  ([ExternalCompanyCode]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WAPITenantCompanySecret] ADD CONSTRAINT [UQ_WAPITenantCompanySecret_Id] UNIQUE NONCLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[WAPITenantCompanySecret] ADD CONSTRAINT [FK_WAPITenantCompanySecret_Tenant] FOREIGN KEY ([TenantId]) REFERENCES [dbo].[Tenant] ([TenantId])
GO
EXEC sp_addextendedproperty N'MS_Description', N'This table hold the relation between the Tenant with the different company secrets associated with it.
One tenant can has many companySecret, but one company secret has only one Tenant.', 'SCHEMA', N'dbo', 'TABLE', N'WAPITenantCompanySecret', NULL, NULL
GO
