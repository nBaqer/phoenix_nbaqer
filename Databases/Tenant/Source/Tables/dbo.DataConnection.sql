CREATE TABLE [dbo].[DataConnection]
(
[ConnectionStringId] [int] NOT NULL IDENTITY(1, 1),
[ServerName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DatabaseName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PasswordENC] [varbinary] (128) NULL,
[Password] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DataConnection] ADD CONSTRAINT [PK_DataConnection] PRIMARY KEY CLUSTERED  ([ConnectionStringId]) ON [PRIMARY]
GO
