SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Procedure [dbo].[USP_DecryptPassword]
@UserName varchar(50)
as
	begin
	/**
		-- Commented by Balaji on April 22 2013 due to master key issues
	
		-- Verify the encryption.
		-- First, open the symmetric key with which to decrypt the data.
		OPEN SYMMETRIC KEY PasswordKey DECRYPTION BY CERTIFICATE UserPasswordEncryptionNew
		
		-- Select Decrypted Password
		SELECT Password
		AS 'Encrypted password', CONVERT(varchar,
		DecryptByKey(Password, 1 , 
		HashBytes('SHA1', CONVERT(varbinary, ConnectionStringId))))
		AS 'Decrypted password' FROM dbo.DataConnection
		--where ConnectionStringId=6
		where username=@UserName
		**/
		
		SELECT Password
		AS 'Encrypted password', Password AS 'Decrypted password' FROM dbo.DataConnection
		where username=@UserName
	end
GO
