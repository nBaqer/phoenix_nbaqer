SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[USP_Environment_List]
as
	Select EnvironmentId,
		   EnvironmentName + '[' + VersionNumber + ']' as Environment 
	from 
		  [dbo].[Environment] 
GO
