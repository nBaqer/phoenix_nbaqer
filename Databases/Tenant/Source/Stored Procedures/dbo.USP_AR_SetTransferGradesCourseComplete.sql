SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[USP_AR_SetTransferGradesCourseComplete]
@stuenrollid UNIQUEIDENTIFIER,
@transferid UNIQUEIDENTIFIER,
@reqid UNIQUEIDENTIFIER,		 
@grdsysdetailid UNIQUEIDENTIFIER = NULL,		
@score DECIMAL(2) = NULL,				
@termid UNIQUEIDENTIFIER,		
@moduser VARCHAR(50),                      
@moddate DATETIME,
@completeddate DATETIME,	
@iscoursecompleted BIT,
@isGradeOverridden BIT							 	
AS
/*----------------------------------------------------------------------------------------------------
	Author          :	Bruce Shanblatt
    
    Create date		:	07/08/2013
    
	Procedure Name	:	USP_AR_SetTransferGradesCourseComplete
	
	Parameters		:	Name			 Type	 Data Type	             Required? 	
						=====			 ====	 =========	             =========
						@transferid      In		 UNIQUEIDENTIFIER		 Required	
						@stuenrollid	 In		 UNIQUEIDENTIFIER		 Required	
						@reqid			 In		 UNIQUEIDENTIFIER		 Required	
						@grdsysdetailid  In		 UNIQUEIDENTIFIER		
						@score			 In      DECIMAL(2)				
						@termid			 In		 UNIQUEIDENTIFIER		 Required	
						@moduser		 In		 VARCHAR(50)			 Required
						@completeddate   In      DATE                    Required
						@moddate		 In		 DATE					 Required
						@iscoursecomplete  In    BIT					 Required
						@isGradeOverridden In    BIT                     Required 					
											
*/-----------------------------------------------------------------------------------------------------

INSERT INTO dbo.arTransferGrades
        ( TransferId ,
          StuEnrollId ,
          ReqId ,
          GrdSysDetailId ,
          Score ,
          TermId ,
          ModDate ,
          ModUser ,          
          IsTransferred ,
          --CompletedDate ,
          isClinicsSatisfied ,
          IsCourseCompleted ,
          IsGradeOverridden
        )
VALUES  ( @transferid , -- TransferId - uniqueidentifier
          @stuenrollid , -- StuEnrollId - uniqueidentifier
          @reqid , -- ReqId - uniqueidentifier
          @grdsysdetailid , -- GrdSysDetailId - uniqueidentifier
          @score , -- Score - decimal
          @termid , -- TermId - uniqueidentifier
          @moddate , -- ModDate - datetime
          @moduser , -- ModUser - varchar(50),          
          0 , -- IsTransferred - bit
          --@completeddate , -- CompletedDate - datetime
          0 , -- isClinicsSatisfied - bit
          @iscoursecompleted , -- IsCourseCompleted - bit
          @isGradeOverridden  -- IsGradeOverridden - bit
        )
GO
