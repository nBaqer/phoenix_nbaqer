SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

 CREATE procedure [dbo].[usp_GetModulesCoursesResultsForMultipleStudentsWithLeadGrps]
(@campGrpId varchar(8000),
 @prgVerId varchar(8000),
 @statusCodeId varchar(8000) =null,
 @termId uniqueidentifier =null,
 @termDate datetime =null,
 @leadGrpId varchar(8000) 

 )
 as
 SET NOCOUNT ON;

           select * from 
            (select tg.StuEnrollId,tg.TermId,tm.TermDescrip,
         
                tg.Score as score,
           
                   tm.StartDate,tm.EndDate,rq.Code,rq.Descrip,tg.ReqId,rq.Credits,rq.FinAidCredits ,'00000000-0000-0000-0000-000000000000' as ClsSectionId, 
             (select Grade from arGradesystemdetails where GrdSysdetailid=tg.GrdSysdetailid) as Grade, 
             (select IsPass from arGradesystemdetails where GrdSysdetailid=tg.GrdSysdetailid) as IsPass, 
             (select IsCreditsAttempted from arGradesystemdetails where GrdSysdetailid=tg.GrdSysdetailid) as IsCreditsAttempted,
             (select IsCreditsEarned from arGradesystemdetails where GrdSysdetailid=tg.GrdSysdetailid) as IsCreditsEarned ,
             (select ExpGradDate from arStuEnrollments where stuEnrollid=tg.StuEnrollId) as ExpGradDate
             from arTransferGrades tg, arTerm tm, arReqs rq 
            where tg.TermId=tm.TermId 
            and tg.ReqId=rq.ReqId 
            and (@termId is null or tm.Termid =@termId)
            and (@termDate is null or tm.StartDate <= @termDate) 
            AND tg.IsCourseCompleted = 1
           
             UNION 

            select ar.StuEnrollId,cs.TermId,tm2.TermDescrip,
            ar.Score as score,
           
                   tm2.StartDate,tm2.EndDate,rq2.Code,rq2.Descrip,cs.ReqId,rq2.Credits,rq2.FinAidCredits ,cs.ClsSectionid, 
             (select Grade from arGradesystemdetails where GrdSysdetailid=ar.GrdSysdetailid) as Grade, 
             (select IsPass from arGradesystemdetails where GrdSysdetailid=ar.GrdSysdetailid) as IsPass, 
             (select IsCreditsAttempted from arGradesystemdetails where GrdSysdetailid=ar.GrdSysdetailid) as IsCreditsAttempted,
             (select IsCreditsEarned from arGradesystemdetails where GrdSysdetailid=ar.GrdSysdetailid) as IsCreditsEarned,
             (select ExpGradDate from arStuEnrollments where stuEnrollid=ar.StuEnrollId) as ExpGradDate 
             from arResults ar, arClassSections cs, arTerm tm2, arReqs rq2 
            where ar.TestId=cs.ClsSectionId 
            and cs.TermId=tm2.TermId 
            and cs.ReqId=rq2.ReqId 
                and (@termId is null or tm2.Termid =@termId)
            and (@termDate is null or tm2.StartDate <= @termDate) 
            AND ar.IsCourseCompleted = 1

            ) P 
            WHERE EXISTS (SELECT DISTINCT arStuEnrollments.StuEnrollId 
                          FROM arStuEnrollments, arStudent A, syCampuses C, syCmpGrpCmps,syCampGrps,adLeadByLeadGroups, adLeadGroups
                          WHERE arStuEnrollments.StudentId=A.StudentId 
                          AND arStuEnrollments.CampusId=C.CampusId 
                          AND arStuEnrollments.StuEnrollId=P.StuEnrollId 
                          AND syCmpGrpCmps.CampusId = arStuEnrollments.CampusId 
                          AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId 
                          and syCmpGrpCmps.campgrpid in (select strval from dbo.SPLIT(@campGrpId))
                          AND arstuenrollments.prgverid in (select strval from dbo.SPLIT(@prgVerId))
                          and (@statuscodeid is null or arstuenrollments.statuscodeid in (select strval from dbo.SPLIT(@statusCodeId)))
                          AND adLeadByLeadGroups.LeadGrpId=adLeadGroups.LeadGrpId 
                        AND adLeadByLeadGroups.StuEnrollId=arStuEnrollments.StuEnrollId 
                        and adLeadByLeadGroups.leadgrpid in (select strval from dbo.SPLIT(@leadgrpid))
            ) 
            and P.StuEnrollId not in (select SGS.StuEnrollId from adStuGrpStudents SGS,adStudentGroups SG where SGS.StuGrpId=SG.StuGrpId and SG.IsTransHold=1 AND SGS.IsDeleted=0 and SGS.StuEnrollId is not null) 
             order by stuEnrollId,StartDate,Grade desc,Score desc 
             
GO
