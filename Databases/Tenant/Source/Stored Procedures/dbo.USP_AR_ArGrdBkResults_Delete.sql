SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/* 
US4257 Course marked as incomplete when score of one of the component is removed

CREATED: 
7/24/2013 TT

PURPOSE: 
Delete arGrdBkResults


MODIFIED:


*/


CREATE Procedure [dbo].[USP_AR_ArGrdBkResults_Delete]
( 
	@GrdBkResultId uniqueidentifier
)
AS
BEGIN

	DECLARE @ClsSectionId uniqueidentifier
	DECLARE @StuEnrollId uniqueidentifier
	
	SELECT TOP 1 @ClsSectionId = ClsSectionId, @StuEnrollId = StuEnrollId 
			FROM arGrdBkResults
			WHERE GrdBkResultId  = @GrdBkResultId

	DELETE FROM arGrdBkResults WHERE GrdBkResultId = 	@GrdBkResultId

	UPDATE arResults
		SET	IsCourseCompleted = 0,
			Score = null,
			GrdSysDetailId = null
		WHERE TestId =@ClsSectionId
					and StuEnrollId = @StuEnrollId	
END	

---------------------------------------------------------------------------------------------------
--US4257: Course marked as incomplete when score of one of the component is removed
--End Added by Tatiana Timochina and Wendy Prudente on 8/01/2013
---------------------------------------------------------------------------------------------------
GO
