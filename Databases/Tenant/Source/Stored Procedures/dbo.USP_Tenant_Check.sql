SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[USP_Tenant_Check]
@TenantName varchar(100)
as
	select Count(*) as TenantRowCount from [TenantAuthDB].[dbo].[Tenant] where TenantName=@TenantName
GO
