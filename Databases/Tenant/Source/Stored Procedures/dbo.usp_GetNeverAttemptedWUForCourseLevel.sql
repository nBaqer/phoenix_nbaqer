SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [dbo].[usp_GetNeverAttemptedWUForCourseLevel]
(@stuEnrollId uniqueidentifier
 )
 as
 SET NOCOUNT ON;

              select d.reqid,d.termId,e.Descrip, 
            (CASE e.SysComponentTypeId    
             WHEN 500 THEN a.Number  
             WHEN 503 THEN a.Number  
             WHEN 544 THEN a.Number 
             ELSE (SELECT MIN(MinVal) 
                         FROM arGradeScaleDetails GSD, arGradeSystemDetails GSS,arClassSections CSR 
                         WHERE GSD.GrdSysDetailId=GSS.GrdSysDetailId 
                         AND GSS.IsPass=1 and GSD.GrdScaleId=CSR.GrdScaleId and CSR.ClsSectionId=d.ClsSectionId)        END        )	AS MinResult, 
             a.required, a.mustpass, e.SysComponentTypeId as WorkUnitType, a.number,(select Resource from syResources where Resourceid=e.SysComponentTypeId) as WorkUnitDescrip, 
            a.InstrGrdBkWgtDetailId,c.stuenrollid    
             from arGrdBkWgtDetails a,arGrdBkWeights b,arResults c,arClassSections d,arGrdComponentTypes e 
             where a.InstrGrdBkWgtId=b.InstrGrdBkWgtId and  
             b.Reqid = d.reqid And c.testid = d.clsSectionid 
             and e.GrdComponentTypeId=a.GrdComponentTypeId 
             and b.EffectiveDate<=d.StartDate 
             and c.StuEnrollid= @stuEnrollId 
             and a.number is not null 
             and a.InstrGrdBkWgtDetailId not in (select InstrGrdBkWgtDetailId from arGrdBkResults 
                         where  StuEnrollId= @stuEnrollId  
             and clsSectionid in (select clsSectionid from arClassSections where Reqid = d.reqid 
             and Termid = d.TermId )) 
             and b.EffectiveDate = 
             (SELECT Distinct Top 1 A.EffectiveDate 
             FROM          arGrdBkWeights A,arClassSections B  
             WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=d.ClsSectionId order by A.EffectiveDate desc) 
             order by d.reqid,e.SysComponentTypeId,e.Descrip 

GO
