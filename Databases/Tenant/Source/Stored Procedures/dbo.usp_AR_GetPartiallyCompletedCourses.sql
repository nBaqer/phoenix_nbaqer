SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/* 
US4110 Transfer Partially Completed Components 

CREATED: 
6/27/2013 WMP

PURPOSE: 
Select incomplete courses to be displayed in transfer components grid 
	A record in arGrdBkResults table (regardless of score) means a grade has been entered (and component is completed)

MODIFIED:
7/10/2013 WMP	DE9829	Remove score > 0 from temp table where
7/22/2013 WMP	DE9875	Exclude externship hours (544) since they are only empty links

*/

CREATE PROC [dbo].[usp_AR_GetPartiallyCompletedCourses]
	@StuEnrollId UniqueIdentifier =  null
AS
BEGIN

	CREATE TABLE #IncludeCompletedComponentsList
	--verifies that at least 1 completed component exists for student's selected course
		(
			ResultId uniqueidentifier,
			StuEnrollId uniqueidentifier
		)
	INSERT INTO #IncludeCompletedComponentsList
		(ResultId, StuEnrollId)
		SELECT
			DISTINCT rs.ResultId, gbr.StuEnrollId
		FROM
			arGrdBkResults gbr
			INNER JOIN arClassSections cs ON cs.ClsSectionId = gbr.ClsSectionId
			INNER JOIN arResults rs ON rs.TestId = cs.ClsSectionId AND rs.StuEnrollId=gbr.StuEnrollId
			INNER JOIN arGrdBkWgtDetails gbwd ON gbwd.InstrGrdBkWgtDetailId = gbr.InstrGrdBkWgtDetailId 
			INNER JOIN arGrdComponentTypes gct ON gct.GrdComponentTypeId = gbwd.GrdComponentTypeId
		WHERE
			gbr.StuEnrollId=@StuEnrollId 
			AND gct.SysComponentTypeId <> 544

	SELECT 
		rq.ReqId,rq.Code,rq.Descrip,t.TermDescrip,rs.ResultId,cs.TermId,cs.ClsSectionId
	FROM
		arResults rs
		INNER JOIN arClassSections cs ON cs.ClsSectionId = rs.TestId
		INNER JOIN arReqs rq ON rq.ReqId = cs.ReqId
		INNER JOIN arTerm t ON t.TermId = cs.TermId
		INNER JOIN #IncludeCompletedComponentsList cc ON cc.ResultId = rs.ResultId
	WHERE
		rs.StuEnrollId=@StuEnrollId 
		AND IsNull(rs.IsCourseCompleted,0) = 0  
	ORDER BY
		t.StartDate, rq.Code, rq.Descrip
	
	DROP TABLE #IncludeCompletedComponentsList
	
END	
GO
