SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetImportedAttendance]
    (
      @stuEnrollId UNIQUEIDENTIFIER  
     
    )
AS 
    SET NOCOUNT ON ;    
    SELECT  ISNULL(SUM(actualhours) * 60, 0) AS Actual ,
            ISNULL(SUM(SchedHours) * 60, 0) AS Scheduled ,
            CASE WHEN ( SUM(SchedHours) - SUM(ActualHours) ) > 0
                 THEN ( SUM(SchedHours) * 60 - SUM(ActualHours) * 60 )
                 ELSE 0
            END AS [Absent] ,
            CASE WHEN ( SUM(ActualHours) - SUM(SchedHours) ) > 0
                 THEN ( SUM(ActualHours) * 60 - SUM(SchedHours) * 60 )
                 ELSE 0
            END AS MakeUp
    FROM    dbo.arStudentClockAttendance
    WHERE   StuEnrollId = @stuEnrollId
            AND converted = 1
            AND ActualHours <> 9999

GO
