SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetExtershipHours]
    (
      @stuEnrollid UNIQUEIDENTIFIER ,
      @ClassSectionID UNIQUEIDENTIFIER
    )
AS 
    DECLARE @TotalHoursAttended DECIMAL(6, 2)
    SET @TotalHoursAttended = ( SELECT  ISNULL(SUM(E.HoursAttended), 0)
                                FROM    dbo.arExternshipAttendance E
                                        INNER JOIN arGrdComponentTypes G ON E.GrdComponentTypeId = G.GrdComponentTypeId
                                        INNER JOIN dbo.arGrdBkWgtDetails D ON G.GrdComponentTypeId = D.GrdComponentTypeId
                                        INNER JOIN dbo.arGrdBkWeights W ON D.InstrGrdBkWgtId = W.InstrGrdBkWgtId
                                        INNER JOIN dbo.arReqs R ON W.ReqId = R.ReqId
                                        INNER JOIN dbo.arClassSections CS ON R.ReqId = CS.ReqId
                                WHERE   G.SysComponentTypeId = 544
                                        AND CS.ClsSectionId = @ClassSectionID AND E.StuEnrollId=@stuEnrollid
                              )
            
    SELECT  @stuEnrollid AS StuEnrollID ,
            @TotalHoursAttended AS TotalHoursAttended ,
            SUM(D.Number) AS RequiredHours ,
            ISNULL(( SUM(D.Number) - @TotalHoursAttended ), 0) AS RemainingHours
    FROM    arGrdComponentTypes G
            INNER JOIN dbo.arGrdBkWgtDetails D ON G.GrdComponentTypeId =
             D.GrdComponentTypeId
            INNER JOIN dbo.arGrdBkWeights W ON D.InstrGrdBkWgtId = W.InstrGrdBkWgtId
            INNER JOIN dbo.arReqs R ON W.ReqId = R.ReqId
            INNER JOIN dbo.arClassSections CS ON R.ReqId = CS.ReqId
    WHERE   G.SysComponentTypeId = 544
            AND CS.ClsSectionId = @ClassSectionID
GO
