SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RemoveTenantbyName]
    @TenantName VARCHAR(50)
AS
    BEGIN
        BEGIN TRY
            BEGIN TRANSACTION DeleteTenant;

            DECLARE @tenantIdValue INT = 0;
            SELECT @tenantIdValue = TenantId
            FROM   dbo.Tenant
            WHERE  DatabaseName = @TenantName;

            DECLARE @return_status INT;
            EXEC @return_status = dbo.RemoveTenant @tenantId = @tenantIdValue; -- int

            COMMIT TRANSACTION DeleteTenant;
        END TRY
        BEGIN CATCH
            ROLLBACK TRANSACTION DeleteTenant;
        END CATCH;
        SET NOCOUNT OFF;
    END;

GO
