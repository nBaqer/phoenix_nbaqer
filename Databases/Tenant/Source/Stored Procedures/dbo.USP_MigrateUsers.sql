SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_MigrateUsers]
    @DatabaseName VARCHAR(50)
   ,@TenantId INT
AS
    BEGIN

        DECLARE @SecurityQuestion VARCHAR(100)
               ,@SecurityAnswer VARCHAR(50);
        DECLARE @ApplicationId UNIQUEIDENTIFIER
               ,@LowerEmail VARCHAR(100);
        DECLARE @UserId UNIQUEIDENTIFIER
               ,@Email VARCHAR(100)
               ,@AccountActive BIT;
        SET @ApplicationId = 'C75D06DD-25F6-45B5-93A3-B71806320191';
        DECLARE @SQL VARCHAR(500)
               ,@SearchString VARCHAR(50)
               ,@saSearchString VARCHAR(5);


        --Set @SecurityQuestion = 'What is your favoutite color?'  
        --Set @SecurityAnswer = 'Blue'  

        SET @SearchString = 'support';
        SET @saSearchString = 'sa';
        SET @SQL = 'insert into tblUsers(DatabaseName,UserId,Email,AccountActive) Select Distinct ''' + @DatabaseName + ''',UserId,Email,AccountActive from '
                   + @DatabaseName + '.dbo.syusers where Lower(username) <> ''' + @SearchString + '''' + ' and Lower(username) <> ''' + @saSearchString + ''''
                   + ' order by email';
        PRINT @SQL;
        EXECUTE ( @SQL );

        DECLARE MigrateUsers_Cursor CURSOR LOCAL FORWARD_ONLY FAST_FORWARD READ_ONLY FOR
            SELECT DISTINCT UserId
                           ,Email
                           ,AccountActive
            FROM   tblUsers;

        OPEN MigrateUsers_Cursor;
        FETCH NEXT FROM MigrateUsers_Cursor
        INTO @UserId
            ,@Email
            ,@AccountActive;
        WHILE @@FETCH_STATUS = 0
            BEGIN
                --insert user's email address  
                --DBName.OWNER.SOURCETABLENAME : Syntax is mandatory in insert statement  
                BEGIN TRY
                    SET @LowerEmail = LOWER(@Email);

                    IF NOT EXISTS (
                                  SELECT *
                                  FROM   TenantAuthDB.dbo.aspnet_Users
                                  WHERE  UserId = @UserId
                                  )
                        BEGIN
                            INSERT INTO TenantAuthDB.dbo.aspnet_Users (
                                                                      ApplicationId
                                                                     ,UserId
                                                                     ,UserName
                                                                     ,LoweredUserName
                                                                     ,MobileAlias
                                                                     ,IsAnonymous
                                                                     ,LastActivityDate
                                                                      )
                            VALUES ( @ApplicationId, @UserId, @LowerEmail, @LowerEmail, NULL, 0, GETDATE());

                            -- Default password being used is : advantage3$  
                            -- Default security anwer: security answer  
                            -- User will be forced to change password at first login  
                            INSERT INTO dbo.aspnet_Membership (
                                                              ApplicationId
                                                             ,UserId
                                                             ,Password
                                                             ,PasswordFormat
                                                             ,PasswordSalt
                                                             ,MobilePIN
                                                             ,Email
                                                             ,LoweredEmail
                                                             ,PasswordQuestion
                                                             ,PasswordAnswer
                                                             ,IsApproved
                                                             ,IsLockedOut
                                                             ,CreateDate
                                                             ,LastLoginDate
                                                             ,LastPasswordChangedDate
                                                             ,LastLockoutDate
                                                             ,FailedPasswordAttemptCount
                                                             ,FailedPasswordAttemptWindowStart
                                                             ,FailedPasswordAnswerAttemptCount
                                                             ,FailedPasswordAnswerAttemptWindowStart
                                                             ,Comment
                                                              )
                            VALUES ( @ApplicationId, @UserId, 'CcnbzYVEUDPVOH1ne2m/X2ttvQ4=', 1, 'ghMdIEJSDwFgiWkA/a0JTw==', NULL, @LowerEmail, @LowerEmail
                                    ,'What is your favourite color', 'BtJLiybtDny54+q4nmhXtzlXovE=', @AccountActive, 0, GETDATE(), GETDATE(), GETDATE()
                                    ,GETDATE(), 0, GETDATE(), 0, GETDATE(), NULL );
                        END;

                    IF NOT EXISTS (
                                  SELECT *
                                  FROM   TenantAuthDB.dbo.TenantUsers
                                  WHERE  TenantId = @TenantId
                                         AND UserId = @UserId
                                  )
                        BEGIN
                            INSERT INTO TenantAuthDB.dbo.TenantUsers (
                                                                     TenantId
                                                                    ,UserId
                                                                    ,IsDefaultTenant
                                                                    ,IsSupportUser
                                                                     )
                            VALUES ( @TenantId, @UserId, 0, 0 );
                        END;

                END TRY
                BEGIN CATCH
                    INSERT INTO UserMigrationErrors (
                                                    DatabaseName
                                                   ,UserId
                                                   ,Email
                                                   ,ErrorNumber
                                                   ,ErrorSeverity
                                                   ,ErrorState
                                                   ,ErrorProcedure
                                                   ,ErrorLine
                                                   ,ErrorMessage
                                                   ,ModUser
                                                   ,ModDate
                                                    )
                                SELECT @DatabaseName
                                      ,@UserId
                                      ,@Email
                                      ,ERROR_NUMBER() AS ErrorNumber
                                      ,ERROR_SEVERITY() AS ErrorSeverity
                                      ,ERROR_STATE() AS ErrorState
                                      ,ERROR_PROCEDURE() AS ErrorProcedure
                                      ,ERROR_LINE() AS ErrorLine
                                      ,ERROR_MESSAGE() AS ErrorMessage
                                      ,'support'
                                      ,GETDATE();
                END CATCH;
                FETCH NEXT FROM MigrateUsers_Cursor
                INTO @UserId
                    ,@Email
                    ,@AccountActive;
            END;
        --Truncate table tblUsers -- Clear contents   
        CLOSE MigrateUsers_Cursor;
        DEALLOCATE MigrateUsers_Cursor;

    END;
GO
