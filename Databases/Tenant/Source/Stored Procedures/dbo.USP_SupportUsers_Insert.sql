SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[USP_SupportUsers_Insert]
 @Username varchar(50)
 as
 begin
	Declare @UserId uniqueidentifier
	Set @UserId = (Select Distinct UserId from dbo.[aspnet_users] where Username=@UserName)
	
	if not exists (select * from dbo.[UserCatagory] where UserId=@UserId and IsSupportUser=1)
	begin
		Insert into dbo.[UserCatagory](UserId,IsSupportUser)
		values(@UserId,1)
	end
 end
GO
