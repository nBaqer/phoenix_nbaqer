SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/* 
DE9916 Student should not be allowed to register on the new "Transfer Partially Completed Components page" if the student is on registration hold 

CREATED: 
8/2/2013 WMP

PURPOSE: 
Get student groups for selected student enrollment

MODIFIED:


*/

CREATE PROC [dbo].[usp_adStuGrpStudents_SELECT_ByEnrollment]
	@StuEnrollId uniqueidentifier
AS
BEGIN

SELECT	
	sgs.StuGrpStuId, 
	sgs.StuGrpId, 
	sgs.StudentId, 
	DateAdded, 
	DateRemoved, 
	sgs.ModUser, 
	sgs.ModDate, 
	IsDeleted, 
	sgs.StuEnrollId,
	sg.Descrip as StudentGroup, sg.Code as StudentGroupCode,
	sg.IsPublic, sg.IsRegHold, sg.IsTransHold,
	s.LastName + ', ' + s.FirstName + ' ' + ISNULL(s.MiddleName,'') as StudentName
FROM
	adStuGrpStudents sgs
	INNER JOIN adStudentGroups sg ON sg.StuGrpId = sgs.StuGrpId
	INNER JOIN arStudent s ON s.StudentId = sgs.StudentId
WHERE
	sgs.StuEnrollId = @StuEnrollId
	AND ISNULL(sgs.IsDeleted,0) = 0
END	
GO
