SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/* 
US4257 Course marked as incomplete when score of one of the component is removed

CREATED: 
7/24/2013 WMP,TT

PURPOSE: 
Update arGrdBkResults


MODIFIED:


*/

CREATE PROC [dbo].[usp_AR_ArGrdBkResults_Update]
	@ClsSectionId uniqueidentifier,
	@StuEnrollId uniqueidentifier,
	@InstrGrdBkWgtDetailId uniqueidentifier,
	@Score decimal(6,2),
	@Comments varchar(50),
	@ModUser varchar(50),
	@ResNum int = 0,
	@IsCompGraded bit
AS
BEGIN

	DECLARE @IsScoreChanged bit = 0
	DECLARE @ExistingScore decimal(6,2)
	DECLARE @ResourceComp int
	
	SELECT TOP 1 @ExistingScore = Score, @ResourceComp = r.ResourceID 
			FROM arGrdBkResults  gbr JOIN arGrdBkWgtDetails gbwr ON gbwr.InstrGrdBkWgtDetailId = gbr.InstrGrdBkWgtDetailId
				JOIN arGrdComponentTypes gct ON gct.GrdComponentTypeId = gbwr.GrdComponentTypeId
				JOIN syResources r ON r.ResourceID = gct.SysComponentTypeId 
			WHERE ClsSectionId = @ClsSectionId 
					and StuEnrollId = @StuEnrollId 
					and gbr.InstrGrdBkWgtDetailId = @InstrGrdBkWgtDetailId
					and @ResNum = CASE @ResNum
									WHEN 0 THEN @ResNum 
									ELSE ResNum END	


	IF @Score <> @ExistingScore
		SET @IsScoreChanged = 1			
	    
  
	
	UPDATE arGrdBkResults
	SET		Score = @Score,
			Comments = @Comments,
			ModUser = @ModUser,
			ModDate = GETDATE(),
			IsCompGraded = @IsCompGraded
	WHERE ClsSectionId = @ClsSectionId 
			and StuEnrollId = @StuEnrollId 
			and InstrGrdBkWgtDetailId = @InstrGrdBkWgtDetailId
			and @ResNum = CASE @ResNum
							WHEN 0 THEN @ResNum 
							ELSE ResNum END								

	IF @IsScoreChanged = 1 AND @ResourceComp <> 612 --Dictation/Speed Test
		UPDATE arResults
		SET	IsCourseCompleted = 0,
			Score = null,
			GrdSysDetailId = null
		WHERE TestId = @ClsSectionId 
					and StuEnrollId = @StuEnrollId
		

END 
GO
