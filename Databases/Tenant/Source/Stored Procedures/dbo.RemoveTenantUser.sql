SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ginzo, John
-- Create date: 11/12/2013
-- Description:	Remove a tenant user
-- =============================================
CREATE PROCEDURE [dbo].[RemoveTenantUser] 
	@UserId uniqueidentifier
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    
    BEGIN TRAN REMOVEUSER
    
    
    DELETE FROM TenantUsers where UserId=@UserId
    
    IF @@error <> 0
		BEGIN
			ROLLBACK TRAN REMOVEUSER
			print 'Delete from tenantuser failed'
		END
    
    
    DELETE from aspnet_Membership where UserId=@UserId
    
    IF @@error <> 0
		BEGIN
			ROLLBACK TRAN REMOVEUSER
			print 'Delete from aspnet_membership failed'
		END
    
    
	DELETE FROM aspnet_Users where UserId=@UserId
	
	IF @@error <> 0
		BEGIN
			ROLLBACK TRAN REMOVEUSER
			print 'Delete from aspnet_membership failed'
		END
		
		
	COMMIT TRAN REMOVEUSER 
	print 'Delete completed' 
    
    
    
END

GO
