SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


--sp_helptext USP_MapSupportUsers_List '[AMC3.4]'

--exec USP_MapSupportUsers_List '[AMC3.4]'
CREATE PROC [dbo].[USP_MapSupportUsers_List]
@DatabaseName varchar(50)
as
begin
-- Query to create support users from Tenant Database to Customer Database
		declare @SupportUserId varchar(50),@SQL varchar(1000)
	Set @SupportUserId = (select UserId from dbo.aspnet_users 
	where username='support@fameinc.com')
	
	
	Create table #tempUserRoles(UserId uniqueidentifier)
	insert into #tempUserRoles
	Select Distinct UserId from dbo.[UserCatagory]
	where UserId <> @SupportUserId and isSupportUser=1

	Declare @SupportUserCount int
	Set @SupportUserCount = (Select Count(*) from #tempUserRoles)

	declare @TotalRows int, @increment int ,@username varchar(255)
	set @increment=1

	Create table #tmpRoles(TotalRows int)
	Create table #tmpMRUS(TotalRows int)
	Create table #tmpUsers(TotalRows int)	
	
	while @increment <= @SupportUserCount
	begin
		---- Print @increment
	
		Declare @MapSupportUserId uniqueidentifier
		Set @MapSupportUserId = (select Top 1 UserId from #tempUserRoles)
		
		
		
		Declare @StartSQL varchar(300),@EndSQL varchar(100)
		Declare @CountSQL varchar(300)
		set @CountSQL = 'insert into #tmpRoles select Count(*) from ' + @DatabaseName + '.dbo.syUsersRolesCampGrps where userId=''' + Convert(char(36),@MapSupportUserId) + ''''
		---- Print @CountSQL
		EXEC (@CountSQL)
		
		if (Select Count(*) from #tmpRoles)=0
		begin
			Set @SQL = ' Insert into ' + @DatabaseName + '.dbo.syUsersRolesCampGrps(UserRoleCampGrpId, UserId,RoleId,CampGrpId,ModDate,ModUser)
			select newid(), ''' +  Convert(char(36),@MapSupportUserId) + ''',RoleId,CampGrpId,getdate(),''support''
			from ' + @DatabaseName + '.dbo.syUsersRolesCampGrps where userId=''' + Convert(char(36),@SupportuserId) + ''''
		
			-- Print (@SQL)
			EXEC(@SQL)
		
		end
		
		Delete from #tmpRoles
		
		--Set @StartSQL = 'if not exists (select * from ' + @DatabaseName + '.dbo.syUsersRolesCampGrps where userId=''' + Convert(char(36),@MapSupportUserId) + '''' + ')'
	
		--Set @SQL = ' begin Insert into ' + @DatabaseName + '.dbo.syUsersRolesCampGrps(UserRoleCampGrpId, UserId,RoleId,CampGrpId,ModDate,ModUser)
		--select newid(), ''' +  Convert(char(36),@MapSupportUserId) + ''',RoleId,CampGrpId,getdate(),''support''
		--from ' + @DatabaseName + '.dbo.syUsersRolesCampGrps where userId=''' + Convert(char(36),@SupportuserId) + ''''
		
		--Set @EndSQL = ' end'
		---- Print (@StartSQL + @SQL + @EndSQL)
		--Exec (@StartSQL + @SQL + @EndSQL)
		
		--syMRus
		
		set @CountSQL = 'insert into #tmpMRUS select Count(*)  from ' + @DatabaseName + '.dbo.syMRUS where userId=''' + Convert(char(36),@MapSupportUserId) + ''''
		---- Print @CountSQL
		EXEC (@CountSQL)
		if (Select Count(*) from #tmpMRUS)=0
		begin
				Set @SQL = ' Insert into ' + @DatabaseName + '.dbo.syMRUS(MRUId,ChildId,MRUTypeId,UserId,CampusId,SortOrder,IsSticky,ModUser,ModDate)
						select newid(),ChildId,MRUTypeId,''' +  Convert(char(36),@MapSupportUserId) + ''',CampusId,SortOrder,IsSticky,''support'',getdate()
						from ' + @DatabaseName + '.dbo.syMRUS where userId=''' + Convert(char(36),@SupportuserId) + '''' 
				-- Print (@SQL)
				EXEC (@SQL)
		end
		Delete from #tmpMRUS
		
		--Set @StartSQL = 'if not exists (select * from ' + @DatabaseName + '.dbo.syMRUS where userId=''' + Convert(char(36),@MapSupportUserId) + '''' + ')'
		--Set @SQL = ' begin 
		--				Insert into ' + @DatabaseName + '.dbo.syMRUS(MRUId,ChildId,MRUTypeId,UserId,CampusId,SortOrder,IsSticky,ModUser,ModDate)
		--				select newid(),ChildId,MRUTypeId,''' +  Convert(char(36),@MapSupportUserId) + ''',CampusId,SortOrder,IsSticky,''support'',getdate()
		--				from ' + @DatabaseName + '.dbo.syMRUS where userId=''' + Convert(char(36),@SupportuserId) + '''' + 
		--		   ' end'
					 
					 
		----Set @EndSQL = ' end'
		
		---- Print (@StartSQL + @SQL)
		--Exec (@StartSQL + @SQL + @EndSQL)
		--select * from syMRUS
		
		declare @userEmail varchar(100)
		set @userEmail = (select top 1 Username from dbo.[aspnet_users] where UserId=@MapSupportUserId)
		
		
		Declare @RowCount int
		Set @StartSQL = 'if not exists (select * from ' + @DatabaseName + '.dbo.syUsers where userId=''' + Convert(char(36),@MapSupportUserId) + '''' + ')'
				
			
		set @CountSQL = 'insert into #tmpUsers select Count(*)  from ' + @DatabaseName + '.dbo.syUsers where userId=''' + Convert(char(36),@MapSupportUserId) + ''''
		-- Print @CountSQL
		EXEC (@CountSQL)
		
		if (Select Count(*) from #tmpUsers)=0
		begin
		Set @SQL = ' Insert into ' + @DatabaseName + '.dbo.syUsers(UserId,FullName,Email,UserName,Password,ConfirmPassword,Salt,AccountActive,ModDate,
		ModUser,CampusId,ShowDefaultCampus,ModuleId,IsAdvantageSuperUser,IsDefaultAdminRep,IsLoggedIn)
		select ''' +  Convert(char(36),@MapSupportUserId) + ''',FullName, ''' + @userEmail + ''',UserName,''' + substring(convert(char(36),@MapSupportUserId),1,5) + ''' ,ConfirmPassword,Salt,AccountActive,
		getdate(),''support'',campusid,ShowDefaultCampus,ModuleId,IsAdvantageSuperUser,IsDefaultAdminRep,IsLoggedIn
		from ' + @DatabaseName + '.dbo.syUsers where userId=''' + Convert(char(36),@SupportuserId) + ''''
		--Set @SQL = ' begin  Insert into ' + @DatabaseName + '.dbo.syUsers(UserId,FullName,Email,UserName,Password,ConfirmPassword,Salt,AccountActive,ModDate,
		--ModUser,CampusId,ShowDefaultCampus,ModuleId,IsAdvantageSuperUser,IsDefaultAdminRep,IsLoggedIn)
		--select ''' +  Convert(char(36),@MapSupportUserId) + ''',FullName, ''' + @userEmail + ''',UserName,''' + substring(convert(char(36),@MapSupportUserId),1,5) + ''' ,ConfirmPassword,Salt,AccountActive,
		--getdate(),''support'',campusid,ShowDefaultCampus,ModuleId,IsAdvantageSuperUser,IsDefaultAdminRep,IsLoggedIn
		--from ' + @DatabaseName + '.dbo.syUsers where userId=''' + Convert(char(36),@SupportuserId) + ''''
		-- Print (@SQL)
		Exec(@SQL)
		end
		
		delete from #tmpUsers
	
		--Set @EndSQL = ' end'
		
		---- Print (@StartSQL + @SQL + @EndSQL)
		
		--Exec (@StartSQL + @SQL + @EndSQL)
		
		
			
		delete from #tempUserRoles where userid=@MapSupportUserId
		set @increment += 1
	end
	Drop table #tempUserRoles
	Drop table #tmpRoles
	Drop table #tmpMRUS
	drop table #tmpUsers
end
GO
