SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[usp_AR_TransferCompletedComponentsByCourse]
	@NewClsSectionId uniqueIdentifier,
	@OldClsSectionId uniqueIdentifier,
	@StuEnrollId uniqueIdentifier,
	@User nvarchar(50)
AS
BEGIN

	INSERT INTO arGrdBkResults (
			GrdBkResultId,
			ClsSectionId,
			InstrGrdBkWgtDetailId,		
			Score,
			Comments,
			StuEnrollId,
			ModUser,
			ModDate,
			ResNum,
			PostDate,
			IsCompGraded,
			IsCourseCredited )
		SELECT
			NEWID(),
			@NewClsSectionId,
			InstrGrdBkWgtDetailId,		
			Score,
			Comments,
			@StuEnrollId,
			@User,
			GETDATE(),
			ResNum,
			PostDate,
			IsCompGraded,
			IsCourseCredited		
		FROM arGrdBkResults
		WHERE StuEnrollId = @StuEnrollId
			AND ClsSectionId = @OldClsSectionId

END
GO
