SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Proc [dbo].[USP_GetExternshipCourses_ForStudentEnrollment]
@StuEnrollId uniqueidentifier
as
Select Distinct t4.TermId,t4.TermDescrip,t3.ReqId,t3.Code,t3.Descrip,0.00 as Credits,0.00 as Hours,NULL as CourseCategoryId,
t4.StartDate as StartDate, t4.EndDate as EndDate,t2.StartDate as ClassStartDate,t2.EndDate as ClassEndDate,t1.GrdSysDetailId,
t1.TestId,t1.ResultId,NULL as Grade,Null as IsPass,Null as GPA,Null as IsCreditsAttempted,
NUll as IsCreditsEarned,Null as IsInGPA,null as IsDrop,Null as CourseCategory,
Null as Score,Null as FinAidCredits,Null as DateIssue,Null as DropDate,
Null as ScheduledHours, Null as IsTransferGrade,Null as AvgScoreNew,Null as AvgGPANew,t1.IsCourseCompleted
from arResults t1 inner join arClassSections t2 on t1.TestId=t2.ClsSectionId 
inner join arReqs t3 on t2.ReqId=t3.ReqId 
inner join arTerm t4 on t2.TermId = t4.TermId
and t3.IsExternship=1
where
t1.StuEnrollid=@StuEnrollId 
GO
