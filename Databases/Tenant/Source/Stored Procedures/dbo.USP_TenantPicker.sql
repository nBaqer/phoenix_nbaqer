SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[USP_TenantPicker]
as
Select T4.TenantName,T4.TENANTNAME + ' (Version:' + t2.VersionNumber + ')' as TenantNameWithVersion, t2.VersionNumber,t4.tenantId
	FROM 
		[TenantAuthDB].dbo.[TenantAccess] T1 Inner Join 
		[TenantAuthDB].dbo.[Environment] T2 ON T1.ENVIRONMENTID=T2.ENVIRONMENTID
		INNER JOIN [TenantAuthDB].dbo.[DATACONNECTION] T3 ON T1.CONNECTIONSTRINGID=T3.CONNECTIONSTRINGID
		INNER JOIN [TenantAuthDB].dbo.[Tenant] T4 ON T1.TENANTID = T4.TENANTID
GO
