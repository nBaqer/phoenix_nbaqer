SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[USP_Tenant_Add]
@TenantName varchar(100)
as
begin
	if not exists (select * from [TenantAuthDB].[dbo].[Tenant] where TenantName=@TenantName)
	begin
		Insert into [TenantAuthDB].[dbo].[Tenant] (TenantName,CreatedDate,ModifiedDate,ModifiedBy) 
		values(@TenantName,getdate(),getdate(),'Support')
	end
	declare @SupportUserId varchar(50),@TenantId int
	Set @TenantId = (Select TenantId from [TenantAuthDB].[dbo].[Tenant] where TenantName=@TenantName)
	--Set @SupportUserId = (select UserId from TenantAuthDB.dbo.aspnet_users 
	--where username='support@fameinc.com')

	DECLARE MigrateUsers_Cursor CURSOR LOCAL 
     FORWARD_ONLY 
     FAST_FORWARD 
     READ_ONLY 
     FOR 
		
		Select Distinct UserId from [dbo].[UserCatagory] where IsSupportUser=1
		
		OPEN MigrateUsers_Cursor
		FETCH NEXT FROM MigrateUsers_Cursor into @SupportUserId
		WHILE @@FETCH_STATUS = 0
		BEGIN
			if not exists (select * from [TenantAuthDB].[dbo].[TenantUsers] where UserId=@SupportUserId 
								and TenantId=@TenantId)
			begin
				Insert into [TenantAuthDB].[dbo].[TenantUsers] (TenantId,UserId) 
				values(@TenantId,@SupportUserId)
			end
			FETCH NEXT FROM MigrateUsers_Cursor into @SupportUserId
		END
		CLOSE MigrateUsers_Cursor
		DEALLOCATE MigrateUsers_Cursor
end
GO
