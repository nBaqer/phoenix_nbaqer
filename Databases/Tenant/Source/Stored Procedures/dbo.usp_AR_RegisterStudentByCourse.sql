SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[usp_AR_RegisterStudentByCourse]
	@ClsSectionId uniqueIdentifier,
	@StuEnrollId uniqueIdentifier,
	@User nvarchar(50)
AS
BEGIN

	INSERT INTO arResults (
			ResultId,
			TestId,
			StuEnrollId,
			ModUser,
			ModDate )
		VALUES (
			NEWID(),
			@ClsSectionId,
			@StuEnrollId,
			@User,
			GETDATE() )

END
GO
