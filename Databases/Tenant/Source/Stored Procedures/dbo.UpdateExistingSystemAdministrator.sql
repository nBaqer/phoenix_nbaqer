SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Proc [dbo].[UpdateExistingSystemAdministrator]
@Databasename varchar(50),
@UserId varchar(50)
as
begin
	declare @SystemAdministratorId varchar(50)
	Set @SystemAdministratorId = @UserId
	
	declare @userDataSQL TABLE(UserId uniqueidentifier)
	declare @SQL varchar(1000),@SearchString varchar(50)
	Create table #tmpUser(UserId uniqueidentifier)
	
	Set @SearchString = 'sa'
	SET @SQL = 'insert into #tmpUser(UserId) Select Distinct UserId from ' + @DatabaseName + '.dbo.syusers where Lower(username)=''' + @SearchString + ''''
	--Print @SQL
	exec (@SQL);
	
	
	begin try
		SET @SQL = 'Alter table ' + @DatabaseName + '.dbo.syUsersRolesCampGrps ' + ' drop constraint DF_syUsers_syUsersRolesCampGrpsUserId'
		exec (@SQL);
	end try
	begin catch
	end catch
	
	begin try
		SET @SQL = 'Alter table ' + @DatabaseName + '.dbo.syMRUs ' + ' drop constraint FK_syUsers_syMRUS'
		--Print @SQL
		exec (@SQL);
	end try
	begin catch
	end catch
	
	begin try
		SET @SQL = 'Alter table ' + @DatabaseName + '.dbo.syMRUs ' + ' drop constraint FK_syUsers_syMRUS_UserId'
		--Print @SQL
		exec (@SQL);
	end try
	begin catch
	end catch
	
	begin try
		SET @SQL = 'Alter table ' + @DatabaseName + '.dbo.syMRUs ' + ' drop constraint FK_syMRUS_UserId'
		--Print @SQL
		exec (@SQL);
	end try
	begin catch
	end catch
	
	begin try
		SET @SQL = 'Alter table ' + @DatabaseName + '.dbo.PlStudentsPlaced ' + ' drop constraint DF_syUsers_PlStudentsPlacedPlacementRep'
		exec (@SQL);
	end try
	begin catch
	end catch
	
	begin try
		SET @SQL = 'Alter table ' + @DatabaseName + '.dbo.arClassSections ' + ' drop constraint FK_syUsers_arClassSections_InstructorId'
		exec (@SQL);
	end try
	begin catch
	end catch
	
	begin try
		SET @SQL = 'Alter table ' + @DatabaseName + '.dbo.arGradeScales ' + ' drop constraint FK_syUsers_arGradeScales_InstructorId'
		exec (@SQL);
	end try
	begin catch
	end catch
	
	begin try
		SET @SQL = 'Alter table ' + @DatabaseName + '.dbo.syStudentNotes ' + ' drop constraint FK_syUsers_syStudentNotes_UserId'
		exec (@SQL);
	end try
	begin catch
	end catch
	
	begin try
		SET @SQL = 'Alter table ' + @DatabaseName + '.dbo.arStuEnrollments ' + ' drop constraint DF_syUsers_arStuEnrollmentsFAAdvisorId'
		exec (@SQL);
	end try
	begin catch
	end catch
	
	begin try
		SET @SQL = 'Alter table ' + @DatabaseName + '.dbo.arStuEnrollments ' + ' drop constraint FK_arStuEnrollments_AcademicAdvisor'
		exec (@SQL);
	end try
	begin catch
	end catch
	
	begin try
		SET @SQL = 'Alter table ' + @DatabaseName + '.dbo.arStuEnrollments ' + ' drop constraint FK_arStuEnrollments_syuser_AdmissionsRep'
		exec (@SQL);
	end try
	begin catch
	end catch
	
	begin try
		SET @SQL = 'Alter table ' + @DatabaseName + '.dbo.arStuEnrollments ' + ' drop constraint FK_syUsers_arStuEnrollmentsAcademicAdvisor'
		exec (@SQL);
	end try
	begin catch
	end catch
	
	begin try
		SET @SQL = 'Alter table ' + @DatabaseName + '.dbo.arStuEnrollments ' + ' drop constraint FK_syUsers_arStuEnrollmentsAdmissionsRep'
		exec (@SQL);
	end try
	begin catch
	end catch
	
	begin try
		SET @SQL = 'Alter table ' + @DatabaseName + '.dbo.syStuRestrictions ' + ' drop constraint DF_syStuRestrictions_UserId'
		exec (@SQL);
	end try
	begin catch
	end catch
	
	begin try
		SET @SQL = 'Alter table ' + @DatabaseName + '.dbo.adLeads ' + ' drop constraint FK_syUsers_adLeads'
		exec (@SQL);
	end try
	begin catch
	end catch
	
	begin try
		SET @SQL = 'Alter table ' + @DatabaseName + '.dbo.rptAdmissionsRep ' + ' drop constraint FK_rptAdmissionsRep_syUsers'
		exec (@SQL);
	end try
	begin catch
	end catch
	
	begin try
		SET @SQL = 'Alter table ' + @DatabaseName + '.dbo.saBatchPayments ' + ' drop constraint DF_syUsers_saBatchPaymentsUserId'
		exec (@SQL);
	end try
	begin catch
	end catch
	
	begin try
		SET @SQL = 'Alter table ' + @DatabaseName + '.dbo.saBatchPayments ' + ' drop constraint DF_syUsers_saBatchPaymentsUserId'
		exec (@SQL);
	end try
	begin catch
	end catch
	
	begin try
		SET @SQL = 'Alter table ' + @DatabaseName + '.dbo.arInstructorsSupervisors ' + ' drop constraint FK_arInstructorsSupervisors_syUsers'
		exec (@SQL);
	end try
	begin catch
	end catch
	
	begin try
		SET @SQL = 'Alter table ' + @DatabaseName + '.dbo.arInstructorsSupervisors ' + ' drop constraint FK_arInstructorsSupervisors_syUsers1'
		exec (@SQL);
	end try
	begin catch
	end catch
	
	begin try
		SET @SQL = 'Alter table ' + @DatabaseName + '.dbo.arGrdBkWeights ' + ' drop constraint DF_arGrdBkWeights_InstructorId'
		exec (@SQL);
	end try
	begin catch
	end catch
	
	--declare @UserId uniqueidentifier
	Set @SQL = 'Update ' + @DatabaseName + '.dbo.syUsersRolesCampGrps set UserId=''' + @SystemAdministratorId + '''' + ' where UserId in (select UserId from #tmpUser)'
	--Print @SQL
	exec (@SQL)
	
	Set @SQL = 'Update ' + @DatabaseName + '.dbo.syMRUS set UserId=''' + @SystemAdministratorId + '''' + ' where UserId in (select UserId from #tmpUser)'
	--Print @SQL
	exec (@SQL)
	
	BEGIN TRY
	Set @SQL = 'Update ' + @DatabaseName + '.dbo.syUsers set UserId=''' + @SystemAdministratorId + '''' + ' where UserId in (select UserId from #tmpUser)'
	--Print @SQL
	exec (@SQL)
	END TRY
	BEGIN CATCH
	END CATCH
	
	-- plStudentsPlaced
	BEGIN TRY
	Set @SQL = 'Update ' + @DatabaseName + '.dbo.PlStudentsPlaced set PlacementRep=''' + @SystemAdministratorId + '''' + ' where PlacementRep in (select UserId from #tmpUser)'
	--Print @SQL
	exec (@SQL)
	END TRY
	BEGIN CATCH
	END CATCH
	
	-- arClasssSections
	BEGIN TRY
	Set @SQL = 'Update ' + @DatabaseName + '.dbo.arClassSections set InstructorId=''' + @SystemAdministratorId + '''' + ' where InstructorId in (select UserId from #tmpUser)'
	--Print @SQL
	exec (@SQL)
	END TRY
	BEGIN CATCH
	END CATCH
	
	-- arGradeScales
	BEGIN TRY
	Set @SQL = 'Update ' + @DatabaseName + '.dbo.arGradeScales set InstructorId=''' + @SystemAdministratorId + '''' + ' where InstructorId in (select UserId from #tmpUser)'
	--Print @SQL
	exec (@SQL)
	END TRY
	BEGIN CATCH
	END CATCH
	
	--syStudentNotes
	BEGIN TRY
	Set @SQL = 'Update ' + @DatabaseName + '.dbo.syStudentNotes set UserId=''' + @SystemAdministratorId + '''' + ' where UserId in (select UserId from #tmpUser)'
	--Print @SQL
	exec (@SQL)
	END TRY
	BEGIN CATCH
	END CATCH
	
	--arStuEnrollments
	BEGIN TRY
	Set @SQL = 'Update ' + @DatabaseName + '.dbo.arStuEnrollments set FAAdvisorId=''' + @SystemAdministratorId + '''' + ' where FAAdvisorId in (select UserId from #tmpUser)'
	--Print @SQL
	exec (@SQL)
	END TRY
	BEGIN CATCH
	END CATCH
	
	--arStuEnrollments
	BEGIN TRY
	Set @SQL = 'Update ' + @DatabaseName + '.dbo.arStuEnrollments set AcademicAdvisor=''' + @SystemAdministratorId + '''' + ' where AcademicAdvisor in (select UserId from #tmpUser)'
	--Print @SQL
	exec (@SQL)
	END TRY
	BEGIN CATCH
	END CATCH
	
	--arStuEnrollments
	BEGIN TRY
	Set @SQL = 'Update ' + @DatabaseName + '.dbo.arStuEnrollments set AdmissionsRep=''' + @SystemAdministratorId + '''' + ' where AdmissionsRep in (select UserId from #tmpUser)'
	--Print @SQL
	exec (@SQL)
	END TRY
	BEGIN CATCH
	END CATCH
	
	--syStuRestrictions
	BEGIN TRY
	Set @SQL = 'Update ' + @DatabaseName + '.dbo.syStuRestrictions set UserId=''' + @SystemAdministratorId + '''' + ' where UserId in (select UserId from #tmpUser)'
	--Print @SQL
	exec (@SQL)
	END TRY
	BEGIN CATCH
	END CATCH
	
	--adLeads
	BEGIN TRY
	Set @SQL = 'Update ' + @DatabaseName + '.dbo.adLeads set AdmissionsRep=''' + @SystemAdministratorId + '''' + ' where AdmissionsRep in (select UserId from #tmpUser)'
	--Print @SQL
	exec (@SQL)
	END TRY
	BEGIN CATCH
	END CATCH
	
	--rptAdmissionsRep
	BEGIN TRY
	Set @SQL = 'Update ' + @DatabaseName + '.dbo.rptAdmissionsRep set rptAdmissionsRepId=''' + @SystemAdministratorId + '''' + ' where rptAdmissionsRepId in (select UserId from #tmpUser)'
	--Print @SQL
	exec (@SQL)
	END TRY
	BEGIN CATCH
	END CATCH
	
	--saBatchPayments
	BEGIN TRY
	Set @SQL = 'Update ' + @DatabaseName + '.dbo.saBatchPayments set UserId=''' + @SystemAdministratorId + '''' + ' where UserId in (select UserId from #tmpUser)'
	--Print @SQL
	exec (@SQL)
	END TRY
	BEGIN CATCH
	END CATCH
	
	--arInstructorsSupervisors
	BEGIN TRY
	Set @SQL = 'Update ' + @DatabaseName + '.dbo.arInstructorsSupervisors set InstructorId=''' + @SystemAdministratorId + '''' + ' where InstructorId in (select UserId from #tmpUser)'
	--Print @SQL
	exec (@SQL)
	END TRY
	BEGIN CATCH
	END CATCH
	
	--arInstructorsSupervisors
	BEGIN TRY
	Set @SQL = 'Update ' + @DatabaseName + '.dbo.arInstructorsSupervisors set SupervisorId=''' + @SystemAdministratorId + '''' + ' where SupervisorId in (select UserId from #tmpUser)'
	--Print @SQL
	exec (@SQL)
	END TRY
	BEGIN CATCH
	END CATCH
	
	--arGrdBkWeights
	BEGIN TRY
	Set @SQL = 'Update ' + @DatabaseName + '.dbo.arGrdBkWeights set InstructorId=''' + @SystemAdministratorId + '''' + ' where InstructorId in (select UserId from #tmpUser)'
	--Print @SQL
	exec (@SQL)
	END TRY
	BEGIN CATCH
	END CATCH
end
GO
