SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Proc [dbo].[Usp_AddSupportUsers_ToTenant]
@TenantId int
as
declare @SupportUserId varchar(50)
DECLARE MigrateUsers_Cursor CURSOR LOCAL 
     FORWARD_ONLY 
     FAST_FORWARD 
     READ_ONLY 
     FOR 
		
		Select Distinct UserId from [dbo].[UserCatagory] where IsSupportUser=1
		
		OPEN MigrateUsers_Cursor
		FETCH NEXT FROM MigrateUsers_Cursor into @SupportUserId
		WHILE @@FETCH_STATUS = 0
		BEGIN
			if not exists (select * from [TenantAuthDB].[dbo].[TenantUsers] where UserId=@SupportUserId 
								and TenantId=@TenantId)
			begin
				Insert into [TenantAuthDB].[dbo].[TenantUsers] (TenantId,UserId) 
				values(@TenantId,@SupportUserId)
			end
			FETCH NEXT FROM MigrateUsers_Cursor into @SupportUserId
		END
		CLOSE MigrateUsers_Cursor
		DEALLOCATE MigrateUsers_Cursor
GO
