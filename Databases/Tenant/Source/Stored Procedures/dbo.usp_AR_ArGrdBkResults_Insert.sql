SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/* 
US4257 Course marked as incomplete when score of one of the component is removed

CREATED: 
7/24/2013 TT

PURPOSE: 
Insert arGrdBkResults


MODIFIED:


*/
CREATE PROC [dbo].[usp_AR_ArGrdBkResults_Insert]
	@GrdBkResultId uniqueidentifier,
	@ClsSectionId uniqueidentifier,
	@StuEnrollId uniqueidentifier,
	@InstrGrdBkWgtDetailId uniqueidentifier,
	@Score decimal(6,2),
	@Comments varchar(50),
	@ResNum int = 0,	
	@ModUser varchar(50),
	@IsCompGraded bit
AS
BEGIN

SET NOCOUNT ON
				INSERT INTO arGrdBkResults
						( GrdBkResultId,
						  ClsSectionId,
						  StuEnrollId,
						  InstrGrdBkWgtDetailId,
						  Score,
						  Comments,
						  ResNum,
					      ModUser,
						  ModDate,
						  PostDate,
						  IsCompGraded 
						)
				VALUES  ( @GrdBkResultId, -- GrdBkResultId - uniqueidentifier
						  @ClsSectionId, -- ClsSectionId - uniqueidentifier
						  @StuEnrollId, -- StuEnrollId - uniqueidentifier
						  @InstrGrdBkWgtDetailId , -- InstrGrdBkWgtDetailId- uniqueidentifier
						  @Score, -- Score  - decimal
						  @Comments, -- Comments - varchar
						  @ResNum, -- ResNum - int
						  @ModUser, -- ModUser - varchar
						  GETDATE(),  -- ModDate - datetime
						  GETDATE(),  -- PostDate - datetime
						  @IsCompGraded --IsCompGraded - bit
						)

		UPDATE arResults
		SET	IsCourseCompleted = 0,
			Score = null,
			GrdSysDetailId = null
		WHERE TestId =@ClsSectionId
					and StuEnrollId = @StuEnrollId
		
END	
SET ANSI_NULLS ON
GO
