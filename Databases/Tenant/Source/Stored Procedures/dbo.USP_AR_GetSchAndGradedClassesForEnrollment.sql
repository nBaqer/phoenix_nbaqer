SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Procedure [dbo].[USP_AR_GetSchAndGradedClassesForEnrollment] 
( @StuEnrollId UniqueIdentifier,
  @CampusID UniqueIdentifier
)
as
/*----------------------------------------------------------------------------------------------------
	Author          :	Janet Robinson
    
    Create date		:	06/11/2013
    
	Procedure Name	:	USP_AR_GetSchAndGradedClassesForEnrollment

	Objective		:	Refactoring of Function: GetGradedAndScheduledClassesForEnrollment
						in File Name: TermProgressDB.vb in DataAccess Project
						Query to get the graded and scheduled classes for Student Schedule page 
	
	Parameters		:	Name			Type	Data Type	Required? 	
						=====			====	=========	=========	
						@StuEnrollID	In		Varchar		Required	
						@CampusID		In		Varchar		Required	
						
*/-----------------------------------------------------------------------------------------------------

BEGIN

	-- File Name: TermProgressDB.vb in DataAccess Project
	-- Function: GetGradedAndScheduledClassesForEnrollment
	-- Query to get the graded and scheduled classes
	
	DECLARE @GradesFormat VARCHAR(10) , @GradeRounding VARCHAR(10), @SQL VARCHAR(1000)
		
	SET @GradeRounding = LOWER((SELECT value FROM dbo.syConfigAppSetValues WHERE SettingId = (SELECT SettingId FROM dbo.syConfigAppSettings WHERE KeyName = 'GradeRounding')))
	IF EXISTS(SELECT value FROM dbo.syConfigAppSetValues WHERE campusId = @CampusID AND SettingId = (SELECT SettingId FROM dbo.syConfigAppSettings WHERE KeyName = 'GradesFormat'))
		SET @GradesFormat  = LOWER((SELECT value FROM dbo.syConfigAppSetValues WHERE campusId = @CampusID AND SettingId = (SELECT SettingId FROM dbo.syConfigAppSettings WHERE KeyName = 'GradesFormat')))
	ELSE
		SET @GradesFormat  = LOWER((SELECT value FROM dbo.syConfigAppSetValues WHERE campusId IS NULL AND SettingId = (SELECT SettingId FROM dbo.syConfigAppSettings WHERE KeyName = 'GradesFormat')))

	
	-- SET 1
	SELECT 
		CourseResults.TestId,Class.StartDate,Class.EndDate,
		Course.Descrip,Class.ReqId,Class.ClsSection,Term.TermDescrip, Term.TermId, 
		CASE WHEN @GradesFormat='numeric'
			 THEN 
					CASE WHEN LTRIM(RTRIM(@GradeRounding))='yes'
					THEN
						Convert(varchar(10),Round(CourseResults.Score,0))
					Else
						Convert(varchar(10),CourseResults.Score)
					END 
			 ELSE 
					(SELECT GradeDetails.Grade
					FROM arGradeSystemDetails GradeDetails 
					WHERE GradeDetails.GrdSysDetailId = CourseResults.GrdSysDetailId
					)
			 END AS Grade,
			 (SELECT t7.IsPass 
			  FROM arGradeSystemDetails t7 
			  WHERE CourseResults.GrdSysDetailId = t7.GrdSysDetailId) AS IsPass,
			  Course.Code as Code,Term.StartDate AS TermStart, Course.AllowCompletedCourseRetake
	FROM 
	
		arResults CourseResults INNER JOIN arClassSections Class ON CourseResults.TestId = Class.ClsSectionId
		INNER JOIN arReqs Course ON Class.ReqId = Course.ReqId 
		INNER JOIN arTerm Term ON Class.TermID=Term.TermID
		INNER JOIN arStuEnrollments StudentEnrollment ON CourseResults.StuEnrollId=StudentEnrollment.StuEnrollId 
					AND (Class.ShiftId = StudentEnrollment.ShiftId OR Class.ShiftId is NULL)
	WHERE CourseResults.StuEnrollId = @StuEnrollID 
		 -- To express IF A THEN B ELSE C
		 -- Write ((Not A) or B) AND (A or C)
		 AND
		 (
			-- if GradesFormat = 'numeric' then look at score column
			(Not @GradesFormat = 'numeric')
			OR
			(Course.ReqId not in (select Distinct ReqId from arTransferGrades where StuEnrollId=@StuEnrollID and Score is not null))
		 )
		 AND
		 (
			@GradesFormat = 'numeric'
			OR
			(Course.ReqId not in (select Distinct ReqId from arTransferGrades where StuEnrollId=@StuEnrollID and GrdSysDetailId is not null)) 
		 )
		 /***
	 Union
	 -- SET 2 : Same as 1 except  
			  --Shift  is an optional field in Class Section page, so the following query
			  --will get the classes with no shift available
		   SELECT CourseResults.TestId,Class.StartDate,Class.EndDate,Course.Descrip,
				  Class.ReqId,Class.ClsSection,Term.TermDescrip, Term.TermId, 
				  CASE WHEN @GradesFormat='numeric'
					THEN 
						 --Ross Type Schools don't use the Transfer Instructor Grade Books Results Page
						 --in that case we need to use the score instead of grade.
						CASE WHEN @GradeRounding='yes'
						THEN
							Round(CourseResults.Score,0)
						Else
							CourseResults.Score 
						END 
					ELSE 
						(SELECT GradeDetails.Grade 
						FROM arGradeSystemDetails GradeDetails 
						WHERE CourseResults.GrdSysDetailId = GradeDetails.GrdSysDetailId) 
					END AS Grade,
				 (SELECT t7.IsPass 
				   FROM arGradeSystemDetails t7 
				   WHERE CourseResults.GrdSysDetailId = t7.GrdSysDetailId) AS IsPass,Course.Code as Code,Term.StartDate AS TermStart 
			FROM 
					arResults CourseResults INNER JOIN arClassSections Class ON CourseResults.TestId = Class.ClsSectionId
					INNER JOIN arReqs Course ON Class.ReqId = Course.ReqId 
					INNER JOIN arTerm Term ON Class.TermID=Term.TermID
					INNER JOIN arStuEnrollments StudentEnrollment ON CourseResults.StuEnrollId=StudentEnrollment.StuEnrollId 
			WHERE CourseResults.StuEnrollId = @StuEnrollID 
			 -- To express IF A THEN B ELSE C
			 -- Write ((Not A) or B) AND (A or C)
			 AND
			 (
				-- if GradesFormat = 'numeric' then look at score column
				(Not @GradesFormat = 'numeric')
				OR
				(Course.ReqId not in (select Distinct ReqId from arTransferGrades where StuEnrollId=@StuEnrollID and Score is not null))
			 )
			 AND
			 (
				@GradesFormat = 'numeric'
				OR
				(Course.ReqId not in (select Distinct ReqId from arTransferGrades where StuEnrollId=@StuEnrollID and GrdSysDetailId is not null)) 
			 )
			 **/
	UNION
	SELECT distinct '00000000-0000-0000-0000-000000000000' as ClsSectionId,Term.StartDate,
	Term.EndDate,Course.Descrip,CourseResults.ReqId,'' as ClsSection,Term.TermDescrip,  Term.TermId, 
			CASE WHEN @GradesFormat='numeric'
					THEN 
						 --Ross Type Schools don't use the Transfer Instructor Grade Books Results Page
						 --in that case we need to use the score instead of grade.
						CASE WHEN LTRIM(RTRIM(@GradeRounding))='yes'
					THEN
						Convert(varchar(10),Round(CourseResults.Score,0))
					Else
						Convert(varchar(10),CourseResults.Score)
					END 
					ELSE 
						(SELECT GradeDetails.Grade 
						FROM arGradeSystemDetails GradeDetails 
						WHERE CourseResults.GrdSysDetailId = GradeDetails.GrdSysDetailId) 
			END AS Grade,
			(SELECT t7.IsPass 
						FROM arGradeSystemDetails t7 
						WHERE CourseResults.GrdSysDetailId = t7.GrdSysDetailId) AS IsPass,
			Course.Code as Code,Term.StartDate AS TermStart, Course.AllowCompletedCourseRetake
						FROM arTransferGrades CourseResults INNER JOIN arReqs Course ON CourseResults.ReqId = Course.ReqId
						INNER JOIN arTerm Term ON CourseResults.TermId=Term.TermId
						INNER JOIN arStuEnrollments StudentEnrollment ON CourseResults.StuEnrollId=StudentEnrollment.StuEnrollId
						WHERE CourseResults.StuEnrollId = @StuEnrollID 
	ORDER BY Term.StartDate, Course.Descrip 
	 	
			  
            
END

GO
