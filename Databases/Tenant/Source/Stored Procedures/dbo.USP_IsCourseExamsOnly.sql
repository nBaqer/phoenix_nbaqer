SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[USP_IsCourseExamsOnly]
@CourseCode VARCHAR(50),
@ResourceName VARCHAR(50)
AS
/*----------------------------------------------------------------------------------------------------
	Author          :	Bruce Shanblatt
    
    Create date		:	06/18/2013
    
	Procedure Name	:	USP_IsCourseExamsOnly
	
	Parameters		:	Name			Type	Data Type	            Required? 	
						=====			====	=========	            =========	
						@CourseCode		In		VARCHAR(50)				Required	
						@ResourceName	In		VARCHAR(50)				Required	
						
*/-----------------------------------------------------------------------------------------------------
DECLARE @cnt1 AS INT
DECLARE @cnt2 AS INT

IF 
	(SELECT COUNT(GradeComponentTypes.GrdComponentTypeId) FROM dbo.arGrdComponentTypes GradeComponentTypes
	WHERE GradeComponentTypes.code = @CourseCode) =
	(SELECT COUNT(GradeComponentTypes.GrdComponentTypeId) FROM dbo.arGrdComponentTypes GradeComponentTypes
	INNER JOIN dbo.syResources Resources ON GradeComponentTypes.SysComponentTypeId = Resources.ResourceID
	WHERE GradeComponentTypes.code = @CourseCode AND
	Resources.Resource = @ResourceName)
BEGIN
	SELECT 1
END
ELSE SELECT 0
GO
