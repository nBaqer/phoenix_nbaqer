SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [dbo].[usp_GetModulesCoursesResults]
(@stuEnrollId uniqueidentifier,
 @termDate datetime 
 )
 as
 SET NOCOUNT ON;

           select * from 
            (select tg.StuEnrollId,tg.TermId,tm.TermDescrip,
         
                tg.Score as score,
           
                   tm.StartDate,tm.EndDate,rq.Code,rq.Descrip,tg.ReqId,rq.Credits,rq.FinAidCredits ,'00000000-0000-0000-0000-000000000000' as ClsSectionId, 
             (select Grade from arGradesystemdetails where GrdSysdetailid=tg.GrdSysdetailid) as Grade, 
             (select IsPass from arGradesystemdetails where GrdSysdetailid=tg.GrdSysdetailid) as IsPass, 
             (select IsCreditsAttempted from arGradesystemdetails where GrdSysdetailid=tg.GrdSysdetailid) as IsCreditsAttempted,
             (select IsCreditsEarned from arGradesystemdetails where GrdSysdetailid=tg.GrdSysdetailid) as IsCreditsEarned,
             (select ExpGradDate from arStuEnrollments where stuEnrollid=tg.StuEnrollId) as ExpGradDate ,
              (select distinct Count(GC.Descrip) from arGrdBkWeights GBW,arGrdComponentTypes GC, arGrdBkWgtDetails GD  where 
              GBW.InstrGrdBkWgtId = GD.InstrGrdBkWgtId And GC.GrdComponentTypeId = GD.GrdComponentTypeId 
              and     GBW.ReqId = tg.ReqId and GC.SysComponentTypeID is not null 
              and GC.SysComponentTypeID in 
				(select Distinct ResourceId from syResources where Resource in ('Lab Work','Lab Hours'))) as labCount
							 from arTransferGrades tg, arTerm tm, arReqs rq 
							where tg.StuEnrollId=@stuEnrollId and
							tg.TermId=tm.TermId 
							and tg.ReqId=rq.ReqId 
							and (tm.StartDate <= @termDate)  
							AND tg.IsCourseCompleted = 1
           

             UNION 

            select ar.StuEnrollId,cs.TermId,tm2.TermDescrip,
            ar.Score as score,
           
                   tm2.StartDate,tm2.EndDate,rq2.Code,rq2.Descrip,cs.ReqId,rq2.Credits,rq2.FinAidCredits  ,cs.ClsSectionid, 
             (select Grade from arGradesystemdetails where GrdSysdetailid=ar.GrdSysdetailid) as Grade, 
             (select IsPass from arGradesystemdetails where GrdSysdetailid=ar.GrdSysdetailid) as IsPass, 
             (select IsCreditsAttempted from arGradesystemdetails where GrdSysdetailid=ar.GrdSysdetailid) as IsCreditsAttempted,
             (select IsCreditsEarned from arGradesystemdetails where GrdSysdetailid=ar.GrdSysdetailid) as IsCreditsEarned ,
             (select ExpGradDate from arStuEnrollments where stuEnrollid=ar.StuEnrollId) as ExpGradDate,
                  (       select distinct Count(GC.Descrip) from arGrdBkWeights GBW,arGrdComponentTypes GC, arGrdBkWgtDetails GD  where 
              GBW.InstrGrdBkWgtId = GD.InstrGrdBkWgtId And GC.GrdComponentTypeId = GD.GrdComponentTypeId 
              and     GBW.ReqId = cs.ReqId and GC.SysComponentTypeID is not null 
              and GC.SysComponentTypeID in 
				(select Distinct ResourceId from syResources where Resource in ('Lab Work','Lab Hours'))) as labCount
							 from arResults ar, arClassSections cs, arTerm tm2, arReqs rq2 
							where ar.StuEnrollId=@stuEnrollId and
							ar.TestId=cs.ClsSectionId 
							and cs.TermId=tm2.TermId 
							and cs.ReqId=rq2.ReqId 
							and (tm2.StartDate <= @termDate)  
							AND ar.IsCourseCompleted = 1
           

            ) P 
            WHERE EXISTS (SELECT DISTINCT arStuEnrollments.StuEnrollId 
                          FROM arStuEnrollments, arStudent A, syCampuses C, syCmpGrpCmps,syCampGrps  
                          WHERE arStuEnrollments.StudentId=A.StudentId 
                          AND arStuEnrollments.CampusId=C.CampusId 
                          AND arStuEnrollments.StuEnrollId=P.StuEnrollId 
                          AND syCmpGrpCmps.CampusId = arStuEnrollments.CampusId 
                          AND syCampGrps.CampGrpId=syCmpGrpCmps.CampGrpId 
                           ) 
             order by stuEnrollId,StartDate,Grade desc,Score desc 


GO
