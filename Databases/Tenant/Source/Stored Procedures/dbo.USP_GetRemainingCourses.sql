SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


----------------------------------------------------------------------------------------------------------------------------------------------------
--End added by Troy R on 6/28/2013
--DE9727: QA: Transcript clock hour report has an incorrect display of course results when they have only a final grade.
-------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------
--Balaji start DE9785,DE9783, DE9768, DE9762
----------------------------------------------------
CREATE PROC [dbo].[USP_GetRemainingCourses]
@stuenrollid UNIQUEIDENTIFIER,
@termid UNIQUEIDENTIFIER
AS
/*----------------------------------------------------------------------------------------------------
	Author          :	Bruce Shanblatt
    
    Create date		:	06/18/2013
    
	Procedure Name	:	USP_GetRemainingCourses
	
	Parameters		:	Name			Type	Data Type	            Required? 	
						=====			====	=========	            =========	
						@stuenrollid	In		UNIQUEIDENTIFIER		Required	
						@termid		    In		UNIQUEIDENTIFIER		Required	
						
*/-----------------------------------------------------------------------------------------------------
-- Section 1 : Get the list of student's courses offered for the selected term and currently enrolled program


--declare @StuEnrollId uniqueidentifier, @TermId uniqueidentifier

--Set @StuEnrollId = '62ADD563-C9FA-4072-B281-43240579BAA3'
--Set @TermId = '00000000-0000-0000-0000-000000000000'

--declare @StuEnrollId uniqueidentifier, @TermId uniqueidentifier

--Set @StuEnrollId ='62ADD563-C9FA-4072-B281-43240579BAA3'
--Set @TermId = '00000000-0000-0000-0000-000000000000'

--Select * from arResults 
--Select * from arStuEnrollments where StudentId in (Select StudentId from arStudent where firstname='QATest05')

Declare @ListCoursesOfferedForTermAndProgramStudentIsEnrolledIn Table
			(StuEnrollId uniqueidentifier, StartDate datetime,ExpStartDate datetime,
			LastName varchar(50),FirstName varchar(50),ExpGradDate datetime,
			PrgVerId uniqueidentifier, PrgVerDescrip varchar(50),
			ReqId uniqueidentifier, Req varchar(50), Code varchar(50),
			Credits decimal(18,2), Hours decimal(18,2),
			AllowCompletedCourseRetake bit,
			IsRequired varchar(10),CampDescrip varchar(50),
			Override UNIQUEIDENTIFIER
			--,TermDescrip varchar(50),TermStartDate datetime,TermEndDate datetime
			)

-- Section 2: Get the list of student's previous attempts in the currently enrolled program
Declare @ListCoursesStudentHasAlreadyAttempted Table
		(
			CourseId uniqueidentifier,
			Code varchar(50),
			CourseDescription varchar(50),
			StuEnrollId uniqueidentifier,
			IsPass BIT,
			TermDescrip VARCHAR(50)
		)

-- Section 3: Get the list of courses student is currently scheduled in		
Declare @ListCoursesStudentIsCurrentlyRegisteredIn Table
		(
			CourseId uniqueidentifier,
			Code varchar(50),
			CourseDescription varchar(50),
			TermDescrip VARCHAR(50)
		)
		


-- Section 1: List of courses offered for the term and program the student is currently enrolled in
Insert into @ListCoursesOfferedForTermAndProgramStudentIsEnrolledIn
SELECT * FROM ( 
   SELECT DISTINCT 
	StudentEnrollment.StuEnrollId,
	StudentEnrollment.StartDate,
	StudentEnrollment.ExpStartDate,
	Student.LastName,
	Student.FirstName,
	StudentEnrollment.ExpGradDate,
	StudentEnrollment.PrgVerId,
	ProgramVersions.PrgVerDescrip,
	ProgramVersionDefinitions.ReqId as ReqId,
	Requirements.Descrip As Req,
	Requirements.Code As Code,
	Requirements.Credits,
	Requirements.Hours,
	Requirements.AllowCompletedCourseRetake,
	IsRequired = (CASE ProgramVersionDefinitions.IsRequired WHEN 0 THEN 'False' ELSE 'True' END),
	(select CampDescrip from syCampuses where CampusId=StudentEnrollment.CampusId) as CampDescrip,
	isnull(ProgramVersionDefinitions.GrdSysDetailId,'00000000-0000-0000-0000-000000000000') as OVERRIDE
	--Term.TermDescrip,Term.StartDate as TermStartDate,Term.EndDate as TermEndDate
	FROM 
	arStuEnrollments StudentEnrollment
	INNER JOIN arStudent Student ON StudentEnrollment.StudentID = Student.StudentID
	INNER JOIN arPrgVersions ProgramVersions ON StudentEnrollment.PrgVerID = ProgramVersions.PrgVerID
	INNER JOIN arProgVerDef ProgramVersionDefinitions ON ProgramVersions.PrgVerID = ProgramVersionDefinitions.PrgVerID
	INNER JOIN arReqs Requirements ON ProgramVersionDefinitions.ReqId = Requirements.ReqId
	INNER JOIN syStatuses Statuses ON Requirements.StatusId = Statuses.StatusId
	INNER JOIN arClassSections ClassSections ON Requirements.ReqId = ClassSections.ReqId
	INNER JOIN dbo.arTerm Term ON ClassSections.TermId = Term.TermId
	WHERE  
	Requirements.ReqTypeId=1 AND 
	StudentEnrollment.StuEnrollId=@stuenrollid AND 
	(@TermId = '00000000-0000-0000-0000-000000000000' or ClassSections.Termid in(@termid)) 
   UNION 
   SELECT DISTINCT    
	StudentEnrollment.StuEnrollId,
	StudentEnrollment.StartDate,
	StudentEnrollment.ExpStartDate,
	Student.LastName, 
	Student.FirstName,
	StudentEnrollment.ExpGradDate,
	StudentEnrollment.PrgVerId,
	ProgramVersions.PrgVerDescrip,
	RequirementGroupDefinitions.ReqId as ReqId,
	Requirements.Descrip As Req,
	Requirements.Code As Code,
	Requirements.Credits,
	Requirements.Hours,
	Requirements.AllowCompletedCourseRetake,
	IsRequired = (CASE ProgramVersionDefinitions.IsRequired WHEN 0 THEN 'False' ELSE 'True' END),
	(select CampDescrip from syCampuses where CampusId=StudentEnrollment.CampusId) as CampDescrip,
	isnull(ProgramVersionDefinitions.GrdSysDetailId,'00000000-0000-0000-0000-000000000000') as Override
	--Term.TermDescrip,Term.StartDate as TermStartDate,Term.EndDate as TermEndDate
    FROM 
    arStuEnrollments StudentEnrollment
    INNER JOIN arStudent Student ON StudentEnrollment.StudentID = Student.StudentID
    INNER JOIN arPrgVersions ProgramVersions ON	StudentEnrollment.PrgVerId = ProgramVersions.PrgVerId	
    INNER JOIN arProgVerDef ProgramVersionDefinitions ON ProgramVersions.PrgVerID = ProgramVersionDefinitions.PrgVerID 	
    INNER JOIN arReqGrpDef RequirementGroupDefinitions ON ProgramVersionDefinitions.ReqId=RequirementGroupDefinitions.GrpId  
    INNER JOIN arReqs Requirements ON RequirementGroupDefinitions.ReqId = Requirements.ReqId
    INNER JOIN syStatuses Statuses ON Requirements.StatusId = Statuses.StatusId
    INNER JOIN arClassSections ClassSections ON	Requirements.Reqid = ClassSections.ReqId 
    INNER JOIN dbo.arTerm Term ON ClassSections.TermId = Term.TermId
    WHERE 
    Requirements.ReqTypeId=1 AND 
    StudentEnrollment.StuEnrollId=@stuenrollid AND     
    (@TermId = '00000000-0000-0000-0000-000000000000' or ClassSections.Termid in(@termid)) 
   UNION 
   SELECT DISTINCT 
    StudentEnrollment.StuEnrollId,
    StudentEnrollment.StartDate,
    StudentEnrollment.ExpStartDate,
    Student.LastName,
    Student.FirstName,
    StudentEnrollment.ExpGradDate,
    StudentEnrollment.PrgVerId,
    ProgramVersions.PrgVerDescrip,
    RequirementGroupDefinitions_1.ReqId as ReqId,
    Requirements.Descrip As Req,
    Requirements.Code As Code,
    Requirements.Credits,
    Requirements.Hours,
    Requirements.AllowCompletedCourseRetake,
    IsRequired = (CASE ProgramVersionDefinitions.IsRequired WHEN 0 THEN 'False' ELSE 'True' END), 
    (select CampDescrip from syCampuses where CampusId=StudentEnrollment.CampusId) as CampDescrip,
    isnull(ProgramVersionDefinitions.GrdSysDetailId,'00000000-0000-0000-0000-000000000000') as Override
    --Term.TermDescrip,Term.StartDate as TermStartDate,Term.EndDate as TermEndDate
    FROM
    arStuEnrollments StudentEnrollment
    INNER JOIN arStudent Student ON StudentEnrollment.StudentID = Student.StudentID
    INNER JOIN arPrgVersions ProgramVersions ON	StudentEnrollment.PrgVerId = ProgramVersions.PrgVerId	
    INNER JOIN arProgVerDef ProgramVersionDefinitions ON ProgramVersions.PrgVerID = ProgramVersionDefinitions.PrgVerID 
    INNER JOIN arReqGrpDef RequirementGroupDefinitions ON ProgramVersionDefinitions.ReqId=RequirementGroupDefinitions.GrpId 
    INNER JOIN arReqGrpDef RequirementGroupDefinitions_1 ON RequirementGroupDefinitions.ReqId=RequirementGroupDefinitions_1.GrpId
    INNER JOIN arReqs Requirements ON RequirementGroupDefinitions.ReqId = Requirements.ReqId
    INNER JOIN syStatuses Statuses ON Requirements.StatusId = Statuses.StatusId
    INNER JOIN arClassSections ClassSections ON	Requirements.Reqid = ClassSections.ReqId
    INNER JOIN dbo.arTerm Term ON ClassSections.TermId = Term.TermId    
    WHERE
    Requirements.ReqTypeId=1 AND 
    StudentEnrollment.StuEnrollId=@stuenrollid AND    
    (@TermId = '00000000-0000-0000-0000-000000000000' or ClassSections.Termid in(@termid)) 
   UNION 
   SELECT DISTINCT 
    StudentEnrollment.StuEnrollId,
    StudentEnrollment.StartDate,
    StudentEnrollment.ExpStartDate,
    Student.LastName,
    Student.FirstName,
    StudentEnrollment.ExpGradDate,
    StudentEnrollment.PrgVerId,
    ProgramVersions.PrgVerDescrip,
    RequirementGroupDefinitions_2.ReqId as ReqId,
    Requirements.Descrip As Req,
    Requirements.Code As Code,
    Requirements.Credits,
    Requirements.Hours,
    Requirements.AllowCompletedCourseRetake,
    IsRequired = (CASE ProgramVersionDefinitions.IsRequired WHEN 0 THEN 'False' ELSE 'True' END), 
    (select CampDescrip from syCampuses where CampusId=StudentEnrollment.CampusId) as CampDescrip,
    isnull(ProgramVersionDefinitions.GrdSysDetailId,'00000000-0000-0000-0000-000000000000') as Override
    --Term.TermDescrip,Term.StartDate as TermStartDate,Term.EndDate as TermEndDate
    FROM 
    arStuEnrollments StudentEnrollment
    INNER JOIN arStudent Student ON StudentEnrollment.StudentID = Student.StudentID
    INNER JOIN arPrgVersions ProgramVersions ON	StudentEnrollment.PrgVerId = ProgramVersions.PrgVerId	
    INNER JOIN arProgVerDef ProgramVersionDefinitions ON StudentEnrollment.PrgVerId=ProgramVersionDefinitions.PrgVerId
    INNER JOIN arReqGrpDef RequirementGroupDefinitions ON ProgramVersionDefinitions.ReqId=RequirementGroupDefinitions.GrpId
    INNER JOIN arReqGrpDef RequirementGroupDefinitions_1 ON RequirementGroupDefinitions.ReqId=RequirementGroupDefinitions_1.GrpId
    INNER JOIN arReqs Requirements ON RequirementGroupDefinitions.ReqId = Requirements.ReqId
    INNER JOIN arReqGrpDef RequirementGroupDefinitions_2 ON Requirements.ReqId=RequirementGroupDefinitions_2.ReqId   
    INNER JOIN syStatuses Statuses ON Requirements.StatusId = Statuses.StatusId
    INNER JOIN arClassSections ClassSections ON	Requirements.Reqid = ClassSections.ReqId
    INNER JOIN dbo.arTerm Term ON ClassSections.TermId = Term.TermId     
    WHERE 
    Requirements.ReqTypeId=1 AND 
    StudentEnrollment.StuEnrollId=@stuenrollid AND    
    (@TermId = '00000000-0000-0000-0000-000000000000' or ClassSections.Termid in(@termid)) 
   UNION 
    SELECT DISTINCT 
    StudentEnrollment.StuEnrollId,
    StudentEnrollment.StartDate,
    StudentEnrollment.ExpStartDate,
    Student.LastName,
    Student.FirstName,
    StudentEnrollment.ExpGradDate,
    StudentEnrollment.PrgVerId,
    ProgramVersions.PrgVerDescrip,
    RequirementGroupDefinitions_3.ReqId as ReqId,
    Requirements.Descrip As Req,
    Requirements.Code As Code,
    Requirements.Credits,
    Requirements.Hours,
    Requirements.AllowCompletedCourseRetake,
    IsRequired = (CASE ProgramVersionDefinitions.IsRequired WHEN 0 THEN 'False' ELSE 'True' END), 
    (select CampDescrip from syCampuses where CampusId=StudentEnrollment.CampusId) as CampDescrip, 
    isnull(ProgramVersionDefinitions.GrdSysDetailId,'00000000-0000-0000-0000-000000000000') as Override
    --Term.TermDescrip,Term.StartDate as TermStartDate,Term.EndDate as TermEndDate
    FROM arStuEnrollments StudentEnrollment
    INNER JOIN arStudent Student ON StudentEnrollment.StudentID = Student.StudentID
    INNER JOIN arPrgVersions ProgramVersions ON StudentEnrollment.PrgVerId = ProgramVersions.PrgVerId
    INNER JOIN arProgVerDef ProgramVersionDefinitions ON StudentEnrollment.PrgVerId=ProgramVersionDefinitions.PrgVerId
    INNER JOIN arReqGrpDef RequirementGroupDefinitions ON ProgramVersionDefinitions.ReqId=RequirementGroupDefinitions.GrpId
    INNER JOIN arReqGrpDef RequirementGroupDefinitions_1 ON RequirementGroupDefinitions.ReqId=RequirementGroupDefinitions_1.GrpId
    INNER JOIN arReqGrpDef RequirementGroupDefinitions_2 ON RequirementGroupDefinitions_1.ReqId=RequirementGroupDefinitions_2.GrpId
    INNER JOIN arReqGrpDef RequirementGroupDefinitions_3 ON RequirementGroupDefinitions_2.ReqId=RequirementGroupDefinitions_3.GrpId
    INNER JOIN arReqs Requirements ON Requirements.ReqId=RequirementGroupDefinitions_3.ReqId
    INNER JOIN syStatuses Statuses ON Requirements.StatusId = Statuses.StatusId
    INNER JOIN arClassSections ClassSections ON	Requirements.Reqid = ClassSections.ReqId
    INNER JOIN dbo.arTerm Term ON ClassSections.TermId = Term.TermId    
    WHERE   
    Requirements.ReqTypeId=1 AND 
    StudentEnrollment.StuEnrollId=@stuenrollid AND    
    (@TermId = '00000000-0000-0000-0000-000000000000' or ClassSections.Termid in(@termid)) 
   UNION  
    SELECT DISTINCT 
    StudentEnrollment.StuEnrollId,
    StudentEnrollment.StartDate,
    StudentEnrollment.ExpStartDate, 
    Student.LastName, 
    Student.FirstName, 
    StudentEnrollment.ExpGradDate, 
    StudentEnrollment.PrgVerId,
    ProgramVersions.PrgVerDescrip,
    RequirementGroupDefinitions_4.ReqId as ReqId, 
    Requirements.Descrip As Req, 
    Requirements.Code As Code,
    Requirements.Credits,
    Requirements.Hours,
    Requirements.AllowCompletedCourseRetake,
    IsRequired = (CASE ProgramVersionDefinitions.IsRequired WHEN 0 THEN 'False' ELSE 'True' END), 
    (select CampDescrip from syCampuses where CampusId=StudentEnrollment.CampusId) as CampDescrip,
    isnull(ProgramVersionDefinitions.GrdSysDetailId,'00000000-0000-0000-0000-000000000000') as Override
    --Term.TermDescrip,Term.StartDate as TermStartDate,Term.EndDate as TermEndDate
    FROM 
    arStuEnrollments StudentEnrollment --t100,
    INNER JOIN arStudent Student ON StudentEnrollment.StudentID = Student.StudentID 
    INNER JOIN arPrgVersions ProgramVersions ON StudentEnrollment.PrgVerId = ProgramVersions.PrgVerId
    INNER JOIN arProgVerDef ProgramVersionDefinitions ON StudentEnrollment.PrgVerId=ProgramVersionDefinitions.PrgVerId
    INNER JOIN arReqGrpDef RequirementGroupDefinitions ON ProgramVersionDefinitions.ReqId=RequirementGroupDefinitions.GrpId
    INNER JOIN arReqGrpDef RequirementGroupDefinitions_1 ON RequirementGroupDefinitions.ReqId=RequirementGroupDefinitions_1.GrpId
    INNER JOIN arReqGrpDef RequirementGroupDefinitions_2 ON RequirementGroupDefinitions_1.ReqId=RequirementGroupDefinitions_2.GrpId
    INNER JOIN arReqGrpDef RequirementGroupDefinitions_3 ON RequirementGroupDefinitions_2.ReqId=RequirementGroupDefinitions_3.GrpId
    INNER JOIN arReqGrpDef RequirementGroupDefinitions_4 ON RequirementGroupDefinitions_3.ReqId=RequirementGroupDefinitions_4.GrpId
    INNER JOIN arReqs Requirements ON Requirements.ReqId=RequirementGroupDefinitions_4.ReqId
    INNER JOIN syStatuses Statuses ON Requirements.StatusId = Statuses.StatusId
    INNER JOIN arClassSections ClassSections ON	Requirements.Reqid = ClassSections.ReqId   
    INNER JOIN dbo.arTerm Term ON ClassSections.TermId = Term.TermId   
    WHERE   
    Requirements.ReqTypeId=1 AND 
    StudentEnrollment.StuEnrollId=@stuenrollid AND    
     (@TermId = '00000000-0000-0000-0000-000000000000' or ClassSections.Termid in(@termid)) 
     ) P 
     
 -- Section 2: This section gets the list of courses student has already attempted 
 Insert into @ListCoursesStudentHasAlreadyAttempted
 SELECT Distinct ReqId,Code,Descrip,StuEnrollId,IsPass,TermDescrip FROM 
        (SELECT DISTINCT 
		 ClassSections.TermId, 
		 Terms.TermDescrip,
		 ClassSections.ReqId,
		 Requirements.Code,
		 Requirements.Descrip,
		 Requirements.Credits,
		 Requirements.Hours,
		 Requirements.AllowCompletedCourseRetake,
		 Terms.StartDate,
		 Terms.EndDate,
		 Results.GrdSysDetailId, 
		 (SELECT Grade FROM arGradeSystemDetails WHERE GrdSysDetailId=Results.GrdSysDetailId)as Grade, 
		 CASE 
		 WHEN 
			(SELECT GrdSysDetailId from arProgVerDef where ReqId=ClassSections.ReqId and PrgVerId=StudentEnrollment.PrgVerId) is not null then 
		 (0) 
		 ELSE 
			(SELECT IsPass FROM arGradeSystemDetails WHERE GrdSysDetailId=Results.GrdSysDetailId ) 
		 END AS ISPass, 
	     (SELECT GPA FROM arGradeSystemDetails WHERE GrdSysDetailId=Results.GrdSysDetailId) as GPA, 
         (SELECT IsCreditsAttempted FROM arGradeSystemDetails WHERE GrdSysDetailId=Results.GrdSysDetailId) as IsCreditsAttempted, 
         (SELECT IsCreditsEarned FROM arGradeSystemDetails WHERE GrdSysDetailId=Results.GrdSysDetailId) as IsCreditsEarned, 
         (SELECT IsInGPA FROM arGradeSystemDetails WHERE GrdSysDetailId=Results.GrdSysDetailId) as IsInGPA,Results.StuEnrollId 
		 FROM
		 arResults Results --t1,
		 INNER JOIN	arClassSections ClassSections ON Results.TestId=ClassSections.ClsSectionId
		 INNER JOIN	arTerm Terms ON ClassSections.TermId=Terms.TermId
		 INNER JOIN	arReqs Requirements ON ClassSections.ReqId=Requirements.ReqId
		 INNER JOIN arStuEnrollments StudentEnrollment on Results.StuEnrollId = StudentEnrollment.StuEnrollId		 
		 WHERE		 
		 Results.GrdSysDetailId IS NOT NULL AND  -- Checking to see if student is currently registered in a course
		 Results.StuEnrollId=@stuenrollid
         UNION 
         SELECT DISTINCT 
         TransferGrades.TermId,
         Terms.TermDescrip,
         TransferGrades.ReqId,
         Requirements.Code,
         Requirements.Descrip,
         Requirements.Credits,
         Requirements.Hours,
         Requirements.AllowCompletedCourseRetake,
         Terms.StartDate,
         Terms.EndDate,
         TransferGrades.GrdSysDetailId, 
         (SELECT Grade FROM arGradeSystemDetails WHERE GrdSysDetailId=TransferGrades.GrdSysDetailId) as Grade, 
		 CASE
		 WHEN 
			(SELECT GrdSysDetailId from arProgVerDef where ReqId=Requirements.ReqId and PrgVerId=StudentEnrollment.PrgVerId) is not null then 
		 (0) 
		 ELSE 
			(SELECT IsPass FROM arGradeSystemDetails WHERE GrdSysDetailId=TransferGrades.GrdSysDetailId ) 
		 END AS ISPass, 
         (SELECT GPA FROM arGradeSystemDetails WHERE GrdSysDetailId=TransferGrades.GrdSysDetailId) as GPA, 
         (SELECT IsCreditsAttempted FROM arGradeSystemDetails WHERE GrdSysDetailId=TransferGrades.GrdSysDetailId) as IsCreditsAttempted, 
         (SELECT IsCreditsEarned FROM arGradeSystemDetails WHERE GrdSysDetailId=TransferGrades.GrdSysDetailId) as IsCreditsEarned, 
         (SELECT IsInGPA FROM arGradeSystemDetails WHERE GrdSysDetailId=TransferGrades.GrdSysDetailId) as IsInGPA,
         TransferGrades.StuEnrollId 
		 FROM 
		 arTransferGrades TransferGrades --t10,
		 INNER JOIN arTerm Terms ON TransferGrades.TermId=Terms.TermId
		 INNER JOIN arReqs Requirements ON TransferGrades.ReqId=Requirements.ReqId		
		 INNER JOIN arStuEnrollments StudentEnrollment on TransferGrades.StuEnrollId = StudentEnrollment.StuEnrollId		 
         WHERE        
         TransferGrades.GrdSysDetailId IS NOT NULL AND -- Checking to see if student is currently registered in a transferred course
         TransferGrades.StuEnrollId=@stuenrollid
   UNION 
   SELECT DISTINCT 
    ClassSections.TermId,
	Terms.TermDescrip,
	S.ReqId,
	Requirements.Code,
	Requirements.Descrip,
	Requirements.Credits, 
	Requirements.Hours,
	Requirements.AllowCompletedCourseRetake,
	Terms.StartDate,
	Terms.EndDate,
	Results.GrdSysDetailId, 
	(Select Grade from arGradeSystemDetails where GrdSysDetailId = Results.GrdSysDetailId)as Grade,    
	(Select IsPass from arGradeSystemDetails where GrdSysDetailId = Results.GrdSysDetailId) as IsPass,       
	(Select isnull(GPA,0) from arGradeSystemDetails WHERE GrdSysDetailId = Results.GrdSysDetailId) as GPA,
	(Select IsCreditsAttempted from arGradeSystemDetails WHERE GrdSysDetailId = Results.GrdSysDetailId) as IsCreditsAttempted,
	(Select IsCreditsEarned from arGradeSystemDetails WHERE	GrdSysDetailId = Results.GrdSysDetailId) as IsCreditsEarned,
	(Select IsInGPA from arGradeSystemDetails WHERE	GrdSysDetailId = Results.GrdSysDetailId) as IsInGPA,  
	Results.stuEnrollId 
   FROM 
	( -- get equivalent courses
	SELECT distinct 
	CourseEquivalent.ReqId as EqReqId,
	Requirements.ReqId,
	StudentEnrollment.StuEnrollId  
	FROM 
	arStuEnrollments StudentEnrollment --t100,
	INNER JOIN arStudent Student ON StudentEnrollment.StudentId = Student.StudentId
	INNER JOIN arPrgVersions ProgramVersions ON StudentEnrollment.PrgVerId = ProgramVersions.PrgVerId  
	INNER JOIN arProgVerDef ProgramVersionDefinitions ON StudentEnrollment.PrgVerId = ProgramVersionDefinitions.PrgVerId
	INNER JOIN arReqs Requirements ON Requirements.ReqId = ProgramVersionDefinitions.ReqId 	
	INNER JOIN arCourseEquivalent CourseEquivalent ON Requirements.Reqid=CourseEquivalent.EquivReqId 
	WHERE 
	StudentEnrollment.StuEnrollId = @stuenrollid AND 
	-- Checking to see if student already has not taken equivalent courses
	Requirements.Reqid not in 
		(SELECT ReqId 
		FROM 
		arResults Results
		INNER JOIN arClassSections ClassSections ON Results.TestId = ClassSections.ClsSectionId		
		WHERE 
        StuEnrollId = @stuenrollid AND 
        ReqId=ProgramVersionDefinitions.ReqId)) 
    S, 
    arResults Results, --t1, 
    arReqs Requirements, --t2, 
	arTerm Terms, --t3, 
	arClassSections ClassSections, -- t4, 
	arClassSectionTerms ClassSectionTerms, --t5, 
	arStuEnrollments StudentEnrollment --t6 
	WHERE  Results.TestId = ClassSections.ClsSectionId AND 
	ClassSections.ClsSectionId = ClassSectionTerms.ClsSectionId AND 
	ClassSectionTerms.TermId = Terms.TermId AND 
	ClassSections.ReqId = Requirements.ReqId AND 
    Results.StuEnrollId = StudentEnrollment.StuEnrollId AND
    Results.GrdSysDetailId Is Not null AND --Graded courses
    Results.StuEnrollId = S.StuEnrollId AND
    Requirements.Reqid=S.EqReqId 
) A 
  WHERE 
  (A.StuEnrollId = @stuEnrollId
  -- AND  P.ReqId=A.ReqId  -- don't need to look at first table variable, we just need all failed courses attempted earlier
  -- AND A.IsPass=0   -- If don't have to check if student passed or failed
  AND A.AllowCompletedCourseRetake= 0 -- If course doesn't allow retakes ignore completed courses
  AND A.IsPass=1)
  --OR  --OR condition added for DE9783
  --(A.StuEnrollId = @stuEnrollId
  --AND A.IsPass=0
  --AND A.AllowCompletedCourseRetake= 1)
  
 -- Section 3: Ignore courses the student currently scheduled in
 Insert into @ListCoursesStudentIsCurrentlyRegisteredIn
 SELECT Distinct Courses.ReqId,Courses.Code,Courses.Descrip,Term.TermDescrip
  FROM 
  arResults CourseResults INNER JOIN arClassSections Class ON CourseResults.TestId=Class.ClsSectionId 
  INNER JOIN arTerm Term ON Class.TermId=Term.TermId 
  INNER JOIN syStatuses Statuses ON Term.StatusId=Statuses.StatusId
  INNER JOIN arReqs Courses ON Class.ReqId=Courses.ReqId
WHERE 
	CourseResults.StuEnrollId=@stuenrollid  AND 
	CourseResults.GrdSysDetailId IS NULL 
	--P.ReqId=Class.ReqId -- don't need to look at first table variable
  

--/**   
--Select * from @ListCoursesOfferedForTermAndProgramStudentIsEnrolledIn
--Select * from @ListCoursesStudentHasAlreadyAttempted
--Select * from @ListCoursesStudentIsCurrentlyRegisteredIn
--**/

---- Final Resultset

Select DISTINCT * from @ListCoursesOfferedForTermAndProgramStudentIsEnrolledIn
Where 
	ReqId not in (Select CourseId from @ListCoursesStudentHasAlreadyAttempted) --Ignore attempted courses
	AND
	ReqId not in (select CourseId from @ListCoursesStudentIsCurrentlyRegisteredIn) -- Ignore Scheduled courses
	--and Code='BUS101'
ORDER BY CampDescrip,Code,Req,PrgVerDescrip
GO
