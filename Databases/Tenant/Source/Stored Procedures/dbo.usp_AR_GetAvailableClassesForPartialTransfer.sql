SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/* 
US4212 Transfer Partially Completed Components 

CREATED: 
7/8/2013 WMP

PURPOSE: 
Select classes available for a student to transfer partially completed components
	Requirements:
	- same course but different class section
	- current or future term
	- seats remaining
	- no student scheduling conflicts (handled in business rules, not this stored proc)

MODIFIED:
7/24/2013 WMP	DE9885	verify that student and class shifts match
7/26/2013 WMP	DE9908	update remaining spaces calculation

*/

CREATE PROC [dbo].[usp_AR_GetAvailableClassesForPartialTransfer]
	@ResultId uniqueIdentifier =  null
AS
BEGIN

	DECLARE @StuEnrollId as uniqueIdentifier
	DECLARE @ReqId as uniqueIdentifier
	DECLARE @CurrentClsSectionId as uniqueIdentifier
	DECLARE @StudentEnrollmentShiftId as uniqueIdentifier
	
	SELECT @ReqId = c.ReqId, @CurrentClsSectionId = r.TestId, @StuEnrollId = r.StuEnrollId, @StudentEnrollmentShiftId = e.ShiftId 
	FROM arResults r
		INNER JOIN arClassSections c ON c.ClsSectionId = r.TestId
		INNER JOIN arStuEnrollments e ON e.StuEnrollId = r.StuEnrollId
	WHERE r.ResultId = @ResultId
	
	
	SELECT
		cs.ClsSectionId, cs.ClsSection, rq.Code as CourseCode, cs.MaxStud as MaxStudents,
		(select COUNT(0) from arResults rs where rs.TestId=cs.ClsSectionId) as Assigned,
		cs.MaxStud - (select COUNT(0) from arResults rs where rs.TestId=cs.ClsSectionId) as Remaining,
		cs.StartDate as ClsSectionStartDate, cs.EndDate as ClsSectionEndDate, @StuEnrollId as StuEnrollId,
		t.TermCode, t.TermDescrip
	FROM
		arClassSections cs
		INNER JOIN arReqs rq ON rq.ReqId = cs.ReqId
		INNER JOIN arTerm t ON t.TermId = cs.TermId
	WHERE
		cs.ReqId = @ReqId AND cs.ClsSectionId <> @CurrentClsSectionId 
		AND IsNull(cs.ShiftId,'00000000-0000-0000-0000-000000000000') = IsNull(@StudentEnrollmentShiftId,'00000000-0000-0000-0000-000000000000')
		AND cs.EndDate >= GETDATE()	
		AND (cs.MaxStud - (select COUNT(0) from arResults rs where rs.TestId=cs.ClsSectionId)) > 0
	ORDER BY
		cs.StartDate, cs.ClsSection

END
GO
