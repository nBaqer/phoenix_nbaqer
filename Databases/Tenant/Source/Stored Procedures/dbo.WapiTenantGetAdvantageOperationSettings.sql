SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- ================================================================================
-- Author:		Jose Alfredo
-- Create date: 4/15/2014
-- Description:	This get the internal setting 
--              for WAPI services. it use only 
--              for WAPI
-- Modification 10/28/2016: Environment and Tenant Access tables are not more used!
-- 
-- ================================================================================
CREATE PROCEDURE [dbo].[WapiTenantGetAdvantageOperationSettings]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT 
	('server= ' + t.ServerName + ';Database=' +  t.DatabaseName 
 	+  ';Uid=' + t.UserName + ';password=' + t.Password) AS AdvantageConnection
	 ,ak.[Key]
	 , wapi.ExternalCompanyCode
	 , wapi.TenantId
	 , t.TenantName
	 FROM dbo.WAPITenantCompanySecret wapi
	 JOIN dbo.Tenant t ON t.TenantId = wapi.TenantId
	--JOIN dbo.TenantAccess ta ON ta.TenantId = t.TenantId
	--JOIN dbo.DataConnection dc ON dc.ConnectionStringId = ta.ConnectionStringId
	JOIN dbo.ApiAuthenticationKey ak ON ak.TenantId = t.TenantId
	--JOIN [dbo].[Environment] ev ON ev.EnvironmentId = t.EnvironmentID
 
END

GO
