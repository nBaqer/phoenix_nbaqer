SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create PROC [dbo].[USP_IsCourseCompleted_ForExternship]
@StuEnrollId uniqueidentifier
as
Declare @IsCourseCompleted bit
Declare @ExternshipAttendanceNumber int
Declare @AttemptedExternshipHours decimal(18,2), @MinAttendanceNeeded decimal(18,2)

-- Get the minimum externship hours required
Set @ExternshipAttendanceNumber = (
			Select Top 1 CourseGradeBookDetails.Number
								  from
									arGrdBkWeights CourseGradeBook inner join arGrdBkWgtDetails CourseGradeBookDetails 
									on CourseGradeBook.InstrGrdBkWgtId = CourseGradeBookDetails.InstrGrdBkWgtId
									inner join arGrdComponentTypes ComponentTypes 
									on CourseGradeBookDetails.GrdComponentTypeId = ComponentTypes.GrdComponentTypeId
								  where
									CourseGradeBook.ReqId in (
										Select Distinct ReqId from arClassSections CS Inner Join arResults R 
										on CS.ClsSectionId=R.TestId
										and R.StuEnrollId=@StuEnrollId
									) 
									and ComponentTypes.SysComponentTypeId in (544)
					)


declare getGradeBookResultsForLabWorkorLabHours cursor for
        Select Distinct t1.StuEnrollId,
		Sum(t1.HoursAttended) as Score,@ExternshipAttendanceNumber as Number
		from arExternshipAttendance t1 inner join arGrdComponentTypes t3 on t1.GrdComponentTypeId = t3.GrdComponentTypeId
		where StuEnrollId=@StuEnrollId
		and t3.SysComponentTypeId in (544) 
		group by t1.StuEnrollId
		
		
open getGradeBookResultsForLabWorkorLabHours

fetch next from getGradeBookResultsForLabWorkorLabHours
into @StuEnrollId,@AttemptedExternshipHours,@MinAttendanceNeeded

Set @IsCourseCompleted = 0 -- by default False

while @@FETCH_STATUS = 0
begin
		
		if @AttemptedExternshipHours >= @MinAttendanceNeeded
		begin
			Set @IsCourseCompleted = 1
		end
		
		if @AttemptedExternshipHours < @MinAttendanceNeeded
		begin
			Set @IsCourseCompleted = 0 
			Break --Exit while loop if any one of component was not completed
		end
       
	fetch next from getGradeBookResultsForLabWorkorLabHours
	into @StuEnrollId,@AttemptedExternshipHours,@MinAttendanceNeeded
end
close getGradeBookResultsForLabWorkorLabHours
deallocate getGradeBookResultsForLabWorkorLabHours

--Print 'Before Applying course component'
--Print @IsCourseCompleted

Declare @ClsSectionId uniqueidentifier
Set @ClsSectionId = (Select Top 1 CS.ClsSectionId  from arClassSections CS Inner Join arResults R 
							on CS.ClsSectionId=R.TestId
							inner join arGrdBkWeights CourseGradeBook on CS.ReqId=CourseGradeBook.ReqId
							inner join arGrdBkWgtDetails CourseGradeBookDetails 
							on CourseGradeBook.InstrGrdBkWgtId = CourseGradeBookDetails.InstrGrdBkWgtId
							inner join arGrdComponentTypes ComponentTypes 
							on CourseGradeBookDetails.GrdComponentTypeId = ComponentTypes.GrdComponentTypeId
							and R.StuEnrollId=@StuEnrollId
						    and ComponentTypes.SysComponentTypeId in (544))


if @IsCourseCompleted=1 
	begin
		declare @IsComponentAnExam int, @IsFinalGradePosted int
		
		-- Among the components assigned to course see if there are any exam components
		Set @IsComponentAnExam = (Select Count(*) as RowCounter
								  from
									arGrdBkWeights CourseGradeBook inner join arGrdBkWgtDetails CourseGradeBookDetails 
									on CourseGradeBook.InstrGrdBkWgtId = CourseGradeBookDetails.InstrGrdBkWgtId
									inner join arGrdComponentTypes ComponentTypes 
									on CourseGradeBookDetails.GrdComponentTypeId = ComponentTypes.GrdComponentTypeId
								  where
									CourseGradeBook.ReqId in (
												Select Distinct ReqId from arClassSections CS Inner Join arResults R 
												on CS.ClsSectionId=R.TestId
												and R.StuEnrollId=@StuEnrollId) 
									and ComponentTypes.SysComponentTypeId in (501,533)
								 )
		
		
		
		
	
		--Print 'ExamComponent='
		--Print @IsComponentAnExam
		--If there is an exam component check if there is a final grade available on
		--the course in arResults table
		if @IsComponentAnExam >=1 
			begin
				Set @IsFinalGradePosted = (select Count(*) from arResults where StuEnrollId=@StuEnrollId and TestId=@ClsSectionId
									 and (GrdSysDetailId is not null or Score is not null))
				
				--Print 'Final Grade Posted='
				--Print @IsFinalGradePosted
				
				-- If student has a final grade on the course set CourseCompleted Flag to 1					 
				if @IsFinalGradePosted >=1
					begin
						SET @IsCourseCompleted=1
						Update arResults set IsCourseCompleted=1 where StuEnrollId=@StuEnrollId and TestId=@ClsSectionId			
					end 		
				else
					begin
						SET @IsCourseCompleted=0
						Update arResults set IsCourseCompleted=0 where StuEnrollId=@StuEnrollId and TestId=@ClsSectionId			
					end
		   end
		else
			begin
				sET @IsCourseCompleted=1
				Update arResults set IsCourseCompleted=1 where StuEnrollId=@StuEnrollId and TestId=@ClsSectionId			
			end
	end
else
	begin
		--sET @IsCourseCompleted=0
		Update arResults set IsCourseCompleted=0 where StuEnrollId=@StuEnrollId and TestId=@ClsSectionId
	end
	--Print @IsCourseCompleted
GO
