SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE Proc [dbo].[USP_Support_IsDefaultTenant]
  as
  select Count(*) as TotalRowCount from [dbo].[TenantUsers] 
  where UserId in (Select Top 1 UserId from 
  [dbo].[aspnet_users] where 
  username like 'support%') and IsDefaultTenant=1
GO
