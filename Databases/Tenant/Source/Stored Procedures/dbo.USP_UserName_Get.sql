SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create PROC [dbo].[USP_UserName_Get]
  @UserId uniqueidentifier
  as
  Select Top 1 UserName from [dbo].[aspnet_Users] where UserId=@UserId
GO
