SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--exec [USP_AR_GetAvailableClassesUsingStartDate] 'ead46332-790c-4666-bf41-0bacfc47dfb4'

CREATE Procedure [dbo].[USP_AR_GetAvailableClassesUsingStartDate] ( @StuEnrollId UniqueIdentifier)
as
/*----------------------------------------------------------------------------------------------------
	Author          :	Janet Robinson
    
    Create date		:	06/11/2013
    
	Procedure Name	:	USP_AR_GetAvailableClassesUsingStartDate

	Objective		:	Refactoring of Function: GetAvailableClassesForEnrollmentUsingStartDate
						in File Name: TermProgressDB.vb in DataAccess Project
						Query to get the available classes for Student Schedule page 
	
	Parameters		:	Name			Type	Data Type	Required? 	
						=====			====	=========	=========	
						@StuEnrollID	In		Varchar		Required	
							
						
*/-----------------------------------------------------------------------------------------------------

BEGIN
    --
	-- File Name: TermProgressDB.vb in DataAccess Project
	-- Function: GetAvailableClassesForEnrollmentUsingStartDate
	-- Query to get the available classes
	-- Discussion points : 1. Subqueries need to be moved into a table variable
	-- Inline sql will be replaced with stored procedure
	-- Look out of Business Rules/Changes comment
	
	Declare @ListCoursesStudentPassed table (CourseId UNIQUEIDENTIFIER, AllowRetake BIT, IsPass BIT, PassedEndDate DATETIME)
	--Declare @ListCoursesOrEquivalentCoursesStudentRegisteredFor table(ClassId uniqueidentifier)
	Declare @ProgramId UNIQUEIDENTIFIER
	DECLARE @Result TABLE (ClsSectionId UNIQUEIDENTIFIER, ClsSection VARCHAR(12), ReqId UNIQUEIDENTIFIER, TermId UNIQUEIDENTIFIER, TermDescrip VARCHAR(50), TermStart DATETIME, 
					StartDate DATETIME, EndDate DATETIME, Code VARCHAR(12), Descrip VARCHAR(100) )

	-- List of Courses the Student Passed 
	INSERT INTO @ListCoursesStudentPassed (CourseId, AllowRetake, IsPass, PassedEndDate) 
	-- Get Courses Student Passed from arResults
	SELECT DISTINCT Courses.ReqId,Courses.AllowCompletedCourseRetake, GradeDetails.IsPass, Classes.EndDate
	FROM 
		arReqs Courses INNER JOIN arClassSections Classes ON Classes.ReqId = Courses.ReqId 
		INNER JOIN arResults CourseGrades ON CourseGrades.TestId = Classes.ClsSectionId
		INNER JOIN arGradeSystemDetails GradeDetails  ON CourseGrades.GrdSysDetailId = GradeDetails.GrdSysDetailId
	WHERE
		CourseGrades.StuEnrollId = @StuEnrollId AND 
		CourseGrades.GrdSysDetailId is NOT NULL 
		AND GradeDetails.IsInComplete=0 -- Courses with incomplete grades are considered incomplete courses
	UNION
	-- Get Courses Student Passed from arTransferGrades
	SELECT DISTINCT Courses.ReqId, Courses.AllowCompletedCourseRetake, GradeDetails.IsPass, Classes.EndDate
	FROM 
		arTransferGrades CourseGrades  INNER JOIN arReqs Courses ON CourseGrades.ReqId = Courses.ReqId 
		INNER JOIN arGradeSystemDetails GradeDetails  ON CourseGrades.GrdSysDetailId = GradeDetails.GrdSysDetailId
		INNER JOIN arClassSections Classes ON Classes.ReqId = Courses.ReqId 
	WHERE
		CourseGrades.StuEnrollId = @StuEnrollId AND 
		CourseGrades.GrdSysDetailId is NOT NULL 
		AND GradeDetails.IsInComplete=0 -- Courses with incomplete grades are considered incomplete courses
			
	Set @ProgramId = (SELECT Top 1 ProgId 
					  FROM arPrgVersions ProgramVersion INNER JOIN arStuEnrollments StudentEnrollment
							ON ProgramVersion.PrgVerId = StudentEnrollment.PrgVerId
					  WHERE StudentEnrollment.StuEnrollId= @StuEnrollID)
					   
	-- returned results                            
	INSERT INTO @Result
	        ( ClsSectionId ,
	          ClsSection ,
	          ReqId ,
	          TermDescrip ,
	          TermId ,
	          TermStart ,
	          StartDate ,
	          EndDate ,
	          Code ,
	          Descrip
	        )
	        
	SELECT DISTINCT t0.ClsSectionId, t0.ClsSection, t0.ReqId,t11.TermDescrip,t11.TermId AS TermId,t11.StartDate AS TermStart, t0.StartDate,t0.EndDate, t9.Code, t9.Descrip 
			   from arClassSections t0, arStuEnrollments t8, arReqs t9, arClassSectionTerms t10, arTerm t11,arProgVerDef t12 
			   where 
	           
			   -- Business Rule 1
			   -- Ignore the courses the student has already taken and got a passing score
			   -- Change 1: If the "AllowRetakesForCompletedCourse" is set to True
			   -- then by pass business rule 1
			   (t0.ReqId not in (Select Distinct CourseId from @ListCoursesStudentPassed WHERE AllowRetake = 0 AND IsPass = 1) 
				OR
				t0.ReqId IN (SELECT DISTINCT CourseId FROM @ListCoursesStudentPassed WHERE AllowRetake = 1)				
				)
			   and t0.ReqId = t9.ReqId 
			   and t0.ClsSectionId = t10.ClsSectionId 
			   and t10.TermId = t11.TermId 
			   and t0.ShiftId = t8.ShiftId 
			   and t8.StuEnrollId = @StuEnrollID
  			   and ( t11.ProgId = @ProgramId or  t11.ProgId is Null) 
				
			   -- Business Rule 2
			   -- Make sure the Student is not registered in any class and
			   -- also make sure the student didn't pass a course marked as a equivalent course
			   -- Change 2: If the "AllowRetakesForCompletedCourse" is set to True
			   -- then by pass business rule 2                                                   
			   and (t0.ClsSectionId not in 
			   (SELECT CourseResults.TestId from arResults CourseResults where CourseResults.StuEnrollId = @StuEnrollID
				UNION 
				SELECT Class.ClsSectionId 
				FROM arResults CourseResults INNER JOIN arClassSections Class ON CourseResults.TestId=Class.ClsSectionId
					INNER JOIN arCourseEquivalent  EquivalentCourse ON EquivalentCourse.EquivReqid = t0.Reqid 
						AND Class.Reqid=EquivalentCourse.Reqid
					INNER JOIN arGradeSystemDetails GradeDetails  ON CourseResults.GrdSysDetailId=GradeDetails.GrdSysDetailId 
					INNER JOIN @ListCoursesStudentPassed ListCoursesStudentPassed ON ListCoursesStudentPassed.CourseId = EquivalentCourse.EquivReqid
				WHERE
					CourseResults.StuEnrollId = @StuEnrollID AND
					GradeDetails.IsPass=1 AND ListCoursesStudentPassed.AllowRetake = 0)
				OR
				t0.ClsSectionId in 
			   (
				SELECT Class.ClsSectionId 
				FROM arResults CourseResults INNER JOIN arClassSections Class ON CourseResults.TestId=Class.ClsSectionId
					INNER JOIN arCourseEquivalent  EquivalentCourse ON EquivalentCourse.EquivReqid = t0.Reqid 
						AND Class.Reqid=EquivalentCourse.Reqid
					INNER JOIN arGradeSystemDetails GradeDetails  ON CourseResults.GrdSysDetailId=GradeDetails.GrdSysDetailId 
					INNER JOIN @ListCoursesStudentPassed ListCoursesStudentPassed ON ListCoursesStudentPassed.CourseId = EquivalentCourse.EquivReqid
				WHERE
					CourseResults.StuEnrollId = @StuEnrollID AND
					GradeDetails.IsPass=1 AND ListCoursesStudentPassed.AllowRetake = 1) 
				)
				 and t0.EndDate >= t8.StartDate and  
				 t0.EndDate<=t8.ExpGradDate and t8.PrgVerId=t12.PrgVerId 
				 and t9.ReqId in 
						(select Distinct ReqId from arProgVerDef where PrgVerId=t12.PrgVerId 
							Union 
						 select Distinct ReqId from arReqGrpDef where GrpId in (select Distinct ReqId 
								from arProgVerDef where PrgVerId=t12.PrgVerId)) 
				
				-- Modified by Balaji on 6.17.2013
			    -- The following line was not allowing a course to be re-taken if grade was transferred for a course
				--and t9.ReqId not in (select Distinct ReqId from arTransferGrades where StuEnrollId=@StuEnrollID) 
				and t9.ReqId not in (Select Distinct CourseId from @ListCoursesStudentPassed WHERE AllowRetake = 0 AND IsPass = 1)
				and T0.CampusID=T8.CampusID  
	            
	Union 
	           
	Select Distinct t0.ClsSectionId, t0.ClsSection, t0.ReqId,t11.TermDescrip,t11.TermId AS TermId,t11.StartDate AS TermStart, t0.StartDate,t0.EndDate, t9.Code, t9.Descrip 
			from arClassSections t0, arStuEnrollments t8, 
			   (SELECT t1.ReqId,ReqSeq,IsRequired,t2.Descrip,t2.Code,t2.ReqTypeId,t2.Hours,t2.Credits,t1.ModUser,t1.ModDate  
				FROM arReqGrpDef t1, arReqs t2 
				WHERE t1.ReqId = t2.ReqId and t1.GrpId in ( 
				select distinct t0.ReqId from arReqs t0,arProgVerDef t1,arReqGrpDef t2 where PrgVerId= 
			   (select PrgVerId from arStuEnrollments where StuEnrollId = @StuEnrollID) 
				and t1.ReqId=t2.GrpId and t0.ReqId=t1.reqid)) t9, 
			    arClassSectionTerms t10, arTerm t11 
			where 
			   -- Modified by Balaji on 6.17.2013
			   -- The following conditions were earlier looking for AllowRetake=1, it has been modified.
			  ( t0.ReqId not in  (Select Distinct CourseId from @ListCoursesStudentPassed WHERE AllowRetake = 0 AND IsPass = 1) 
				OR
				t0.ReqId IN (SELECT DISTINCT CourseId FROM @ListCoursesStudentPassed WHERE AllowRetake = 1) 				
				)
			   and t0.ReqId = t9.ReqId 
			   and t0.ClsSectionId = t10.ClsSectionId 
			   and t10.TermId = t11.TermId and t0.ShiftId = t8.ShiftId 
			   and t8.StuEnrollId = @StuEnrollID 
	            
			   and (t11.ProgId = @ProgramId or t11.ProgId is Null) 
	            
			   -- Business Rule 4
			   -- Ignore the graded courses the student has already taken
			   -- Ignore the courses with "transferred" grade
			   -- Change 3: If the "AllowRetakesForCompletedCourse" is set to True
			   -- then by pass business rule 4
				and t0.ClsSectionId not in 
			   (Select t7.TestId from arResults t7 where t7.StuEnrollId = @StuEnrollID ) 
			   
			   -- Modified by Balaji on 6.17.2013
			   -- The following line was not allowing a course to be re-taken if grade was transferred for a course
			   --and t9.ReqId not in (select Distinct ReqId from arTransferGrades where StuEnrollId=@StuEnrollID) 
				and t9.ReqId not in (Select Distinct CourseId from @ListCoursesStudentPassed WHERE AllowRetake = 0 AND IsPass = 1)
	            
				--DE1140: Student's Scheduled tab does not show the class sections if it is current. 
				and t0.EndDate >= t8.StartDate and t0.EndDate<=t8.ExpGradDate 
				--Added by SAraswathi lakshmanan to show the ClassSections related to the student campus related to the student
				--Added on mAy 12 2010
				--To fix Mantis case : 18921 data Fix issues for Ross
				and T0.CampusID=T8.CampusID 
	  
	   	
	   Select ClsSectionId, ClsSection, ReqId, TermDescrip, TermId AS TermId, TermStart, StartDate, EndDate, Code, Descrip 
	   FROM @Result
	   WHERE ReqId NOT IN (SELECT CourseId FROM @ListCoursesStudentPassed)
	      OR ReqID IN (SELECT CourseId FROM @ListCoursesStudentPassed ListCoursesStudentPassed WHERE ListCoursesStudentPassed.AllowRetake = 1 AND StartDate > ListCoursesStudentPassed.PassedEndDate)	   				
	   ORDER BY 
	   TermStart desc,Descrip,StartDate 
	   
END	   

GO
