SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[USP_DatabaseWithVersion_GetList]
  as
  Select ConnectionStringId,D1.DatabaseName + d2.VersionNumber as databaseversion
  from [TenantAuthDB].dbo.[DataConnection] D1
  cross join [TenantAuthDB].dbo.[Environment] D2
GO
