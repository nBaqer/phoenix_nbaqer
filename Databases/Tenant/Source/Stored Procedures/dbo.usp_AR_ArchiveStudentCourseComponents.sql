SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[usp_AR_ArchiveStudentCourseComponents]
	@ClsSectionId uniqueIdentifier,
	@StuEnrollId uniqueIdentifier,
	@User nvarchar(50)
AS
BEGIN
	
	INSERT INTO _archive_arGrdBkResults (
			Archive_User,
			GrdBkResultId,
			ClsSectionId,
			InstrGrdBkWgtDetailId,		
			Score,
			Comments,
			StuEnrollId,
			ModUser,
			ModDate,
			ResNum,
			PostDate,
			IsCompGraded,
			IsCourseCredited )
		SELECT
			@User,
			GrdBkResultId,
			ClsSectionId,
			InstrGrdBkWgtDetailId,		
			Score,
			Comments,
			StuEnrollId,
			ModUser,
			ModDate,
			ResNum,
			PostDate,
			IsCompGraded,
			IsCourseCredited					
		FROM arGrdBkResults
		WHERE StuEnrollId = @StuEnrollId
			AND ClsSectionId = @ClsSectionId
	
	DELETE arGrdBkResults
	WHERE StuEnrollId = @StuEnrollId
		AND ClsSectionId = @ClsSectionId	

END
GO
