SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:	JAGG
-- Create date: 4/8/2016
-- Description:	Remove all tenant information from
-- the database. this include all user in aspnet_membership
-- and also in tenant User, the api key and all other
-- references to the tenant in the database
-- ONLY THE Support User is not deleted in 
-- the membership tables
-- =============================================

CREATE PROCEDURE [dbo].[RemoveTenant]
	-- Tenant Id (As appear in the Tenant table)
    @tenantId INT
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @UserToDelete TABLE
            (
             UserId UNIQUEIDENTIFIER
            )
       
        -- Get all user id reference to the tenant to delete
        -- except the support user.
        INSERT  INTO @UserToDelete
                ( UserId 
                )
                SELECT  UserId
                FROM    TenantUsers
                WHERE   TenantId = @tenantId
                        AND IsSupportUser = 0

		-- Get the support user (never ever should be deleted)
        DECLARE @supportId VARCHAR(38) = ( SELECT   UserId
                                           FROM     [dbo].[aspnet_Users]
                                           WHERE    UserName = 'support@fameinc.com'
                                         )
		-- Delete from @UserToDelete
		DELETE @UserToDelete WHERE UserId = @supportId

		-- Begin the transaction to delete users......
        BEGIN TRY
            BEGIN TRANSACTION DeleteTenant
	        -- Delete User for aspnet membership
            DELETE  FROM aspnet_Profile
            WHERE   UserId IN ( SELECT  *
                                FROM    @UserToDelete )
            DELETE  FROM aspnet_UsersInRoles
            WHERE   UserId IN ( SELECT  *
                                FROM    @UserToDelete )
            DELETE  FROM aspnet_PersonalizationPerUser
            WHERE   UserId IN ( SELECT  *
                                FROM    @UserToDelete )
            DELETE  FROM dbo.aspnet_Membership
            WHERE   UserId IN ( SELECT  *
                                FROM    @UserToDelete )
            DELETE  FROM aspnet_Users
            WHERE   UserId IN ( SELECT  *
                                FROM    @UserToDelete )

	        -- Delete Tenant Id references for Tenant tables
            DELETE  dbo.TenantAccess
            WHERE   TenantId = @tenantId
            DELETE  dbo.TenantUsers
            WHERE   TenantId = @tenantId
            DELETE  dbo.ApiAuthenticationKey
            WHERE   TenantId = @tenantId

            -- Delete Tenant Id reference from WAPI secret company
            DELETE  dbo.WAPITenantCompanySecret
            WHERE   TenantId = @tenantId

            DELETE  Tenant
            WHERE   TenantId = @tenantId
            COMMIT TRANSACTION DeleteTenant
        END TRY
        BEGIN CATCH
            ROLLBACK TRANSACTION DeleteTenant	
        END CATCH
  

        SET NOCOUNT OFF
    END
GO
