SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Proc [dbo].[USP_AttemptedClass_TermStartDate]
@StuEnrollId uniqueidentifier,
@ClsSectionId uniqueidentifier
as
begin
Select Top 1 TermStartDate
from
(
Select 
	  Top 1 Term.Startdate as TermStartDate
from
	arresults results inner join arClassSections Class on results.testid = Class.ClsSectionId
	inner join arTerm Term on Class.TermId = Term.TermId
where
	results.stuenrollid=@StuEnrollId and
	results.TestId=@ClsSectionId
Union
Select Top 1 Term.startDate as termStartDate from arTransferGrades t9 inner join arTerm Term on t9.TermId=Term.termId 
where t9.StuEnrollId = @StuEnrollId and 
t9.ReqId in (Select ReqId from arClassSections where ClsSectionId = @ClsSectionId)
) tblDerived
end
GO
