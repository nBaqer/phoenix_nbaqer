SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE proc [dbo].[USP_DefaultTenant_Set]
@TenantId int
as
Declare @supportUserId uniqueidentifier
Set @supportUserId = (Select Top 1 UserId from 
  [dbo].[aspnet_users] where 
  username like 'support%')
If not exists (select * from [dbo].[TenantUsers] where 
			   userid=@supportUserId and TenantId=@TenantId)
	begin
		insert into [dbo].[TenantUsers](TenantId,UserId,IsDefaultTenant)
		values(@TenantId,@supportUserId,1)
	end
else
	begin
		Update [dbo].[TenantUsers] set IsDefaultTenant=0 where
		UserId=@supportUserId

		Update [dbo].[TenantUsers]  set IsDefaultTenant=1 where
		UserId=@supportUserId
		and TenantId=@TenantId
	end 
GO
