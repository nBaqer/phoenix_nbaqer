SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Proc [dbo].[USP_CheckIfClassWasAlreadyCompleted]
@StuEnrollId uniqueidentifier,
@ClsSectionId uniqueidentifier
as
	Select Count(*) as gradedcount from arResults where StuEnrollId=@StuEnrollId and TestId=@ClsSectionId
	and (Score is not null or Grdsysdetailid is not null)
GO
