SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROC [dbo].[USP_IsCourseCompleted_ForClinicWorkorLabHours]
@StuEnrollId uniqueidentifier,
@ClsSectionId uniqueidentifier
as
DECLARE @GrdBkResultId uniqueidentifier,@ClassSectionId uniqueidentifier, @InstrGrdBkWgtDetailId uniqueidentifier
DECLARE @AttemptedLabWorkorHours decimal(18,2), @MinLabWorkorHoursNeeded decimal(18,2)
DECLARE @IsCourseCompleted BIT
DECLARE @ReqId UNIQUEIDENTIFIER
DECLARE @AllGrdBkWgtDetails table (ClsSectionId UNIQUEIDENTIFIER, InstrGrdBkWgId UNIQUEIDENTIFIER, InstrGrdBkWgtDetailId UNIQUEIDENTIFIER, Score DECIMAL(18,2), Number DECIMAL(18,2))
DECLARE @StudGrdBkWgtDetails table (ClsSectionId UNIQUEIDENTIFIER, InstrGrdBkWgId UNIQUEIDENTIFIER, InstrGrdBkWgtDetailId UNIQUEIDENTIFIER, Score DECIMAL(18,2), Number DECIMAL(18,2))

SET @ReqId = (SELECT ReqId FROM dbo.arReqs WHERE ReqId IN 
(SELECT ReqId FROM dbo.arClassSections WHERE ClsSectionId = @ClsSectionId))

INSERT INTO @AllGrdBkWgtDetails (ClsSectionId, InstrGrdBkWgId, InstrGrdBkWgtDetailId, Score, Number) 
SELECT @ClsSectionId, w.InstrGrdBkWgtId, d.InstrGrdBkWgtDetailId, 0, Number
		FROM dbo.arGrdBkWeights w INNER JOIN dbo.arGrdBkWgtDetails d ON d.InstrGrdBkWgtId = w.InstrGrdBkWgtId
		WHERE ReqId = @ReqId 
		
INSERT INTO @StudGrdBkWgtDetails (ClsSectionId, InstrGrdBkWgId, InstrGrdBkWgtDetailId, Score, Number) 
SELECT DISTINCT t1.ClsSectionId,t2.InstrGrdBkWgtId,t1.InstrGrdBkWgtDetailId, SUM(t1.Score) as Score,MAX(t2.Number) as Number
		from arGrdBkResults t1 inner join arGrdBkWgtDetails t2 on 
		t1.InstrGrdBkWgtDetailId = t2.InstrGrdBkWgtDetailId
		inner join arGrdComponentTypes t3 on t2.GrdComponentTypeId = t3.GrdComponentTypeId
		LEFT OUTER JOIN @AllGrdBkWgtDetails a ON a.InstrGrdBkWgtDetailId = t1.InstrGrdBkWgtDetailId 
		where StuEnrollId=@StuEnrollId
		and t1.ClsSectionId=@ClsSectionId
		and t3.SysComponentTypeId in (500,503)				
		GROUP BY t1.ClsSectionId,t2.InstrGrdBkWgtId,t1.InstrGrdBkWgtDetailId
		ORDER BY t1.ClsSectionId,t2.InstrGrdBkWgtId,t1.InstrGrdBkWgtDetailId
		
INSERT INTO @StudGrdBkWgtDetails (ClsSectionId, InstrGrdBkWgId, InstrGrdBkWgtDetailId, Score, Number) 
SELECT ClsSectionId, InstrGrdBkWgId,InstrGrdBkWgtDetailId, Score, Number
FROM @AllGrdBkWgtDetails a
WHERE InstrGrdBkWgtDetailId NOT IN (SELECT DISTINCT InstrGrdBkWgtDetailId FROM @StudGrdBkWgtDetails)

declare getGradeBookResultsForLabWorkorLabHours cursor FOR
		SELECT ClsSectionId,InstrGrdBkWgtDetailId,Score,Number FROM @StudGrdBkWgtDetails

  --      Select Distinct 
		--t1.GrdBkResultId,
		--t1.ClsSectionId,t1.InstrGrdBkWgtDetailId, COALESCE(Sum(t1.Score),0) as Score,Max(t2.Number) as Number
		--from arGrdBkResults t1 inner join arGrdBkWgtDetails t2 on 
		--t1.InstrGrdBkWgtDetailId = t2.InstrGrdBkWgtDetailId
		--inner join arGrdComponentTypes t3 on t2.GrdComponentTypeId = t3.GrdComponentTypeId
		--where StuEnrollId=@StuEnrollId
		--and ClsSectionId=@ClsSectionId
		--and t3.SysComponentTypeId in (500,503) 
		--group by t1.ClsSectionId,t1.InstrGrdBkWgtDetailId,t1.GrdBkResultId
		
		
		/**
		Select Distinct t1.ClsSectionId,t1.InstrGrdBkWgtDetailId, SUM(t1.Score) as Score,Max(t2.Number) as Number
		from arGrdBkResults t1 inner join arGrdBkWgtDetails t2 on 
		t1.InstrGrdBkWgtDetailId = t2.InstrGrdBkWgtDetailId
		inner join arGrdComponentTypes t3 on t2.GrdComponentTypeId = t3.GrdComponentTypeId
		where StuEnrollId='710466B4-C4E6-46F5-A769-21FDE7488A98'
		and ClsSectionId='0BCEE910-C95E-4580-956B-AAAD079D4CF3'
		and t3.SysComponentTypeId in (500,503)
		group by t1.ClsSectionId,t1.InstrGrdBkWgtDetailId
		**/
		
open getGradeBookResultsForLabWorkorLabHours

fetch next from getGradeBookResultsForLabWorkorLabHours
into @ClassSectionId,@InstrGrdBkWgtDetailId,@AttemptedLabWorkorHours,@MinLabWorkorHoursNeeded

Set @IsCourseCompleted = 0 -- by default False

while @@FETCH_STATUS = 0
BEGIN		
		if @AttemptedLabWorkorHours >= @MinLabWorkorHoursNeeded
		begin
			Set @IsCourseCompleted = 1
		end
		
		if @AttemptedLabWorkorHours < @MinLabWorkorHoursNeeded
		begin
			Set @IsCourseCompleted = 0 
			Break --Exit while loop if any one of component was not completed
		end
       
		fetch next from getGradeBookResultsForLabWorkorLabHours
		into @ClassSectionId,@InstrGrdBkWgtDetailId,
		@AttemptedLabWorkorHours,@MinLabWorkorHoursNeeded
end
close getGradeBookResultsForLabWorkorLabHours
deallocate getGradeBookResultsForLabWorkorLabHours

if @IsCourseCompleted=1 
	begin
		declare @IsComponentAnExam int, @IsFinalGradePosted int
		
		-- Among the components assigned to course see if there are any exam components
		Set @IsComponentAnExam = (Select Count(*) as RowCounter
								  from
									arGrdBkWeights CourseGradeBook inner join arGrdBkWgtDetails CourseGradeBookDetails 
									on CourseGradeBook.InstrGrdBkWgtId = CourseGradeBookDetails.InstrGrdBkWgtId
									inner join arGrdComponentTypes ComponentTypes 
									on CourseGradeBookDetails.GrdComponentTypeId = ComponentTypes.GrdComponentTypeId
								  where
									CourseGradeBook.ReqId in (Select Top 1 ReqId from arClassSections where 
															  ClsSectionId=@ClassSectionId)  --  'F33BCEE0-6334-47D5-98BC-A35B5AE6B2B1'
									and ComponentTypes.SysComponentTypeId in (501,533)
								 )
	
		--Print 'ExamComponent='
		--Print @IsComponentAnExam
		--If there is an exam component check if there is a final grade available on
		--the course in arResults table
		if @IsComponentAnExam >=1 
			begin
				Set @IsFinalGradePosted = (select Count(*) from arResults where StuEnrollId=@StuEnrollId and TestId=@ClsSectionId
									 and (GrdSysDetailId is not null or Score is not null))
				
				--Print 'Final Grade Posted='
				--Print @IsFinalGradePosted
				
				-- If student has a final grade on the course set CourseCompleted Flag to 1					 
				if @IsFinalGradePosted >=1
					begin
						Update arResults set IsCourseCompleted=1 where StuEnrollId=@StuEnrollId and TestId=@ClsSectionId			
					end 		
				else
					begin
						Update arResults set IsCourseCompleted=0 where StuEnrollId=@StuEnrollId and TestId=@ClsSectionId			
					end
		   end
		else
			begin
				Update arResults set IsCourseCompleted=1 where StuEnrollId=@StuEnrollId and TestId=@ClsSectionId			
			end
	end
else
	begin
		Update arResults set IsCourseCompleted=0 where StuEnrollId=@StuEnrollId and TestId=@ClsSectionId
	end

GO
