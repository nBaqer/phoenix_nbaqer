SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Proc [dbo].[USP_PasswordDate_Update]
@UserId uniqueidentifier
as
begin
	Declare @CurrentDate datetime
	Set @CurrentDate = getdate()
	Update dbo.aspnet_membership set CreateDate=@CurrentDate, LastPasswordChangedDate=@CurrentDate
	where UserId=@UserId
end
GO
