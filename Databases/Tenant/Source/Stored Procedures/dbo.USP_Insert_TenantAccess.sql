SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[USP_Insert_TenantAccess]
    @TenantId INT ,
    @EnvironmentId INT ,
    @ConnectionStringId INT
AS 
    DELETE  FROM dbo.[TenantAccess]
    WHERE   TenantId = @TenantId
    INSERT  INTO dbo.[TenantAccess]
            ( TenantId ,
              EnvironmentId ,
              ConnectionStringId
            )
    VALUES  ( @TenantId ,
              @EnvironmentId ,
              @ConnectionStringId
            )

GO
