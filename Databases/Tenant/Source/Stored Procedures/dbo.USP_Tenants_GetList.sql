SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE Proc [dbo].[USP_Tenants_GetList]
as
  SELECT [TenantId]
      ,[TenantName]
  FROM [dbo].[Tenant]
GO
