SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


  
CREATE PROCEDURE [dbo].[usp_GetSAPDetails]
    (
      @SAPDetailId UNIQUEIDENTIFIER  
    )
AS 
    SET NOCOUNT ON ;  
  
    DECLARE @MySAPDetails TABLE
        (
          SAPDetailId UNIQUEIDENTIFIER PRIMARY KEY ,
          QualMinValue DECIMAL(18,2) ,
          QuantMinValue DECIMAL,
          QuantMinUnitTypId INT ,
          QualMinTypId INT ,
          Seq INT ,
          TrigValue INT ,
          TrigUnitTypId INT ,
          TrigOffsetTypId INT ,
          TrigOffsetSeq INT ,
          ConsequenceTypId INT ,
          MinCredsCompltd DECIMAL,
          MinAttendanceValue DECIMAL,
          SAPId UNIQUEIDENTIFIER ,
          accuracy DECIMAL
        )  
  
    INSERT  INTO @MySAPDetails
            SELECT  SAPDetailId ,
                    QualMinValue ,
                    QuantMinValue ,
                    QuantMinUnitTypId ,
                    QualMinTypId ,
                    ROW_NUMBER() OVER ( ORDER BY TrigOffsetSeq, TrigValue ) AS Seq ,
                    TrigValue ,
                    TrigUnitTypId ,
                    TrigOffsetTypId ,
                    TrigOffsetSeq ,
                    ConsequenceTypId ,
                    MinCredsCompltd ,
                    MinAttendanceValue ,
                    SAPId ,
                    accuracy
            FROM    arSAPDetails
            WHERE   SAPId = ( SELECT    SAPId
                              FROM      dbo.arSAPDetails
                              WHERE     SAPDetailId = @SAPDetailId
                            )
            ORDER BY TrigOffsetSeq ,
                    dbo.arSAPDetails.TrigValue  
  
    SELECT  t1.SAPDetailId ,
            t1.QualMinValue ,
            ISNULL(t1.QuantMinValue, 0) AS QuantMinValue ,
            t1.QuantMinUnitTypId ,
            t1.QualMinTypId ,
            t1.seq ,
            t1.TrigValue ,
            t1.TrigUnitTypId ,
            t1.TrigOffsetTypId ,
            t1.TrigOffsetSeq ,
            t1.ConsequenceTypId ,
            t1.MinCredsCompltd ,
            t1.MinAttendanceValue ,
            t1.SAPId ,
            t2.TrigUnitTypDescrip ,
            t3.TrigOffTypDescrip ,
            t4.QuantMinTypDesc ,
            t5.QualMinTypDesc ,
            t6.ConseqTypDesc ,
            t1.Accuracy ,
            IsSAPPolicyUsed = ( CASE WHEN ( SELECT  COUNT(*)
                                            FROM    arSAPChkResults
                                            WHERE   SAPDetailId = @SAPDetailId
                                                    AND PreviewSapCheck = 0
                                          ) > 0 THEN 'True'
                                     ELSE 'False'
                                END )
    FROM    @MySAPDetails t1 ,
            arTrigUnitTyps t2 ,
            arTrigOffsetTyps t3 ,
            arQuantMinUnitTyps t4 ,
            arQualMinTyps t5 ,
            arConsequenceTyps t6
    WHERE   t1.SAPDetailId = @SAPDetailId
            AND t1.TrigUnitTypId = t2.TrigUnitTypId
            AND t1.TrigOffsetTypId = t3.TrigOffsetTypId
            AND t1.QuantMinUnitTypId = t4.QuantMinUnitTypId
            AND t1.QualMinTypId = t5.QualMinTypId
            AND t1.ConsequenceTypId = t6.ConsequenceTypId
    ORDER BY TrigOffsetSeq ,
            TrigValue   

    SELECT  SAPQuantInsTypeID ,
            QuantMinValue ,
            SAPDetailId ,
            InstructionTypeId
    FROM    arSAPQuantByInstruction
    WHERE   SAPDetailId = @SAPDetailId

GO
