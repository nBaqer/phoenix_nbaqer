SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[USP_CreateLogin]
@LoginName varchar(50),
@ServerName varchar(50),
@DatabaseName varchar(50),
@UserName varchar(50),
@Password varchar(50),
@DatabaseNoVersion varchar(50)
as
BEGIN
SET NOCOUNT ON
DECLARE @SQL NVARCHAR(4000);
	
SET @SQL = 'CREATE LOGIN ' + @LoginName + ' WITH PASSWORD = ''' + @Password + '''' + ',DEFAULT_DATABASE=[' + @DatabaseName + ']';
print @SQL;
EXECUTE(@SQL);

declare @newtable nvarchar(200),@newinsert nvarchar(200),@nsql nvarchar(300)
set @newtable = ' USE [master] ' + ' if not exists (select * from sysobjects where type=''u'' and name=''tblTotalRows'') begin Create table tblTotalRows(TotalRows int) end'
print @newtable
exec sys.sp_executesql @newtable
set @newinsert = ' insert into master.dbo.tblTotalRows(TotalRows) Select Count(*) from sys.database_principals '
set @nsql = ' USE [' + @DatabaseName +'] ' + @newtable +  @newinsert + ' WHERE name=''' + @Username + ''''
exec sys.sp_executesql @nsql

IF (Select Count(*) from master.dbo.tblTotalRows)>=1
--IF NOT EXISTS (SELECT * FROM [DEV3.2COPY].sys.database_principals WHERE NAME=@UserName)
BEGIN
	declare @newuser nvarchar(4000)
	declare @userdbname varchar(25)
	declare @newvar nvarchar(300)
	set @userdbname = ' use [' + @DatabaseName + ']'
	set @newuser = 'CREATE USER ' + @USERNAME + ' FOR LOGIN ' + @LoginName + ' WITH DEFAULT_SCHEMA=[dbo]';
	set @newvar = ' USE [' + @DatabaseName +'] ' + @newuser
	exec sys.sp_executesql @newvar
	
	-- Role : DB_OWNER
	-- Data writers can INSERT, UPDATE and DELETE data from any table in the database.
	-- Recommended for Users that need to make changes to data, but should not change database schema or settings.
	-- Role : DB_datareader
	-- Data readers can read data from any table in the database.
	Declare @newvarrole nvarchar(300)
	set @newvarrole=' USE [' + @DatabaseName + '] ' + ' EXEC SP_ADDROLEMEMBER ' + 'DB_OWNER' + ',' + @UserName
	EXEC sys.sp_executesql @newvarrole 
	--set @newvarrole=' USE [TenantAuthDB] ' + ' EXEC SP_ADDROLEMEMBER ' + 'DB_OWNER' + ',' + @UserName
	--EXEC sys.sp_executesql @newvarrole 
END

TRUNCATE TABLE master.dbo.tblTotalRows


-- Help Link: http://msdn.microsoft.com/en-us/library/ms179331.aspx
--If there is no master key, create one now. 

-- Commented by balaji on April 22 2013 due to master key issues
/**
IF NOT EXISTS (SELECT * FROM sys.symmetric_keys WHERE symmetric_key_id = 101)
    CREATE MASTER KEY ENCRYPTION BY PASSWORD = 'Bg^@*24$!05'

IF (select Count(*) from sys.certificates where name = 'UserPasswordEncryptionNew') = 0
BEGIN
	CREATE CERTIFICATE UserPasswordEncryptionNew
    WITH SUBJECT = 'EncryptDatabasePassword';
END

IF (select Count(*) from sys.symmetric_keys where name = 'PasswordKey') = 0
BEGIN
	CREATE SYMMETRIC KEY PasswordKey
    WITH ALGORITHM = AES_256
    ENCRYPTION BY CERTIFICATE UserPasswordEncryptionNew;
END

OPEN SYMMETRIC KEY PasswordKey DECRYPTION BY CERTIFICATE UserPasswordEncryptionNew;
**/

if not exists (select * from dbo.DataConnection where userName=@LoginName)
begin
	select * from dbo.DataConnection where userName=@LoginName
	declare @connectionstringidentifier int
	if (select count(*) from dbo.DataConnection) = 0 
		begin
			set @connectionstringidentifier = 1
		end
	else
		begin
	  		set @connectionstringidentifier = (select max(ConnectionStringId)+1 from dbo.DataConnection)
		end
	
	
	SET IDENTITY_INSERT dbo.DataConnection ON
	
	/***
	-- The following section was commented by Balaji on April 22 2013 due to master key issues
		insert into dbo.DataConnection(ConnectionStringId,ServerName,DatabaseName,UserName,Password)
		values(@connectionstringidentifier,@ServerName,@DatabaseNoVersion,@LoginName,NULL)
	
		UPDATE dbo.DataConnection
		SET Password = EncryptByKey(Key_GUID('PasswordKey')
		, @Password, 1, HashBytes('SHA1', CONVERT(varbinary
		, ConnectionStringId)))
		WHERE ConnectionStringId=@connectionstringidentifier
	
	
	***/
	
	insert into dbo.DataConnection(ConnectionStringId,ServerName,DatabaseName,UserName,Password)
	values(@connectionstringidentifier,@ServerName,@DatabaseNoVersion,@LoginName,@Password)
	
	--UPDATE dbo.DataConnection
	--SET Password = EncryptByKey(Key_GUID('PasswordKey')
 --   , @Password, 1, HashBytes('SHA1', CONVERT(varbinary
 --   , ConnectionStringId)))
	--WHERE ConnectionStringId=@connectionstringidentifier
	
	SET IDENTITY_INSERT dbo.DataConnection OFF 
end
end
GO
