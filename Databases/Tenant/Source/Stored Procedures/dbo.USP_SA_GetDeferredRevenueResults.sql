SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/**
Author: Troy
Date: 6/8/2012
Comments: The original code was written by Anatoly when we were not using sprocs. I took the code he had in a class and moved all the logic here.
		  The UI page first allows the user to specify the report date they want to run. When they click build list, if there are no exceptions
		  then a summary by transcodes is displayed. If there are exceptions, they are displayed in a grid and the user cannot post until they have
		  cleared all the exceptions. If there are no exceptions, the user can click the Post Deferred Revenue button to do the actual posting.
		  
		  The original code returned a DataSet with several tables. However, the two tables are used in the UI are the TransCodes and the 
		  StudentExceptions. Since we can only return one result set here, the GetDeferredRevenueResults functionn in TuitionEarningsDB.vb
		  specifies the DataTable name as DeferredRevenueResultsTable. That table could be the contents of the TransCodes or the StudentExceptions
		  table. These two temp tables have a field called TableName that is set to TransCodes and StudentExceptions respectively. The UI code
		  can therefore check the TableName field of the first record to determine if the result set is for the TransCodes or the 
		  StudentExceptions.
		  
		  The mode parameter allows us to specify what function(s) we want the SP to perform. 
		  Mode=1: When the user clicks the Build List button we just want to get back a summary for each transcode. 
		  Mode=2: When the user clicks on a transcode after building the list, we want to show the details for that transcode.
		  Mode=3: When the user confirm that they want to post the deferred revenue the actual posting should be done.

**/

----EXECUTE USP_SA_GetDeferredRevenueResults '2/29/2012','436BF45E-5420-4C1F-8687-CD32A60D64BE','ssn',1,'sa'

CREATE PROCEDURE [dbo].[USP_SA_GetDeferredRevenueResults]   
	@ReportDate DATE=NULL,
	@CampusId VARCHAR(50)=null,	
	@StudentIdentifierType VARCHAR(50)=NULL,
	@Mode INTEGER=1, --BuildList=1, BindTransCodeDetails=2 and PostDeferredRevenue=3
	@User VARCHAR(50)=NULL,
	@CutoffTransDate DATE=NULL
	
AS
--Testing Purposes----------------------------------------------------------------------------------------------------------
--DECLARE @ReportDate DATETIME
--DECLARE @CampusId VARCHAR(50)
--DECLARE @CutoffTransDate DATETIME
--DECLARE @StudentIdentifierType VARCHAR(50)
--DECLARE @PostDeferredRevenue BIT
--DECLARE @User VARCHAR(50)
--DECLARE @Mode INTEGER
-----------------------------------------------------------------------------------------------------------------------------

DECLARE @TransactionId UNIQUEIDENTIFIER
DECLARE @StudentName VARCHAR(200)
DECLARE @StudentIdentifier VARCHAR(50)
DECLARE @StuEnrollId UNIQUEIDENTIFIER
DECLARE @TransCodeId UNIQUEIDENTIFIER
DECLARE @TransAmount DECIMAL(18,2)
DECLARE @LOAStartDate DATETIME
DECLARE @LOAEndDate DATETIME
DECLARE @Earned DECIMAL(18,2)
DECLARE @FeeLevelId INT
DECLARE	@TermStart DATETIME
DECLARE @TermEnd DATETIME
DECLARE @percentToEarnIdx VARCHAR(50)
DECLARE @RemainingMethod INT
DECLARE @DeferredRevenue DECIMAL(18,2)

----------------Testing Purposes----------------------------------------------------------------------------------------------
--SET @ReportDate='3/31/2012'
--SET @CampusId='436BF45E-5420-4C1F-8687-CD32A60D64BE'
--SET @StudentIdentifier='ssn'
--SET @PostDeferredRevenue=1
--SET @User = 'sa'
--SET @Mode=2
-------------------------------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------------------------
--This section creates the supporting temp tables
-----------------------------------------------------------------------------------------------------------------------------
--Transactions Table
CREATE TABLE #Transactions(
	TransactionId UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,
	StudentName VARCHAR(200) NOT NULL,
	StudentIdentifier VARCHAR(50) NULL,
	StuEnrollId UNIQUEIDENTIFIER NOT NULL,
	TransCodeId UNIQUEIDENTIFIER NULL,
	TransAmount DECIMAL(18,2) NOT NULL,
	LOAStartDate DATETIME NULL,
	LOAEndDate DATETIME NULL,
	Earned DECIMAL(18,2) NULL,
	FeeLevelId INT NULL,
	TermStart DATETIME NULL,
	TermEnd DATETIME NULL,
	percentToEarnIdx INT NULL,
	RemainingMethod INT NULL,
	DeferredRevenue DECIMAL(18,2) NULL

) 


--TransCodes Table
CREATE TABLE #TransCodes(
	TransCodeId UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,
	TransCodeDescrip VARCHAR(100) NOT NULL,
	TotalDeferredRevenue DECIMAL(18,2) NULL,
	AccruedAmount DECIMAL(18,2) NULL,
	DeferredRevenueAmount DECIMAL(18,2) NULL,
	TotalAmount DECIMAL(18,2) NULL,
	TableName VARCHAR(50) NOT NULL

)


--Enrollments Table
CREATE TABLE #StuEnrollments(
	StuEnrollId UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,
	StudentId UNIQUEIDENTIFIER NOT NULL,
	PrgVerId UNIQUEIDENTIFIER NOT NULL,
	StartDate DATETIME NOT NULL,
	ExpGradDate DATETIME NULL,
	DropReasonId UNIQUEIDENTIFIER NULL,
	DateDetermined DATETIME NULL,
	SysSTatusID INT NULL,
	LDA DATETIME NULL


)


--Program Versions Table
CREATE TABLE #PrgVersions(
	PrgVerId UNIQUEIDENTIFIER NOT NULL PRIMARY KEY ,
	TuitionEarningId UNIQUEIDENTIFIER NOT NULL,
	Weeks SMALLINT NOT NULL,
	[Hours] DECIMAL(9,2) NOT NULL,
	Credits DECIMAL(9,2) NOT NULL

)


--Tuition Earnings Table
CREATE TABLE #TuitionEarnings(
	TuitionEarningId UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,
	FullMonthBeforeDay TINYINT NULL,
	HalfMonthBeforeDay TINYINT NULL,
	RevenueBasisIdx TINYINT NOT NULL,
	PercentageRangeBasisIdx TINYINT NOT NULL,
	PercentToEarnIdx TINYINT NOT NULL,
	TakeHolidays BIT NULL,
	RemainingMethod BIT NULL

)


CREATE TABLE #TuitionEarningsPercentageRanges(
	TuitionEarningsPercentageRangeId UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,
	TuitionEarningId UNIQUEIDENTIFIER NOT NULL,
	UpTo DECIMAL(5,2) NOT NULL,
	EarnPercent DECIMAL(5,2) NOT NULL
)


CREATE TABLE #Results(
	ResultId UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,
	ClsSectionId UNIQUEIDENTIFIER NOT NULL,
	StuEnrollId UNIQUEIDENTIFIER NOT NULL,
	Credits DECIMAL(9,2) NULL,
	IsPass BIT NULL,
	IsCreditsEarned BIT NULL,
	IsCreditsAttempted BIT NULL

)


CREATE TABLE #Reqs(
	PrgVerId UNIQUEIDENTIFIER NOT NULL,
	TuitionEarningId UNIQUEIDENTIFIER NOT NULL,
	Descrip VARCHAR(150) NOT NULL,
	IsRequired BIT NOT NULL

)


--Exceptions Table
CREATE TABLE #StudentExceptions(
	StudentName VARCHAR(200) NOT NULL,
	StuEnrollId VARCHAR(50) NOT NULL,
	FeeLevelId INT NULL,
	TermStart DATETIME NULL,
	TermEnd DATETIME NULL,
	TransCode VARCHAR(100) NULL,
	TransAmount VARCHAR(50) NULL,
	Exception VARCHAR(250) NULL,
	TableName VARCHAR(50) NOT NULL


) 

--Table to be used for inserting records into saDeferredRevenues
CREATE TABLE #DeferredRevenues(
	DefRevenueId UNIQUEIDENTIFIER NOT NULL PRIMARY KEY,
	DefRevenueDate DATETIME NOT NULL,
	TransactionId UNIQUEIDENTIFIER NOT NULL,
	[Type] BIT NOT NULL,
	[Source] TINYINT NOT NULL,
	Amount MONEY NOT NULL,
	IsPosted BIT NOT NULL,
	ModUser VARCHAR(50) NOT NULL,
	ModDate DATETIME NOT NULL
)


--Table to be used for displaying the details of a transcode selected on the left hand side



----------------------------------------------------------------------------------------------------------------------------------------------
--Populate the Temp Tables
-----------------------------------------------------------------------------------------------------------------------------------------------
--Transactions Table
INSERT INTO #Transactions
SELECT 
		 T.TransactionId, 
		 (Select FirstName + ' ' + LastName
		  from arStudent S
		  where StudentId=(Select StudentId
						   from arStuEnrollments
						   where StuEnrollId=(Select StuEnrollId from saTransactions where TransactionId=T.TransactionId and saTransactions.Voided=0))) As StudentName, 
		
		(CASE
			WHEN UPPER(@StudentIdentifier)='SSN' then
				   (Select Coalesce(Case Len(S.SSN) When 9 then '***-**-' + SUBSTRING(SSN,6,4) else ' ' end, ' ') 
					from arStudent S 
					where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=(Select StuEnrollId from saTransactions where TransactionId=T.TransactionId and saTransactions.Voided=0)))
			WHEN LOWER(@StudentIdentifier)='studentid' THEN
					(Select Coalesce(StudentNumber, ' ')
					 from arStudent S 
					 where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=(Select StuEnrollId from saTransactions where TransactionId=T.TransactionId and saTransactions.Voided=0)))
			ELSE
					(Select ''
					 from arStudent S 
					 where StudentId=(Select StudentId from arStuEnrollments where StuEnrollId=(Select StuEnrollId from saTransactions where TransactionId=T.TransactionId and saTransactions.Voided=0)))
			END
		   ) AS StudentIdentifier,
		
	T.StuEnrollId, 
	T.TransCodeId, 
	T.TransAmount, 
	(Select Top 1 StartDate from arStudentLOAs where StuEnrollId=T.StuEnrollId order by StartDate desc) AS LOAStartDate, 
	(Select Top 1 EndDate from arStudentLOAs where StuEnrollId=T.StuEnrollId order by EndDate desc) AS LOAEndDate, 
	Coalesce((Select sum(Amount) from saDeferredRevenues where TransactionId=T.TransactionId),0) Earned, 
	T.FeeLevelId, 
	(Select StartDate from arTerm tm where tm.TermId=T.TermId) as TermStart, 
	(Select EndDate from arTerm tm where tm.TermId=T.TermId) as TermEnd,
	 te.PercentToEarnIdx AS percentToEarnIdx,te.RemainingMethod as RemainingMethod,
	 0.00 AS  DeferredRevenue  
	 
	From   saTransactions T, saTransCodes TC, arStuEnrollments SE,saTransTypes S, dbo.arPrgVersions pv, dbo.saTuitionEarnings te
	Where 

		(T.TransCodeId = TC.TransCodeId) 
		And    TC.DefEarnings = 1 
		And    SE.StuEnrollId=T.StuEnrollId 
		And    T.Voided=0 
		and    T.TransTypeId=S.TransTypeId and T.TransTypeId<>2 
		And    SE.StartDate <= @ReportDate
		And    T.TransDate <= @ReportDate
		And    T.CampusId = ISNULL(@CampusId,T.CampusId)
		And    T.TransDate >= ISNULL(@CutOffTransDate,T.TransDate)
		AND	   SE.PrgVerId=PV.PrgVerId
		AND    PV.TuitionEarningId=TE.TuitionEarningId
		--AND	   SE.StuEnrollId='6820107F-BA68-4081-8269-320ABE6BE526' --Dawn Higgins
		--AND	   T.TransactionId='C2CB1BDB-A6E3-4DC6-92D5-7935250CCD74' --Dawn Higgins Tuition Charged on 2012-03-05 00:00:00.000
		--AND		SE.StuEnrollId='B527B234-4961-46CA-A108-E5D6960F7E6D' --Ammanda Medina (Expelled on 8/2/2011 LDA 7/19/2011)
		--AND		T.TransactionId='8AAD7B80-C082-41E8-8F3D-9CF058929D67' --Ammanda Medina Tuition charged on 5/2/2011
		--And T.StuEnrollId='67887763-86CC-4829-90BA-B01C056191A0' --Aaron Defauw Tuition charged on 8/29/2011 $12,154.00
		--And T.TransactionId='4805ED14-748D-4660-B60B-D16228BFF180'  --Aaron Defauw Tuition charged on 8/29/2011 $12,154.00
		--And T.StuEnrollId='04F8E9F3-BA4C-4A3F-9C35-2365FD8BC5C7' --Aaron Rodgers Tuition charged on 3/5/2012 $12074.0000
		--And T.TransactionId='BCDA9AB4-C465-4B99-BDCC-9DE2F3ABFF7B'	--Aaron Rodgers Tuition charged on 3/5/2012 $12074.0000
		--AND T.StuEnrollId='B7FF0506-98E2-463E-97B9-1019AEC2F2A6'	--Aaron Renter Tuition charged on 1/3/2012 $12,830.00
		--AND T.TransactionId='30E7EE3D-ACC2-47F4-BEEC-002EC4D09B20'	--Aaron Renter Tuition charged on 1/3/2012 $12,830.00
		--AND T.StuEnrollId='CFE46667-C6EC-4EBB-AFB3-EA354881371F'	--Bobbie Vance Tuition charged on 1/3/2012 $13,400.00
		--AND T.TransactionId='78C35984-9EF3-4D3C-B5BC-001A862AA2C6'	--Bobbie Vance Tuition charged on 1/3/2012 $13,400.00
		--AND T.StuEnrollId='2923660B-5C3E-4962-B30B-920F05472D19'	--Anastasia Anderson Started 3/5/2012 Tuition Charged on 3/5/2012 $13,400
		--AND T.TransactionId='04AD729A-F8C3-4821-A6F0-6EC6A7DD5ACA'	--Anastasia Anderson Started 3/5/2012 Tuition Charged on 3/5/2012 $13,400

--TransCodes Table
INSERT INTO #TransCodes(TransCodeId,TransCodeDescrip,TableName,TotalDeferredRevenue,AccruedAmount,DeferredRevenueAmount,TotalAmount)
SELECT TC.TransCodeId, 
		   TC.TransCodeDescrip,
		   'TransCodes' AS TableName,
		   0.00 AS TotalDeferredRevenue,
		   0.00 AS AccruedAmount,
		   0.00 AS DeferredRevenueAmount, 
		   SUM(T.TransAmount) TotalAmount  
	From   saTransCodes TC, saTransactions T, saTransTypes S 
	Where (T.TransCodeId = TC.TransCodeId) 
	And    (TC.DefEarnings = 1) 
	And    T.CampusId = ISNULL(@CampusId,T.CampusId)                        
	And    T.Voided=0 
	And T.TransTypeId=S.TransTypeId
	and T.TransTypeId<>2     
	Group By TC.TransCodeId, TC.TransCodeDescrip
	
	--SELECT ((12074*90)/100)*100/736
--Enrollments Table
INSERT INTO #StuEnrollments
SELECT DISTINCT SE.StuEnrollId, 
			   SE.StudentId, 
			   SE.PrgVerId, 
			   SE.StartDate, 
			   SE.ExpGradDate, 
			   SE.DropReasonId, 
			   SE.DateDetermined, 
			   (Select SysStatusId from SySTatusCodes where StatusCodeId in (Select StatusCodeId  from arStuEnrollments where StuENrollId=SE.StuEnrollId) ) SysSTatusID ,
			   (select Max(LDA) from 
				( 
					select max(AttendedDate)as LDA from arExternshipAttendance where StuEnrollId=SE.StuEnrollID 
					union all 
					select max(MeetDate) as LDA from atClsSectAttendance where StuEnrollId=SE.StuEnrollID and Actual >= 1 
					union all 
					select max(AttendanceDate) as LDA from atAttendance where EnrollId=SE.StuEnrollID and Actual >=1 
					union all 
					select max(RecordDate) as LDA from arStudentClockAttendance where StuEnrollId=SE.StuEnrollID and (ActualHours >=1.00 and  ActualHours <> 99.00 and ActualHours <> 999.00 and ActualHours <> 9999.00) 
					union all 
					select max(MeetDate) as LDA from atConversionAttendance where StuEnrollId=SE.StuEnrollID and (Actual >=1.00 and  Actual <> 99.00 and Actual <> 999.00 and Actual <> 9999.00) 
				 Union All select LDA from arStuEnrollments where StuEnrollId=SE.StuEnrollID  
				)T1 )as LDA	
				
From   arStuEnrollments SE, saTransactions T, saTransCodes TC 
Where 
	   (SE.StuEnrollId = T.StuEnrollId) 
And    (T.TransCodeId=TC.TransCodeId) 
And    TC.DefEarnings = 1 
And    T.Voided=0 
And    T.CampusId = ISNULL(@CampusId,T.CampusId) 



--Program Versions Table
INSERT INTO #PrgVersions
SELECT DISTINCT PV.PrgVerId, 
					PV.TuitionEarningId, 
					PV.Weeks, 
					PV.Hours, 
					PV.Credits 
From   arPrgVersions PV, arStuEnrollments SE, saTransactions T, saTransCodes TC 
Where 
	   (PV.PrgVerId=SE.PrgVerId) 
And    (SE.StuEnrollId = T.StuEnrollId) 
And    (T.TransCodeId=TC.TransCodeId) 
And    (TC.DefEarnings = 1) 
And    T.Voided=0 
And    T.CampusId = ISNULL(@CampusId,T.CampusId) 


--Tuition Earnings Table
INSERT INTO #TuitionEarnings
SELECT DISTINCT	TE.TuitionEarningId, 
						TE.FullMonthBeforeDay, 
						TE.HalfMonthBeforeDay, 
						TE.RevenueBasisIdx, 
						TE.PercentageRangeBasisIdx, 
						TE.PercentToEarnIdx ,
						TE.TakeHolidays, 
						TE.RemainingMethod 

From   saTuitionEarnings TE, arPrgVersions PV, arStuEnrollments SE, saTransactions T, saTransCodes TC 
Where 
	   (TE.TuitionEarningId=PV.TuitionEarningId) 
And    (PV.PrgVerId=SE.PrgVerId) 
And    (SE.StuEnrollId=T.StuEnrollId) 
And    (T.TransCodeId=TC.TransCodeId) 
And    (TC.DefEarnings = 1) 
And    T.Voided=0 
And    T.CampusId = ISNULL(@CampusId,T.CampusId)


--TuitionEarningsPercentageRanges Table
INSERT INTO #TuitionEarningsPercentageRanges
SELECT Distinct
	   TEPR.TuitionEarningsPercentageRangeId, 
	   TE.TuitionEarningId, 
	   TEPR.UpTo, 
	   TEPR.EarnPercent 
From   saTuitionEarningsPercentageRanges TEPR, saTuitionEarnings TE, arPrgVersions PV, arStuEnrollments SE, saTransactions T, saTransCodes TC 
Where 
	   (TEPR.TuitionEarningId=TE.TuitionEarningId) 
And    (TE.TuitionEarningId=PV.TuitionEarningId) 
And    (PV.PrgVerId=SE.PrgVerId) 
And    (SE.StuEnrollId=T.StuEnrollId) 
And    (T.TransCodeId=TC.TransCodeId) 
And    (TC.DefEarnings = 1) 
And    T.CampusId = ISNULL(@CampusId,T.CampusId)    
Order by TEPR.UpTo 


--Results Table
INSERT INTO #Results
SELECT DISTINCT  
	   R.ResultId,  
	   CS.ClsSectionId,  
	   R.StuEnrollId,  
	   RQ.Credits,  
	   (Select IsPass from arGradeSystemDetails where GrdSysDetailId = R.GrdSysDetailId) as IsPass,  
	   (Select IsCreditsEarned from arGradeSystemDetails where GrdSysDetailId = R.GrdSysDetailId) as IsCreditsEarned,  
	   (Select IsCreditsAttempted from arGradeSystemDetails where GrdSysDetailId = R.GrdSysDetailId) as IsCreditsAttempted  
FROM   saTransactions T, saTransCodes TC, arResults R, arClassSections CS, arReqs RQ  
WHERE  
		T.TransCodeId = TC.TransCodeId  
AND     TC.DefEarnings = 1  
AND     T.Voided=0  
AND     T.StuEnrollId = R.StuEnrollId  
AND		R.TestId = CS.ClsSectionId  
AND 	CS.ReqId = RQ.ReqId 
And    T.CampusId = ISNULL(@CampusId,T.CampusId)


--Reqs Table
INSERT INTO #Reqs
SELECT DISTINCT 
	   PV.PrgVerId, 
	   PV.TuitionEarningId, 
	   R.Descrip, 
	   IsRequired 
FROM   arPrgVersions PV, arStuEnrollments SE, saTransactions T, saTransCodes TC, arProgVerDef PVD, arReqs R 
WHERE 
	   PV.PrgVerId = SE.PrgVerId 
And    SE.PrgVerId=PVD.PrgVerId 
And    SE.StuEnrollId = T.StuEnrollId 
And    T.TransCodeId=TC.TransCodeId 
And    TC.DefEarnings = 1 
And    T.Voided=0 
And    PVD.ReqId=R.ReqId  
And    T.CampusId = ISNULL(@CampusId,T.CampusId) 





-------------------------------------------------------------------------------------------------------------------------------------------------------------------
--This section is where the calculations are performed.
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
 DECLARE transCursor CURSOR FAST_FORWARD FORWARD_ONLY  FOR
	SELECT *
	FROM #Transactions
	

OPEN transCursor
FETCH NEXT FROM transCursor
INTO @TransactionId,@StudentName,@StudentIdentifier,@StuEnrollId,@TransCodeId,@TransAmount,@LOAStartDate,@LOAEndDate,@Earned,@FeeLevelId,
	 @TermStart,@TermEnd,@percentToEarnIdx,@RemainingMethod,@DeferredRevenue


WHILE @@FETCH_STATUS=0
	BEGIN
	
		--Get the status of the student, if the student is of nostart status and it was changed from currently attending
		--Then get the date they modified the status
		--Post deferred revenue for the student for the remaining amount in the next month and then donot post deferred revenue at all, once they have earned all the revenue
		--Modified on july 27 2009
		DECLARE @nostartdate DATETIME
		DECLARE @ExceptionNo INTEGER
		DECLARE @TransCodeDescrip VARCHAR(100)
		DECLARE @SpecialCAseUnearnedRevExistsandRemainingDurationNotExists As BIT
		DECLARE @ExitFor AS BIT
		DECLARE @halfMonthBeforeDay As INTEGER
		DECLARE @fullMonthBeforeDay As INTEGER
		
		SET @SpecialCAseUnearnedRevExistsandRemainingDurationNotExists = 0
		SET @ExitFor = 0
		
		SET @nostartdate = '1/1/1800'
		
		IF (SELECT SysSTatusID FROM #StuEnrollments d WHERE d.StuEnrollId=@StuEnrollId AND d.SysSTatusID=8) IS not NULL
			BEGIN
				SET @nostartdate=(SELECT Top 1 MOdDate from syStudentStatusChanges where StuEnrollId= @StuEnrollId
					and NewStatusID in (Select StatusCodeId from systatusCodes where SysStatusId=8) 
					and OrigStatusID in (Select StatusCodeId from systatusCodes where SysStatusId=9 )) 
			
			end
		
		--This is the calculated duration in months of the term
		DECLARE @termStart1 DATE
		DECLARE @termEnd1 DATE
		
		SET @termStart1='1/1/1800'
		SET @termEnd1='1/1/1800'
		
		--Calculate last date of attendance
		DECLARE @lda DATE
		
		IF (SELECT DropReasonId FROM #StuEnrollments WHERE StuEnrollId=@StuEnrollId) IS null
			BEGIN
				SET @lda = '1/1/5000' --Use 1/1/5000 as a max date				
			END	
				
		ELSE
			BEGIN
				IF (SELECT DateDetermined FROM #StuEnrollments WHERE StuEnrollId=@StuEnrollId) IS null
					BEGIN
						IF (SELECT LDA FROM #StuEnrollments WHERE StuEnrollId=@StuEnrollId) IS NOT NULL
							BEGIN
								SET @lda = (SELECT LDA FROM #StuEnrollments WHERE StuEnrollId=@StuEnrollId)
							END
						ELSE
							BEGIN
								SET @lda = '1/1/1800'
							END
				
					END
				ELSE
					BEGIN
						SET @lda = (SELECT DateDetermined FROM #StuEnrollments WHERE StuEnrollId=@StuEnrollId)
					END
				
			END
		
		
		IF @lda='1/1/1800'
			BEGIN
				--Exception date determined is null for this student
				--Add a record to the exceptions table
				SET @ExceptionNo = 3
								
				SET @TransCodeDescrip=(SELECT TransCodeDescrip FROM #TransCodes tc WHERE tc.TransCodeId=@TransCodeId)
				
				INSERT INTO #StudentExceptions
				VALUES(@StudentName,@StuEnrollId,@FeeLevelId,@TermStart,@TermEnd,@TransCodeDescrip,@TransAmount,'The student has a drop reason and no date determined.','StudentExceptions')
			
			END
		
		ELSE
			BEGIN			
		
				If @lda > @reportDate
					BEGIN
						SET @lda = '1/1/5000'
					END 
				 
				DECLARE @FeeLevelId1 INT
				SET @FeeLevelId1=0
				
				IF @FeeLevelId IS NOT NULL
					BEGIN
						SET @FeeLevelId1=@FeeLevelId
					END
				 
				DECLARE @strTransactionID VARCHAR(50)
				SET @strTransactionID = RTRIM(CONVERT(VARCHAR(50),@TransactionId))
				
				DECLARE @defrevPostedDate DATE
				
				SET @defrevPostedDate=(Select MAX(DefrevenueDate) from saDeferredRevenues where TransactionId= @TransactionId)
				
				
				--Modified by Saraswathi On June 03 2009
				--1- Term and 3- Course 2- Program level
				-- Course added to function as term.
				IF (@FeeLevelId1=1 OR @FeeLevelId1=3)
					BEGIN
					
						SET @termStart1 = @TermStart
						SET @termEnd1 = (SELECT dbo.MinDate(@TermEnd,@lda))
											
						IF @nostartdate <> '1/1/1800'
							BEGIN					
								SET @termEnd1 = (SELECT dbo.MinDate(@termEnd1,@nostartdate))				
							END
						
					END
					
				--Case 2 is program version
				--Modified by Saraswathi On June 3 2009
				IF @FeeLevelId1=2
					BEGIN
						SET @termStart1 = (SELECT StartDate FROM #StuEnrollments WHERE StuEnrollId=@StuEnrollId)
					
						IF (SELECT ExpGradDate FROM #StuEnrollments WHERE StuEnrollId=@StuEnrollId) IS NULL
							BEGIN
								DECLARE @programDurationInWeeks INT
								SET @programDurationInWeeks = (SELECT Weeks 
															   FROM #StuEnrollments se, #PrgVersions pv
															   WHERE se.PrgVerId=pv.PrgVerId
															   AND se.StuEnrollId=@StuEnrollId)
							
								SET @termEnd1 = (SELECT dbo.MinDate(DATEADD(DAY,@programDurationInWeeks*7,@termStart1),@lda)) 
							
							END
						ELSE
							BEGIN
								DECLARE @ExpGradDate DATE
								SET @ExpGradDate = (SELECT ExpGradDate FROM #StuEnrollments WHERE StuEnrollId=@StuEnrollId)
								SET @termEnd1 = (SELECT dbo.MinDate(@ExpGradDate,@lda))
							
							
							END	
							
						IF @nostartdate <> '1/1/1800'		
							BEGIN
								SET @termEnd1 = (SELECT dbo.MinDate(@termEnd1,@nostartdate))
							END
					
					
					END			
				
					
				
				IF (@FeeLevelId1 = 0) --No FeeLevelId is on the Transaction Record so add record to the exceptions table	
					BEGIN
						SET @ExceptionNo = 1
										
						SET @TransCodeDescrip=(SELECT TransCodeDescrip FROM #TransCodes tc WHERE tc.TransCodeId=@TransCodeId)
						
						INSERT INTO #StudentExceptions
						VALUES(@StudentName,@StuEnrollId,@FeeLevelId,@TermStart,@TermEnd,@TransCodeDescrip,@TransAmount,'There is no Fee Level Id defined for the student','StudentExceptions')
					
					END
			
				ELSE 
					BEGIN
						DECLARE @termDuration DECIMAL
						DECLARE @NewtermDuration DECIMAL
						DECLARE @RemainingMethod1 BIT
						
						SET @termDuration = 0
						SET @NewtermDuration = 0
						
						SET @RemainingMethod1 = (SELECT RemainingMethod
												FROM #StuEnrollments se, #PrgVersions pv, #TuitionEarnings te
												WHERE se.PrgVerId=pv.PrgVerId
												AND pv.TuitionEarningId=te.TuitionEarningId
												AND se.StuEnrollId=@StuEnrollId)	
												
							
						IF @termStart1=@termEnd1
							BEGIN
								SET @ExceptionNo = 2
						
								SET @TransCodeDescrip=(SELECT TransCodeDescrip FROM #TransCodes tc WHERE tc.TransCodeId=@TransCodeId)
						
								INSERT INTO #StudentExceptions
								VALUES(@StudentName,@StuEnrollId,@FeeLevelId,@TermStart,@TermEnd,@TransCodeDescrip,@TransAmount,'Term Start Date and Term End Date/Date Determined/LDA are the same','StudentExceptions')
						
					
							END	
							
						ELSE
							BEGIN
								--Added by Saraswathi lakshmanan On July 1 2009
								--check whether to apply a straight percentage or a conversion table
								DECLARE @DeferredRevenuePostingBasedonHolidays VARCHAR(5)
								
								IF (SELECT TakeHolidays
									FROM #StuEnrollments se, #PrgVersions pv, #TuitionEarnings te
									WHERE se.StuEnrollId=@StuEnrollId
									AND se.PrgVerId=pv.PrgVerId
									AND pv.TuitionEarningId=te.TuitionEarningId) is NOT NULL
									BEGIN
										IF (SELECT TakeHolidays
											FROM #StuEnrollments se, #PrgVersions pv, #TuitionEarnings te
											WHERE se.StuEnrollId=@StuEnrollId
											AND se.PrgVerId=pv.PrgVerId
											AND pv.TuitionEarningId=te.TuitionEarningId) = 1
											BEGIN
												SET @DeferredRevenuePostingBasedonHolidays='yes'																				
											END
										ELSE
											BEGIN
												SET @DeferredRevenuePostingBasedonHolidays='no'												
											END
									
									
									END
								ELSE
									BEGIN
										SET @DeferredRevenuePostingBasedonHolidays='no'
									
									END
									
									
								DECLARE @newStartDate As Date
								SET @newStartDate=(SELECT dbo.MaxDate(@termStart1, DATEADD(DAY,1,@defrevPostedDate)))
								
								DECLARE @startDate Date 
								SET @startDate = @termStart1
								
								--Code modified by Saraswathi Lakshmanan on Sept 10 2010
								--INclude LOA to elapsed time and term duration only if LOA is between the elapsed time period.
								DECLARE @Trad_numberofHOlidaysbetweentheGivenPeriod As INTEGER
								DECLARE @Rem_numberofHOlidaysbetweentheGivenPeriod As INTEGER
								DECLARE @Trad_numberofLOAdaysbetweentheGivenPeriod As INTEGER
								DECLARE @Rem_numberofLOAdaysbetweentheGivenPeriod As Integer 								
								
								SET @Trad_numberofHOlidaysbetweentheGivenPeriod = (SELECT dbo.NumberofHolidaysBetweentheGivenPeriod(@reportDate, @startDate, @campusId))					
								set @Rem_numberofHOlidaysbetweentheGivenPeriod = (SELECT dbo.NumberofHolidaysBetweentheGivenPeriod(@reportDate, @newStartDate, @campusId))
								SET @Trad_numberofLOAdaysbetweentheGivenPeriod = (SELECT dbo.NumberofLOADaysBetweentheGivenPeriod(@reportDate, @startDate, @StuEnrollId))
								SET @Rem_numberofLOAdaysbetweentheGivenPeriod = (SELECT dbo.NumberofLOADaysBetweentheGivenPeriod(@reportDate, @newStartDate, @StuEnrollId))
																								
								IF @termStart1 <> '1/1/1800' And @termEnd1 <> '1/1/1800'
									BEGIN										
										IF @percentToEarnIdx = 1
											BEGIN												
												--If it is a equal percentage every month  Then there is no holidays into considereation
												--termDuration = (termEnd.Subtract(termStart).TotalDays) / 30
												SET @termDuration = (DateDiff(Month, @startDate, @termEnd1) + 1 )*100
												SET @NewtermDuration = (DateDiff(Month, @newStartDate, @termEnd1) + 1 )*100
												SET @DeferredRevenuePostingBasedonHolidays = 'no'
											
											END
										ELSE
											BEGIN												
												If (DateDiff(DayOfYear, @startDate, @termEnd1) + 1 - @Trad_numberofLOAdaysbetweentheGivenPeriod) <= 0
													BEGIN														
														SET @termDuration = 0
													END
												ELSE
													BEGIN														
														SET @termDuration = ((DateDiff(DayOfYear, @startDate, @termEnd1) + 1 - @Trad_numberofLOAdaysbetweentheGivenPeriod)*100) / 30
														
													END
													
												 If (DateDiff(DayOfYear, @newStartDate, @termEnd1) + 1 - @Rem_numberofLOAdaysbetweentheGivenPeriod) <= 0
													BEGIN
														SET @NewtermDuration = 0
													END
												ELSE
													BEGIN
														SET @NewtermDuration = ((DateDiff(DayOfYear, @newStartDate, @termEnd1) + 1 - @Rem_numberofLOAdaysbetweentheGivenPeriod)*100) / 30
													
													END
												
												If LOWER(@DeferredRevenuePostingBasedonHolidays) = 'yes'
													BEGIN
														If (DateDiff(DayOfYear, @startDate, @termEnd1) + 1 - @Trad_numberofLOAdaysbetweentheGivenPeriod - @Trad_numberofHOlidaysbetweentheGivenPeriod) <= 0
															BEGIN
																SET @termDuration = 0
															END
														ELSE
															BEGIN
																SET @termDuration = ((DateDiff(DayOfYear, @startDate, @termEnd1) + 1 - @Trad_numberofLOAdaysbetweentheGivenPeriod - @Trad_numberofHOlidaysbetweentheGivenPeriod)*100) / 30
															END
															
														If (DateDiff(DayOfYear, @newStartDate, @termEnd1) + 1 - @Rem_numberofLOAdaysbetweentheGivenPeriod - @Rem_numberofHOlidaysbetweentheGivenPeriod) <= 0
															BEGIN
																SET @NewtermDuration = 0
															END
														ELSE	
															BEGIN
																SET @NewtermDuration = ((DateDiff(DayOfYear, @newStartDate, @termEnd1) + 1 - @Rem_numberofLOAdaysbetweentheGivenPeriod - @Rem_numberofHOlidaysbetweentheGivenPeriod)*100) / 30
															END
															
													
													END
													
													
												
											END
											
											
									
									
									END
									
								DECLARE @EarnedRevenue As Decimal
								DECLARE @UnearnedRevenue As DECIMAL
								
								SET @EarnedRevenue = ISNULL((Select SUM(Amount) from saDeferredRevenues where TransactionId= @TransactionId),0)
								SET @UnearnedRevenue = @TransAmount - @EarnedRevenue								
								
								--ignore all calculations if the termDuration is zero or negative
								--If percentToEarnIdx = 0 And Not (termDuration > 0) Then Exit For
								--Modified by Saraswathi on July 1 2009								
								If (@RemainingMethod = 0)
									BEGIN
										If (@percentToEarnIdx = 0 Or @percentToEarnIdx = 1) And (@termDuration <= 0 And @UnearnedRevenue > 0)
											BEGIN
												SET @SpecialCAseUnearnedRevExistsandRemainingDurationNotExists = 1
												
											END
										ELSE
											BEGIN
												IF (@percentToEarnIdx = 0 Or @percentToEarnIdx = 1) And Not (@termDuration > 0) And (@UnearnedRevenue < 0)
													BEGIN
														SET @EXITFOR = 1	
													END
																				
											END									
									END
								
								If @RemainingMethod = 1
									BEGIN
										If (@percentToEarnIdx = 0 Or @percentToEarnIdx = 1) And (@NewtermDuration <= 0 And @UnearnedRevenue > 0)
											BEGIN
												SET @SpecialCAseUnearnedRevExistsandRemainingDurationNotExists = 1									
											END
										ELSE
											BEGIN
												IF (@percentToEarnIdx = 0 Or @percentToEarnIdx = 1) And Not (@NewtermDuration > 0) And (@UnearnedRevenue < 0)
													BEGIN
														SET @EXITFOR = 1
													END										
												
											END
									
									END
								
								--The rest of the code should only execute if @EXITFOR is still 0
								IF @ExitFor = 0
									BEGIN
										--this is the elapsed time in months from the beginning of the period to the report date
										DECLARE @elapsedTime As DECIMAL = 0.00
										DECLARE @newElapsedTime As DECIMAL = 0.00
										
										If LOWER(@DeferredRevenuePostingBasedonHolidays) = 'yes'
											BEGIN
												IF (DateDiff(DayOfYear, @startDate, @reportDate) + 1 - @Trad_numberofLOAdaysbetweentheGivenPeriod - @Trad_numberofHOlidaysbetweentheGivenPeriod) <= 0
													BEGIN
														SET @elapsedTime = 0
													END
												ELSE 
													BEGIN
														SET @elapsedTime = ((DateDiff(DayOfYear, @startDate, @reportDate) + 1 - @Trad_numberofLOAdaysbetweentheGivenPeriod - @Trad_numberofHOlidaysbetweentheGivenPeriod)*100) / 30
													END
													
												If @RemainingMethod = 1
													BEGIN
														If (DateDiff(DayOfYear, @newStartDate, @reportDate) + 1 - @Rem_numberofLOAdaysbetweentheGivenPeriod - @Rem_numberofHOlidaysbetweentheGivenPeriod) <= 0
															BEGIN
																SET @newElapsedTime = 0
															END
														ELSE	
															BEGIN
																SET @newElapsedTime = ((DateDiff(DayOfYear, @newStartDate, @reportDate) + 1 - @Rem_numberofLOAdaysbetweentheGivenPeriod - @Rem_numberofHOlidaysbetweentheGivenPeriod)*100) / 30
															END
													END
													
													
											END
										ELSE
											BEGIN												
												
												If (DateDiff(DayOfYear, @startDate, @reportDate) + 1 - @Trad_numberofLOAdaysbetweentheGivenPeriod) <= 0
													BEGIN														
														SET @elapsedTime = 0
													END
												ELSE
													BEGIN														
														--The original code from the VB class used (DateDiff(DayOfYear, @startDate, @reportDate) + 1 - @Trad_numberofLOAdaysbetweentheGivenPeriod)/100
														--However, in SQL Server the value returned is 0 rather than 0.90. For SQL Server we have to first mulitply by 100 and then remember
														--later on to divide by 100 since here we are now going to get 90.
														SET @elapsedTime = ((DateDiff(DayOfYear, @startDate, @reportDate) + 1 - @Trad_numberofLOAdaysbetweentheGivenPeriod)*100) / 30
														
													END
													
												If @RemainingMethod = 1
													BEGIN
														 If (DateDiff(DayOfYear, @newStartDate, @reportDate) + 1 - @Rem_numberofLOAdaysbetweentheGivenPeriod) <= 0
															 BEGIN
																SET @newElapsedTime = 0
															 END
														ELSE
															BEGIN
																SET @newElapsedTime = ((DateDiff(DayOfYear, @newStartDate, @reportDate) + 1 - @Rem_numberofLOAdaysbetweentheGivenPeriod)*100) / 30
															END
														 
													END
													
											
											END
										
										 --elapsed time can not be negative
										If @elapsedTime < 0 
											BEGIN
												SET @elapsedTime = 0.0
											END
											
										If @newElapsedTime < 0 
											BEGIN
												SET @newElapsedTime = 0.0
											END	
										
										--if the student started before this day of the month .. round elapsed time to a half month
										DECLARE @roundedElapsedTime As Decimal
										DECLARE @NewroundedElapsedTime As Decimal 
										
										SET  @roundedElapsedTime = @elapsedTime
										SET	@NewroundedElapsedTime = @newElapsedTime
										
										--round elapsed time only if the elapsed time is > 0
										If @elapsedTime > 0
											BEGIN
												If @percentToEarnIdx = 1
													BEGIN
														SET @roundedElapsedTime = (DateDiff(Month, @startDate, @reportDate) + 1)*100
													END
												ELSE
													BEGIN
														IF DATEPART(MONTH,@reportDate) = DATEPART(MONTH,@startDate) And DATEPART(YEAR,@reportDate) = DATEPART(YEAR,@startDate)
															BEGIN
																
																SET @halfMonthBeforeDay = (SELECT te.HalfMonthBeforeDay
																						   FROM #StuEnrollments se, #PrgVersions pv, #TuitionEarnings te
																						   WHERE se.StuEnrollId=@StuEnrollId
																						   AND se.PrgVerId=pv.PrgVerId
																						   AND pv.TuitionEarningId=te.TuitionEarningId)
																						   
																IF DATEPART(DAY,@startDate) <= @halfMonthBeforeDay And @halfMonthBeforeDay > 0
																	BEGIN
																		 SET @roundedElapsedTime = 0.5*100
																	
																	END
																
																
																SET @fullMonthBeforeDay = (SELECT te.FullMonthBeforeDay
																						   FROM #StuEnrollments se, #PrgVersions pv, #TuitionEarnings te
																						   WHERE se.StuEnrollId=@StuEnrollId
																						   AND se.PrgVerId=pv.PrgVerId
																						   AND pv.TuitionEarningId=te.TuitionEarningId)
																						   
																If DATEPART(DAY,@startDate) <= @fullMonthBeforeDay And @fullMonthBeforeDay > 0
																	BEGIN
																		SET @roundedElapsedTime = 1*100
																	END																	
																					   
															END
													END
													
											
											END
											
										If @newElapsedTime > 0
											BEGIN
												If @percentToEarnIdx = 1
													BEGIN
														SET @NewroundedElapsedTime = (DateDiff(Month, @newStartDate, @reportDate) + 1)*100
													END
												ELSE
													BEGIN
														 If DATEPART(MONTH,@reportDate) = DATEPART(MONTH,@startDate) And DATEPART(YEAR,@reportDate) = DATEPART(YEAR,@startDate)
															BEGIN
																SET @halfMonthBeforeDay = (SELECT te.HalfMonthBeforeDay
																						   FROM #StuEnrollments se, #PrgVersions pv, #TuitionEarnings te
																						   WHERE se.StuEnrollId=@StuEnrollId
																						   AND se.PrgVerId=pv.PrgVerId
																						   AND pv.TuitionEarningId=te.TuitionEarningId)
																IF DATEPART(DAY,@startDate) <= @halfMonthBeforeDay And @halfMonthBeforeDay > 0
																	BEGIN
																		SET @NewroundedElapsedTime = 0.5*100
																	END
																	
																SET @fullMonthBeforeDay = (SELECT te.FullMonthBeforeDay
																						   FROM #StuEnrollments se, #PrgVersions pv, #TuitionEarnings te
																						   WHERE se.StuEnrollId=@StuEnrollId
																						   AND se.PrgVerId=pv.PrgVerId
																						   AND pv.TuitionEarningId=te.TuitionEarningId)
																						   
																If DATEPART(DAY,@startDate) <= @fullMonthBeforeDay And @fullMonthBeforeDay > 0
																	BEGIN
																		SET @NewroundedElapsedTime = 1*100
																	END						   
																
															END
													
													
													END
											
											END											
											
											--check that rounded elapsed time does not exceed term duration
											IF @roundedElapsedTime > @termDuration
												BEGIN
													SET @roundedElapsedTime = @termDuration
												END 
												
											IF @NewroundedElapsedTime > @NewtermDuration
												BEGIN
													SET @NewroundedElapsedTime = @NewtermDuration
												END 
												
											IF (@percentToEarnIdx = 0 Or @percentToEarnIdx = 1)
												BEGIN											
													If @RemainingMethod = 1
														BEGIN
															 IF @NewtermDuration <= 0 And @SpecialCAseUnearnedRevExistsandRemainingDurationNotExists = 1
																BEGIN
																	 UPDATE #Transactions
																	 SET DeferredRevenue = @UnearnedRevenue
																	 WHERE TransactionId = @TransactionId
																	 AND StuEnrollId = @StuEnrollId														
																END
															ELSE
																BEGIN
																	IF @NewtermDuration <> 0
																		BEGIN																
																			
																			 --apply straight line percentage
																			 UPDATE #Transactions
																			 SET DeferredRevenue =  (@UnearnedRevenue * @NewroundedElapsedTime) / (@NewtermDuration)
																			 WHERE TransactionId = @TransactionId
																			 AND StuEnrollId = @StuEnrollId	
																		END
																	ELSE
																		BEGIN
																			--Exception
																			--CAse 1: feelevelid is not set,
																			--Case 2:term duration =0 ,  term Start and term End is the same or term start and LDA or Datedetermined is the same..
																			UPDATE #Transactions
																			SET DeferredRevenue = 0, Earned = 0
																			WHERE TransactionId = @TransactionId
																			AND StuEnrollId = @StuEnrollId
																		END
																	 
																
																END
															
																
														END
													
													ELSE
														BEGIN
															 IF @termDuration <= 0 And @SpecialCAseUnearnedRevExistsandRemainingDurationNotExists = 1
																BEGIN
																	UPDATE #Transactions
																	SET DeferredRevenue = @TransAmount
																	WHERE TransactionId = @TransactionId
																	AND StuEnrollId = @StuEnrollId	
																END
															ELSE
																BEGIN
																	If @termDuration <> 0
																		BEGIN																			
																			-- apply straight line percentage
																			UPDATE #Transactions
																			SET DeferredRevenue =  (@TransAmount *@roundedElapsedTime) / (@termDuration)
																			WHERE TransactionId = @TransactionId
																			AND StuEnrollId = @StuEnrollId
																		END
																	ELSE
																		BEGIN
																			--Exception
																			--CAse 1: feelevelid is not set,
																			--Case 2:term duration =0 ,  term Start and term End is the same or term start and LDA or Datedetermined is the same..
																			UPDATE #Transactions
																			SET DeferredRevenue = 0, Earned = 0
																			WHERE TransactionId = @TransactionId
																			AND StuEnrollId = @StuEnrollId
																		END
																
																END
														
														END
													
													
												END
												
											IF (@percentToEarnIdx = 2)
												BEGIN											
													DECLARE @totalCredits As INTEGER
													DECLARE @PercentageRangeBasisIdx As INTEGER
													DECLARE @hoursScheduled As Decimal = 0.0
													DECLARE @hoursCompleted AS DECIMAL = 0.0
													DECLARE @totalProgramHours As DECIMAL = 0.00
													DECLARE @ClsSectionId VARCHAR(50)
													 
													SET @PercentageRangeBasisIdx=(SELECT PercentageRangeBasisIdx
																				   FROM #StuEnrollments se, #PrgVersions pv, #TuitionEarnings te
																				   WHERE se.StuEnrollId=@StuEnrollId
																				   AND se.PrgVerId=pv.PrgVerId
																				   AND pv.TuitionEarningId=te.TuitionEarningId)
																				   
													DECLARE @percentArgument As INTEGER
													
													IF @PercentageRangeBasisIdx=0
														BEGIN													
															-- Program Length
															--Modified by Saraswathi lakshmanan
															--When the term duration is zero the percent argument is considered to be zero . Since it is giving divide by zero error
															 IF @termDuration > 0
																BEGIN
																	SET @percentArgument = @roundedElapsedTime * 100 / @termDuration
																END
															 ELSE
																BEGIN
																	SET @percentArgument = 0.0														
																END														 
														
														END
														
													IF @PercentageRangeBasisIdx=1
														BEGIN													
															--Credits Earned
															-- add up Credits Earned
															DECLARE @creditsEarned As Integer = 0
															
															SET @creditsEarned = (SELECT SUM(Credits) 
																				  FROM #Results
																				  WHERE StuEnrollId=@StuEnrollId
																				  AND IsCreditsEarned IS NOT NULL
																				  AND IsCreditsEarned = 1)
																				  
															-- this is the total number of credits for the program
															SET @totalCredits = (SELECT pv.Credits
																				  FROM #StuEnrollments se, #PrgVersions pv
																				  WHERE se.StuEnrollId=@StuEnrollId
																				  AND se.PrgVerId=pv.PrgVerid)
																				  
															-- Credits Earned
															If @totalCredits > 0
																BEGIN
																	SET @percentArgument = @creditsEarned * 100.0 / @totalCredits 
																END
															ELSE
																BEGIN
																	SET @percentArgument = 0.0
																END
														
														END
														
													IF @PercentageRangeBasisIdx=2
														BEGIN
															--Credits Attempted
															--add up Credits Attempted
															 DECLARE @creditsAttempted As Integer = 0
															 
															 SET @creditsAttempted = (SELECT SUM(Credits) 
																					  FROM #Results
																					  WHERE StuEnrollId=@StuEnrollId
																					  AND IsCreditsAttempted IS NOT NULL
																					  AND IsCreditsAttempted = 1)
																					  
															--this is the total number of credits for the program
															SET @totalCredits = (SELECT pv.Credits
																				  FROM #StuEnrollments se, #PrgVersions pv
																				  WHERE se.StuEnrollId=@StuEnrollId
																				  AND se.PrgVerId=pv.PrgVerid)
																				  
															-- Credits Attempted
															 IF @totalCredits > 0
																BEGIN
																	SET @percentArgument = @creditsAttempted * 100.0 / @totalCredits														
																END 
															ELSE	
																BEGIN
																	 SET @percentArgument = 0.0
																END								 
														
														
														END
														
														
													IF @PercentageRangeBasisIdx=3
														BEGIN													
															-- Courses completed
															--get the courses for the program version
															 DECLARE @numberOfCourses As INTEGER
															 
															 SET @numberOfCourses = (SELECT COUNT(*) 
																					 FROM #StuEnrollments se, #PrgVersions pv, #Reqs rq
																					 WHERE se.StuEnrollId=@StuEnrollId
																					 AND se.PrgVerId=pv.PrgVerId
																					 AND pv.PrgVerId=rq.PrgVerId)
																					 
															--calculate number of courses taken
															DECLARE @numberOfCoursesTaken As Integer = 0
															
															SET @numberOfCoursesTaken = (SELECT COUNT(*)
																						 FROM #StuEnrollments se, #Results rs
																						 WHERE se.StuEnrollId=@StuEnrollId
																						 AND se.StuEnrollId=rs.StuEnrollId
																						 AND rs.IsPass IS NOT NULL
																						 AND rs.IsPass=1)
																						 
															--'Percentage of courses completed
															If @numberOfCourses > 0
																BEGIN
																	SET @percentArgument = @numberOfCoursesTaken * 100.0 / @numberOfCourses 
																END
															ELSE
																BEGIN
																	SET @percentArgument = 0.0
																END
															
														
														END
														
														
													IF @PercentageRangeBasisIdx=4
														BEGIN
															--get number of scheduled hours												
																
															--If attendance is being tracked by class then we need to get the total scheduled hours so far for those classes
															--If attendance is by day there is no need to use the cursor as we can go straight to the arStudentClockAttendance table.
															IF LOWER((SELECT dbo.AppSettings('TrackSapAttendance',@CampusId))) = 'byclass'
																BEGIN
																	DECLARE ResultCursor CURSOR FORWARD_ONLY  FAST_FORWARD FOR	
																		SELECT ClsSectionId
																		FROM #Results rs
																		WHERE rs.StuEnrollId=@StuEnrollId
																		
																	OPEN ResultCursor
																	FETCH NEXT FROM ResultCursor
																	INTO @ClsSectionId
																	
																	WHILE @@FETCH_STATUS=0
																		BEGIN
																			SET @hoursScheduled = @hoursScheduled + (SELECT SUM(Scheduled)
																													 from dbo.atClsSectAttendance
																													 WHERE ClsSectionId=@ClsSectionId
																													 AND StuEnrollId=@StuEnrollId
																													 AND MeetDate <= @reportDate)													 
																																						
																			FETCH NEXT FROM ResultCursor
																			INTO @ClsSectionId
																		END
																		
																	CLOSE ResultCursor
																	DEALLOCATE ResultCursor
																
																
																END
															ELSE
																BEGIN
																	SET @hoursScheduled = @hoursScheduled + (SELECT SUM(SchedHours)
																											 from dbo.arStudentClockAttendance
																											 WHERE StuEnrollId=@StuEnrollId
																											 AND RecordDate <= @reportDate)
																END
																
															
															
															--get the total duration of the program in hours																										
															SET @totalProgramHours = (SELECT [Hours]
																					  FROM #StuEnrollments se, #PrgVersions pv
																					  WHERE se.StuEnrollId=@StuEnrollId
																					  AND se.PrgVerId=pv.PrgVerId)
															
															--Percentage of scheduled hours
															IF @totalProgramHours > 0
																BEGIN
																	SET @percentArgument = @hoursScheduled * 100.0 / @totalProgramHours 
																END 
															ELSE
																BEGIN
																	SET @percentArgument = 0.0
																END
															
														
														END
														
														
													IF @PercentageRangeBasisIdx=5
														BEGIN
															--get number of completed hours
															IF LOWER((SELECT dbo.AppSettings('TrackSapAttendance',@CampusId))) = 'byclass'
																BEGIN
																	DECLARE ResultCursor CURSOR FAST_FORWARD FORWARD_ONLY  FOR	
																		SELECT ClsSectionId
																		FROM #Results rs
																		WHERE rs.StuEnrollId=@StuEnrollId
																		
																	OPEN ResultCursor
																	FETCH NEXT FROM ResultCursor
																	INTO @ClsSectionId
																	
																	WHILE @@FETCH_STATUS=0
																		BEGIN
																			SET @hoursScheduled = @hoursScheduled + (SELECT SUM(Scheduled)
																													 from dbo.atClsSectAttendance
																													 WHERE ClsSectionId=@ClsSectionId
																													 AND StuEnrollId=@StuEnrollId
																													 AND MeetDate <= @reportDate)
																													 
																			SET @hoursCompleted = @hoursCompleted + (SELECT SUM(Actual)
																													 from dbo.atClsSectAttendance
																													 WHERE ClsSectionId=@ClsSectionId
																													 AND StuEnrollId=@StuEnrollId
																													 AND MeetDate <= @reportDate)
																																 
																																						
																			FETCH NEXT FROM ResultCursor
																			INTO @ClsSectionId
																		END
																		
																	CLOSE ResultCursor
																	DEALLOCATE ResultCursor
																
																
																END
															ELSE
																BEGIN
																	SET @hoursScheduled = @hoursScheduled + (SELECT SUM(SchedHours)
																											 from dbo.arStudentClockAttendance
																											 WHERE StuEnrollId=@StuEnrollId
																											 AND RecordDate <= @reportDate)
																											 
																	SET @hoursCompleted = @hoursCompleted + (SELECT SUM(ActualHours)
																											 from dbo.arStudentClockAttendance
																											 WHERE StuEnrollId=@StuEnrollId
																											 AND RecordDate <= @reportDate)
																											 
																END
																
															--get the total duration of the program in hours																										
															SET @totalProgramHours = (SELECT [Hours]
																					  FROM #StuEnrollments se, #PrgVersions pv
																					  WHERE se.StuEnrollId=@StuEnrollId
																					  AND se.PrgVerId=pv.PrgVerId)
																					  
															--Percentage of scheduled hours
															IF @totalProgramHours > 0
																BEGIN
																	SET @percentArgument = @hoursCompleted * 100.0 / @totalProgramHours 
																END 
															ELSE
																BEGIN
																	SET @percentArgument = 0.0	
																END
																
														
														END	
													
													--percent argument should never be grater that 100
													IF @percentArgument > 100.0
														BEGIN
															SET @percentArgument = 100.0
														END
														
														
													DECLARE @percentResult As Integer = 0 
													
													IF @percentArgument > 0
														BEGIN
															SET @percentResult = (SELECT TOP 1 tepr.EarnPercent
																		  FROM #StuEnrollments se, #PrgVersions pv, #TuitionEarnings te, #TuitionEarningsPercentageRanges tepr
																		  WHERE se.StuEnrollId=@StuEnrollId
																		  AND se.PrgVerId=pv.PrgVerId
																		  AND pv.TuitionEarningId=te.TuitionEarningId
																		  AND te.TuitionEarningId=tepr.TuitionEarningId
																		  AND tepr.UpTo >= @percentArgument	
																		 )														
														END
																		 
													--apply percentage
													UPDATE #Transactions
													SET DeferredRevenue = (TransAmount* @percentResult)/100
													WHERE TransactionId = @TransactionId
													AND StuEnrollId = @StuEnrollId																		  
												
												END	
												
											--add this value to the totalDeferredRevenue in the transCodes table
											--add Earned Amount from each transaction to AccruedAmount in the transCodes table
											IF (SELECT TotalDeferredRevenue FROM #TransCodes WHERE TransCodeId=@TransCodeId) IS NOT NULL
												BEGIN
													UPDATE #TransCodes
													SET TotalDeferredRevenue = TotalDeferredRevenue + (SELECT DeferredRevenue
																									   FROM #Transactions
																									   WHERE TransactionId=@TransactionId)
													WHERE TransCodeId=@TransCodeId										
												END
											ELSE
												BEGIN
													UPDATE #TransCodes
													SET TotalDeferredRevenue = (SELECT DeferredRevenue
																				FROM #Transactions
																				WHERE TransactionId=@TransactionId)	
													WHERE TransCodeId=@TransCodeId									
												END
																				
																							  
											IF (SELECT AccruedAmount FROM #TransCodes WHERE TransCodeId=@TransCodeId) IS NOT NULL
												BEGIN
													UPDATE #TransCodes
													SET AccruedAmount = AccruedAmount + (SELECT Earned
																						 FROM #Transactions
																						 WHERE TransactionId=@TransactionId)
													WHERE TransCodeId=@TransCodeId
												END	
											ELSE
												BEGIN
													UPDATE #TransCodes
													SET AccruedAmount = (SELECT Earned
																		 FROM #Transactions
																		 WHERE TransactionId=@TransactionId)
													WHERE TransCodeId=@TransCodeId
												END								
											
											
											
											
											IF @RemainingMethod = 1							
												BEGIN
													UPDATE #TransCodes
													SET DeferredRevenueAmount = TotalDeferredRevenue
													WHERE TransCodeId = @TransCodeId										
												END	
											ELSE
												BEGIN
													UPDATE #TransCodes
													SET DeferredRevenueAmount = TotalDeferredRevenue - AccruedAmount
													WHERE TransCodeId = @TransCodeId
												END	
											
											--At this point we should populate the #DeferredRevenues table if the @PostDeferredRevenue was passed in as 1
											IF @Mode=3
												BEGIN
													DECLARE @Amount AS DECIMAL(18,2)
													
													IF @RemainingMethod = 1
														BEGIN
															SET @Amount = (SELECT DeferredRevenue FROM #Transactions WHERE TransactionId=@TransactionId)												
														END
													ELSE
														BEGIN
															
															IF (SELECT DeferredRevenue FROM #Transactions WHERE TransactionId=@TransactionId) <> 0.0
																BEGIN
																	SET @Amount = (SELECT DeferredRevenue FROM #Transactions WHERE TransactionId=@TransactionId) - (SELECT Earned FROM #Transactions WHERE TransactionId=@TransactionId)
																END
															ELSE
																BEGIN
																	SET @Amount = 0
																END
																
														
														END
													
													--Add row to the table only if amount is <> 0
													IF @Amount <> 0
														BEGIN
															INSERT INTO #DeferredRevenues( DefRevenueId,DefRevenueDate,TransactionId,[Type],[Source],Amount,IsPosted,ModUser,ModDate)
															SELECT  
																  NEWID() , -- DefRevenueId - uniqueidentifier
																  @ReportDate , -- DefRevenueDate - datetime
																  @TransactionId , -- TransactionId - uniqueidentifier
																  0 , -- Type - bit
																  1 , -- Source - tinyint
																  @Amount , -- Amount - money
																  1 , -- IsPosted - bit
																  @User , -- ModUser - varchar(50)
																  CONVERT(SMALLDATETIME,GETDATE())  -- ModDate - datetime													 
														
														END											
														
														
														
												
												END															 
											
											
									
									END							
								
								
								
							
							END						
																	
						
					
					END
			
		
		
			END
		
		
		
		
		FETCH NEXT FROM transCursor
		INTO @TransactionId,@StudentName,@StudentIdentifier,@StuEnrollId,@TransCodeId,@TransAmount,@LOAStartDate,@LOAEndDate,@Earned,@FeeLevelId,
			 @TermStart,@TermEnd,@percentToEarnIdx,@RemainingMethod,@DeferredRevenue


	END
	

CLOSE transCursor
DEALLOCATE transCursor


ALTER TABLE [dbo].[saDeferredRevenues] DISABLE TRIGGER ALL
ALTER TABLE dbo.saDeferredRevenues NOCHECK CONSTRAINT ALL  

IF @Mode=3 
	BEGIN
		BEGIN TRAN	
			INSERT INTO dbo.saDeferredRevenues
			SELECT * FROM #DeferredRevenues WITH (NOLOCK)	 
		COMMIT TRAN
	
	END


ALTER TABLE dbo.saDeferredRevenues CHECK CONSTRAINT ALL 

--When Mode is 1 (BuildList) or 3 (Post)
--If there are student exceptions then that table should be returned
--Else we should return the transcodes table
IF (@Mode=1 OR @Mode=3)
	BEGIN
		IF (SELECT ISNULL(COUNT(*),0) FROM #StudentExceptions) > 0
			BEGIN
				SELECT * FROM #StudentExceptions	
			END
		ELSE	
			BEGIN
				SELECT * FROM #TransCodes
			END	
	END
	

--When Mode is 2 (BindTransCodeDetails)
--Return the #Transactions table
IF @Mode=2
	BEGIN
		SELECT * FROM #Transactions
	END




DROP TABLE #Transactions
DROP TABLE #TransCodes
DROP TABLE #StuEnrollments
DROP TABLE #PrgVersions
DROP TABLE #TuitionEarnings
DROP TABLE #TuitionEarningsPercentageRanges
DROP TABLE #Results
DROP TABLE #Reqs
DROP TABLE #StudentExceptions
DROP TABLE #DeferredRevenues




GO
