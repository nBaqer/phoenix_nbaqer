SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[USP_GetRemainingCoursesForContinuingEdProgVersion]
@stuenrollid UNIQUEIDENTIFIER
AS
			/**********************************************************************************
			'   Build query that:
            '       1. retrieves all courses regardless of the program version they belong to.
            '       2. excludes courses already taken with a passing grade.
            '       3. excludes courses already scheduled for.
            **********************************************************************************/
            
            Declare @ListOfCoursesStudentIsScheduledIn Table(ReqId uniqueidentifier)
            Declare @ListOfCourseStudentPassed Table(TermId uniqueidentifier, TermDescrip varchar(100),ReqId uniqueidentifier,Code varchar(50),
							Descrip varchar(50),Credits decimal(18,2),Hours decimal(18,2),StartDate datetime,EndDate datetime,
							GrdSysDetailId uniqueidentifier,Grade varchar(10),IsPass bit,GPA decimal(18,2),
							IsCreditsAttempted bit, IsCreditsEarned bit, IsInGPA bit, StuEnrollId UNIQUEIDENTIFIER, AllowCompletedCourseRetake bit)
							
			Declare @ListOfCourses Table(StuEnrollId uniqueidentifier, LastName varchar(50), FirstName varchar(50),
					ExpGradDate datetime,PrgVerId uniqueidentifier,PrgVerDescrip varchar(50),
				ReqId uniqueidentifier,Req varchar(50),Code varchar(50),
				CourseCredits decimal(18,2),CourseHours decimal(18,2),IsRequired bit,StartDate datetime, ExpStartdate datetime, 
				CampDescrip varchar(50), AllowCompletedCourseRetake bit)
			
							
            -- Get the list of courses Student is currently scheduled to attend
            INSERT INTO @ListOfCoursesStudentIsScheduledIn(ReqId)
            SELECT Distinct Course.ReqId 
            FROM arResults CourseResults INNER JOIN arClassSections Class ON CourseResults.TestId=Class.ClsSectionId 
					INNER JOIN arTerm Term ON Class.TermId=Term.TermId
					INNER JOIN syStatuses Statuses ON Term.StatusId=Statuses.StatusId 
					INNER JOIN arReqs Course ON Class.ReqId=Course.ReqId
			WHERE
				CourseResults.StuEnrollId=@StuEnrollId 
            
            
            --Select * from @ListOfCoursesStudentIsScheduledIn
            
            -- Get the list of courses Student attended and passed
            INSERT INTO @ListOfCourseStudentPassed(TermId, TermDescrip,ReqId,Code,
				Descrip,Credits,Hours,StartDate,EndDate,
				GrdSysDetailId,Grade,IsPass ,GPA,
				IsCreditsAttempted,IsCreditsEarned,IsInGPA,StuEnrollId,AllowCompletedCourseRetake)
								SELECT DISTINCT 
                                       Class.TermId,Term.TermDescrip,Class.ReqId,Course.Code,Course.Descrip,Course.Credits,
                                       Course.Hours,Term.StartDate, Term.EndDate, CourseResults.GrdSysDetailId, 
                                       (SELECT Grade FROM arGradeSystemDetails WHERE GrdSysDetailId=CourseResults.GrdSysDetailId)as Grade,  
                                       (SELECT IsPass FROM arGradeSystemDetails WHERE GrdSysDetailId=CourseResults.GrdSysDetailId) as IsPass,  
                                       (SELECT GPA FROM arGradeSystemDetails WHERE GrdSysDetailId=CourseResults.GrdSysDetailId) as GPA,  
                                       (SELECT IsCreditsAttempted FROM arGradeSystemDetails WHERE GrdSysDetailId=CourseResults.GrdSysDetailId) as IsCreditsAttempted,  
                                       (SELECT IsCreditsEarned FROM arGradeSystemDetails WHERE GrdSysDetailId=CourseResults.GrdSysDetailId) as IsCreditsEarned,  
                                       (SELECT IsInGPA FROM arGradeSystemDetails WHERE GrdSysDetailId=CourseResults.GrdSysDetailId) as IsInGPA,CourseResults.StuEnrollId,
                                       Course.AllowCompletedCourseRetake  
                                   FROM arResults CourseResults INNER JOIN arClassSections Class ON CourseResults.TestId = Class.ClsSectionId 
                                   INNER JOIN arTerm Term ON Class.TermId = Term.TermId
                                   INNER JOIN arReqs Course ON Class.ReqId = Course.ReqId 
                                   WHERE 
									   CourseResults.StuEnrollId=@StuEnrollId  AND
                                       CourseResults.GrdSysDetailId IS NOT NULL 
                                   UNION  
                                   SELECT DISTINCT 
                                       TransferGrade.TermId,Term.TermDescrip,TransferGrade.ReqId,Course.Code,Course.Descrip,Course.Credits,Course.Hours,Term.StartDate,Term.EndDate,TransferGrade.GrdSysDetailId,
                                       (SELECT Grade FROM arGradeSystemDetails WHERE GrdSysDetailId=TransferGrade.GrdSysDetailId) as Grade,  
                                       (SELECT IsPass FROM arGradeSystemDetails WHERE GrdSysDetailId=TransferGrade.GrdSysDetailId) as IsPass,  
                                       (SELECT GPA FROM arGradeSystemDetails WHERE GrdSysDetailId=TransferGrade.GrdSysDetailId) as GPA,  
                                       (SELECT IsCreditsAttempted FROM arGradeSystemDetails WHERE GrdSysDetailId=TransferGrade.GrdSysDetailId) as IsCreditsAttempted, 
                                       (SELECT IsCreditsEarned FROM arGradeSystemDetails WHERE GrdSysDetailId=TransferGrade.GrdSysDetailId) as IsCreditsEarned,  
                                       (SELECT IsInGPA FROM arGradeSystemDetails WHERE GrdSysDetailId=TransferGrade.GrdSysDetailId) as IsInGPA,TransferGrade.StuEnrollId,
                                       Course.AllowCompletedCourseRetake  
                                   FROM arTransferGrades TransferGrade INNER JOIN arTerm Term ON TransferGrade.TermId = Term.TermId  
                                   Inner Join arReqs Course ON TransferGrade.ReqId = Course.ReqId
                                   WHERE    
										TransferGrade.StuEnrollId=@StuEnrollId  AND
                                        TransferGrade.GrdSysDetailId IS NOT NULL  
			
            -- Get list of courses the student is registered for
            Insert into @ListofCourses(StuEnrollId, LastName, FirstName,ExpGradDate,PrgVerId,PrgVerDescrip,
				ReqId,Req,Code,CourseCredits,CourseHours,IsRequired,StartDate,ExpStartDate,CampDescrip,AllowCompletedCourseRetake)
            SELECT DISTINCT StudentEnrollment.StuEnrollId,Student.LastName,Student.FirstName,StudentEnrollment.ExpGradDate,
                       StudentEnrollment.PrgVerId,ProgramVersion.PrgVerDescrip, 
                       Course.ReqId as ReqId,Course.Descrip As Req,Course.Code As Code,Course.Credits,Course.Hours,'False' AS IsRequired,
                       StudentEnrollment.StartDate,StudentEnrollment.ExpStartDate,
					   (select CampDescrip from syCampuses where CampusId=StudentEnrollment.CampusId) as CampDescrip,
					   Course.AllowCompletedCourseRetake 
                       FROM arStuEnrollments StudentEnrollment INNER JOIN arStudent Student ON StudentEnrollment.StudentId = Student.StudentId
                       INNER JOIN arPrgVersions ProgramVersion ON StudentEnrollment.PrgVerId=ProgramVersion.PrgVerId
                       INNER JOIN  syStatuses Statuses  ON StudentEnrollment.StatusCodeId = Statuses.StatusId 
                       INNER JOIN  arReqs Course ON Course.StatusId=Statuses.StatusId
                       WHERE  StudentEnrollment.StuEnrollId=@StuEnrollId AND  Statuses.Status='Active'
            
            -- Get list of courses the student is registered for
            -- Ignore courses that student passed
            -- Also, ignore courses the student is currently registered in
            SELECT * FROM @ListofCourses
            -- This portion gets the courses that the student has already taken with a passing grade
            WHERE NOT EXISTS (Select * from @ListOfCourseStudentPassed where IsPass=1) 
            -- This portion gets the courses the student is already scheduled for
            AND NOT EXISTS (Select ReqId from @ListOfCoursesStudentIsScheduledIn) 
            ORDER BY CampDescrip,Code,Req,PrgVerDescrip 



GO
