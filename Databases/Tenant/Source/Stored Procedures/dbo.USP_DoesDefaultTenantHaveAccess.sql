SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[USP_DoesDefaultTenantHaveAccess]
as
begin
	Declare @supportUserId uniqueidentifier
	Set @supportUserId = (Select Top 1 UserId from 
	[dbo].[aspnet_users] where 
	username like 'support%')
	
	Select Count(*) as TotalRowCount from  dbo.[TenantAccess] where 
	TenantId in (select Top 1 TenantId from dbo.[TenantUsers]
	where IsdefaultTenant=1 and userid=@supportUserId)
end
GO
