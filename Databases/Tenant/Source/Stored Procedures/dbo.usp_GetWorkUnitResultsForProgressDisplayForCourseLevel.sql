SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [dbo].[usp_GetWorkUnitResultsForProgressDisplayForCourseLevel] 
(@stuEnrollId uniqueidentifier 
 ) 
 as 
 SET NOCOUNT ON; 
  
          SELECT 
               ConversionResultid,GBCR.ReqId,GBCR.TermId,rtrim(GCT.Descrip) + (case when GBCR.ResNum in (0,1) then '' else cast(GBCR.ResNum as char) end) as Descrip ,GCT.Descrip as Des,GBCR.Score,GBCR.MinResult, 
               GBCR.Required,GBCR.MustPass, 
             (case when GCT.SysComponentTypeId in(500,503,544) then 
             (CASE WHEN ISNULL(Score,0) > MinResult THEN NULL WHEN ISNULL(Score,0) = MinResult 
             THEN 0 WHEN MinResult > ISNULL(Score,0) THEN (MinResult - ISNULL(Score,0)) END) 
             else null 
                END) AS Remaining,GCT.SysComponentTypeId as WorkUnitType,(select Resource from syResources where Resourceid=GCT.SysComponentTypeId) as WorkUnitDescrip,null as InstrGrdBkWgtDetailId,GBCR.StuEnrollid,GBCR.ResNum,'false' as RossType 
            FROM arGrdBkConversionResults GBCR, arGrdComponentTypes GCT 
            WHERE GBCR.GrdComponentTypeId=GCT.GrdComponentTypeId 
            AND GBCR.StuEnrollId=@stuEnrollId 
            
					UNION 
            
            SELECT 
               grdBkResultid,ReqId,TermId, 
           
                Descrip,Des, 
         
            
             Score, MinResult, Required, MustPass, 
             (case when WorkUnitType in(500,503,544) then 
             (CASE WHEN ISNULL(Score,0) > MinResult THEN NULL WHEN ISNULL(Score,0) = MinResult 
             THEN 0 WHEN MinResult > ISNULL(Score,0) THEN (MinResult - ISNULL(Score,0)) END) 
             else null 
                END) AS Remaining,WorkUnitType,WorkUnitDescrip,InstrGrdBkWgtDetailId,StuEnrollid,ResNum ,'false' as RossType 
               FROM 
               (SELECT 
                  CS.ReqId,CS.TermId, 
            
             (case when (select distinct number from arGrdBkWgtDetails where InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId) = 1 then 
             GCT.Descrip 
             else 
             rtrim(GCT.Descrip) + (case when GBRS.ResNum =0 then '' else cast(GBRS.ResNum as char) end) 
             end) 
             as Descrip, 
                 (CASE GCT.SysComponentTypeId 
                   WHEN 544 THEN (SELECT SUM(HoursAttended) FROM arExternshipAttendance WHERE StuEnrollId=@stuEnrollId) 
                   ELSE GBRS.Score 
                   END 
                 ) AS Score, 
                (CASE GCT.SysComponentTypeId 
                   WHEN 500 THEN GBWD.Number 
                   WHEN 503 THEN GBWD.Number 
                   WHEN 544 THEN GBWD.Number 
                   ELSE (SELECT MIN(MinVal) 
                         FROM arGradeScaleDetails GSD, arGradeSystemDetails GSS,arClassSections CSR 
                         WHERE GSD.GrdSysDetailId=GSS.GrdSysDetailId 
                         AND GSS.IsPass=1 and GSD.GrdScaleId=CSR.GrdScaleId and CSR.ClsSectionId=CS.ClsSectionId) 
                   END 
                   ) AS MinResult, 
                   GBWD.Required, GBWD.MustPass,GCT.SysComponentTypeId as WorkUnitType,(select Resource from syResources where Resourceid=GCT.SysComponentTypeId) as WorkUnitDescrip,GBRS.grdBkResultid,GBRS.InstrGrdBkWgtDetailId,GBRS.StuEnrollid,GCT.Descrip
					as Des,GBRS.ResNum,GBWD.Descrip as grdWgtDescrip 
                   FROM arGrdBkResults GBRS, arClassSections CS, arGrdBkWgtDetails GBWD, arGrdComponentTypes GCT 
                   WHERE GBRS.StuEnrollId=@stuEnrollId 
                   AND GBRS.ClsSectionId=CS.ClsSectionId 
                   AND GBRS.InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId 
                            AND GBWD.GrdComponentTypeId=GCT.GrdComponentTypeId) P where WorkUnitType not in(500,503,544) 
  
							Union 
             
             SELECT '00000000-0000-0000-0000-000000000000' as grdBkResultid, 
             ReqId,TermId, 
              Descrip,Des, 
         
             sum(score) as score,isnull(MinResult,0) as MinResult,Required,MustPass, 
             (CASE WHEN sum(ISNULL(Score,0)) > MinResult THEN NULL WHEN sum(ISNULL(Score,0)) = MinResult 
             THEN 0 WHEN MinResult > sum(ISNULL(Score,0)) THEN (MinResult - sum(ISNULL(Score,0))) END) as remaining, 
             WorkUnitType, WorkUnitDescrip, InstrGrdBkWgtDetailId, StuEnrollId, ResNum ,'false' as RossType 
             from 
             (SELECT CS.ReqId,CS.TermId, 
                     (case when (select distinct number from arGrdBkWgtDetails where InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId) = 1 then 
             GCT.Descrip 
             else 
             rtrim(GCT.Descrip) + (case when GBRS.ResNum =0 then '' else cast(GBRS.ResNum as char) end) 
             end) 
             as Descrip, 
             (CASE GCT.SysComponentTypeId WHEN 544 THEN (SELECT SUM(HoursAttended) FROM arExternshipAttendance WHERE StuEnrollId= @stuEnrollId ) 
             ELSE GBRS.Score END ) AS Score, (CASE GCT.SysComponentTypeId WHEN 500 THEN GBWD.Number 
             WHEN 503 THEN GBWD.Number WHEN 544 THEN GBWD.Number ELSE (SELECT MIN(MinVal) 
             FROM arGradeScaleDetails GSD, arGradeSystemDetails GSS,arClassSections CSR WHERE GSD.GrdSysDetailId=GSS.GrdSysDetailId 
             AND GSS.IsPass=1 and GSD.GrdScaleId=CSR.GrdScaleId and CSR.ClsSectionId=CS.ClsSectionId) END ) AS MinResult, GBWD.Required, GBWD.MustPass,GCT.SysComponentTypeId as WorkUnitType, 
             (select Resource from syResources where Resourceid=GCT.SysComponentTypeId) as WorkUnitDescrip,GBRS.grdBkResultid, 
             GBRS.InstrGrdBkWgtDetailId,GBRS.StuEnrollid,GCT.Descrip as Des,GBRS.ResNum,GBWD.Descrip as grdWgtDescrip 
             FROM arGrdBkResults GBRS, arClassSections CS, arGrdBkWgtDetails GBWD, arGrdComponentTypes GCT 
             WHERE GBRS.StuEnrollId= @stuEnrollId 
             AND GBRS.ClsSectionId=CS.ClsSectionId AND GBRS.InstrGrdBkWgtDetailId=GBWD.InstrGrdBkWgtDetailId 
              
             AND GBWD.GrdComponentTypeId=GCT.GrdComponentTypeId) P where WorkUnitType in(500,503,544) 
  
             group by 
             ReqId,TermId,Descrip,Des,MinResult,Required,MustPass, 
              WorkUnitType, WorkUnitDescrip, InstrGrdBkWgtDetailId, StuEnrollId, ResNum order by Descrip 

GO
