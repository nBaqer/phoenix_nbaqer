SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [dbo].[usp_GetNeverAttemptedWUForInstructorLevel]
(@stuEnrollId uniqueidentifier
 )
 as
 SET NOCOUNT ON;

            select d.reqid,d.termId,a.Descrip, 
            (CASE e.SysComponentTypeId    
             WHEN 500 THEN a.Number  
             WHEN 503 THEN a.Number  
             WHEN 544 THEN a.Number 
             ELSE (SELECT MIN(MinVal) 
                         FROM arGradeScaleDetails GSD, arGradeSystemDetails GSS,arClassSections CSR 
                         WHERE GSD.GrdSysDetailId=GSS.GrdSysDetailId 
                         AND GSS.IsPass=1 and GSD.GrdScaleId=CSR.GrdScaleId and CSR.ClsSectionId=d.ClsSectionId)        END        )	AS MinResult, 
             a.required, a.mustpass, isnull(e.SysComponentTypeId,0) as WorkUnitType, a.number,(select Resource from syResources where Resourceid=e.SysComponentTypeId) as WorkUnitDescrip, 
             a.InstrGrdBkWgtDetailId, c.stuenrollid,0 as IsExternShip 
             from arGrdBkWgtDetails a,arGrdBkWeights b,arResults c,arClassSections d,arGrdComponentTypes e 
             where a.InstrGrdBkWgtId=b.InstrGrdBkWgtId and  
             c.testid = d.clsSectionid 
             and b.InstrGrdBkWgtId=d.InstrGrdBkWgtId 
             and e.GrdComponentTypeId=a.GrdComponentTypeId 
             and c.StuEnrollid= @stuEnrollId 
             and a.InstrGrdBkWgtDetailId not in (select InstrGrdBkWgtDetailId from arGrdBkResults 
             where StuEnrollId= @stuEnrollId 
             and clsSectionid in (select clsSectionid from arClassSections where Reqid = d.reqid and Termid = d.TermId )) 
             Union 
             select d.reqid,d.termId,e.Descrip, 
            (CASE e.SysComponentTypeId    
             WHEN 500 THEN a.Number  
             WHEN 503 THEN a.Number  
             WHEN 544 THEN a.Number 
             ELSE (SELECT MIN(MinVal)              FROM arGradeScaleDetails GSD, arGradeSystemDetails GSS   
             WHERE GSD.GrdSysDetailId=GSS.GrdSysDetailId              AND GSS.IsPass=1)        END        )	AS MinResult, 
             a.required, a.mustpass, isnull(e.SysComponentTypeId,0) as WorkUnitType, a.number,(select Resource from syResources where Resourceid=e.SysComponentTypeId) as WorkUnitDescrip, 
            a.InstrGrdBkWgtDetailId,c.stuenrollid, f.IsExternship   
             from arGrdBkWgtDetails a,arGrdBkWeights b,arResults c,arClassSections d,arGrdComponentTypes e,arReqs f 
             where a.InstrGrdBkWgtId=b.InstrGrdBkWgtId and  
             b.Reqid = d.reqid And c.testid = d.clsSectionid 
             and e.GrdComponentTypeId=a.GrdComponentTypeId 
             and d.Reqid=f.Reqid and f.IsExternship=1 
             and b.EffectiveDate<=d.StartDate 
             and c.StuEnrollid= @stuEnrollId 
             and a.number is not null 
             and a.InstrGrdBkWgtDetailId not in (select InstrGrdBkWgtDetailId from arGrdBkResults 
            
             where  StuEnrollId= @stuEnrollId   
             and clsSectionid in (select clsSectionid from arClassSections where Reqid = d.reqid 
             and Termid = d.TermId )) 
             and b.EffectiveDate = 
             (SELECT Distinct Top 1 A.EffectiveDate 
             FROM          arGrdBkWeights A,arClassSections B  
             WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=d.ClsSectionId order by A.EffectiveDate desc) 

GO
