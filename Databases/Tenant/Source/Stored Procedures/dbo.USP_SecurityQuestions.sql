SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROC [dbo].[USP_SecurityQuestions]
AS 
    SELECT  Id ,
            Description
    FROM    dbo.[TenantQuestions]
GO
