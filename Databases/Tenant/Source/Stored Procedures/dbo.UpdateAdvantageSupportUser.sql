SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ginzo, John
-- Create date: 06/26/2015
-- Description:	Sync up the support user from tenant
--				to Advantage
-- =============================================
CREATE PROCEDURE [dbo].[UpdateAdvantageSupportUser]
    @DatabaseName VARCHAR(50)
   ,@CampusCode VARCHAR(5) = NULL
   ,@Username VARCHAR(50) = NULL
AS
    BEGIN



        BEGIN TRAN UpdateUsers;

        BEGIN TRY
            --DECLARE @DatabaseName VARCHAR(50)
            --SET @DatabaseName='[IDTI_3.8_MIG]'
            DECLARE @oldUserId VARCHAR(50);
            DECLARE @newUserId VARCHAR(50);
            DECLARE @Sql VARCHAR(1000);

            -- Clean up rptAdmissionsRep Table from support user.
            --SET @Sql = 'DELETE  ' + @DatabaseName + '.dbo.rptAdmissionsRep
            --WHERE   AdmissionsRepId IN ( SELECT AdmissionsRepId
            --                             FROM    ' + @DatabaseName + '.dbo.rptAdmissionsRep
            --                             WHERE  AdmissionsRepDescrip LIKE ''%Support%'' );'
            PRINT @Sql;
            EXECUTE ( @Sql );

            --Support User id from tenant auth db
            SET @newUserId = (
                             SELECT TOP 1 UserId
                             FROM   TenantAuthDB.dbo.aspnet_Users
                             WHERE  (
                                    @Username IS NULL
                                    AND UserName IN ( 'support@fameinc.com', 'advantagedev@fameinc.com' )
                                    )
                                    OR (
                                       @Username IS NOT NULL
                                       AND UserName = @Username
                                       )
                             );

            IF ( @CampusCode IS NULL )
                BEGIN



                    DECLARE @SQL2 NVARCHAR(MAX) = 'SELECT TOP 1 @oldUserId = UserId FROM ' + @DatabaseName + '.dbo.syUsers where username=' + '''support''';
                    EXEC sp_executesql @SQL2
                                      ,N'@oldUserId uniqueidentifier out'
                                      ,@oldUserId OUT;
                    SET @oldUserId = CAST(@oldUserId AS VARCHAR(50));




                    SET @Sql = ( 'INSERT INTO ' + @DatabaseName
                                 + '.dbo.syUsers
			( UserId
			,FullName
			,Email
			,UserName
			,Password
			,ConfirmPassword
			,Salt
			,AccountActive
			,ModDate
			,ModUser
			,CampusId
			,ShowDefaultCampus
			,ModuleId
			,IsAdvantageSuperUser
			,IsDefaultAdminRep
			,IsLoggedIn
			)
	SELECT  top 1 '''            + @newUserId + '''
			,''support''
			,'                   + '''support@yahoo.com''' + '
			,''support''
			,'                   + '''no password''' + '
			,'                   + '''no password'''
                                 + '
			,Salt
			,AccountActive
			,ModDate
			,ModUser
			,null
			,ShowDefaultCampus
			,ModuleId
			,IsAdvantageSuperUser
			,IsDefaultAdminRep
			,IsLoggedIn
	FROM '                       + @DatabaseName + '.dbo.syUsers
	WHERE IsAdvantageSuperUser=1'
                               );

                    --PRINT @sql
                    EXECUTE ( @Sql );

                    SET @Sql = 'insert into ' + @DatabaseName
                               + '..syUsersRolesCampGrps (
                                     UserRoleCampGrpId
                                    ,UserId
                                    ,RoleId
                                    ,CampGrpId
                                    ,ModDate
                                    ,ModUser
                                     )  
									 SELECT NEWID(),''' + @newUserId + ''', RoleId, ''2AC97FC1-BB9B-450A-AA84-7C503C90A1EB'', GETDATE(), ''SA'' FROM '
                               + @DatabaseName + '..syroles WHERE Code  IN (''SYSADMINS'', ''AD-Api'', ''Reports-Api'', ''DIRADM'')';

                    EXECUTE ( @Sql );

                    PRINT @Sql;

                    DECLARE @UserUpdateFKSQL VARCHAR(MAX);

                    DECLARE @newUserIdString VARCHAR(50) = CONVERT(NVARCHAR(50), @newUserId);
                    DECLARE @oldUserIdString VARCHAR(50) = CONVERT(NVARCHAR(50), @oldUserId);



                    CREATE TABLE #Updates
                        (
                            UpdateStatement NVARCHAR(MAX)
                        );


                    SET @UserUpdateFKSQL = 'insert into #Updates SELECT     ''UPDATE ''+ ''' + @DatabaseName
                                           + '''+''..''+ + detail.name + '' SET '' + dcolumn.name + '' = ''''' + @newUserIdString
                                           + '''''   WHERE '' + dcolumn.name + '' = ''''' + @oldUserIdString + ''''''' 

FROM       '                               + @DatabaseName + '.sys.columns AS mcolumn
INNER JOIN '                               + @DatabaseName
                                           + '.sys.foreign_key_columns fkColumn ON mcolumn.object_id = fkColumn.referenced_object_id
                                      AND mcolumn.column_id = fkColumn.referenced_column_id
INNER JOIN '                               + @DatabaseName + '.sys.tables AS master ON mcolumn.object_id = master.object_id
INNER JOIN '                               + @DatabaseName
                                           + '.sys.columns AS dcolumn ON fkColumn.parent_object_id = dcolumn.object_id
                                     AND fkColumn.parent_column_id = dcolumn.column_id
INNER JOIN '                               + @DatabaseName
                                           + '.sys.tables AS detail ON dcolumn.object_id = detail.object_id
                                   AND detail.name <> ''syUsers''
WHERE      ( master.name = N''syUsers'' )';


                    EXEC ( @UserUpdateFKSQL );

                    DECLARE @currentUpdateStatement NVARCHAR(MAX);

                    DECLARE update_cursor CURSOR FOR
                        SELECT UpdateStatement
                        FROM   #Updates;

                    OPEN update_cursor;

                    FETCH NEXT FROM update_cursor
                    INTO @currentUpdateStatement;

                    WHILE @@FETCH_STATUS = 0
                        BEGIN
                            EXEC ( @currentUpdateStatement );
                            IF @@ERROR <> 0
                                BEGIN
                                    ROLLBACK TRAN UpdateUsers;
                                    PRINT 'ERROR=' + CAST(@@ERROR AS VARCHAR(100));
                                    PRINT @currentUpdateStatement;
                                    RETURN @@ERROR;
                                END;

                            FETCH NEXT FROM update_cursor
                            INTO @currentUpdateStatement;

                        END;

                    CLOSE update_cursor;
                    DEALLOCATE update_cursor;


                --make sure username or email are not repeated
                    SET @Sql = 'update ' + @DatabaseName + '..syUsers set username = ''support_' + REPLACE(@oldUserId, '-', '_') + ''', email = ''support_'
                               + REPLACE(@oldUserId, '-', '_') + '@famein.com'' where userid = ''' + @oldUserId + '''';

                    EXECUTE ( @Sql );






                    PRINT 'Update is compete';
                END;
            ELSE
                BEGIN

                    SET @Sql = ( 'INSERT INTO ' + @DatabaseName
                                 + '.dbo.syUsers
			( UserId
			,FullName
			,Email
			,UserName
			,Password
			,ConfirmPassword
			,Salt
			,AccountActive
			,ModDate
			,ModUser
			,CampusId
			,ShowDefaultCampus
			,ModuleId
			,IsAdvantageSuperUser
			,IsDefaultAdminRep
			,IsLoggedIn
			)
	SELECT  top 1 '''            + @newUserId + '''
			,''support_'         + @CampusCode + '''
			,''support_'         + @CampusCode + '@yahoo.com''' + '
			,''support_'         + @CampusCode + '''
			,'                   + '''no password''' + '
			,'                   + '''no password'''
                                 + '
			,Salt
			,AccountActive
			,ModDate
			,ModUser
			,(SELECT TOP 1 Campusid FROM FreedomAdvantage..syCampuses ORDER BY ModDate desc)
			,ShowDefaultCampus
			,ModuleId
			,IsAdvantageSuperUser
			,IsDefaultAdminRep
			,IsLoggedIn
	FROM '                       + @DatabaseName + '.dbo.syUsers
	WHERE IsAdvantageSuperUser=1'
                               );

                    PRINT @Sql;

                    --PRINT @sql
                    EXECUTE ( @Sql );

                    SET @Sql = 'insert into ' + @DatabaseName
                               + '..syUsersRolesCampGrps (
                                     UserRoleCampGrpId
                                    ,UserId
                                    ,RoleId
                                    ,CampGrpId
                                    ,ModDate
                                    ,ModUser
                                     )  
									 SELECT NEWID(),''' + @newUserId + ''', RoleId, ''2AC97FC1-BB9B-450A-AA84-7C503C90A1EB'', GETDATE(), ''SA'' FROM '
                               + @DatabaseName + '..syroles WHERE Code  IN (''SYSADMINS'', ''AD-Api'', ''Reports-Api'', ''DIRADM'')';

                    EXECUTE ( @Sql );
                END;

            IF OBJECT_ID('tempdb..#Updates', 'U') IS NOT NULL
                BEGIN
                    DROP TABLE #Updates;

                END;


            COMMIT TRAN UpdateUsers;
        END TRY
        BEGIN CATCH
            IF OBJECT_ID('tempdb..#Updates', 'U') IS NOT NULL
                BEGIN
                    DROP TABLE #Updates;

                END;

            ROLLBACK TRANSACTION;
        END CATCH;
        RETURN 0;



    END;




GO
