SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[Usp_Advantage_CommonTaskNavigation_ByModule_GetList]
	@SchoolEnumerator INT ,
	@CampusId UNIQUEIDENTIFIER
AS 
	BEGIN  
 -- declare local variables  
		DECLARE @ShowRossOnlyTabs BIT ,
			@SchedulingMethod VARCHAR(50) ,
			@TrackSAPAttendance VARCHAR(50)  
		DECLARE @ShowCollegeOfCourtReporting VARCHAR(5) ,
			@FameESP VARCHAR(5) ,
			@EdExpress VARCHAR(5)   
		DECLARE @GradeBookWeightingLevel VARCHAR(20) ,
			@ShowExternshipTabs VARCHAR(5) 
		DECLARE @ConsolidateCourseComponents BIT 
   
 -- Get Values  
		--Declare @IsUserAPowerUser bit
		
  --      Set @IsUserAPowerUser = 0
  --       IF ( SELECT COUNT(*)
  --           FROM   syUsers
  --           WHERE  ( LOWER(UserName) = 'support' OR LOWER(UserName) = 'sa' or LOWER(UserName) = 'super')
  --                  AND UserId = @UserId
  --         ) >= 1 
  --          BEGIN  
  --              SET @IsUserAPowerUser = 1 -- set PowerUser to True  
  --          END   
								
		IF ( SELECT COUNT(*)
			 FROM   syConfigAppSetValues
			 WHERE  SettingId = 68
					AND CampusId = @CampusId
		   ) >= 1 
			BEGIN
				SET @ShowRossOnlyTabs = ( SELECT    VALUE
										  FROM      dbo.syConfigAppSetValues
										  WHERE     SettingId = 68
													AND CampusId = @CampusId
										)  
								
			END 
		ELSE 
			BEGIN
				SET @ShowRossOnlyTabs = ( SELECT    VALUE
										  FROM      dbo.syConfigAppSetValues
										  WHERE     SettingId = 68
													AND CampusId IS NULL
										)
			END
	
	
	
		IF ( SELECT COUNT(*)
			 FROM   syConfigAppSetValues
			 WHERE  SettingId = 65
					AND CampusId = @CampusId
		   ) >= 1 
			BEGIN
				SET @SchedulingMethod = ( SELECT    VALUE
										  FROM      dbo.syConfigAppSetValues
										  WHERE     SettingId = 65
													AND CampusId = @CampusId
										)  
								
			END 
		ELSE 
			BEGIN
				SET @SchedulingMethod = ( SELECT    VALUE
										  FROM      dbo.syConfigAppSetValues
										  WHERE     SettingId = 65
													AND CampusId IS NULL
										)
			END                      
								
	
								  
		IF ( SELECT COUNT(*)
			 FROM   syConfigAppSetValues
			 WHERE  SettingId = 72
					AND CampusId = @CampusId
		   ) >= 1 
			BEGIN
				SET @TrackSAPAttendance = ( SELECT  VALUE
											FROM    dbo.syConfigAppSetValues
											WHERE   SettingId = 72
													AND CampusId = @CampusId
										  )  
								
			END 
		ELSE 
			BEGIN
				SET @TrackSAPAttendance = ( SELECT  VALUE
											FROM    dbo.syConfigAppSetValues
											WHERE   SettingId = 72
													AND CampusId IS NULL
										  )
			END 
	   
										   
		IF ( SELECT COUNT(*)
			 FROM   syConfigAppSetValues
			 WHERE  SettingId = 118
					AND CampusId = @CampusId
		   ) >= 1 
			BEGIN
				SET @ShowCollegeOfCourtReporting = ( SELECT VALUE
													 FROM   dbo.syConfigAppSetValues
													 WHERE  SettingId = 118
															AND CampusId = @CampusId
												   )  
								
			END 
		ELSE 
			BEGIN
				SET @ShowCollegeOfCourtReporting = ( SELECT VALUE
													 FROM   dbo.syConfigAppSetValues
													 WHERE  SettingId = 118
															AND CampusId IS NULL
												   )
			END 
	 
					   
		IF ( SELECT COUNT(*)
			 FROM   syConfigAppSetValues
			 WHERE  SettingId = 37
					AND CampusId = @CampusId
		   ) >= 1 
			BEGIN
				SET @FameESP = ( SELECT VALUE
								 FROM   dbo.syConfigAppSetValues
								 WHERE  SettingId = 37
										AND CampusId = @CampusId
							   )  
								
			END 
		ELSE 
			BEGIN
				SET @FameESP = ( SELECT VALUE
								 FROM   dbo.syConfigAppSetValues
								 WHERE  SettingId = 37
										AND CampusId IS NULL
							   )
			END 
	 
						 
		IF ( SELECT COUNT(*)
			 FROM   syConfigAppSetValues
			 WHERE  SettingId = 91
					AND CampusId = @CampusId
		   ) >= 1 
			BEGIN
				SET @EdExpress = ( SELECT   VALUE
								   FROM     dbo.syConfigAppSetValues
								   WHERE    SettingId = 91
											AND CampusId = @CampusId
								 )  
								
			END 
		ELSE 
			BEGIN
				SET @EdExpress = ( SELECT   VALUE
								   FROM     dbo.syConfigAppSetValues
								   WHERE    SettingId = 91
											AND CampusId IS NULL
								 )
			END
		
								   
									   
		IF ( SELECT COUNT(*)
			 FROM   syConfigAppSetValues
			 WHERE  SettingId = 43
					AND CampusId = @CampusId
		   ) >= 1 
			BEGIN
				SET @GradeBookWeightingLevel = ( SELECT VALUE
												 FROM   dbo.syConfigAppSetValues
												 WHERE  SettingId = 43
														AND CampusId = @CampusId
											   )  
								
			END 
		ELSE 
			BEGIN
				SET @GradeBookWeightingLevel = ( SELECT VALUE
												 FROM   dbo.syConfigAppSetValues
												 WHERE  SettingId = 43
														AND CampusId IS NULL
											   )
			END
	   
		PRINT @GradeBookWeightingLevel                         
								  
		IF ( SELECT COUNT(*)
			 FROM   syConfigAppSetValues
			 WHERE  SettingId = 71
					AND CampusId ='5ebf6c1f-1fb1-492d-ad53-348f3986a178'
		   ) >= 1 
			BEGIN
				SET @ShowExternshipTabs = ( SELECT  VALUE
											FROM    dbo.syConfigAppSetValues
											WHERE   SettingId = 71
													AND CampusId = '5ebf6c1f-1fb1-492d-ad53-348f3986a178'
										  )  
								
			END 
		ELSE 
			BEGIN
				SET @ShowExternshipTabs = ( SELECT  VALUE
											FROM    dbo.syConfigAppSetValues
											WHERE   SettingId = 71
													AND CampusId IS NULL
										  )
			END
  
  
  		IF ( SELECT COUNT(*)
			 FROM   syConfigAppSetValues
			 WHERE  SettingId = 147
					AND CampusId = @CampusId
		   ) >= 1 
			BEGIN
				SET @ConsolidateCourseComponents = ( SELECT    VALUE
										  FROM      dbo.syConfigAppSetValues
										  WHERE     SettingId = 147
													AND CampusId = @CampusId
										)  
								
			END 
		ELSE 
			BEGIN
				SET @ConsolidateCourseComponents = ( SELECT    VALUE
										  FROM      dbo.syConfigAppSetValues
										  WHERE     SettingId = 147
													AND CampusId IS NULL
										)
			END
  
 -- The first column always refers to the Module Resource Id (189 or 26 or .....)  
   
		SELECT  *
		FROM    ( SELECT    189 AS ModuleResourceId ,
							NNChild.ResourceId AS ChildResourceId ,
							CASE WHEN NNChild.ResourceId = 395
								 THEN 'Lead Tabs'
								 ELSE CASE WHEN NNChild.ResourceId = 398
										   THEN 'Leads'
										   ELSE RChild.Resource
									  END
							END AS ChildResource ,
							RChild.ResourceURL AS ChildResourceURL ,
							CASE WHEN NNParent.ResourceId IN ( 687, 688, 689)
								 THEN NULL
								 ELSE NNParent.ResourceId
							END AS ParentResourceId ,
							RParent.Resource AS ParentResource ,
							CASE WHEN NNChild.ResourceId = 398 THEN 1
								 ELSE CASE WHEN NNChild.ResourceId = 395
										   THEN 2
										   ELSE 3
									  END
							END AS FirstSortOrder ,
							CASE WHEN ( NNChild.ResourceId IN ( 398 )
										OR NNParent.ResourceId IN ( 398 )
									  ) THEN 1
								 ELSE 2
							END AS DisplaySequence  
 --NNChild.HierarchyIndex AS SecondSortOrder,  
 --RParentModule.Resource AS Module  
				  FROM      syResources RChild
							INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
							INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
							INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
							LEFT OUTER JOIN ( SELECT    *
											  FROM      syResources
											  WHERE     ResourceTypeId = 1
											) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
				  WHERE     RChild.ResourceTypeId IN ( 2, 3, 8 )
							AND ( RChild.ChildTypeId IS NULL
								  OR RChild.ChildTypeId = 3
								)
							AND ( RChild.ResourceId NOT IN ( 395 ) )
							AND ( NNParent.ResourceId NOT IN ( 395 ) )
							AND ( NNParent.ParentId IN (
								  SELECT    HierarchyId
								  FROM      syNavigationNodes
								  WHERE     ResourceId = 189 )
								  OR NNChild.ParentId IN (
								  SELECT    HierarchyId
								  FROM      syNavigationNodes
								  WHERE     ResourceId IN ( 694, 398,790 ) )
								)
							AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
				  UNION ALL
				  SELECT    26 AS ModuleResourceId ,
							ChildResourceId ,
							ChildResource ,
							ChildResourceURL ,
							ParentResourceId ,
							ParentResource ,
							( SELECT TOP 1
										HierarchyIndex
							  FROM      dbo.syNavigationNodes
							  WHERE     ResourceId = ChildResourceId
							) AS FirstSortOrder ,
							CASE WHEN ( ChildResourceId IN ( 190, 129, 71 )
										OR ParentResourceId IN ( 190, 129, 71 )
									  ) THEN 1
								 ELSE 2
							END AS DisplaySequence
				  FROM      ( SELECT DISTINCT
										NNChild.ResourceId AS ChildResourceId ,
										CASE WHEN NNChild.ResourceId = 394
											 THEN 'Student Tabs'
											 ELSE RChild.Resource
										END AS ChildResource ,
										RChild.ResourceURL AS ChildResourceURL ,
										CASE WHEN NNParent.ResourceId = 687
											 THEN NULL
											 ELSE NNParent.ResourceId
										END AS ParentResourceId ,
										RParent.Resource AS ParentResource ,
										RParentModule.Resource AS MODULE  
	--NNParent.ParentId  
							  FROM      syResources RChild
										INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
										INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
										INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
										LEFT OUTER JOIN ( SELECT
															  *
														  FROM
															  syResources
														  WHERE
															  ResourceTypeId = 1
														) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
							  WHERE     RChild.ResourceTypeId IN ( 2, 3, 8 )
										AND ( RChild.ChildTypeId IS NULL
											  OR RChild.ChildTypeId = 3
											)
										AND ( RChild.ResourceId <> 394 )
										AND ( NNParent.ResourceId NOT IN ( 394 ) )
										AND ( NNParent.Parentid IN (
											  SELECT    HierarchyId
											  FROM      syNavigationNodes
											  WHERE     ResourceId = 26 )
											  OR NNChild.ParentId IN (
											  SELECT    HierarchyId
											  FROM      syNavigationNodes
											  WHERE     ResourceId IN ( 693,
															  71, 129, 132,
															  160, 190 ) )
											)
										AND ( RChild.UsedIn
											  & @SchoolEnumerator > 0 )
							) t1
				  UNION ALL
				  SELECT    194 AS ModuleResourceId ,
							ChildResourceId ,
							ChildResource ,
							ChildResourceURL ,
							ParentResourceId ,
							ParentResource ,
							( SELECT TOP 1
										HierarchyIndex
							  FROM      dbo.syNavigationNodes
							  WHERE     ResourceId = ChildResourceId
							) AS FirstSortOrder ,
							CASE WHEN ( ChildResourceId IN ( 695 )
										OR ParentResourceId IN ( 695 )
									  ) THEN 1
								 ELSE 2
							END AS DisplaySequence
				  FROM      ( SELECT DISTINCT
										NNChild.ResourceId AS ChildResourceId ,
										CASE WHEN NNChild.ResourceId = 394
											 THEN 'Student Tabs'
											 ELSE RChild.Resource
										END AS ChildResource ,
										RChild.ResourceURL AS ChildResourceURL ,
										CASE WHEN NNParent.ResourceId IN ( 194 )
												  OR NNChild.ResourceId IN (
												  695, 403, 698 ) THEN NULL
											 ELSE NNParent.ResourceId
										END AS ParentResourceId ,
										RParent.Resource AS ParentResource ,
										RParentModule.Resource AS MODULE  
	--NNParent.ParentId  
							  FROM      syResources RChild
										INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
										INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
										INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
										LEFT OUTER JOIN ( SELECT
															  *
														  FROM
															  syResources
														  WHERE
															  ResourceTypeId = 1
														) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
							  WHERE     RChild.ResourceTypeId IN ( 2, 3, 8 )
										AND ( RChild.ChildTypeId IS NULL
											  OR RChild.ChildTypeId = 3
											)  
	--AND   
	--(  
	-- --NNParent.ResourceId IN (687,403,695)  
	--)  
										AND ( NNParent.Parentid IN (
											  SELECT    HierarchyId
											  FROM      syNavigationNodes
											  WHERE     ResourceId = 194 )
											  OR NNChild.ParentId IN (
											  SELECT    HierarchyId
											  FROM      syNavigationNodes
											  WHERE     ResourceId IN ( 403,
															  695, 698 ) )
											)
										AND ( RChild.ResourceId <> 394 )
										AND ( NNParent.ResourceId NOT IN ( 394 ) )   
	-- The following condition uses Bitwise Operator  
										AND ( RChild.UsedIn
											  & @SchoolEnumerator > 0 )
							) t2
				  UNION ALL
				  SELECT    300 AS ModuleResourceId ,
							ChildResourceId ,
							ChildResource ,
							ChildResourceURL ,
							ParentResourceId ,
							ParentResource ,
							( SELECT TOP 1
										HierarchyIndex
							  FROM      dbo.syNavigationNodes
							  WHERE     ResourceId = ChildResourceId
							) AS FirstSortOrder ,
							1 AS DisplaySequence
				  FROM      ( SELECT DISTINCT
										NNChild.ResourceId AS ChildResourceId ,
										CASE WHEN NNChild.ResourceId = 394
											 THEN 'Student Tabs'
											 ELSE RChild.Resource
										END AS ChildResource ,
										RChild.ResourceURL AS ChildResourceURL ,
										CASE WHEN NNParent.ResourceId = 300
												  OR NNChild.ResourceId = 697
											 THEN NULL
											 ELSE NNParent.ResourceId
										END AS ParentResourceId ,
										RParent.Resource AS ParentResource ,
										RParentModule.Resource AS MODULE  
	--NNParent.ParentId  
							  FROM      syResources RChild
										INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
										INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
										INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
										LEFT OUTER JOIN ( SELECT
															  *
														  FROM
															  syResources
														  WHERE
															  ResourceTypeId = 1
														) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
							  WHERE     RChild.ResourceTypeId IN ( 2, 3, 8 )
										AND ( RChild.ChildTypeId IS NULL
											  OR RChild.ChildTypeId = 3
											)
										AND ( RChild.ResourceId <> 394 )
										AND ( NNParent.ResourceId NOT IN ( 394 ) )
										AND ( NNParent.Parentid IN (
											  SELECT    HierarchyId
											  FROM      syNavigationNodes
											  WHERE     ResourceId = 300 )
											  OR NNChild.ParentId IN (
											  SELECT    HierarchyId
											  FROM      syNavigationNodes
											  WHERE     ResourceId IN ( 697 ) )
											)   
	-- The following condition uses Bitwise Operator  
										AND ( RChild.UsedIn
											  & @SchoolEnumerator > 0 )
							) t3  
  --SELECT * FROM syResources WHERE ResourceId=697  
				  UNION ALL
				  SELECT    191 AS ModuleResourceId ,
							ChildResourceId ,
							ChildResource ,
							ChildResourceURL ,
							ParentResourceId ,
							ParentResource ,
							( SELECT TOP 1
										HierarchyIndex
							  FROM      dbo.syNavigationNodes
							  WHERE     ResourceId = ChildResourceId
							) AS FirstSortOrder ,
							CASE WHEN ( ChildResourceId IN ( 698, 700 )
										OR ParentResourceId IN ( 698, 700 )
									  ) THEN 1
								 ELSE 2
							END AS DisplaySequence
				  FROM      ( SELECT DISTINCT
										NNChild.ResourceId AS ChildResourceId ,
										CASE WHEN NNChild.ResourceId = 394
											 THEN 'Student Tabs'
											 ELSE RChild.Resource
										END AS ChildResource ,
										RChild.ResourceURL AS ChildResourceURL ,
										CASE WHEN ( NNParent.ResourceId = 191
													OR NNChild.ResourceId = 698
													OR NNChild.ResourceId = 699
													OR NNChild.ResourceId = 700
												  ) THEN NULL
											 ELSE NNParent.ResourceId
										END AS ParentResourceId ,
										RParent.Resource AS ParentResource ,
										RParentModule.Resource AS MODULE  
	--NNParent.ParentId  
							  FROM      syResources RChild
										INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
										INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
										INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
										LEFT OUTER JOIN ( SELECT
															  *
														  FROM
															  syResources
														  WHERE
															  ResourceTypeId = 1
														) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
							  WHERE     RChild.ResourceTypeId IN ( 2, 3, 8 )
										AND ( RChild.ChildTypeId IS NULL
											  OR RChild.ChildTypeId = 3
											)
										AND ( RChild.ResourceId <> 394 )
										AND ( NNParent.ResourceId NOT IN ( 394 ) )
										AND ( NNParent.Parentid IN (
											  SELECT    HierarchyId
											  FROM      syNavigationNodes
											  WHERE     ResourceId = 191 )
											  OR NNChild.ParentId IN (
											  SELECT    HierarchyId
											  FROM      syNavigationNodes
											  WHERE     ResourceId IN ( 700,
															  698, 699 ) )
											)   
	-- The following condition uses Bitwise Operator  
										AND ( RChild.UsedIn
											  & @SchoolEnumerator > 0 )
							) t4
				  UNION ALL
				  SELECT    193 AS ModuleResourceId ,
							ChildResourceId ,
							ChildResource ,
							ChildResourceURL ,
							ParentResourceId ,
							ParentResource ,
							( SELECT TOP 1
										HierarchyIndex
							  FROM      dbo.syNavigationNodes
							  WHERE     ResourceId = ChildResourceId
							) AS FirstSortOrder ,
							CASE WHEN ( ChildResourceId IN ( 696 )
										OR ParentResourceId IN ( 696 )
									  ) THEN 1
								 ELSE 2
							END AS DisplaySequence
				  FROM      ( SELECT DISTINCT
										NNChild.ResourceId AS ChildResourceId ,
										CASE WHEN NNChild.ResourceId = 394
											 THEN 'Student Tabs'
											 ELSE CASE WHEN NNChild.ResourceId = 397
													   THEN 'Employer Tabs'
													   ELSE RChild.Resource
												  END
										END AS ChildResource ,
										RChild.ResourceURL AS ChildResourceURL ,
										CASE WHEN ( NNParent.ResourceId = 193
													OR NNChild.ResourceId IN (
													696, 404 )
												  ) THEN NULL
											 ELSE NNParent.ResourceId
										END AS ParentResourceId ,
										RParent.Resource AS ParentResource ,
										RParentModule.Resource AS MODULE  
	--NNParent.ParentId  
							  FROM      syResources RChild
										INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
										INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
										INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
										LEFT OUTER JOIN ( SELECT
															  *
														  FROM
															  syResources
														  WHERE
															  ResourceTypeId = 1
														) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
							  WHERE     RChild.ResourceTypeId IN ( 2, 3, 8 )
										AND ( RChild.ChildTypeId IS NULL
											  OR RChild.ChildTypeId = 3
											)
										AND ( RChild.ResourceId <> 394 )
										AND ( NNParent.ResourceId NOT IN ( 394,
															  397 ) )
										AND ( NNParent.Parentid IN (
											  SELECT    HierarchyId
											  FROM      syNavigationNodes
											  WHERE     ResourceId = 193 )
											  OR NNChild.ParentId IN (
											  SELECT    HierarchyId
											  FROM      syNavigationNodes
											  WHERE     ResourceId IN ( 404,
															  696 ) )
											)   
	-- The following condition uses Bitwise Operator  
										AND ( RChild.UsedIn
											  & @SchoolEnumerator > 0 )
							) t5
				  UNION ALL
				  SELECT    195 AS ModuleResourceId ,
							ChildResourceId ,
							ChildResource ,
							ChildResourceURL ,
							ParentResourceId ,
							ParentResource ,
							( SELECT TOP 1
										HierarchyIndex
							  FROM      dbo.syNavigationNodes
							  WHERE     ResourceId = ChildResourceId
							) AS FirstSortOrder ,
							CASE WHEN ( ChildResourceId IN ( 701 )
										OR ParentResourceId IN ( 701 )
									  ) THEN 1
								 ELSE 2
							END AS DisplaySequence
				  FROM      ( SELECT DISTINCT
										NNChild.ResourceId AS ChildResourceId ,
										CASE WHEN NNChild.ResourceId = 394
											 THEN 'Student Tabs'
											 ELSE RChild.Resource
										END AS ChildResource ,
										RChild.ResourceURL AS ChildResourceURL ,
										CASE WHEN ( NNParent.ResourceId = 195
													OR NNChild.ResourceId = 701
													OR NNChild.ResourceId = 407
												  ) THEN NULL
											 ELSE NNParent.ResourceId
										END AS ParentResourceId ,
										RParent.Resource AS ParentResource ,
										RParentModule.Resource AS MODULE  
	--NNParent.ParentId  
							  FROM      syResources RChild
										INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
										INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
										INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
										LEFT OUTER JOIN ( SELECT
															  *
														  FROM
															  syResources
														  WHERE
															  ResourceTypeId = 1
														) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
							  WHERE     RChild.ResourceTypeId IN ( 2, 3, 8 )
										AND ( RChild.ChildTypeId IS NULL
											  OR RChild.ChildTypeId = 3
											)
										AND ( RChild.ResourceId <> 394 )
										AND ( NNParent.ResourceId NOT IN ( 394 ) )
										AND ( NNParent.Parentid IN (
											  SELECT    HierarchyId
											  FROM      syNavigationNodes
											  WHERE     ResourceId = 195 )
											  OR NNChild.ParentId IN (
											  SELECT    HierarchyId
											  FROM      syNavigationNodes
											  WHERE     ResourceId IN ( 701,
															  407 ) )
											)   
	-- The following condition uses Bitwise Operator  
										AND ( RChild.UsedIn
											  & @SchoolEnumerator > 0 )
							) t6
				  UNION ALL
				  SELECT    192 AS ModuleResourceId ,
							ChildResourceId ,
							ChildResource ,
							ChildResourceURL ,
							ParentResourceId ,
							ParentResource ,
							( SELECT TOP 1
										HierarchyIndex
							  FROM      dbo.syNavigationNodes
							  WHERE     ResourceId = ChildResourceId
							) AS FirstSortOrder ,
							CASE WHEN ( ChildResourceId IN ( 708 )
										OR ParentResourceId IN ( 708 )
									  ) THEN 1
								 ELSE 2
							END AS DisplaySequence
				  FROM      ( SELECT DISTINCT
										NNChild.ResourceId AS ChildResourceId ,
										CASE WHEN NNChild.ResourceId = 394
											 THEN 'Student Tabs'
											 ELSE RChild.Resource
										END AS ChildResource ,
										RChild.ResourceURL AS ChildResourceURL ,
										CASE WHEN NNParent.ResourceId = 192
												  OR NNChild.ResourceId = 708
											 THEN NULL
											 ELSE NNParent.ResourceId
										END AS ParentResourceId ,
										RParent.Resource AS ParentResource ,
										RParentModule.Resource AS MODULE  
	--NNParent.ParentId  
							  FROM      syResources RChild
										INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
										INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
										INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
										LEFT OUTER JOIN ( SELECT
															  *
														  FROM
															  syResources
														  WHERE
															  ResourceTypeId = 1
														) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
							  WHERE     RChild.ResourceTypeId IN ( 2, 3, 8 )
										AND ( RChild.ChildTypeId IS NULL
											  OR RChild.ChildTypeId = 3
											)
										AND ( RChild.ResourceId NOT IN ( 394,
															  396 ) )
										AND ( NNParent.ResourceId NOT IN ( 394,
															  396 ) )
										AND ( NNParent.Parentid IN (
											  SELECT    HierarchyId
											  FROM      syNavigationNodes
											  WHERE     ResourceId = 192 )
											  OR NNChild.ParentId IN (
											  SELECT    HierarchyId
											  FROM      syNavigationNodes
											  WHERE     ResourceId IN ( 708 ) )
											)  
	-- The following condition uses Bitwise Operator  
										AND ( RChild.UsedIn
											  & @SchoolEnumerator > 0 )
							) t7
				) MainQuery
		WHERE   -- Hide resources based on Configuration Settings  
				(  
	 -- The following expression means if showross... is set to false, hide   
	 -- ResourceIds 541,542,532,534,535,538,543,539  
	 -- (Not False) OR (Condition)  
				  ( ( NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 0
					)
					OR ( ChildResourceId NOT IN ( 541, 542, 532, 534, 535, 538,
								--Commmented for DE8204              --    543,
											   539 ) )
				  )
				  AND  
	 -- The following expression means if showross... is set to true, hide   
	 -- ResourceIds 142,375,330,476,508,102,107,237  
	 -- (Not True) OR (Condition)  
				  ( ( NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 1
					)
					OR ( ChildResourceId NOT IN ( 142, 375, 330, 476, 508, 102,
												  107, 237, 217, 290 ) )
				  )
				  AND  
	 ---- The following expression means if SchedulingMethod=regulartraditional, hide   
	 ---- ResourceIds 91 and 497  
	 ---- (Not True) OR (Condition)  
				  ( ( NOT LOWER(LTRIM(RTRIM(@SchedulingMethod))) = 'regulartraditional'
					)
					OR ( ChildResourceId NOT IN ( 91, 497 ) )
				  )
				  AND  
	 -- The following expression means if TrackSAPAttendance=byday, hide   
	 -- ResourceIds 633  
	 -- (Not True) OR (Condition)  
				  ( ( NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byday'
					)
					OR ( ChildResourceId NOT IN ( 633 ) )
				  )
				  AND  
	 ---- The following expression means if @ShowCollegeOfCourtReporting is set to false, hide   
	 ---- ResourceIds 614,615  
	 ---- (Not False) OR (Condition)  
				  ( ( NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'no'
					)
					OR ( ChildResourceId NOT IN ( 614, 615 ) )
				  )
				  AND  
	 -- The following expression means if @ShowCollegeOfCourtReporting is set to true, hide   
	 -- ResourceIds 497  
	 -- (Not True) OR (Condition)  
				  ( ( NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'yes'
					)
					OR ( ChildResourceId NOT IN ( 497 ) )
				  )
				  AND  
	 -- The following expression means if FAMEESP is set to false, hide   
	 -- ResourceIds 517,523, 525  
	 -- (Not False) OR (Condition)  
				  ( ( NOT LOWER(LTRIM(RTRIM(@FameESP))) = 'no'
					)
					OR ( ChildResourceId NOT IN ( 517, 523, 525, 699 ) )
				  )
				  AND  
	 ---- The following expression means if EDExpress is set to false, hide   
	 ---- ResourceIds 603,604,606,619  
	 ---- (Not False) OR (Condition)  
				  ( ( NOT LOWER(LTRIM(RTRIM(@EdExpress))) = 'no'
					)
					OR ( ChildResourceId NOT IN ( 603, 604, 605, 606, 619, 698 ) )
				  )
				  AND  
	 ---- The following expression means if @GradeBookWeightingLevel is set to courselevel, hide   
	 ---- ResourceIds 107,96,222  
	 ---- (Not False) OR (Condition)  
				  ( ( NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'courselevel'
					)
					OR ( ChildResourceId NOT IN ( 107, 96, 222 ) )
				  )
				  AND ( ( NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'instructorlevel'
						)
						OR ( ChildResourceId NOT IN ( 476 ) )
					  )
				  AND ( ( NOT LOWER(LTRIM(RTRIM(@ShowExternshipTabs))) = 'no'
						)
						OR ( ChildResourceId NOT IN ( 543, 538 ) )
					  )
	 --            AND
	 --            ---- If user is not a power user (sa or support or super), hide Setup Required Fields (234)
				 ------ (Not False) OR (Condition)    
	 --             ((NOT @IsUserAPowerUser = 0) OR (ChildResourceId NOT IN (234)))   
				  AND  
	 ---- The following expression means if ConsolidateCourseComponents is set to false, hide   
	 ---- ResourceIds 796
	 ---- (Not False) OR (Condition)  
				  ( ( NOT LTRIM(RTRIM(@ConsolidateCourseComponents)) = 0
					)
					OR ( ChildResourceId NOT IN ( 796 ) )
				  )				  
				)
				
				
		ORDER BY ModuleResourceId ,
				FirstSortOrder ,
				ParentResourceId ,
				ChildResource  
		
	END


/* 
US4180 Add Transfer Components page

CREATED: 
6/20/2013 WMP

PURPOSE: 
Add transfer course component page to menu
*/

IF NOT EXISTS (SELECT * FROM syResources 
					WHERE ResourceURL like '~/AR/TransferComponents.aspx')

	BEGIN
	
	DECLARE @NewResourceId int
	SET @NewResourceId = 796
	
	DECLARE @ParentId uniqueidentifier
	SELECT TOP 1 @ParentId = n3.[HierarchyId] from syNavigationNodes n3 join syResources r3 on r3.ResourceID=n3.ResourceId
		where n3.ParentId =	
			(select top 1 n2.[HierarchyId] from syNavigationNodes n2 join syResources r2 on r2.ResourceID=n2.ResourceId
				where n2.ParentId =
					(select top 1 n1.[HierarchyId] from syNavigationNodes n1 join syResources r1 on r1.ResourceID = n1.ResourceId
						where n1.ParentId =
							(select top 1 n0.[HierarchyId] from syNavigationNodes n0 join syResources r0 on r0.ResourceID = n0.ResourceId 
								where n0.ParentId='00000000-0000-0000-0000-000000000000'  --Advantage
								and r0.ResourceTypeID=6 and n0.ResourceId is not null)
						and r1.ResourceTypeID=1 and r1.[Resource] like 'Academics')
				and r2.ResourceTypeID=2 and r2.[Resource] like 'Common Tasks')
		and r3.ResourceTypeID=2 and r3.[Resource] like 'Students'	
		
		--Add resource
		INSERT INTO syResources (
				ResourceID,
				[Resource],
				ResourceTypeID,
				ResourceURL,
				ModDate,
				ModUser,
				AllowSchlReqFlds,
				MRUTypeId,
				UsedIn)
			VALUES (
				@NewResourceId,
				'Transfer Partially Completed Components',
				3,
				'~/AR/TransferComponents.aspx',
				GETDATE(),
				'sa',
				0,
				1,
				975)

		--Add navigation node
		INSERT INTO syNavigationNodes (
				HierarchyIndex,
				ResourceId,
				ParentId,
				ModDate,
				ModUser,
				IsPopupWindow,
				IsShipped)
			VALUES (
				8,
				@NewResourceId,
				@ParentId,
				GETDATE(),
				'sa',
				0,
				1)
		
		--Link new resource to sysadmins role with full access (15)
		INSERT INTO syRlsResLvls (
				RRLId,
				RoleId,
				ResourceID,
				AccessLevel,
				ModDate,
				ModUser)
			VALUES (
				NEWID(),
				(select TOP 1 RoleId from syRoles where Code='SYSADMINS'),
				@NewResourceId,
				15,
				GETDATE(),
				'sa')		
			
	END

------------------------------------------------------------------------------------------
--US4180 Navigate to Transfer Partially Completed components
--End Wendy Prudente on 7/3/2013
------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------
--DE9829 Transfer partially completed components to future instance of course - 0 score 
--Start Wendy Prudente on 7/10/2013
------------------------------------------------------------------------------------------


SET ANSI_NULLS ON
GO
