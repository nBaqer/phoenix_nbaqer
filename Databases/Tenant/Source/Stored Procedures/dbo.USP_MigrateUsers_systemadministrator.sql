SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE Proc [dbo].[USP_MigrateUsers_systemadministrator]
@DatabaseName varchar(50),
@TenantId int
as
begin

	Declare @SecurityQuestion varchar(100),@SecurityAnswer varchar(50)
	Declare @ApplicationId uniqueidentifier,@LowerEmail varchar(100), @dbname varchar(50)
	DECLARE @UserId uniqueidentifier,@Email varchar(100),@AccountActive bit
	Set @ApplicationId='C75D06DD-25F6-45B5-93A3-B71806320191'
	Declare @SQL varchar(500),@SearchString varchar(50)
	
	Set @SearchString = 'sa'
	--Set @SQL = 'Select Distinct ''' + @DatabaseName + ''',UserId,Email,AccountActive from ' 
	--			+ @DatabaseName + '.dbo.syusers where Lower(username) = ''' + @SearchString + '''' + ' order by email'
	--Print @SQL
	--Execute(@SQL)
	
	Truncate table tblUsers
	
	
	SET @SQL = 'insert into tblUsers(DatabaseName,UserId,Email,AccountActive) Select Distinct ''' + @DatabaseName + ''',UserId,Email,AccountActive from ' 
				+ @DatabaseName + '.dbo.syusers where Lower(username) = ''' + @SearchString + '''' + ' order by email'
	--Print @SQL
	EXECUTE(@SQL);
	
	Declare @saUserId uniqueidentifier,@UserEmail varchar(50)
	
	
		
	DECLARE MigrateUsers_Cursor CURSOR LOCAL 
     FORWARD_ONLY 
     FAST_FORWARD 
     READ_ONLY 
     FOR 
		
		Select Distinct Top 1 newid() as UserId,Email,AccountActive from tblUsers
		
		OPEN MigrateUsers_Cursor
		FETCH NEXT FROM MigrateUsers_Cursor into @UserId,@Email,@AccountActive
		WHILE @@FETCH_STATUS = 0
		BEGIN
		
			--insert user's email address
			--DBName.OWNER.SOURCETABLENAME : Syntax is mandatory in insert statement
			BEGIN TRY			
				
				-- Build email address for sa based on school database name used
				BEGIN TRY
					
					Set @dbname = (Select Substring(@DatabaseName,2,CHARINDEX('3', @DatabaseName)-2))
					set @LowerEmail = 'sa@' + LTRIM(RTRIM(@dbname)) + '.edu'
					set @LowerEmail = Lower(@LowerEmail)
					--print 'try1'
					
				END TRY
				BEGIN CATCH
					--Print 'Catch1'
					set @LowerEmail = LOWER(@Email)
					set @UserEmail = Lower(@Email)
				END CATCH
				
				--Select @LowerEmail
				--Select @UserId
			
				Set @saUserId=@UserId
				
				if not exists (select * from TenantAuthDB.dbo.aspnet_Users where UserId=@saUserId)
				begin
					--Print @saUserId
					--Print @LowerEmail
					INSERT INTO TenantAuthDB.dbo.aspnet_Users 
							(ApplicationId,UserId,Username,LoweredUsername,MobileAlias,IsAnonymous,LastActivityDate)
					Values(@ApplicationId,@saUserId,@LowerEmail,@LowerEmail,NULL,0,getdate())
					
						
					-- Default password being used is : advantage3$
					-- Default security anwer: security answer
					-- User will be forced to change password at first login
					INSERT INTO dbo.aspnet_Membership 
					(ApplicationId,UserId,Password,PasswordFormat,PasswordSalt,MobilePIN,
					Email,LoweredEmail,PasswordQuestion,PasswordAnswer,IsApproved,IsLockedOut,
					CreateDate,LastLoginDate,LastPasswordChangedDate,LastLockoutDate,
					FailedPasswordAttemptCount,FailedPasswordAttemptWindowStart,FailedPasswordAnswerAttemptCount,
					FailedPasswordAnswerAttemptWindowStart,Comment)
					Values
					(@ApplicationId,@saUserId,'CcnbzYVEUDPVOH1ne2m/X2ttvQ4=', 1, 
					'ghMdIEJSDwFgiWkA/a0JTw==',NULL,@LowerEmail,@LowerEmail, 
					'What is your favourite color', 'BtJLiybtDny54+q4nmhXtzlXovE=',
					@AccountActive, 0, getdate(), getdate(), getdate(), getdate(), 0, getdate(), 0,getdate(), NULL)
					
				
					--Insert into TenantUsers
					If not exists (select * from TenantAuthDB.dbo.TenantUsers where TenantId=@TenantId and UserId=@saUserId)
					begin
						INSERT INTO TenantAuthDB.dbo.TenantUsers(TenantId,UserId,IsDefaultTenant)
						values(@TenantId,@saUserId,0)
					end
					
					--select @saUserId
				end
			END TRY
			BEGIN CATCH

				Insert into UserMigrationErrors(DatabaseName,UserId,Email,ErrorNumber,ErrorSeverity,ErrorState,ErrorProcedure,ErrorLine,ErrorMessage,ModUser,ModDate)
				SELECT 
				@DatabaseName,
				@UserId,
				@Email,
				ERROR_NUMBER() AS ErrorNumber,
				ERROR_SEVERITY() AS ErrorSeverity,
				ERROR_STATE() as ErrorState,
				ERROR_PROCEDURE() as ErrorProcedure,
				ERROR_LINE() as ErrorLine,
				ERROR_MESSAGE() as ErrorMessage,
				'support',getdate()
			END CATCH
			FETCH NEXT FROM MigrateUsers_Cursor into @UserId,@Email,@AccountActive
		END
		--Truncate table tblUsers -- Clear contents 
		CLOSE MigrateUsers_Cursor
		DEALLOCATE MigrateUsers_Cursor
		
			BEGIN TRY
					Set @dbname = (Select Substring(@DatabaseName,2,CHARINDEX('3', @DatabaseName)-2))
					set @LowerEmail = 'sa@' + LTRIM(RTRIM(@dbname)) + '.edu'
					set @LowerEmail = Lower(@LowerEmail)
					--print 'try1'
				END TRY
				BEGIN CATCH
					--Print 'Catch1'
					set @LowerEmail = LOWER(@UserEmail)
				END CATCH
		
		BEGIN TRY
			declare @saAccUserId uniqueidentifier
			Set @saAccUserId = (select Top 1 UserId from tblUsers)
			SET @SQL = 'Update ' + @DatabaseName + '.dbo.syusers set Email=''' + @LowerEmail + ''''  + ' where userId = ''' + Convert(char(36),@saAccUserId) + '''' 
			EXECUTE(@SQL);
		END TRY
		
		BEGIN CATCH 
		
		END CATCH
		
		Truncate table tblUsers
		
		Select @saUserId
		
END
GO
