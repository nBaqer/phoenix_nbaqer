SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create Proc [dbo].[USP_AttemptedClass_ClassStartDate]
@StuEnrollId uniqueidentifier,
@ClsSectionId uniqueidentifier
as
begin
Select Top 1 ClassStartDate
from
(
Select 
	  Top 1 Class.Startdate as ClassStartDate
from
	arresults results inner join arClassSections Class on results.testid = Class.ClsSectionId
	inner join arTerm Term on Class.TermId = Term.TermId
where
	results.stuenrollid=@StuEnrollId and
	results.TestId=@ClsSectionId
Union
Select Top 1 t4.startDate as ClassStartDate from arTransferGrades t9 inner join arTerm Term on t9.TermId=Term.termId 
inner join arClassSections t4 on t4.TermId=t9.TermId and t4.ReqId=t9.ReqId
where t9.StuEnrollId = @StuEnrollId and 
t4.ClsSectionId = @ClsSectionId
) tblDerived
end
GO
